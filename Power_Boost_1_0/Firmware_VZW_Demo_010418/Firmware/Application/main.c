
//#include "io430.h"
#include "main.h" 
//#define DEBUG_MAIN
#if defined (DEBUG_MAIN)
  #include "stdio.h"
#endif

#define APPLICATION_COMPANY     "JMA WIRELESS"
#define APPLICATION_HW_VERSION  "1.0.0"
#define APPLICATION_SW_VERSION  "1.0.0"

 void init_PSU_Comm_Peripheral()
 {
   init_UartAx(0);              // UART<->RS485 Delta        
   init_UartAx(3);              // UART Delta debug   
 }

 void init_Comm_Peripheral()
 {
   init_UartAx(1);              // UART RC Debug 
   init_UartAx(2);              // UART<-RS485 from RC 
 }

void main( void )
{
  // Stop watchdog timer to prevent time out reset
  Stop_WatchDog_Timer();
  #if defined (DEBUG_MAIN)
    printf("WDT DISABLED\n");
  #endif
  // Disable Global Interrupts
  __disable_interrupt();
    #if defined (DEBUG_MAIN)
    printf("INT DISABLED\n");
  #endif
  // Set Core voltage 
  SetVCore(2);
  #if defined (DEBUG_MAIN)
  printf("PWR UP\n");
  #endif
  // Initialize hardware IO to default/known state
  init_Hardware(); 
  #if defined (DEBUG_MAIN)
    printf("HW IO INIT\n");
  #endif  
  // Initialize system clocks
  init_sys_clock();
    #if defined (DEBUG_MAIN)
    printf("CLOCK SETUP\n");
  #endif

  // Set up peripherials UART
  init_PSU_Comm_Peripheral();
  
  #if defined (DEBUG_MAIN)
    printf("PHER COMM INIT\n");
  #endif
  // Initialize software parameters
  //  init_Protocol();
  // Enable Global interrupts
  __enable_interrupt();
  #if defined (DEBUG_MAIN)
    printf("INT ENABLED\n");
  #endif
  unsigned int startup_delay;
  for(startup_delay = 0; startup_delay < 500; startup_delay++)
  {
    if(process_HeartBeat_Message() == FALSE)
    {
      __delay_cycles(5000000);
    }
    else
    {
      break;
    }
  }
  
  #if defined (DEBUG_MAIN)
    printf("TIMER SETUP\n");
  #endif
  // Reset all the previous data 
  setup_OVP_Register();
  // Initialize PSU to know Setpoint
  Setup_PSU();
  //Set up peripherials UART
  init_Comm_Peripheral();
  // Initialize timer 
  init_Timer();
  
  while(1)
  {
    // Verify if end of message flag set
    unsigned char Rx_Message_Flag = verify_Msg_End_Flag_Rx(); 
    unsigned char Heartbeat_Flag = verify_Heartbeat_Flag();
    // Check if the Rx Message is Valid
    if (Rx_Message_Flag == 1) 
    {
      process_Message();
    }
    else if (Heartbeat_Flag == 1)
    {
      process_HeartBeat_Message();
    }
    // else if invalid or incomplete message received 
    else 
    {
      // LPM0;
      // No operation
      __no_operation();
    }
  }
}
