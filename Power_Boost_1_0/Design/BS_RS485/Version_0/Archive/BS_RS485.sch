<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.7.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="no"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="110" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="111" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="no"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="top_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="14" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="15" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
<layer number="255" name="Accent" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="JMA_LBR">
<packages>
<package name="QFP50P1600X1600X120-100N">
<smd name="1" x="-7.7" y="6" dx="1.5" dy="0.3" layer="1"/>
<smd name="2" x="-7.7" y="5.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="3" x="-7.7" y="5" dx="1.5" dy="0.3" layer="1"/>
<smd name="4" x="-7.7" y="4.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="5" x="-7.7" y="4" dx="1.5" dy="0.3" layer="1"/>
<smd name="6" x="-7.7" y="3.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="7" x="-7.7" y="3" dx="1.5" dy="0.3" layer="1"/>
<smd name="8" x="-7.7" y="2.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="9" x="-7.7" y="2" dx="1.5" dy="0.3" layer="1"/>
<smd name="10" x="-7.7" y="1.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="11" x="-7.7" y="1" dx="1.5" dy="0.3" layer="1"/>
<smd name="12" x="-7.7" y="0.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="13" x="-7.7" y="0" dx="1.5" dy="0.3" layer="1"/>
<smd name="14" x="-7.7" y="-0.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="15" x="-7.7" y="-1" dx="1.5" dy="0.3" layer="1"/>
<smd name="16" x="-7.7" y="-1.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="17" x="-7.7" y="-2" dx="1.5" dy="0.3" layer="1"/>
<smd name="18" x="-7.7" y="-2.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="19" x="-7.7" y="-3" dx="1.5" dy="0.3" layer="1"/>
<smd name="20" x="-7.7" y="-3.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="21" x="-7.7" y="-4" dx="1.5" dy="0.3" layer="1"/>
<smd name="22" x="-7.7" y="-4.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="23" x="-7.7" y="-5" dx="1.5" dy="0.3" layer="1"/>
<smd name="24" x="-7.7" y="-5.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="25" x="-7.7" y="-6" dx="1.5" dy="0.3" layer="1"/>
<smd name="26" x="-6" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="27" x="-5.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="28" x="-5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="29" x="-4.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="30" x="-4" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="31" x="-3.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="32" x="-3" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="33" x="-2.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="34" x="-2" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="35" x="-1.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="36" x="-1" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="37" x="-0.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="38" x="0" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="39" x="0.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="40" x="1" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="41" x="1.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="42" x="2" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="43" x="2.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="44" x="3" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="45" x="3.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="46" x="4" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="47" x="4.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="48" x="5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="49" x="5.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="50" x="6" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="51" x="7.7" y="-6" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="52" x="7.7" y="-5.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="53" x="7.7" y="-5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="54" x="7.7" y="-4.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="55" x="7.7" y="-4" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="56" x="7.7" y="-3.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="57" x="7.7" y="-3" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="58" x="7.7" y="-2.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="59" x="7.7" y="-2" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="60" x="7.7" y="-1.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="61" x="7.7" y="-1" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="62" x="7.7" y="-0.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="63" x="7.7" y="0" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="64" x="7.7" y="0.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="65" x="7.7" y="1" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="66" x="7.7" y="1.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="67" x="7.7" y="2" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="68" x="7.7" y="2.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="69" x="7.7" y="3" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="70" x="7.7" y="3.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="71" x="7.7" y="4" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="72" x="7.7" y="4.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="73" x="7.7" y="5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="74" x="7.7" y="5.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="75" x="7.7" y="6" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="76" x="6" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="77" x="5.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="78" x="5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="79" x="4.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="80" x="4" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="81" x="3.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="82" x="3" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="83" x="2.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="84" x="2" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="85" x="1.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="86" x="1" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="87" x="0.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="88" x="0" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="89" x="-0.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="90" x="-1" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="91" x="-1.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="92" x="-2" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="93" x="-2.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="94" x="-3" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="95" x="-3.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="96" x="-4" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="97" x="-4.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="98" x="-5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="99" x="-5.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="100" x="-6" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<wire x1="-6.5" y1="6.5" x2="6.5" y2="6.5" width="0.127" layer="21"/>
<wire x1="6.5" y1="6.5" x2="6.5" y2="-6.5" width="0.127" layer="21"/>
<wire x1="6.5" y1="-6.5" x2="-6.5" y2="-6.5" width="0.127" layer="21"/>
<wire x1="-6.5" y1="-6.5" x2="-6.5" y2="6.5" width="0.127" layer="21"/>
<wire x1="-9" y1="7" x2="-9" y2="-9" width="0.127" layer="21"/>
<wire x1="-9" y1="-9" x2="9" y2="-9" width="0.127" layer="21"/>
<wire x1="9" y1="-9" x2="9" y2="9" width="0.127" layer="21"/>
<wire x1="9" y1="9" x2="-7" y2="9" width="0.127" layer="21"/>
<wire x1="-7" y1="9" x2="-9" y2="7" width="0.127" layer="21"/>
<polygon width="0.127" layer="21">
<vertex x="-9" y="9"/>
<vertex x="-8.25" y="9"/>
<vertex x="-8.25" y="8.25"/>
<vertex x="-9" y="8.25"/>
</polygon>
<wire x1="-6.5" y1="6.5" x2="-6.5" y2="-6.5" width="0.127" layer="51"/>
<wire x1="-6.5" y1="-6.5" x2="6.5" y2="-6.5" width="0.127" layer="51"/>
<wire x1="6.5" y1="-6.5" x2="6.5" y2="6.5" width="0.127" layer="51"/>
<wire x1="6.5" y1="6.5" x2="-6.5" y2="6.5" width="0.127" layer="51"/>
<wire x1="-9" y1="7" x2="-9" y2="-9" width="0.127" layer="51"/>
<wire x1="-9" y1="-9" x2="9" y2="-9" width="0.127" layer="51"/>
<wire x1="9" y1="-9" x2="9" y2="9" width="0.127" layer="51"/>
<wire x1="9" y1="9" x2="-7" y2="9" width="0.127" layer="51"/>
<wire x1="-7" y1="9" x2="-9" y2="7" width="0.127" layer="51"/>
<wire x1="-9" y1="9" x2="-8.25" y2="9" width="0.127" layer="51"/>
<wire x1="-8.25" y1="9" x2="-8.25" y2="8.25" width="0.127" layer="51"/>
<wire x1="-8.25" y1="8.25" x2="-9" y2="8.25" width="0.127" layer="51"/>
<wire x1="-9" y1="8.25" x2="-9" y2="9" width="0.127" layer="51"/>
<text x="-7" y="9.5" size="0.6096" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-7" y="10.5" size="0.6096" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="TST-107-X-X-D">
<wire x1="-12.7" y1="5.461" x2="-11.43" y2="5.461" width="0.2032" layer="21"/>
<wire x1="-11.43" y1="5.461" x2="-11.43" y2="4.699" width="0.2032" layer="21"/>
<wire x1="-11.43" y1="4.699" x2="-1.27" y2="4.699" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="4.699" x2="-1.27" y2="5.461" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="5.461" x2="1.27" y2="5.461" width="0.2032" layer="21"/>
<wire x1="1.27" y1="5.461" x2="1.27" y2="4.699" width="0.2032" layer="21"/>
<wire x1="1.27" y1="4.699" x2="11.43" y2="4.699" width="0.2032" layer="21"/>
<wire x1="11.43" y1="4.699" x2="11.43" y2="5.461" width="0.2032" layer="21"/>
<wire x1="11.43" y1="5.461" x2="12.7" y2="5.461" width="0.2032" layer="21"/>
<wire x1="12.7" y1="5.461" x2="12.7" y2="-4.699" width="0.2032" layer="21"/>
<wire x1="12.7" y1="-4.699" x2="1.27" y2="-4.699" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-4.699" x2="-1.27" y2="-4.699" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-4.699" x2="-6.985" y2="-4.699" width="0.2032" layer="21"/>
<wire x1="-6.985" y1="-4.699" x2="-6.985" y2="-5.08" width="0.2032" layer="21"/>
<wire x1="-6.985" y1="-5.08" x2="-8.255" y2="-5.08" width="0.2032" layer="21"/>
<wire x1="-8.255" y1="-5.08" x2="-8.255" y2="-4.699" width="0.2032" layer="21"/>
<wire x1="-8.255" y1="-4.699" x2="-12.7" y2="-4.699" width="0.2032" layer="21"/>
<wire x1="-12.7" y1="-4.699" x2="-12.7" y2="5.461" width="0.2032" layer="21"/>
<pad name="1" x="7.62" y="-1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="2" x="7.62" y="1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="3" x="5.08" y="-1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="4" x="5.08" y="1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="5" x="2.54" y="-1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="6" x="2.54" y="1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="7" x="0" y="-1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="8" x="0" y="1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="9" x="-2.54" y="-1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="10" x="-2.54" y="1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="11" x="-5.08" y="-1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="12" x="-5.08" y="1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="13" x="-7.62" y="-1.27" drill="1" diameter="1.5" shape="square" rot="R180"/>
<pad name="14" x="-7.62" y="1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<text x="-9.098" y="-6.358" size="1.27" layer="21" font="vector" ratio="15" rot="SR0">1</text>
<text x="-8.89" y="6.35" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-8.89" y="7.62" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<rectangle x1="-7.97" y1="-1.62" x2="-7.27" y2="-0.92" layer="51"/>
<rectangle x1="-7.97" y1="0.92" x2="-7.27" y2="1.62" layer="51"/>
<rectangle x1="-5.43" y1="-1.62" x2="-4.73" y2="-0.92" layer="51"/>
<rectangle x1="-5.43" y1="0.92" x2="-4.73" y2="1.62" layer="51"/>
<rectangle x1="-2.89" y1="-1.62" x2="-2.19" y2="-0.92" layer="51"/>
<rectangle x1="-2.89" y1="0.92" x2="-2.19" y2="1.62" layer="51"/>
<rectangle x1="-0.35" y1="-1.62" x2="0.35" y2="-0.92" layer="51"/>
<rectangle x1="-0.35" y1="0.92" x2="0.35" y2="1.62" layer="51"/>
<rectangle x1="2.19" y1="-1.62" x2="2.89" y2="-0.92" layer="51"/>
<rectangle x1="2.19" y1="0.92" x2="2.89" y2="1.62" layer="51"/>
<rectangle x1="4.73" y1="-1.62" x2="5.43" y2="-0.92" layer="51"/>
<rectangle x1="4.73" y1="0.92" x2="5.43" y2="1.62" layer="51"/>
<rectangle x1="7.27" y1="-1.62" x2="7.97" y2="-0.92" layer="51"/>
<rectangle x1="7.27" y1="0.92" x2="7.97" y2="1.62" layer="51"/>
<wire x1="-11.43" y1="-3.81" x2="-1.27" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-3.81" x2="1.27" y2="-3.81" width="0.127" layer="21"/>
<wire x1="1.27" y1="-3.81" x2="11.43" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-11.43" y1="-3.81" x2="-11.43" y2="3.81" width="0.127" layer="21"/>
<wire x1="-11.43" y1="3.81" x2="11.43" y2="3.81" width="0.127" layer="21"/>
<wire x1="11.43" y1="3.81" x2="11.43" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-3.81" x2="-1.27" y2="-4.699" width="0.127" layer="21"/>
<wire x1="1.27" y1="-3.81" x2="1.27" y2="-4.699" width="0.127" layer="21"/>
</package>
<package name="RESC1005X40N">
<wire x1="-0.48" y1="-0.94" x2="-0.48" y2="0.94" width="0.127" layer="21"/>
<wire x1="-0.48" y1="0.94" x2="0.48" y2="0.94" width="0.127" layer="21"/>
<wire x1="0.48" y1="0.94" x2="0.48" y2="-0.94" width="0.127" layer="21"/>
<wire x1="0.48" y1="-0.94" x2="-0.48" y2="-0.94" width="0.127" layer="21" style="shortdash"/>
<smd name="1" x="0" y="0.48" dx="0.57" dy="0.62" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.48" dx="0.57" dy="0.62" layer="1" roundness="25" rot="R270"/>
<text x="0.635" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.524" y="1.397" size="0.635" layer="27" font="vector" ratio="10" rot="R270">&gt;VALUE</text>
<wire x1="-0.48" y1="-0.93" x2="-0.48" y2="0.93" width="0.127" layer="51"/>
<wire x1="-0.48" y1="0.93" x2="0.48" y2="0.93" width="0.127" layer="51"/>
<wire x1="0.48" y1="0.93" x2="0.48" y2="-0.93" width="0.127" layer="51"/>
<wire x1="0.48" y1="-0.93" x2="-0.48" y2="-0.93" width="0.127" layer="51" style="shortdash"/>
</package>
<package name="RESC1608X50N">
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.8" dx="0.95" dy="1" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.8" dx="0.95" dy="1" layer="1" roundness="25" rot="R270"/>
<text x="0.889" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.5875" y="1.27" size="0.635" layer="27" ratio="10" rot="R270">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
</package>
<package name="CAPC1005X55N">
<wire x1="-0.48" y1="-0.93" x2="-0.48" y2="0.93" width="0.127" layer="21"/>
<wire x1="-0.48" y1="0.93" x2="0.48" y2="0.93" width="0.127" layer="21"/>
<wire x1="0.48" y1="0.93" x2="0.48" y2="-0.93" width="0.127" layer="21"/>
<wire x1="0.48" y1="-0.93" x2="-0.48" y2="-0.93" width="0.127" layer="21" style="shortdash"/>
<smd name="1" x="0" y="0.45" dx="0.62" dy="0.62" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.45" dx="0.62" dy="0.62" layer="1" roundness="25" rot="R270"/>
<text x="0.635" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.524" y="1.397" size="0.635" layer="27" font="vector" ratio="10" rot="R270">&gt;VALUE</text>
<wire x1="-0.48" y1="-0.93" x2="-0.48" y2="0.93" width="0.127" layer="51"/>
<wire x1="-0.48" y1="0.93" x2="0.48" y2="0.93" width="0.127" layer="51"/>
<wire x1="0.48" y1="0.93" x2="0.48" y2="-0.93" width="0.127" layer="51"/>
<wire x1="0.48" y1="-0.93" x2="-0.48" y2="-0.93" width="0.127" layer="51" style="shortdash"/>
</package>
<package name="CAPC1608X55N">
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.762" dx="0.9" dy="0.95" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.762" dx="0.9" dy="0.95" layer="1" roundness="25" rot="R270"/>
<text x="0.889" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.5875" y="1.27" size="0.635" layer="27" ratio="10" rot="R270">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
</package>
<package name="CAPC2013X145N">
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.9" dx="1.15" dy="1.45" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.9" dx="1.15" dy="1.45" layer="1" roundness="25" rot="R270"/>
<text x="2.111" y="-2.02" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1.1625" y="-1.52" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="51"/>
</package>
<package name="3PIN_2POS_JUMPER">
<wire x1="-3.939" y1="1.155" x2="3.939" y2="1.155" width="0.2032" layer="21"/>
<wire x1="3.939" y1="1.155" x2="3.939" y2="-1.155" width="0.2032" layer="21"/>
<wire x1="3.939" y1="-1.155" x2="-3.939" y2="-1.155" width="0.2032" layer="21"/>
<wire x1="-3.939" y1="-1.155" x2="-3.939" y2="1.155" width="0.2032" layer="21"/>
<pad name="1" x="2.54" y="0" drill="1" diameter="1.5" shape="square" rot="R180"/>
<pad name="C" x="0" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="2" x="-2.54" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-6.985" y="-1.27" size="1.27" layer="25" font="vector" ratio="15" rot="R90">&gt;NAME</text>
<text x="6.985" y="-1.27" size="1.27" layer="27" font="vector" ratio="15" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.89" y1="-0.35" x2="-2.19" y2="0.35" layer="51"/>
<rectangle x1="-0.35" y1="-0.35" x2="0.35" y2="0.35" layer="51"/>
<rectangle x1="2.19" y1="-0.35" x2="2.89" y2="0.35" layer="51"/>
<wire x1="-3.939" y1="1.155" x2="3.939" y2="1.155" width="0.2032" layer="51"/>
<wire x1="3.939" y1="1.155" x2="3.939" y2="-1.155" width="0.2032" layer="51"/>
<wire x1="3.939" y1="-1.155" x2="-3.939" y2="-1.155" width="0.2032" layer="51"/>
<wire x1="-3.939" y1="-1.155" x2="-3.939" y2="1.155" width="0.2032" layer="51"/>
<text x="1.905" y="1.905" size="1.27" layer="21" font="vector" ratio="15">1</text>
<text x="-3.175" y="1.905" size="1.27" layer="21" font="vector" ratio="15">2</text>
<text x="-0.635" y="1.905" size="1.27" layer="21" font="vector" ratio="15">C</text>
</package>
<package name="RESC2013X70N">
<wire x1="1" y1="1.8" x2="1" y2="-1.8" width="0.127" layer="21"/>
<wire x1="1" y1="-1.8" x2="-1" y2="-1.8" width="0.127" layer="21"/>
<wire x1="-1" y1="-1.8" x2="-1" y2="1.8" width="0.127" layer="21"/>
<wire x1="-1" y1="1.8" x2="1" y2="1.8" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.95" dx="1.05" dy="1.4" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.95" dx="1.05" dy="1.4" layer="1" roundness="25" rot="R270"/>
<text x="1.511" y="-1.47" size="0.4064" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1.2125" y="-1.47" size="0.4064" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
</package>
<package name="RESC3216X84N">
<wire x1="1.275" y1="2.27" x2="1.275" y2="-2.27" width="0.127" layer="21"/>
<wire x1="1.275" y1="-2.27" x2="-1.275" y2="-2.27" width="0.127" layer="21"/>
<wire x1="-1.275" y1="-2.27" x2="-1.275" y2="2.27" width="0.127" layer="21"/>
<wire x1="-1.275" y1="2.27" x2="1.275" y2="2.27" width="0.127" layer="21"/>
<smd name="1" x="0" y="1.5" dx="1.05" dy="1.75" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-1.5" dx="1.05" dy="1.75" layer="1" roundness="25" rot="R270"/>
<text x="1.95" y="-2" size="0.4064" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1.5" y="-2" size="0.4064" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.75" x2="1.27" y2="0.75" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
</package>
<package name="RES2013X38N">
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="21"/>
<smd name="1" x="0" y="1" dx="0.95" dy="1.45" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-1" dx="0.95" dy="1.45" layer="1" roundness="25" rot="R270"/>
<text x="2" y="-2" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1" y="-2" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="51"/>
</package>
<package name="RESC5325X84N">
<wire x1="1.775" y1="3.52" x2="1.775" y2="-3.52" width="0.127" layer="21"/>
<wire x1="1.775" y1="-3.52" x2="-1.775" y2="-3.52" width="0.127" layer="21"/>
<wire x1="-1.775" y1="-3.52" x2="-1.775" y2="3.52" width="0.127" layer="21"/>
<wire x1="-1.775" y1="3.52" x2="1.775" y2="3.52" width="0.127" layer="21"/>
<smd name="1" x="0" y="2.55" dx="1.1" dy="2.65" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-2.55" dx="1.1" dy="2.65" layer="1" roundness="25" rot="R270"/>
<text x="2.45" y="-2" size="0.4064" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-2" y="-2" size="0.4064" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-3" y1="-1.27" x2="3" y2="1.27" layer="39" rot="R270"/>
</package>
<package name="CAPC3216X190N">
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.127" layer="21"/>
<smd name="1" x="0" y="1.5" dx="1.15" dy="1.8" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-1.5" dx="1.15" dy="1.8" layer="1" roundness="25" rot="R270"/>
<text x="2.361" y="-2.02" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1.4125" y="-1.52" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.75" x2="1.27" y2="0.75" layer="39" rot="R270"/>
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="51"/>
</package>
<package name="CAPC4532X279N">
<wire x1="2.175" y1="3.22" x2="2.175" y2="-3.22" width="0.127" layer="21"/>
<wire x1="2.175" y1="-3.22" x2="-2.175" y2="-3.22" width="0.127" layer="21"/>
<wire x1="-2.175" y1="-3.22" x2="-2.175" y2="3.22" width="0.127" layer="21"/>
<wire x1="-2.175" y1="3.22" x2="2.175" y2="3.22" width="0.127" layer="21"/>
<smd name="1" x="0" y="2.05" dx="1.4" dy="3.4" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-2.05" dx="1.4" dy="3.4" layer="1" roundness="25" rot="R270"/>
<text x="3.361" y="-2.02" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-2.6625" y="-1.52" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.264" y1="-1.494" x2="2.256" y2="1.514" layer="39" rot="R270"/>
<wire x1="1.56" y1="2.331" x2="1.56" y2="-2.355" width="0.127" layer="51"/>
<wire x1="1.56" y1="-2.355" x2="-1.56" y2="-2.355" width="0.127" layer="51"/>
<wire x1="-1.56" y1="-2.355" x2="-1.56" y2="2.331" width="0.127" layer="51"/>
<wire x1="-1.56" y1="2.331" x2="1.56" y2="2.331" width="0.127" layer="51"/>
</package>
<package name="ESWITCH_TL3304AF260QJ">
<smd name="1" x="-4.8006" y="-1.9939" dx="3.302" dy="0.9906" layer="1"/>
<smd name="2" x="4.8006" y="-1.9939" dx="3.302" dy="0.9906" layer="1"/>
<smd name="4" x="4.8006" y="1.9939" dx="3.302" dy="0.9906" layer="1"/>
<smd name="3" x="-4.8006" y="1.9939" dx="3.302" dy="0.9906" layer="1"/>
<wire x1="-5.08" y1="3.175" x2="-5.08" y2="5.08" width="0.127" layer="21"/>
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.127" layer="21"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="3.175" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-3.175" x2="-5.08" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="-5.08" width="0.127" layer="21"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="-3.175" width="0.127" layer="21"/>
<circle x="0" y="0" radius="2.54" width="0.127" layer="21"/>
<circle x="0" y="0" radius="2.54" width="0.127" layer="51"/>
<wire x1="-5.08" y1="3.175" x2="-5.08" y2="5.08" width="0.127" layer="51"/>
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.127" layer="51"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="3.175" width="0.127" layer="51"/>
<wire x1="-5.08" y1="-3.175" x2="-5.08" y2="-5.08" width="0.127" layer="51"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="-5.08" width="0.127" layer="51"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="-3.175" width="0.127" layer="51"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.127" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.127" layer="21"/>
</package>
<package name="TP_1.5MM">
<pad name="1" x="0" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-1.905" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<rectangle x1="-0.35" y1="-0.35" x2="0.35" y2="0.35" layer="51"/>
</package>
<package name="C120H40">
<pad name="1" x="0" y="0" drill="0.6" diameter="1.2" thermals="no"/>
<text x="0.9" y="-0.1" size="0.254" layer="25">&gt;NAME</text>
<text x="0.9" y="-0.5" size="0.254" layer="21">&gt;LABEL</text>
</package>
<package name="C131H51">
<pad name="1" x="0" y="0" drill="0.71" diameter="1.31" thermals="no"/>
<text x="1.2" y="0.1" size="0.254" layer="25">&gt;NAME</text>
<text x="1.2" y="-0.2" size="0.254" layer="21">&gt;LABEL</text>
</package>
<package name="C406H326">
<pad name="1" x="0" y="0" drill="3.26" diameter="4.06" thermals="no"/>
<text x="2.2" y="1.1" size="0.254" layer="25">&gt;NAME</text>
<text x="2.2" y="0.8" size="0.254" layer="21">&gt;LABEL</text>
</package>
<package name="C491H411">
<pad name="1" x="0" y="0" drill="4.31" diameter="4.91" thermals="no"/>
<text x="3.2" y="1.1" size="0.254" layer="25">&gt;NAME</text>
<text x="3.2" y="0.8" size="0.254" layer="21">&gt;LABEL</text>
</package>
<package name="LEDC1005X60N">
<wire x1="-0.52" y1="-0.93" x2="-0.52" y2="0.93" width="0.127" layer="21"/>
<wire x1="-0.52" y1="0.93" x2="0.52" y2="0.93" width="0.127" layer="21"/>
<wire x1="0.52" y1="0.93" x2="0.52" y2="-0.93" width="0.127" layer="21"/>
<wire x1="0.52" y1="-0.93" x2="-0.52" y2="-0.93" width="0.127" layer="21" style="shortdash"/>
<smd name="1" x="0" y="0.45" dx="0.5" dy="0.7" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.45" dx="0.5" dy="0.7" layer="1" roundness="25" rot="R270"/>
<text x="0.635" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.524" y="1.397" size="0.635" layer="27" font="vector" ratio="10" rot="R270">&gt;VALUE</text>
<wire x1="-0.52" y1="-0.93" x2="-0.52" y2="0.93" width="0.127" layer="51"/>
<wire x1="-0.52" y1="0.93" x2="0.52" y2="0.93" width="0.127" layer="51"/>
<wire x1="0.52" y1="0.93" x2="0.52" y2="-0.93" width="0.127" layer="51"/>
<wire x1="0.52" y1="-0.93" x2="-0.52" y2="-0.93" width="0.127" layer="51" style="shortdash"/>
<wire x1="-0.5" y1="-1.15" x2="0.5" y2="-1.15" width="0.127" layer="21"/>
<wire x1="-0.5" y1="-1.15" x2="0.5" y2="-1.15" width="0.127" layer="51"/>
</package>
<package name="LEDC1608X75N/80N">
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.825" dx="0.95" dy="1" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.825" dx="0.95" dy="1" layer="1" roundness="25" rot="R270"/>
<text x="0.889" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.5875" y="1.27" size="0.635" layer="27" ratio="10" rot="R270">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.65" x2="0.675" y2="-1.65" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.65" x2="0.675" y2="-1.65" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.65" x2="0.675" y2="-1.65" width="0.127" layer="21"/>
</package>
<package name="3PIN_JMP1608X50N">
<wire x1="-1.597" y1="0.675" x2="3.121" y2="0.675" width="0.127" layer="21"/>
<wire x1="3.121" y1="0.675" x2="3.121" y2="-0.675" width="0.127" layer="21"/>
<wire x1="3.121" y1="-0.675" x2="-1.597" y2="-0.675" width="0.127" layer="21"/>
<wire x1="-1.597" y1="-0.675" x2="-1.597" y2="0.675" width="0.127" layer="21"/>
<smd name="1" x="-0.8" y="0" dx="0.95" dy="1" layer="1" roundness="25"/>
<smd name="C" x="0.8" y="0" dx="0.95" dy="1" layer="1" roundness="25"/>
<text x="2.54" y="1.27" size="0.6096" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.3302" y1="-0.508" x2="2.8702" y2="0.508" layer="39"/>
<wire x1="-1.597" y1="0.675" x2="3.121" y2="0.675" width="0.127" layer="51"/>
<wire x1="3.121" y1="0.675" x2="3.121" y2="-0.675" width="0.127" layer="51"/>
<wire x1="3.121" y1="-0.675" x2="-1.597" y2="-0.675" width="0.127" layer="51"/>
<wire x1="-1.597" y1="-0.675" x2="-1.597" y2="0.675" width="0.127" layer="51"/>
<smd name="2" x="2.39511875" y="0" dx="0.95" dy="1" layer="1" roundness="25"/>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39"/>
</package>
<package name="AB26TRQ">
<smd name="1" x="-0.45" y="2.45" dx="1.1" dy="0.6" layer="1" rot="R90"/>
<smd name="2" x="0.45" y="2.45" dx="1.1" dy="0.6" layer="1" rot="R90"/>
<smd name="3" x="0" y="-3.1" dx="2.4" dy="1.5" layer="1" rot="R90"/>
<wire x1="-1" y1="2" x2="-1" y2="-4.5" width="0.127" layer="21"/>
<wire x1="1" y1="2" x2="1" y2="-4.5" width="0.127" layer="21"/>
<wire x1="1" y1="-4.5" x2="-1" y2="-4.5" width="0.127" layer="21"/>
<wire x1="-1" y1="2" x2="-1" y2="-4.5" width="0.127" layer="51"/>
<wire x1="-1" y1="-4.5" x2="1" y2="-4.5" width="0.127" layer="51"/>
<wire x1="1" y1="-4.5" x2="1" y2="2" width="0.127" layer="51"/>
<wire x1="-1" y1="2.75" x2="-1" y2="3.25" width="0.127" layer="51"/>
<wire x1="-1" y1="3.25" x2="1" y2="3.25" width="0.127" layer="51"/>
<wire x1="1" y1="3.25" x2="1" y2="2.75" width="0.127" layer="51"/>
<wire x1="-1" y1="2.75" x2="-1" y2="3.25" width="0.127" layer="21"/>
<wire x1="-1" y1="3.25" x2="1" y2="3.25" width="0.127" layer="21"/>
<wire x1="1" y1="3.25" x2="1" y2="2.75" width="0.127" layer="21"/>
<text x="2" y="2" size="0.4064" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="2" y="1" size="0.4064" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="TSW-104-XX-X-S">
<wire x1="-5.209" y1="1.155" x2="5.209" y2="1.155" width="0.2032" layer="21"/>
<wire x1="5.209" y1="1.155" x2="5.209" y2="-1.155" width="0.2032" layer="21"/>
<wire x1="5.209" y1="-1.155" x2="-5.209" y2="-1.155" width="0.2032" layer="21"/>
<wire x1="-5.209" y1="-1.155" x2="-5.209" y2="1.155" width="0.2032" layer="21"/>
<pad name="1" x="3.81" y="0" drill="1" diameter="1.5" shape="square" rot="R180"/>
<pad name="2" x="1.27" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="3" x="-1.27" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="4" x="-3.81" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-5.715" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="8.255" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.16" y1="-0.35" x2="-3.46" y2="0.35" layer="51"/>
<rectangle x1="-1.62" y1="-0.35" x2="-0.92" y2="0.35" layer="51"/>
<rectangle x1="0.92" y1="-0.35" x2="1.62" y2="0.35" layer="51"/>
<rectangle x1="3.46" y1="-0.35" x2="4.16" y2="0.35" layer="51"/>
<wire x1="5.715" y1="0.9525" x2="5.715" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.5875" x2="5.715" y2="1.5875" width="0.127" layer="51"/>
<wire x1="5.715" y1="1.5875" x2="5.715" y2="0" width="0.127" layer="51"/>
<wire x1="-5.209" y1="1.155" x2="5.209" y2="1.155" width="0.2032" layer="51"/>
<wire x1="5.209" y1="1.155" x2="5.209" y2="-1.155" width="0.2032" layer="51"/>
<wire x1="5.209" y1="-1.155" x2="-5.209" y2="-1.155" width="0.2032" layer="51"/>
<wire x1="-5.209" y1="-1.155" x2="-5.209" y2="1.155" width="0.2032" layer="51"/>
</package>
<package name="TSW-104-XX-X-S_NOOUTLINE">
<pad name="1" x="3.81" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="2" x="1.27" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="3" x="-1.27" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="4" x="-3.81" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-5.715" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="8.255" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.16" y1="-0.35" x2="-3.46" y2="0.35" layer="51"/>
<rectangle x1="-1.62" y1="-0.35" x2="-0.92" y2="0.35" layer="51"/>
<rectangle x1="0.92" y1="-0.35" x2="1.62" y2="0.35" layer="51"/>
<rectangle x1="3.46" y1="-0.35" x2="4.16" y2="0.35" layer="51"/>
<wire x1="5.08" y1="-0.3175" x2="5.08" y2="0.3175" width="0.254" layer="21"/>
<wire x1="5.08" y1="-0.3175" x2="5.08" y2="0.3175" width="0.254" layer="51"/>
</package>
<package name="SOIC127P600X173-8N">
<smd name="1" x="-2.7" y="1.905" dx="1.6" dy="0.6" layer="1"/>
<smd name="2" x="-2.7" y="0.635" dx="1.6" dy="0.6" layer="1"/>
<smd name="3" x="-2.7" y="-0.635" dx="1.6" dy="0.6" layer="1"/>
<smd name="4" x="-2.7" y="-1.905" dx="1.6" dy="0.6" layer="1"/>
<smd name="5" x="2.7" y="-1.905" dx="1.6" dy="0.6" layer="1"/>
<smd name="6" x="2.7" y="-0.635" dx="1.6" dy="0.6" layer="1"/>
<smd name="7" x="2.7" y="0.635" dx="1.6" dy="0.6" layer="1"/>
<smd name="8" x="2.7" y="1.905" dx="1.6" dy="0.6" layer="1"/>
<wire x1="2.5" y1="3" x2="2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="-3" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-3" x2="2.5" y2="-3" width="0.127" layer="21"/>
<wire x1="2.5" y1="-3" x2="2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="3" x2="-2.5" y2="3" width="0.127" layer="21"/>
<wire x1="-2.5" y1="3" x2="-2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2.5" x2="-2.5" y2="3" width="0.127" layer="51"/>
<wire x1="-2.5" y1="3" x2="2.5" y2="3" width="0.127" layer="51"/>
<wire x1="2.5" y1="3" x2="2.5" y2="2.5" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="-3" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-3" x2="2.5" y2="-3" width="0.127" layer="51"/>
<wire x1="2.5" y1="-3" x2="2.5" y2="-2.5" width="0.127" layer="51"/>
<rectangle x1="-2.5" y1="3.5" x2="-2" y2="4" layer="51"/>
<rectangle x1="-2.5" y1="3.5" x2="-2" y2="4" layer="21"/>
<text x="-1.27" y="3.81" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-1.27" y="5.08" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<wire x1="-2.54" y1="2.54" x2="-3.81" y2="2.54" width="0.127" layer="51"/>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="-2.54" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.127" layer="51"/>
<wire x1="2.54" y1="2.54" x2="3.81" y2="2.54" width="0.127" layer="51"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-2.54" width="0.127" layer="51"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="51"/>
</package>
<package name="MURATA_PKLCS1212E4001-R1">
<smd name="1" x="-6.2" y="0" dx="4" dy="2.4" layer="1" rot="R90"/>
<smd name="2" x="6.2" y="0" dx="4" dy="2.4" layer="1" rot="R90"/>
<wire x1="-5" y1="3" x2="-5" y2="5" width="0.127" layer="21"/>
<wire x1="-5" y1="5" x2="5" y2="5" width="0.127" layer="21"/>
<wire x1="5" y1="5" x2="5" y2="3" width="0.127" layer="21"/>
<wire x1="-5" y1="-3" x2="-5" y2="-5" width="0.127" layer="21"/>
<wire x1="-5" y1="-5" x2="5" y2="-5" width="0.127" layer="21"/>
<wire x1="5" y1="-5" x2="5" y2="-3" width="0.127" layer="21"/>
<text x="9" y="1" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="9" y="-1" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="SOT-23-3">
<smd name="D" x="0" y="-1.1" dx="1.4" dy="1" layer="1" rot="R90"/>
<smd name="S" x="-0.95" y="1.1" dx="1.4" dy="1" layer="1" rot="R90"/>
<smd name="G" x="0.95" y="1.1" dx="1.4" dy="1" layer="1" rot="R90"/>
<wire x1="-2" y1="1" x2="-2" y2="-1" width="0.127" layer="21"/>
<wire x1="-2" y1="-1" x2="-1" y2="-1" width="0.127" layer="21"/>
<wire x1="1" y1="-1" x2="2" y2="-1" width="0.127" layer="21"/>
<wire x1="2" y1="-1" x2="2" y2="1" width="0.127" layer="21"/>
<wire x1="-0.25" y1="1" x2="0.25" y2="1" width="0.127" layer="21"/>
<wire x1="-2" y1="1" x2="-2" y2="-1" width="0.127" layer="51"/>
<wire x1="-2" y1="-1" x2="-1" y2="-1" width="0.127" layer="51"/>
<wire x1="2" y1="1" x2="2" y2="-1" width="0.127" layer="51"/>
<wire x1="2" y1="-1" x2="1" y2="-1" width="0.127" layer="51"/>
<wire x1="-0.25" y1="1" x2="0.25" y2="1" width="0.127" layer="51"/>
<text x="-2" y="2" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-2" y="4" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="TI_TCA9406_DCU">
<smd name="1" x="-0.75" y="-1.55" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="2" x="-0.25" y="-1.55" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="3" x="0.25" y="-1.55" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="4" x="0.75" y="-1.55" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="5" x="0.75" y="1.55" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="6" x="0.25" y="1.55" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="7" x="-0.25" y="1.55" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="8" x="-0.75" y="1.55" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<wire x1="-1.25" y1="1.5" x2="-1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.5" x2="-1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="-1.25" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.25" y1="1.5" x2="1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.5" x2="1.25" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.25" y1="1.5" x2="-1.5" y2="1.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="1.5" x2="-1.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-1.5" x2="-1.25" y2="-1.5" width="0.127" layer="51"/>
<wire x1="1.25" y1="1.5" x2="1.5" y2="1.5" width="0.127" layer="51"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="1.5" y1="-1.5" x2="1.25" y2="-1.5" width="0.127" layer="51"/>
<rectangle x1="-2.5" y1="-1.5" x2="-2" y2="-1" layer="51"/>
<rectangle x1="-2.5" y1="-1.5" x2="-2" y2="-1" layer="21"/>
<text x="-1.5" y="2.5" size="1.016" layer="21" font="vector" ratio="15">&gt;NAME</text>
<text x="-1.5" y="3.75" size="1.016" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="TERM_BLK_2POS_5MM">
<pad name="P" x="-2.5" y="0" drill="1.3" shape="square"/>
<pad name="N" x="2.5" y="0" drill="1.3"/>
<wire x1="-5.5" y1="-5" x2="-5.5" y2="5" width="0.127" layer="21"/>
<wire x1="-5.5" y1="5" x2="5.5" y2="5" width="0.127" layer="21"/>
<wire x1="5.5" y1="5" x2="5.5" y2="-5" width="0.127" layer="21"/>
<wire x1="5.5" y1="-5" x2="-5.5" y2="-5" width="0.127" layer="21"/>
<text x="-3.75" y="5.5" size="1.27" layer="21" font="vector" ratio="15">(+)</text>
<text x="1.25" y="5.5" size="1.27" layer="21" font="vector" ratio="15">(-)</text>
<text x="6" y="3" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="6" y="1" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="DO-214AC_475X290">
<wire x1="-2.02" y1="-3.43" x2="-2.02" y2="3.43" width="0.127" layer="21"/>
<wire x1="-2.02" y1="3.43" x2="2.02" y2="3.43" width="0.127" layer="21"/>
<wire x1="2.02" y1="3.43" x2="2.02" y2="-3.43" width="0.127" layer="21"/>
<wire x1="2.02" y1="-3.43" x2="-2.02" y2="-3.43" width="0.127" layer="21" style="shortdash"/>
<smd name="1" x="0" y="2.25" dx="1.7" dy="2.5" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-2.25" dx="1.7" dy="2.5" layer="1" roundness="25" rot="R270"/>
<text x="3" y="-2" size="0.635" layer="21" ratio="11" rot="R90">&gt;NAME</text>
<text x="-2.25" y="-2" size="0.635" layer="27" font="vector" ratio="10" rot="R90">&gt;VALUE</text>
<wire x1="-2.02" y1="-3.43" x2="-2.02" y2="3.43" width="0.127" layer="21"/>
<wire x1="-2.02" y1="3.43" x2="2.02" y2="3.43" width="0.127" layer="21"/>
<wire x1="2.02" y1="3.43" x2="2.02" y2="-3.43" width="0.127" layer="21"/>
<wire x1="2.02" y1="-3.43" x2="-2.02" y2="-3.43" width="0.127" layer="21" style="shortdash"/>
<wire x1="-2" y1="-3.65" x2="2" y2="-3.65" width="0.127" layer="21"/>
<wire x1="-2" y1="-3.65" x2="2" y2="-3.65" width="0.127" layer="21"/>
</package>
<package name="IND2013X130N">
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.85" dx="1.2" dy="1.45" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.85" dx="1.2" dy="1.45" layer="1" roundness="25" rot="R270"/>
<text x="2" y="-2" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1" y="-2" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="51"/>
</package>
<package name="ONSEMI_LM317">
<smd name="1" x="-2.54" y="-10.213" dx="1.016" dy="3.504" layer="1"/>
<smd name="2" x="0" y="0" dx="10.49" dy="8.38" layer="1"/>
<smd name="3" x="2.54" y="-10.213" dx="1.016" dy="3.504" layer="1"/>
<wire x1="-6" y1="5" x2="-6" y2="-6" width="0.127" layer="21"/>
<wire x1="-6" y1="-6" x2="-4" y2="-6" width="0.127" layer="21"/>
<wire x1="-4" y1="-6" x2="-4" y2="-13" width="0.127" layer="21"/>
<wire x1="-4" y1="-13" x2="-1" y2="-13" width="0.127" layer="21"/>
<wire x1="-1" y1="-13" x2="-1" y2="-6" width="0.127" layer="21"/>
<wire x1="-1" y1="-6" x2="1" y2="-6" width="0.127" layer="21"/>
<wire x1="1" y1="-6" x2="1" y2="-13" width="0.127" layer="21"/>
<wire x1="1" y1="-13" x2="4" y2="-13" width="0.127" layer="21"/>
<wire x1="4" y1="-13" x2="4" y2="-6" width="0.127" layer="21"/>
<wire x1="4" y1="-6" x2="6" y2="-6" width="0.127" layer="21"/>
<wire x1="6" y1="-6" x2="6" y2="5" width="0.127" layer="21"/>
<wire x1="6" y1="5" x2="-6" y2="5" width="0.127" layer="21"/>
<wire x1="-6" y1="5" x2="-6" y2="-6" width="0.127" layer="51"/>
<wire x1="-6" y1="-6" x2="-4" y2="-6" width="0.127" layer="51"/>
<wire x1="-4" y1="-6" x2="-4" y2="-13" width="0.127" layer="51"/>
<wire x1="-4" y1="-13" x2="-1" y2="-13" width="0.127" layer="51"/>
<wire x1="-1" y1="-13" x2="-1" y2="-6" width="0.127" layer="51"/>
<wire x1="-1" y1="-6" x2="1" y2="-6" width="0.127" layer="51"/>
<wire x1="1" y1="-6" x2="1" y2="-13" width="0.127" layer="51"/>
<wire x1="1" y1="-13" x2="4" y2="-13" width="0.127" layer="51"/>
<wire x1="4" y1="-13" x2="4" y2="-6" width="0.127" layer="51"/>
<wire x1="4" y1="-6" x2="6" y2="-6" width="0.127" layer="51"/>
<wire x1="6" y1="-6" x2="6" y2="5" width="0.127" layer="51"/>
<wire x1="6" y1="5" x2="-6" y2="5" width="0.127" layer="51"/>
<text x="-6" y="6" size="1.27" layer="25">&gt;NAME</text>
<text x="-6" y="8" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="DO-214AC_450X280">
<wire x1="-2.02" y1="-3.18" x2="-2.02" y2="3.18" width="0.127" layer="21"/>
<wire x1="-2.02" y1="3.18" x2="2.02" y2="3.18" width="0.127" layer="21"/>
<wire x1="2.02" y1="3.18" x2="2.02" y2="-3.18" width="0.127" layer="21"/>
<wire x1="2.02" y1="-3.18" x2="-2.02" y2="-3.18" width="0.127" layer="21" style="shortdash"/>
<smd name="1" x="0" y="2.25" dx="1.5" dy="3.6" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-2.25" dx="1.5" dy="3.6" layer="1" roundness="25" rot="R270"/>
<text x="3" y="-2" size="0.635" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-2.25" y="-2" size="0.635" layer="27" font="vector" ratio="10" rot="R90">&gt;VALUE</text>
<wire x1="-2.02" y1="-3.18" x2="-2.02" y2="3.18" width="0.127" layer="51"/>
<wire x1="-2.02" y1="3.18" x2="2.02" y2="3.18" width="0.127" layer="51"/>
<wire x1="2.02" y1="3.18" x2="2.02" y2="-3.18" width="0.127" layer="51"/>
<wire x1="2.02" y1="-3.18" x2="-2.02" y2="-3.18" width="0.127" layer="51" style="shortdash"/>
<wire x1="-2" y1="-3.4" x2="2" y2="-3.4" width="0.127" layer="21"/>
<wire x1="-2" y1="-3.4" x2="2" y2="-3.4" width="0.127" layer="51"/>
</package>
<package name="SOT-23">
<smd name="3" x="0" y="-1" dx="0.9" dy="0.8" layer="1" rot="R90"/>
<smd name="2" x="-0.95" y="1" dx="0.9" dy="0.8" layer="1" rot="R90"/>
<smd name="1" x="0.95" y="1" dx="0.9" dy="0.8" layer="1" rot="R90"/>
<wire x1="-2" y1="1" x2="-2" y2="-1" width="0.127" layer="21"/>
<wire x1="-2" y1="-1" x2="-1" y2="-1" width="0.127" layer="21"/>
<wire x1="1" y1="-1" x2="2" y2="-1" width="0.127" layer="21"/>
<wire x1="2" y1="-1" x2="2" y2="1" width="0.127" layer="21"/>
<wire x1="-0.25" y1="1" x2="0.25" y2="1" width="0.127" layer="21"/>
<wire x1="-2" y1="1" x2="-2" y2="-1" width="0.127" layer="51"/>
<wire x1="-2" y1="-1" x2="-1" y2="-1" width="0.127" layer="51"/>
<wire x1="2" y1="1" x2="2" y2="-1" width="0.127" layer="51"/>
<wire x1="2" y1="-1" x2="1" y2="-1" width="0.127" layer="51"/>
<wire x1="-0.25" y1="1" x2="0.25" y2="1" width="0.127" layer="51"/>
<text x="-2" y="2" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-2" y="4" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="TI_MSP430F5438A">
<pin name="1" x="-5.08" y="-30.48" visible="pad" length="middle"/>
<pin name="2" x="-5.08" y="-33.02" visible="pad" length="middle"/>
<pin name="3" x="-5.08" y="-35.56" visible="pad" length="middle"/>
<pin name="4" x="-5.08" y="-38.1" visible="pad" length="middle"/>
<pin name="5" x="-5.08" y="-40.64" visible="pad" length="middle"/>
<pin name="6" x="-5.08" y="-43.18" visible="pad" length="middle"/>
<pin name="7" x="-5.08" y="-45.72" visible="pad" length="middle"/>
<pin name="8" x="-5.08" y="-48.26" visible="pad" length="middle"/>
<pin name="9" x="-5.08" y="-50.8" visible="pad" length="middle"/>
<pin name="10" x="-5.08" y="-53.34" visible="pad" length="middle"/>
<pin name="11" x="-5.08" y="-55.88" visible="pad" length="middle"/>
<pin name="12" x="-5.08" y="-58.42" visible="pad" length="middle"/>
<pin name="13" x="-5.08" y="-60.96" visible="pad" length="middle"/>
<pin name="14" x="-5.08" y="-63.5" visible="pad" length="middle"/>
<pin name="15" x="-5.08" y="-66.04" visible="pad" length="middle"/>
<pin name="16" x="-5.08" y="-68.58" visible="pad" length="middle"/>
<pin name="17" x="-5.08" y="-71.12" visible="pad" length="middle"/>
<pin name="18" x="-5.08" y="-73.66" visible="pad" length="middle"/>
<pin name="19" x="-5.08" y="-76.2" visible="pad" length="middle"/>
<pin name="20" x="-5.08" y="-78.74" visible="pad" length="middle"/>
<pin name="21" x="-5.08" y="-81.28" visible="pad" length="middle"/>
<pin name="22" x="-5.08" y="-83.82" visible="pad" length="middle"/>
<pin name="23" x="-5.08" y="-86.36" visible="pad" length="middle"/>
<pin name="24" x="-5.08" y="-88.9" visible="pad" length="middle"/>
<pin name="25" x="-5.08" y="-91.44" visible="pad" length="middle"/>
<pin name="26" x="30.48" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="27" x="33.02" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="28" x="35.56" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="29" x="38.1" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="30" x="40.64" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="31" x="43.18" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="32" x="45.72" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="33" x="48.26" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="34" x="50.8" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="35" x="53.34" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="36" x="55.88" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="37" x="58.42" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="38" x="60.96" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="39" x="63.5" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="40" x="66.04" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="41" x="68.58" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="42" x="71.12" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="43" x="73.66" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="44" x="76.2" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="45" x="78.74" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="46" x="81.28" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="47" x="83.82" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="48" x="86.36" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="49" x="88.9" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="50" x="91.44" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="51" x="127" y="-91.44" visible="pad" length="middle" rot="R180"/>
<pin name="52" x="127" y="-88.9" visible="pad" length="middle" rot="R180"/>
<pin name="53" x="127" y="-86.36" visible="pad" length="middle" rot="R180"/>
<pin name="54" x="127" y="-83.82" visible="pad" length="middle" rot="R180"/>
<pin name="55" x="127" y="-81.28" visible="pad" length="middle" rot="R180"/>
<pin name="56" x="127" y="-78.74" visible="pad" length="middle" rot="R180"/>
<pin name="57" x="127" y="-76.2" visible="pad" length="middle" rot="R180"/>
<pin name="58" x="127" y="-73.66" visible="pad" length="middle" rot="R180"/>
<pin name="59" x="127" y="-71.12" visible="pad" length="middle" rot="R180"/>
<pin name="60" x="127" y="-68.58" visible="pad" length="middle" rot="R180"/>
<pin name="61" x="127" y="-66.04" visible="pad" length="middle" rot="R180"/>
<pin name="62" x="127" y="-63.5" visible="pad" length="middle" rot="R180"/>
<pin name="63" x="127" y="-60.96" visible="pad" length="middle" rot="R180"/>
<pin name="64" x="127" y="-58.42" visible="pad" length="middle" rot="R180"/>
<pin name="65" x="127" y="-55.88" visible="pad" length="middle" rot="R180"/>
<pin name="66" x="127" y="-53.34" visible="pad" length="middle" rot="R180"/>
<pin name="67" x="127" y="-50.8" visible="pad" length="middle" rot="R180"/>
<pin name="68" x="127" y="-48.26" visible="pad" length="middle" rot="R180"/>
<pin name="69" x="127" y="-45.72" visible="pad" length="middle" rot="R180"/>
<pin name="70" x="127" y="-43.18" visible="pad" length="middle" rot="R180"/>
<pin name="71" x="127" y="-40.64" visible="pad" length="middle" rot="R180"/>
<pin name="72" x="127" y="-38.1" visible="pad" length="middle" rot="R180"/>
<pin name="73" x="127" y="-35.56" visible="pad" length="middle" rot="R180"/>
<pin name="74" x="127" y="-33.02" visible="pad" length="middle" rot="R180"/>
<pin name="75" x="127" y="-30.48" visible="pad" length="middle" rot="R180"/>
<pin name="76" x="91.44" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="77" x="88.9" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="78" x="86.36" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="79" x="83.82" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="80" x="81.28" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="81" x="78.74" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="82" x="76.2" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="83" x="73.66" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="84" x="71.12" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="85" x="68.58" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="86" x="66.04" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="87" x="63.5" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="88" x="60.96" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="89" x="58.42" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="90" x="55.88" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="91" x="53.34" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="92" x="50.8" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="93" x="48.26" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="94" x="45.72" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="95" x="43.18" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="96" x="40.64" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="97" x="38.1" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="98" x="35.56" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="99" x="33.02" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="100" x="30.48" y="5.08" visible="pad" length="middle" rot="R270"/>
<text x="2.54" y="-71.12" size="1.778" layer="94">P1.0/TA0CLK/ACLK</text>
<text x="2.54" y="-73.66" size="1.778" layer="94">P1.1/TA0.0</text>
<text x="2.54" y="-76.2" size="1.778" layer="94">P1.2/TA0.1</text>
<text x="2.54" y="-78.74" size="1.778" layer="94">P1.3/TA0.2</text>
<text x="2.54" y="-81.28" size="1.778" layer="94">P1.4/TA0.3</text>
<text x="2.54" y="-83.82" size="1.778" layer="94">P1.5/TA0.4</text>
<text x="2.54" y="-86.36" size="1.778" layer="94">P1.6/SMCLK</text>
<text x="2.54" y="-88.9" size="1.778" layer="94">P1.7</text>
<text x="2.54" y="-91.44" size="1.778" layer="94">P2.0/TA1CLK/MCLK</text>
<text x="30.48" y="-119.38" size="1.778" layer="94" rot="R90">P2.1/TA1.0</text>
<text x="33.02" y="-119.38" size="1.778" layer="94" rot="R90">P2.2/TA1.1</text>
<text x="35.56" y="-119.38" size="1.778" layer="94" rot="R90">P2.3/TA1.2</text>
<text x="38.1" y="-119.38" size="1.778" layer="94" rot="R90">P2.4/RTCCLK</text>
<text x="40.64" y="-119.38" size="1.778" layer="94" rot="R90">P2.5</text>
<text x="43.18" y="-119.38" size="1.778" layer="94" rot="R90">P2.6/ACLK</text>
<text x="45.72" y="-119.38" size="1.778" layer="94" rot="R90">P2.7/ADC12CLK/DMAE0</text>
<text x="2.54" y="-55.88" size="1.778" layer="94">AVCC</text>
<text x="2.54" y="-58.42" size="1.778" layer="94">AVSS</text>
<text x="58.42" y="-119.38" size="1.778" layer="94" rot="R90">DVSS3</text>
<text x="60.96" y="-119.38" size="1.778" layer="94" rot="R90">DVCC3</text>
<text x="119.38" y="-58.42" size="1.778" layer="94" align="bottom-right">DVCC2</text>
<text x="119.38" y="-60.96" size="1.778" layer="94" align="bottom-right">DVSS2</text>
<text x="60.96" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">DVSS4</text>
<text x="63.5" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">DVCC4</text>
<text x="2.54" y="-68.58" size="1.778" layer="94">DVCC1</text>
<text x="2.54" y="-66.04" size="1.778" layer="94">DVSS1</text>
<text x="48.26" y="-119.38" size="1.778" layer="94" rot="R90">P3.0/USBSTE/UCA0CLK</text>
<text x="50.8" y="-119.38" size="1.778" layer="94" rot="R90">P3.1/UCB0SIMO/UCB0SDA</text>
<text x="53.34" y="-119.38" size="1.778" layer="94" rot="R90">P3.2/UCB0SOMI/UCB0SCL</text>
<text x="55.88" y="-119.38" size="1.778" layer="94" rot="R90">P3.2/UCB0CLK/UCA0STE</text>
<text x="63.5" y="-119.38" size="1.778" layer="94" rot="R90">P3.4/UCA0TXD/UCA0SIMO</text>
<text x="66.04" y="-119.38" size="1.778" layer="94" rot="R90">P3.5/UCA0RXD/UCA0SOMI</text>
<text x="68.58" y="-119.38" size="1.778" layer="94" rot="R90">P3.6/UCB1STE/UCA0CLK</text>
<text x="71.12" y="-119.38" size="1.778" layer="94" rot="R90">P3.7/UCB1SIMO/UCB1SDA</text>
<text x="73.66" y="-119.38" size="1.778" layer="94" rot="R90">P4.0/TB0.0</text>
<text x="76.2" y="-119.38" size="1.778" layer="94" rot="R90">P4.1/TB0.1</text>
<text x="78.74" y="-119.38" size="1.778" layer="94" rot="R90">P4.2/TB0.2</text>
<text x="81.28" y="-119.38" size="1.778" layer="94" rot="R90">P4.3/TB0.3</text>
<text x="83.82" y="-119.38" size="1.778" layer="94" rot="R90">P4.4/TB0.4</text>
<text x="86.36" y="-119.38" size="1.778" layer="94" rot="R90">P4.5/TB0.5</text>
<text x="88.9" y="-119.38" size="1.778" layer="94" rot="R90">P4.6/TB0.6</text>
<text x="91.44" y="-119.38" size="1.778" layer="94" rot="R90">P4.7/TB0CLK/SMCLK</text>
<text x="2.54" y="-30.48" size="1.778" layer="94">P6.4/A4</text>
<text x="2.54" y="-33.02" size="1.778" layer="94">P6.5/A5</text>
<text x="2.54" y="-35.56" size="1.778" layer="94">P6.6/A6</text>
<text x="2.54" y="-38.1" size="1.778" layer="94">P6.7/A7</text>
<text x="30.48" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P6.3/A3</text>
<text x="33.02" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P6.2/A2</text>
<text x="35.56" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P6.1/A1</text>
<text x="38.1" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P6.0/A0</text>
<text x="2.54" y="-40.64" size="1.778" layer="94">P7.4/A12</text>
<text x="2.54" y="-43.18" size="1.778" layer="94">P7.5/A13</text>
<text x="2.54" y="-45.72" size="1.778" layer="94">P7.6/A14</text>
<text x="2.54" y="-48.26" size="1.778" layer="94">P7.7/A15</text>
<text x="2.54" y="-50.8" size="1.778" layer="94">P5.0/A8/VREF+/VeREF+</text>
<text x="2.54" y="-53.34" size="1.778" layer="94">P5.1/A9/VREF-/VeREF-</text>
<text x="2.54" y="-60.96" size="1.778" layer="94">P7.0/XIN</text>
<text x="2.54" y="-63.5" size="1.778" layer="94">P7.1/XOUT</text>
<text x="40.64" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">!RST!/NMI/SBTDIO</text>
<text x="43.18" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">PJ.3/TCK</text>
<text x="45.72" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">PJ.2/TMS</text>
<text x="48.26" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">PJ.1/TDI/TCLK</text>
<text x="50.8" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">PJ.0/TDO</text>
<text x="53.34" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">TEST/SBWTCK</text>
<text x="55.88" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P5.3/XT2OUT</text>
<text x="58.42" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P5.2/XT2IN</text>
<text x="66.04" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P11.2/SMCLK</text>
<text x="68.58" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P11.1/MCLK</text>
<text x="71.12" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P11.0/ACLK</text>
<text x="73.66" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P10.7</text>
<text x="76.2" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P10.6</text>
<text x="78.74" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P10.5/UCA3RXD/UCA3SOMI</text>
<text x="81.28" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P10.4/UCA3TXD/UCA3SIMO</text>
<text x="83.82" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P10.3/UCA3CLK/UCA3STE</text>
<text x="86.36" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P10.2/UCB3SOMI/UCB3SCL</text>
<text x="88.9" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P10.1/UCB3SIMO/UCB3SDA</text>
<text x="91.44" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P10.0/UCB3STE/UCB3CLK</text>
<text x="119.38" y="-30.48" size="1.778" layer="94" align="bottom-right">P9.7</text>
<text x="119.38" y="-33.02" size="1.778" layer="94" align="bottom-right">P9.6</text>
<text x="119.38" y="-35.56" size="1.778" layer="94" align="bottom-right">P9.5/UCA2RXD/UCA2SOMI</text>
<text x="119.38" y="-38.1" size="1.778" layer="94" align="bottom-right">P9.4/UCA2TXD/UCA2SIMO</text>
<text x="119.38" y="-40.64" size="1.778" layer="94" align="bottom-right">P9.3/UCB2CLK/UCA2STE</text>
<text x="119.38" y="-43.18" size="1.778" layer="94" align="bottom-right">P9.2/UCB2SOMI/UCB2SCL</text>
<text x="119.38" y="-45.72" size="1.778" layer="94" align="bottom-right">P9.1/UCB2SIMO/UCB2SDA</text>
<text x="119.38" y="-48.26" size="1.778" layer="94" align="bottom-right">P9.0/UCB2STE/UCA2CLK</text>
<text x="119.38" y="-50.8" size="1.778" layer="94" align="bottom-right">P8.7</text>
<text x="119.38" y="-53.34" size="1.778" layer="94" align="bottom-right">P8.6/TA1.1</text>
<text x="119.38" y="-55.88" size="1.778" layer="94" align="bottom-right">P8.5/TA1.0</text>
<text x="119.38" y="-63.5" size="1.778" layer="94" align="bottom-right">VCORE</text>
<text x="119.38" y="-66.04" size="1.778" layer="94" align="bottom-right">P8.4/TA0.4</text>
<text x="119.38" y="-68.58" size="1.778" layer="94" align="bottom-right">P8.3/TA0.3</text>
<text x="119.38" y="-71.12" size="1.778" layer="94" align="bottom-right">P8.2/TA0.2</text>
<text x="119.38" y="-73.66" size="1.778" layer="94" align="bottom-right">P8.1/TA0.1</text>
<text x="119.38" y="-76.2" size="1.778" layer="94" align="bottom-right">P8.0/TA0.0</text>
<text x="119.38" y="-78.74" size="1.778" layer="94" align="bottom-right">P7.3/TA1.2</text>
<text x="119.38" y="-81.28" size="1.778" layer="94" align="bottom-right">P7.2/TB0OUTH/SVMOUT</text>
<text x="119.38" y="-83.82" size="1.778" layer="94" align="bottom-right">P5.7/UCA1RXD/UCA1SOMI</text>
<text x="119.38" y="-86.36" size="1.778" layer="94" align="bottom-right">P5.6/UCA1TXD/UCA1SIMO</text>
<text x="119.38" y="-88.9" size="1.778" layer="94" align="bottom-right">P5.5/UCB1CLK/UCA1STE</text>
<text x="119.38" y="-91.44" size="1.778" layer="94" align="bottom-right">P5.4/UCB1SOMI/UCB1SCL</text>
<wire x1="0" y1="-121.92" x2="0" y2="-15.24" width="0.254" layer="94"/>
<wire x1="0" y1="-15.24" x2="15.24" y2="0" width="0.254" layer="94"/>
<wire x1="15.24" y1="0" x2="121.92" y2="0" width="0.254" layer="94"/>
<wire x1="121.92" y1="0" x2="121.92" y2="-121.92" width="0.254" layer="94"/>
<wire x1="121.92" y1="-121.92" x2="0" y2="-121.92" width="0.254" layer="94"/>
<text x="50.8" y="-55.88" size="1.778" layer="94">MSP430F543XAIPZ</text>
</symbol>
<symbol name="PIN">
<pin name="1" x="-5.08" y="0" visible="off"/>
<wire x1="-3.302" y1="0" x2="-1.27" y2="0" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="94"/>
<text x="-3.302" y="0.508" size="1.27" layer="95" ratio="15">&gt;NAME</text>
</symbol>
<symbol name="RESISTER">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-1.27" y="1.4986" size="1.27" layer="95">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="96" distance="49">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="CAPACITOR">
<wire x1="-0.635" y1="-1.016" x2="-0.635" y2="0" width="0.254" layer="94"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="1.016" width="0.254" layer="94"/>
<wire x1="0.635" y1="1.016" x2="0.635" y2="0" width="0.254" layer="94"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="0.635" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-1.27" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="NC">
<wire x1="-0.635" y1="0.635" x2="0.635" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0.635" y1="0.635" x2="-0.635" y2="-0.635" width="0.254" layer="94"/>
</symbol>
<symbol name="3PIN_2POS_JUMPER">
<pin name="1" x="-2.54" y="-7.62" visible="pad" length="middle" rot="R90"/>
<pin name="C" x="0" y="-7.62" visible="pad" length="middle" rot="R90"/>
<pin name="2" x="2.54" y="-7.62" visible="pad" length="middle" rot="R90"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-2.54" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="-3.81" width="0.254" layer="94"/>
<wire x1="1.27" y1="-3.81" x2="-3.81" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="3.81" y2="0" width="0.254" layer="94" style="shortdash"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-1.27" width="0.254" layer="94" style="shortdash"/>
<wire x1="3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94" style="shortdash"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94" style="shortdash"/>
</symbol>
<symbol name="SWITCH_4PIN_SPST">
<text x="8.89" y="4.0386" size="1.27" layer="95">&gt;NAME</text>
<text x="8.89" y="1.778" size="1.27" layer="96" distance="49">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="3" x="-5.08" y="5.08" visible="pad" length="short" direction="pas"/>
<pin name="4" x="5.08" y="5.08" visible="pad" length="short" direction="pas" rot="R180"/>
<wire x1="-2.54" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="1.27" width="0.254" layer="94"/>
<circle x="0" y="3.556" radius="0.254" width="0.254" layer="94"/>
<circle x="0" y="1.524" radius="0.254" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.81" x2="0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="0.762" y2="1.27" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="1.778" y2="2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="TP">
<wire x1="0.762" y1="1.778" x2="0" y2="0.254" width="0.254" layer="94"/>
<wire x1="0" y1="0.254" x2="-0.762" y2="1.778" width="0.254" layer="94"/>
<wire x1="-0.762" y1="1.778" x2="0.762" y2="1.778" width="0.254" layer="94" curve="-180"/>
<text x="-1.27" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<pin name="PP" x="0" y="0" visible="off" length="short" direction="in" rot="R90"/>
</symbol>
<symbol name="LED">
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.143" y2="2.413" width="0.254" layer="94"/>
<wire x1="-1.143" y1="2.413" x2="-0.508" y2="1.778" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.778" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.143" y1="2.413" x2="0.889" y2="4.445" width="0.254" layer="94"/>
<wire x1="0.889" y1="4.445" x2="0.127" y2="4.318" width="0.254" layer="94"/>
<wire x1="0.889" y1="4.445" x2="0.762" y2="3.683" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.778" x2="1.524" y2="3.81" width="0.254" layer="94"/>
<wire x1="1.524" y1="3.81" x2="0.762" y2="3.683" width="0.254" layer="94"/>
<wire x1="1.524" y1="3.81" x2="1.397" y2="3.048" width="0.254" layer="94"/>
<text x="-7.62" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="+" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="-" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.15875" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.15875" layer="94"/>
</symbol>
<symbol name="3PIN_JUMPER">
<pin name="1" x="-5.08" y="-2.54" visible="pad" length="short" rot="R90"/>
<pin name="C" x="0" y="-2.54" visible="pad" length="short" rot="R90"/>
<pin name="2" x="5.08" y="-2.54" visible="pad" length="short" rot="R90"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="0" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="-5.08" y2="0" width="0.254" layer="94"/>
<text x="8.89" y="2.54" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="8.89" y="0" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<wire x1="-5.08" y1="0" x2="-7.62" y2="0" width="0.254" layer="94"/>
<wire x1="-4.4958" y1="2.5654" x2="-4.1148" y2="3.5814" width="0.2032" layer="94"/>
<wire x1="-4.1148" y1="3.5814" x2="-3.4798" y2="1.5494" width="0.2032" layer="94"/>
<wire x1="-3.4798" y1="1.5494" x2="-2.8448" y2="3.5814" width="0.2032" layer="94"/>
<wire x1="-2.8448" y1="3.5814" x2="-2.2098" y2="1.5494" width="0.2032" layer="94"/>
<wire x1="-2.2098" y1="1.5494" x2="-1.5748" y2="3.5814" width="0.2032" layer="94"/>
<wire x1="-1.5748" y1="3.5814" x2="-0.9398" y2="1.5494" width="0.2032" layer="94"/>
<wire x1="-4.5466" y1="2.54" x2="-5.08" y2="2.54" width="0.2032" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="0" width="0.2032" layer="94"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="2.54" width="0.2032" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="-0.0254" width="0.2032" layer="94"/>
<wire x1="-0.9398" y1="1.6002" x2="-0.635" y2="2.54" width="0.2032" layer="94"/>
<text x="-0.889" y="3.048" size="1.27" layer="97">0</text>
</symbol>
<symbol name="CRYSTAL_3PIN">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="0" width="0.254" layer="94"/>
<wire x1="1.016" y1="0" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="0" width="0.254" layer="94"/>
<wire x1="-1.016" y1="0" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="2.54" y="2.54" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="2.54" y="4.064" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="3" x="0" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R90"/>
<wire x1="-1.778" y1="-1.524" x2="-1.778" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-2.54" x2="1.778" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.778" y1="-2.54" x2="1.778" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-1.778" y1="2.54" x2="1.778" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.778" y1="2.54" x2="1.778" y2="1.524" width="0.254" layer="94"/>
<wire x1="-1.778" y1="2.54" x2="-1.778" y2="1.524" width="0.254" layer="94"/>
</symbol>
<symbol name="VALUE_LABEL">
<text x="0" y="0" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="RS485">
<text x="1.27" y="-7.62" size="1.778" layer="94" ratio="15">R0</text>
<text x="1.27" y="-12.7" size="1.778" layer="94" ratio="15">!RE</text>
<text x="1.27" y="-17.78" size="1.778" layer="94" ratio="15">DE</text>
<text x="1.27" y="-22.86" size="1.778" layer="94" ratio="15">DI</text>
<text x="19.05" y="-22.86" size="1.778" layer="94" ratio="15" align="bottom-right">GND</text>
<text x="19.05" y="-17.78" size="1.778" layer="94" ratio="15" align="bottom-right">A(D0/RI)</text>
<text x="19.05" y="-12.7" size="1.778" layer="94" ratio="15" align="bottom-right">B(!D0!/!RI!)</text>
<text x="19.05" y="-7.62" size="1.778" layer="94" ratio="15" align="bottom-right">VCC</text>
<wire x1="0" y1="-2.54" x2="0" y2="-25.4" width="0.254" layer="94"/>
<wire x1="0" y1="-25.4" x2="20.32" y2="-25.4" width="0.254" layer="94"/>
<wire x1="20.32" y1="-25.4" x2="20.32" y2="0" width="0.254" layer="94"/>
<wire x1="20.32" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<pin name="1" x="-2.54" y="-7.62" visible="pad" length="short"/>
<pin name="2" x="-2.54" y="-12.7" visible="pad" length="short"/>
<pin name="3" x="-2.54" y="-17.78" visible="pad" length="short"/>
<pin name="4" x="-2.54" y="-22.86" visible="pad" length="short"/>
<pin name="5" x="22.86" y="-22.86" visible="pad" length="short" rot="R180"/>
<pin name="6" x="22.86" y="-17.78" visible="pad" length="short" rot="R180"/>
<pin name="7" x="22.86" y="-12.7" visible="pad" length="short" rot="R180"/>
<pin name="8" x="22.86" y="-7.62" visible="pad" length="short" rot="R180"/>
<text x="6.35" y="-2.54" size="1.778" layer="94" ratio="15">RS485</text>
<text x="0" y="2.54" size="1.778" layer="95" ratio="15">&gt;NAME</text>
<text x="0" y="-27.94" size="1.778" layer="96" ratio="15">&gt;VALUE</text>
</symbol>
<symbol name="BUZZER">
<pin name="1" x="-2.54" y="-2.54" visible="pad" length="short" rot="R90"/>
<pin name="2" x="2.54" y="-2.54" visible="pad" length="short" rot="R90"/>
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="0.508" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.508" x2="-2.54" y2="0.508" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0.508" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0.508" x2="-2.54" y2="1.016" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.016" x2="-5.08" y2="3.556" width="0.254" layer="94"/>
<wire x1="-5.08" y1="3.556" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.508" x2="2.54" y2="1.016" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.016" x2="5.08" y2="3.556" width="0.254" layer="94"/>
<wire x1="5.08" y1="3.556" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="5.588" width="0.254" layer="94"/>
<wire x1="-5.08" y1="5.588" x2="5.08" y2="5.588" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.588" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-3.302" y1="7.62" x2="0" y2="10.668" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="10.668" x2="3.302" y2="7.62" width="0.254" layer="94" curve="-90"/>
<wire x1="-2.54" y1="6.858" x2="0" y2="9.144" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="9.144" x2="2.54" y2="6.604" width="0.254" layer="94" curve="-90"/>
<wire x1="-1.778" y1="6.096" x2="0" y2="7.874" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="7.874" x2="1.778" y2="6.096" width="0.254" layer="94" curve="-90"/>
<text x="7.62" y="2.54" size="1.27" layer="95" ratio="15">&gt;NAME</text>
<text x="7.62" y="0" size="1.27" layer="96" ratio="15">&gt;VALUE</text>
</symbol>
<symbol name="NMOS_FET">
<pin name="G" x="-2.54" y="-2.54" visible="pad" length="short"/>
<pin name="S" x="2.54" y="-5.08" visible="pad" length="short" rot="R90"/>
<pin name="D" x="2.54" y="5.08" visible="pad" length="short" rot="R270"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<text x="5.08" y="0" size="1.27" layer="95" ratio="15">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.27" layer="96" ratio="15">&gt;VALUE</text>
<wire x1="0.508" y1="2.54" x2="0.508" y2="2.032" width="0.254" layer="94"/>
<wire x1="0.508" y1="2.032" x2="0.508" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.508" y1="0.762" x2="0.508" y2="0" width="0.254" layer="94"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.508" y1="-1.524" x2="0.508" y2="-2.032" width="0.254" layer="94"/>
<wire x1="0.508" y1="-2.032" x2="0.508" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.508" y1="2.032" x2="2.54" y2="2.032" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.032" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.508" y1="-2.032" x2="2.54" y2="-2.032" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.032" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.508" y1="0" x2="0.762" y2="0" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.032" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="1.524" y2="0.254" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="1.524" y2="-0.254" width="0.254" layer="94"/>
<wire x1="1.524" y1="-0.254" x2="1.524" y2="0.254" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="4.064" y2="2.54" width="0.254" layer="94"/>
<wire x1="4.064" y1="2.54" x2="4.064" y2="0.508" width="0.254" layer="94"/>
<wire x1="4.064" y1="0.508" x2="3.556" y2="0.508" width="0.254" layer="94"/>
<wire x1="4.572" y1="0.508" x2="4.064" y2="0.508" width="0.254" layer="94"/>
<wire x1="4.064" y1="-0.508" x2="4.572" y2="-0.508" width="0.254" layer="94"/>
<wire x1="4.572" y1="-0.508" x2="4.064" y2="0.508" width="0.254" layer="94"/>
<wire x1="4.064" y1="0.508" x2="3.556" y2="-0.508" width="0.254" layer="94"/>
<wire x1="3.556" y1="-0.508" x2="4.064" y2="-0.508" width="0.254" layer="94"/>
<wire x1="4.064" y1="-0.508" x2="4.064" y2="-2.54" width="0.254" layer="94"/>
<wire x1="4.064" y1="-2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="TI_TCA9406">
<pin name="1" x="-12.7" y="7.62" visible="pad" length="middle"/>
<pin name="2" x="-12.7" y="2.54" visible="pad" length="middle"/>
<pin name="3" x="-12.7" y="-2.54" visible="pad" length="middle"/>
<pin name="4" x="-12.7" y="-7.62" visible="pad" length="middle"/>
<pin name="5" x="12.7" y="-7.62" visible="pad" length="middle" rot="R180"/>
<pin name="6" x="12.7" y="-2.54" visible="pad" length="middle" rot="R180"/>
<pin name="7" x="12.7" y="2.54" visible="pad" length="middle" rot="R180"/>
<pin name="8" x="12.7" y="7.62" visible="pad" length="middle" rot="R180"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="7.62" y2="12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="12.7" x2="-5.08" y2="12.7" width="0.254" layer="94"/>
<wire x1="-5.08" y1="12.7" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<text x="-6.35" y="7.62" size="1.27" layer="94" ratio="15">SDA_B</text>
<text x="-6.35" y="2.54" size="1.27" layer="94" ratio="15">GND</text>
<text x="-6.35" y="-2.54" size="1.27" layer="94" ratio="15">VCCA</text>
<text x="-6.35" y="-7.62" size="1.27" layer="94" ratio="15">SDA_A</text>
<text x="6.35" y="-7.62" size="1.27" layer="94" ratio="15" align="bottom-right">SCL_A</text>
<text x="6.35" y="-2.54" size="1.27" layer="94" ratio="15" align="bottom-right">OE</text>
<text x="6.35" y="2.54" size="1.27" layer="94" ratio="15" align="bottom-right">VCCB</text>
<text x="6.35" y="7.62" size="1.27" layer="94" ratio="15" align="bottom-right">SCL_B</text>
<text x="-7.62" y="15.24" size="1.27" layer="95" ratio="15">&gt;NAME</text>
<text x="-7.62" y="17.78" size="1.27" layer="95" ratio="15">&gt;VALUE</text>
<text x="-3.81" y="10.16" size="1.27" layer="94" ratio="15">TCA9406</text>
</symbol>
<symbol name="SCHOTTKY_DIODE">
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="3.302" x2="1.27" y2="0" width="0.254" layer="94"/>
<text x="-7.62" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="2" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="1" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-3.302" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.15875" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.15875" layer="94"/>
<wire x1="1.27" y1="3.302" x2="0.508" y2="3.302" width="0.254" layer="94"/>
<wire x1="0.508" y1="3.302" x2="0.508" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="-3.302" x2="2.032" y2="-3.302" width="0.254" layer="94"/>
<wire x1="2.032" y1="-3.302" x2="2.032" y2="-2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="INDUCTOR">
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="0" x2="1.27" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.254" layer="94" curve="-180"/>
<text x="-2.54" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="ONSEMI_LM317">
<text x="6.604" y="0" size="1.778" layer="94" ratio="15" rot="R180" align="center-left">VOUT</text>
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<pin name="2" x="10.16" y="0" visible="pad" length="short" rot="R180"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<text x="-6.604" y="0" size="1.778" layer="94" ratio="15" align="center-left">VIN</text>
<text x="-2.032" y="-3.81" size="1.778" layer="94" ratio="15" align="center-left">ADJ</text>
<text x="-3.81" y="3.556" size="1.778" layer="94" ratio="15" align="center-left">LM317</text>
<pin name="1" x="0" y="-7.62" visible="pad" length="short" rot="R90"/>
<pin name="3" x="-10.16" y="0" visible="pad" length="short"/>
<text x="-7.62" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="10.16" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="DIODE">
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="0" width="0.254" layer="94"/>
<text x="-7.62" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="+" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="-" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.15875" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.15875" layer="94"/>
</symbol>
<symbol name="PNP_TRANSISTOR">
<wire x1="-2.086" y1="-1.678" x2="-1.578" y2="-2.594" width="0.1524" layer="94"/>
<wire x1="-1.578" y1="-2.594" x2="-0.516" y2="-1.478" width="0.1524" layer="94"/>
<wire x1="-0.516" y1="-1.478" x2="-2.086" y2="-1.678" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.808" y2="-2.124" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-0.508" y2="1.524" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.778" x2="-1.524" y2="-2.413" width="0.254" layer="94"/>
<wire x1="-1.524" y1="-2.413" x2="-0.762" y2="-1.651" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-1.651" x2="-1.778" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-1.778" x2="-1.524" y2="-2.159" width="0.254" layer="94"/>
<wire x1="-1.524" y1="-2.159" x2="-1.143" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.143" y1="-1.905" x2="-1.524" y2="-1.905" width="0.254" layer="94"/>
<text x="10.16" y="-7.62" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<text x="10.16" y="-5.08" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<rectangle x1="-0.508" y1="-2.54" x2="0.254" y2="2.54" layer="94" rot="R180"/>
<pin name="B" x="2.54" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="E" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="C" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TI_MSP430F5438AIPZR" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="TI_MSP430F5438A" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFP50P1600X1600X120-100N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="100" pad="100"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="21" pad="21"/>
<connect gate="G$1" pin="22" pad="22"/>
<connect gate="G$1" pin="23" pad="23"/>
<connect gate="G$1" pin="24" pad="24"/>
<connect gate="G$1" pin="25" pad="25"/>
<connect gate="G$1" pin="26" pad="26"/>
<connect gate="G$1" pin="27" pad="27"/>
<connect gate="G$1" pin="28" pad="28"/>
<connect gate="G$1" pin="29" pad="29"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="30" pad="30"/>
<connect gate="G$1" pin="31" pad="31"/>
<connect gate="G$1" pin="32" pad="32"/>
<connect gate="G$1" pin="33" pad="33"/>
<connect gate="G$1" pin="34" pad="34"/>
<connect gate="G$1" pin="35" pad="35"/>
<connect gate="G$1" pin="36" pad="36"/>
<connect gate="G$1" pin="37" pad="37"/>
<connect gate="G$1" pin="38" pad="38"/>
<connect gate="G$1" pin="39" pad="39"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="40" pad="40"/>
<connect gate="G$1" pin="41" pad="41"/>
<connect gate="G$1" pin="42" pad="42"/>
<connect gate="G$1" pin="43" pad="43"/>
<connect gate="G$1" pin="44" pad="44"/>
<connect gate="G$1" pin="45" pad="45"/>
<connect gate="G$1" pin="46" pad="46"/>
<connect gate="G$1" pin="47" pad="47"/>
<connect gate="G$1" pin="48" pad="48"/>
<connect gate="G$1" pin="49" pad="49"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="50" pad="50"/>
<connect gate="G$1" pin="51" pad="51"/>
<connect gate="G$1" pin="52" pad="52"/>
<connect gate="G$1" pin="53" pad="53"/>
<connect gate="G$1" pin="54" pad="54"/>
<connect gate="G$1" pin="55" pad="55"/>
<connect gate="G$1" pin="56" pad="56"/>
<connect gate="G$1" pin="57" pad="57"/>
<connect gate="G$1" pin="58" pad="58"/>
<connect gate="G$1" pin="59" pad="59"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="60" pad="60"/>
<connect gate="G$1" pin="61" pad="61"/>
<connect gate="G$1" pin="62" pad="62"/>
<connect gate="G$1" pin="63" pad="63"/>
<connect gate="G$1" pin="64" pad="64"/>
<connect gate="G$1" pin="65" pad="65"/>
<connect gate="G$1" pin="66" pad="66"/>
<connect gate="G$1" pin="67" pad="67"/>
<connect gate="G$1" pin="68" pad="68"/>
<connect gate="G$1" pin="69" pad="69"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="70" pad="70"/>
<connect gate="G$1" pin="71" pad="71"/>
<connect gate="G$1" pin="72" pad="72"/>
<connect gate="G$1" pin="73" pad="73"/>
<connect gate="G$1" pin="74" pad="74"/>
<connect gate="G$1" pin="75" pad="75"/>
<connect gate="G$1" pin="76" pad="76"/>
<connect gate="G$1" pin="77" pad="77"/>
<connect gate="G$1" pin="78" pad="78"/>
<connect gate="G$1" pin="79" pad="79"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="80" pad="80"/>
<connect gate="G$1" pin="81" pad="81"/>
<connect gate="G$1" pin="82" pad="82"/>
<connect gate="G$1" pin="83" pad="83"/>
<connect gate="G$1" pin="84" pad="84"/>
<connect gate="G$1" pin="85" pad="85"/>
<connect gate="G$1" pin="86" pad="86"/>
<connect gate="G$1" pin="87" pad="87"/>
<connect gate="G$1" pin="88" pad="88"/>
<connect gate="G$1" pin="89" pad="89"/>
<connect gate="G$1" pin="9" pad="9"/>
<connect gate="G$1" pin="90" pad="90"/>
<connect gate="G$1" pin="91" pad="91"/>
<connect gate="G$1" pin="92" pad="92"/>
<connect gate="G$1" pin="93" pad="93"/>
<connect gate="G$1" pin="94" pad="94"/>
<connect gate="G$1" pin="95" pad="95"/>
<connect gate="G$1" pin="96" pad="96"/>
<connect gate="G$1" pin="97" pad="97"/>
<connect gate="G$1" pin="98" pad="98"/>
<connect gate="G$1" pin="99" pad="99"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TST-107-X-X-D" prefix="J" uservalue="yes">
<gates>
<gate name="-1" symbol="PIN" x="5.08" y="15.24"/>
<gate name="-2" symbol="PIN" x="5.08" y="12.7"/>
<gate name="-3" symbol="PIN" x="5.08" y="10.16"/>
<gate name="-4" symbol="PIN" x="5.08" y="7.62"/>
<gate name="-5" symbol="PIN" x="5.08" y="5.08"/>
<gate name="-6" symbol="PIN" x="5.08" y="2.54"/>
<gate name="-7" symbol="PIN" x="5.08" y="0"/>
<gate name="-8" symbol="PIN" x="5.08" y="-2.54"/>
<gate name="-9" symbol="PIN" x="5.08" y="-5.08"/>
<gate name="-10" symbol="PIN" x="5.08" y="-7.62"/>
<gate name="-11" symbol="PIN" x="5.08" y="-10.16"/>
<gate name="-12" symbol="PIN" x="5.08" y="-12.7"/>
<gate name="-13" symbol="PIN" x="5.08" y="-15.24"/>
<gate name="-14" symbol="PIN" x="5.08" y="-17.78"/>
</gates>
<devices>
<device name="" package="TST-107-X-X-D">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-10" pin="1" pad="10"/>
<connect gate="-11" pin="1" pad="11"/>
<connect gate="-12" pin="1" pad="12"/>
<connect gate="-13" pin="1" pad="13"/>
<connect gate="-14" pin="1" pad="14"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
<connect gate="-5" pin="1" pad="5"/>
<connect gate="-6" pin="1" pad="6"/>
<connect gate="-7" pin="1" pad="7"/>
<connect gate="-8" pin="1" pad="8"/>
<connect gate="-9" pin="1" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RES_SMD_" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="RESISTER" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="RESC1005X40N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="RESC1608X50N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="RESC2013X70N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="RESC3216X84N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805_SH" package="RES2013X38N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="RESC5325X84N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP_SMD_" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="CAPC1005X55N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="CAPC1608X55N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="CAPC2013X145N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="CAPC3216X190N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="CAPC4532X279N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NC" prefix="NC">
<gates>
<gate name="G$1" symbol="NC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3PIN_2POS_JMP" prefix="J" uservalue="yes">
<gates>
<gate name="G$1" symbol="3PIN_2POS_JUMPER" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="3PIN_2POS_JUMPER">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ESWITCH_TL3304AF260QL" prefix="S" uservalue="yes">
<gates>
<gate name="G$1" symbol="SWITCH_4PIN_SPST" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ESWITCH_TL3304AF260QJ">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="4"/>
<connect gate="G$1" pin="4" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TP_CLASSB_" prefix="TP">
<gates>
<gate name="G$1" symbol="TP" x="0" y="0"/>
</gates>
<devices>
<device name="1.5MM" package="TP_1.5MM">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0.4MM" package="C120H40">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0.51MM" package="C131H51">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.26MM" package="C406H326">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4.11MM" package="C491H411">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED_SMD_" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="LEDC1005X60N">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="LEDC1608X75N/80N">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3PIN_JUMPER_" prefix="J" uservalue="yes">
<gates>
<gate name="G$1" symbol="3PIN_JUMPER" x="0" y="0"/>
</gates>
<devices>
<device name="0603" package="3PIN_JMP1608X50N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AB26TRQ-32.768KHZ" prefix="X" uservalue="yes">
<gates>
<gate name="G$1" symbol="CRYSTAL_3PIN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="AB26TRQ">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TSW-104-XX-X-S" prefix="J" uservalue="yes">
<gates>
<gate name="-1" symbol="PIN" x="5.08" y="0"/>
<gate name="-2" symbol="PIN" x="5.08" y="-2.54"/>
<gate name="-3" symbol="PIN" x="5.08" y="-5.08"/>
<gate name="-4" symbol="PIN" x="5.08" y="-7.62"/>
<gate name="G$1" symbol="VALUE_LABEL" x="2.54" y="2.54"/>
</gates>
<devices>
<device name="VERTICAL" package="TSW-104-XX-X-S">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NO_OUTLINE" package="TSW-104-XX-X-S_NOOUTLINE">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TI_DS3695AX" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="RS485" x="10.16" y="-12.7"/>
</gates>
<devices>
<device name="" package="SOIC127P600X173-8N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MURATA_PKLCS1212E4001-R1" prefix="LS" uservalue="yes">
<gates>
<gate name="G$1" symbol="BUZZER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MURATA_PKLCS1212E4001-R1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BSS123" prefix="Q" uservalue="yes">
<gates>
<gate name="G$1" symbol="NMOS_FET" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT-23-3">
<connects>
<connect gate="G$1" pin="D" pad="D"/>
<connect gate="G$1" pin="G" pad="G"/>
<connect gate="G$1" pin="S" pad="S"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TI_TCA9406" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="TI_TCA9406" x="0" y="0"/>
</gates>
<devices>
<device name="8-VFSOP" package="TI_TCA9406_DCU">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PHEONIX_CONT_1935161" prefix="J" uservalue="yes">
<gates>
<gate name="P" symbol="PIN" x="5.08" y="0"/>
<gate name="N" symbol="PIN" x="5.08" y="-2.54"/>
</gates>
<devices>
<device name="" package="TERM_BLK_2POS_5MM">
<connects>
<connect gate="N" pin="1" pad="N"/>
<connect gate="P" pin="1" pad="P"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="COMCHIP_CDBA240LL-HF" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="SCHOTTKY_DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DO-214AC_475X290">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="2" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LBR2012T101K" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="IND2013X130N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ONSEMI_LM317" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="ONSEMI_LM317" x="0" y="0"/>
</gates>
<devices>
<device name="D2PAK-3_D2T-SUFFIX_CASE936" package="ONSEMI_LM317">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="COMCHIP_CGRA400X-G" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DO-214AC_450X280">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SMMBT2907ALT1G" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="PNP_TRANSISTOR" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="SOT-23">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+3V3">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+5V">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" prefix="+3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A3L-LOC_JMA">
<wire x1="288.29" y1="3.81" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="3.81" x2="373.38" y2="3.81" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="383.54" y2="3.81" width="0.1016" layer="94"/>
<wire x1="383.54" y1="3.81" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="383.54" y1="8.89" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="383.54" y1="13.97" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="383.54" y1="19.05" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="3.81" x2="288.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="24.13" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="342.265" y1="24.13" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="373.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="342.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="342.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<text x="344.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="344.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="357.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="343.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="387.35" y2="260.35" columns="8" rows="5" layer="94"/>
<text x="292.1" y="15.24" size="2.54" layer="94" ratio="15">Proprietory Information
JMA WIRELESS </text>
<text x="344.424" y="20.32" size="2.54" layer="95" ratio="15">&gt;AUTHER</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="A3L-LOC_JMA">
<gates>
<gate name="G$1" symbol="A3L-LOC_JMA" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="JMA_LBR" deviceset="TI_MSP430F5438AIPZR" device=""/>
<part name="J1" library="JMA_LBR" deviceset="TST-107-X-X-D" device=""/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="R1" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="47K"/>
<part name="C1" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C2" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="10uF"/>
<part name="C3" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="10uF"/>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="+3V1" library="supply1" deviceset="+3V3" device=""/>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="NC1" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC2" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC3" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC4" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC5" library="JMA_LBR" deviceset="NC" device=""/>
<part name="J2" library="JMA_LBR" deviceset="3PIN_2POS_JMP" device=""/>
<part name="+3V2" library="supply1" deviceset="+3V3" device=""/>
<part name="U$1" library="frames" deviceset="A3L-LOC_JMA" device=""/>
<part name="U$2" library="frames" deviceset="A3L-LOC_JMA" device=""/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="+3V3" library="supply1" deviceset="+3V3" device=""/>
<part name="C4" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="C5" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="C6" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="C7" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="C9" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="C10" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.47uF"/>
<part name="C11" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="4.7uF"/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="S1" library="JMA_LBR" deviceset="ESWITCH_TL3304AF260QL" device=""/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="GND10" library="supply1" deviceset="GND" device=""/>
<part name="GND11" library="supply1" deviceset="GND" device=""/>
<part name="J3" library="JMA_LBR" deviceset="3PIN_2POS_JMP" device="" value="DNP"/>
<part name="J4" library="JMA_LBR" deviceset="3PIN_2POS_JMP" device="" value="DNP"/>
<part name="C8" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="4.7uF"/>
<part name="C12" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="GND12" library="supply1" deviceset="GND" device=""/>
<part name="C13" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="+3V4" library="supply1" deviceset="+3V3" device=""/>
<part name="TP1" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="TP2" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="TP3" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="R2" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="330"/>
<part name="R3" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="330"/>
<part name="D1" library="JMA_LBR" deviceset="LED_SMD_" device="0402"/>
<part name="D2" library="JMA_LBR" deviceset="LED_SMD_" device="0402"/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="J5" library="JMA_LBR" deviceset="3PIN_JUMPER_" device="0603"/>
<part name="J6" library="JMA_LBR" deviceset="3PIN_JUMPER_" device="0603"/>
<part name="J7" library="JMA_LBR" deviceset="3PIN_JUMPER_" device="0603"/>
<part name="J8" library="JMA_LBR" deviceset="3PIN_JUMPER_" device="0603"/>
<part name="J9" library="JMA_LBR" deviceset="3PIN_JUMPER_" device="0603"/>
<part name="J10" library="JMA_LBR" deviceset="3PIN_JUMPER_" device="0603"/>
<part name="J11" library="JMA_LBR" deviceset="3PIN_JUMPER_" device="0603"/>
<part name="J12" library="JMA_LBR" deviceset="3PIN_JUMPER_" device="0603"/>
<part name="J13" library="JMA_LBR" deviceset="3PIN_JUMPER_" device="0603"/>
<part name="J14" library="JMA_LBR" deviceset="3PIN_JUMPER_" device="0603"/>
<part name="J15" library="JMA_LBR" deviceset="3PIN_JUMPER_" device="0603"/>
<part name="J16" library="JMA_LBR" deviceset="3PIN_JUMPER_" device="0603"/>
<part name="J17" library="JMA_LBR" deviceset="3PIN_JUMPER_" device="0603"/>
<part name="J18" library="JMA_LBR" deviceset="3PIN_JUMPER_" device="0603"/>
<part name="EXT_A1" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="EXT_A2" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="EXT_A3" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="EXT_A4" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="EXT_A5" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="EXT_A6" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="EXT_A7" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="EXT_A12" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="EXT_A11" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="EXT_A10" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="EXT_A9" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="EXT_A14" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="EXT_A13" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="EXT_A8" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="R4" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="2.7K"/>
<part name="R5" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="50.5K"/>
<part name="R6" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="50.5K"/>
<part name="R7" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="2.7K"/>
<part name="R8" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="2.7K"/>
<part name="R9" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="50.5K"/>
<part name="R10" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="50.5K"/>
<part name="R11" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="2.7K"/>
<part name="R12" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="2.7K"/>
<part name="R13" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="50.5K"/>
<part name="R14" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="50.5"/>
<part name="R15" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="2.7K"/>
<part name="R16" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="2.7K"/>
<part name="R17" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="50.5K"/>
<part name="R18" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="2.7K"/>
<part name="R19" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="50.5K"/>
<part name="R20" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="50.5K"/>
<part name="R21" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="2.7K"/>
<part name="R22" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="2.7K"/>
<part name="R23" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="50.5K"/>
<part name="R24" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="50.5K"/>
<part name="R25" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="2.7K"/>
<part name="R26" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="2.7K"/>
<part name="R27" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="50.5K"/>
<part name="R28" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="2.7K"/>
<part name="R29" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="50.5K"/>
<part name="R30" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="50.5K"/>
<part name="R31" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="2.7K"/>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="OVP_IN_1" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="OVP_IN_12" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="OVP_IN_2" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="OVP_IN_11" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="OVP_IN_3" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="OVP_IN_4" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="OVP_IN_5" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="OVP_IN_6" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="OVP_IN_7" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="OVP_IN_10" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="OVP_IN_9" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="OVP_IN_14" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="OVP_IN_13" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="OVP_IN_8" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="X1" library="JMA_LBR" deviceset="AB26TRQ-32.768KHZ" device="" value="AB26TRQ-32.768KHZ-9"/>
<part name="C14" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="18pF"/>
<part name="C15" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="18pF"/>
<part name="C16" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="GND13" library="supply1" deviceset="GND" device=""/>
<part name="GND14" library="supply1" deviceset="GND" device=""/>
<part name="C17" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="GND15" library="supply1" deviceset="GND" device=""/>
<part name="C18" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="10uF"/>
<part name="J19" library="JMA_LBR" deviceset="TSW-104-XX-X-S" device="VERTICAL"/>
<part name="J20" library="JMA_LBR" deviceset="TSW-104-XX-X-S" device="VERTICAL"/>
<part name="GND16" library="supply1" deviceset="GND" device=""/>
<part name="J21" library="JMA_LBR" deviceset="TSW-104-XX-X-S" device="VERTICAL"/>
<part name="J22" library="JMA_LBR" deviceset="TSW-104-XX-X-S" device="VERTICAL"/>
<part name="R32" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="R33" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="120"/>
<part name="R34" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="R35" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="R36" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="U2" library="JMA_LBR" deviceset="TI_DS3695AX" device="" value="SN65HVD1781"/>
<part name="C19" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C20" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="15pF"/>
<part name="C21" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="15pF"/>
<part name="GND17" library="supply1" deviceset="GND" device=""/>
<part name="GND18" library="supply1" deviceset="GND" device=""/>
<part name="GND19" library="supply1" deviceset="GND" device=""/>
<part name="R37" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R38" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="10K"/>
<part name="R39" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="470"/>
<part name="LS1" library="JMA_LBR" deviceset="MURATA_PKLCS1212E4001-R1" device="" value="PKLCS1212E4001-R1"/>
<part name="Q1" library="JMA_LBR" deviceset="BSS123" device="" value="BSS123"/>
<part name="GND20" library="supply1" deviceset="GND" device=""/>
<part name="+3V5" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V6" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V7" library="supply1" deviceset="+3V3" device=""/>
<part name="U3" library="JMA_LBR" deviceset="TI_TCA9406" device="8-VFSOP"/>
<part name="R40" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="R41" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="C22" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="1uF"/>
<part name="R42" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="R43" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="C23" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="1uF"/>
<part name="GND21" library="supply1" deviceset="GND" device=""/>
<part name="GND22" library="supply1" deviceset="GND" device=""/>
<part name="GND23" library="supply1" deviceset="GND" device=""/>
<part name="+3V8" library="supply1" deviceset="+3V3" device=""/>
<part name="R44" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="R45" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="R46" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="R47" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="R48" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="R49" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="J23" library="JMA_LBR" deviceset="3PIN_2POS_JMP" device="" value="DNP"/>
<part name="TP4" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="TP5" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="GND24" library="supply1" deviceset="GND" device=""/>
<part name="+3V9" library="supply1" deviceset="+3V3" device=""/>
<part name="U4" library="JMA_LBR" deviceset="TI_TCA9406" device="8-VFSOP"/>
<part name="R50" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="R51" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="C24" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="1uF"/>
<part name="R52" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="R53" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="C25" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="1uF"/>
<part name="GND25" library="supply1" deviceset="GND" device=""/>
<part name="GND26" library="supply1" deviceset="GND" device=""/>
<part name="GND27" library="supply1" deviceset="GND" device=""/>
<part name="+3V10" library="supply1" deviceset="+3V3" device=""/>
<part name="R54" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="R55" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="J24" library="JMA_LBR" deviceset="3PIN_2POS_JMP" device="" value="DNP"/>
<part name="TP6" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="TP7" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="GND28" library="supply1" deviceset="GND" device=""/>
<part name="+3V11" library="supply1" deviceset="+3V3" device=""/>
<part name="J25" library="JMA_LBR" deviceset="TSW-104-XX-X-S" device="VERTICAL"/>
<part name="J26" library="JMA_LBR" deviceset="PHEONIX_CONT_1935161" device="" value="PHEONIX_CONT_1935161"/>
<part name="GND29" library="supply1" deviceset="GND" device=""/>
<part name="D3" library="JMA_LBR" deviceset="COMCHIP_CDBA240LL-HF" device="" value="CDBA240LL-HF"/>
<part name="L1" library="JMA_LBR" deviceset="LBR2012T101K" device="" value="10uF"/>
<part name="R56" library="JMA_LBR" deviceset="RES_SMD_" device="0805" value="DNP"/>
<part name="R57" library="JMA_LBR" deviceset="RES_SMD_" device="0805" value="DNP"/>
<part name="U5" library="JMA_LBR" deviceset="ONSEMI_LM317" device="D2PAK-3_D2T-SUFFIX_CASE936"/>
<part name="C26" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="C27" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="R58" library="JMA_LBR" deviceset="RES_SMD_" device="0805" value="240"/>
<part name="R59" library="JMA_LBR" deviceset="RES_SMD_" device="0805" value="392"/>
<part name="R60" library="JMA_LBR" deviceset="RES_SMD_" device="0805" value="50k"/>
<part name="D4" library="JMA_LBR" deviceset="COMCHIP_CGRA400X-G" device="" value="CGRA4001-G"/>
<part name="U6" library="JMA_LBR" deviceset="SMMBT2907ALT1G" device="" value="SMMBT2907ALT1G"/>
<part name="C28" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="C29" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="L3" library="JMA_LBR" deviceset="LBR2012T101K" device="" value="10uH"/>
<part name="C30" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="C31" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="4.7uF"/>
<part name="C32" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="GND30" library="supply1" deviceset="GND" device=""/>
<part name="GND32" library="supply1" deviceset="GND" device=""/>
<part name="+3V12" library="supply1" deviceset="+3V3" device=""/>
<part name="D5" library="JMA_LBR" deviceset="LED_SMD_" device="0402"/>
<part name="D6" library="JMA_LBR" deviceset="LED_SMD_" device="0402"/>
<part name="GND31" library="supply1" deviceset="GND" device=""/>
<part name="FAULT_CTRL1" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="FAULT_CTRL2" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="FAULT_CTRL3" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="FAULT_CTRL4" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="FAULT_CTRL5" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="FAULT_CTRL6" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="FAULT_CTRL7" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="FAULT_CTRL8" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="FAULT_CTRL9" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="FAULT_CTRL10" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="FAULT_CTRL11" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="FAULT_CTRL12" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="FAULT_CTRL13" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="FAULT_CTRL14" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="U$3" library="frames" deviceset="A3L-LOC_JMA" device=""/>
<part name="C33" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="C34" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="C35" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="C36" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="C37" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="+3V13" library="supply1" deviceset="+3V3" device=""/>
<part name="GND33" library="supply1" deviceset="GND" device=""/>
<part name="GND34" library="supply1" deviceset="GND" device=""/>
<part name="GND35" library="supply1" deviceset="GND" device=""/>
<part name="GND36" library="supply1" deviceset="GND" device=""/>
<part name="GND37" library="supply1" deviceset="GND" device=""/>
<part name="D7" library="JMA_LBR" deviceset="LED_SMD_" device="0402"/>
<part name="D8" library="JMA_LBR" deviceset="LED_SMD_" device="0402"/>
<part name="D9" library="JMA_LBR" deviceset="LED_SMD_" device="0402"/>
<part name="D10" library="JMA_LBR" deviceset="LED_SMD_" device="0402"/>
<part name="D11" library="JMA_LBR" deviceset="LED_SMD_" device="0402"/>
<part name="D12" library="JMA_LBR" deviceset="LED_SMD_" device="0402"/>
<part name="D13" library="JMA_LBR" deviceset="LED_SMD_" device="0402"/>
<part name="D14" library="JMA_LBR" deviceset="LED_SMD_" device="0402"/>
<part name="D15" library="JMA_LBR" deviceset="LED_SMD_" device="0402"/>
<part name="D16" library="JMA_LBR" deviceset="LED_SMD_" device="0402"/>
<part name="D17" library="JMA_LBR" deviceset="LED_SMD_" device="0402"/>
<part name="D18" library="JMA_LBR" deviceset="LED_SMD_" device="0402"/>
<part name="D19" library="JMA_LBR" deviceset="LED_SMD_" device="0402"/>
<part name="D20" library="JMA_LBR" deviceset="LED_SMD_" device="0402"/>
<part name="R61" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="470"/>
<part name="R62" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="470"/>
<part name="R63" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="470"/>
<part name="R64" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="470"/>
<part name="R65" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="470"/>
<part name="R66" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="470"/>
<part name="R67" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="470"/>
<part name="R68" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="470"/>
<part name="R69" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="470"/>
<part name="R70" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="470"/>
<part name="R71" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="470"/>
<part name="R72" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="470"/>
<part name="R73" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="470"/>
<part name="R74" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="470"/>
<part name="GND38" library="supply1" deviceset="GND" device=""/>
<part name="TP15" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="R75" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="R76" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="+3V14" library="supply1" deviceset="+3V3" device=""/>
<part name="GND39" library="supply1" deviceset="GND" device=""/>
<part name="R77" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="480"/>
<part name="R78" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="480"/>
<part name="D21" library="JMA_LBR" deviceset="LED_SMD_" device="0402"/>
<part name="D22" library="JMA_LBR" deviceset="LED_SMD_" device="0402"/>
<part name="D23" library="JMA_LBR" deviceset="LED_SMD_" device="0402"/>
<part name="R79" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="470"/>
<part name="R80" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="470"/>
<part name="R81" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="470"/>
<part name="GND40" library="supply1" deviceset="GND" device=""/>
<part name="D24" library="JMA_LBR" deviceset="LED_SMD_" device="0402"/>
<part name="D25" library="JMA_LBR" deviceset="LED_SMD_" device="0402"/>
<part name="R82" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="470"/>
<part name="R83" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="550"/>
<part name="+3V15" library="supply1" deviceset="+3V3" device=""/>
<part name="U7" library="JMA_LBR" deviceset="TI_TCA9406" device="8-VFSOP"/>
<part name="R84" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="R85" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="C38" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="1uF"/>
<part name="R86" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="R87" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="C39" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="1uF"/>
<part name="GND41" library="supply1" deviceset="GND" device=""/>
<part name="GND42" library="supply1" deviceset="GND" device=""/>
<part name="GND43" library="supply1" deviceset="GND" device=""/>
<part name="+3V16" library="supply1" deviceset="+3V3" device=""/>
<part name="R88" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="R89" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="J27" library="JMA_LBR" deviceset="3PIN_2POS_JMP" device="" value="DNP"/>
<part name="TP8" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="TP9" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="GND44" library="supply1" deviceset="GND" device=""/>
<part name="+3V17" library="supply1" deviceset="+3V3" device=""/>
<part name="J28" library="JMA_LBR" deviceset="TSW-104-XX-X-S" device="VERTICAL"/>
<part name="C40" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="GND45" library="supply1" deviceset="GND" device=""/>
<part name="D26" library="JMA_LBR" deviceset="COMCHIP_CDBA240LL-HF" device="" value="CDBA240LL-HF"/>
<part name="L2" library="JMA_LBR" deviceset="LBR2012T101K" device="" value="10uF"/>
<part name="R90" library="JMA_LBR" deviceset="RES_SMD_" device="0805" value="DNP"/>
<part name="R91" library="JMA_LBR" deviceset="RES_SMD_" device="0805" value="DNP"/>
<part name="U8" library="JMA_LBR" deviceset="ONSEMI_LM317" device="D2PAK-3_D2T-SUFFIX_CASE936"/>
<part name="C41" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="C42" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="R92" library="JMA_LBR" deviceset="RES_SMD_" device="0805" value="240"/>
<part name="R93" library="JMA_LBR" deviceset="RES_SMD_" device="0805" value="698"/>
<part name="R94" library="JMA_LBR" deviceset="RES_SMD_" device="0805" value="50k"/>
<part name="D27" library="JMA_LBR" deviceset="COMCHIP_CGRA400X-G" device="" value="CGRA4001-G"/>
<part name="U9" library="JMA_LBR" deviceset="SMMBT2907ALT1G" device="" value="SMMBT2907ALT1G"/>
<part name="C43" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="C44" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="L4" library="JMA_LBR" deviceset="LBR2012T101K" device="" value="10uH"/>
<part name="C45" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="C46" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="4.7uF"/>
<part name="C47" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="GND46" library="supply1" deviceset="GND" device=""/>
<part name="GND47" library="supply1" deviceset="GND" device=""/>
<part name="P+1" library="supply1" deviceset="+5V" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="141.986" y="196.342" size="1.27" layer="97">Programming Pins</text>
<text x="20.32" y="185.42" size="1.27" layer="97">Programming Pins</text>
<text x="17.272" y="234.188" size="1.27" layer="97">RST SW</text>
<text x="322.58" y="220.98" size="1.27" layer="97">Vcore</text>
<text x="297.18" y="220.98" size="1.27" layer="97">DVCC(1-4)</text>
<text x="17.272" y="140.208" size="1.27" layer="97">External AVcc</text>
<text x="17.526" y="112.268" size="1.27" layer="97">External AVss</text>
<text x="12.7" y="132.08" size="1.27" layer="97">Connect TP1,3,2 (100 mil apart)
For external Analog input</text>
<text x="178.816" y="203.962" size="1.27" layer="97">Debug UART_3</text>
<text x="258.826" y="134.112" size="1.27" layer="97" rot="R90">External SPI</text>
<text x="247.142" y="89.408" size="1.27" layer="97">External UART2</text>
<text x="177.038" y="209.55" size="1.27" layer="97" rot="R90">Green</text>
<text x="184.658" y="210.82" size="1.27" layer="97" rot="R90">Red</text>
<text x="449.58" y="246.38" size="1.27" layer="97">OVP_IN_0-13: Analog voltage swing from 44V - 72V</text>
<text x="429.26" y="238.76" size="1.27" layer="97">Analog Input bus</text>
<text x="83.058" y="124.46" size="1.27" layer="97" rot="R90">Analog Input bus</text>
<text x="167.894" y="21.59" size="1.27" layer="97" rot="R90">RS485_To
_UART_In</text>
<text x="360.68" y="434.34" size="1.27" layer="97">TXD-&gt;RXD</text>
<text x="360.68" y="431.8" size="1.27" layer="97">RXD-&gt;TXD</text>
<text x="360.68" y="416.56" size="1.27" layer="97">TXD-&gt;RXD</text>
<text x="360.68" y="414.02" size="1.27" layer="97">RXD-&gt;TXD</text>
<text x="360.68" y="398.78" size="1.27" layer="97">TXD-&gt;RXD</text>
<text x="360.68" y="396.24" size="1.27" layer="97">RXD-&gt;TXD</text>
<text x="360.68" y="381" size="1.27" layer="97">I2C_SDA</text>
<text x="360.68" y="378.46" size="1.27" layer="97">I2C_SCL</text>
<text x="271.018" y="436.626" size="1.27" layer="97">TX_OUT-&gt;UART_RXD</text>
<text x="271.018" y="415.798" size="1.27" layer="97">RXD_IN &lt;-UART_TXD</text>
<text x="281.94" y="426.72" size="1.27" layer="97">INT0</text>
<text x="361.188" y="479.298" size="1.27" layer="97">Buzzer PWM</text>
<text x="93.726" y="101.346" size="1.27" layer="97" rot="R180">Buzzer PWM</text>
<text x="117.094" y="428.752" size="1.27" layer="97">External PS operating voltage</text>
<text x="252.476" y="129.54" size="1.27" layer="97">Auxillary I2C</text>
<text x="110.998" y="342.646" size="1.27" layer="97">External auxillary device operating voltage</text>
<text x="360.68" y="363.22" size="1.27" layer="97">I2C_SDA</text>
<text x="360.68" y="360.68" size="1.27" layer="97">I2C_SCL</text>
<text x="245.618" y="54.61" size="1.27" layer="97" rot="R90">Green</text>
<text x="237.998" y="55.88" size="1.27" layer="97" rot="R90">Red</text>
<wire x1="10.16" y1="248.92" x2="96.52" y2="248.92" width="0.1524" layer="97" style="shortdash"/>
<wire x1="96.52" y1="248.92" x2="99.06" y2="246.38" width="0.1524" layer="97" style="shortdash"/>
<wire x1="99.06" y1="246.38" x2="99.06" y2="182.88" width="0.1524" layer="97" style="shortdash"/>
<wire x1="99.06" y1="182.88" x2="96.52" y2="180.34" width="0.1524" layer="97" style="shortdash"/>
<wire x1="96.52" y1="180.34" x2="10.16" y2="180.34" width="0.1524" layer="97" style="shortdash"/>
<wire x1="10.16" y1="180.34" x2="7.62" y2="182.88" width="0.1524" layer="97" style="shortdash"/>
<wire x1="7.62" y1="182.88" x2="7.62" y2="246.38" width="0.1524" layer="97" style="shortdash"/>
<wire x1="7.62" y1="246.38" x2="10.16" y2="248.92" width="0.1524" layer="97" style="shortdash"/>
<text x="10.16" y="251.46" size="1.778" layer="97">PROGRAMMINNG HEADER</text>
<wire x1="289.56" y1="248.92" x2="342.9" y2="248.92" width="0.1524" layer="97" style="shortdash"/>
<wire x1="342.9" y1="248.92" x2="345.44" y2="246.38" width="0.1524" layer="97" style="shortdash"/>
<wire x1="345.44" y1="246.38" x2="345.44" y2="215.9" width="0.1524" layer="97" style="shortdash"/>
<wire x1="345.44" y1="215.9" x2="342.9" y2="213.36" width="0.1524" layer="97" style="shortdash"/>
<wire x1="342.9" y1="213.36" x2="289.56" y2="213.36" width="0.1524" layer="97" style="shortdash"/>
<wire x1="289.56" y1="213.36" x2="287.02" y2="215.9" width="0.1524" layer="97" style="shortdash"/>
<wire x1="287.02" y1="215.9" x2="287.02" y2="246.38" width="0.1524" layer="97" style="shortdash"/>
<wire x1="287.02" y1="246.38" x2="289.56" y2="248.92" width="0.1524" layer="97" style="shortdash"/>
<text x="289.56" y="251.46" size="1.778" layer="97">Decoupling Capacitor</text>
<wire x1="276.86" y1="246.38" x2="276.86" y2="10.16" width="0.1524" layer="97" style="shortdash"/>
<wire x1="276.86" y1="10.16" x2="274.32" y2="7.62" width="0.1524" layer="97" style="shortdash"/>
<wire x1="274.32" y1="7.62" x2="10.16" y2="7.62" width="0.1524" layer="97" style="shortdash"/>
<wire x1="10.16" y1="7.62" x2="7.62" y2="10.16" width="0.1524" layer="97" style="shortdash"/>
<wire x1="7.62" y1="10.16" x2="7.62" y2="172.72" width="0.1524" layer="97" style="shortdash"/>
<wire x1="7.62" y1="172.72" x2="10.16" y2="175.26" width="0.1524" layer="97" style="shortdash"/>
<wire x1="10.16" y1="175.26" x2="99.06" y2="175.26" width="0.1524" layer="97" style="shortdash"/>
<wire x1="309.88" y1="513.08" x2="370.84" y2="513.08" width="0.1524" layer="97" style="shortdash"/>
<wire x1="370.84" y1="513.08" x2="373.38" y2="510.54" width="0.1524" layer="97" style="shortdash"/>
<wire x1="373.38" y1="510.54" x2="373.38" y2="462.28" width="0.1524" layer="97" style="shortdash"/>
<wire x1="373.38" y1="462.28" x2="370.84" y2="459.74" width="0.1524" layer="97" style="shortdash"/>
<wire x1="370.84" y1="459.74" x2="309.88" y2="459.74" width="0.1524" layer="97" style="shortdash"/>
<wire x1="309.88" y1="459.74" x2="307.34" y2="462.28" width="0.1524" layer="97" style="shortdash"/>
<wire x1="307.34" y1="462.28" x2="307.34" y2="510.54" width="0.1524" layer="97" style="shortdash"/>
<wire x1="307.34" y1="510.54" x2="309.88" y2="513.08" width="0.1524" layer="97" style="shortdash"/>
<text x="309.88" y="515.62" size="1.778" layer="97" ratio="15">Fault Buzzer - PWM</text>
<wire x1="10.16" y1="510.54" x2="12.7" y2="513.08" width="0.1524" layer="97" style="shortdash"/>
<wire x1="12.7" y1="513.08" x2="152.4" y2="513.08" width="0.1524" layer="97" style="shortdash"/>
<wire x1="152.4" y1="513.08" x2="154.94" y2="510.54" width="0.1524" layer="97" style="shortdash"/>
<wire x1="154.94" y1="510.54" x2="154.94" y2="462.28" width="0.1524" layer="97" style="shortdash"/>
<wire x1="154.94" y1="462.28" x2="152.4" y2="459.74" width="0.1524" layer="97" style="shortdash"/>
<wire x1="152.4" y1="459.74" x2="12.7" y2="459.74" width="0.1524" layer="97" style="shortdash"/>
<wire x1="12.7" y1="459.74" x2="10.16" y2="462.28" width="0.1524" layer="97" style="shortdash"/>
<wire x1="10.16" y1="462.28" x2="10.16" y2="510.54" width="0.1524" layer="97" style="shortdash"/>
<text x="12.7" y="515.62" size="1.778" layer="97" ratio="15">Voltage regulator (9/5V to 3.3V)</text>
<wire x1="12.7" y1="449.58" x2="152.4" y2="449.58" width="0.1524" layer="97" style="shortdash"/>
<wire x1="152.4" y1="449.58" x2="154.94" y2="447.04" width="0.1524" layer="97" style="shortdash"/>
<wire x1="154.94" y1="447.04" x2="154.94" y2="383.54" width="0.1524" layer="97" style="shortdash"/>
<wire x1="154.94" y1="383.54" x2="152.4" y2="381" width="0.1524" layer="97" style="shortdash"/>
<wire x1="152.4" y1="381" x2="12.7" y2="381" width="0.1524" layer="97" style="shortdash"/>
<wire x1="12.7" y1="381" x2="10.16" y2="383.54" width="0.1524" layer="97" style="shortdash"/>
<wire x1="10.16" y1="383.54" x2="10.16" y2="447.04" width="0.1524" layer="97" style="shortdash"/>
<wire x1="10.16" y1="447.04" x2="12.7" y2="449.58" width="0.1524" layer="97" style="shortdash"/>
<wire x1="12.7" y1="368.3" x2="152.4" y2="368.3" width="0.1524" layer="97" style="shortdash"/>
<wire x1="152.4" y1="368.3" x2="154.94" y2="365.76" width="0.1524" layer="97" style="shortdash"/>
<wire x1="154.94" y1="365.76" x2="154.94" y2="302.26" width="0.1524" layer="97" style="shortdash"/>
<wire x1="154.94" y1="302.26" x2="152.4" y2="299.72" width="0.1524" layer="97" style="shortdash"/>
<wire x1="152.4" y1="299.72" x2="12.7" y2="299.72" width="0.1524" layer="97" style="shortdash"/>
<wire x1="12.7" y1="299.72" x2="10.16" y2="302.26" width="0.1524" layer="97" style="shortdash"/>
<wire x1="10.16" y1="302.26" x2="10.16" y2="365.76" width="0.1524" layer="97" style="shortdash"/>
<wire x1="10.16" y1="365.76" x2="12.7" y2="368.3" width="0.1524" layer="97" style="shortdash"/>
<text x="12.7" y="452.12" size="1.778" layer="97" ratio="15">I2C Accelerator for Power Supply </text>
<text x="12.7" y="370.84" size="1.778" layer="97" ratio="15">I2C Accelerator for Auxlillary device</text>
<wire x1="160.02" y1="447.04" x2="160.02" y2="383.54" width="0.1524" layer="97" style="shortdash"/>
<wire x1="160.02" y1="383.54" x2="162.56" y2="381" width="0.1524" layer="97" style="shortdash"/>
<wire x1="162.56" y1="381" x2="299.72" y2="381" width="0.1524" layer="97" style="shortdash"/>
<wire x1="299.72" y1="381" x2="302.26" y2="383.54" width="0.1524" layer="97" style="shortdash"/>
<wire x1="302.26" y1="383.54" x2="302.26" y2="447.04" width="0.1524" layer="97" style="shortdash"/>
<wire x1="302.26" y1="447.04" x2="299.72" y2="449.58" width="0.1524" layer="97" style="shortdash"/>
<wire x1="299.72" y1="449.58" x2="162.56" y2="449.58" width="0.1524" layer="97" style="shortdash"/>
<wire x1="162.56" y1="449.58" x2="160.02" y2="447.04" width="0.1524" layer="97" style="shortdash"/>
<text x="162.56" y="452.12" size="1.778" layer="97" ratio="15">RS485 to UART Converter</text>
<wire x1="403.86" y1="248.92" x2="538.48" y2="248.92" width="0.1524" layer="97" style="shortdash"/>
<wire x1="538.48" y1="248.92" x2="541.02" y2="246.38" width="0.1524" layer="97" style="shortdash"/>
<wire x1="541.02" y1="246.38" x2="541.02" y2="66.04" width="0.1524" layer="97" style="shortdash"/>
<wire x1="541.02" y1="66.04" x2="538.48" y2="63.5" width="0.1524" layer="97" style="shortdash"/>
<wire x1="538.48" y1="63.5" x2="403.86" y2="63.5" width="0.1524" layer="97" style="shortdash"/>
<wire x1="403.86" y1="63.5" x2="401.32" y2="66.04" width="0.1524" layer="97" style="shortdash"/>
<wire x1="401.32" y1="66.04" x2="401.32" y2="246.38" width="0.1524" layer="97" style="shortdash"/>
<wire x1="401.32" y1="246.38" x2="403.86" y2="248.92" width="0.1524" layer="97" style="shortdash"/>
<text x="155.194" y="19.05" size="1.27" layer="97" rot="R90">I2C to control 
Power Supply</text>
<text x="186.182" y="201.422" size="1.27" layer="97">DAC_I2C</text>
<text x="289.56" y="68.58" size="1.27" layer="97" ratio="15" rot="R270" align="bottom-right">Status LED</text>
<text x="294.64" y="48.26" size="1.27" layer="97" ratio="15" rot="R270" align="bottom-right">Power LED</text>
<text x="353.06" y="93.98" size="1.27" layer="97" ratio="15" rot="R270" align="bottom-right">Fault LED's</text>
<text x="12.7" y="482.6" size="1.27" layer="97" ratio="15" rot="R270" align="bottom-right">PHEONIX_CONT_1935161</text>
<text x="265.938" y="347.726" size="1.27" layer="97">External DAC operating voltage</text>
<wire x1="162.56" y1="368.3" x2="299.72" y2="368.3" width="0.1524" layer="97" style="shortdash"/>
<wire x1="299.72" y1="368.3" x2="302.26" y2="365.76" width="0.1524" layer="97" style="shortdash"/>
<wire x1="302.26" y1="365.76" x2="302.26" y2="302.26" width="0.1524" layer="97" style="shortdash"/>
<wire x1="302.26" y1="302.26" x2="299.72" y2="299.72" width="0.1524" layer="97" style="shortdash"/>
<wire x1="299.72" y1="299.72" x2="162.56" y2="299.72" width="0.1524" layer="97" style="shortdash"/>
<wire x1="162.56" y1="299.72" x2="160.02" y2="302.26" width="0.1524" layer="97" style="shortdash"/>
<wire x1="160.02" y1="302.26" x2="160.02" y2="365.76" width="0.1524" layer="97" style="shortdash"/>
<wire x1="160.02" y1="365.76" x2="162.56" y2="368.3" width="0.1524" layer="97" style="shortdash"/>
<text x="162.56" y="370.84" size="1.778" layer="97" ratio="15">I2C Accelerator for DAC</text>
<text x="216.662" y="305.562" size="1.27" layer="97">DAC_I2C</text>
<text x="59.69" y="385.826" size="1.27" layer="97">I2C to control Power Supply</text>
<text x="353.06" y="81.28" size="1.27" layer="97" ratio="15">STATUS: GOOD</text>
<text x="353.06" y="73.66" size="1.27" layer="97" ratio="15">STATUS: REGULATING</text>
<text x="353.06" y="66.04" size="1.27" layer="97" ratio="15">STATUS: FAULT</text>
<text x="353.06" y="53.34" size="1.27" layer="97" ratio="15">3.3V LED</text>
<text x="353.06" y="45.72" size="1.27" layer="97" ratio="15">PWR IN LED</text>
<wire x1="289.56" y1="205.74" x2="373.38" y2="205.74" width="0.1524" layer="97" style="shortdash"/>
<wire x1="373.38" y1="205.74" x2="375.92" y2="203.2" width="0.1524" layer="97" style="shortdash"/>
<wire x1="375.92" y1="203.2" x2="375.92" y2="33.02" width="0.1524" layer="97" style="shortdash"/>
<wire x1="375.92" y1="33.02" x2="373.38" y2="30.48" width="0.1524" layer="97" style="shortdash"/>
<wire x1="373.38" y1="30.48" x2="289.56" y2="30.48" width="0.1524" layer="97" style="shortdash"/>
<wire x1="289.56" y1="30.48" x2="287.02" y2="33.02" width="0.1524" layer="97" style="shortdash"/>
<wire x1="287.02" y1="33.02" x2="287.02" y2="203.2" width="0.1524" layer="97" style="shortdash"/>
<wire x1="287.02" y1="203.2" x2="289.56" y2="205.74" width="0.1524" layer="97" style="shortdash"/>
<text x="106.68" y="251.46" size="1.778" layer="97" ratio="15">MSP430 Micro-processor</text>
<text x="289.56" y="208.28" size="1.778" layer="97" ratio="15">FAULT, STATUS AND POWER LED</text>
<wire x1="307.34" y1="447.04" x2="309.88" y2="449.58" width="0.1524" layer="97" style="shortdash"/>
<wire x1="309.88" y1="449.58" x2="375.92" y2="449.58" width="0.1524" layer="97" style="shortdash"/>
<wire x1="375.92" y1="449.58" x2="378.46" y2="447.04" width="0.1524" layer="97" style="shortdash"/>
<wire x1="378.46" y1="447.04" x2="378.46" y2="332.74" width="0.1524" layer="97" style="shortdash"/>
<wire x1="378.46" y1="332.74" x2="375.92" y2="330.2" width="0.1524" layer="97" style="shortdash"/>
<wire x1="375.92" y1="330.2" x2="309.88" y2="330.2" width="0.1524" layer="97" style="shortdash"/>
<wire x1="309.88" y1="330.2" x2="307.34" y2="332.74" width="0.1524" layer="97" style="shortdash"/>
<wire x1="307.34" y1="332.74" x2="307.34" y2="447.04" width="0.1524" layer="97" style="shortdash"/>
<text x="309.88" y="452.12" size="1.778" layer="97" ratio="15">HEADER</text>
<text x="403.86" y="251.46" size="1.778" layer="97" ratio="15">Analog Input</text>
<wire x1="99.06" y1="175.26" x2="104.14" y2="180.34" width="0.1524" layer="97" style="shortdash"/>
<wire x1="104.14" y1="180.34" x2="104.14" y2="246.38" width="0.1524" layer="97" style="shortdash"/>
<wire x1="104.14" y1="246.38" x2="106.68" y2="248.92" width="0.1524" layer="97" style="shortdash"/>
<wire x1="106.68" y1="248.92" x2="274.32" y2="248.92" width="0.1524" layer="97" style="shortdash"/>
<wire x1="274.32" y1="248.92" x2="276.86" y2="246.38" width="0.1524" layer="97" style="shortdash"/>
<wire x1="160.02" y1="510.54" x2="162.56" y2="513.08" width="0.1524" layer="97" style="shortdash"/>
<wire x1="162.56" y1="513.08" x2="299.72" y2="513.08" width="0.1524" layer="97" style="shortdash"/>
<wire x1="299.72" y1="513.08" x2="302.26" y2="510.54" width="0.1524" layer="97" style="shortdash"/>
<wire x1="302.26" y1="510.54" x2="302.26" y2="462.28" width="0.1524" layer="97" style="shortdash"/>
<wire x1="302.26" y1="462.28" x2="299.72" y2="459.74" width="0.1524" layer="97" style="shortdash"/>
<wire x1="299.72" y1="459.74" x2="162.56" y2="459.74" width="0.1524" layer="97" style="shortdash"/>
<wire x1="162.56" y1="459.74" x2="160.02" y2="462.28" width="0.1524" layer="97" style="shortdash"/>
<wire x1="160.02" y1="462.28" x2="160.02" y2="510.54" width="0.1524" layer="97" style="shortdash"/>
</plain>
<instances>
<instance part="U1" gate="G$1" x="101.6" y="175.26" smashed="yes"/>
<instance part="J1" gate="-1" x="53.34" y="200.66" smashed="yes">
<attribute name="NAME" x="50.038" y="201.168" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-2" x="66.04" y="200.66" smashed="yes" rot="R180">
<attribute name="NAME" x="69.342" y="200.152" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J1" gate="-3" x="53.34" y="203.2" smashed="yes">
<attribute name="NAME" x="50.038" y="203.708" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-4" x="66.04" y="203.2" smashed="yes" rot="R180">
<attribute name="NAME" x="69.342" y="202.692" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J1" gate="-5" x="53.34" y="205.74" smashed="yes">
<attribute name="NAME" x="50.038" y="206.248" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-6" x="66.04" y="205.74" smashed="yes" rot="R180">
<attribute name="NAME" x="69.342" y="205.232" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J1" gate="-7" x="53.34" y="208.28" smashed="yes">
<attribute name="NAME" x="50.038" y="208.788" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-8" x="66.04" y="208.28" smashed="yes" rot="R180">
<attribute name="NAME" x="69.342" y="207.772" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J1" gate="-9" x="53.34" y="210.82" smashed="yes">
<attribute name="NAME" x="50.038" y="211.328" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-10" x="66.04" y="210.82" smashed="yes" rot="R180">
<attribute name="NAME" x="69.342" y="210.312" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J1" gate="-11" x="53.34" y="213.36" smashed="yes">
<attribute name="NAME" x="50.038" y="213.868" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-12" x="66.04" y="213.36" smashed="yes" rot="R180">
<attribute name="NAME" x="69.342" y="212.852" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J1" gate="-13" x="53.34" y="215.9" smashed="yes">
<attribute name="NAME" x="50.038" y="216.408" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-14" x="66.04" y="215.9" smashed="yes" rot="R180">
<attribute name="NAME" x="69.342" y="215.392" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="GND1" gate="1" x="43.18" y="195.58" smashed="yes">
<attribute name="VALUE" x="40.64" y="193.04" size="1.27" layer="96"/>
</instance>
<instance part="R1" gate="G$1" x="20.32" y="238.76" smashed="yes" rot="R180">
<attribute name="NAME" x="14.986" y="239.2426" size="1.27" layer="95"/>
<attribute name="VALUE" x="23.114" y="239.268" size="1.27" layer="96"/>
</instance>
<instance part="C1" gate="G$1" x="35.56" y="238.76" smashed="yes" rot="R180">
<attribute name="NAME" x="30.48" y="239.268" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="36.83" y="239.268" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C2" gate="G$1" x="83.82" y="198.12" smashed="yes">
<attribute name="NAME" x="88.9" y="197.612" size="1.27" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="82.55" y="197.612" size="1.27" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="C3" gate="G$1" x="83.82" y="193.04" smashed="yes">
<attribute name="NAME" x="88.9" y="192.532" size="1.27" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="82.55" y="192.532" size="1.27" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="GND3" gate="1" x="93.98" y="187.96" smashed="yes">
<attribute name="VALUE" x="91.44" y="185.42" size="1.27" layer="96"/>
</instance>
<instance part="+3V1" gate="G$1" x="12.7" y="243.84" smashed="yes">
<attribute name="VALUE" x="10.414" y="244.094" size="1.27" layer="96"/>
</instance>
<instance part="GND4" gate="1" x="12.7" y="215.9" smashed="yes">
<attribute name="VALUE" x="10.16" y="213.36" size="1.27" layer="96"/>
</instance>
<instance part="NC1" gate="G$1" x="73.66" y="205.74" smashed="yes"/>
<instance part="NC2" gate="G$1" x="73.66" y="213.36" smashed="yes"/>
<instance part="NC3" gate="G$1" x="73.66" y="210.82" smashed="yes"/>
<instance part="NC4" gate="G$1" x="45.72" y="215.9" smashed="yes"/>
<instance part="NC5" gate="G$1" x="73.66" y="215.9" smashed="yes"/>
<instance part="J2" gate="G$1" x="96.52" y="223.52" smashed="yes" rot="R270"/>
<instance part="+3V2" gate="G$1" x="86.36" y="243.84" smashed="yes">
<attribute name="VALUE" x="84.074" y="244.094" size="1.27" layer="96"/>
</instance>
<instance part="U$1" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="344.17" y="15.24" size="1.27" layer="94"/>
<attribute name="LAST_DATE_TIME" x="344.17" y="10.16" size="1.27" layer="94"/>
<attribute name="SHEET" x="357.505" y="5.08" size="1.27" layer="94"/>
</instance>
<instance part="U$2" gate="G$1" x="393.7" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="737.87" y="15.24" size="1.27" layer="94"/>
<attribute name="LAST_DATE_TIME" x="737.87" y="10.16" size="1.27" layer="94"/>
<attribute name="SHEET" x="751.205" y="5.08" size="1.27" layer="94"/>
</instance>
<instance part="GND7" gate="1" x="162.56" y="187.96" smashed="yes" rot="R180">
<attribute name="VALUE" x="165.1" y="190.5" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="+3V3" gate="G$1" x="297.18" y="243.84" smashed="yes">
<attribute name="VALUE" x="294.64" y="238.76" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C4" gate="G$1" x="297.18" y="231.14" smashed="yes" rot="R270">
<attribute name="NAME" x="296.672" y="226.06" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="296.672" y="232.41" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C5" gate="G$1" x="302.26" y="231.14" smashed="yes" rot="R270">
<attribute name="NAME" x="301.752" y="226.06" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="301.752" y="232.41" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C6" gate="G$1" x="307.34" y="231.14" smashed="yes" rot="R270">
<attribute name="NAME" x="306.832" y="226.06" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="306.832" y="232.41" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C7" gate="G$1" x="312.42" y="231.14" smashed="yes" rot="R270">
<attribute name="NAME" x="311.912" y="226.06" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="311.912" y="232.41" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C9" gate="G$1" x="322.58" y="231.14" smashed="yes" rot="R270">
<attribute name="NAME" x="322.072" y="226.06" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="322.072" y="232.41" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C10" gate="G$1" x="327.66" y="231.14" smashed="yes" rot="R270">
<attribute name="NAME" x="327.152" y="226.06" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="327.152" y="232.41" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C11" gate="G$1" x="332.74" y="231.14" smashed="yes" rot="R270">
<attribute name="NAME" x="332.232" y="226.06" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="332.232" y="232.41" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND8" gate="1" x="332.74" y="218.44" smashed="yes">
<attribute name="VALUE" x="330.2" y="215.9" size="1.27" layer="96"/>
</instance>
<instance part="S1" gate="G$1" x="22.86" y="226.06" smashed="yes" rot="R90">
<attribute name="NAME" x="19.304" y="219.1766" size="1.27" layer="95"/>
<attribute name="VALUE" x="21.082" y="234.95" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="GND9" gate="1" x="236.22" y="114.3" smashed="yes" rot="R90">
<attribute name="VALUE" x="238.76" y="111.76" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="GND10" gate="1" x="160.02" y="40.64" smashed="yes">
<attribute name="VALUE" x="158.242" y="38.862" size="1.27" layer="96"/>
</instance>
<instance part="GND11" gate="1" x="88.9" y="109.22" smashed="yes" rot="R270">
<attribute name="VALUE" x="84.582" y="108.966" size="1.27" layer="96"/>
</instance>
<instance part="J3" gate="G$1" x="12.7" y="144.78" smashed="yes" rot="MR270"/>
<instance part="J4" gate="G$1" x="12.7" y="116.84" smashed="yes" rot="MR270"/>
<instance part="C8" gate="G$1" x="58.42" y="137.16" smashed="yes" rot="R270">
<attribute name="NAME" x="57.912" y="132.08" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="57.912" y="138.43" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C12" gate="G$1" x="53.34" y="137.16" smashed="yes" rot="R90">
<attribute name="NAME" x="52.832" y="132.08" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="52.832" y="138.43" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND12" gate="1" x="63.5" y="124.46" smashed="yes">
<attribute name="VALUE" x="60.96" y="121.92" size="1.27" layer="96"/>
</instance>
<instance part="C13" gate="G$1" x="63.5" y="137.16" smashed="yes" rot="R90">
<attribute name="NAME" x="62.992" y="132.08" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="62.992" y="138.43" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="+3V4" gate="G$1" x="50.8" y="154.94" smashed="yes">
<attribute name="VALUE" x="48.514" y="155.194" size="1.27" layer="96"/>
</instance>
<instance part="TP1" gate="G$1" x="27.94" y="142.24" smashed="yes" rot="R270">
<attribute name="NAME" x="30.988" y="142.24" size="1.27" layer="95"/>
</instance>
<instance part="TP2" gate="G$1" x="27.94" y="114.3" smashed="yes" rot="R270">
<attribute name="NAME" x="30.988" y="114.3" size="1.27" layer="95"/>
</instance>
<instance part="TP3" gate="G$1" x="50.8" y="127" smashed="yes" rot="R90">
<attribute name="NAME" x="47.752" y="127" size="1.27" layer="95" rot="R180"/>
</instance>
<instance part="R2" gate="G$1" x="175.26" y="200.66" smashed="yes" rot="R90">
<attribute name="NAME" x="174.7774" y="195.072" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="174.752" y="203.708" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R3" gate="G$1" x="177.8" y="200.66" smashed="yes" rot="R90">
<attribute name="NAME" x="177.3174" y="195.072" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="177.292" y="203.708" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="D1" gate="G$1" x="175.26" y="215.9" smashed="yes" rot="R90">
<attribute name="NAME" x="174.752" y="210.82" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="180.34" y="210.82" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="D2" gate="G$1" x="182.88" y="215.9" smashed="yes" rot="R90">
<attribute name="NAME" x="182.372" y="210.82" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="187.96" y="210.82" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND5" gate="1" x="187.96" y="220.98" smashed="yes">
<attribute name="VALUE" x="185.42" y="218.44" size="1.27" layer="96"/>
</instance>
<instance part="J5" gate="G$1" x="530.86" y="218.44" smashed="yes" rot="R270">
<attribute name="NAME" x="533.4" y="209.55" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="530.86" y="209.55" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="J6" gate="G$1" x="411.48" y="218.44" smashed="yes" rot="R90">
<attribute name="NAME" x="408.94" y="227.33" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="411.48" y="227.33" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="J7" gate="G$1" x="530.86" y="195.58" smashed="yes" rot="R270">
<attribute name="NAME" x="533.4" y="186.69" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="530.86" y="186.69" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="J8" gate="G$1" x="411.48" y="195.58" smashed="yes" rot="R90">
<attribute name="NAME" x="408.94" y="204.47" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="411.48" y="204.47" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="J9" gate="G$1" x="530.86" y="172.72" smashed="yes" rot="R270">
<attribute name="NAME" x="533.4" y="163.83" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="530.86" y="163.83" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="J10" gate="G$1" x="411.48" y="172.72" smashed="yes" rot="R90">
<attribute name="NAME" x="408.94" y="181.61" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="411.48" y="181.61" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="J11" gate="G$1" x="530.86" y="152.4" smashed="yes" rot="R270">
<attribute name="NAME" x="533.4" y="143.51" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="530.86" y="143.51" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="J12" gate="G$1" x="411.48" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="408.94" y="158.75" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="411.48" y="158.75" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="J13" gate="G$1" x="530.86" y="129.54" smashed="yes" rot="R270">
<attribute name="NAME" x="533.4" y="120.65" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="530.86" y="120.65" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="J14" gate="G$1" x="411.48" y="127" smashed="yes" rot="R90">
<attribute name="NAME" x="408.94" y="135.89" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="411.48" y="135.89" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="J15" gate="G$1" x="530.86" y="106.68" smashed="yes" rot="R270">
<attribute name="NAME" x="533.4" y="97.79" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="530.86" y="97.79" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="J16" gate="G$1" x="411.48" y="106.68" smashed="yes" rot="R90">
<attribute name="NAME" x="408.94" y="115.57" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="411.48" y="115.57" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="J17" gate="G$1" x="530.86" y="83.82" smashed="yes" rot="R270">
<attribute name="NAME" x="533.4" y="74.93" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="530.86" y="74.93" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="J18" gate="G$1" x="411.48" y="83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="408.94" y="92.71" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="411.48" y="92.71" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="EXT_A1" gate="G$1" x="421.64" y="223.52" smashed="yes" rot="R270">
<attribute name="NAME" x="417.83" y="224.79" size="1.27" layer="95"/>
</instance>
<instance part="EXT_A2" gate="G$1" x="421.64" y="200.66" smashed="yes" rot="R270">
<attribute name="NAME" x="417.83" y="201.93" size="1.27" layer="95"/>
</instance>
<instance part="EXT_A3" gate="G$1" x="421.64" y="177.8" smashed="yes" rot="R270">
<attribute name="NAME" x="417.83" y="179.07" size="1.27" layer="95"/>
</instance>
<instance part="EXT_A4" gate="G$1" x="421.64" y="154.94" smashed="yes" rot="R270">
<attribute name="NAME" x="417.83" y="156.21" size="1.27" layer="95"/>
</instance>
<instance part="EXT_A5" gate="G$1" x="421.64" y="132.08" smashed="yes" rot="R270">
<attribute name="NAME" x="417.83" y="133.35" size="1.27" layer="95"/>
</instance>
<instance part="EXT_A6" gate="G$1" x="421.64" y="111.76" smashed="yes" rot="R270">
<attribute name="NAME" x="417.83" y="113.03" size="1.27" layer="95"/>
</instance>
<instance part="EXT_A7" gate="G$1" x="421.64" y="88.9" smashed="yes" rot="R270">
<attribute name="NAME" x="417.83" y="90.17" size="1.27" layer="95"/>
</instance>
<instance part="EXT_A12" gate="G$1" x="520.7" y="213.36" smashed="yes" rot="R90">
<attribute name="NAME" x="514.35" y="209.55" size="1.27" layer="95"/>
</instance>
<instance part="EXT_A11" gate="G$1" x="520.7" y="190.5" smashed="yes" rot="R90">
<attribute name="NAME" x="514.35" y="186.69" size="1.27" layer="95"/>
</instance>
<instance part="EXT_A10" gate="G$1" x="520.7" y="167.64" smashed="yes" rot="R90">
<attribute name="NAME" x="514.35" y="163.83" size="1.27" layer="95"/>
</instance>
<instance part="EXT_A9" gate="G$1" x="520.7" y="147.32" smashed="yes" rot="R90">
<attribute name="NAME" x="514.35" y="143.51" size="1.27" layer="95"/>
</instance>
<instance part="EXT_A14" gate="G$1" x="520.7" y="124.46" smashed="yes" rot="R90">
<attribute name="NAME" x="514.35" y="120.65" size="1.27" layer="95"/>
</instance>
<instance part="EXT_A13" gate="G$1" x="520.7" y="101.6" smashed="yes" rot="R90">
<attribute name="NAME" x="514.35" y="97.79" size="1.27" layer="95"/>
</instance>
<instance part="EXT_A8" gate="G$1" x="520.7" y="78.74" smashed="yes" rot="R90">
<attribute name="NAME" x="514.35" y="74.93" size="1.27" layer="95"/>
</instance>
<instance part="R4" gate="G$1" x="454.66" y="213.36" smashed="yes">
<attribute name="NAME" x="453.39" y="214.8586" size="1.27" layer="95"/>
<attribute name="VALUE" x="453.39" y="210.058" size="1.27" layer="96"/>
</instance>
<instance part="R5" gate="G$1" x="454.66" y="218.44" smashed="yes">
<attribute name="NAME" x="453.39" y="219.9386" size="1.27" layer="95"/>
<attribute name="VALUE" x="453.39" y="215.138" size="1.27" layer="96"/>
</instance>
<instance part="R6" gate="G$1" x="487.68" y="223.52" smashed="yes">
<attribute name="NAME" x="486.41" y="225.0186" size="1.27" layer="95"/>
<attribute name="VALUE" x="486.41" y="220.218" size="1.27" layer="96"/>
</instance>
<instance part="R7" gate="G$1" x="487.68" y="218.44" smashed="yes">
<attribute name="NAME" x="486.41" y="219.9386" size="1.27" layer="95"/>
<attribute name="VALUE" x="486.41" y="215.138" size="1.27" layer="96"/>
</instance>
<instance part="R8" gate="G$1" x="454.66" y="190.5" smashed="yes">
<attribute name="NAME" x="453.39" y="191.9986" size="1.27" layer="95"/>
<attribute name="VALUE" x="453.39" y="187.198" size="1.27" layer="96"/>
</instance>
<instance part="R9" gate="G$1" x="454.66" y="195.58" smashed="yes">
<attribute name="NAME" x="453.39" y="197.0786" size="1.27" layer="95"/>
<attribute name="VALUE" x="453.39" y="192.278" size="1.27" layer="96"/>
</instance>
<instance part="R10" gate="G$1" x="487.68" y="200.66" smashed="yes">
<attribute name="NAME" x="486.41" y="202.1586" size="1.27" layer="95"/>
<attribute name="VALUE" x="486.41" y="197.358" size="1.27" layer="96"/>
</instance>
<instance part="R11" gate="G$1" x="487.68" y="195.58" smashed="yes">
<attribute name="NAME" x="486.41" y="197.0786" size="1.27" layer="95"/>
<attribute name="VALUE" x="486.41" y="192.278" size="1.27" layer="96"/>
</instance>
<instance part="R12" gate="G$1" x="454.66" y="167.64" smashed="yes">
<attribute name="NAME" x="453.39" y="169.1386" size="1.27" layer="95"/>
<attribute name="VALUE" x="453.39" y="164.338" size="1.27" layer="96"/>
</instance>
<instance part="R13" gate="G$1" x="454.66" y="172.72" smashed="yes">
<attribute name="NAME" x="453.39" y="174.2186" size="1.27" layer="95"/>
<attribute name="VALUE" x="453.39" y="169.418" size="1.27" layer="96"/>
</instance>
<instance part="R14" gate="G$1" x="487.68" y="177.8" smashed="yes">
<attribute name="NAME" x="486.41" y="179.2986" size="1.27" layer="95"/>
<attribute name="VALUE" x="486.41" y="174.498" size="1.27" layer="96"/>
</instance>
<instance part="R15" gate="G$1" x="487.68" y="172.72" smashed="yes">
<attribute name="NAME" x="486.41" y="174.2186" size="1.27" layer="95"/>
<attribute name="VALUE" x="486.41" y="169.418" size="1.27" layer="96"/>
</instance>
<instance part="R16" gate="G$1" x="454.66" y="144.78" smashed="yes">
<attribute name="NAME" x="453.39" y="146.2786" size="1.27" layer="95"/>
<attribute name="VALUE" x="453.39" y="141.478" size="1.27" layer="96"/>
</instance>
<instance part="R17" gate="G$1" x="454.66" y="149.86" smashed="yes">
<attribute name="NAME" x="453.39" y="151.3586" size="1.27" layer="95"/>
<attribute name="VALUE" x="453.39" y="146.558" size="1.27" layer="96"/>
</instance>
<instance part="R18" gate="G$1" x="487.68" y="152.4" smashed="yes">
<attribute name="NAME" x="486.41" y="153.8986" size="1.27" layer="95"/>
<attribute name="VALUE" x="486.41" y="149.098" size="1.27" layer="96"/>
</instance>
<instance part="R19" gate="G$1" x="487.68" y="157.48" smashed="yes">
<attribute name="NAME" x="486.41" y="158.9786" size="1.27" layer="95"/>
<attribute name="VALUE" x="486.41" y="154.178" size="1.27" layer="96"/>
</instance>
<instance part="R20" gate="G$1" x="454.66" y="127" smashed="yes">
<attribute name="NAME" x="453.39" y="128.4986" size="1.27" layer="95"/>
<attribute name="VALUE" x="453.39" y="123.698" size="1.27" layer="96"/>
</instance>
<instance part="R21" gate="G$1" x="454.66" y="121.92" smashed="yes">
<attribute name="NAME" x="453.39" y="123.4186" size="1.27" layer="95"/>
<attribute name="VALUE" x="453.39" y="118.618" size="1.27" layer="96"/>
</instance>
<instance part="R22" gate="G$1" x="487.68" y="129.54" smashed="yes">
<attribute name="NAME" x="486.41" y="131.0386" size="1.27" layer="95"/>
<attribute name="VALUE" x="486.41" y="126.238" size="1.27" layer="96"/>
</instance>
<instance part="R23" gate="G$1" x="487.68" y="134.62" smashed="yes">
<attribute name="NAME" x="486.41" y="136.1186" size="1.27" layer="95"/>
<attribute name="VALUE" x="486.41" y="131.318" size="1.27" layer="96"/>
</instance>
<instance part="R24" gate="G$1" x="454.66" y="106.68" smashed="yes">
<attribute name="NAME" x="453.39" y="108.1786" size="1.27" layer="95"/>
<attribute name="VALUE" x="453.39" y="103.378" size="1.27" layer="96"/>
</instance>
<instance part="R25" gate="G$1" x="454.66" y="101.6" smashed="yes">
<attribute name="NAME" x="453.39" y="103.0986" size="1.27" layer="95"/>
<attribute name="VALUE" x="453.39" y="98.298" size="1.27" layer="96"/>
</instance>
<instance part="R26" gate="G$1" x="487.68" y="106.68" smashed="yes">
<attribute name="NAME" x="486.41" y="108.1786" size="1.27" layer="95"/>
<attribute name="VALUE" x="486.41" y="103.378" size="1.27" layer="96"/>
</instance>
<instance part="R27" gate="G$1" x="487.68" y="111.76" smashed="yes">
<attribute name="NAME" x="486.41" y="113.2586" size="1.27" layer="95"/>
<attribute name="VALUE" x="486.41" y="108.458" size="1.27" layer="96"/>
</instance>
<instance part="R28" gate="G$1" x="454.66" y="78.74" smashed="yes">
<attribute name="NAME" x="453.39" y="80.2386" size="1.27" layer="95"/>
<attribute name="VALUE" x="453.39" y="75.438" size="1.27" layer="96"/>
</instance>
<instance part="R29" gate="G$1" x="454.66" y="83.82" smashed="yes">
<attribute name="NAME" x="453.39" y="85.3186" size="1.27" layer="95"/>
<attribute name="VALUE" x="453.39" y="80.518" size="1.27" layer="96"/>
</instance>
<instance part="R30" gate="G$1" x="487.68" y="88.9" smashed="yes">
<attribute name="NAME" x="486.41" y="90.3986" size="1.27" layer="95"/>
<attribute name="VALUE" x="486.41" y="85.598" size="1.27" layer="96"/>
</instance>
<instance part="R31" gate="G$1" x="487.68" y="83.82" smashed="yes">
<attribute name="NAME" x="486.41" y="85.3186" size="1.27" layer="95"/>
<attribute name="VALUE" x="486.41" y="80.518" size="1.27" layer="96"/>
</instance>
<instance part="GND6" gate="1" x="472.44" y="68.58" smashed="yes">
<attribute name="VALUE" x="469.9" y="66.04" size="1.27" layer="96"/>
</instance>
<instance part="OVP_IN_1" gate="G$1" x="462.28" y="218.44" smashed="yes" rot="R270">
<attribute name="NAME" x="468.63" y="217.17" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="OVP_IN_12" gate="G$1" x="480.06" y="223.52" smashed="yes" rot="R90">
<attribute name="NAME" x="476.25" y="222.25" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="OVP_IN_2" gate="G$1" x="462.28" y="195.58" smashed="yes" rot="R270">
<attribute name="NAME" x="468.63" y="194.31" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="OVP_IN_11" gate="G$1" x="480.06" y="200.66" smashed="yes" rot="R90">
<attribute name="NAME" x="476.25" y="199.39" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="OVP_IN_3" gate="G$1" x="462.28" y="172.72" smashed="yes" rot="R270">
<attribute name="NAME" x="468.63" y="171.45" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="OVP_IN_4" gate="G$1" x="462.28" y="149.86" smashed="yes" rot="R270">
<attribute name="NAME" x="468.63" y="148.59" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="OVP_IN_5" gate="G$1" x="462.28" y="127" smashed="yes" rot="R270">
<attribute name="NAME" x="468.63" y="125.73" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="OVP_IN_6" gate="G$1" x="462.28" y="106.68" smashed="yes" rot="R270">
<attribute name="NAME" x="468.63" y="105.41" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="OVP_IN_7" gate="G$1" x="462.28" y="83.82" smashed="yes" rot="R270">
<attribute name="NAME" x="468.63" y="82.55" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="OVP_IN_10" gate="G$1" x="480.06" y="177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="476.25" y="176.53" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="OVP_IN_9" gate="G$1" x="480.06" y="157.48" smashed="yes" rot="R90">
<attribute name="NAME" x="476.25" y="156.21" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="OVP_IN_14" gate="G$1" x="480.06" y="134.62" smashed="yes" rot="R90">
<attribute name="NAME" x="476.25" y="133.35" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="OVP_IN_13" gate="G$1" x="480.06" y="111.76" smashed="yes" rot="R90">
<attribute name="NAME" x="476.25" y="110.49" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="OVP_IN_8" gate="G$1" x="480.06" y="88.9" smashed="yes" rot="R90">
<attribute name="NAME" x="476.25" y="87.63" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="X1" gate="G$1" x="48.26" y="104.14" smashed="yes">
<attribute name="NAME" x="46.482" y="107.188" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="41.402" y="112.268" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C14" gate="G$1" x="40.64" y="96.52" smashed="yes" rot="R90">
<attribute name="NAME" x="40.132" y="91.44" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="40.132" y="97.79" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C15" gate="G$1" x="55.88" y="96.52" smashed="yes" rot="R90">
<attribute name="NAME" x="55.372" y="91.44" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="55.372" y="97.79" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C16" gate="G$1" x="78.74" y="236.22" smashed="yes" rot="R180">
<attribute name="NAME" x="73.66" y="236.728" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="80.01" y="236.728" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND13" gate="1" x="71.12" y="231.14" smashed="yes">
<attribute name="VALUE" x="68.58" y="228.6" size="1.27" layer="96"/>
</instance>
<instance part="GND14" gate="1" x="40.64" y="83.82" smashed="yes">
<attribute name="VALUE" x="38.1" y="81.28" size="1.27" layer="96"/>
</instance>
<instance part="C17" gate="G$1" x="261.62" y="116.84" smashed="yes" rot="R180">
<attribute name="NAME" x="256.54" y="117.348" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="262.89" y="117.348" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND15" gate="1" x="269.24" y="106.68" smashed="yes">
<attribute name="VALUE" x="266.7" y="104.14" size="1.27" layer="96"/>
</instance>
<instance part="C18" gate="G$1" x="261.62" y="111.76" smashed="yes">
<attribute name="NAME" x="256.54" y="112.268" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="262.89" y="112.268" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="J19" gate="-1" x="355.6" y="436.88" smashed="yes">
<attribute name="NAME" x="352.298" y="437.388" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J19" gate="-2" x="355.6" y="434.34" smashed="yes">
<attribute name="NAME" x="352.298" y="434.848" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J19" gate="-3" x="355.6" y="431.8" smashed="yes">
<attribute name="NAME" x="352.298" y="432.308" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J19" gate="-4" x="355.6" y="429.26" smashed="yes">
<attribute name="NAME" x="352.298" y="429.768" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J19" gate="G$1" x="363.22" y="426.72" smashed="yes" rot="R90">
<attribute name="VALUE" x="363.22" y="426.72" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="J20" gate="-1" x="355.6" y="419.1" smashed="yes">
<attribute name="NAME" x="352.298" y="419.608" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J20" gate="-2" x="355.6" y="416.56" smashed="yes">
<attribute name="NAME" x="352.298" y="417.068" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J20" gate="-3" x="355.6" y="414.02" smashed="yes">
<attribute name="NAME" x="352.298" y="414.528" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J20" gate="-4" x="355.6" y="411.48" smashed="yes">
<attribute name="NAME" x="352.298" y="411.988" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J20" gate="G$1" x="363.22" y="411.48" smashed="yes" rot="R90">
<attribute name="VALUE" x="363.22" y="411.48" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND16" gate="1" x="347.98" y="335.28" smashed="yes">
<attribute name="VALUE" x="345.44" y="332.74" size="1.27" layer="96"/>
</instance>
<instance part="J21" gate="-1" x="355.6" y="401.32" smashed="yes">
<attribute name="NAME" x="352.298" y="401.828" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J21" gate="-2" x="355.6" y="398.78" smashed="yes">
<attribute name="NAME" x="352.298" y="399.288" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J21" gate="-3" x="355.6" y="396.24" smashed="yes">
<attribute name="NAME" x="352.298" y="396.748" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J21" gate="-4" x="355.6" y="393.7" smashed="yes">
<attribute name="NAME" x="352.298" y="394.208" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J21" gate="G$1" x="363.22" y="393.7" smashed="yes" rot="R90">
<attribute name="VALUE" x="363.22" y="393.7" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="J22" gate="-1" x="355.6" y="383.54" smashed="yes">
<attribute name="NAME" x="352.298" y="384.048" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J22" gate="-2" x="355.6" y="381" smashed="yes">
<attribute name="NAME" x="352.298" y="381.508" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J22" gate="-3" x="355.6" y="378.46" smashed="yes">
<attribute name="NAME" x="352.298" y="378.968" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J22" gate="-4" x="355.6" y="375.92" smashed="yes">
<attribute name="NAME" x="352.298" y="376.428" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J22" gate="G$1" x="363.22" y="375.92" smashed="yes" rot="R90">
<attribute name="VALUE" x="363.22" y="375.92" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R32" gate="G$1" x="203.2" y="429.26" smashed="yes">
<attribute name="NAME" x="196.85" y="429.7426" size="1.27" layer="95"/>
<attribute name="VALUE" x="206.248" y="429.768" size="1.27" layer="96"/>
</instance>
<instance part="R33" gate="G$1" x="203.2" y="426.72" smashed="yes">
<attribute name="NAME" x="196.85" y="427.2026" size="1.27" layer="95"/>
<attribute name="VALUE" x="206.248" y="427.228" size="1.27" layer="96"/>
</instance>
<instance part="R34" gate="G$1" x="203.2" y="424.18" smashed="yes">
<attribute name="NAME" x="196.85" y="424.6626" size="1.27" layer="95"/>
<attribute name="VALUE" x="206.248" y="424.688" size="1.27" layer="96"/>
</instance>
<instance part="R35" gate="G$1" x="256.54" y="434.34" smashed="yes">
<attribute name="NAME" x="250.698" y="434.8226" size="1.27" layer="95"/>
<attribute name="VALUE" x="259.334" y="434.848" size="1.27" layer="96"/>
</instance>
<instance part="R36" gate="G$1" x="251.46" y="419.1" smashed="yes">
<attribute name="NAME" x="245.364" y="419.5826" size="1.27" layer="95"/>
<attribute name="VALUE" x="254.254" y="419.608" size="1.27" layer="96"/>
</instance>
<instance part="U2" gate="G$1" x="238.76" y="441.96" smashed="yes" rot="MR0">
<attribute name="NAME" x="218.44" y="443.738" size="1.27" layer="95" ratio="15" rot="MR180"/>
<attribute name="VALUE" x="218.44" y="415.798" size="1.27" layer="96" ratio="15" rot="MR180"/>
</instance>
<instance part="C19" gate="G$1" x="200.66" y="439.42" smashed="yes" rot="R180">
<attribute name="NAME" x="195.58" y="439.928" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="201.93" y="439.928" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C20" gate="G$1" x="180.34" y="416.56" smashed="yes" rot="R270">
<attribute name="NAME" x="179.832" y="411.48" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="179.832" y="417.83" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C21" gate="G$1" x="187.96" y="416.56" smashed="yes" rot="R270">
<attribute name="NAME" x="187.452" y="411.48" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="187.452" y="417.83" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND17" gate="1" x="193.04" y="434.34" smashed="yes">
<attribute name="VALUE" x="190.5" y="431.8" size="1.27" layer="96"/>
</instance>
<instance part="GND18" gate="1" x="180.34" y="403.86" smashed="yes">
<attribute name="VALUE" x="177.8" y="401.32" size="1.27" layer="96"/>
</instance>
<instance part="GND19" gate="1" x="187.96" y="403.86" smashed="yes">
<attribute name="VALUE" x="185.42" y="401.32" size="1.27" layer="96"/>
</instance>
<instance part="R37" gate="G$1" x="355.6" y="477.52" smashed="yes" rot="R180">
<attribute name="NAME" x="349.758" y="478.0026" size="1.27" layer="95"/>
<attribute name="VALUE" x="358.648" y="478.028" size="1.27" layer="96"/>
</instance>
<instance part="R38" gate="G$1" x="340.36" y="472.44" smashed="yes" rot="R180">
<attribute name="NAME" x="334.01" y="472.9226" size="1.27" layer="95"/>
<attribute name="VALUE" x="343.408" y="472.948" size="1.27" layer="96"/>
</instance>
<instance part="R39" gate="G$1" x="332.74" y="495.3" smashed="yes" rot="R270">
<attribute name="NAME" x="332.2574" y="489.204" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="332.232" y="498.094" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="LS1" gate="G$1" x="325.12" y="495.3" smashed="yes" rot="R90">
<attribute name="NAME" x="315.722" y="499.618" size="1.27" layer="95" ratio="15"/>
<attribute name="VALUE" x="310.896" y="487.934" size="1.27" layer="96" ratio="15"/>
</instance>
<instance part="Q1" gate="G$1" x="335.28" y="480.06" smashed="yes" rot="MR0">
<attribute name="NAME" x="324.612" y="481.076" size="1.27" layer="95" ratio="15" rot="MR180"/>
<attribute name="VALUE" x="324.612" y="479.044" size="1.27" layer="96" ratio="15" rot="MR180"/>
</instance>
<instance part="GND20" gate="1" x="332.74" y="467.36" smashed="yes">
<attribute name="VALUE" x="330.2" y="464.82" size="1.27" layer="96"/>
</instance>
<instance part="+3V5" gate="G$1" x="327.66" y="508" smashed="yes">
<attribute name="VALUE" x="325.628" y="508.508" size="1.27" layer="96"/>
</instance>
<instance part="+3V6" gate="G$1" x="210.82" y="447.04" smashed="yes">
<attribute name="VALUE" x="208.788" y="447.802" size="1.27" layer="96"/>
</instance>
<instance part="+3V7" gate="G$1" x="254" y="121.92" smashed="yes">
<attribute name="VALUE" x="251.46" y="116.84" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="U3" gate="G$1" x="71.12" y="401.32" smashed="yes">
<attribute name="NAME" x="63.5" y="416.56" size="1.27" layer="95" ratio="15"/>
<attribute name="VALUE" x="63.5" y="419.1" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="R40" gate="G$1" x="53.34" y="426.72" smashed="yes" rot="R90">
<attribute name="NAME" x="52.8574" y="420.624" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="52.832" y="429.514" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R41" gate="G$1" x="43.18" y="426.72" smashed="yes" rot="R90">
<attribute name="NAME" x="42.6974" y="420.878" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="42.672" y="429.514" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C22" gate="G$1" x="38.1" y="426.72" smashed="yes" rot="R270">
<attribute name="NAME" x="37.592" y="421.64" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="37.592" y="427.99" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R42" gate="G$1" x="93.98" y="426.72" smashed="yes" rot="R90">
<attribute name="NAME" x="93.4974" y="420.37" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="93.472" y="429.768" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R43" gate="G$1" x="104.14" y="426.72" smashed="yes" rot="R90">
<attribute name="NAME" x="103.6574" y="420.624" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="103.632" y="429.768" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C23" gate="G$1" x="109.22" y="426.72" smashed="yes" rot="R270">
<attribute name="NAME" x="108.712" y="421.64" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="108.712" y="427.99" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND21" gate="1" x="38.1" y="416.56" smashed="yes">
<attribute name="VALUE" x="35.56" y="414.02" size="1.27" layer="96"/>
</instance>
<instance part="GND22" gate="1" x="109.22" y="416.56" smashed="yes">
<attribute name="VALUE" x="106.68" y="414.02" size="1.27" layer="96"/>
</instance>
<instance part="GND23" gate="1" x="53.34" y="401.32" smashed="yes">
<attribute name="VALUE" x="51.816" y="399.542" size="1.27" layer="96"/>
</instance>
<instance part="+3V8" gate="G$1" x="30.48" y="441.96" smashed="yes">
<attribute name="VALUE" x="27.94" y="436.88" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R44" gate="G$1" x="127" y="436.88" smashed="yes">
<attribute name="NAME" x="120.904" y="437.3626" size="1.27" layer="95"/>
<attribute name="VALUE" x="129.794" y="437.388" size="1.27" layer="96"/>
</instance>
<instance part="R45" gate="G$1" x="127" y="439.42" smashed="yes">
<attribute name="NAME" x="120.904" y="439.9026" size="1.27" layer="95"/>
<attribute name="VALUE" x="129.794" y="439.928" size="1.27" layer="96"/>
</instance>
<instance part="R46" gate="G$1" x="33.02" y="152.4" smashed="yes">
<attribute name="NAME" x="26.416" y="152.8826" size="1.27" layer="95"/>
<attribute name="VALUE" x="36.068" y="152.908" size="1.27" layer="96"/>
</instance>
<instance part="R47" gate="G$1" x="33.02" y="154.94" smashed="yes">
<attribute name="NAME" x="26.67" y="155.4226" size="1.27" layer="95"/>
<attribute name="VALUE" x="35.814" y="155.448" size="1.27" layer="96"/>
</instance>
<instance part="R48" gate="G$1" x="33.02" y="124.46" smashed="yes">
<attribute name="NAME" x="26.924" y="124.9426" size="1.27" layer="95"/>
<attribute name="VALUE" x="35.814" y="124.968" size="1.27" layer="96"/>
</instance>
<instance part="R49" gate="G$1" x="33.02" y="127" smashed="yes">
<attribute name="NAME" x="26.924" y="127.4826" size="1.27" layer="95"/>
<attribute name="VALUE" x="35.814" y="127.508" size="1.27" layer="96"/>
</instance>
<instance part="J23" gate="G$1" x="147.32" y="434.34" smashed="yes" rot="R270"/>
<instance part="TP4" gate="G$1" x="127" y="431.8" smashed="yes" rot="R90">
<attribute name="NAME" x="120.904" y="431.8" size="1.27" layer="95"/>
</instance>
<instance part="TP5" gate="G$1" x="127" y="426.72" smashed="yes" rot="R90">
<attribute name="NAME" x="120.904" y="426.72" size="1.27" layer="95"/>
</instance>
<instance part="GND24" gate="1" x="129.54" y="421.64" smashed="yes">
<attribute name="VALUE" x="127" y="419.1" size="1.27" layer="96"/>
</instance>
<instance part="+3V9" gate="G$1" x="137.16" y="444.5" smashed="yes">
<attribute name="VALUE" x="134.874" y="444.754" size="1.27" layer="96"/>
</instance>
<instance part="U4" gate="G$1" x="68.58" y="314.96" smashed="yes">
<attribute name="NAME" x="60.96" y="330.2" size="1.27" layer="95" ratio="15"/>
<attribute name="VALUE" x="60.96" y="332.74" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="R50" gate="G$1" x="50.8" y="337.82" smashed="yes" rot="R90">
<attribute name="NAME" x="50.3174" y="331.978" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="50.292" y="340.614" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R51" gate="G$1" x="40.64" y="337.82" smashed="yes" rot="R90">
<attribute name="NAME" x="40.1574" y="331.978" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="40.132" y="340.614" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C24" gate="G$1" x="35.56" y="340.36" smashed="yes" rot="R270">
<attribute name="NAME" x="35.052" y="335.28" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="35.052" y="341.63" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R52" gate="G$1" x="91.44" y="337.82" smashed="yes" rot="R90">
<attribute name="NAME" x="90.9574" y="331.724" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="90.932" y="340.868" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R53" gate="G$1" x="101.6" y="337.82" smashed="yes" rot="R90">
<attribute name="NAME" x="101.1174" y="331.724" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="101.092" y="340.868" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C25" gate="G$1" x="106.68" y="340.36" smashed="yes" rot="R270">
<attribute name="NAME" x="106.172" y="335.28" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="106.172" y="341.63" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND25" gate="1" x="35.56" y="330.2" smashed="yes">
<attribute name="VALUE" x="33.02" y="327.66" size="1.27" layer="96"/>
</instance>
<instance part="GND26" gate="1" x="106.68" y="330.2" smashed="yes">
<attribute name="VALUE" x="104.14" y="327.66" size="1.27" layer="96"/>
</instance>
<instance part="GND27" gate="1" x="50.8" y="314.96" smashed="yes">
<attribute name="VALUE" x="48.26" y="312.42" size="1.27" layer="96"/>
</instance>
<instance part="+3V10" gate="G$1" x="27.94" y="355.6" smashed="yes">
<attribute name="VALUE" x="25.4" y="350.52" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R54" gate="G$1" x="124.46" y="350.52" smashed="yes">
<attribute name="NAME" x="118.618" y="351.0026" size="1.27" layer="95"/>
<attribute name="VALUE" x="127.254" y="351.028" size="1.27" layer="96"/>
</instance>
<instance part="R55" gate="G$1" x="124.46" y="353.06" smashed="yes">
<attribute name="NAME" x="118.618" y="353.5426" size="1.27" layer="95"/>
<attribute name="VALUE" x="127.254" y="353.568" size="1.27" layer="96"/>
</instance>
<instance part="J24" gate="G$1" x="144.78" y="347.98" smashed="yes" rot="R270"/>
<instance part="TP6" gate="G$1" x="124.46" y="345.44" smashed="yes" rot="R90">
<attribute name="NAME" x="118.364" y="345.44" size="1.27" layer="95"/>
</instance>
<instance part="TP7" gate="G$1" x="124.46" y="340.36" smashed="yes" rot="R90">
<attribute name="NAME" x="118.364" y="340.36" size="1.27" layer="95"/>
</instance>
<instance part="GND28" gate="1" x="127" y="335.28" smashed="yes">
<attribute name="VALUE" x="124.46" y="332.74" size="1.27" layer="96"/>
</instance>
<instance part="+3V11" gate="G$1" x="134.62" y="358.14" smashed="yes">
<attribute name="VALUE" x="132.334" y="358.394" size="1.27" layer="96"/>
</instance>
<instance part="J25" gate="-1" x="355.6" y="365.76" smashed="yes">
<attribute name="NAME" x="352.298" y="366.268" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J25" gate="-2" x="355.6" y="363.22" smashed="yes">
<attribute name="NAME" x="352.298" y="363.728" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J25" gate="-3" x="355.6" y="360.68" smashed="yes">
<attribute name="NAME" x="352.298" y="361.188" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J25" gate="-4" x="355.6" y="358.14" smashed="yes">
<attribute name="NAME" x="352.298" y="358.648" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J26" gate="P" x="17.78" y="500.38" smashed="yes" rot="R180">
<attribute name="NAME" x="21.082" y="499.872" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J26" gate="N" x="17.78" y="485.14" smashed="yes" rot="R180">
<attribute name="NAME" x="21.082" y="484.632" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="GND29" gate="1" x="25.4" y="480.06" smashed="yes">
<attribute name="VALUE" x="22.86" y="477.52" size="1.27" layer="96"/>
</instance>
<instance part="D3" gate="G$1" x="33.02" y="500.38" smashed="yes">
<attribute name="NAME" x="28.194" y="500.888" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="27.432" y="504.444" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="L1" gate="G$1" x="48.26" y="500.38" smashed="yes">
<attribute name="NAME" x="42.418" y="500.888" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="51.308" y="500.888" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R56" gate="G$1" x="48.26" y="505.46" smashed="yes">
<attribute name="NAME" x="42.164" y="505.9426" size="1.27" layer="95"/>
<attribute name="VALUE" x="50.8" y="505.968" size="1.27" layer="96"/>
</instance>
<instance part="R57" gate="G$1" x="132.08" y="505.46" smashed="yes">
<attribute name="NAME" x="126.492" y="505.9426" size="1.27" layer="95"/>
<attribute name="VALUE" x="134.874" y="505.968" size="1.27" layer="96"/>
</instance>
<instance part="U5" gate="G$1" x="78.74" y="500.38" smashed="yes">
<attribute name="NAME" x="71.12" y="505.968" size="1.27" layer="95"/>
<attribute name="VALUE" x="71.12" y="510.54" size="1.778" layer="96"/>
</instance>
<instance part="C26" gate="G$1" x="58.42" y="492.76" smashed="yes" rot="R270">
<attribute name="NAME" x="57.912" y="487.68" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="57.912" y="494.03" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C27" gate="G$1" x="63.5" y="492.76" smashed="yes" rot="R270">
<attribute name="NAME" x="62.992" y="487.68" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="62.992" y="494.03" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R58" gate="G$1" x="96.52" y="492.76" smashed="yes" rot="R90">
<attribute name="NAME" x="96.0374" y="486.918" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="96.012" y="496.062" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R59" gate="G$1" x="71.12" y="482.6" smashed="yes" rot="R180">
<attribute name="NAME" x="72.39" y="481.1014" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="72.39" y="485.902" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="R60" gate="G$1" x="104.14" y="485.14" smashed="yes" rot="R180">
<attribute name="NAME" x="98.298" y="485.6226" size="1.27" layer="95"/>
<attribute name="VALUE" x="106.934" y="485.648" size="1.27" layer="96"/>
</instance>
<instance part="D4" gate="G$1" x="106.68" y="492.76" smashed="yes" rot="R180">
<attribute name="NAME" x="102.362" y="493.268" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="102.362" y="496.062" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="U6" gate="G$1" x="104.14" y="474.98" smashed="yes" rot="MR90">
<attribute name="NAME" x="99.06" y="472.948" size="1.27" layer="95" rot="MR0"/>
<attribute name="VALUE" x="96.52" y="471.932" size="1.27" layer="96" rot="MR180"/>
</instance>
<instance part="C28" gate="G$1" x="116.84" y="492.76" smashed="yes" rot="R270">
<attribute name="NAME" x="116.332" y="487.68" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="116.332" y="494.03" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C29" gate="G$1" x="121.92" y="492.76" smashed="yes" rot="R270">
<attribute name="NAME" x="121.412" y="487.68" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="121.412" y="494.03" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="L3" gate="G$1" x="132.08" y="500.38" smashed="yes">
<attribute name="NAME" x="126.492" y="500.888" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="135.128" y="500.888" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C30" gate="G$1" x="142.24" y="492.76" smashed="yes" rot="R270">
<attribute name="NAME" x="141.732" y="487.68" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="141.732" y="494.03" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C31" gate="G$1" x="147.32" y="492.76" smashed="yes" rot="R270">
<attribute name="NAME" x="146.812" y="487.68" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="146.812" y="494.03" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C32" gate="G$1" x="132.08" y="480.06" smashed="yes">
<attribute name="NAME" x="127" y="480.568" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="133.35" y="480.568" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND30" gate="1" x="147.32" y="467.36" smashed="yes">
<attribute name="VALUE" x="144.78" y="464.82" size="1.27" layer="96"/>
</instance>
<instance part="GND32" gate="1" x="58.42" y="477.52" smashed="yes">
<attribute name="VALUE" x="55.88" y="474.98" size="1.27" layer="96"/>
</instance>
<instance part="+3V12" gate="G$1" x="147.32" y="505.46" smashed="yes">
<attribute name="VALUE" x="145.034" y="506.222" size="1.27" layer="96"/>
</instance>
<instance part="D5" gate="G$1" x="243.84" y="60.96" smashed="yes" rot="R270">
<attribute name="NAME" x="243.332" y="55.88" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="238.76" y="66.04" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="D6" gate="G$1" x="236.22" y="60.96" smashed="yes" rot="R270">
<attribute name="NAME" x="235.712" y="55.88" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="231.14" y="66.04" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="GND31" gate="1" x="233.68" y="45.72" smashed="yes">
<attribute name="VALUE" x="231.902" y="43.942" size="1.27" layer="96"/>
</instance>
<instance part="FAULT_CTRL1" gate="G$1" x="236.22" y="106.68" smashed="yes" rot="R270">
<attribute name="NAME" x="241.3" y="106.68" size="1.27" layer="95"/>
</instance>
<instance part="FAULT_CTRL2" gate="G$1" x="236.22" y="101.6" smashed="yes" rot="R270">
<attribute name="NAME" x="241.3" y="101.6" size="1.27" layer="95"/>
</instance>
<instance part="FAULT_CTRL3" gate="G$1" x="236.22" y="93.98" smashed="yes" rot="R270">
<attribute name="NAME" x="241.3" y="93.98" size="1.27" layer="95"/>
</instance>
<instance part="FAULT_CTRL4" gate="G$1" x="190.5" y="43.18" smashed="yes" rot="R180">
<attribute name="NAME" x="190.5" y="38.1" size="1.27" layer="95" rot="R90" align="bottom-right"/>
</instance>
<instance part="FAULT_CTRL5" gate="G$1" x="185.42" y="43.18" smashed="yes" rot="R180">
<attribute name="NAME" x="185.42" y="38.1" size="1.27" layer="95" rot="R90" align="bottom-right"/>
</instance>
<instance part="FAULT_CTRL6" gate="G$1" x="180.34" y="43.18" smashed="yes" rot="R180">
<attribute name="NAME" x="180.34" y="38.1" size="1.27" layer="95" rot="R90" align="bottom-right"/>
</instance>
<instance part="FAULT_CTRL7" gate="G$1" x="175.26" y="43.18" smashed="yes" rot="R180">
<attribute name="NAME" x="175.26" y="38.1" size="1.27" layer="95" rot="R90" align="bottom-right"/>
</instance>
<instance part="FAULT_CTRL8" gate="G$1" x="170.18" y="43.18" smashed="yes" rot="R180">
<attribute name="NAME" x="170.18" y="38.1" size="1.27" layer="95" rot="R90" align="bottom-right"/>
</instance>
<instance part="FAULT_CTRL9" gate="G$1" x="147.32" y="43.18" smashed="yes" rot="R180">
<attribute name="NAME" x="147.32" y="38.1" size="1.27" layer="95" rot="R90" align="bottom-right"/>
</instance>
<instance part="FAULT_CTRL10" gate="G$1" x="142.24" y="43.18" smashed="yes" rot="R180">
<attribute name="NAME" x="142.24" y="38.1" size="1.27" layer="95" rot="R90" align="bottom-right"/>
</instance>
<instance part="FAULT_CTRL11" gate="G$1" x="137.16" y="43.18" smashed="yes" rot="R180">
<attribute name="NAME" x="137.16" y="38.1" size="1.27" layer="95" rot="R90" align="bottom-right"/>
</instance>
<instance part="FAULT_CTRL12" gate="G$1" x="132.08" y="43.18" smashed="yes" rot="R180">
<attribute name="NAME" x="132.08" y="38.1" size="1.27" layer="95" rot="R90" align="bottom-right"/>
</instance>
<instance part="FAULT_CTRL13" gate="G$1" x="86.36" y="86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="71.12" y="86.36" size="1.27" layer="95"/>
</instance>
<instance part="FAULT_CTRL14" gate="G$1" x="86.36" y="91.44" smashed="yes" rot="R90">
<attribute name="NAME" x="71.12" y="91.44" size="1.27" layer="95"/>
</instance>
<instance part="U$3" gate="G$1" x="0" y="264.16" smashed="yes">
<attribute name="DRAWING_NAME" x="344.17" y="279.4" size="1.27" layer="94"/>
<attribute name="LAST_DATE_TIME" x="344.17" y="274.32" size="1.27" layer="94"/>
<attribute name="SHEET" x="357.505" y="269.24" size="1.27" layer="94"/>
</instance>
<instance part="C33" gate="G$1" x="337.82" y="439.42" smashed="yes">
<attribute name="NAME" x="336.55" y="440.69" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="336.55" y="436.88" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C34" gate="G$1" x="337.82" y="424.18" smashed="yes">
<attribute name="NAME" x="336.55" y="425.45" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="336.55" y="421.64" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C35" gate="G$1" x="337.82" y="403.86" smashed="yes">
<attribute name="NAME" x="336.55" y="405.13" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="336.55" y="401.32" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C36" gate="G$1" x="337.82" y="388.62" smashed="yes">
<attribute name="NAME" x="336.55" y="389.89" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="336.55" y="386.08" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C37" gate="G$1" x="337.82" y="370.84" smashed="yes">
<attribute name="NAME" x="336.55" y="372.11" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="336.55" y="368.3" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="+3V13" gate="G$1" x="345.44" y="444.5" smashed="yes">
<attribute name="VALUE" x="345.44" y="444.5" size="1.27" layer="96"/>
</instance>
<instance part="GND33" gate="1" x="327.66" y="370.84" smashed="yes" rot="R270">
<attribute name="VALUE" x="325.12" y="373.38" size="1.27" layer="96" rot="R270"/>
</instance>
<instance part="GND34" gate="1" x="327.66" y="388.62" smashed="yes" rot="R270">
<attribute name="VALUE" x="325.12" y="391.16" size="1.27" layer="96" rot="R270"/>
</instance>
<instance part="GND35" gate="1" x="327.66" y="403.86" smashed="yes" rot="R270">
<attribute name="VALUE" x="325.12" y="406.4" size="1.27" layer="96" rot="R270"/>
</instance>
<instance part="GND36" gate="1" x="327.66" y="424.18" smashed="yes" rot="R270">
<attribute name="VALUE" x="325.12" y="426.72" size="1.27" layer="96" rot="R270"/>
</instance>
<instance part="GND37" gate="1" x="327.66" y="439.42" smashed="yes" rot="R270">
<attribute name="VALUE" x="325.12" y="441.96" size="1.27" layer="96" rot="R270"/>
</instance>
<instance part="D7" gate="G$1" x="340.36" y="193.04" smashed="yes">
<attribute name="NAME" x="342.138" y="193.548" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="335.28" y="187.96" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="D8" gate="G$1" x="340.36" y="185.42" smashed="yes">
<attribute name="NAME" x="342.138" y="185.928" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="335.28" y="180.34" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="D9" gate="G$1" x="340.36" y="177.8" smashed="yes">
<attribute name="NAME" x="342.138" y="178.308" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="335.28" y="172.72" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="D10" gate="G$1" x="340.36" y="170.18" smashed="yes">
<attribute name="NAME" x="342.138" y="170.688" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="335.28" y="165.1" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="D11" gate="G$1" x="340.36" y="162.56" smashed="yes">
<attribute name="NAME" x="342.138" y="163.068" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="335.28" y="157.48" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="D12" gate="G$1" x="340.36" y="154.94" smashed="yes">
<attribute name="NAME" x="342.392" y="155.448" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="335.28" y="149.86" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="D13" gate="G$1" x="340.36" y="147.32" smashed="yes">
<attribute name="NAME" x="342.138" y="147.828" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="335.28" y="142.24" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="D14" gate="G$1" x="340.36" y="139.7" smashed="yes">
<attribute name="NAME" x="342.138" y="140.208" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="335.28" y="134.62" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="D15" gate="G$1" x="340.36" y="132.08" smashed="yes">
<attribute name="NAME" x="342.138" y="132.588" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="335.28" y="127" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="D16" gate="G$1" x="340.36" y="124.46" smashed="yes">
<attribute name="NAME" x="342.138" y="124.968" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="335.28" y="119.38" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="D17" gate="G$1" x="340.36" y="116.84" smashed="yes">
<attribute name="NAME" x="342.138" y="117.348" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="335.28" y="111.76" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="D18" gate="G$1" x="340.36" y="109.22" smashed="yes">
<attribute name="NAME" x="342.138" y="109.728" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="335.28" y="104.14" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="D19" gate="G$1" x="340.36" y="101.6" smashed="yes">
<attribute name="NAME" x="342.138" y="102.108" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="335.28" y="96.52" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="D20" gate="G$1" x="340.36" y="93.98" smashed="yes">
<attribute name="NAME" x="342.138" y="94.488" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="335.28" y="88.9" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R61" gate="G$1" x="325.12" y="193.04" smashed="yes" rot="R180">
<attribute name="NAME" x="318.77" y="193.5226" size="1.27" layer="95"/>
<attribute name="VALUE" x="327.66" y="193.548" size="1.27" layer="96"/>
</instance>
<instance part="R62" gate="G$1" x="325.12" y="185.42" smashed="yes" rot="R180">
<attribute name="NAME" x="318.77" y="185.9026" size="1.27" layer="95"/>
<attribute name="VALUE" x="327.66" y="185.928" size="1.27" layer="96"/>
</instance>
<instance part="R63" gate="G$1" x="325.12" y="177.8" smashed="yes" rot="R180">
<attribute name="NAME" x="318.77" y="178.2826" size="1.27" layer="95"/>
<attribute name="VALUE" x="327.66" y="178.308" size="1.27" layer="96"/>
</instance>
<instance part="R64" gate="G$1" x="325.12" y="170.18" smashed="yes" rot="R180">
<attribute name="NAME" x="318.77" y="170.6626" size="1.27" layer="95"/>
<attribute name="VALUE" x="327.66" y="170.688" size="1.27" layer="96"/>
</instance>
<instance part="R65" gate="G$1" x="325.12" y="162.56" smashed="yes" rot="R180">
<attribute name="NAME" x="318.77" y="163.0426" size="1.27" layer="95"/>
<attribute name="VALUE" x="327.66" y="163.068" size="1.27" layer="96"/>
</instance>
<instance part="R66" gate="G$1" x="325.12" y="154.94" smashed="yes" rot="R180">
<attribute name="NAME" x="318.77" y="155.4226" size="1.27" layer="95"/>
<attribute name="VALUE" x="327.66" y="155.448" size="1.27" layer="96"/>
</instance>
<instance part="R67" gate="G$1" x="325.12" y="147.32" smashed="yes" rot="R180">
<attribute name="NAME" x="318.77" y="147.8026" size="1.27" layer="95"/>
<attribute name="VALUE" x="327.66" y="147.828" size="1.27" layer="96"/>
</instance>
<instance part="R68" gate="G$1" x="325.12" y="139.7" smashed="yes" rot="R180">
<attribute name="NAME" x="318.77" y="140.1826" size="1.27" layer="95"/>
<attribute name="VALUE" x="327.66" y="140.208" size="1.27" layer="96"/>
</instance>
<instance part="R69" gate="G$1" x="325.12" y="132.08" smashed="yes" rot="R180">
<attribute name="NAME" x="318.77" y="132.5626" size="1.27" layer="95"/>
<attribute name="VALUE" x="327.66" y="132.588" size="1.27" layer="96"/>
</instance>
<instance part="R70" gate="G$1" x="325.12" y="124.46" smashed="yes" rot="R180">
<attribute name="NAME" x="318.77" y="124.9426" size="1.27" layer="95"/>
<attribute name="VALUE" x="327.66" y="124.968" size="1.27" layer="96"/>
</instance>
<instance part="R71" gate="G$1" x="325.12" y="116.84" smashed="yes" rot="R180">
<attribute name="NAME" x="318.77" y="117.3226" size="1.27" layer="95"/>
<attribute name="VALUE" x="327.66" y="117.348" size="1.27" layer="96"/>
</instance>
<instance part="R72" gate="G$1" x="325.12" y="109.22" smashed="yes" rot="R180">
<attribute name="NAME" x="318.77" y="109.7026" size="1.27" layer="95"/>
<attribute name="VALUE" x="327.66" y="109.728" size="1.27" layer="96"/>
</instance>
<instance part="R73" gate="G$1" x="325.12" y="101.6" smashed="yes" rot="R180">
<attribute name="NAME" x="318.77" y="102.0826" size="1.27" layer="95"/>
<attribute name="VALUE" x="327.66" y="102.108" size="1.27" layer="96"/>
</instance>
<instance part="R74" gate="G$1" x="325.12" y="93.98" smashed="yes" rot="R180">
<attribute name="NAME" x="318.77" y="94.4626" size="1.27" layer="95"/>
<attribute name="VALUE" x="327.66" y="94.488" size="1.27" layer="96"/>
</instance>
<instance part="GND38" gate="1" x="350.52" y="88.9" smashed="yes">
<attribute name="VALUE" x="347.98" y="86.36" size="1.27" layer="96"/>
</instance>
<instance part="TP15" gate="G$1" x="91.44" y="104.14" smashed="yes" rot="R90">
<attribute name="NAME" x="83.82" y="104.14" size="1.27" layer="95"/>
</instance>
<instance part="R75" gate="G$1" x="256.54" y="429.26" smashed="yes">
<attribute name="NAME" x="250.698" y="429.7426" size="1.27" layer="95"/>
<attribute name="VALUE" x="259.334" y="429.768" size="1.27" layer="96"/>
</instance>
<instance part="R76" gate="G$1" x="256.54" y="426.72" smashed="yes">
<attribute name="NAME" x="250.698" y="427.2026" size="1.27" layer="95"/>
<attribute name="VALUE" x="259.334" y="427.228" size="1.27" layer="96"/>
</instance>
<instance part="+3V14" gate="G$1" x="264.16" y="431.8" smashed="yes">
<attribute name="VALUE" x="261.62" y="432.308" size="1.27" layer="96"/>
</instance>
<instance part="GND39" gate="1" x="269.24" y="426.72" smashed="yes" rot="R90">
<attribute name="VALUE" x="271.018" y="424.942" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R77" gate="G$1" x="236.22" y="73.66" smashed="yes" rot="R270">
<attribute name="NAME" x="235.7374" y="67.564" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="235.712" y="76.708" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R78" gate="G$1" x="243.84" y="73.66" smashed="yes" rot="R270">
<attribute name="NAME" x="243.3574" y="67.818" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="243.332" y="76.708" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="D21" gate="G$1" x="340.36" y="81.28" smashed="yes">
<attribute name="NAME" x="342.138" y="81.788" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="335.28" y="76.2" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="D22" gate="G$1" x="340.36" y="73.66" smashed="yes">
<attribute name="NAME" x="342.138" y="74.168" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="335.28" y="68.58" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="D23" gate="G$1" x="340.36" y="66.04" smashed="yes">
<attribute name="NAME" x="342.138" y="66.548" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="335.28" y="60.96" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R79" gate="G$1" x="325.12" y="81.28" smashed="yes" rot="R180">
<attribute name="NAME" x="318.77" y="81.7626" size="1.27" layer="95"/>
<attribute name="VALUE" x="327.66" y="81.788" size="1.27" layer="96"/>
</instance>
<instance part="R80" gate="G$1" x="325.12" y="73.66" smashed="yes" rot="R180">
<attribute name="NAME" x="318.77" y="74.1426" size="1.27" layer="95"/>
<attribute name="VALUE" x="327.66" y="74.168" size="1.27" layer="96"/>
</instance>
<instance part="R81" gate="G$1" x="325.12" y="66.04" smashed="yes" rot="R180">
<attribute name="NAME" x="318.77" y="66.5226" size="1.27" layer="95"/>
<attribute name="VALUE" x="327.66" y="66.548" size="1.27" layer="96"/>
</instance>
<instance part="GND40" gate="1" x="350.52" y="38.1" smashed="yes">
<attribute name="VALUE" x="347.98" y="35.56" size="1.27" layer="96"/>
</instance>
<instance part="D24" gate="G$1" x="340.36" y="53.34" smashed="yes">
<attribute name="NAME" x="342.138" y="53.848" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="335.28" y="48.26" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="D25" gate="G$1" x="340.36" y="45.72" smashed="yes">
<attribute name="NAME" x="342.138" y="46.228" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="335.28" y="40.64" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R82" gate="G$1" x="325.12" y="53.34" smashed="yes" rot="R180">
<attribute name="NAME" x="318.77" y="53.8226" size="1.27" layer="95"/>
<attribute name="VALUE" x="327.66" y="53.848" size="1.27" layer="96"/>
</instance>
<instance part="R83" gate="G$1" x="325.12" y="45.72" smashed="yes" rot="R180">
<attribute name="NAME" x="318.77" y="46.2026" size="1.27" layer="95"/>
<attribute name="VALUE" x="327.66" y="46.228" size="1.27" layer="96"/>
</instance>
<instance part="+3V15" gate="G$1" x="304.8" y="60.96" smashed="yes">
<attribute name="VALUE" x="302.26" y="55.88" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="U7" gate="G$1" x="220.98" y="320.04" smashed="yes">
<attribute name="NAME" x="213.36" y="335.28" size="1.27" layer="95" ratio="15"/>
<attribute name="VALUE" x="213.36" y="337.82" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="R84" gate="G$1" x="203.2" y="342.9" smashed="yes" rot="R90">
<attribute name="NAME" x="202.7174" y="337.058" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="202.692" y="345.694" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R85" gate="G$1" x="193.04" y="342.9" smashed="yes" rot="R90">
<attribute name="NAME" x="192.5574" y="337.058" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="192.532" y="345.694" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C38" gate="G$1" x="187.96" y="345.44" smashed="yes" rot="R270">
<attribute name="NAME" x="187.452" y="340.36" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="187.452" y="346.71" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R86" gate="G$1" x="243.84" y="342.9" smashed="yes" rot="R90">
<attribute name="NAME" x="243.3574" y="336.804" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="243.332" y="345.948" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R87" gate="G$1" x="254" y="342.9" smashed="yes" rot="R90">
<attribute name="NAME" x="253.5174" y="336.804" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="253.492" y="345.948" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C39" gate="G$1" x="259.08" y="345.44" smashed="yes" rot="R270">
<attribute name="NAME" x="258.572" y="340.36" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="258.572" y="346.71" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND41" gate="1" x="187.96" y="335.28" smashed="yes">
<attribute name="VALUE" x="185.42" y="332.74" size="1.27" layer="96"/>
</instance>
<instance part="GND42" gate="1" x="259.08" y="335.28" smashed="yes">
<attribute name="VALUE" x="256.54" y="332.74" size="1.27" layer="96"/>
</instance>
<instance part="GND43" gate="1" x="203.2" y="320.04" smashed="yes">
<attribute name="VALUE" x="200.66" y="317.5" size="1.27" layer="96"/>
</instance>
<instance part="+3V16" gate="G$1" x="180.34" y="360.68" smashed="yes">
<attribute name="VALUE" x="177.8" y="355.6" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R88" gate="G$1" x="276.86" y="355.6" smashed="yes">
<attribute name="NAME" x="271.018" y="356.0826" size="1.27" layer="95"/>
<attribute name="VALUE" x="279.654" y="356.108" size="1.27" layer="96"/>
</instance>
<instance part="R89" gate="G$1" x="276.86" y="358.14" smashed="yes">
<attribute name="NAME" x="271.018" y="358.6226" size="1.27" layer="95"/>
<attribute name="VALUE" x="279.654" y="358.648" size="1.27" layer="96"/>
</instance>
<instance part="J27" gate="G$1" x="297.18" y="353.06" smashed="yes" rot="R270"/>
<instance part="TP8" gate="G$1" x="276.86" y="350.52" smashed="yes" rot="R90">
<attribute name="NAME" x="270.764" y="350.52" size="1.27" layer="95"/>
</instance>
<instance part="TP9" gate="G$1" x="276.86" y="345.44" smashed="yes" rot="R90">
<attribute name="NAME" x="270.764" y="345.44" size="1.27" layer="95"/>
</instance>
<instance part="GND44" gate="1" x="279.4" y="340.36" smashed="yes">
<attribute name="VALUE" x="276.86" y="337.82" size="1.27" layer="96"/>
</instance>
<instance part="+3V17" gate="G$1" x="287.02" y="363.22" smashed="yes">
<attribute name="VALUE" x="284.734" y="363.474" size="1.27" layer="96"/>
</instance>
<instance part="J25" gate="G$1" x="365.76" y="340.36" rot="R90"/>
<instance part="J28" gate="-1" x="355.6" y="347.98"/>
<instance part="J28" gate="-2" x="355.6" y="345.44"/>
<instance part="J28" gate="-3" x="355.6" y="342.9"/>
<instance part="J28" gate="-4" x="355.6" y="340.36"/>
<instance part="C40" gate="G$1" x="337.82" y="353.06" smashed="yes">
<attribute name="NAME" x="336.55" y="354.33" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="336.55" y="350.52" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND45" gate="1" x="327.66" y="353.06" smashed="yes" rot="R270">
<attribute name="VALUE" x="325.12" y="355.6" size="1.27" layer="96" rot="R270"/>
</instance>
<instance part="D26" gate="G$1" x="182.88" y="500.38" smashed="yes">
<attribute name="NAME" x="178.054" y="500.888" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="177.292" y="504.444" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="L2" gate="G$1" x="198.12" y="500.38" smashed="yes">
<attribute name="NAME" x="192.278" y="500.888" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="201.168" y="500.888" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R90" gate="G$1" x="198.12" y="505.46" smashed="yes">
<attribute name="NAME" x="192.024" y="505.9426" size="1.27" layer="95"/>
<attribute name="VALUE" x="200.66" y="505.968" size="1.27" layer="96"/>
</instance>
<instance part="R91" gate="G$1" x="281.94" y="505.46" smashed="yes">
<attribute name="NAME" x="276.352" y="505.9426" size="1.27" layer="95"/>
<attribute name="VALUE" x="284.734" y="505.968" size="1.27" layer="96"/>
</instance>
<instance part="U8" gate="G$1" x="228.6" y="500.38" smashed="yes">
<attribute name="NAME" x="220.98" y="505.968" size="1.27" layer="95"/>
<attribute name="VALUE" x="220.98" y="510.54" size="1.778" layer="96"/>
</instance>
<instance part="C41" gate="G$1" x="208.28" y="492.76" smashed="yes" rot="R270">
<attribute name="NAME" x="207.772" y="487.68" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="207.772" y="494.03" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C42" gate="G$1" x="213.36" y="492.76" smashed="yes" rot="R270">
<attribute name="NAME" x="212.852" y="487.68" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="212.852" y="494.03" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R92" gate="G$1" x="246.38" y="492.76" smashed="yes" rot="R90">
<attribute name="NAME" x="245.8974" y="486.918" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="245.872" y="496.062" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R93" gate="G$1" x="220.98" y="482.6" smashed="yes" rot="R180">
<attribute name="NAME" x="222.25" y="481.1014" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="222.25" y="485.902" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="R94" gate="G$1" x="254" y="485.14" smashed="yes" rot="R180">
<attribute name="NAME" x="248.158" y="485.6226" size="1.27" layer="95"/>
<attribute name="VALUE" x="256.794" y="485.648" size="1.27" layer="96"/>
</instance>
<instance part="D27" gate="G$1" x="256.54" y="492.76" smashed="yes" rot="R180">
<attribute name="NAME" x="252.222" y="493.268" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="252.222" y="496.062" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="U9" gate="G$1" x="254" y="474.98" smashed="yes" rot="MR90">
<attribute name="NAME" x="248.92" y="472.948" size="1.27" layer="95" rot="MR0"/>
<attribute name="VALUE" x="246.38" y="471.932" size="1.27" layer="96" rot="MR180"/>
</instance>
<instance part="C43" gate="G$1" x="266.7" y="492.76" smashed="yes" rot="R270">
<attribute name="NAME" x="266.192" y="487.68" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="266.192" y="494.03" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C44" gate="G$1" x="271.78" y="492.76" smashed="yes" rot="R270">
<attribute name="NAME" x="271.272" y="487.68" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="271.272" y="494.03" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="L4" gate="G$1" x="281.94" y="500.38" smashed="yes">
<attribute name="NAME" x="276.352" y="500.888" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="284.988" y="500.888" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C45" gate="G$1" x="292.1" y="492.76" smashed="yes" rot="R270">
<attribute name="NAME" x="291.592" y="487.68" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="291.592" y="494.03" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C46" gate="G$1" x="297.18" y="492.76" smashed="yes" rot="R270">
<attribute name="NAME" x="296.672" y="487.68" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="296.672" y="494.03" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C47" gate="G$1" x="281.94" y="480.06" smashed="yes">
<attribute name="NAME" x="276.86" y="480.568" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="283.21" y="480.568" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND46" gate="1" x="297.18" y="467.36" smashed="yes">
<attribute name="VALUE" x="294.64" y="464.82" size="1.27" layer="96"/>
</instance>
<instance part="GND47" gate="1" x="208.28" y="477.52" smashed="yes">
<attribute name="VALUE" x="205.74" y="474.98" size="1.27" layer="96"/>
</instance>
<instance part="P+1" gate="1" x="297.18" y="505.46"/>
</instances>
<busses>
<bus name="TDO,TDI,TMS,TCK,!RST,TEST">
<segment>
<wire x1="142.24" y1="195.58" x2="154.94" y2="195.58" width="0.762" layer="92"/>
</segment>
<segment>
<wire x1="17.78" y1="210.82" x2="17.78" y2="185.42" width="0.762" layer="92"/>
<wire x1="17.78" y1="185.42" x2="20.32" y2="182.88" width="0.762" layer="92"/>
<wire x1="20.32" y1="182.88" x2="93.98" y2="182.88" width="0.762" layer="92"/>
<wire x1="93.98" y1="182.88" x2="96.52" y2="185.42" width="0.762" layer="92"/>
<wire x1="96.52" y1="185.42" x2="96.52" y2="205.74" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="A[0..13]">
<segment>
<wire x1="137.16" y1="193.04" x2="121.92" y2="193.04" width="0.762" layer="92"/>
<wire x1="121.92" y1="193.04" x2="83.82" y2="154.94" width="0.762" layer="92"/>
<wire x1="83.82" y1="154.94" x2="83.82" y2="124.46" width="0.762" layer="92"/>
</segment>
<segment>
<wire x1="515.62" y1="236.22" x2="426.72" y2="236.22" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="SCLK,MISO,MOSI,CS">
<segment>
<wire x1="256.54" y1="142.24" x2="256.54" y2="134.62" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="DBG_TX,DBG_RX">
<segment>
<wire x1="180.34" y1="203.2" x2="182.88" y2="203.2" width="0.762" layer="92"/>
</segment>
<segment>
<wire x1="327.66" y1="434.34" x2="327.66" y2="431.8" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="EXT_TXD,EXT_RXD">
<segment>
<wire x1="246.38" y1="88.9" x2="246.38" y2="91.44" width="0.762" layer="92"/>
</segment>
<segment>
<wire x1="327.66" y1="416.56" x2="327.66" y2="414.02" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="COM_TXD,COM_RXD">
<segment>
<wire x1="167.64" y1="30.48" x2="165.1" y2="30.48" width="0.762" layer="92"/>
</segment>
<segment>
<wire x1="327.66" y1="398.78" x2="327.66" y2="396.24" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="PS_I2C_SDA,PS_I2C_SCL">
<segment>
<wire x1="154.94" y1="30.48" x2="152.4" y2="30.48" width="0.762" layer="92"/>
</segment>
<segment>
<wire x1="22.86" y1="388.62" x2="121.92" y2="388.62" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="AUX_I2C_SDA,AUX_I2C_SCL">
<segment>
<wire x1="251.46" y1="132.08" x2="251.46" y2="129.54" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="FAULT_LED[1..14]">
<segment>
<wire x1="93.98" y1="12.7" x2="231.14" y2="12.7" width="0.762" layer="92"/>
<wire x1="231.14" y1="12.7" x2="261.62" y2="43.18" width="0.762" layer="92"/>
<wire x1="261.62" y1="43.18" x2="261.62" y2="106.68" width="0.762" layer="92"/>
<wire x1="93.98" y1="12.7" x2="68.58" y2="38.1" width="0.762" layer="92"/>
<wire x1="68.58" y1="38.1" x2="68.58" y2="86.36" width="0.762" layer="92"/>
</segment>
<segment>
<wire x1="302.26" y1="195.58" x2="302.26" y2="91.44" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="DAC_SDA,DAC_SCL">
<segment>
<wire x1="187.96" y1="200.66" x2="190.5" y2="200.66" width="0.762" layer="92"/>
</segment>
<segment>
<wire x1="170.18" y1="307.34" x2="276.86" y2="307.34" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="TDO" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="92"/>
<wire x1="152.4" y1="180.34" x2="152.4" y2="195.58" width="0.1524" layer="91"/>
<label x="152.4" y="182.88" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="J1" gate="-1" pin="1"/>
<wire x1="20.32" y1="200.66" x2="48.26" y2="200.66" width="0.1524" layer="91"/>
<label x="20.32" y="200.66" size="1.27" layer="95"/>
<wire x1="20.32" y1="200.66" x2="17.78" y2="198.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TDI" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="93"/>
<wire x1="149.86" y1="180.34" x2="149.86" y2="195.58" width="0.1524" layer="91"/>
<label x="149.86" y="182.88" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="J1" gate="-3" pin="1"/>
<wire x1="20.32" y1="203.2" x2="48.26" y2="203.2" width="0.1524" layer="91"/>
<label x="20.32" y="203.2" size="1.27" layer="95"/>
<wire x1="20.32" y1="203.2" x2="17.78" y2="200.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TMS" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="94"/>
<wire x1="147.32" y1="180.34" x2="147.32" y2="195.58" width="0.1524" layer="91"/>
<label x="147.32" y="182.88" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="J1" gate="-5" pin="1"/>
<wire x1="20.32" y1="205.74" x2="48.26" y2="205.74" width="0.1524" layer="91"/>
<label x="20.32" y="205.74" size="1.27" layer="95"/>
<wire x1="20.32" y1="205.74" x2="17.78" y2="203.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TCK" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="95"/>
<wire x1="144.78" y1="180.34" x2="144.78" y2="195.58" width="0.1524" layer="91"/>
<label x="144.78" y="182.88" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="J1" gate="-7" pin="1"/>
<wire x1="20.32" y1="208.28" x2="48.26" y2="208.28" width="0.1524" layer="91"/>
<label x="20.32" y="208.28" size="1.27" layer="95"/>
<wire x1="20.32" y1="208.28" x2="17.78" y2="205.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="!RST" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="96"/>
<wire x1="142.24" y1="180.34" x2="142.24" y2="195.58" width="0.1524" layer="91"/>
<label x="142.24" y="182.88" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="J1" gate="-11" pin="1"/>
<wire x1="20.32" y1="213.36" x2="27.94" y2="213.36" width="0.1524" layer="91"/>
<label x="20.32" y="213.36" size="1.27" layer="95"/>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="27.94" y1="213.36" x2="48.26" y2="213.36" width="0.1524" layer="91"/>
<wire x1="25.4" y1="238.76" x2="27.94" y2="238.76" width="0.1524" layer="91"/>
<wire x1="27.94" y1="238.76" x2="30.48" y2="238.76" width="0.1524" layer="91"/>
<wire x1="27.94" y1="238.76" x2="27.94" y2="220.98" width="0.1524" layer="91"/>
<junction x="27.94" y="213.36"/>
<junction x="27.94" y="238.76"/>
<wire x1="27.94" y1="220.98" x2="27.94" y2="213.36" width="0.1524" layer="91"/>
<junction x="27.94" y="220.98"/>
<wire x1="20.32" y1="213.36" x2="17.78" y2="210.82" width="0.1524" layer="91"/>
<wire x1="27.94" y1="220.98" x2="25.4" y2="220.98" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="1"/>
<wire x1="25.4" y1="220.98" x2="22.86" y2="220.98" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="2"/>
<wire x1="22.86" y1="231.14" x2="25.4" y2="231.14" width="0.1524" layer="91"/>
<wire x1="25.4" y1="231.14" x2="25.4" y2="220.98" width="0.1524" layer="91"/>
<junction x="25.4" y="220.98"/>
</segment>
</net>
<net name="TEST" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="91"/>
<wire x1="154.94" y1="180.34" x2="154.94" y2="195.58" width="0.1524" layer="91"/>
<label x="154.94" y="182.88" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="J1" gate="-8" pin="1"/>
<wire x1="71.12" y1="208.28" x2="93.98" y2="208.28" width="0.1524" layer="91"/>
<wire x1="93.98" y1="208.28" x2="96.52" y2="205.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="J1" gate="-9" pin="1"/>
<wire x1="48.26" y1="210.82" x2="43.18" y2="210.82" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="43.18" y1="210.82" x2="43.18" y2="198.12" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="40.64" y1="238.76" x2="43.18" y2="238.76" width="0.1524" layer="91"/>
<wire x1="43.18" y1="238.76" x2="43.18" y2="210.82" width="0.1524" layer="91"/>
<junction x="43.18" y="210.82"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="88.9" y1="198.12" x2="93.98" y2="198.12" width="0.1524" layer="91"/>
<wire x1="93.98" y1="198.12" x2="93.98" y2="193.04" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="93.98" y1="193.04" x2="93.98" y2="190.5" width="0.1524" layer="91"/>
<wire x1="88.9" y1="193.04" x2="93.98" y2="193.04" width="0.1524" layer="91"/>
<junction x="93.98" y="193.04"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="12.7" y1="218.44" x2="12.7" y2="220.98" width="0.1524" layer="91"/>
<wire x1="12.7" y1="220.98" x2="15.24" y2="220.98" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="3"/>
<wire x1="15.24" y1="220.98" x2="17.78" y2="220.98" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="4"/>
<wire x1="17.78" y1="231.14" x2="15.24" y2="231.14" width="0.1524" layer="91"/>
<wire x1="15.24" y1="231.14" x2="15.24" y2="220.98" width="0.1524" layer="91"/>
<junction x="15.24" y="220.98"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="88"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="162.56" y1="180.34" x2="162.56" y2="185.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="297.18" y1="226.06" x2="297.18" y2="223.52" width="0.1524" layer="91"/>
<wire x1="297.18" y1="223.52" x2="302.26" y2="223.52" width="0.1524" layer="91"/>
<wire x1="302.26" y1="223.52" x2="307.34" y2="223.52" width="0.1524" layer="91"/>
<wire x1="307.34" y1="223.52" x2="312.42" y2="223.52" width="0.1524" layer="91"/>
<wire x1="312.42" y1="223.52" x2="322.58" y2="223.52" width="0.1524" layer="91"/>
<wire x1="322.58" y1="223.52" x2="327.66" y2="223.52" width="0.1524" layer="91"/>
<wire x1="327.66" y1="223.52" x2="332.74" y2="223.52" width="0.1524" layer="91"/>
<wire x1="332.74" y1="223.52" x2="332.74" y2="220.98" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="332.74" y1="226.06" x2="332.74" y2="223.52" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="302.26" y1="226.06" x2="302.26" y2="223.52" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="307.34" y1="226.06" x2="307.34" y2="223.52" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="312.42" y1="226.06" x2="312.42" y2="223.52" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="322.58" y1="226.06" x2="322.58" y2="223.52" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="327.66" y1="226.06" x2="327.66" y2="223.52" width="0.1524" layer="91"/>
<junction x="302.26" y="223.52"/>
<junction x="307.34" y="223.52"/>
<junction x="312.42" y="223.52"/>
<junction x="322.58" y="223.52"/>
<junction x="327.66" y="223.52"/>
<junction x="332.74" y="223.52"/>
<pinref part="GND8" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND11" gate="1" pin="GND"/>
<pinref part="U1" gate="G$1" pin="15"/>
<wire x1="91.44" y1="109.22" x2="96.52" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="63"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="228.6" y1="114.3" x2="233.68" y2="114.3" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="37"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="160.02" y1="48.26" x2="160.02" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="63.5" y1="132.08" x2="63.5" y2="129.54" width="0.1524" layer="91"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="63.5" y1="129.54" x2="63.5" y2="127" width="0.1524" layer="91"/>
<wire x1="63.5" y1="129.54" x2="58.42" y2="129.54" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="58.42" y1="129.54" x2="53.34" y2="129.54" width="0.1524" layer="91"/>
<wire x1="53.34" y1="129.54" x2="53.34" y2="132.08" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="58.42" y1="132.08" x2="58.42" y2="129.54" width="0.1524" layer="91"/>
<junction x="58.42" y="129.54"/>
<junction x="63.5" y="129.54"/>
<wire x1="53.34" y1="129.54" x2="53.34" y2="127" width="0.1524" layer="91"/>
<junction x="53.34" y="129.54"/>
<pinref part="J4" gate="G$1" pin="1"/>
<wire x1="53.34" y1="127" x2="53.34" y2="119.38" width="0.1524" layer="91"/>
<wire x1="20.32" y1="119.38" x2="25.4" y2="119.38" width="0.1524" layer="91"/>
<pinref part="TP3" gate="G$1" pin="PP"/>
<wire x1="25.4" y1="119.38" x2="53.34" y2="119.38" width="0.1524" layer="91"/>
<wire x1="50.8" y1="127" x2="53.34" y2="127" width="0.1524" layer="91"/>
<junction x="53.34" y="127"/>
<pinref part="R48" gate="G$1" pin="1"/>
<wire x1="27.94" y1="124.46" x2="25.4" y2="124.46" width="0.1524" layer="91"/>
<wire x1="25.4" y1="124.46" x2="25.4" y2="119.38" width="0.1524" layer="91"/>
<junction x="25.4" y="119.38"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="-"/>
<wire x1="175.26" y1="220.98" x2="175.26" y2="226.06" width="0.1524" layer="91"/>
<wire x1="175.26" y1="226.06" x2="182.88" y2="226.06" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="-"/>
<wire x1="182.88" y1="220.98" x2="182.88" y2="226.06" width="0.1524" layer="91"/>
<junction x="182.88" y="226.06"/>
<wire x1="182.88" y1="226.06" x2="187.96" y2="226.06" width="0.1524" layer="91"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="187.96" y1="226.06" x2="187.96" y2="223.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="482.6" y1="218.44" x2="472.44" y2="218.44" width="0.1524" layer="91"/>
<wire x1="472.44" y1="218.44" x2="472.44" y2="213.36" width="0.1524" layer="91"/>
<pinref part="GND6" gate="1" pin="GND"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="472.44" y1="213.36" x2="472.44" y2="195.58" width="0.1524" layer="91"/>
<wire x1="472.44" y1="195.58" x2="472.44" y2="190.5" width="0.1524" layer="91"/>
<wire x1="472.44" y1="190.5" x2="472.44" y2="172.72" width="0.1524" layer="91"/>
<wire x1="472.44" y1="172.72" x2="472.44" y2="167.64" width="0.1524" layer="91"/>
<wire x1="472.44" y1="167.64" x2="472.44" y2="152.4" width="0.1524" layer="91"/>
<wire x1="472.44" y1="152.4" x2="472.44" y2="144.78" width="0.1524" layer="91"/>
<wire x1="472.44" y1="144.78" x2="472.44" y2="129.54" width="0.1524" layer="91"/>
<wire x1="472.44" y1="129.54" x2="472.44" y2="121.92" width="0.1524" layer="91"/>
<wire x1="472.44" y1="121.92" x2="472.44" y2="106.68" width="0.1524" layer="91"/>
<wire x1="472.44" y1="106.68" x2="472.44" y2="101.6" width="0.1524" layer="91"/>
<wire x1="472.44" y1="101.6" x2="472.44" y2="83.82" width="0.1524" layer="91"/>
<wire x1="472.44" y1="83.82" x2="472.44" y2="78.74" width="0.1524" layer="91"/>
<wire x1="472.44" y1="78.74" x2="472.44" y2="71.12" width="0.1524" layer="91"/>
<wire x1="459.74" y1="213.36" x2="472.44" y2="213.36" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="482.6" y1="195.58" x2="472.44" y2="195.58" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="459.74" y1="167.64" x2="472.44" y2="167.64" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="482.6" y1="172.72" x2="472.44" y2="172.72" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="459.74" y1="144.78" x2="472.44" y2="144.78" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="482.6" y1="152.4" x2="472.44" y2="152.4" width="0.1524" layer="91"/>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="459.74" y1="121.92" x2="472.44" y2="121.92" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="482.6" y1="129.54" x2="472.44" y2="129.54" width="0.1524" layer="91"/>
<pinref part="R25" gate="G$1" pin="2"/>
<wire x1="459.74" y1="101.6" x2="472.44" y2="101.6" width="0.1524" layer="91"/>
<pinref part="R26" gate="G$1" pin="1"/>
<wire x1="482.6" y1="106.68" x2="472.44" y2="106.68" width="0.1524" layer="91"/>
<pinref part="R28" gate="G$1" pin="2"/>
<wire x1="459.74" y1="78.74" x2="472.44" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R31" gate="G$1" pin="1"/>
<wire x1="482.6" y1="83.82" x2="472.44" y2="83.82" width="0.1524" layer="91"/>
<junction x="472.44" y="213.36"/>
<junction x="472.44" y="195.58"/>
<junction x="472.44" y="167.64"/>
<junction x="472.44" y="172.72"/>
<junction x="472.44" y="144.78"/>
<junction x="472.44" y="152.4"/>
<junction x="472.44" y="121.92"/>
<junction x="472.44" y="129.54"/>
<junction x="472.44" y="101.6"/>
<junction x="472.44" y="106.68"/>
<junction x="472.44" y="78.74"/>
<junction x="472.44" y="83.82"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="459.74" y1="190.5" x2="472.44" y2="190.5" width="0.1524" layer="91"/>
<junction x="472.44" y="190.5"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="2"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="73.66" y1="236.22" x2="71.12" y2="236.22" width="0.1524" layer="91"/>
<wire x1="71.12" y1="236.22" x2="71.12" y2="233.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="40.64" y1="91.44" x2="40.64" y2="88.9" width="0.1524" layer="91"/>
<wire x1="40.64" y1="88.9" x2="48.26" y2="88.9" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="48.26" y1="88.9" x2="55.88" y2="88.9" width="0.1524" layer="91"/>
<wire x1="55.88" y1="88.9" x2="55.88" y2="91.44" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="3"/>
<wire x1="48.26" y1="99.06" x2="48.26" y2="88.9" width="0.1524" layer="91"/>
<wire x1="40.64" y1="88.9" x2="40.64" y2="86.36" width="0.1524" layer="91"/>
<pinref part="GND14" gate="1" pin="GND"/>
<junction x="40.64" y="88.9"/>
<junction x="48.26" y="88.9"/>
</segment>
<segment>
<pinref part="C17" gate="G$1" pin="1"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="266.7" y1="116.84" x2="269.24" y2="116.84" width="0.1524" layer="91"/>
<wire x1="269.24" y1="116.84" x2="269.24" y2="111.76" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="269.24" y1="111.76" x2="269.24" y2="109.22" width="0.1524" layer="91"/>
<wire x1="266.7" y1="111.76" x2="269.24" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J19" gate="-4" pin="1"/>
<wire x1="350.52" y1="429.26" x2="347.98" y2="429.26" width="0.1524" layer="91"/>
<wire x1="347.98" y1="429.26" x2="347.98" y2="411.48" width="0.1524" layer="91"/>
<pinref part="GND16" gate="1" pin="GND"/>
<pinref part="J21" gate="-4" pin="1"/>
<wire x1="347.98" y1="411.48" x2="347.98" y2="393.7" width="0.1524" layer="91"/>
<wire x1="347.98" y1="393.7" x2="347.98" y2="375.92" width="0.1524" layer="91"/>
<wire x1="347.98" y1="375.92" x2="347.98" y2="358.14" width="0.1524" layer="91"/>
<wire x1="347.98" y1="358.14" x2="347.98" y2="340.36" width="0.1524" layer="91"/>
<wire x1="347.98" y1="340.36" x2="347.98" y2="337.82" width="0.1524" layer="91"/>
<wire x1="350.52" y1="393.7" x2="347.98" y2="393.7" width="0.1524" layer="91"/>
<pinref part="J20" gate="-4" pin="1"/>
<wire x1="350.52" y1="411.48" x2="347.98" y2="411.48" width="0.1524" layer="91"/>
<junction x="347.98" y="411.48"/>
<junction x="347.98" y="393.7"/>
<pinref part="J22" gate="-4" pin="1"/>
<wire x1="350.52" y1="375.92" x2="347.98" y2="375.92" width="0.1524" layer="91"/>
<junction x="347.98" y="375.92"/>
<pinref part="J25" gate="-4" pin="1"/>
<wire x1="350.52" y1="358.14" x2="347.98" y2="358.14" width="0.1524" layer="91"/>
<junction x="347.98" y="358.14"/>
<pinref part="J28" gate="-4" pin="1"/>
<wire x1="350.52" y1="340.36" x2="347.98" y2="340.36" width="0.1524" layer="91"/>
<junction x="347.98" y="340.36"/>
</segment>
<segment>
<pinref part="C19" gate="G$1" pin="2"/>
<wire x1="195.58" y1="439.42" x2="193.04" y2="439.42" width="0.1524" layer="91"/>
<wire x1="193.04" y1="439.42" x2="193.04" y2="436.88" width="0.1524" layer="91"/>
<pinref part="GND17" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C20" gate="G$1" pin="2"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="180.34" y1="411.48" x2="180.34" y2="406.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C21" gate="G$1" pin="2"/>
<pinref part="GND19" gate="1" pin="GND"/>
<wire x1="187.96" y1="411.48" x2="187.96" y2="406.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q1" gate="G$1" pin="S"/>
<wire x1="332.74" y1="474.98" x2="332.74" y2="472.44" width="0.1524" layer="91"/>
<pinref part="R38" gate="G$1" pin="2"/>
<wire x1="332.74" y1="472.44" x2="335.28" y2="472.44" width="0.1524" layer="91"/>
<wire x1="332.74" y1="472.44" x2="332.74" y2="469.9" width="0.1524" layer="91"/>
<pinref part="GND20" gate="1" pin="GND"/>
<junction x="332.74" y="472.44"/>
</segment>
<segment>
<pinref part="C22" gate="G$1" pin="2"/>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="38.1" y1="421.64" x2="38.1" y2="419.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C23" gate="G$1" pin="2"/>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="109.22" y1="421.64" x2="109.22" y2="419.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND23" gate="1" pin="GND"/>
<pinref part="U3" gate="G$1" pin="2"/>
<wire x1="53.34" y1="403.86" x2="58.42" y2="403.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="TP5" gate="G$1" pin="PP"/>
<pinref part="GND24" gate="1" pin="GND"/>
<wire x1="127" y1="426.72" x2="129.54" y2="426.72" width="0.1524" layer="91"/>
<wire x1="129.54" y1="426.72" x2="129.54" y2="424.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C24" gate="G$1" pin="2"/>
<pinref part="GND25" gate="1" pin="GND"/>
<wire x1="35.56" y1="335.28" x2="35.56" y2="332.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C25" gate="G$1" pin="2"/>
<pinref part="GND26" gate="1" pin="GND"/>
<wire x1="106.68" y1="335.28" x2="106.68" y2="332.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND27" gate="1" pin="GND"/>
<pinref part="U4" gate="G$1" pin="2"/>
<wire x1="50.8" y1="317.5" x2="55.88" y2="317.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="TP7" gate="G$1" pin="PP"/>
<pinref part="GND28" gate="1" pin="GND"/>
<wire x1="124.46" y1="340.36" x2="127" y2="340.36" width="0.1524" layer="91"/>
<wire x1="127" y1="340.36" x2="127" y2="337.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND29" gate="1" pin="GND"/>
<wire x1="25.4" y1="485.14" x2="25.4" y2="482.6" width="0.1524" layer="91"/>
<pinref part="J26" gate="N" pin="1"/>
<wire x1="22.86" y1="485.14" x2="25.4" y2="485.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C28" gate="G$1" pin="2"/>
<wire x1="116.84" y1="487.68" x2="116.84" y2="485.14" width="0.1524" layer="91"/>
<pinref part="C31" gate="G$1" pin="2"/>
<wire x1="116.84" y1="485.14" x2="121.92" y2="485.14" width="0.1524" layer="91"/>
<wire x1="121.92" y1="485.14" x2="142.24" y2="485.14" width="0.1524" layer="91"/>
<wire x1="142.24" y1="485.14" x2="147.32" y2="485.14" width="0.1524" layer="91"/>
<wire x1="147.32" y1="485.14" x2="147.32" y2="487.68" width="0.1524" layer="91"/>
<pinref part="C30" gate="G$1" pin="2"/>
<wire x1="142.24" y1="487.68" x2="142.24" y2="485.14" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="2"/>
<wire x1="121.92" y1="487.68" x2="121.92" y2="485.14" width="0.1524" layer="91"/>
<pinref part="C32" gate="G$1" pin="2"/>
<wire x1="137.16" y1="480.06" x2="147.32" y2="480.06" width="0.1524" layer="91"/>
<wire x1="147.32" y1="480.06" x2="147.32" y2="485.14" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="C"/>
<wire x1="109.22" y1="472.44" x2="147.32" y2="472.44" width="0.1524" layer="91"/>
<wire x1="147.32" y1="472.44" x2="147.32" y2="480.06" width="0.1524" layer="91"/>
<pinref part="GND30" gate="1" pin="GND"/>
<wire x1="147.32" y1="472.44" x2="147.32" y2="469.9" width="0.1524" layer="91"/>
<junction x="147.32" y="485.14"/>
<junction x="142.24" y="485.14"/>
<junction x="121.92" y="485.14"/>
<junction x="147.32" y="480.06"/>
<junction x="147.32" y="472.44"/>
</segment>
<segment>
<pinref part="C26" gate="G$1" pin="2"/>
<pinref part="GND32" gate="1" pin="GND"/>
<wire x1="58.42" y1="487.68" x2="58.42" y2="482.6" width="0.1524" layer="91"/>
<pinref part="R59" gate="G$1" pin="2"/>
<wire x1="58.42" y1="482.6" x2="58.42" y2="480.06" width="0.1524" layer="91"/>
<wire x1="66.04" y1="482.6" x2="63.5" y2="482.6" width="0.1524" layer="91"/>
<junction x="58.42" y="482.6"/>
<pinref part="C27" gate="G$1" pin="2"/>
<wire x1="63.5" y1="482.6" x2="58.42" y2="482.6" width="0.1524" layer="91"/>
<wire x1="63.5" y1="482.6" x2="63.5" y2="487.68" width="0.1524" layer="91"/>
<junction x="63.5" y="482.6"/>
</segment>
<segment>
<pinref part="D5" gate="G$1" pin="-"/>
<wire x1="243.84" y1="55.88" x2="243.84" y2="50.8" width="0.1524" layer="91"/>
<wire x1="243.84" y1="50.8" x2="236.22" y2="50.8" width="0.1524" layer="91"/>
<pinref part="D6" gate="G$1" pin="-"/>
<wire x1="236.22" y1="55.88" x2="236.22" y2="50.8" width="0.1524" layer="91"/>
<junction x="236.22" y="50.8"/>
<wire x1="236.22" y1="50.8" x2="233.68" y2="50.8" width="0.1524" layer="91"/>
<wire x1="233.68" y1="50.8" x2="233.68" y2="48.26" width="0.1524" layer="91"/>
<pinref part="GND31" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND37" gate="1" pin="GND"/>
<pinref part="C33" gate="G$1" pin="1"/>
<wire x1="330.2" y1="439.42" x2="332.74" y2="439.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND36" gate="1" pin="GND"/>
<pinref part="C34" gate="G$1" pin="1"/>
<wire x1="330.2" y1="424.18" x2="332.74" y2="424.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND35" gate="1" pin="GND"/>
<pinref part="C35" gate="G$1" pin="1"/>
<wire x1="330.2" y1="403.86" x2="332.74" y2="403.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND34" gate="1" pin="GND"/>
<pinref part="C36" gate="G$1" pin="1"/>
<wire x1="330.2" y1="388.62" x2="332.74" y2="388.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND33" gate="1" pin="GND"/>
<pinref part="C37" gate="G$1" pin="1"/>
<wire x1="330.2" y1="370.84" x2="332.74" y2="370.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D7" gate="G$1" pin="-"/>
<wire x1="345.44" y1="193.04" x2="350.52" y2="193.04" width="0.1524" layer="91"/>
<pinref part="GND38" gate="1" pin="GND"/>
<wire x1="350.52" y1="193.04" x2="350.52" y2="185.42" width="0.1524" layer="91"/>
<pinref part="D8" gate="G$1" pin="-"/>
<wire x1="350.52" y1="185.42" x2="350.52" y2="177.8" width="0.1524" layer="91"/>
<wire x1="350.52" y1="177.8" x2="350.52" y2="170.18" width="0.1524" layer="91"/>
<wire x1="350.52" y1="170.18" x2="350.52" y2="162.56" width="0.1524" layer="91"/>
<wire x1="350.52" y1="162.56" x2="350.52" y2="154.94" width="0.1524" layer="91"/>
<wire x1="350.52" y1="154.94" x2="350.52" y2="147.32" width="0.1524" layer="91"/>
<wire x1="350.52" y1="147.32" x2="350.52" y2="139.7" width="0.1524" layer="91"/>
<wire x1="350.52" y1="139.7" x2="350.52" y2="132.08" width="0.1524" layer="91"/>
<wire x1="350.52" y1="132.08" x2="350.52" y2="124.46" width="0.1524" layer="91"/>
<wire x1="350.52" y1="124.46" x2="350.52" y2="116.84" width="0.1524" layer="91"/>
<wire x1="350.52" y1="116.84" x2="350.52" y2="109.22" width="0.1524" layer="91"/>
<wire x1="350.52" y1="109.22" x2="350.52" y2="101.6" width="0.1524" layer="91"/>
<wire x1="350.52" y1="101.6" x2="350.52" y2="93.98" width="0.1524" layer="91"/>
<wire x1="350.52" y1="93.98" x2="350.52" y2="91.44" width="0.1524" layer="91"/>
<wire x1="345.44" y1="185.42" x2="350.52" y2="185.42" width="0.1524" layer="91"/>
<pinref part="D9" gate="G$1" pin="-"/>
<wire x1="345.44" y1="177.8" x2="350.52" y2="177.8" width="0.1524" layer="91"/>
<pinref part="D10" gate="G$1" pin="-"/>
<wire x1="345.44" y1="170.18" x2="350.52" y2="170.18" width="0.1524" layer="91"/>
<pinref part="D11" gate="G$1" pin="-"/>
<wire x1="345.44" y1="162.56" x2="350.52" y2="162.56" width="0.1524" layer="91"/>
<pinref part="D12" gate="G$1" pin="-"/>
<wire x1="345.44" y1="154.94" x2="350.52" y2="154.94" width="0.1524" layer="91"/>
<pinref part="D13" gate="G$1" pin="-"/>
<wire x1="345.44" y1="147.32" x2="350.52" y2="147.32" width="0.1524" layer="91"/>
<pinref part="D14" gate="G$1" pin="-"/>
<wire x1="345.44" y1="139.7" x2="350.52" y2="139.7" width="0.1524" layer="91"/>
<pinref part="D15" gate="G$1" pin="-"/>
<wire x1="345.44" y1="132.08" x2="350.52" y2="132.08" width="0.1524" layer="91"/>
<pinref part="D16" gate="G$1" pin="-"/>
<wire x1="345.44" y1="124.46" x2="350.52" y2="124.46" width="0.1524" layer="91"/>
<pinref part="D17" gate="G$1" pin="-"/>
<wire x1="345.44" y1="116.84" x2="350.52" y2="116.84" width="0.1524" layer="91"/>
<pinref part="D18" gate="G$1" pin="-"/>
<wire x1="345.44" y1="109.22" x2="350.52" y2="109.22" width="0.1524" layer="91"/>
<pinref part="D20" gate="G$1" pin="-"/>
<wire x1="345.44" y1="93.98" x2="350.52" y2="93.98" width="0.1524" layer="91"/>
<junction x="350.52" y="185.42"/>
<junction x="350.52" y="177.8"/>
<junction x="350.52" y="170.18"/>
<junction x="350.52" y="162.56"/>
<junction x="350.52" y="154.94"/>
<junction x="350.52" y="147.32"/>
<junction x="350.52" y="139.7"/>
<junction x="350.52" y="132.08"/>
<junction x="350.52" y="116.84"/>
<junction x="350.52" y="109.22"/>
<pinref part="D19" gate="G$1" pin="-"/>
<wire x1="345.44" y1="101.6" x2="350.52" y2="101.6" width="0.1524" layer="91"/>
<junction x="350.52" y="101.6"/>
<junction x="350.52" y="93.98"/>
</segment>
<segment>
<pinref part="R76" gate="G$1" pin="2"/>
<wire x1="261.62" y1="426.72" x2="266.7" y2="426.72" width="0.1524" layer="91"/>
<pinref part="GND39" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND40" gate="1" pin="GND"/>
<wire x1="350.52" y1="81.28" x2="350.52" y2="73.66" width="0.1524" layer="91"/>
<wire x1="350.52" y1="73.66" x2="350.52" y2="66.04" width="0.1524" layer="91"/>
<wire x1="350.52" y1="66.04" x2="350.52" y2="53.34" width="0.1524" layer="91"/>
<pinref part="D21" gate="G$1" pin="-"/>
<wire x1="350.52" y1="53.34" x2="350.52" y2="45.72" width="0.1524" layer="91"/>
<wire x1="350.52" y1="45.72" x2="350.52" y2="40.64" width="0.1524" layer="91"/>
<wire x1="345.44" y1="81.28" x2="350.52" y2="81.28" width="0.1524" layer="91"/>
<pinref part="D23" gate="G$1" pin="-"/>
<wire x1="345.44" y1="66.04" x2="350.52" y2="66.04" width="0.1524" layer="91"/>
<pinref part="D22" gate="G$1" pin="-"/>
<wire x1="345.44" y1="73.66" x2="350.52" y2="73.66" width="0.1524" layer="91"/>
<junction x="350.52" y="73.66"/>
<junction x="350.52" y="66.04"/>
<pinref part="D24" gate="G$1" pin="-"/>
<wire x1="345.44" y1="53.34" x2="350.52" y2="53.34" width="0.1524" layer="91"/>
<pinref part="D25" gate="G$1" pin="-"/>
<wire x1="345.44" y1="45.72" x2="350.52" y2="45.72" width="0.1524" layer="91"/>
<junction x="350.52" y="53.34"/>
<junction x="350.52" y="45.72"/>
</segment>
<segment>
<pinref part="C38" gate="G$1" pin="2"/>
<pinref part="GND41" gate="1" pin="GND"/>
<wire x1="187.96" y1="340.36" x2="187.96" y2="337.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C39" gate="G$1" pin="2"/>
<pinref part="GND42" gate="1" pin="GND"/>
<wire x1="259.08" y1="340.36" x2="259.08" y2="337.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND43" gate="1" pin="GND"/>
<pinref part="U7" gate="G$1" pin="2"/>
<wire x1="203.2" y1="322.58" x2="208.28" y2="322.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="TP9" gate="G$1" pin="PP"/>
<pinref part="GND44" gate="1" pin="GND"/>
<wire x1="276.86" y1="345.44" x2="279.4" y2="345.44" width="0.1524" layer="91"/>
<wire x1="279.4" y1="345.44" x2="279.4" y2="342.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND45" gate="1" pin="GND"/>
<pinref part="C40" gate="G$1" pin="1"/>
<wire x1="330.2" y1="353.06" x2="332.74" y2="353.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C43" gate="G$1" pin="2"/>
<wire x1="266.7" y1="487.68" x2="266.7" y2="485.14" width="0.1524" layer="91"/>
<pinref part="C46" gate="G$1" pin="2"/>
<wire x1="266.7" y1="485.14" x2="271.78" y2="485.14" width="0.1524" layer="91"/>
<wire x1="271.78" y1="485.14" x2="292.1" y2="485.14" width="0.1524" layer="91"/>
<wire x1="292.1" y1="485.14" x2="297.18" y2="485.14" width="0.1524" layer="91"/>
<wire x1="297.18" y1="485.14" x2="297.18" y2="487.68" width="0.1524" layer="91"/>
<pinref part="C45" gate="G$1" pin="2"/>
<wire x1="292.1" y1="487.68" x2="292.1" y2="485.14" width="0.1524" layer="91"/>
<pinref part="C44" gate="G$1" pin="2"/>
<wire x1="271.78" y1="487.68" x2="271.78" y2="485.14" width="0.1524" layer="91"/>
<pinref part="C47" gate="G$1" pin="2"/>
<wire x1="287.02" y1="480.06" x2="297.18" y2="480.06" width="0.1524" layer="91"/>
<wire x1="297.18" y1="480.06" x2="297.18" y2="485.14" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="C"/>
<wire x1="259.08" y1="472.44" x2="297.18" y2="472.44" width="0.1524" layer="91"/>
<wire x1="297.18" y1="472.44" x2="297.18" y2="480.06" width="0.1524" layer="91"/>
<pinref part="GND46" gate="1" pin="GND"/>
<wire x1="297.18" y1="472.44" x2="297.18" y2="469.9" width="0.1524" layer="91"/>
<junction x="297.18" y="485.14"/>
<junction x="292.1" y="485.14"/>
<junction x="271.78" y="485.14"/>
<junction x="297.18" y="480.06"/>
<junction x="297.18" y="472.44"/>
</segment>
<segment>
<pinref part="C41" gate="G$1" pin="2"/>
<pinref part="GND47" gate="1" pin="GND"/>
<wire x1="208.28" y1="487.68" x2="208.28" y2="482.6" width="0.1524" layer="91"/>
<pinref part="R93" gate="G$1" pin="2"/>
<wire x1="208.28" y1="482.6" x2="208.28" y2="480.06" width="0.1524" layer="91"/>
<wire x1="215.9" y1="482.6" x2="213.36" y2="482.6" width="0.1524" layer="91"/>
<junction x="208.28" y="482.6"/>
<pinref part="C42" gate="G$1" pin="2"/>
<wire x1="213.36" y1="482.6" x2="208.28" y2="482.6" width="0.1524" layer="91"/>
<wire x1="213.36" y1="482.6" x2="213.36" y2="487.68" width="0.1524" layer="91"/>
<junction x="213.36" y="482.6"/>
</segment>
</net>
<net name="VCC_PRG_IN" class="0">
<segment>
<pinref part="J1" gate="-4" pin="1"/>
<wire x1="71.12" y1="203.2" x2="73.66" y2="203.2" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="73.66" y1="203.2" x2="83.82" y2="203.2" width="0.1524" layer="91"/>
<wire x1="78.74" y1="193.04" x2="73.66" y2="193.04" width="0.1524" layer="91"/>
<wire x1="73.66" y1="193.04" x2="73.66" y2="203.2" width="0.1524" layer="91"/>
<junction x="73.66" y="203.2"/>
<wire x1="83.82" y1="203.2" x2="83.82" y2="226.06" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="1"/>
<wire x1="83.82" y1="226.06" x2="88.9" y2="226.06" width="0.1524" layer="91"/>
<label x="83.82" y="209.296" size="1.27" layer="95" rot="R90"/>
</segment>
</net>
<net name="VCC_PRG_OUT" class="0">
<segment>
<pinref part="J1" gate="-2" pin="1"/>
<wire x1="71.12" y1="200.66" x2="76.2" y2="200.66" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="76.2" y1="200.66" x2="86.36" y2="200.66" width="0.1524" layer="91"/>
<wire x1="78.74" y1="198.12" x2="76.2" y2="198.12" width="0.1524" layer="91"/>
<wire x1="76.2" y1="198.12" x2="76.2" y2="200.66" width="0.1524" layer="91"/>
<junction x="76.2" y="200.66"/>
<pinref part="J2" gate="G$1" pin="2"/>
<wire x1="88.9" y1="220.98" x2="86.36" y2="220.98" width="0.1524" layer="91"/>
<wire x1="86.36" y1="220.98" x2="86.36" y2="200.66" width="0.1524" layer="91"/>
<label x="86.36" y="209.296" size="1.27" layer="95" rot="R90"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<wire x1="12.7" y1="241.3" x2="12.7" y2="238.76" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="12.7" y1="238.76" x2="15.24" y2="238.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="C"/>
<wire x1="88.9" y1="223.52" x2="86.36" y2="223.52" width="0.1524" layer="91"/>
<wire x1="86.36" y1="223.52" x2="86.36" y2="236.22" width="0.1524" layer="91"/>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="86.36" y1="236.22" x2="86.36" y2="241.3" width="0.1524" layer="91"/>
<wire x1="83.82" y1="236.22" x2="86.36" y2="236.22" width="0.1524" layer="91"/>
<junction x="86.36" y="236.22"/>
</segment>
<segment>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="297.18" y1="241.3" x2="297.18" y2="238.76" width="0.1524" layer="91"/>
<wire x1="297.18" y1="238.76" x2="297.18" y2="236.22" width="0.1524" layer="91"/>
<wire x1="297.18" y1="238.76" x2="302.26" y2="238.76" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="302.26" y1="238.76" x2="307.34" y2="238.76" width="0.1524" layer="91"/>
<wire x1="307.34" y1="238.76" x2="312.42" y2="238.76" width="0.1524" layer="91"/>
<wire x1="312.42" y1="238.76" x2="322.58" y2="238.76" width="0.1524" layer="91"/>
<wire x1="322.58" y1="238.76" x2="327.66" y2="238.76" width="0.1524" layer="91"/>
<wire x1="327.66" y1="238.76" x2="332.74" y2="238.76" width="0.1524" layer="91"/>
<wire x1="332.74" y1="238.76" x2="332.74" y2="236.22" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="302.26" y1="236.22" x2="302.26" y2="238.76" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="307.34" y1="236.22" x2="307.34" y2="238.76" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="312.42" y1="236.22" x2="312.42" y2="238.76" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="322.58" y1="236.22" x2="322.58" y2="238.76" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="327.66" y1="236.22" x2="327.66" y2="238.76" width="0.1524" layer="91"/>
<junction x="297.18" y="238.76"/>
<junction x="302.26" y="238.76"/>
<junction x="307.34" y="238.76"/>
<junction x="312.42" y="238.76"/>
<junction x="322.58" y="238.76"/>
<junction x="327.66" y="238.76"/>
<wire x1="332.74" y1="238.76" x2="335.28" y2="238.76" width="0.1524" layer="91"/>
<label x="335.28" y="238.76" size="1.27" layer="95" xref="yes"/>
<junction x="332.74" y="238.76"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="38"/>
<wire x1="162.56" y1="48.26" x2="162.56" y2="33.02" width="0.1524" layer="91"/>
<label x="162.56" y="33.02" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="62"/>
<wire x1="228.6" y1="111.76" x2="254" y2="111.76" width="0.1524" layer="91"/>
<wire x1="254" y1="111.76" x2="254" y2="116.84" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="254" y1="116.84" x2="254" y2="119.38" width="0.1524" layer="91"/>
<wire x1="256.54" y1="116.84" x2="254" y2="116.84" width="0.1524" layer="91"/>
<junction x="254" y="116.84"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="256.54" y1="111.76" x2="254" y2="111.76" width="0.1524" layer="91"/>
<junction x="254" y="111.76"/>
<pinref part="+3V7" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="64"/>
<wire x1="228.6" y1="116.84" x2="241.3" y2="116.84" width="0.1524" layer="91"/>
<label x="241.3" y="116.84" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="16"/>
<wire x1="96.52" y1="106.68" x2="83.82" y2="106.68" width="0.1524" layer="91"/>
<label x="83.82" y="106.68" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="87"/>
<wire x1="165.1" y1="180.34" x2="165.1" y2="193.04" width="0.1524" layer="91"/>
<label x="165.1" y="193.04" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="50.8" y1="147.32" x2="63.5" y2="147.32" width="0.1524" layer="91"/>
<wire x1="63.5" y1="147.32" x2="63.5" y2="142.24" width="0.1524" layer="91"/>
<wire x1="50.8" y1="147.32" x2="50.8" y2="152.4" width="0.1524" layer="91"/>
<pinref part="+3V4" gate="G$1" pin="+3V3"/>
<pinref part="J3" gate="G$1" pin="1"/>
<wire x1="20.32" y1="147.32" x2="25.4" y2="147.32" width="0.1524" layer="91"/>
<junction x="50.8" y="147.32"/>
<pinref part="R46" gate="G$1" pin="1"/>
<wire x1="25.4" y1="147.32" x2="50.8" y2="147.32" width="0.1524" layer="91"/>
<wire x1="27.94" y1="152.4" x2="25.4" y2="152.4" width="0.1524" layer="91"/>
<wire x1="25.4" y1="152.4" x2="25.4" y2="147.32" width="0.1524" layer="91"/>
<junction x="25.4" y="147.32"/>
</segment>
<segment>
<pinref part="J19" gate="-1" pin="1"/>
<wire x1="350.52" y1="436.88" x2="345.44" y2="436.88" width="0.1524" layer="91"/>
<wire x1="345.44" y1="436.88" x2="345.44" y2="439.42" width="0.1524" layer="91"/>
<pinref part="J20" gate="-1" pin="1"/>
<wire x1="345.44" y1="439.42" x2="345.44" y2="441.96" width="0.1524" layer="91"/>
<wire x1="350.52" y1="419.1" x2="345.44" y2="419.1" width="0.1524" layer="91"/>
<wire x1="345.44" y1="419.1" x2="345.44" y2="424.18" width="0.1524" layer="91"/>
<pinref part="J21" gate="-1" pin="1"/>
<wire x1="345.44" y1="424.18" x2="345.44" y2="436.88" width="0.1524" layer="91"/>
<wire x1="350.52" y1="401.32" x2="345.44" y2="401.32" width="0.1524" layer="91"/>
<wire x1="345.44" y1="401.32" x2="345.44" y2="403.86" width="0.1524" layer="91"/>
<junction x="345.44" y="419.1"/>
<junction x="345.44" y="436.88"/>
<pinref part="+3V13" gate="G$1" pin="+3V3"/>
<pinref part="C33" gate="G$1" pin="2"/>
<wire x1="345.44" y1="403.86" x2="345.44" y2="419.1" width="0.1524" layer="91"/>
<wire x1="342.9" y1="439.42" x2="345.44" y2="439.42" width="0.1524" layer="91"/>
<pinref part="C34" gate="G$1" pin="2"/>
<wire x1="342.9" y1="424.18" x2="345.44" y2="424.18" width="0.1524" layer="91"/>
<pinref part="C35" gate="G$1" pin="2"/>
<wire x1="342.9" y1="403.86" x2="345.44" y2="403.86" width="0.1524" layer="91"/>
<junction x="345.44" y="439.42"/>
<junction x="345.44" y="424.18"/>
<junction x="345.44" y="403.86"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="8"/>
<wire x1="215.9" y1="434.34" x2="210.82" y2="434.34" width="0.1524" layer="91"/>
<wire x1="210.82" y1="434.34" x2="210.82" y2="439.42" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="210.82" y1="439.42" x2="210.82" y2="444.5" width="0.1524" layer="91"/>
<wire x1="205.74" y1="439.42" x2="210.82" y2="439.42" width="0.1524" layer="91"/>
<junction x="210.82" y="439.42"/>
<pinref part="+3V6" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="LS1" gate="G$1" pin="2"/>
<wire x1="327.66" y1="497.84" x2="327.66" y2="502.92" width="0.1524" layer="91"/>
<pinref part="R39" gate="G$1" pin="1"/>
<wire x1="327.66" y1="502.92" x2="327.66" y2="505.46" width="0.1524" layer="91"/>
<wire x1="332.74" y1="500.38" x2="332.74" y2="502.92" width="0.1524" layer="91"/>
<wire x1="332.74" y1="502.92" x2="327.66" y2="502.92" width="0.1524" layer="91"/>
<junction x="327.66" y="502.92"/>
<pinref part="+3V5" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="38.1" y1="431.8" x2="38.1" y2="436.88" width="0.1524" layer="91"/>
<wire x1="38.1" y1="436.88" x2="30.48" y2="436.88" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="3"/>
<wire x1="58.42" y1="398.78" x2="48.26" y2="398.78" width="0.1524" layer="91"/>
<wire x1="48.26" y1="398.78" x2="48.26" y2="436.88" width="0.1524" layer="91"/>
<wire x1="48.26" y1="436.88" x2="43.18" y2="436.88" width="0.1524" layer="91"/>
<pinref part="R41" gate="G$1" pin="2"/>
<wire x1="43.18" y1="436.88" x2="38.1" y2="436.88" width="0.1524" layer="91"/>
<wire x1="43.18" y1="431.8" x2="43.18" y2="436.88" width="0.1524" layer="91"/>
<pinref part="R43" gate="G$1" pin="2"/>
<wire x1="48.26" y1="436.88" x2="88.9" y2="436.88" width="0.1524" layer="91"/>
<wire x1="88.9" y1="436.88" x2="104.14" y2="436.88" width="0.1524" layer="91"/>
<wire x1="104.14" y1="436.88" x2="104.14" y2="431.8" width="0.1524" layer="91"/>
<junction x="43.18" y="436.88"/>
<junction x="38.1" y="436.88"/>
<junction x="48.26" y="436.88"/>
<pinref part="U3" gate="G$1" pin="6"/>
<wire x1="83.82" y1="398.78" x2="88.9" y2="398.78" width="0.1524" layer="91"/>
<wire x1="88.9" y1="398.78" x2="88.9" y2="436.88" width="0.1524" layer="91"/>
<junction x="88.9" y="436.88"/>
<pinref part="+3V8" gate="G$1" pin="+3V3"/>
<wire x1="30.48" y1="439.42" x2="30.48" y2="436.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J23" gate="G$1" pin="1"/>
<wire x1="139.7" y1="436.88" x2="137.16" y2="436.88" width="0.1524" layer="91"/>
<wire x1="137.16" y1="436.88" x2="137.16" y2="439.42" width="0.1524" layer="91"/>
<pinref part="R45" gate="G$1" pin="2"/>
<wire x1="137.16" y1="439.42" x2="137.16" y2="441.96" width="0.1524" layer="91"/>
<wire x1="132.08" y1="439.42" x2="137.16" y2="439.42" width="0.1524" layer="91"/>
<junction x="137.16" y="439.42"/>
<pinref part="+3V9" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="35.56" y1="345.44" x2="35.56" y2="350.52" width="0.1524" layer="91"/>
<wire x1="35.56" y1="350.52" x2="27.94" y2="350.52" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="3"/>
<wire x1="55.88" y1="312.42" x2="45.72" y2="312.42" width="0.1524" layer="91"/>
<wire x1="45.72" y1="312.42" x2="45.72" y2="350.52" width="0.1524" layer="91"/>
<wire x1="45.72" y1="350.52" x2="40.64" y2="350.52" width="0.1524" layer="91"/>
<pinref part="R51" gate="G$1" pin="2"/>
<wire x1="40.64" y1="350.52" x2="35.56" y2="350.52" width="0.1524" layer="91"/>
<wire x1="40.64" y1="342.9" x2="40.64" y2="350.52" width="0.1524" layer="91"/>
<pinref part="R53" gate="G$1" pin="2"/>
<wire x1="45.72" y1="350.52" x2="86.36" y2="350.52" width="0.1524" layer="91"/>
<wire x1="86.36" y1="350.52" x2="101.6" y2="350.52" width="0.1524" layer="91"/>
<wire x1="101.6" y1="350.52" x2="101.6" y2="342.9" width="0.1524" layer="91"/>
<junction x="40.64" y="350.52"/>
<junction x="35.56" y="350.52"/>
<junction x="45.72" y="350.52"/>
<pinref part="U4" gate="G$1" pin="6"/>
<wire x1="81.28" y1="312.42" x2="86.36" y2="312.42" width="0.1524" layer="91"/>
<wire x1="86.36" y1="312.42" x2="86.36" y2="350.52" width="0.1524" layer="91"/>
<junction x="86.36" y="350.52"/>
<pinref part="+3V10" gate="G$1" pin="+3V3"/>
<wire x1="27.94" y1="353.06" x2="27.94" y2="350.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J24" gate="G$1" pin="1"/>
<wire x1="137.16" y1="350.52" x2="134.62" y2="350.52" width="0.1524" layer="91"/>
<wire x1="134.62" y1="350.52" x2="134.62" y2="353.06" width="0.1524" layer="91"/>
<pinref part="R55" gate="G$1" pin="2"/>
<wire x1="134.62" y1="353.06" x2="134.62" y2="355.6" width="0.1524" layer="91"/>
<wire x1="129.54" y1="353.06" x2="134.62" y2="353.06" width="0.1524" layer="91"/>
<junction x="134.62" y="353.06"/>
<pinref part="+3V11" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R57" gate="G$1" pin="2"/>
<wire x1="137.16" y1="505.46" x2="139.7" y2="505.46" width="0.1524" layer="91"/>
<wire x1="139.7" y1="505.46" x2="139.7" y2="500.38" width="0.1524" layer="91"/>
<pinref part="L3" gate="G$1" pin="2"/>
<pinref part="C31" gate="G$1" pin="1"/>
<wire x1="137.16" y1="500.38" x2="139.7" y2="500.38" width="0.1524" layer="91"/>
<wire x1="139.7" y1="500.38" x2="142.24" y2="500.38" width="0.1524" layer="91"/>
<wire x1="142.24" y1="500.38" x2="147.32" y2="500.38" width="0.1524" layer="91"/>
<wire x1="147.32" y1="500.38" x2="147.32" y2="497.84" width="0.1524" layer="91"/>
<pinref part="C30" gate="G$1" pin="1"/>
<wire x1="142.24" y1="497.84" x2="142.24" y2="500.38" width="0.1524" layer="91"/>
<junction x="139.7" y="500.38"/>
<junction x="142.24" y="500.38"/>
<wire x1="147.32" y1="500.38" x2="147.32" y2="502.92" width="0.1524" layer="91"/>
<junction x="147.32" y="500.38"/>
<pinref part="+3V12" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R75" gate="G$1" pin="2"/>
<wire x1="261.62" y1="429.26" x2="264.16" y2="429.26" width="0.1524" layer="91"/>
<pinref part="+3V14" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R82" gate="G$1" pin="2"/>
<wire x1="320.04" y1="53.34" x2="304.8" y2="53.34" width="0.1524" layer="91"/>
<pinref part="+3V15" gate="G$1" pin="+3V3"/>
<wire x1="304.8" y1="53.34" x2="304.8" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C38" gate="G$1" pin="1"/>
<wire x1="187.96" y1="350.52" x2="187.96" y2="355.6" width="0.1524" layer="91"/>
<wire x1="187.96" y1="355.6" x2="180.34" y2="355.6" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="3"/>
<wire x1="208.28" y1="317.5" x2="198.12" y2="317.5" width="0.1524" layer="91"/>
<wire x1="198.12" y1="317.5" x2="198.12" y2="355.6" width="0.1524" layer="91"/>
<wire x1="198.12" y1="355.6" x2="193.04" y2="355.6" width="0.1524" layer="91"/>
<pinref part="R85" gate="G$1" pin="2"/>
<wire x1="193.04" y1="355.6" x2="187.96" y2="355.6" width="0.1524" layer="91"/>
<wire x1="193.04" y1="347.98" x2="193.04" y2="355.6" width="0.1524" layer="91"/>
<pinref part="R87" gate="G$1" pin="2"/>
<wire x1="198.12" y1="355.6" x2="238.76" y2="355.6" width="0.1524" layer="91"/>
<wire x1="238.76" y1="355.6" x2="254" y2="355.6" width="0.1524" layer="91"/>
<wire x1="254" y1="355.6" x2="254" y2="347.98" width="0.1524" layer="91"/>
<junction x="193.04" y="355.6"/>
<junction x="187.96" y="355.6"/>
<junction x="198.12" y="355.6"/>
<pinref part="U7" gate="G$1" pin="6"/>
<wire x1="233.68" y1="317.5" x2="238.76" y2="317.5" width="0.1524" layer="91"/>
<wire x1="238.76" y1="317.5" x2="238.76" y2="355.6" width="0.1524" layer="91"/>
<junction x="238.76" y="355.6"/>
<pinref part="+3V16" gate="G$1" pin="+3V3"/>
<wire x1="180.34" y1="358.14" x2="180.34" y2="355.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J27" gate="G$1" pin="1"/>
<wire x1="289.56" y1="355.6" x2="287.02" y2="355.6" width="0.1524" layer="91"/>
<wire x1="287.02" y1="355.6" x2="287.02" y2="358.14" width="0.1524" layer="91"/>
<pinref part="R89" gate="G$1" pin="2"/>
<wire x1="287.02" y1="358.14" x2="287.02" y2="360.68" width="0.1524" layer="91"/>
<wire x1="281.94" y1="358.14" x2="287.02" y2="358.14" width="0.1524" layer="91"/>
<junction x="287.02" y="358.14"/>
<pinref part="+3V17" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="J1" gate="-13" pin="1"/>
<wire x1="45.72" y1="215.9" x2="48.26" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="J1" gate="-12" pin="1"/>
<wire x1="71.12" y1="213.36" x2="73.66" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="J1" gate="-10" pin="1"/>
<wire x1="71.12" y1="210.82" x2="73.66" y2="210.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="J1" gate="-6" pin="1"/>
<wire x1="71.12" y1="205.74" x2="73.66" y2="205.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="J1" gate="-14" pin="1"/>
<wire x1="71.12" y1="215.9" x2="73.66" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A0" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="97"/>
<wire x1="139.7" y1="180.34" x2="139.7" y2="190.5" width="0.1524" layer="91"/>
<wire x1="139.7" y1="190.5" x2="137.16" y2="193.04" width="0.1524" layer="91"/>
<label x="139.7" y="182.88" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="J6" gate="G$1" pin="C"/>
<wire x1="414.02" y1="218.44" x2="429.26" y2="218.44" width="0.1524" layer="91"/>
<wire x1="429.26" y1="218.44" x2="429.26" y2="236.22" width="0.1524" layer="91"/>
<label x="429.26" y="228.6" size="1.27" layer="95" rot="R90"/>
</segment>
</net>
<net name="A1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="98"/>
<wire x1="137.16" y1="180.34" x2="137.16" y2="190.5" width="0.1524" layer="91"/>
<wire x1="137.16" y1="190.5" x2="134.62" y2="193.04" width="0.1524" layer="91"/>
<label x="137.16" y="182.88" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="J8" gate="G$1" pin="C"/>
<wire x1="414.02" y1="195.58" x2="431.8" y2="195.58" width="0.1524" layer="91"/>
<wire x1="431.8" y1="195.58" x2="431.8" y2="236.22" width="0.1524" layer="91"/>
<label x="431.8" y="228.6" size="1.27" layer="95" rot="R90"/>
</segment>
</net>
<net name="A2" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="99"/>
<wire x1="134.62" y1="180.34" x2="134.62" y2="190.5" width="0.1524" layer="91"/>
<wire x1="134.62" y1="190.5" x2="132.08" y2="193.04" width="0.1524" layer="91"/>
<label x="134.62" y="182.88" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="J10" gate="G$1" pin="C"/>
<wire x1="414.02" y1="172.72" x2="434.34" y2="172.72" width="0.1524" layer="91"/>
<wire x1="434.34" y1="172.72" x2="434.34" y2="236.22" width="0.1524" layer="91"/>
<label x="434.34" y="228.6" size="1.27" layer="95" rot="R90"/>
</segment>
</net>
<net name="A3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="100"/>
<wire x1="132.08" y1="180.34" x2="132.08" y2="190.5" width="0.1524" layer="91"/>
<wire x1="132.08" y1="190.5" x2="129.54" y2="193.04" width="0.1524" layer="91"/>
<label x="132.08" y="182.88" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="C"/>
<wire x1="414.02" y1="149.86" x2="436.88" y2="149.86" width="0.1524" layer="91"/>
<wire x1="436.88" y1="149.86" x2="436.88" y2="236.22" width="0.1524" layer="91"/>
<label x="436.88" y="228.6" size="1.27" layer="95" rot="R90"/>
</segment>
</net>
<net name="A4" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="1"/>
<wire x1="96.52" y1="144.78" x2="86.36" y2="144.78" width="0.1524" layer="91"/>
<wire x1="86.36" y1="144.78" x2="83.82" y2="147.32" width="0.1524" layer="91"/>
<label x="88.9" y="144.78" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="J14" gate="G$1" pin="C"/>
<wire x1="414.02" y1="127" x2="439.42" y2="127" width="0.1524" layer="91"/>
<wire x1="439.42" y1="127" x2="439.42" y2="236.22" width="0.1524" layer="91"/>
<label x="439.42" y="228.6" size="1.27" layer="95" rot="R90"/>
</segment>
</net>
<net name="A5" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="2"/>
<wire x1="96.52" y1="142.24" x2="86.36" y2="142.24" width="0.1524" layer="91"/>
<wire x1="86.36" y1="142.24" x2="83.82" y2="144.78" width="0.1524" layer="91"/>
<label x="88.9" y="142.24" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="J16" gate="G$1" pin="C"/>
<wire x1="414.02" y1="106.68" x2="441.96" y2="106.68" width="0.1524" layer="91"/>
<wire x1="441.96" y1="106.68" x2="441.96" y2="236.22" width="0.1524" layer="91"/>
<label x="441.96" y="228.6" size="1.27" layer="95" rot="R90"/>
</segment>
</net>
<net name="A6" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="3"/>
<wire x1="96.52" y1="139.7" x2="86.36" y2="139.7" width="0.1524" layer="91"/>
<wire x1="86.36" y1="139.7" x2="83.82" y2="142.24" width="0.1524" layer="91"/>
<label x="88.9" y="139.7" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="J18" gate="G$1" pin="C"/>
<wire x1="414.02" y1="83.82" x2="444.5" y2="83.82" width="0.1524" layer="91"/>
<wire x1="444.5" y1="83.82" x2="444.5" y2="236.22" width="0.1524" layer="91"/>
<label x="444.5" y="228.6" size="1.27" layer="95" rot="R90"/>
</segment>
</net>
<net name="A7" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="4"/>
<wire x1="96.52" y1="137.16" x2="86.36" y2="137.16" width="0.1524" layer="91"/>
<wire x1="86.36" y1="137.16" x2="83.82" y2="139.7" width="0.1524" layer="91"/>
<label x="88.9" y="137.16" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="J17" gate="G$1" pin="C"/>
<wire x1="528.32" y1="83.82" x2="497.84" y2="83.82" width="0.1524" layer="91"/>
<wire x1="497.84" y1="83.82" x2="497.84" y2="236.22" width="0.1524" layer="91"/>
<label x="497.84" y="228.6" size="1.27" layer="95" rot="R90"/>
</segment>
</net>
<net name="A9" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="10"/>
<wire x1="96.52" y1="121.92" x2="86.36" y2="121.92" width="0.1524" layer="91"/>
<wire x1="86.36" y1="121.92" x2="83.82" y2="124.46" width="0.1524" layer="91"/>
<label x="88.9" y="121.92" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="528.32" y1="129.54" x2="502.92" y2="129.54" width="0.1524" layer="91"/>
<wire x1="502.92" y1="129.54" x2="502.92" y2="236.22" width="0.1524" layer="91"/>
<label x="502.92" y="228.6" size="1.27" layer="95" rot="R90"/>
<pinref part="J13" gate="G$1" pin="C"/>
</segment>
</net>
<net name="A8" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="9"/>
<wire x1="96.52" y1="124.46" x2="86.36" y2="124.46" width="0.1524" layer="91"/>
<wire x1="86.36" y1="124.46" x2="83.82" y2="127" width="0.1524" layer="91"/>
<label x="88.9" y="124.46" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="J15" gate="G$1" pin="C"/>
<wire x1="528.32" y1="106.68" x2="500.38" y2="106.68" width="0.1524" layer="91"/>
<wire x1="500.38" y1="106.68" x2="500.38" y2="236.22" width="0.1524" layer="91"/>
<label x="500.38" y="228.6" size="1.27" layer="95" rot="R90"/>
</segment>
</net>
<net name="A10" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="5"/>
<wire x1="96.52" y1="134.62" x2="86.36" y2="134.62" width="0.1524" layer="91"/>
<wire x1="86.36" y1="134.62" x2="83.82" y2="137.16" width="0.1524" layer="91"/>
<label x="88.9" y="134.62" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="J11" gate="G$1" pin="C"/>
<wire x1="528.32" y1="152.4" x2="505.46" y2="152.4" width="0.1524" layer="91"/>
<wire x1="505.46" y1="152.4" x2="505.46" y2="236.22" width="0.1524" layer="91"/>
<label x="505.46" y="228.6" size="1.27" layer="95" rot="R90"/>
</segment>
</net>
<net name="A11" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="6"/>
<wire x1="96.52" y1="132.08" x2="86.36" y2="132.08" width="0.1524" layer="91"/>
<wire x1="86.36" y1="132.08" x2="83.82" y2="134.62" width="0.1524" layer="91"/>
<label x="88.9" y="132.08" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="C"/>
<wire x1="528.32" y1="172.72" x2="508" y2="172.72" width="0.1524" layer="91"/>
<wire x1="508" y1="172.72" x2="508" y2="236.22" width="0.1524" layer="91"/>
<label x="508" y="228.6" size="1.27" layer="95" rot="R90"/>
</segment>
</net>
<net name="A12" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="7"/>
<wire x1="96.52" y1="129.54" x2="86.36" y2="129.54" width="0.1524" layer="91"/>
<wire x1="86.36" y1="129.54" x2="83.82" y2="132.08" width="0.1524" layer="91"/>
<label x="88.9" y="129.54" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="J7" gate="G$1" pin="C"/>
<wire x1="528.32" y1="195.58" x2="510.54" y2="195.58" width="0.1524" layer="91"/>
<wire x1="510.54" y1="195.58" x2="510.54" y2="236.22" width="0.1524" layer="91"/>
<label x="510.54" y="228.6" size="1.27" layer="95" rot="R90"/>
</segment>
</net>
<net name="A13" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="8"/>
<wire x1="96.52" y1="127" x2="86.36" y2="127" width="0.1524" layer="91"/>
<wire x1="86.36" y1="127" x2="83.82" y2="129.54" width="0.1524" layer="91"/>
<label x="88.9" y="127" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="J5" gate="G$1" pin="C"/>
<wire x1="528.32" y1="218.44" x2="513.08" y2="218.44" width="0.1524" layer="91"/>
<wire x1="513.08" y1="218.44" x2="513.08" y2="236.22" width="0.1524" layer="91"/>
<label x="513.08" y="228.6" size="1.27" layer="95" rot="R90"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="53.34" y1="144.78" x2="58.42" y2="144.78" width="0.1524" layer="91"/>
<wire x1="58.42" y1="144.78" x2="58.42" y2="142.24" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="53.34" y1="142.24" x2="53.34" y2="144.78" width="0.1524" layer="91"/>
<wire x1="58.42" y1="144.78" x2="68.58" y2="144.78" width="0.1524" layer="91"/>
<wire x1="68.58" y1="144.78" x2="68.58" y2="119.38" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="11"/>
<wire x1="68.58" y1="119.38" x2="96.52" y2="119.38" width="0.1524" layer="91"/>
<junction x="58.42" y="144.78"/>
<pinref part="J3" gate="G$1" pin="C"/>
<wire x1="20.32" y1="144.78" x2="40.64" y2="144.78" width="0.1524" layer="91"/>
<junction x="53.34" y="144.78"/>
<pinref part="R46" gate="G$1" pin="2"/>
<wire x1="40.64" y1="144.78" x2="43.18" y2="144.78" width="0.1524" layer="91"/>
<wire x1="43.18" y1="144.78" x2="53.34" y2="144.78" width="0.1524" layer="91"/>
<wire x1="38.1" y1="152.4" x2="40.64" y2="152.4" width="0.1524" layer="91"/>
<wire x1="40.64" y1="152.4" x2="40.64" y2="144.78" width="0.1524" layer="91"/>
<pinref part="R47" gate="G$1" pin="2"/>
<wire x1="38.1" y1="154.94" x2="43.18" y2="154.94" width="0.1524" layer="91"/>
<wire x1="43.18" y1="154.94" x2="43.18" y2="144.78" width="0.1524" layer="91"/>
<junction x="40.64" y="144.78"/>
<junction x="43.18" y="144.78"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="2"/>
<pinref part="TP1" gate="G$1" pin="PP"/>
<wire x1="20.32" y1="142.24" x2="22.86" y2="142.24" width="0.1524" layer="91"/>
<pinref part="R47" gate="G$1" pin="1"/>
<wire x1="22.86" y1="142.24" x2="27.94" y2="142.24" width="0.1524" layer="91"/>
<wire x1="27.94" y1="154.94" x2="22.86" y2="154.94" width="0.1524" layer="91"/>
<wire x1="22.86" y1="154.94" x2="22.86" y2="142.24" width="0.1524" layer="91"/>
<junction x="22.86" y="142.24"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="2"/>
<pinref part="TP2" gate="G$1" pin="PP"/>
<wire x1="20.32" y1="114.3" x2="22.86" y2="114.3" width="0.1524" layer="91"/>
<pinref part="R49" gate="G$1" pin="1"/>
<wire x1="22.86" y1="114.3" x2="27.94" y2="114.3" width="0.1524" layer="91"/>
<wire x1="27.94" y1="127" x2="22.86" y2="127" width="0.1524" layer="91"/>
<wire x1="22.86" y1="127" x2="22.86" y2="114.3" width="0.1524" layer="91"/>
<junction x="22.86" y="114.3"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="C"/>
<pinref part="U1" gate="G$1" pin="12"/>
<wire x1="20.32" y1="116.84" x2="40.64" y2="116.84" width="0.1524" layer="91"/>
<pinref part="R48" gate="G$1" pin="2"/>
<wire x1="40.64" y1="116.84" x2="43.18" y2="116.84" width="0.1524" layer="91"/>
<wire x1="43.18" y1="116.84" x2="96.52" y2="116.84" width="0.1524" layer="91"/>
<wire x1="38.1" y1="124.46" x2="40.64" y2="124.46" width="0.1524" layer="91"/>
<wire x1="40.64" y1="124.46" x2="40.64" y2="116.84" width="0.1524" layer="91"/>
<pinref part="R49" gate="G$1" pin="2"/>
<wire x1="38.1" y1="127" x2="43.18" y2="127" width="0.1524" layer="91"/>
<wire x1="43.18" y1="127" x2="43.18" y2="116.84" width="0.1524" layer="91"/>
<junction x="40.64" y="116.84"/>
<junction x="43.18" y="116.84"/>
</segment>
</net>
<net name="DBG_TX" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="80"/>
<wire x1="182.88" y1="180.34" x2="182.88" y2="203.2" width="0.1524" layer="91"/>
<label x="182.88" y="182.88" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="J19" gate="-2" pin="1"/>
<wire x1="350.52" y1="434.34" x2="327.66" y2="434.34" width="0.1524" layer="91"/>
<label x="330.2" y="434.34" size="1.27" layer="95"/>
</segment>
</net>
<net name="DBG_RX" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="81"/>
<wire x1="180.34" y1="180.34" x2="180.34" y2="203.2" width="0.1524" layer="91"/>
<label x="180.34" y="182.88" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="J19" gate="-3" pin="1"/>
<wire x1="350.52" y1="431.8" x2="327.66" y2="431.8" width="0.1524" layer="91"/>
<label x="330.2" y="431.8" size="1.27" layer="95"/>
</segment>
</net>
<net name="EXT_TXD" class="0">
<segment>
<pinref part="J20" gate="-2" pin="1"/>
<wire x1="350.52" y1="416.56" x2="327.66" y2="416.56" width="0.1524" layer="91"/>
<label x="330.2" y="416.56" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="53"/>
<wire x1="228.6" y1="88.9" x2="246.38" y2="88.9" width="0.1524" layer="91"/>
<label x="231.14" y="88.9" size="1.27" layer="95" ratio="15"/>
</segment>
</net>
<net name="EXT_RXD" class="0">
<segment>
<pinref part="J20" gate="-3" pin="1"/>
<wire x1="350.52" y1="414.02" x2="327.66" y2="414.02" width="0.1524" layer="91"/>
<label x="330.2" y="414.02" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="54"/>
<wire x1="228.6" y1="91.44" x2="246.38" y2="91.44" width="0.1524" layer="91"/>
<label x="231.14" y="91.44" size="1.27" layer="95" ratio="15"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="175.26" y1="205.74" x2="175.26" y2="210.82" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="+"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="177.8" y1="205.74" x2="177.8" y2="208.28" width="0.1524" layer="91"/>
<wire x1="177.8" y1="208.28" x2="182.88" y2="208.28" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="+"/>
<wire x1="182.88" y1="208.28" x2="182.88" y2="210.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DBG_TX_LED" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<pinref part="U1" gate="G$1" pin="83"/>
<wire x1="175.26" y1="195.58" x2="175.26" y2="180.34" width="0.1524" layer="91"/>
<label x="175.26" y="182.88" size="1.27" layer="95" ratio="15" rot="R90"/>
</segment>
</net>
<net name="DBG_RX_LED" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<pinref part="U1" gate="G$1" pin="82"/>
<wire x1="177.8" y1="195.58" x2="177.8" y2="180.34" width="0.1524" layer="91"/>
<label x="177.8" y="182.88" size="1.27" layer="95" ratio="15" rot="R90"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="EXT_A12" gate="G$1" pin="PP"/>
<pinref part="J5" gate="G$1" pin="2"/>
<wire x1="520.7" y1="213.36" x2="528.32" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="EXT_A11" gate="G$1" pin="PP"/>
<pinref part="J7" gate="G$1" pin="2"/>
<wire x1="520.7" y1="190.5" x2="528.32" y2="190.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="EXT_A10" gate="G$1" pin="PP"/>
<pinref part="J9" gate="G$1" pin="2"/>
<wire x1="520.7" y1="167.64" x2="528.32" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="EXT_A9" gate="G$1" pin="PP"/>
<pinref part="J11" gate="G$1" pin="2"/>
<wire x1="520.7" y1="147.32" x2="528.32" y2="147.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="EXT_A14" gate="G$1" pin="PP"/>
<pinref part="J13" gate="G$1" pin="2"/>
<wire x1="520.7" y1="124.46" x2="528.32" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="EXT_A13" gate="G$1" pin="PP"/>
<pinref part="J15" gate="G$1" pin="2"/>
<wire x1="520.7" y1="101.6" x2="528.32" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="EXT_A8" gate="G$1" pin="PP"/>
<pinref part="J17" gate="G$1" pin="2"/>
<wire x1="520.7" y1="78.74" x2="528.32" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="J6" gate="G$1" pin="2"/>
<pinref part="EXT_A1" gate="G$1" pin="PP"/>
<wire x1="414.02" y1="223.52" x2="421.64" y2="223.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="J8" gate="G$1" pin="2"/>
<pinref part="EXT_A2" gate="G$1" pin="PP"/>
<wire x1="414.02" y1="200.66" x2="421.64" y2="200.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="J10" gate="G$1" pin="2"/>
<pinref part="EXT_A3" gate="G$1" pin="PP"/>
<wire x1="414.02" y1="177.8" x2="421.64" y2="177.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="J12" gate="G$1" pin="2"/>
<pinref part="EXT_A4" gate="G$1" pin="PP"/>
<wire x1="414.02" y1="154.94" x2="421.64" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="J14" gate="G$1" pin="2"/>
<pinref part="EXT_A5" gate="G$1" pin="PP"/>
<wire x1="414.02" y1="132.08" x2="421.64" y2="132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="J16" gate="G$1" pin="2"/>
<pinref part="EXT_A6" gate="G$1" pin="PP"/>
<wire x1="414.02" y1="111.76" x2="421.64" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="J18" gate="G$1" pin="2"/>
<pinref part="EXT_A7" gate="G$1" pin="PP"/>
<wire x1="414.02" y1="88.9" x2="421.64" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="J6" gate="G$1" pin="1"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="414.02" y1="213.36" x2="447.04" y2="213.36" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="447.04" y1="213.36" x2="449.58" y2="213.36" width="0.1524" layer="91"/>
<wire x1="449.58" y1="218.44" x2="447.04" y2="218.44" width="0.1524" layer="91"/>
<wire x1="447.04" y1="218.44" x2="447.04" y2="213.36" width="0.1524" layer="91"/>
<junction x="447.04" y="213.36"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="J8" gate="G$1" pin="1"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="414.02" y1="190.5" x2="447.04" y2="190.5" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="447.04" y1="190.5" x2="449.58" y2="190.5" width="0.1524" layer="91"/>
<wire x1="449.58" y1="195.58" x2="447.04" y2="195.58" width="0.1524" layer="91"/>
<wire x1="447.04" y1="195.58" x2="447.04" y2="190.5" width="0.1524" layer="91"/>
<junction x="447.04" y="190.5"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="J10" gate="G$1" pin="1"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="414.02" y1="167.64" x2="447.04" y2="167.64" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="447.04" y1="167.64" x2="449.58" y2="167.64" width="0.1524" layer="91"/>
<wire x1="449.58" y1="172.72" x2="447.04" y2="172.72" width="0.1524" layer="91"/>
<wire x1="447.04" y1="172.72" x2="447.04" y2="167.64" width="0.1524" layer="91"/>
<junction x="447.04" y="167.64"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="J12" gate="G$1" pin="1"/>
<wire x1="414.02" y1="144.78" x2="447.04" y2="144.78" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="1"/>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="447.04" y1="144.78" x2="449.58" y2="144.78" width="0.1524" layer="91"/>
<wire x1="449.58" y1="149.86" x2="447.04" y2="149.86" width="0.1524" layer="91"/>
<wire x1="447.04" y1="149.86" x2="447.04" y2="144.78" width="0.1524" layer="91"/>
<junction x="447.04" y="144.78"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="J14" gate="G$1" pin="1"/>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="414.02" y1="121.92" x2="447.04" y2="121.92" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="447.04" y1="121.92" x2="449.58" y2="121.92" width="0.1524" layer="91"/>
<wire x1="449.58" y1="127" x2="447.04" y2="127" width="0.1524" layer="91"/>
<wire x1="447.04" y1="127" x2="447.04" y2="121.92" width="0.1524" layer="91"/>
<junction x="447.04" y="121.92"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="J16" gate="G$1" pin="1"/>
<wire x1="414.02" y1="101.6" x2="447.04" y2="101.6" width="0.1524" layer="91"/>
<pinref part="R25" gate="G$1" pin="1"/>
<pinref part="R24" gate="G$1" pin="1"/>
<wire x1="447.04" y1="101.6" x2="449.58" y2="101.6" width="0.1524" layer="91"/>
<wire x1="449.58" y1="106.68" x2="447.04" y2="106.68" width="0.1524" layer="91"/>
<wire x1="447.04" y1="106.68" x2="447.04" y2="101.6" width="0.1524" layer="91"/>
<junction x="447.04" y="101.6"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="J18" gate="G$1" pin="1"/>
<pinref part="R28" gate="G$1" pin="1"/>
<wire x1="414.02" y1="78.74" x2="447.04" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R29" gate="G$1" pin="1"/>
<wire x1="447.04" y1="78.74" x2="449.58" y2="78.74" width="0.1524" layer="91"/>
<wire x1="449.58" y1="83.82" x2="447.04" y2="83.82" width="0.1524" layer="91"/>
<wire x1="447.04" y1="83.82" x2="447.04" y2="78.74" width="0.1524" layer="91"/>
<junction x="447.04" y="78.74"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<pinref part="J5" gate="G$1" pin="1"/>
<wire x1="492.76" y1="223.52" x2="495.3" y2="223.52" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="495.3" y1="223.52" x2="528.32" y2="223.52" width="0.1524" layer="91"/>
<wire x1="492.76" y1="218.44" x2="495.3" y2="218.44" width="0.1524" layer="91"/>
<wire x1="495.3" y1="218.44" x2="495.3" y2="223.52" width="0.1524" layer="91"/>
<junction x="495.3" y="223.52"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<pinref part="J7" gate="G$1" pin="1"/>
<wire x1="492.76" y1="200.66" x2="495.3" y2="200.66" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="495.3" y1="200.66" x2="528.32" y2="200.66" width="0.1524" layer="91"/>
<wire x1="492.76" y1="195.58" x2="495.3" y2="195.58" width="0.1524" layer="91"/>
<wire x1="495.3" y1="195.58" x2="495.3" y2="200.66" width="0.1524" layer="91"/>
<junction x="495.3" y="200.66"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<pinref part="J9" gate="G$1" pin="1"/>
<wire x1="492.76" y1="177.8" x2="495.3" y2="177.8" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="495.3" y1="177.8" x2="528.32" y2="177.8" width="0.1524" layer="91"/>
<wire x1="492.76" y1="172.72" x2="495.3" y2="172.72" width="0.1524" layer="91"/>
<wire x1="495.3" y1="172.72" x2="495.3" y2="177.8" width="0.1524" layer="91"/>
<junction x="495.3" y="177.8"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="R19" gate="G$1" pin="2"/>
<pinref part="J11" gate="G$1" pin="1"/>
<wire x1="492.76" y1="157.48" x2="495.3" y2="157.48" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="495.3" y1="157.48" x2="528.32" y2="157.48" width="0.1524" layer="91"/>
<wire x1="492.76" y1="152.4" x2="495.3" y2="152.4" width="0.1524" layer="91"/>
<wire x1="495.3" y1="152.4" x2="495.3" y2="157.48" width="0.1524" layer="91"/>
<junction x="495.3" y="157.48"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="R23" gate="G$1" pin="2"/>
<pinref part="J13" gate="G$1" pin="1"/>
<wire x1="492.76" y1="134.62" x2="495.3" y2="134.62" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="2"/>
<wire x1="495.3" y1="134.62" x2="528.32" y2="134.62" width="0.1524" layer="91"/>
<wire x1="492.76" y1="129.54" x2="495.3" y2="129.54" width="0.1524" layer="91"/>
<wire x1="495.3" y1="129.54" x2="495.3" y2="134.62" width="0.1524" layer="91"/>
<junction x="495.3" y="134.62"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="R27" gate="G$1" pin="2"/>
<pinref part="J15" gate="G$1" pin="1"/>
<wire x1="492.76" y1="111.76" x2="495.3" y2="111.76" width="0.1524" layer="91"/>
<pinref part="R26" gate="G$1" pin="2"/>
<wire x1="495.3" y1="111.76" x2="528.32" y2="111.76" width="0.1524" layer="91"/>
<wire x1="492.76" y1="106.68" x2="495.3" y2="106.68" width="0.1524" layer="91"/>
<wire x1="495.3" y1="106.68" x2="495.3" y2="111.76" width="0.1524" layer="91"/>
<junction x="495.3" y="111.76"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="R30" gate="G$1" pin="2"/>
<pinref part="J17" gate="G$1" pin="1"/>
<wire x1="492.76" y1="88.9" x2="495.3" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R31" gate="G$1" pin="2"/>
<wire x1="495.3" y1="88.9" x2="528.32" y2="88.9" width="0.1524" layer="91"/>
<wire x1="492.76" y1="83.82" x2="495.3" y2="83.82" width="0.1524" layer="91"/>
<wire x1="495.3" y1="83.82" x2="495.3" y2="88.9" width="0.1524" layer="91"/>
<junction x="495.3" y="88.9"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<pinref part="OVP_IN_1" gate="G$1" pin="PP"/>
<wire x1="459.74" y1="218.44" x2="462.28" y2="218.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<pinref part="OVP_IN_2" gate="G$1" pin="PP"/>
<wire x1="459.74" y1="195.58" x2="462.28" y2="195.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="2"/>
<pinref part="OVP_IN_3" gate="G$1" pin="PP"/>
<wire x1="459.74" y1="172.72" x2="462.28" y2="172.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="2"/>
<pinref part="OVP_IN_4" gate="G$1" pin="PP"/>
<wire x1="459.74" y1="149.86" x2="462.28" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="R20" gate="G$1" pin="2"/>
<pinref part="OVP_IN_5" gate="G$1" pin="PP"/>
<wire x1="459.74" y1="127" x2="462.28" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="R24" gate="G$1" pin="2"/>
<pinref part="OVP_IN_6" gate="G$1" pin="PP"/>
<wire x1="459.74" y1="106.68" x2="462.28" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OVP_IN_7" class="0">
<segment>
<pinref part="R29" gate="G$1" pin="2"/>
<pinref part="OVP_IN_7" gate="G$1" pin="PP"/>
<wire x1="459.74" y1="83.82" x2="462.28" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="OVP_IN_8" gate="G$1" pin="PP"/>
<pinref part="R30" gate="G$1" pin="1"/>
<wire x1="480.06" y1="88.9" x2="482.6" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="OVP_IN_13" gate="G$1" pin="PP"/>
<pinref part="R27" gate="G$1" pin="1"/>
<wire x1="480.06" y1="111.76" x2="482.6" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="OVP_IN_14" gate="G$1" pin="PP"/>
<pinref part="R23" gate="G$1" pin="1"/>
<wire x1="480.06" y1="134.62" x2="482.6" y2="134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="OVP_IN_9" gate="G$1" pin="PP"/>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="480.06" y1="157.48" x2="482.6" y2="157.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="OVP_IN_10" gate="G$1" pin="PP"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="480.06" y1="177.8" x2="482.6" y2="177.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="OVP_IN_11" gate="G$1" pin="PP"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="480.06" y1="200.66" x2="482.6" y2="200.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="OVP_IN_12" gate="G$1" pin="PP"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="480.06" y1="223.52" x2="482.6" y2="223.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="40.64" y1="101.6" x2="40.64" y2="104.14" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="1"/>
<wire x1="40.64" y1="104.14" x2="43.18" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="13"/>
<wire x1="96.52" y1="114.3" x2="40.64" y2="114.3" width="0.1524" layer="91"/>
<wire x1="40.64" y1="114.3" x2="40.64" y2="104.14" width="0.1524" layer="91"/>
<junction x="40.64" y="104.14"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="2"/>
<wire x1="53.34" y1="104.14" x2="55.88" y2="104.14" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="55.88" y1="104.14" x2="55.88" y2="101.6" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="14"/>
<wire x1="96.52" y1="111.76" x2="55.88" y2="111.76" width="0.1524" layer="91"/>
<wire x1="55.88" y1="111.76" x2="55.88" y2="104.14" width="0.1524" layer="91"/>
<junction x="55.88" y="104.14"/>
</segment>
</net>
<net name="PS_I2C_SDA" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="34"/>
<wire x1="152.4" y1="48.26" x2="152.4" y2="30.48" width="0.1524" layer="91"/>
<label x="152.4" y="33.02" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="4"/>
<pinref part="R41" gate="G$1" pin="1"/>
<wire x1="58.42" y1="393.7" x2="43.18" y2="393.7" width="0.1524" layer="91"/>
<wire x1="43.18" y1="393.7" x2="43.18" y2="421.64" width="0.1524" layer="91"/>
<wire x1="22.86" y1="388.62" x2="22.86" y2="391.16" width="0.1524" layer="91"/>
<wire x1="22.86" y1="391.16" x2="25.4" y2="393.7" width="0.1524" layer="91"/>
<wire x1="25.4" y1="393.7" x2="43.18" y2="393.7" width="0.1524" layer="91"/>
<junction x="43.18" y="393.7"/>
<label x="25.4" y="393.7" size="1.27" layer="95"/>
</segment>
</net>
<net name="PS_I2C_SCL" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="35"/>
<wire x1="154.94" y1="48.26" x2="154.94" y2="30.48" width="0.1524" layer="91"/>
<label x="154.94" y="33.02" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="5"/>
<pinref part="R43" gate="G$1" pin="1"/>
<wire x1="83.82" y1="393.7" x2="104.14" y2="393.7" width="0.1524" layer="91"/>
<wire x1="104.14" y1="393.7" x2="104.14" y2="421.64" width="0.1524" layer="91"/>
<wire x1="121.92" y1="388.62" x2="121.92" y2="391.16" width="0.1524" layer="91"/>
<wire x1="121.92" y1="391.16" x2="119.38" y2="393.7" width="0.1524" layer="91"/>
<wire x1="119.38" y1="393.7" x2="104.14" y2="393.7" width="0.1524" layer="91"/>
<junction x="104.14" y="393.7"/>
<label x="109.22" y="393.7" size="1.27" layer="95"/>
</segment>
</net>
<net name="COM_TXD" class="0">
<segment>
<pinref part="J21" gate="-2" pin="1"/>
<wire x1="350.52" y1="398.78" x2="327.66" y2="398.78" width="0.1524" layer="91"/>
<label x="330.2" y="398.78" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="39"/>
<wire x1="165.1" y1="48.26" x2="165.1" y2="30.48" width="0.1524" layer="91"/>
<label x="165.1" y="35.56" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="R36" gate="G$1" pin="2"/>
<wire x1="256.54" y1="419.1" x2="274.32" y2="419.1" width="0.1524" layer="91"/>
<label x="274.32" y="419.1" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="COM_RXD" class="0">
<segment>
<pinref part="J21" gate="-3" pin="1"/>
<wire x1="350.52" y1="396.24" x2="327.66" y2="396.24" width="0.1524" layer="91"/>
<label x="330.2" y="396.24" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="40"/>
<wire x1="167.64" y1="48.26" x2="167.64" y2="30.48" width="0.1524" layer="91"/>
<label x="167.64" y="35.56" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="R35" gate="G$1" pin="2"/>
<wire x1="261.62" y1="434.34" x2="274.32" y2="434.34" width="0.1524" layer="91"/>
<label x="274.32" y="434.34" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="R34" gate="G$1" pin="2"/>
<pinref part="U2" gate="G$1" pin="6"/>
<wire x1="215.9" y1="424.18" x2="208.28" y2="424.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="R32" gate="G$1" pin="2"/>
<pinref part="U2" gate="G$1" pin="7"/>
<wire x1="208.28" y1="429.26" x2="213.36" y2="429.26" width="0.1524" layer="91"/>
<pinref part="R33" gate="G$1" pin="2"/>
<wire x1="213.36" y1="429.26" x2="215.9" y2="429.26" width="0.1524" layer="91"/>
<wire x1="208.28" y1="426.72" x2="213.36" y2="426.72" width="0.1524" layer="91"/>
<wire x1="213.36" y1="426.72" x2="213.36" y2="429.26" width="0.1524" layer="91"/>
<junction x="213.36" y="429.26"/>
</segment>
</net>
<net name="IN_A" class="0">
<segment>
<pinref part="R33" gate="G$1" pin="1"/>
<wire x1="198.12" y1="426.72" x2="193.04" y2="426.72" width="0.1524" layer="91"/>
<wire x1="193.04" y1="426.72" x2="193.04" y2="424.18" width="0.1524" layer="91"/>
<pinref part="R34" gate="G$1" pin="1"/>
<wire x1="198.12" y1="424.18" x2="193.04" y2="424.18" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="193.04" y1="424.18" x2="187.96" y2="424.18" width="0.1524" layer="91"/>
<wire x1="187.96" y1="424.18" x2="175.26" y2="424.18" width="0.1524" layer="91"/>
<wire x1="187.96" y1="421.64" x2="187.96" y2="424.18" width="0.1524" layer="91"/>
<junction x="187.96" y="424.18"/>
<label x="175.26" y="424.18" size="1.27" layer="95" rot="R180" xref="yes"/>
<junction x="193.04" y="424.18"/>
</segment>
</net>
<net name="IN_B" class="0">
<segment>
<pinref part="R32" gate="G$1" pin="1"/>
<wire x1="198.12" y1="429.26" x2="180.34" y2="429.26" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="180.34" y1="429.26" x2="175.26" y2="429.26" width="0.1524" layer="91"/>
<wire x1="180.34" y1="421.64" x2="180.34" y2="429.26" width="0.1524" layer="91"/>
<junction x="180.34" y="429.26"/>
<label x="175.26" y="429.26" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="1"/>
<pinref part="R35" gate="G$1" pin="1"/>
<wire x1="241.3" y1="434.34" x2="251.46" y2="434.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="4"/>
<pinref part="R36" gate="G$1" pin="1"/>
<wire x1="241.3" y1="419.1" x2="246.38" y2="419.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RS485_INT" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="3"/>
<wire x1="241.3" y1="424.18" x2="243.84" y2="424.18" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="2"/>
<wire x1="243.84" y1="424.18" x2="248.92" y2="424.18" width="0.1524" layer="91"/>
<wire x1="248.92" y1="424.18" x2="274.32" y2="424.18" width="0.1524" layer="91"/>
<wire x1="241.3" y1="429.26" x2="243.84" y2="429.26" width="0.1524" layer="91"/>
<wire x1="243.84" y1="429.26" x2="243.84" y2="424.18" width="0.1524" layer="91"/>
<junction x="243.84" y="424.18"/>
<label x="274.32" y="424.18" size="1.27" layer="95" xref="yes"/>
<pinref part="R75" gate="G$1" pin="1"/>
<wire x1="251.46" y1="429.26" x2="248.92" y2="429.26" width="0.1524" layer="91"/>
<wire x1="248.92" y1="429.26" x2="248.92" y2="426.72" width="0.1524" layer="91"/>
<pinref part="R76" gate="G$1" pin="1"/>
<wire x1="248.92" y1="426.72" x2="248.92" y2="424.18" width="0.1524" layer="91"/>
<wire x1="251.46" y1="426.72" x2="248.92" y2="426.72" width="0.1524" layer="91"/>
<junction x="248.92" y="426.72"/>
<junction x="248.92" y="424.18"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="36"/>
<wire x1="157.48" y1="48.26" x2="157.48" y2="33.02" width="0.1524" layer="91"/>
<label x="157.48" y="33.02" size="1.27" layer="95" ratio="15" rot="R270" xref="yes"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<pinref part="LS1" gate="G$1" pin="1"/>
<wire x1="327.66" y1="492.76" x2="327.66" y2="487.68" width="0.1524" layer="91"/>
<wire x1="327.66" y1="487.68" x2="332.74" y2="487.68" width="0.1524" layer="91"/>
<pinref part="R39" gate="G$1" pin="2"/>
<wire x1="332.74" y1="487.68" x2="332.74" y2="490.22" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$1" pin="D"/>
<wire x1="332.74" y1="487.68" x2="332.74" y2="485.14" width="0.1524" layer="91"/>
<junction x="332.74" y="487.68"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="G"/>
<wire x1="337.82" y1="477.52" x2="347.98" y2="477.52" width="0.1524" layer="91"/>
<pinref part="R38" gate="G$1" pin="1"/>
<wire x1="347.98" y1="477.52" x2="350.52" y2="477.52" width="0.1524" layer="91"/>
<wire x1="345.44" y1="472.44" x2="347.98" y2="472.44" width="0.1524" layer="91"/>
<wire x1="347.98" y1="472.44" x2="347.98" y2="477.52" width="0.1524" layer="91"/>
<pinref part="R37" gate="G$1" pin="2"/>
<junction x="347.98" y="477.52"/>
</segment>
</net>
<net name="P1.1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="18"/>
<wire x1="96.52" y1="101.6" x2="83.82" y2="101.6" width="0.1524" layer="91"/>
<label x="83.82" y="101.6" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R37" gate="G$1" pin="1"/>
<wire x1="360.68" y1="477.52" x2="363.22" y2="477.52" width="0.1524" layer="91"/>
<label x="363.22" y="477.52" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PS_I2C_ACC_SDA" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="1"/>
<pinref part="R40" gate="G$1" pin="1"/>
<wire x1="58.42" y1="408.94" x2="53.34" y2="408.94" width="0.1524" layer="91"/>
<wire x1="53.34" y1="408.94" x2="53.34" y2="421.64" width="0.1524" layer="91"/>
<wire x1="53.34" y1="408.94" x2="35.56" y2="408.94" width="0.1524" layer="91"/>
<junction x="53.34" y="408.94"/>
<label x="35.56" y="408.94" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J22" gate="-2" pin="1"/>
<wire x1="350.52" y1="381" x2="340.36" y2="381" width="0.1524" layer="91"/>
<label x="340.36" y="381" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="PS_I2C_ACC_SCL" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="8"/>
<pinref part="R42" gate="G$1" pin="1"/>
<wire x1="83.82" y1="408.94" x2="93.98" y2="408.94" width="0.1524" layer="91"/>
<wire x1="93.98" y1="408.94" x2="93.98" y2="421.64" width="0.1524" layer="91"/>
<wire x1="93.98" y1="408.94" x2="114.3" y2="408.94" width="0.1524" layer="91"/>
<junction x="93.98" y="408.94"/>
<label x="114.3" y="408.94" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="J22" gate="-3" pin="1"/>
<wire x1="350.52" y1="378.46" x2="340.36" y2="378.46" width="0.1524" layer="91"/>
<label x="340.36" y="378.46" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="PS_I2C_VOUT" class="0">
<segment>
<pinref part="R40" gate="G$1" pin="2"/>
<wire x1="53.34" y1="431.8" x2="53.34" y2="434.34" width="0.1524" layer="91"/>
<pinref part="R42" gate="G$1" pin="2"/>
<wire x1="53.34" y1="434.34" x2="93.98" y2="434.34" width="0.1524" layer="91"/>
<wire x1="93.98" y1="434.34" x2="93.98" y2="431.8" width="0.1524" layer="91"/>
<wire x1="93.98" y1="434.34" x2="99.06" y2="434.34" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="99.06" y1="434.34" x2="109.22" y2="434.34" width="0.1524" layer="91"/>
<wire x1="109.22" y1="431.8" x2="109.22" y2="434.34" width="0.1524" layer="91"/>
<junction x="93.98" y="434.34"/>
<junction x="109.22" y="434.34"/>
<pinref part="U3" gate="G$1" pin="7"/>
<wire x1="83.82" y1="403.86" x2="99.06" y2="403.86" width="0.1524" layer="91"/>
<wire x1="99.06" y1="403.86" x2="99.06" y2="434.34" width="0.1524" layer="91"/>
<junction x="99.06" y="434.34"/>
<pinref part="J23" gate="G$1" pin="C"/>
<pinref part="R45" gate="G$1" pin="1"/>
<wire x1="114.3" y1="434.34" x2="116.84" y2="434.34" width="0.1524" layer="91"/>
<wire x1="116.84" y1="434.34" x2="119.38" y2="434.34" width="0.1524" layer="91"/>
<wire x1="119.38" y1="434.34" x2="139.7" y2="434.34" width="0.1524" layer="91"/>
<wire x1="121.92" y1="439.42" x2="116.84" y2="439.42" width="0.1524" layer="91"/>
<wire x1="116.84" y1="439.42" x2="116.84" y2="434.34" width="0.1524" layer="91"/>
<pinref part="R44" gate="G$1" pin="1"/>
<wire x1="121.92" y1="436.88" x2="119.38" y2="436.88" width="0.1524" layer="91"/>
<wire x1="119.38" y1="436.88" x2="119.38" y2="434.34" width="0.1524" layer="91"/>
<junction x="119.38" y="434.34"/>
<junction x="116.84" y="434.34"/>
<wire x1="109.22" y1="434.34" x2="114.3" y2="434.34" width="0.1524" layer="91"/>
<wire x1="114.3" y1="434.34" x2="114.3" y2="444.5" width="0.1524" layer="91"/>
<wire x1="114.3" y1="444.5" x2="111.76" y2="444.5" width="0.1524" layer="91"/>
<junction x="114.3" y="434.34"/>
<label x="111.76" y="444.5" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J22" gate="-1" pin="1"/>
<wire x1="350.52" y1="383.54" x2="345.44" y2="383.54" width="0.1524" layer="91"/>
<label x="340.36" y="383.54" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="C36" gate="G$1" pin="2"/>
<wire x1="345.44" y1="383.54" x2="340.36" y2="383.54" width="0.1524" layer="91"/>
<wire x1="342.9" y1="388.62" x2="345.44" y2="388.62" width="0.1524" layer="91"/>
<wire x1="345.44" y1="388.62" x2="345.44" y2="383.54" width="0.1524" layer="91"/>
<junction x="345.44" y="383.54"/>
</segment>
</net>
<net name="N$68" class="0">
<segment>
<pinref part="J23" gate="G$1" pin="2"/>
<wire x1="139.7" y1="431.8" x2="134.62" y2="431.8" width="0.1524" layer="91"/>
<pinref part="R44" gate="G$1" pin="2"/>
<wire x1="134.62" y1="431.8" x2="127" y2="431.8" width="0.1524" layer="91"/>
<wire x1="132.08" y1="436.88" x2="134.62" y2="436.88" width="0.1524" layer="91"/>
<wire x1="134.62" y1="436.88" x2="134.62" y2="431.8" width="0.1524" layer="91"/>
<junction x="134.62" y="431.8"/>
<pinref part="TP4" gate="G$1" pin="PP"/>
</segment>
</net>
<net name="AUX_I2C_SCL" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="70"/>
<wire x1="228.6" y1="132.08" x2="251.46" y2="132.08" width="0.1524" layer="91"/>
<label x="231.14" y="132.08" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="5"/>
<pinref part="R53" gate="G$1" pin="1"/>
<wire x1="81.28" y1="307.34" x2="101.6" y2="307.34" width="0.1524" layer="91"/>
<wire x1="101.6" y1="307.34" x2="101.6" y2="332.74" width="0.1524" layer="91"/>
<wire x1="101.6" y1="307.34" x2="109.22" y2="307.34" width="0.1524" layer="91"/>
<label x="109.22" y="307.34" size="1.27" layer="95" xref="yes"/>
<junction x="101.6" y="307.34"/>
</segment>
</net>
<net name="AUX_I2C_SDA" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="69"/>
<wire x1="228.6" y1="129.54" x2="251.46" y2="129.54" width="0.1524" layer="91"/>
<label x="231.14" y="129.54" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="R51" gate="G$1" pin="1"/>
<wire x1="40.64" y1="332.74" x2="40.64" y2="307.34" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="4"/>
<wire x1="40.64" y1="307.34" x2="55.88" y2="307.34" width="0.1524" layer="91"/>
<wire x1="40.64" y1="307.34" x2="33.02" y2="307.34" width="0.1524" layer="91"/>
<label x="33.02" y="307.34" size="1.27" layer="95" rot="R180" xref="yes"/>
<junction x="40.64" y="307.34"/>
</segment>
</net>
<net name="AUX_I2C_VOUT" class="0">
<segment>
<pinref part="R50" gate="G$1" pin="2"/>
<wire x1="50.8" y1="342.9" x2="50.8" y2="347.98" width="0.1524" layer="91"/>
<pinref part="R52" gate="G$1" pin="2"/>
<wire x1="50.8" y1="347.98" x2="91.44" y2="347.98" width="0.1524" layer="91"/>
<wire x1="91.44" y1="347.98" x2="91.44" y2="342.9" width="0.1524" layer="91"/>
<wire x1="91.44" y1="347.98" x2="96.52" y2="347.98" width="0.1524" layer="91"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="96.52" y1="347.98" x2="106.68" y2="347.98" width="0.1524" layer="91"/>
<wire x1="106.68" y1="345.44" x2="106.68" y2="347.98" width="0.1524" layer="91"/>
<junction x="91.44" y="347.98"/>
<junction x="106.68" y="347.98"/>
<pinref part="U4" gate="G$1" pin="7"/>
<wire x1="81.28" y1="317.5" x2="96.52" y2="317.5" width="0.1524" layer="91"/>
<wire x1="96.52" y1="317.5" x2="96.52" y2="347.98" width="0.1524" layer="91"/>
<junction x="96.52" y="347.98"/>
<pinref part="J24" gate="G$1" pin="C"/>
<wire x1="106.68" y1="347.98" x2="111.76" y2="347.98" width="0.1524" layer="91"/>
<pinref part="R55" gate="G$1" pin="1"/>
<wire x1="111.76" y1="347.98" x2="114.3" y2="347.98" width="0.1524" layer="91"/>
<wire x1="114.3" y1="347.98" x2="116.84" y2="347.98" width="0.1524" layer="91"/>
<wire x1="116.84" y1="347.98" x2="137.16" y2="347.98" width="0.1524" layer="91"/>
<wire x1="119.38" y1="353.06" x2="114.3" y2="353.06" width="0.1524" layer="91"/>
<wire x1="114.3" y1="353.06" x2="114.3" y2="347.98" width="0.1524" layer="91"/>
<pinref part="R54" gate="G$1" pin="1"/>
<wire x1="119.38" y1="350.52" x2="116.84" y2="350.52" width="0.1524" layer="91"/>
<wire x1="116.84" y1="350.52" x2="116.84" y2="347.98" width="0.1524" layer="91"/>
<junction x="116.84" y="347.98"/>
<junction x="114.3" y="347.98"/>
<wire x1="111.76" y1="347.98" x2="111.76" y2="358.14" width="0.1524" layer="91"/>
<wire x1="111.76" y1="358.14" x2="106.68" y2="358.14" width="0.1524" layer="91"/>
<junction x="111.76" y="347.98"/>
<label x="106.68" y="358.14" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J25" gate="-1" pin="1"/>
<wire x1="350.52" y1="365.76" x2="345.44" y2="365.76" width="0.1524" layer="91"/>
<label x="340.36" y="365.76" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="C37" gate="G$1" pin="2"/>
<wire x1="345.44" y1="365.76" x2="340.36" y2="365.76" width="0.1524" layer="91"/>
<wire x1="342.9" y1="370.84" x2="345.44" y2="370.84" width="0.1524" layer="91"/>
<wire x1="345.44" y1="370.84" x2="345.44" y2="365.76" width="0.1524" layer="91"/>
<junction x="345.44" y="365.76"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<pinref part="J24" gate="G$1" pin="2"/>
<wire x1="137.16" y1="345.44" x2="132.08" y2="345.44" width="0.1524" layer="91"/>
<pinref part="R54" gate="G$1" pin="2"/>
<wire x1="132.08" y1="345.44" x2="124.46" y2="345.44" width="0.1524" layer="91"/>
<wire x1="129.54" y1="350.52" x2="132.08" y2="350.52" width="0.1524" layer="91"/>
<wire x1="132.08" y1="350.52" x2="132.08" y2="345.44" width="0.1524" layer="91"/>
<junction x="132.08" y="345.44"/>
<pinref part="TP6" gate="G$1" pin="PP"/>
</segment>
</net>
<net name="AUX_I2C_ACC_SDA" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="1"/>
<pinref part="R50" gate="G$1" pin="1"/>
<wire x1="55.88" y1="322.58" x2="50.8" y2="322.58" width="0.1524" layer="91"/>
<wire x1="50.8" y1="322.58" x2="50.8" y2="332.74" width="0.1524" layer="91"/>
<wire x1="50.8" y1="322.58" x2="33.02" y2="322.58" width="0.1524" layer="91"/>
<label x="33.02" y="322.58" size="1.27" layer="95" rot="R180" xref="yes"/>
<junction x="50.8" y="322.58"/>
</segment>
<segment>
<pinref part="J25" gate="-2" pin="1"/>
<wire x1="350.52" y1="363.22" x2="340.36" y2="363.22" width="0.1524" layer="91"/>
<label x="340.36" y="363.22" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="AUX_I2C_ACC_SCL" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="8"/>
<pinref part="R52" gate="G$1" pin="1"/>
<wire x1="81.28" y1="322.58" x2="91.44" y2="322.58" width="0.1524" layer="91"/>
<wire x1="91.44" y1="322.58" x2="91.44" y2="332.74" width="0.1524" layer="91"/>
<wire x1="91.44" y1="322.58" x2="109.22" y2="322.58" width="0.1524" layer="91"/>
<label x="109.22" y="322.58" size="1.27" layer="95" xref="yes"/>
<junction x="91.44" y="322.58"/>
</segment>
<segment>
<pinref part="J25" gate="-3" pin="1"/>
<wire x1="350.52" y1="360.68" x2="340.36" y2="360.68" width="0.1524" layer="91"/>
<label x="340.36" y="360.68" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="PWR_IN" class="0">
<segment>
<pinref part="J26" gate="P" pin="1"/>
<wire x1="22.86" y1="500.38" x2="25.4" y2="500.38" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="2"/>
<label x="22.86" y="505.46" size="1.27" layer="95" ratio="15" rot="R180" xref="yes"/>
<wire x1="25.4" y1="500.38" x2="27.94" y2="500.38" width="0.1524" layer="91"/>
<wire x1="25.4" y1="500.38" x2="25.4" y2="505.46" width="0.1524" layer="91"/>
<wire x1="25.4" y1="505.46" x2="22.86" y2="505.46" width="0.1524" layer="91"/>
<junction x="25.4" y="500.38"/>
</segment>
<segment>
<pinref part="R83" gate="G$1" pin="2"/>
<wire x1="320.04" y1="45.72" x2="304.8" y2="45.72" width="0.1524" layer="91"/>
<label x="304.8" y="45.72" size="1.27" layer="95" ratio="15" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="D26" gate="G$1" pin="2"/>
<label x="172.72" y="500.38" size="1.27" layer="95" ratio="15" rot="R180" xref="yes"/>
<wire x1="177.8" y1="500.38" x2="172.72" y2="500.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="1"/>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="38.1" y1="500.38" x2="40.64" y2="500.38" width="0.1524" layer="91"/>
<pinref part="R56" gate="G$1" pin="1"/>
<wire x1="40.64" y1="500.38" x2="43.18" y2="500.38" width="0.1524" layer="91"/>
<wire x1="43.18" y1="505.46" x2="40.64" y2="505.46" width="0.1524" layer="91"/>
<wire x1="40.64" y1="505.46" x2="40.64" y2="500.38" width="0.1524" layer="91"/>
<junction x="40.64" y="500.38"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="2"/>
<pinref part="U5" gate="G$1" pin="3"/>
<wire x1="53.34" y1="500.38" x2="55.88" y2="500.38" width="0.1524" layer="91"/>
<pinref part="R56" gate="G$1" pin="2"/>
<wire x1="55.88" y1="500.38" x2="58.42" y2="500.38" width="0.1524" layer="91"/>
<wire x1="58.42" y1="500.38" x2="63.5" y2="500.38" width="0.1524" layer="91"/>
<wire x1="63.5" y1="500.38" x2="68.58" y2="500.38" width="0.1524" layer="91"/>
<wire x1="53.34" y1="505.46" x2="55.88" y2="505.46" width="0.1524" layer="91"/>
<wire x1="55.88" y1="505.46" x2="55.88" y2="500.38" width="0.1524" layer="91"/>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="58.42" y1="497.84" x2="58.42" y2="500.38" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="63.5" y1="497.84" x2="63.5" y2="500.38" width="0.1524" layer="91"/>
<junction x="58.42" y="500.38"/>
<junction x="63.5" y="500.38"/>
<junction x="55.88" y="500.38"/>
</segment>
</net>
<net name="N$67" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="2"/>
<pinref part="R58" gate="G$1" pin="2"/>
<wire x1="88.9" y1="500.38" x2="96.52" y2="500.38" width="0.1524" layer="91"/>
<wire x1="96.52" y1="500.38" x2="96.52" y2="497.84" width="0.1524" layer="91"/>
<pinref part="L3" gate="G$1" pin="1"/>
<wire x1="96.52" y1="500.38" x2="101.6" y2="500.38" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="1"/>
<wire x1="101.6" y1="500.38" x2="116.84" y2="500.38" width="0.1524" layer="91"/>
<wire x1="116.84" y1="500.38" x2="121.92" y2="500.38" width="0.1524" layer="91"/>
<wire x1="121.92" y1="500.38" x2="124.46" y2="500.38" width="0.1524" layer="91"/>
<wire x1="124.46" y1="500.38" x2="127" y2="500.38" width="0.1524" layer="91"/>
<wire x1="121.92" y1="497.84" x2="121.92" y2="500.38" width="0.1524" layer="91"/>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="116.84" y1="497.84" x2="116.84" y2="500.38" width="0.1524" layer="91"/>
<pinref part="R57" gate="G$1" pin="1"/>
<wire x1="127" y1="505.46" x2="124.46" y2="505.46" width="0.1524" layer="91"/>
<wire x1="124.46" y1="505.46" x2="124.46" y2="500.38" width="0.1524" layer="91"/>
<junction x="96.52" y="500.38"/>
<pinref part="D4" gate="G$1" pin="-"/>
<wire x1="101.6" y1="492.76" x2="101.6" y2="500.38" width="0.1524" layer="91"/>
<junction x="101.6" y="500.38"/>
<junction x="116.84" y="500.38"/>
<junction x="121.92" y="500.38"/>
<junction x="124.46" y="500.38"/>
</segment>
</net>
<net name="N$69" class="0">
<segment>
<pinref part="R59" gate="G$1" pin="1"/>
<pinref part="U5" gate="G$1" pin="1"/>
<wire x1="76.2" y1="482.6" x2="78.74" y2="482.6" width="0.1524" layer="91"/>
<wire x1="78.74" y1="482.6" x2="78.74" y2="492.76" width="0.1524" layer="91"/>
<pinref part="R60" gate="G$1" pin="2"/>
<pinref part="R58" gate="G$1" pin="1"/>
<wire x1="96.52" y1="487.68" x2="96.52" y2="485.14" width="0.1524" layer="91"/>
<wire x1="96.52" y1="485.14" x2="99.06" y2="485.14" width="0.1524" layer="91"/>
<wire x1="78.74" y1="482.6" x2="96.52" y2="482.6" width="0.1524" layer="91"/>
<wire x1="96.52" y1="482.6" x2="96.52" y2="485.14" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="E"/>
<wire x1="99.06" y1="472.44" x2="96.52" y2="472.44" width="0.1524" layer="91"/>
<wire x1="96.52" y1="472.44" x2="96.52" y2="482.6" width="0.1524" layer="91"/>
<junction x="78.74" y="482.6"/>
<junction x="96.52" y="485.14"/>
<junction x="96.52" y="482.6"/>
</segment>
</net>
<net name="N$72" class="0">
<segment>
<pinref part="R60" gate="G$1" pin="1"/>
<pinref part="D4" gate="G$1" pin="+"/>
<wire x1="109.22" y1="485.14" x2="111.76" y2="485.14" width="0.1524" layer="91"/>
<wire x1="111.76" y1="485.14" x2="111.76" y2="492.76" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="B"/>
<wire x1="104.14" y1="477.52" x2="104.14" y2="480.06" width="0.1524" layer="91"/>
<wire x1="104.14" y1="480.06" x2="111.76" y2="480.06" width="0.1524" layer="91"/>
<pinref part="C32" gate="G$1" pin="1"/>
<wire x1="111.76" y1="480.06" x2="127" y2="480.06" width="0.1524" layer="91"/>
<wire x1="111.76" y1="485.14" x2="111.76" y2="480.06" width="0.1524" layer="91"/>
<junction x="111.76" y="480.06"/>
<junction x="111.76" y="485.14"/>
</segment>
</net>
<net name="FAULT_LED1" class="0">
<segment>
<pinref part="R61" gate="G$1" pin="2"/>
<wire x1="302.26" y1="193.04" x2="320.04" y2="193.04" width="0.1524" layer="91"/>
<label x="304.8" y="193.04" size="1.27" layer="95" ratio="15"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="61"/>
<wire x1="228.6" y1="109.22" x2="259.08" y2="109.22" width="0.1524" layer="91"/>
<wire x1="259.08" y1="109.22" x2="261.62" y2="106.68" width="0.1524" layer="91"/>
<label x="231.14" y="109.22" size="1.27" layer="95" ratio="15"/>
</segment>
</net>
<net name="FAULT_LED2" class="0">
<segment>
<pinref part="R62" gate="G$1" pin="2"/>
<wire x1="302.26" y1="185.42" x2="320.04" y2="185.42" width="0.1524" layer="91"/>
<label x="304.8" y="185.42" size="1.27" layer="95" ratio="15"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="59"/>
<wire x1="228.6" y1="104.14" x2="259.08" y2="104.14" width="0.1524" layer="91"/>
<wire x1="259.08" y1="104.14" x2="261.62" y2="101.6" width="0.1524" layer="91"/>
<label x="231.14" y="104.14" size="1.27" layer="95" ratio="15"/>
</segment>
</net>
<net name="FAULT_LED3" class="0">
<segment>
<pinref part="R63" gate="G$1" pin="2"/>
<wire x1="302.26" y1="177.8" x2="320.04" y2="177.8" width="0.1524" layer="91"/>
<label x="304.8" y="177.8" size="1.27" layer="95" ratio="15"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="56"/>
<wire x1="228.6" y1="96.52" x2="259.08" y2="96.52" width="0.1524" layer="91"/>
<wire x1="259.08" y1="96.52" x2="261.62" y2="93.98" width="0.1524" layer="91"/>
<label x="231.14" y="96.52" size="1.27" layer="95" ratio="15"/>
</segment>
</net>
<net name="FAULT_LED4" class="0">
<segment>
<pinref part="R64" gate="G$1" pin="2"/>
<wire x1="302.26" y1="170.18" x2="320.04" y2="170.18" width="0.1524" layer="91"/>
<label x="304.8" y="170.18" size="1.27" layer="95" ratio="15"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="50"/>
<wire x1="193.04" y1="48.26" x2="193.04" y2="15.24" width="0.1524" layer="91"/>
<wire x1="193.04" y1="15.24" x2="190.5" y2="12.7" width="0.1524" layer="91"/>
<label x="193.04" y="26.924" size="1.27" layer="95" ratio="15" rot="R90"/>
</segment>
</net>
<net name="FAULT_LED5" class="0">
<segment>
<pinref part="R65" gate="G$1" pin="2"/>
<wire x1="302.26" y1="162.56" x2="320.04" y2="162.56" width="0.1524" layer="91"/>
<label x="304.8" y="162.56" size="1.27" layer="95" ratio="15"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="48"/>
<wire x1="187.96" y1="48.26" x2="187.96" y2="15.24" width="0.1524" layer="91"/>
<wire x1="187.96" y1="15.24" x2="185.42" y2="12.7" width="0.1524" layer="91"/>
<label x="187.96" y="27.178" size="1.27" layer="95" ratio="15" rot="R90"/>
</segment>
</net>
<net name="FAULT_LED6" class="0">
<segment>
<pinref part="R66" gate="G$1" pin="2"/>
<wire x1="302.26" y1="154.94" x2="320.04" y2="154.94" width="0.1524" layer="91"/>
<label x="304.8" y="154.94" size="1.27" layer="95" ratio="15"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="46"/>
<wire x1="182.88" y1="48.26" x2="182.88" y2="15.24" width="0.1524" layer="91"/>
<wire x1="182.88" y1="15.24" x2="180.34" y2="12.7" width="0.1524" layer="91"/>
<label x="182.88" y="27.178" size="1.27" layer="95" ratio="15" rot="R90"/>
</segment>
</net>
<net name="FAULT_LED7" class="0">
<segment>
<pinref part="R67" gate="G$1" pin="2"/>
<wire x1="302.26" y1="147.32" x2="320.04" y2="147.32" width="0.1524" layer="91"/>
<label x="304.8" y="147.32" size="1.27" layer="95" ratio="15"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="44"/>
<wire x1="177.8" y1="48.26" x2="177.8" y2="15.24" width="0.1524" layer="91"/>
<wire x1="177.8" y1="15.24" x2="175.26" y2="12.7" width="0.1524" layer="91"/>
<label x="177.8" y="27.178" size="1.27" layer="95" ratio="15" rot="R90"/>
</segment>
</net>
<net name="FAULT_LED8" class="0">
<segment>
<pinref part="R68" gate="G$1" pin="2"/>
<wire x1="302.26" y1="139.7" x2="320.04" y2="139.7" width="0.1524" layer="91"/>
<label x="304.8" y="139.7" size="1.27" layer="95" ratio="15"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="42"/>
<wire x1="172.72" y1="48.26" x2="172.72" y2="15.24" width="0.1524" layer="91"/>
<wire x1="172.72" y1="15.24" x2="170.18" y2="12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="FAULT_LED9" class="0">
<segment>
<pinref part="R69" gate="G$1" pin="2"/>
<wire x1="302.26" y1="132.08" x2="320.04" y2="132.08" width="0.1524" layer="91"/>
<label x="304.8" y="132.08" size="1.27" layer="95" ratio="15"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="33"/>
<wire x1="149.86" y1="48.26" x2="149.86" y2="15.24" width="0.1524" layer="91"/>
<wire x1="149.86" y1="15.24" x2="152.4" y2="12.7" width="0.1524" layer="91"/>
<label x="149.86" y="26.924" size="1.27" layer="95" ratio="15" rot="R90"/>
</segment>
</net>
<net name="FAULT_LED10" class="0">
<segment>
<pinref part="R70" gate="G$1" pin="2"/>
<wire x1="302.26" y1="124.46" x2="320.04" y2="124.46" width="0.1524" layer="91"/>
<label x="304.8" y="124.46" size="1.27" layer="95" ratio="15"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="31"/>
<wire x1="144.78" y1="48.26" x2="144.78" y2="15.24" width="0.1524" layer="91"/>
<wire x1="144.78" y1="15.24" x2="147.32" y2="12.7" width="0.1524" layer="91"/>
<label x="144.78" y="26.162" size="1.27" layer="95" ratio="15" rot="R90"/>
</segment>
</net>
<net name="FAULT_LED11" class="0">
<segment>
<pinref part="R71" gate="G$1" pin="2"/>
<wire x1="302.26" y1="116.84" x2="320.04" y2="116.84" width="0.1524" layer="91"/>
<label x="304.8" y="116.84" size="1.27" layer="95" ratio="15"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="29"/>
<wire x1="139.7" y1="48.26" x2="139.7" y2="15.24" width="0.1524" layer="91"/>
<wire x1="139.7" y1="15.24" x2="142.24" y2="12.7" width="0.1524" layer="91"/>
<label x="139.7" y="26.162" size="1.27" layer="95" ratio="15" rot="R90"/>
</segment>
</net>
<net name="FAULT_LED12" class="0">
<segment>
<pinref part="R72" gate="G$1" pin="2"/>
<wire x1="302.26" y1="109.22" x2="320.04" y2="109.22" width="0.1524" layer="91"/>
<label x="304.8" y="109.22" size="1.27" layer="95" ratio="15"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="27"/>
<wire x1="134.62" y1="48.26" x2="134.62" y2="15.24" width="0.1524" layer="91"/>
<wire x1="134.62" y1="15.24" x2="137.16" y2="12.7" width="0.1524" layer="91"/>
<label x="134.62" y="26.162" size="1.27" layer="95" ratio="15" rot="R90"/>
</segment>
</net>
<net name="FAULT_LED13" class="0">
<segment>
<pinref part="R73" gate="G$1" pin="2"/>
<wire x1="302.26" y1="101.6" x2="320.04" y2="101.6" width="0.1524" layer="91"/>
<label x="304.8" y="101.6" size="1.27" layer="95" ratio="15"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="25"/>
<wire x1="96.52" y1="83.82" x2="71.12" y2="83.82" width="0.1524" layer="91"/>
<wire x1="71.12" y1="83.82" x2="68.58" y2="81.28" width="0.1524" layer="91"/>
<label x="71.12" y="83.82" size="1.27" layer="95" ratio="15"/>
</segment>
</net>
<net name="FAULT_LED14" class="0">
<segment>
<pinref part="R74" gate="G$1" pin="2"/>
<wire x1="302.26" y1="93.98" x2="320.04" y2="93.98" width="0.1524" layer="91"/>
<label x="304.8" y="93.98" size="1.27" layer="95" ratio="15"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="23"/>
<wire x1="96.52" y1="88.9" x2="71.12" y2="88.9" width="0.1524" layer="91"/>
<wire x1="71.12" y1="88.9" x2="68.58" y2="86.36" width="0.1524" layer="91"/>
<label x="71.12" y="88.9" size="1.27" layer="95" ratio="15"/>
</segment>
</net>
<net name="N$84" class="0">
<segment>
<pinref part="R61" gate="G$1" pin="1"/>
<pinref part="D7" gate="G$1" pin="+"/>
<wire x1="330.2" y1="193.04" x2="335.28" y2="193.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$88" class="0">
<segment>
<pinref part="R62" gate="G$1" pin="1"/>
<pinref part="D8" gate="G$1" pin="+"/>
<wire x1="330.2" y1="185.42" x2="335.28" y2="185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$89" class="0">
<segment>
<pinref part="R63" gate="G$1" pin="1"/>
<pinref part="D9" gate="G$1" pin="+"/>
<wire x1="330.2" y1="177.8" x2="335.28" y2="177.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$90" class="0">
<segment>
<pinref part="R64" gate="G$1" pin="1"/>
<pinref part="D10" gate="G$1" pin="+"/>
<wire x1="330.2" y1="170.18" x2="335.28" y2="170.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$91" class="0">
<segment>
<pinref part="R65" gate="G$1" pin="1"/>
<pinref part="D11" gate="G$1" pin="+"/>
<wire x1="330.2" y1="162.56" x2="335.28" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$92" class="0">
<segment>
<pinref part="R66" gate="G$1" pin="1"/>
<pinref part="D12" gate="G$1" pin="+"/>
<wire x1="330.2" y1="154.94" x2="335.28" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$93" class="0">
<segment>
<pinref part="R67" gate="G$1" pin="1"/>
<pinref part="D13" gate="G$1" pin="+"/>
<wire x1="330.2" y1="147.32" x2="335.28" y2="147.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$94" class="0">
<segment>
<pinref part="R68" gate="G$1" pin="1"/>
<pinref part="D14" gate="G$1" pin="+"/>
<wire x1="330.2" y1="139.7" x2="335.28" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$95" class="0">
<segment>
<pinref part="R69" gate="G$1" pin="1"/>
<pinref part="D15" gate="G$1" pin="+"/>
<wire x1="330.2" y1="132.08" x2="335.28" y2="132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$96" class="0">
<segment>
<pinref part="R70" gate="G$1" pin="1"/>
<pinref part="D16" gate="G$1" pin="+"/>
<wire x1="330.2" y1="124.46" x2="335.28" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$97" class="0">
<segment>
<pinref part="R71" gate="G$1" pin="1"/>
<pinref part="D17" gate="G$1" pin="+"/>
<wire x1="330.2" y1="116.84" x2="335.28" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$98" class="0">
<segment>
<pinref part="R72" gate="G$1" pin="1"/>
<pinref part="D18" gate="G$1" pin="+"/>
<wire x1="330.2" y1="109.22" x2="335.28" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$99" class="0">
<segment>
<pinref part="R73" gate="G$1" pin="1"/>
<pinref part="D19" gate="G$1" pin="+"/>
<wire x1="330.2" y1="101.6" x2="335.28" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$100" class="0">
<segment>
<pinref part="R74" gate="G$1" pin="1"/>
<pinref part="D20" gate="G$1" pin="+"/>
<wire x1="330.2" y1="93.98" x2="335.28" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$103" class="0">
<segment>
<pinref part="TP15" gate="G$1" pin="PP"/>
<pinref part="U1" gate="G$1" pin="17"/>
<wire x1="91.44" y1="104.14" x2="96.52" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$73" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="60"/>
<pinref part="FAULT_CTRL1" gate="G$1" pin="PP"/>
<wire x1="228.6" y1="106.68" x2="236.22" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$74" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="58"/>
<pinref part="FAULT_CTRL2" gate="G$1" pin="PP"/>
<wire x1="228.6" y1="101.6" x2="236.22" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$75" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="55"/>
<pinref part="FAULT_CTRL3" gate="G$1" pin="PP"/>
<wire x1="228.6" y1="93.98" x2="236.22" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$76" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="41"/>
<pinref part="FAULT_CTRL8" gate="G$1" pin="PP"/>
<wire x1="170.18" y1="48.26" x2="170.18" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$77" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="43"/>
<pinref part="FAULT_CTRL7" gate="G$1" pin="PP"/>
<wire x1="175.26" y1="48.26" x2="175.26" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$78" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="45"/>
<pinref part="FAULT_CTRL6" gate="G$1" pin="PP"/>
<wire x1="180.34" y1="48.26" x2="180.34" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$79" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="47"/>
<pinref part="FAULT_CTRL5" gate="G$1" pin="PP"/>
<wire x1="185.42" y1="48.26" x2="185.42" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$80" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="49"/>
<pinref part="FAULT_CTRL4" gate="G$1" pin="PP"/>
<wire x1="190.5" y1="48.26" x2="190.5" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$81" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="32"/>
<pinref part="FAULT_CTRL9" gate="G$1" pin="PP"/>
<wire x1="147.32" y1="48.26" x2="147.32" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$82" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="30"/>
<pinref part="FAULT_CTRL10" gate="G$1" pin="PP"/>
<wire x1="142.24" y1="48.26" x2="142.24" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$83" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="28"/>
<pinref part="FAULT_CTRL11" gate="G$1" pin="PP"/>
<wire x1="137.16" y1="48.26" x2="137.16" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$85" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="26"/>
<pinref part="FAULT_CTRL12" gate="G$1" pin="PP"/>
<wire x1="132.08" y1="48.26" x2="132.08" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$86" class="0">
<segment>
<pinref part="FAULT_CTRL14" gate="G$1" pin="PP"/>
<pinref part="U1" gate="G$1" pin="22"/>
<wire x1="86.36" y1="91.44" x2="96.52" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$87" class="0">
<segment>
<pinref part="FAULT_CTRL13" gate="G$1" pin="PP"/>
<pinref part="U1" gate="G$1" pin="24"/>
<wire x1="86.36" y1="86.36" x2="96.52" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="73"/>
<wire x1="228.6" y1="139.7" x2="256.54" y2="139.7" width="0.1524" layer="91"/>
<label x="231.14" y="139.7" size="1.27" layer="95" ratio="15"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="72"/>
<wire x1="228.6" y1="137.16" x2="256.54" y2="137.16" width="0.1524" layer="91"/>
<label x="231.14" y="137.16" size="1.27" layer="95" ratio="15"/>
</segment>
</net>
<net name="SCLK" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="71"/>
<wire x1="228.6" y1="134.62" x2="256.54" y2="134.62" width="0.1524" layer="91"/>
<label x="231.14" y="134.62" size="1.27" layer="95" ratio="15"/>
</segment>
</net>
<net name="CS" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="74"/>
<wire x1="228.6" y1="142.24" x2="256.54" y2="142.24" width="0.1524" layer="91"/>
<label x="231.14" y="142.24" size="1.27" layer="95" ratio="15"/>
</segment>
</net>
<net name="EXT_TX_LED" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="52"/>
<wire x1="228.6" y1="86.36" x2="243.84" y2="86.36" width="0.1524" layer="91"/>
<pinref part="R78" gate="G$1" pin="1"/>
<wire x1="243.84" y1="86.36" x2="243.84" y2="78.74" width="0.1524" layer="91"/>
<label x="231.14" y="86.36" size="1.27" layer="95" ratio="15"/>
</segment>
</net>
<net name="EXT_RX_LED" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="51"/>
<wire x1="228.6" y1="83.82" x2="236.22" y2="83.82" width="0.1524" layer="91"/>
<pinref part="R77" gate="G$1" pin="1"/>
<wire x1="236.22" y1="83.82" x2="236.22" y2="78.74" width="0.1524" layer="91"/>
<label x="231.14" y="83.82" size="1.27" layer="95" ratio="15"/>
</segment>
</net>
<net name="N$101" class="0">
<segment>
<pinref part="R77" gate="G$1" pin="2"/>
<pinref part="D6" gate="G$1" pin="+"/>
<wire x1="236.22" y1="68.58" x2="236.22" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$102" class="0">
<segment>
<pinref part="R78" gate="G$1" pin="2"/>
<pinref part="D5" gate="G$1" pin="+"/>
<wire x1="243.84" y1="68.58" x2="243.84" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DAC_SCL" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="78"/>
<wire x1="187.96" y1="180.34" x2="187.96" y2="200.66" width="0.1524" layer="91"/>
<label x="187.96" y="182.88" size="1.27" layer="95" ratio="15" rot="R90"/>
</segment>
<segment>
<wire x1="276.86" y1="307.34" x2="276.86" y2="309.88" width="0.1524" layer="91"/>
<wire x1="276.86" y1="309.88" x2="274.32" y2="312.42" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="5"/>
<pinref part="R87" gate="G$1" pin="1"/>
<wire x1="233.68" y1="312.42" x2="254" y2="312.42" width="0.1524" layer="91"/>
<wire x1="254" y1="312.42" x2="254" y2="337.82" width="0.1524" layer="91"/>
<junction x="254" y="312.42"/>
<wire x1="274.32" y1="312.42" x2="254" y2="312.42" width="0.1524" layer="91"/>
<label x="266.7" y="312.42" size="1.27" layer="95" ratio="15"/>
</segment>
</net>
<net name="DAC_SDA" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="77"/>
<wire x1="190.5" y1="180.34" x2="190.5" y2="200.66" width="0.1524" layer="91"/>
<label x="190.5" y="182.88" size="1.27" layer="95" ratio="15" rot="R90"/>
</segment>
<segment>
<wire x1="170.18" y1="307.34" x2="170.18" y2="309.88" width="0.1524" layer="91"/>
<wire x1="170.18" y1="309.88" x2="172.72" y2="312.42" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="4"/>
<pinref part="R85" gate="G$1" pin="1"/>
<wire x1="208.28" y1="312.42" x2="193.04" y2="312.42" width="0.1524" layer="91"/>
<wire x1="193.04" y1="312.42" x2="193.04" y2="337.82" width="0.1524" layer="91"/>
<junction x="193.04" y="312.42"/>
<wire x1="172.72" y1="312.42" x2="193.04" y2="312.42" width="0.1524" layer="91"/>
<label x="172.72" y="312.42" size="1.27" layer="95" ratio="15"/>
</segment>
</net>
<net name="STATUS_LED1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="86"/>
<wire x1="167.64" y1="180.34" x2="167.64" y2="200.66" width="0.1524" layer="91"/>
<wire x1="167.64" y1="200.66" x2="162.56" y2="200.66" width="0.1524" layer="91"/>
<wire x1="162.56" y1="200.66" x2="162.56" y2="208.28" width="0.1524" layer="91"/>
<label x="162.56" y="208.28" size="1.27" layer="95" ratio="15" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="R79" gate="G$1" pin="2"/>
<wire x1="320.04" y1="81.28" x2="304.8" y2="81.28" width="0.1524" layer="91"/>
<label x="304.8" y="81.28" size="1.27" layer="95" ratio="15" rot="R180" xref="yes"/>
</segment>
</net>
<net name="STATUS_LED2" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="85"/>
<wire x1="170.18" y1="180.34" x2="170.18" y2="203.2" width="0.1524" layer="91"/>
<wire x1="170.18" y1="203.2" x2="165.1" y2="203.2" width="0.1524" layer="91"/>
<wire x1="165.1" y1="203.2" x2="165.1" y2="208.28" width="0.1524" layer="91"/>
<label x="165.1" y="208.28" size="1.27" layer="95" ratio="15" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="R80" gate="G$1" pin="2"/>
<wire x1="320.04" y1="73.66" x2="304.8" y2="73.66" width="0.1524" layer="91"/>
<label x="304.8" y="73.66" size="1.27" layer="95" ratio="15" rot="R180" xref="yes"/>
</segment>
</net>
<net name="STATUS_LED3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="84"/>
<wire x1="172.72" y1="180.34" x2="172.72" y2="205.74" width="0.1524" layer="91"/>
<wire x1="172.72" y1="205.74" x2="167.64" y2="205.74" width="0.1524" layer="91"/>
<wire x1="167.64" y1="205.74" x2="167.64" y2="208.28" width="0.1524" layer="91"/>
<label x="167.64" y="208.28" size="1.27" layer="95" ratio="15" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="R81" gate="G$1" pin="2"/>
<wire x1="320.04" y1="66.04" x2="304.8" y2="66.04" width="0.1524" layer="91"/>
<label x="304.8" y="66.04" size="1.27" layer="95" ratio="15" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="R79" gate="G$1" pin="1"/>
<pinref part="D21" gate="G$1" pin="+"/>
<wire x1="330.2" y1="81.28" x2="335.28" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="R80" gate="G$1" pin="1"/>
<pinref part="D22" gate="G$1" pin="+"/>
<wire x1="330.2" y1="73.66" x2="335.28" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<pinref part="R81" gate="G$1" pin="1"/>
<pinref part="D23" gate="G$1" pin="+"/>
<wire x1="330.2" y1="66.04" x2="335.28" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<pinref part="R82" gate="G$1" pin="1"/>
<pinref part="D24" gate="G$1" pin="+"/>
<wire x1="330.2" y1="53.34" x2="335.28" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$104" class="0">
<segment>
<pinref part="R83" gate="G$1" pin="1"/>
<pinref part="D25" gate="G$1" pin="+"/>
<wire x1="330.2" y1="45.72" x2="335.28" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<pinref part="J27" gate="G$1" pin="2"/>
<wire x1="289.56" y1="350.52" x2="284.48" y2="350.52" width="0.1524" layer="91"/>
<pinref part="R88" gate="G$1" pin="2"/>
<wire x1="284.48" y1="350.52" x2="276.86" y2="350.52" width="0.1524" layer="91"/>
<wire x1="281.94" y1="355.6" x2="284.48" y2="355.6" width="0.1524" layer="91"/>
<wire x1="284.48" y1="355.6" x2="284.48" y2="350.52" width="0.1524" layer="91"/>
<junction x="284.48" y="350.52"/>
<pinref part="TP8" gate="G$1" pin="PP"/>
</segment>
</net>
<net name="DAC_I2C_ACC_SDA" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="1"/>
<pinref part="R84" gate="G$1" pin="1"/>
<wire x1="208.28" y1="327.66" x2="203.2" y2="327.66" width="0.1524" layer="91"/>
<wire x1="203.2" y1="327.66" x2="203.2" y2="337.82" width="0.1524" layer="91"/>
<wire x1="203.2" y1="327.66" x2="182.88" y2="327.66" width="0.1524" layer="91"/>
<junction x="203.2" y="327.66"/>
<label x="182.88" y="327.66" size="1.27" layer="95" ratio="15" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J28" gate="-2" pin="1"/>
<wire x1="350.52" y1="345.44" x2="340.36" y2="345.44" width="0.1524" layer="91"/>
<label x="340.36" y="345.44" size="1.27" layer="95" ratio="15" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DAC_I2C_ACC_SCL" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="8"/>
<pinref part="R86" gate="G$1" pin="1"/>
<wire x1="233.68" y1="327.66" x2="243.84" y2="327.66" width="0.1524" layer="91"/>
<wire x1="243.84" y1="327.66" x2="243.84" y2="337.82" width="0.1524" layer="91"/>
<wire x1="243.84" y1="327.66" x2="264.16" y2="327.66" width="0.1524" layer="91"/>
<junction x="243.84" y="327.66"/>
<label x="264.16" y="327.66" size="1.27" layer="95" ratio="15" xref="yes"/>
</segment>
<segment>
<pinref part="J28" gate="-3" pin="1"/>
<wire x1="350.52" y1="342.9" x2="340.36" y2="342.9" width="0.1524" layer="91"/>
<label x="340.36" y="342.9" size="1.27" layer="95" ratio="15" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DAC_I2C_VOUT" class="0">
<segment>
<pinref part="J27" gate="G$1" pin="C"/>
<wire x1="289.56" y1="353.06" x2="269.24" y2="353.06" width="0.1524" layer="91"/>
<pinref part="R84" gate="G$1" pin="2"/>
<wire x1="269.24" y1="353.06" x2="266.7" y2="353.06" width="0.1524" layer="91"/>
<wire x1="266.7" y1="353.06" x2="264.16" y2="353.06" width="0.1524" layer="91"/>
<wire x1="264.16" y1="353.06" x2="259.08" y2="353.06" width="0.1524" layer="91"/>
<wire x1="259.08" y1="353.06" x2="243.84" y2="353.06" width="0.1524" layer="91"/>
<wire x1="243.84" y1="353.06" x2="203.2" y2="353.06" width="0.1524" layer="91"/>
<wire x1="203.2" y1="353.06" x2="203.2" y2="347.98" width="0.1524" layer="91"/>
<pinref part="R86" gate="G$1" pin="2"/>
<wire x1="243.84" y1="347.98" x2="243.84" y2="353.06" width="0.1524" layer="91"/>
<pinref part="C39" gate="G$1" pin="1"/>
<wire x1="259.08" y1="350.52" x2="259.08" y2="353.06" width="0.1524" layer="91"/>
<pinref part="R88" gate="G$1" pin="1"/>
<wire x1="271.78" y1="355.6" x2="269.24" y2="355.6" width="0.1524" layer="91"/>
<wire x1="269.24" y1="355.6" x2="269.24" y2="353.06" width="0.1524" layer="91"/>
<pinref part="R89" gate="G$1" pin="1"/>
<wire x1="271.78" y1="358.14" x2="266.7" y2="358.14" width="0.1524" layer="91"/>
<wire x1="266.7" y1="358.14" x2="266.7" y2="353.06" width="0.1524" layer="91"/>
<wire x1="264.16" y1="353.06" x2="264.16" y2="363.22" width="0.1524" layer="91"/>
<wire x1="264.16" y1="363.22" x2="259.08" y2="363.22" width="0.1524" layer="91"/>
<junction x="269.24" y="353.06"/>
<junction x="266.7" y="353.06"/>
<junction x="264.16" y="353.06"/>
<junction x="259.08" y="353.06"/>
<junction x="243.84" y="353.06"/>
<label x="259.08" y="363.22" size="1.27" layer="95" ratio="15" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J28" gate="-1" pin="1"/>
<wire x1="350.52" y1="347.98" x2="345.44" y2="347.98" width="0.1524" layer="91"/>
<label x="340.36" y="347.98" size="1.27" layer="95" ratio="15" rot="R180" xref="yes"/>
<pinref part="C40" gate="G$1" pin="2"/>
<wire x1="345.44" y1="347.98" x2="340.36" y2="347.98" width="0.1524" layer="91"/>
<wire x1="342.9" y1="353.06" x2="345.44" y2="353.06" width="0.1524" layer="91"/>
<wire x1="345.44" y1="353.06" x2="345.44" y2="347.98" width="0.1524" layer="91"/>
<junction x="345.44" y="347.98"/>
</segment>
</net>
<net name="N$105" class="0">
<segment>
<pinref part="D26" gate="G$1" pin="1"/>
<pinref part="L2" gate="G$1" pin="1"/>
<wire x1="187.96" y1="500.38" x2="190.5" y2="500.38" width="0.1524" layer="91"/>
<pinref part="R90" gate="G$1" pin="1"/>
<wire x1="190.5" y1="500.38" x2="193.04" y2="500.38" width="0.1524" layer="91"/>
<wire x1="193.04" y1="505.46" x2="190.5" y2="505.46" width="0.1524" layer="91"/>
<wire x1="190.5" y1="505.46" x2="190.5" y2="500.38" width="0.1524" layer="91"/>
<junction x="190.5" y="500.38"/>
</segment>
</net>
<net name="N$106" class="0">
<segment>
<pinref part="L2" gate="G$1" pin="2"/>
<pinref part="U8" gate="G$1" pin="3"/>
<wire x1="203.2" y1="500.38" x2="205.74" y2="500.38" width="0.1524" layer="91"/>
<pinref part="R90" gate="G$1" pin="2"/>
<wire x1="205.74" y1="500.38" x2="208.28" y2="500.38" width="0.1524" layer="91"/>
<wire x1="208.28" y1="500.38" x2="213.36" y2="500.38" width="0.1524" layer="91"/>
<wire x1="213.36" y1="500.38" x2="218.44" y2="500.38" width="0.1524" layer="91"/>
<wire x1="203.2" y1="505.46" x2="205.74" y2="505.46" width="0.1524" layer="91"/>
<wire x1="205.74" y1="505.46" x2="205.74" y2="500.38" width="0.1524" layer="91"/>
<pinref part="C41" gate="G$1" pin="1"/>
<wire x1="208.28" y1="497.84" x2="208.28" y2="500.38" width="0.1524" layer="91"/>
<pinref part="C42" gate="G$1" pin="1"/>
<wire x1="213.36" y1="497.84" x2="213.36" y2="500.38" width="0.1524" layer="91"/>
<junction x="208.28" y="500.38"/>
<junction x="213.36" y="500.38"/>
<junction x="205.74" y="500.38"/>
</segment>
</net>
<net name="N$107" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="2"/>
<pinref part="R92" gate="G$1" pin="2"/>
<wire x1="238.76" y1="500.38" x2="246.38" y2="500.38" width="0.1524" layer="91"/>
<wire x1="246.38" y1="500.38" x2="246.38" y2="497.84" width="0.1524" layer="91"/>
<pinref part="L4" gate="G$1" pin="1"/>
<wire x1="246.38" y1="500.38" x2="251.46" y2="500.38" width="0.1524" layer="91"/>
<pinref part="C44" gate="G$1" pin="1"/>
<wire x1="251.46" y1="500.38" x2="266.7" y2="500.38" width="0.1524" layer="91"/>
<wire x1="266.7" y1="500.38" x2="271.78" y2="500.38" width="0.1524" layer="91"/>
<wire x1="271.78" y1="500.38" x2="274.32" y2="500.38" width="0.1524" layer="91"/>
<wire x1="274.32" y1="500.38" x2="276.86" y2="500.38" width="0.1524" layer="91"/>
<wire x1="271.78" y1="497.84" x2="271.78" y2="500.38" width="0.1524" layer="91"/>
<pinref part="C43" gate="G$1" pin="1"/>
<wire x1="266.7" y1="497.84" x2="266.7" y2="500.38" width="0.1524" layer="91"/>
<pinref part="R91" gate="G$1" pin="1"/>
<wire x1="276.86" y1="505.46" x2="274.32" y2="505.46" width="0.1524" layer="91"/>
<wire x1="274.32" y1="505.46" x2="274.32" y2="500.38" width="0.1524" layer="91"/>
<junction x="246.38" y="500.38"/>
<pinref part="D27" gate="G$1" pin="-"/>
<wire x1="251.46" y1="492.76" x2="251.46" y2="500.38" width="0.1524" layer="91"/>
<junction x="251.46" y="500.38"/>
<junction x="266.7" y="500.38"/>
<junction x="271.78" y="500.38"/>
<junction x="274.32" y="500.38"/>
</segment>
</net>
<net name="N$108" class="0">
<segment>
<pinref part="R93" gate="G$1" pin="1"/>
<pinref part="U8" gate="G$1" pin="1"/>
<wire x1="226.06" y1="482.6" x2="228.6" y2="482.6" width="0.1524" layer="91"/>
<wire x1="228.6" y1="482.6" x2="228.6" y2="492.76" width="0.1524" layer="91"/>
<pinref part="R94" gate="G$1" pin="2"/>
<pinref part="R92" gate="G$1" pin="1"/>
<wire x1="246.38" y1="487.68" x2="246.38" y2="485.14" width="0.1524" layer="91"/>
<wire x1="246.38" y1="485.14" x2="248.92" y2="485.14" width="0.1524" layer="91"/>
<wire x1="228.6" y1="482.6" x2="246.38" y2="482.6" width="0.1524" layer="91"/>
<wire x1="246.38" y1="482.6" x2="246.38" y2="485.14" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="E"/>
<wire x1="248.92" y1="472.44" x2="246.38" y2="472.44" width="0.1524" layer="91"/>
<wire x1="246.38" y1="472.44" x2="246.38" y2="482.6" width="0.1524" layer="91"/>
<junction x="228.6" y="482.6"/>
<junction x="246.38" y="485.14"/>
<junction x="246.38" y="482.6"/>
</segment>
</net>
<net name="N$109" class="0">
<segment>
<pinref part="R94" gate="G$1" pin="1"/>
<pinref part="D27" gate="G$1" pin="+"/>
<wire x1="259.08" y1="485.14" x2="261.62" y2="485.14" width="0.1524" layer="91"/>
<wire x1="261.62" y1="485.14" x2="261.62" y2="492.76" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="B"/>
<wire x1="254" y1="477.52" x2="254" y2="480.06" width="0.1524" layer="91"/>
<wire x1="254" y1="480.06" x2="261.62" y2="480.06" width="0.1524" layer="91"/>
<pinref part="C47" gate="G$1" pin="1"/>
<wire x1="261.62" y1="480.06" x2="276.86" y2="480.06" width="0.1524" layer="91"/>
<wire x1="261.62" y1="485.14" x2="261.62" y2="480.06" width="0.1524" layer="91"/>
<junction x="261.62" y="480.06"/>
<junction x="261.62" y="485.14"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="R91" gate="G$1" pin="2"/>
<wire x1="287.02" y1="505.46" x2="289.56" y2="505.46" width="0.1524" layer="91"/>
<pinref part="C46" gate="G$1" pin="1"/>
<wire x1="297.18" y1="497.84" x2="297.18" y2="500.38" width="0.1524" layer="91"/>
<wire x1="297.18" y1="500.38" x2="292.1" y2="500.38" width="0.1524" layer="91"/>
<wire x1="292.1" y1="500.38" x2="289.56" y2="500.38" width="0.1524" layer="91"/>
<wire x1="289.56" y1="500.38" x2="289.56" y2="505.46" width="0.1524" layer="91"/>
<pinref part="L4" gate="G$1" pin="2"/>
<wire x1="287.02" y1="500.38" x2="289.56" y2="500.38" width="0.1524" layer="91"/>
<pinref part="C45" gate="G$1" pin="1"/>
<wire x1="292.1" y1="497.84" x2="292.1" y2="500.38" width="0.1524" layer="91"/>
<wire x1="297.18" y1="500.38" x2="297.18" y2="502.92" width="0.1524" layer="91"/>
<junction x="289.56" y="500.38"/>
<junction x="292.1" y="500.38"/>
<junction x="297.18" y="500.38"/>
<pinref part="P+1" gate="1" pin="+5V"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
