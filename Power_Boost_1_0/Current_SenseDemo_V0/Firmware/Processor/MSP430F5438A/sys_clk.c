

#include "processor_Def.h"


void init_sys_clock()
{
  /* Set up Unifed Clock System (UCS) */
  UCSCTL0 = (SYS_DCO_TAP                                // Set DCO tap selection (modified during FLL operation) = 0
             | SYS_FLL_MOD_COUNTER);                    // Set modulation bit counter (modified during FLL operation) = 0

  UCSCTL1 = (SYS_DCO_RANGE_SELECT                       // Set DCO frequency range 
             | SYS_DISMOD) ;                            // Set enable/disable modulation

  UCSCTL2 = (SYS_FLL_LOOPDIV                            // Set FLL loop divider
             | SYS_DCO_MULTIPLIER_BITS);                // Set DCO multiplier
  
  UCSCTL3 = (SYS_FLL_REFERENCE                          // Select reference for FLL
            | SYS_FLL_REFDIV);                          // Select FLL Reference Divider
  /* Select references for clock sources */
  UCSCTL4 = (SYS_ACLK_SOURCE                            // Set reference for ACLK
             | SYS_SMCLK_SOURCE                         // Set reference for SMCLK
             | SYS_MCLK_SOURCE);                        // Set reference for MCLK
  /* Set up UCS to use XIN/XOUT and external crystal */
  /* Select XIN,XOUT operation on P7.0,P7.1 */
  SYS_XT1_PSEL |= SYS_XT1_XINSEL + SYS_XT1_XOUTSEL;                       // Set XIN operation on P7.0
  //  SYS_XT1_PSEL |= SYS_XT1_XOUTSEL;                      // Set XIN operation on P7.1,X-DNC is XIN PxSEL =1
//  Toggle1(1);
  /* XT1 oscillator on board (32768Hz) */
  UCSCTL6 &= ~SYS_XT1_BYPASS;                           // Set XT1 bypass mode
  // TODO: Remove physical capacitor from board
  UCSCTL6 |= SYS_XT1_XCAP;                              // Set XIN/XOUT capacitor
  UCSCTL6 &= ~SYS_XT1_OFF;                              // Set high frequency oscillator 1 disable
  UCSCTL6 &= ~SYS_XT1_XTS;                              // Set select high frequency oscillator
//  Toggle1(1);
  SFRIE1 |= OFIE;                                       // Set osc fault interrupt enable
//  Toggle1(3);
}

             
#pragma vector=UNMI_VECTOR
__interrupt void USER_NMI_ISR(void)
{
  if(UCSCTL7){                              // Any osc fault detected
    UCSCTL7 = 0;                            // Clear osc fault flag
    SFRIFG1 &= ~OFIFG;                      // Clear osc fault interrupt flag
  }
}