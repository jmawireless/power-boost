#include "uart.h"
//#include "protocol.h"

unsigned char init_UartAx(unsigned char uart_ucaX)
{
  switch(uart_ucaX)
  {
  case 0: 
    /* Set up USCI UART A0 mode */
    UCA0CTL1 |= UCXXRST;                                // Hold USCI in reset state
    UCA0CTL1 |= UART_UCA0_CLOCK_SOURCE;                 // Select clock source [7]

    UCA0CTL0 |= (UART_UCA0_PARITY_ENABLE                // Set parity enable bit [7]
                 | UART_UCA0_PARITY_SELECT);            // Set parity select bit [6]
    UCA0CTL0 |= UART_UCA0_ENDIAN_BITS;                  // Set MSB first select [5]
    UCA0CTL0 |= UART_UCA0_DATA_BIT_SELECT;              // Set character length [4]
    UCA0CTL0 |= UART_UCA0_STOP_BIT_SELECT;              // Set stop bit select [3]
    UCA0CTL0 |= UART_UCA0_MODE;                         // Set UART mode [2:1]

    UCA0BR0 = UART_UCA0_BAUDRATE_CTL_REG0;              // Set baudrate low byte [7:0]
    UCA0BR1 = UART_UCA0_BAUDRATE_CTL_REG1;              // Set baudrate high byte [7:0]

    UCA0MCTL |= UART_UCA0_MODULATION_UCBRFX;            // Set first modulation stage select [7:4]
    UCA0MCTL |= UART_UCA0_MODULATION_UCBRSX;            // Set second modulation stage select [3:1]
 
    UCA0MCTL &= ~UART_UCA0_MODULATION_UCOS16;           // Set oversampling mode enable [0]

    UCA0CTL1 &= ~UCXXRST;                               // Release USCI reset

    UCA0IFG = 0x00;                                     // Clear Tx [1] and Rx [0] flags
    UCA0IE |= (UCRXIE | UCTXIE);                        // Enable Tx [1] and Rx [0] interrupts
    break;
  case 1: 
    /* Set up USCI UART A0 mode */
    UCA1CTL1 |= UCXXRST;                                // Hold USCI in reset state
    UCA1CTL1 |= UART_UCA1_CLOCK_SOURCE;                 // Select clock source [7]

    UCA1CTL0 |= (UART_UCA1_PARITY_ENABLE                // Set parity enable bit [7]
                 | UART_UCA1_PARITY_SELECT);            // Set parity select bit [6]
    UCA1CTL0 |= UART_UCA1_ENDIAN_BITS;                  // Set MSB first select [5]
    UCA1CTL0 |= UART_UCA1_DATA_BIT_SELECT;              // Set character length [4]
    UCA1CTL0 |= UART_UCA1_STOP_BIT_SELECT;              // Set stop bit select [3]
    UCA1CTL0 |= UART_UCA1_MODE;                         // Set UART mode [2:1]

    UCA1BR0 = UART_UCA1_BAUDRATE_CTL_REG0;              // Set baudrate low byte [7:0]
    UCA1BR1 = UART_UCA1_BAUDRATE_CTL_REG1;              // Set baudrate high byte [7:0]

    UCA1MCTL |= UART_UCA1_MODULATION_UCBRFX;            // Set first modulation stage select [7:4]
    UCA1MCTL |= UART_UCA1_MODULATION_UCBRSX;            // Set second modulation stage select [3:1]
 
    UCA1MCTL &= ~UART_UCA1_MODULATION_UCOS16;           // Set oversampling mode enable [0]

    UCA1CTL1 &= ~UCXXRST;                               // Release USCI reset

    UCA1IFG = 0x00;                                     // Clear Tx [1] and Rx [0] flags
    UCA1IE |= (UCRXIE | UCTXIE);                        // Enable Tx [1] and Rx [0] interrupts
    break;
  case 2: 
    /* Set up USCI UART A0 mode */
    UCA2CTL1 |= UCXXRST;                                // Hold USCI in reset state
    UCA2CTL1 |= UART_UCA2_CLOCK_SOURCE;                 // Select clock source [7]

    UCA2CTL0 |= (UART_UCA2_PARITY_ENABLE                // Set parity enable bit [7]
                 | UART_UCA2_PARITY_SELECT);            // Set parity select bit [6]
    UCA2CTL0 |= UART_UCA2_ENDIAN_BITS;                  // Set MSB first select [5]
    UCA2CTL0 |= UART_UCA2_DATA_BIT_SELECT;              // Set character length [4]
    UCA2CTL0 |= UART_UCA2_STOP_BIT_SELECT;              // Set stop bit select [3]
    UCA2CTL0 |= UART_UCA2_MODE;                         // Set UART mode [2:1]

    UCA2BR0 = UART_UCA2_BAUDRATE_CTL_REG0;              // Set baudrate low byte [7:0]
    UCA2BR1 = UART_UCA2_BAUDRATE_CTL_REG1;              // Set baudrate high byte [7:0]

    UCA2MCTL |= UART_UCA2_MODULATION_UCBRFX;            // Set first modulation stage select [7:4]
    UCA2MCTL |= UART_UCA2_MODULATION_UCBRSX;            // Set second modulation stage select [3:1]
 
    UCA2MCTL &= ~UART_UCA2_MODULATION_UCOS16;           // Set oversampling mode enable [0]

    UCA2CTL1 &= ~UCXXRST;                               // Release USCI reset

    UCA2IFG = 0x00;                                     // Clear Tx [1] and Rx [0] flags
    UCA2IE |= (UCRXIE | UCTXIE);                        // Enable Tx [1] and Rx [0] interrupts
    break;
    
  case 3: 
    /* Set up USCI UART A0 mode */
    UCA3CTL1 |= UCXXRST;                                // Hold USCI in reset state
    UCA3CTL1 |= UART_UCA3_CLOCK_SOURCE;                 // Select clock source [7]

    UCA3CTL0 |= (UART_UCA3_PARITY_ENABLE                // Set parity enable bit [7]
                 | UART_UCA3_PARITY_SELECT);            // Set parity select bit [6]
    UCA3CTL0 |= UART_UCA3_ENDIAN_BITS;                  // Set MSB first select [5]
    UCA3CTL0 |= UART_UCA3_DATA_BIT_SELECT;              // Set character length [4]
    UCA3CTL0 |= UART_UCA3_STOP_BIT_SELECT;              // Set stop bit select [3]
    UCA3CTL0 |= UART_UCA3_MODE;                         // Set UART mode [2:1]

    UCA3BR0 = UART_UCA3_BAUDRATE_CTL_REG0;              // Set baudrate low byte [7:0]
    UCA3BR1 = UART_UCA3_BAUDRATE_CTL_REG1;              // Set baudrate high byte [7:0]

    UCA3MCTL |= UART_UCA3_MODULATION_UCBRFX;            // Set first modulation stage select [7:4]
    UCA3MCTL |= UART_UCA3_MODULATION_UCBRSX;            // Set second modulation stage select [3:1]
 
    UCA3MCTL &= ~UART_UCA3_MODULATION_UCOS16;           // Set oversampling mode enable [0]

    UCA3CTL1 &= ~UCXXRST;                               // Release USCI reset

    UCA3IFG = 0x00;                                     // Clear Tx [1] and Rx [0] flags
    UCA3IE |= (UCRXIE | UCTXIE);                        // Enable Tx [1] and Rx [0] interrupts
    break;
  default: 
    //Add code if necessary
    __no_operation();
    break;
  } 
  return FALSE;
}

/***************************************************************************//**
 
 ******************************************************************************/
unsigned char check_Uart_Int_Flag(unsigned char uart_ucaX, unsigned char flag)
{
  unsigned short int bval = 0;
  switch (uart_ucaX)
  {
  case 0: 
    switch (flag) 
    {
    case 0:                                     // Receive (Rx) flag
      if (UCA0IFG & UCRXIFG) { bval = 1; }      // Check interrupt flag
      break;
    case 1:                                     // Transmit (Tx) flag
      if (UCA0IFG & UCTXIFG) { bval = 1; }      // Check interrupt flag
      break;
    }  
    break;
  case 1: 
    switch (flag) 
    {
    case 0:                                     // Receive (Rx) flag
      if (UCA1IFG & UCRXIFG) { bval = 1; }      // Check interrupt flag
      break;
    case 1:                                     // Transmit (Tx) flag
      if (UCA1IFG & UCTXIFG) { bval = 1; }      // Check interrupt flag
      break;
    }
    break;
  case 2:
    switch (flag) 
    {
    case 0:                                     // Receive (Rx) flag
      if (UCA2IFG & UCRXIFG) { bval = 1; }      // Check interrupt flag
      break;
    case 1:                                     // Transmit (Tx) flag
      if (UCA2IFG & UCTXIFG) { bval = 1; }      // Check interrupt flag
      break;
    }
    break;
  case 3: 
    switch (flag) 
    {
    case 0:                                     // Receive (Rx) flag
      if (UCA3IFG & UCRXIFG) { bval = 1; }      // Check interrupt flag
      break;
    case 1:                                     // Transmit (Tx) flag
      if (UCA3IFG & UCTXIFG) { bval = 1; }      // Check interrupt flag
      break;
    }
    break;
  default:
    __no_operation();                           // todo: add exception if any
    break;  
  }
 return bval;
}

/***************************************************************************//**
 
 ******************************************************************************/
void Set_Uart_Int_Flag(unsigned char uart_ucaX)
{
  switch (uart_ucaX)
  {  
  case 0:
      UCA0IFG |= UCTXIFG;                  // Set interrupt flag
    break;
  case 1:                 
      UCA1IFG |= UCTXIFG;                  // Set interrupt flag
    break;
  case 2:                               
      UCA2IFG |= UCTXIFG;                  // Set interrupt flag
    break; 
  case 3:                        
      UCA3IFG |= UCTXIFG;                  // Set interrupt flag
    break;    
  default:
    __no_operation();                      // todo: add exception if any
    break;  
  }
}
/***************************************************************************//**
 
 ******************************************************************************/
void clear_Uart_Int_Flag(unsigned char uart_ucaX, unsigned char flag)
{
  switch (uart_ucaX)
  { 
  case 0:
    if (UCA0STAT & UCBUSY)
    {
      __delay_cycles(500);
    }
    switch (flag) 
    {
    case 0:                                 // Receive (Rx) flag
      UCA0IFG &= ~UCRXIFG;                  // Clear interrupt flag
      break;
    case 1:                                 // Transmit (Tx) flag
      UCA0IFG &= ~UCTXIFG;                  // Clear interrupt flag
      break;
    } 
    break;
    
  case 1:
    if (UCA1STAT & UCBUSY)
    {
      __delay_cycles(200);
    }
    switch (flag) 
    {
    case 0:                                 // Receive (Rx) flag
      UCA1IFG &= ~UCRXIFG;                  // Clear interrupt flag
      break;
    case 1:                                 // Transmit (Tx) flag
      UCA1IFG &= ~UCTXIFG;                  // Clear interrupt flag
      break;
    } 
    break;
  case 2:
    if (UCA2STAT & UCBUSY)
    {
      __delay_cycles(200);
    }
    switch (flag) 
    {
    case 0:                                 // Receive (Rx) flag
      UCA2IFG &= ~UCRXIFG;                  // Clear interrupt flag
      break;
    case 1:                                 // Transmit (Tx) flag
      UCA2IFG &= ~UCTXIFG;                  // Clear interrupt flag
      break;
    } 
    break; 
  case 3:
    if (UCA3STAT & UCBUSY)
    {
      __delay_cycles(200);
    }
    switch (flag) 
    {
    case 0:                                 // Receive (Rx) flag
      UCA3IFG &= ~UCRXIFG;                  // Clear interrupt flag
      break;
    case 1:                                 // Transmit (Tx) flag
      UCA3IFG &= ~UCTXIFG;                  // Clear interrupt flag
      break;
    } 
    break;
    
  default:
    __no_operation();                           // todo: add exception if any
    break;  
  }
}

unsigned char Uart_TxInt_Flag_Status(unsigned char uart_ucaX)
{
    switch(uart_ucaX)
  {
  case 0: 
    return (UCA0IFG & UCTXIFG);  
    break;  
  case 1: 
    return (UCA1IFG & UCTXIFG);  
    break;    
  case 2: 
    return (UCA2IFG & UCTXIFG);  
    break;   
  case 3: 
    return (UCA3IFG & UCTXIFG);  
    break;  
  default:
    return 0x00;
  break;
  }
}

char get_Uart_Rx_Data(unsigned char uart_ucaX) 
{ 
  switch(uart_ucaX)
  {
  case 0: 
    return UCA0RXBUF;  
    break;  
  case 1: 
    return UCA1RXBUF;  
    break;    
  case 2: 
    return UCA2RXBUF;  
    break;   
  case 3: 
    return UCA3RXBUF;  
    break;  
  default:
    return 0x00;
  break;
  } 
}


void set_Uart_Tx_Data(unsigned char uart_ucaX, char data) 
{ 
  switch(uart_ucaX)
  {
  case 0: 
    UCA0TXBUF = data;
    break;   
  case 1: 
    UCA1TXBUF = data;  
    break;    
  case 2: 
    UCA2TXBUF = data;  
    break;    
  case 3: 
    UCA3TXBUF = data; 
    break;
  }   
}



unsigned char AppBufferIsFull(CyclicBuffer * pAppBuffer)
{
  // Check if incrementing the write pointer by 1 causes a rollover to start.
  if ((pAppBuffer->pWrite) < (pAppBuffer->pBufferEnd))
  {
    // If pWrite + 1 and pRead are equal, the buffer is full.
    return ((pAppBuffer->pWrite + 1) == pAppBuffer->pRead);
  }
  else
  {
    // If a rollover occurs, we can use pBuffer in place of pWrite for the
    // comparison. If pBuffer and pRead are the same, the buffer is full.
    return (pAppBuffer->pBufferStart == pAppBuffer->pRead);
  } 
}

unsigned char AppBufferIsEmpty(CyclicBuffer * pAppBuffer)
{
  return (pAppBuffer->pWrite == pAppBuffer->pRead);
}


unsigned char AppWriteBuffer(CyclicBuffer * pAppBuffer, unsigned char writebyte)
{
  unsigned short intState = __get_interrupt_state();
  __disable_interrupt();
 
  unsigned char isFull = AppBufferIsFull(pAppBuffer);
  
  // Verify that there is free space available to write data.
  if (!isFull)
  {
    // Write value at current write pointer position.
    *(pAppBuffer->pWrite) = writebyte;
  
    // Manipulate pointers.
    // Compare the current ptr offset position to the buffer start.
    if (pAppBuffer->pWrite < pAppBuffer->pBufferEnd)
    {
      // If the pointer address is less than the buffer start address + size of
      // the buffer, increment the pointer.
      pAppBuffer->pWrite = pAppBuffer->pWrite + 1;
    }
    else
    {
      // If the pointer reaches the end, set it back to the beginning.
      pAppBuffer->pWrite = pAppBuffer->pBufferStart;
    }
  }

  __set_interrupt_state(intState);
  
  return isFull;
}

unsigned char AppReadBuffer(CyclicBuffer * pAppBuffer, unsigned char * readbyte)
{
  unsigned short intState = __get_interrupt_state();   // store current interrurpt state 
  __disable_interrupt();                                // disable all interrupt
  
  unsigned char isEmpty = AppBufferIsEmpty(pAppBuffer);       // check if buffer empty

  // Verify that there is data to read.
  if (!isEmpty)
  {
    // Read current value at current read pointer position.
    *(readbyte) = *(pAppBuffer->pRead);
  
    // Manipulate pointers.
    // Compare the current ptr offset position to the buffer start.
    if (pAppBuffer->pRead < pAppBuffer->pBufferEnd)
    {
      // If the pointer address is less than the buffer start address + size of
      // the buffer, increment the pointer.
      pAppBuffer->pRead = pAppBuffer->pRead + 1;
    }
    else
    {
      // If the pointer reaches the end, set it back to the beginning.
      pAppBuffer->pRead = pAppBuffer->pBufferStart;
    }
  }

  __set_interrupt_state(intState);
  
  return isEmpty;
}


unsigned char AppBufferBlockFull(CyclicBuffer * pAppBuffer, unsigned char blocklength)
{
  if (AppBufferIsEmpty(pAppBuffer))
  {
    return FALSE;
  }
  else
  {
    if (pAppBuffer->pWrite < pAppBuffer->pRead)
    {
      if ((pAppBuffer->pRead - pAppBuffer->pWrite ) > blocklength)
        return FALSE;
      else
        return TRUE;
    }
    else
    {
      if (((pAppBuffer->pBufferEnd - pAppBuffer->pBufferStart) - (pAppBuffer->pWrite - pAppBuffer->pRead )) > blocklength)
        return FALSE;
      else
        return TRUE;
    }
  }
}
unsigned char AppWriteBufferBlock(CyclicBuffer * pAppBuffer, unsigned char *pWriteblock, unsigned char blocklength)
{
  unsigned short intState = __get_interrupt_state();
  __disable_interrupt();
 
  unsigned char isFull = AppBufferBlockFull(pAppBuffer, blocklength);
  unsigned char writecount;

    // Verify that there is free space available to write data.
    if (!isFull)
    {
      for (writecount = 0; writecount < blocklength; writecount ++)
      {
        // Write value at current write pointer position.
       *pAppBuffer->pWrite = *(pWriteblock + writecount);
      
        // Manipulate pointers.
        // Compare the current ptr offset position to the buffer start.
        if (pAppBuffer->pWrite < pAppBuffer->pBufferEnd)
        {
          // If the pointer address is less than the buffer start address + size of
          // the buffer, increment the pointer.
          pAppBuffer->pWrite = pAppBuffer->pWrite + 1;
        }
        else
        {
          // If the pointer reaches the end, set it back to the beginning.
          pAppBuffer->pWrite = pAppBuffer->pBufferStart;
        }
      }
    } 
  __set_interrupt_state(intState); 
  return isFull;
}



/***************************************************************************//**
 *                          ISR for UART
*******************************************************************************/

/***************************************************************************//**
 * @param Function      __interrupt void vUART0_WIRED_ISR( void )
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  Application.c
 * @param Description   This hardware dependant function used to transmit and receive
                        date from UART
 ******************************************************************************/
// PCU IN/OUT
#pragma vector=UART_0
__interrupt void vUART0_WIRED_ISR( void )
{ 
  LPM0_EXIT;
  // check if data received or data transmitted. 
  if(check_Uart_Int_Flag(0,0)){                           // Ckeck if Received data flag set
    // set Rx LED on
    //Set_UART0_LED(RX,ON);
    Change_Led(LED_1,ON); 
    Change_Led(LED_3,ON);

    clear_Uart_Int_Flag(0,0);
    //Process PSU received Command
    process_PSU_Rx_Msg(Uart_UCA0_Rx_Buffer); // Process Delta Command
    #if defined (Debug_RS485)
    Uart_UCA3_Tx_Buffer = Uart_UCA0_Rx_Buffer;
    #endif
    //Set_UART0_LED(RX,OFF);
    Change_Led(LED_1,OFF); 
    Change_Led(LED_3,OFF);
  }
  if(check_Uart_Int_Flag(0,1)){                           // Check Transmit data flag set
    //Disable RS485 Rx
    P4OUT |= BIT7;
    __delay_cycles(100);
    // set Tx LED on
    Set_UART0_LED(TX,ON);
    // This application just accepts data from RS485 and will not respond back
        unsigned char readvalue;
    // set Tx LED on
    if (!AppBufferIsEmpty(&PSU_TX_Buffer))
    {
      // Read data 
      AppReadBuffer(&PSU_TX_Buffer, &readvalue);
      // write data to TX buffer 
      set_Uart_Tx_Data(0,readvalue);  // read data from circular buffer
      #if defined (Debug_RS485)
      Uart_UCA3_Tx_Buffer = readvalue;
      #endif
    }
    // check if data present to read from buffer
    if (AppBufferIsEmpty(&PSU_TX_Buffer))
    {
      //Clear if Transmit interrupt flag pending only if all pending data transmitted
      clear_Uart_Int_Flag(0,1);
      // Enable RS485 Rx
      __delay_cycles(3000);
      P4OUT &= ~BIT7;
      
    }
    Set_UART0_LED(TX,OFF);
  }
}


/***************************************************************************//**
 * @param Function      __interrupt void vUART1_WIRED_ISR( void )
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  Application.c
 * @param Description   This hardware dependant function used to transmit and receive
                        date from UART
 ******************************************************************************/
//Delta Debug UART
#pragma vector=UART_1
__interrupt void vUART1_WIRED_ISR( void )
{ 
  LPM0_EXIT;
  // check if data received or data transmitted. 
  if(check_Uart_Int_Flag(1,0)){                           // Ckeck if Received data flag set
    // set Rx LED on
    clear_Uart_Int_Flag(1,0);
    // No message will be received as this is used for debugging RS485 msg
  }
  if(check_Uart_Int_Flag(1,1)){                           // Check Transmit data flag set
    //Clear if Transmit interrupt flag pending
    clear_Uart_Int_Flag(1,1);
  } 
}
/***************************************************************************//**
 * @param Function      __interrupt void vUART2_WIRED_ISR( void )
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  Application.c
 * @param Description   This hardware dependant function used to transmit and receive
                        date from UART
 ******************************************************************************/
// RS485 Input
#pragma vector=UART_2
__interrupt void vUART2_WIRED_ISR( void )
{ 
     LPM0_EXIT;
  // check if data received or data transmitted. 
  if(check_Uart_Int_Flag(2,0)){                           // Ckeck if Received data flag set
    // set Rx LED on
    //Set_UART2_LED(RX,ON);
    clear_Uart_Int_Flag(2,0);
    process_Rx_data(Uart_UCA2_Rx_Buffer);
    //Uart_UCA1_Tx_Buffer = Uart_UCA3_Rx_Buffer;
    //#if defined (Debug_RS485)
    Uart_UCA1_Tx_Buffer = Uart_UCA2_Rx_Buffer;
    //#endif
    //Set_UART2_LED(RX,OFF);
  }
  if(check_Uart_Int_Flag(2,1)){                           // Check Transmit data flag set
    // This application just accepts data from RS485 and will not respond back
    //Clear if Transmit interrupt flag pending
    clear_Uart_Int_Flag(2,1);
   
  }
}

/***************************************************************************//**
 * @param Function      __interrupt void vUART3_WIRED_ISR( void )
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  Application.c
 * @param Description   This hardware dependant function used to transmit and receive
                        date from UART
 ******************************************************************************/
// Debug UART
#pragma vector=UART_3
__interrupt void vUART3_WIRED_ISR( void )
{ 
  // check if data received or data transmitted. 
  if(check_Uart_Int_Flag(3,0)){                           // Ckeck if Received data flag set
    //Clear if receive interrupt flag pending
    clear_Uart_Int_Flag(3,0);
  }
  if(check_Uart_Int_Flag(3,1))                          // Check Transmit data flag set
  { 
    //Clear if Transmit interrupt flag pending
    clear_Uart_Int_Flag(3,1);
  }
}

 
        
