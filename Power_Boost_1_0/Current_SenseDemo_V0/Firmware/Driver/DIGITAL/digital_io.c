#include "digital_io.h"

void init_Hardware()
{
  /********************** PORT 1 Defination**********************
   P1.0 - TP8 - IO, OUT
   P1.1 - Buzzer PWM - SP, OUT
   P1.2 - NC
   P1.3 - NC
   P1.4 - NC
   P1.5 - Fault Control 14 - IO, OUT
   P1.6 - Fault Led 14, IO, OUT
   P1.7 - Fault Control 13 - IO, OUT
**************************************************************/    
  P1SEL &= ~(TP8_BIT + Fault_Ctrl_14_BIT + Fault_Led_14_BIT + Fault_Ctrl_13_BIT);
  P1SEL |=  (Buzzer_BIT);
  P1DIR  =  (TP8_BIT + Buzzer_BIT + Fault_Ctrl_14_BIT + Fault_Led_14_BIT + Fault_Ctrl_13_BIT);
  P1OUT  &= ~(TP8_BIT + Buzzer_BIT + Fault_Ctrl_14_BIT + Fault_Led_14_BIT + Fault_Ctrl_13_BIT);
  
  /********************** PORT 2 Defination**********************
   P2.0 - Fault_Led_13_BIT - IO, OUT
   P2.1 - Fault_Ctrl_12_BIT - IO, OUT
   P2.2 - Fault_Led_12_BIT - IO, OUT
   P2.3 - Fault_Ctrl_11_BIT - IO, OUT
   P2.4 - Fault_Led_11_BIT - IO, OUT
   P2.5 - Fault_Ctrl_10_BIT - IO, OUT
   P2.6 - Fault_Led_10_BIT - IO, OUT 
   P2.7 - Fault_Ctrl_9_BIT - IO, OUT
**************************************************************/      
  P2SEL &= ~(Fault_Led_13_BIT + Fault_Ctrl_12_BIT + Fault_Led_12_BIT + Fault_Ctrl_11_BIT + Fault_Led_11_BIT + Fault_Ctrl_10_BIT + Fault_Led_10_BIT + Fault_Ctrl_9_BIT);
  P2DIR =  (Fault_Led_13_BIT + Fault_Ctrl_12_BIT + Fault_Led_12_BIT + Fault_Ctrl_11_BIT + Fault_Led_11_BIT + Fault_Ctrl_10_BIT + Fault_Led_10_BIT + Fault_Ctrl_9_BIT);
  P2OUT &= ~(Fault_Led_13_BIT + Fault_Ctrl_12_BIT + Fault_Led_12_BIT + Fault_Ctrl_11_BIT + Fault_Led_11_BIT + Fault_Ctrl_10_BIT + Fault_Led_10_BIT + Fault_Ctrl_9_BIT);

 /********************** PORT 3 Defination**********************
   P3.0 - Fault_Led_9  - IO, OUT         
   P3.1 - SDA  - SP,                          
   P3.2 - SCL  - SP                  
   P3.3 - RS485_INT - IO,             
   P3.4 - COM_TX - SP               
   P3.5 - COM_RX - SP               
   P3.6 - Fault_Ctrl_8_BIT - IO, OUT      
   P3.7 - Fault_Led_8_BIT - IO, OUT      
**************************************************************/     
  P3SEL &= ~(Fault_Led_9_BIT + RS485_INT_BIT +  Fault_Ctrl_8_BIT + Fault_Led_8_BIT);
  P3SEL |= (PS_SDA_BIT + PS_SCL_BIT + COM_TX_BIT + COM_RX_BIT);
  P3DIR =  (Fault_Led_9_BIT + RS485_INT_BIT +  Fault_Ctrl_8_BIT + Fault_Led_8_BIT);
  P3OUT &= ~(Fault_Led_9_BIT + PS_SDA_BIT + PS_SCL_BIT + RS485_INT_BIT + COM_TX_BIT + COM_RX_BIT +  Fault_Ctrl_8_BIT + Fault_Led_8_BIT);
 
 /********************** PORT 4 Defination**********************
   P4.0 - Fault_Ctrl_7_BIT - IO, OUT  
   P4.1 - Fault_Led_7_BIT - IO, OUT  
   P4.2 - Fault_Ctrl_6_BIT - IO, OUT  
   P4.3 - Fault_Led_6_BIT - IO, OUT  
   P4.4 - Fault_Ctrl_5_BIT - IO, OUT  
   P4.5 - Fault_Led_5_BIT - IO, OUT   
   P4.6 - Fault_Ctrl_4_BIT - IO, OUT  
   P4.7 - Fault_Led_4_BIT - IO, OUT  
**************************************************************/     
  P4SEL &= ~(Fault_Ctrl_7_BIT + Fault_Led_7_BIT + Fault_Ctrl_6_BIT + Fault_Led_6_BIT + Fault_Ctrl_5_BIT + Fault_Led_5_BIT  + Fault_Ctrl_4_BIT + Fault_Led_4_BIT);
  P4DIR =  (Fault_Ctrl_7_BIT + Fault_Led_7_BIT + Fault_Ctrl_6_BIT + Fault_Led_6_BIT + Fault_Ctrl_5_BIT + Fault_Led_5_BIT  + Fault_Ctrl_4_BIT + Fault_Led_4_BIT);
  P4OUT &= ~(Fault_Ctrl_7_BIT + Fault_Led_7_BIT + Fault_Ctrl_6_BIT + Fault_Led_6_BIT + Fault_Ctrl_5_BIT + Fault_Led_5_BIT  + Fault_Ctrl_4_BIT + Fault_Led_4_BIT);
  
/********************** PORT 5 Defination**********************
   P5.0 - AIN8 - SP, IN
   P5.1 - AIN9 - SP, IN
   P5.2 - NC
   P5.3 - NC
   P5.4 - EXT_RX_LED - IO, OUT
   P5.5 - EXT_TX_LED - IO, OUT
   P5.6 - EXT_UART_TX - SP
   P5.7 - EXT_UART_RX - SP
**************************************************************/       
  P5SEL &=  ~(EXT_RX_LED_BIT + EXT_TX_LED_BIT);
  P5SEL |=  (AIN8_BIT + AIN9_BIT + EXT_UART_TX_BIT + EXT_UART_RX_BIT);
  P5DIR  =  (EXT_RX_LED_BIT + EXT_TX_LED_BIT);
  P5OUT &= ~(EXT_RX_LED_BIT + EXT_TX_LED_BIT);
  
  /********************** PORT 6 Defination**********************
   P6.0 - AIN0 - SP, IN
   P6.1 - AIN1 - SP, IN
   P6.2 - AIN2 - SP, IN
   P6.3 - AIN3 - SP, IN
   P6.4 - AIN4 - SP, IN
   P6.5 - AIN5 - SP, IN
   P6.6 - AIN6 - SP, IN
   P6.7 - AIN7 - SP, IN
**************************************************************/   
  P6SEL =  (AIN0_BIT + AIN1_BIT + AIN2_BIT + AIN3_BIT + AIN4_BIT + AIN5_BIT + AIN6_BIT + AIN7_BIT);
  P6DIR &= ~(AIN0_BIT + AIN1_BIT + AIN2_BIT + AIN3_BIT + AIN4_BIT + AIN5_BIT + AIN6_BIT + AIN7_BIT);
  P6OUT &= ~(AIN0_BIT + AIN1_BIT + AIN2_BIT + AIN3_BIT + AIN4_BIT + AIN5_BIT + AIN6_BIT + AIN7_BIT);
  
  /********************** PORT 7 Defination**********************
   P7.0 - XIN - SP, IN
   P7.1 - XOUT - SP, OUT
   P7.2 - Fault_Ctrl_3 - IO, OUT
   P7.3 - Fault_Led_3 - IO, OUT
   P7.4 - AIN10 - SP, IN
   P7.5 - AIN11 - SP, IN
   P7.6 - AIN12 - SP, IN
   P7.7 - AIN13 - SP, IN
**************************************************************/ 
P7SEL &= ~(Fault_Ctrl_3_BIT + Fault_Led_3_BIT);
P7SEL |=  (XIN_BIT + XOUT_BIT + AIN10_BIT + AIN11_BIT + AIN12_BIT  + AIN13_BIT);
//P7DIR  =  (XIN_BIT + AIN10_BIT + AIN11_BIT + AIN12_BIT  + AIN13_BIT);
P7DIR =  (XOUT_BIT + Fault_Ctrl_3_BIT + Fault_Led_3_BIT);
P7OUT &= ~(Fault_Ctrl_3_BIT + Fault_Led_3_BIT);

/********************** PORT 8 Defination**********************
   P8.0 - NC
   P8.1 - Fault_Ctrl_2
   P8.2 - Fault_Led_2
   P8.1 - Fault_Ctrl_1
   P8.2 - Fault_Led_1
   P8.5 - Mode_In_0
   P8.6 - Mode_In_1
   P8.7 - Mode_In_2
**************************************************************/  
  P8SEL &= ~(Fault_Ctrl_2_BIT + Fault_Led_2_BIT + Fault_Ctrl_1_BIT + Fault_Led_1_BIT + Mode_In_0_BIT + Mode_In_1_BIT + Mode_In_2_BIT);
 // P8DIR &= ~(Mode_In_0_BIT + Mode_In_1_BIT + Mode_In_2_BIT);
  P8DIR = (Fault_Ctrl_2_BIT + Fault_Led_2_BIT + Fault_Ctrl_1_BIT + Fault_Led_1_BIT);
  P8SEL &= ~(Fault_Ctrl_2_BIT + Fault_Led_2_BIT + Fault_Ctrl_1_BIT + Fault_Led_1_BIT + Mode_In_0_BIT + Mode_In_1_BIT + Mode_In_2_BIT);

/********************** PORT 9 Defination**********************
   P9.0 - NC
   P9.1 - AUX_SDA
   P9.2 - AUX_SCL
   P9.3 - EXT_SCLK
   P9.4 - EXT_MOSI
   P9.5 - EXT_MISO
   P9.6 - EXT_CS
   P9.7 - NC
**************************************************************/ 
  P9SEL |= (AUX_SDA_BIT + AUX_SCL_BIT + EXT_SCLK_BIT + EXT_MOSI_BIT + EXT_MISO_BIT + EXT_CS_BIT);
//  P9DIR = ~(AUX_SDA_BIT + AUX_SCL_BIT + EXT_SCLK_BIT + EXT_MOSI_BIT + EXT_MISO_BIT + EXT_CS_BIT);
//  P9OUT = ~(AUX_SDA_BIT + AUX_SCL_BIT + EXT_SCLK_BIT + EXT_MOSI_BIT + EXT_MISO_BIT + EXT_CS_BIT);

/********************** PORT 10 Defination**********************
   P10.0 - NC
   P10.1 - DAC_SDA
   P10.2 - DAC_SCL
   P10.3 - NC
   P10.4 - DBG_TX
   P10.5 - DBG_RX
   P10.6 - DBG_LED_RX
   P10.7 - DBG_LED_TX
**************************************************************/  
  P10SEL &= ~(DBG_LED_RX_BIT + DBG_LED_TX_BIT);
  P10SEL |=  (DAC_SDA_BIT + DAC_SCL_BIT + DBG_TX_BIT + DBG_RX_BIT);
  P10DIR &= ~(DBG_LED_RX_BIT + DBG_LED_TX_BIT);
  P10OUT &= ~(DBG_LED_RX_BIT + DBG_LED_TX_BIT);
  
/********************** PORT 11 Defination**********************
   P11.0 - Status_Led_3
   P11.1 - Status_Led_2
   P11.2 - Status_Led_1
**************************************************************/    
  P11SEL &= ~(Status_Led_3_BIT + Status_Led_2_BIT + Status_Led_1_BIT);
  P11DIR |= (Status_Led_3_BIT + Status_Led_2_BIT + Status_Led_1_BIT);
  P11OUT &= ~(Status_Led_3_BIT + Status_Led_2_BIT + Status_Led_1_BIT);
  
}

void Change_Led( unsigned char led_color, unsigned char state)
{
  switch(led_color)
  {
  case Red_Status_Led:
    if (state == ON)
      Status_Led_POUT |= Status_Led_3_BIT;
    else if (state == OFF)
      Status_Led_POUT &= ~Status_Led_3_BIT;
    else
      Status_Led_POUT ^= Status_Led_3_BIT;
    break;
  case Yellow_Status_Led: 
    if (state == ON)
      Status_Led_POUT |= Status_Led_2_BIT;
    else if (state == OFF)
      Status_Led_POUT &= ~Status_Led_2_BIT;
    else
      Status_Led_POUT ^= Status_Led_2_BIT;
    break;
  case Green_Status_Led:
    if (state == ON)
      Status_Led_POUT |= Status_Led_1_BIT;
    else if (state == OFF)
      Status_Led_POUT &= ~Status_Led_1_BIT;
    else
      Status_Led_POUT ^= Status_Led_1_BIT;
    break;
  }
}
 
void Set_UART0_LED(unsigned char mode, unsigned char state)
{
  switch(mode)
  {
  case TX:
    if (state == ON)
    {
      P1OUT |= Fault_Led_14_BIT;
    }
    else
    {
      P1OUT &= ~Fault_Led_14_BIT;
    }  
    break;
  default:
    if (state == ON)
    {
      P2OUT |= Fault_Led_13_BIT;
    }
    else
    {
      P2OUT &= ~Fault_Led_13_BIT;
    }  
    
    break;
  }
}

void Set_Fault_LED(unsigned char Led_No, unsigned char state)
{
  switch (Led_No)
  {
    case 1: 
      if (state == ON)
        P8OUT |= Fault_Led_1_BIT;
      else if (state == OFF)
        P8OUT &= ~Fault_Led_1_BIT;  
      else 
        P8OUT ^= Fault_Led_1_BIT;
    break;
    
    case 2: 
      if (state == ON)
        P8OUT |= Fault_Led_2_BIT;
      else if (state == OFF)
        P8OUT &= ~Fault_Led_2_BIT;  
      else 
        P8OUT ^= Fault_Led_2_BIT; 
    break;
    
    case 3:
      if (state == ON)
        P7OUT |= Fault_Led_3_BIT;
      else if (state == OFF)
        P7OUT &= ~Fault_Led_3_BIT;  
      else 
        P7OUT ^= Fault_Led_3_BIT; 
    break;
    
    case 4: 
      if (state == ON)
        P4OUT |= Fault_Led_4_BIT;
      else if (state == OFF)
        P4OUT &= ~Fault_Led_4_BIT;  
      else 
        P4OUT ^= Fault_Led_4_BIT;  
    break;
    
    case 5: 
      if (state == ON)
        P4OUT |= Fault_Led_5_BIT;
      else if (state == OFF)
        P4OUT &= ~Fault_Led_5_BIT;  
      else 
        P4OUT ^= Fault_Led_5_BIT; 
      break;
    
    case 6: 
      if (state == ON)
        P4OUT |= Fault_Led_6_BIT;
      else if (state == OFF)
        P4OUT &= ~Fault_Led_6_BIT;  
      else 
        P4OUT ^= Fault_Led_6_BIT;       
    break;
    
    case 7: 
      if (state == ON)
        P4OUT |= Fault_Led_7_BIT;
      else if (state == OFF)
        P4OUT &= ~Fault_Led_7_BIT;  
      else 
        P4OUT ^= Fault_Led_7_BIT; 
    break;
    
    case 8: 
      if (state == ON)
        P3OUT |= Fault_Led_8_BIT;
      else if (state == OFF)
        P3OUT &= ~Fault_Led_8_BIT;  
      else 
        P3OUT ^= Fault_Led_8_BIT;
    break;
    
    case 9:
      if (state == ON)
        P3OUT |= Fault_Led_9_BIT;
      else if (state == OFF)
        P3OUT &= ~Fault_Led_9_BIT;  
      else 
        P3OUT ^= Fault_Led_9_BIT;
    break;
    
    case 10: 
      if (state == ON)
        P2OUT |= Fault_Led_10_BIT;
      else if (state == OFF)
        P2OUT &= ~Fault_Led_10_BIT;  
      else 
        P2OUT ^= Fault_Led_10_BIT;
    break;
    
    case 11: 
      if (state == ON)
        P2OUT |= Fault_Led_11_BIT;
      else if (state == OFF)
        P2OUT &= ~Fault_Led_11_BIT;  
      else 
        P2OUT ^= Fault_Led_11_BIT;
    break;
    
    case 12: 
      if (state == ON)
        P2OUT |= Fault_Led_12_BIT;
      else if (state == OFF)
        P2OUT &= ~Fault_Led_12_BIT;  
      else 
        P2OUT ^= Fault_Led_12_BIT;
    break;
    
  }
}
