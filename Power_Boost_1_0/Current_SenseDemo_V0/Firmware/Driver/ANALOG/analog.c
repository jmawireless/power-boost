#include "analog.h"

/***************************************************************************//**
 * @param Function      init_ADC()
 * @param Input         void
 * @param Return        void
 * @param Func_Type     Hardware - Processor
 * @param Description   Used to initialize ADC config
 ******************************************************************************/
void init_ADC(){
    // Select internal ref = 2.5V
    REFCTL0 |= REFMSTR+REFVSEL_2+REFON+REFTCOFF; 
    // 32 ADC10CLKs; ADC ON
    ADC12CTL0 = ADC12SHT0_3 + ADC12ON + ADC12REFON + ADC12REF2_5V  + ADC12MSC;
    // ADC sample and hold, ADC10 sourced from MCLK
    ADC12CTL1 = ADC12SHP + ADC12SSEL_2 + ADC12CONSEQ_2;
    // Channel A0, internal Vref+
    ADC12MCTL0 = ADC12SREF_1 + ADC12INCH_0;  
    
    ADC12CTL2 = ADC12RES_2;
    // 75 us delay @ ~8MHz
    __delay_cycles(600);                       
    // Enable conversions
    ADC12CTL0 |= ADC12ENC;                    
}

/***************************************************************************//**
 * @param Function      Start_TemperatureConversion()
 * @param Input         void
 * @param Return        void
 * @param Func_Type     Hardware - Processor
 * @param Description   Used to measure device temperature
 ******************************************************************************/
unsigned int Start_AnalogConversion(){
    unsigned long Current = 0x00;
    // Start sampling/conversion
   ADC12CTL0 |= (ADC12ON +ADC12ENC + ADC12SC);
   __delay_cycles(1000);
  if (!(ADC12CTL0 & ADC12TOVIE))
  {
    Current = ADC12MEM0; 
    if(Current < 0x4FF)
    {
      __no_operation();
    }
  }
  else
  {
    __no_operation();
  }
//    for(char i = 0; i<3; i++)
//    {
      
//      // Get reading from ADC10
//      Current = ((ADC10MEM0 + Current) >> 1);
//    }
    // Stop Conversion
    ADC12CTL0 &= ~(ADC12ON +ADC12ENC + ADC12SC);
    
    // Return temperature in celcius
    return Current;
}