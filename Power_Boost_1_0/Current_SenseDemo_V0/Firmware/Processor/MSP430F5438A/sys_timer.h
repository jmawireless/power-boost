/***************************************************************************//**
 *  \file         pmm.h
 *  \brief        Program include file and function declaration for Power Management
 *  \author       RBG
 *  \date         September,2011
 *  \version      1.0.0
 ******************************************************************************/
#ifndef __SYS_TIMER
#define __SYS_TIMER

/***************************************************************************//**
 *                           FILE INCLUDES
 ******************************************************************************/
#include "processor_Def.h"
   
#define Max_Timer_Count         40000                   // 10ms at 8M clock
#define Desired_Delay_mSec      100
#define Max_Application_Count   ROUND(Desired_Delay_mSec/(SYS_CLOCK_FREQ_HZ / Max_Timer_Count))
   
   
typedef struct s_Failure_Timer 
{
  unsigned short int Max_Counter_Val;
  unsigned short int TimerCount;
  unsigned char TimerOF_Flag;
}Failure_Timer;

/* Interrupt Vectors */
#define TIMER0_INTA0   TIMER0_A0_VECTOR         
#define TIMER0_INTA1   TIMER0_A1_VECTOR  
   
#define TIMER0_INTB0   TIMER1_B0_VECTOR   
#define TIMER0_INTB1   TIMER0_B1_VECTOR 
   
#define TIMER1_INTA0   TIMER1_A0_VECTOR         
#define TIMER1_INTA1   TIMER1_A1_VECTOR
              
/***************************************************************************//**
 *                           EXTERNAL DELCLERATION
 ******************************************************************************/

/***************************************************************************//**
 *                           FUNCTION DELCLERATION
 ******************************************************************************/
/* Prototypes */
void Timer_A0_Configure(void);
void Set_Timer_A0_CCR(unsigned char reg, unsigned int val);
void Set_Timer_A0_Overflow_Counter(unsigned short int val);
void Stop_Timer_A0();

void Timer_B0_Configure(void);
void Set_Timer_B0_CCR(unsigned char reg, unsigned int val);
void Set_Timer_B0_Overflow_Counter(unsigned short int val);
void Stop_Timer_B0();

void init_Timer();
void Clear_Timer_Count();

extern void Change_Led( unsigned char led_color, unsigned char state);
//extern void Set_Timer_A0_CCR(unsigned char reg, unsigned int val);
#endif /* End __TIMER */
