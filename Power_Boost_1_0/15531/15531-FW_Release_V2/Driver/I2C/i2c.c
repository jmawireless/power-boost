#include "i2c.h"

unsigned char *PTxData;                     // Pointer to TX data
unsigned char TXByteCtr;

DAC_Msg DAC_Config_X =
{
//  UCB3_I2C_SLADDR,
  0x01,         
  0xC0,
  0x00,       //REG1- REG0 = 1, D13->D0 = 0x0000  
};

DAC_Msg DAC_Config_C =
{
//  UCB3_I2C_SLADDR,
  0x01,         //A3->A0 = 0001
  0xA0,
  0x00,       //REG1 = 1, REG0 = 0, D13->D0 = 0x2000  
};

DAC_Msg DAC_Config_M =
{
//  UCB3_I2C_SLADDR,
  0x01,         //A3->A0 = 0001
  0x7F,
  0xFF,       //REG1 = 0, REG0 = 1, D13->D0 = 0x3FFF   
};

DAC_Msg DAC_Config_SFR =
{
//  UCB3_I2C_SLADDR,
  0x01,         //A3->A0 = 0001
  0x3E,
  0x00,       //REG1 = REG0 = 0, D13->D0 = 0x1400   
};

//DAC_Msg DAC_Msg_1 =
//{
//  UCB0_I2C_SLADDR,
//  0x01,         //A3->A0 = 0001
//  0xC000,       //REG1- REG0 = 1, D13->D0 = 0x0000   
//};


DAC_Msg *pDAC_Config_X = &DAC_Config_X;
DAC_Msg *pDAC_Config_M = &DAC_Config_M;
DAC_Msg *pDAC_Config_C = &DAC_Config_C;
DAC_Msg *pDAC_Config_SFR = &DAC_Config_SFR;


char init_I2cAx(char uart_ucaX)
{
  switch(uart_ucaX)
  {
  default: 
    //Add code if necessary
    __no_operation();
    break;
  } 
  return 0;
}

//char init_I2cBx(char i2c_ucbX)
//{
//  switch(i2c_ucbX)
//  {
//  case 0:
//    
//    UCB0CTL1 |= UCXXRST;                                        // Enable SW reset
//    UCB0CTL0 = UCB0_MST_MODE + UCB0_MODEx + UCB0_SYNCMODE;      // I2C Master, synchronous mode
//    UCB0CTL1 = UCB0_I2C_CLK_SOURCE;                             // Use SMCLK, keep SW reset
//    UCB0BR0 = UCB0BR0_L;                                        // fSCL = ~ 400kHz
//    UCB0BR1 = UCB0BR0_H;
////    UCB0I2COA = UCB0_I2C_OWN_ADD;                               // Own Address 
//    UCB0I2CSA = UCB0_I2C_SLADDR;                                // Slave Address 
//    UCB0CTL1 &= ~UCXXRST;                                       // Clear SW reset, resume operation
//    UCB0IE |= UCTXIE;                                           // Enable TX interrupt   
//    break;
//    
//  case 2: 
//    UCB2CTL1 |= UCXXRST;                                        // Enable SW reset
//    UCB2CTL0 = UCB2_MST_MODE + UCB2_MODEx + UCB2_SYNCMODE;      // I2C Master, synchronous mode
//    UCB2CTL1 = UCB2_I2C_CLK_SOURCE + UCXXRST;                   // Use SMCLK, keep SW reset
//    UCB2BR0 = UCB2BR0_L;                                        // fSCL = ~100kHz
//    UCB2BR1 = UCB2BR0_H;
////    UCB2I2COA = UCB2_I2C_OWN_ADD;                               // Own Address
//    UCB2I2CSA = UCB2_I2C_SLADDR;                                // Slave Address
//    UCB2CTL1 &= ~UCXXRST;                                       // Clear SW reset, resume operation
//    UCB2IE |= UCTXIE;                                           // Enable TX interrupt  
//    break;
//    
//  case 3:
//    UCB3CTL1 |= UCXXRST;                                        // Enable SW reset
//    UCB3CTL0 = UCB3_MST_MODE + UCB3_MODEx + UCB3_SYNCMODE;      // I2C Master, synchronous mode
//    UCB3CTL1 = UCB3_I2C_CLK_SOURCE + UCXXRST;                   // Use SMCLK, keep SW reset
//    UCB3BR0 = UCB3BR0_L;                                        // fSCL = ~100kHz
//    UCB3BR1 = UCB3BR0_H;
////    UCB3I2COA = UCB3_I2C_OWN_ADD;                               // Own Address
//    UCB3I2CSA = UCB3_I2C_SLADDR;                                // Slave Address
//    UCB3CTL1 &= ~UCXXRST;                                       // Clear SW reset, resume operation
//    UCB3IE |= UCTXIE;                                           // Enable TX interrupt  
//    SetupDAC();
//    break; 
//  default: 
//    //Add code if necessary
//    __no_operation();
//    break;
//  }
//  return 0;
//}



void SetupDAC()
{
    unsigned char i;
//    UCB0I2CSA = 0x54;
    for (i = 0; i< 1;i++)
    {
      SendD2AConvertorMsg(x,i, 0x0000);
      SendD2AConvertorMsg(m,i, 0x3FFF);
      SendD2AConvertorMsg(c,i, 0x2000);
      SendD2AConvertorMsg(s,i, 0x3E00);    
    }
//    UCB0I2CSA = 0x55;
//    for (i = 0; i< 1;i++)
//    {
//      SendD2AConvertorMsg(x,i, 0x003F);
//      SendD2AConvertorMsg(m,i, 0x3FFF);
//      SendD2AConvertorMsg(c,i, 0x2000);
//      SendD2AConvertorMsg(s,i, 0x3E00);    
//    }
//    UCB0I2CSA = 0x56;
//    for (i = 0; i< 1;i++)
//    {
//      SendD2AConvertorMsg(x,i, 0x007F);
//      SendD2AConvertorMsg(m,i, 0x3FFF);
//      SendD2AConvertorMsg(c,i, 0x2000);
//      SendD2AConvertorMsg(s,i, 0x3E00);    
//    }
//    UCB0I2CSA = 0x57;
//    for (i = 0; i< 1;i++)
//    {
//      SendD2AConvertorMsg(x,i, 0x00FF);
//      SendD2AConvertorMsg(m,i, 0x3FFF);
//      SendD2AConvertorMsg(c,i, 0x2000);
//      SendD2AConvertorMsg(s,i, 0x3E00);    
//    }
}

void SendD2AConvertorMsg(unsigned char Reg_Type, unsigned char DAC_Channel, unsigned int Raw_OVP_Data)
{
    unsigned short int Data = 0x0000;
    switch (Reg_Type)
    {
    case x:
      Data = (((Raw_OVP_Data << 6) & 0x3FFF ) | 0xC000);
      DAC_Config_X.OVP_DAC_data_L = (unsigned char) (Data & 0xFF);
      DAC_Config_X.OVP_DAC_data_H = (unsigned char) ((Data >> 8) & 0xFF);
//      DAC_Config_X.Slave_Addr = (UCB3_I2C_SLADDR & 0xFE);
      
      DAC_Config_X.OVP_Reg_Addr = DAC_Channel;
      PTxData = (unsigned char *)pDAC_Config_X;  // TX array start address
                                              // Place breakpoint here to see each
                                              // transmit operation.
      TXByteCtr = sizeof DAC_Config_X;              // Load TX byte counter   
      break;
    
    
    case m:
      Data = ((Raw_OVP_Data & 0x3FFF ) | 0x4000);
      DAC_Config_M.OVP_DAC_data_L = (unsigned char) (Data & 0xFF);
      DAC_Config_M.OVP_DAC_data_H = (unsigned char) ((Data >> 8) & 0xFF);
//      DAC_Config_M.Slave_Addr = (UCB3_I2C_SLADDR & 0xFE);
      
      DAC_Config_M.OVP_Reg_Addr = DAC_Channel;
      PTxData = (unsigned char *)pDAC_Config_M;  // TX array start address
                                              // Place breakpoint here to see each
                                              // transmit operation.
      TXByteCtr = sizeof DAC_Config_M;              // Load TX byte counter   
      break;
    

  case c:
      Data = ((Raw_OVP_Data & 0x3FFF ) | 0x8000);
      DAC_Config_C.OVP_DAC_data_L = (unsigned char) (Data & 0xFF);
      DAC_Config_C.OVP_DAC_data_H = (unsigned char) ((Data >> 8) & 0xFF);
//      DAC_Config_C.Slave_Addr = (UCB3_I2C_SLADDR & 0xFE);
      
      DAC_Config_C.OVP_Reg_Addr = DAC_Channel;
      PTxData = (unsigned char *)pDAC_Config_C;  // TX array start address
                                              // Place breakpoint here to see each
                                              // transmit operation.
      TXByteCtr = sizeof DAC_Config_M;              // Load TX byte counter   
      break;
   
  case s:
      Data = (Raw_OVP_Data & 0x3FFF );
      DAC_Config_SFR.OVP_DAC_data_L = (Data & 0xFF);
      DAC_Config_SFR.OVP_DAC_data_H = ((Data >> 8) & 0xFF);
//      DAC_Config_SFR.Slave_Addr = (UCB3_I2C_SLADDR & 0xFE);
      
      DAC_Config_SFR.OVP_Reg_Addr = DAC_Channel;
      PTxData = (unsigned char *)pDAC_Config_SFR;  // TX array start address
                                              // Place breakpoint here to see each
                                              // transmit operation.
      TXByteCtr = sizeof DAC_Config_SFR;              // Load TX byte counter   
      break;
    }
    
    if (TXByteCtr)
    {
         
    }
    while (TXByteCtr)
    {
      __delay_cycles(500);                     // Delay required between transaction  
      UCB3CTL1 |= UCTR + UCTXSTT;             // I2C TX, start condition 
      __bis_SR_register(LPM1_bits + GIE);     // Enter LPM0, enable interrupts
      __no_operation();                       // Remain in LPM0 until all data  txd     
      while (UCB0CTL1 & UCTXSTP);             // Ensure stop condition got sent
    }
}

#pragma vector = USCI_B0_VECTOR
__interrupt void USCI_B0_ISR(void)
{
  switch(__even_in_range(UCB0IV,12))
  {
  case  0: break;                           // Vector  0: No interrupts
  case  2: break;                           // Vector  2: ALIFG
  case  4: break;                           // Vector  4: NACKIFG
  case  6: break;                           // Vector  6: STTIFG   
  case  8: break;                           // Vector  8: STPIFG
  case 10: break;                           // Vector 10: RXIFG  
  case 12: break;                           // Vector 12: TXIFG 
  default: break; 
  }
}

#pragma vector = USCI_B2_VECTOR
__interrupt void USCI_B2_ISR(void)
{
  switch(__even_in_range(UCB2IV,12))
  {
  case  0: break;                           // Vector  0: No interrupts (ti: 38.4.11 Section)
  case  2: break;                           // Vector  2: ALIFG
  case  4: break;                           // Vector  4: NACKIFG
  case  6: break;                           // Vector  6: STTIFG   
  case  8: break;                           // Vector  8: STPIFG 
  case 10: break;                           // Vector 10: RXIFG  
  case 12: break;                           // Vector 12: TXIFG  
  default: break; 
  }
}

#pragma vector = USCI_B3_VECTOR
__interrupt void USCI_B3_ISR(void)
{
  switch(__even_in_range(UCB3IV,12))
  {
  case  0: break;                           // Vector  0: No interrupts (ti: 38.4.11 Section)
  case  2: break;                           // Vector  2: ALIFG
  case  4: break;                           // Vector  4: NACKIFG
  case  6: break;                           // Vector  6: STTIFG   
  case  8: break;                           // Vector  8: STPIFG 
  case 10: break;                           // Vector 10: RXIFG  
  case 12: 
//    UCB3TXBUF = 0xA5;
    if (TXByteCtr)                          // Check TX byte counter
    {
      UCB3TXBUF = *PTxData++;               // Load TX buffer
      TXByteCtr--;                          // Decrement TX byte counter
    }
    else
    {
      UCB3CTL1 |= UCTXSTP;                  // I2C stop condition
      UCB3IFG &= ~UCTXIFG;                  // Clear USCI_B0 TX int flag
      __bic_SR_register_on_exit(LPM0_bits); // Exit LPM0
    }
    break;                           // Vector 12: TXIFG  
  default: break; 
  }
}

