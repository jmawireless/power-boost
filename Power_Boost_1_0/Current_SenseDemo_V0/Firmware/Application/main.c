
//#include "io430.h"
#include "main.h" 
#define START_CHARACTER ":"
unsigned char *pSTART_CHARACTER = START_CHARACTER;
 void init_Comm_Peripheral()
 {
//  init_UartAx(0);      // Communucation UART from RS485
   init_UartAx(1);      // External Interface UART 
   init_UartAx(3);      // Debug Uart
   
//   init_I2cBx(0);       // i2c to PS control Addr: 0x77
//   init_I2cBx(2);       // Auxillary i2c Addr: 0x78
//   init_I2cBx(3);       // DAC i2c Addr: 0x79
   
//   init_SpiBx(2);       // External SPI
 }

void main( void )
{
  // Stop watchdog timer to prevent time out reset
  Stop_WatchDog_Timer();
  // Disable Global Interrupts
  __disable_interrupt();
  // Set Core voltage 
  SetVCore(2);
  // Initialize hardware IO to default/known state
  init_Hardware();   
  // Initialize system clocks
  init_sys_clock();
  // Initialize timer 
  init_Timer();
  // Set up peripherials UART, I2C, SPI etc.
  init_Comm_Peripheral();
  // Initialize software parameters
//  init_Protocol();
  init_ADC();
  // Enable Global interrupts
  __enable_interrupt();
  
  while(1)
  {
    // Verify if end of message flag set
    unsigned char Current_Monitor_Flag = Timer_Count_Expired();
    
    if (Current_Monitor_Flag == 1 )
    {
      unsigned int current = Start_AnalogConversion();
      // print current value
      Clear_Timer_Count();
//      for(unsigned char i = 0; i < 100; i++)
//      {
//        if (Uart_Tx_Busy(3) == 1)
//        {
//          __delay_cycles(100);
//        }
//        else
//        {
//          break;
//        }
//      }
      set_Uart_Tx_Data(3,*pSTART_CHARACTER);
      __delay_cycles(1000);
//      for(unsigned char i = 0; i < 100; i++)
//      {
//        if (Uart_Tx_Busy(3) == 1)
//        {
//          __delay_cycles(1000);
//        }
//        else
//        {
//          break;
//        }
//      }
      set_Uart_Tx_Data(3,current >> 8);
      __delay_cycles(1000);
//      for(unsigned char i = 0; i < 100; i++)
//      {
//        if (Uart_Tx_Busy(3) == 1)
//        {
//          __delay_cycles(100);
//        }
//        else
//        {
//          break;
//        }
//      }
      set_Uart_Tx_Data(3,current & 0x00FF);
      __delay_cycles(100);
    }
  }
}
