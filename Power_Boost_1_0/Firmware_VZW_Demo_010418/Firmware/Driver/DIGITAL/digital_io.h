/***************************************************************************//**
 *  \file         digital_io.h
 *  \brief        Program include file and function declaration for digital IO
 *  \author       RBG
 *  \date         December,2017
 *  \version      1.0.0
 ******************************************************************************/
#ifndef __DIGITAL_IO
#define __DIGITAL_IO
/***************************************************************************//**
* FILE INCLUDES
*******************************************************************************/
#include "processor_Def.h"

/***************************************************************************//**
 * GPIO VARIABLE DECLARATION
 ******************************************************************************/
/* SEL, DIR, OUT, IN, REN, IE, IES not defined for IO pins only defined for 
   SP function registers */
 /********************** PORT 1 Defination**********************
   P1.0 - TP3
   P1.1 - TP6
   P1.2 - NC
   P1.3 - NC
   P1.4 - NC
   P1.5 - NC
   P1.6 - NC
   P1.7 - NC
**************************************************************/     
#define TP9_BIT          BIT0
#define TP10_BIT         BIT1 
/********************** PORT 2 Defination**********************
   P2.0 - TP2, Default IN 
   P2.1 - TP1, Default IN 
   P2.2 - TP4, Default IN 
   P2.3 - TP3, Default IN 
   P2.4 - NC
   P2.5 - NC
   P2.6 - NC
   P2.7 - TP6, Default IN 
**************************************************************/     
#define PBC_ERROR_POUT P2OUT   
#define PBC_ERROR_BIT  BIT0
#define TP1_BIT        BIT1
#define TP4_BIT        BIT2
#define TP3_BIT        BIT3
#define TP6_BIT        BIT6
/********************** PORT 3 Defination**********************
   P3.0 - TP5, Default IN         
   P3.1 - TP8, Default IN                          
   P3.2 - TP7, Default IN                
   P3.3 - NC           
   P3.4 - COM_TX - Delta              
   P3.5 - COM_RX - Delta             
   P3.6 - NC    
   P3.7 - NC      
**************************************************************/     
#define TP5_BIT        BIT0
#define TP8_BIT        BIT1
#define TP7_BIT        BIT2
//#define PBC_UART_SEL            P3SEL
//#define PBC_UART_DIR            P3DIR
#define PBC_TX_BIT              BIT4
#define PBC_RX_BIT              BIT5
/********************** PORT 4 Defination**********************
   P4.0 - NC
   P4.1 - NC
   P4.2 - NC
   P4.3 - NC
   P4.4 - NC
   P4.5 - NC  
   P4.6 - PBC_DE
   P4.7 - PBC_RE
**************************************************************/     
#define PBC_DE        BIT6
#define PBC_RE        BIT7
/********************** PORT 5 Defination**********************
   P5.0 - NC
   P5.1 - NC
   P5.2 - NC
   P5.3 - NC
   P5.4 - COM_RX_LED
   P5.5 - COM_TX_LED
   P5.6 - COM_DBG_UART_TX
   P5.7 - COM_DBG_UART_RX
**************************************************************/       
#define COM_RX_LED_BIT          BIT4
#define COM_TX_LED_BIT          BIT5
//#define COM_DBG_UART_SEL            P5SEL
//#define COM_DBG_UART_DIR            P5DIR
#define COM_DBG_UART_TX_BIT         BIT6    
#define COM_DBG_UART_RX_BIT         BIT7  
/********************** PORT 6 Defination**********************
   P6.0 - LED_1
   P6.1 - LED_2
   P6.2 - LED_3
   P6.3 - LED_4
   P6.4 - TP11
   P6.5 - TP12 
   P6.6 - TP13
   P6.7 - TP14
**************************************************************/    
#define RJ45_LED_POUT            P6OUT     
#define LED_1_BIT                BIT0
#define LED_2_BIT                BIT1
#define LED_3_BIT                BIT2
#define LED_4_BIT                BIT3
#define TP11_BIT                 BIT4
#define TP12_BIT                 BIT5
#define TP13_BIT                 BIT6
#define TP14_BIT                 BIT7
 /********************** PORT 7 Defination**********************
   P7.0 - XIN - SP, IN
   P7.1 - XOUT - SP, OUT
   P7.2 - NC
   P7.3 - NC
   P7.4 - NC
   P7.5 - NC
   P7.6 - NC
   P7.7 - NC
**************************************************************/     
#define XIN_BIT                 BIT0
#define XOUT_BIT                BIT1 
/********************** PORT 8 Defination**********************
   P8.0 - NC
   P8.1 - NC
   P8.2 - NC
   P8.3 - NC
   P8.4 - NC
   P8.5 - Mode_In_0
   P8.6 - Mode_In_1
   P8.7 - Mode_In_2
**************************************************************/      
#define Mode_In_0_BIT           BIT5
#define Mode_In_1_BIT           BIT6
#define Mode_In_2_BIT           BIT7
/********************** PORT 9 Defination**********************
   P9.0 - NC
   P9.1 - NC
   P9.2 - NC
   P9.3 - NC
   P9.4 - COM_TXD
   P9.5 - COM_RXD
   P9.6 - RS_485_INT
   P9.7 - NC
**************************************************************/  
//#define COM_TXD_SEL             P9SEL
//#define COM_TXD_DIR             P9DIR
#define COM_TXD_BIT             BIT4
#define COM_RXD_BIT             BIT5
#define RS_485_INT_BIT          BIT6    
/********************** PORT 10 Defination**********************
   P10.0 - ADD_1
   P10.1 - ADD_2
   P10.2 - ADD_4
   P10.3 - ADD_8
   P10.4 - PBC_DBG_TX
   P10.5 - PBC_DBG_RX
   P10.6 - PBC_DBG_LED_RX
   P10.7 - PBC_DBG_LED_TX
**************************************************************/     
//BIT0 - NC
#define ADD_1_BIT               BIT0
#define ADD_2_BIT               BIT1
#define ADD_4_BIT               BIT2
#define ADD_8_BIT               BIT3
//#define PBC_DBG_UART_SEL            P10SEL
//#define PBC_DBG_UART_DIR            P10DIR
#define PBC_DBG_TX_BIT              BIT4
#define PBC_DBG_RX_BIT              BIT5
#define PBC_DBG_LED_RX_BIT          BIT6    
#define PBC_DBG_LED_TX_BIT          BIT7  
/********************** PORT 11 Defination**********************
   P11.0 - Status_Led_3
   P11.1 - Status_Led_2
   P11.2 - Status_Led_1
**************************************************************/
#define Status_Led_POUT         P11OUT
#define Status_Led_3_BIT        BIT0
#define Status_Led_2_BIT        BIT1
#define Status_Led_1_BIT        BIT2
 /********************** PORT J Defination**********************
   PJ.0 - TDO
   PJ.1 - TDI
   PJ.2 - TMS
   PJ.3 - TCK
**************************************************************/     
// J port is defaulted tp programming and debugging so no need to configure   
   
   
/***************************************************************************//**
* FUNCTION DEFINATION
*******************************************************************************/
void init_Hardware();
void Set_UART0_LED(unsigned char mode, unsigned char state);
void Change_Led( unsigned char led_name, unsigned char state); 

#endif