**********************************************************************
This email message, including any attachments, is for the sole use of 
the intended recipient(s) and may contain confidential and privileged 
information. Any unauthorized review, use, disclosure or distribution
is prohibited. If you are not the intended recipient, please contact 
the sender by reply e-mail and destroy all copies of the original
message. 
**********************************************************************
