<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.2.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="no"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="110" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="111" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="no"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="top_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="14" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="15" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
<layer number="255" name="Accent" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="JMA_LBR">
<packages>
<package name="JST_PHD_14PIN_2MM">
<pad name="P$1" x="6" y="-1" drill="0.7"/>
<pad name="P$2" x="6" y="1" drill="0.7"/>
<pad name="P$3" x="4" y="-1" drill="0.7"/>
<pad name="P$4" x="4" y="1" drill="0.7"/>
<pad name="P$5" x="2" y="-1" drill="0.7"/>
<pad name="P$6" x="2" y="1" drill="0.7"/>
<pad name="P$7" x="0" y="-1" drill="0.7"/>
<pad name="P$8" x="0" y="1" drill="0.7"/>
<pad name="P$9" x="-2" y="-1" drill="0.7"/>
<pad name="P$10" x="-2" y="1" drill="0.7"/>
<pad name="P$11" x="-4" y="-1" drill="0.7"/>
<pad name="P$12" x="-4" y="1" drill="0.7"/>
<pad name="P$13" x="-6" y="-1" drill="0.7"/>
<pad name="P$14" x="-6" y="1" drill="0.7"/>
<wire x1="-8" y1="-3" x2="-8" y2="8.5" width="0.127" layer="21"/>
<wire x1="-8" y1="8.5" x2="8" y2="8.5" width="0.127" layer="21"/>
<wire x1="8" y1="8.5" x2="8" y2="-3" width="0.127" layer="21"/>
<wire x1="8" y1="-3" x2="-8" y2="-3" width="0.127" layer="21"/>
<text x="9" y="7" size="1.27" layer="21" font="vector" ratio="15">&gt;NAME</text>
<text x="9" y="5" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<wire x1="-8" y1="0" x2="-8" y2="11" width="0.127" layer="21"/>
<wire x1="-8" y1="11" x2="8" y2="11" width="0.127" layer="21"/>
<wire x1="8" y1="11" x2="8" y2="8" width="0.127" layer="21"/>
<text x="-2" y="9" size="1.27" layer="21" font="vector" ratio="15">MATE </text>
<text x="-3" y="7" size="1.27" layer="21" font="vector" ratio="15">HEADER</text>
<wire x1="5.969" y1="-3.429" x2="8.001" y2="-3.429" width="0.127" layer="21"/>
<wire x1="8.001" y1="-3.429" x2="6.985" y2="-4.318" width="0.127" layer="21"/>
<wire x1="6.985" y1="-4.318" x2="5.969" y2="-3.429" width="0.127" layer="21"/>
</package>
<package name="TST-107-X-X-D">
<wire x1="-12.7" y1="5.461" x2="-11.43" y2="5.461" width="0.2032" layer="22"/>
<wire x1="-11.43" y1="5.461" x2="-11.43" y2="4.699" width="0.2032" layer="22"/>
<wire x1="-11.43" y1="4.699" x2="-1.27" y2="4.699" width="0.2032" layer="22"/>
<wire x1="-1.27" y1="4.699" x2="-1.27" y2="5.461" width="0.2032" layer="22"/>
<wire x1="-1.27" y1="5.461" x2="1.27" y2="5.461" width="0.2032" layer="22"/>
<wire x1="1.27" y1="5.461" x2="1.27" y2="4.699" width="0.2032" layer="22"/>
<wire x1="1.27" y1="4.699" x2="11.43" y2="4.699" width="0.2032" layer="22"/>
<wire x1="11.43" y1="4.699" x2="11.43" y2="5.461" width="0.2032" layer="22"/>
<wire x1="11.43" y1="5.461" x2="12.7" y2="5.461" width="0.2032" layer="22"/>
<wire x1="12.7" y1="5.461" x2="12.7" y2="-4.699" width="0.2032" layer="22"/>
<wire x1="12.7" y1="-4.699" x2="1.27" y2="-4.699" width="0.2032" layer="22"/>
<wire x1="1.27" y1="-4.699" x2="-1.27" y2="-4.699" width="0.2032" layer="22"/>
<wire x1="-1.27" y1="-4.699" x2="-12.7" y2="-4.699" width="0.2032" layer="22"/>
<wire x1="-12.7" y1="-4.699" x2="-12.7" y2="5.461" width="0.2032" layer="22"/>
<pad name="1" x="7.62" y="-1.27" drill="1" diameter="1.5" shape="square"/>
<pad name="2" x="7.62" y="1.27" drill="1" diameter="1.5" shape="octagon"/>
<pad name="3" x="5.08" y="-1.27" drill="1" diameter="1.5" shape="octagon"/>
<pad name="4" x="5.08" y="1.27" drill="1" diameter="1.5" shape="octagon"/>
<pad name="5" x="2.54" y="-1.27" drill="1" diameter="1.5" shape="octagon"/>
<pad name="6" x="2.54" y="1.27" drill="1" diameter="1.5" shape="octagon"/>
<pad name="7" x="0" y="-1.27" drill="1" diameter="1.5" shape="octagon"/>
<pad name="8" x="0" y="1.27" drill="1" diameter="1.5" shape="octagon"/>
<pad name="9" x="-2.54" y="-1.27" drill="1" diameter="1.5" shape="octagon"/>
<pad name="10" x="-2.54" y="1.27" drill="1" diameter="1.5" shape="octagon"/>
<pad name="11" x="-5.08" y="-1.27" drill="1" diameter="1.5" shape="octagon"/>
<pad name="12" x="-5.08" y="1.27" drill="1" diameter="1.5" shape="octagon"/>
<pad name="13" x="-7.62" y="-1.27" drill="1" diameter="1.5" shape="octagon"/>
<pad name="14" x="-7.62" y="1.27" drill="1" diameter="1.5" shape="octagon"/>
<text x="9.952" y="-6.358" size="1.27" layer="22" font="vector" ratio="15" rot="SMR0">1</text>
<text x="-8.89" y="6.35" size="1.27" layer="26" font="vector" ratio="15" rot="MR0">&gt;NAME</text>
<text x="-8.89" y="7.62" size="1.27" layer="28" font="vector" ratio="15" rot="MR0">&gt;VALUE</text>
<rectangle x1="-7.97" y1="-1.62" x2="-7.27" y2="-0.92" layer="51"/>
<rectangle x1="-7.97" y1="0.92" x2="-7.27" y2="1.62" layer="51"/>
<rectangle x1="-5.43" y1="-1.62" x2="-4.73" y2="-0.92" layer="51"/>
<rectangle x1="-5.43" y1="0.92" x2="-4.73" y2="1.62" layer="51"/>
<rectangle x1="-2.89" y1="-1.62" x2="-2.19" y2="-0.92" layer="51"/>
<rectangle x1="-2.89" y1="0.92" x2="-2.19" y2="1.62" layer="51"/>
<rectangle x1="-0.35" y1="-1.62" x2="0.35" y2="-0.92" layer="51"/>
<rectangle x1="-0.35" y1="0.92" x2="0.35" y2="1.62" layer="51"/>
<rectangle x1="2.19" y1="-1.62" x2="2.89" y2="-0.92" layer="51"/>
<rectangle x1="2.19" y1="0.92" x2="2.89" y2="1.62" layer="51"/>
<rectangle x1="4.73" y1="-1.62" x2="5.43" y2="-0.92" layer="51"/>
<rectangle x1="4.73" y1="0.92" x2="5.43" y2="1.62" layer="51"/>
<rectangle x1="7.27" y1="-1.62" x2="7.97" y2="-0.92" layer="51"/>
<rectangle x1="7.27" y1="0.92" x2="7.97" y2="1.62" layer="51"/>
<wire x1="-11.43" y1="-3.81" x2="-1.27" y2="-3.81" width="0.127" layer="22"/>
<wire x1="-1.27" y1="-3.81" x2="1.27" y2="-3.81" width="0.127" layer="22"/>
<wire x1="1.27" y1="-3.81" x2="11.43" y2="-3.81" width="0.127" layer="22"/>
<wire x1="-11.43" y1="-3.81" x2="-11.43" y2="3.81" width="0.127" layer="22"/>
<wire x1="-11.43" y1="3.81" x2="11.43" y2="3.81" width="0.127" layer="22"/>
<wire x1="11.43" y1="3.81" x2="11.43" y2="-3.81" width="0.127" layer="22"/>
<wire x1="-1.27" y1="-3.81" x2="-1.27" y2="-4.699" width="0.127" layer="22"/>
<wire x1="1.27" y1="-3.81" x2="1.27" y2="-4.699" width="0.127" layer="22"/>
</package>
<package name="RESC1005X40N">
<wire x1="-0.48" y1="-0.94" x2="-0.48" y2="0.94" width="0.127" layer="21"/>
<wire x1="-0.48" y1="0.94" x2="0.48" y2="0.94" width="0.127" layer="21"/>
<wire x1="0.48" y1="0.94" x2="0.48" y2="-0.94" width="0.127" layer="21"/>
<wire x1="0.48" y1="-0.94" x2="-0.48" y2="-0.94" width="0.127" layer="21" style="shortdash"/>
<smd name="1" x="0" y="0.48" dx="0.57" dy="0.62" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.48" dx="0.57" dy="0.62" layer="1" roundness="25" rot="R270"/>
<text x="0.635" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.524" y="1.397" size="0.635" layer="27" font="vector" ratio="10" rot="R270">&gt;VALUE</text>
<wire x1="-0.48" y1="-0.93" x2="-0.48" y2="0.93" width="0.127" layer="51"/>
<wire x1="-0.48" y1="0.93" x2="0.48" y2="0.93" width="0.127" layer="51"/>
<wire x1="0.48" y1="0.93" x2="0.48" y2="-0.93" width="0.127" layer="51"/>
<wire x1="0.48" y1="-0.93" x2="-0.48" y2="-0.93" width="0.127" layer="51" style="shortdash"/>
</package>
<package name="RESC1608X50N">
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.8" dx="0.95" dy="1" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.8" dx="0.95" dy="1" layer="1" roundness="25" rot="R270"/>
<text x="0.889" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.5875" y="1.27" size="0.635" layer="27" ratio="10" rot="R270">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
</package>
<package name="RESC2013X70N">
<wire x1="1" y1="1.8" x2="1" y2="-1.8" width="0.127" layer="21"/>
<wire x1="1" y1="-1.8" x2="-1" y2="-1.8" width="0.127" layer="21"/>
<wire x1="-1" y1="-1.8" x2="-1" y2="1.8" width="0.127" layer="21"/>
<wire x1="-1" y1="1.8" x2="1" y2="1.8" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.95" dx="1.05" dy="1.4" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.95" dx="1.05" dy="1.4" layer="1" roundness="25" rot="R270"/>
<text x="1.511" y="-1.47" size="0.4064" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1.2125" y="-1.47" size="0.4064" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
</package>
<package name="RESC3216X84N">
<wire x1="1.275" y1="2.27" x2="1.275" y2="-2.27" width="0.127" layer="21"/>
<wire x1="1.275" y1="-2.27" x2="-1.275" y2="-2.27" width="0.127" layer="21"/>
<wire x1="-1.275" y1="-2.27" x2="-1.275" y2="2.27" width="0.127" layer="21"/>
<wire x1="-1.275" y1="2.27" x2="1.275" y2="2.27" width="0.127" layer="21"/>
<smd name="1" x="0" y="1.5" dx="1.05" dy="1.75" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-1.5" dx="1.05" dy="1.75" layer="1" roundness="25" rot="R270"/>
<text x="1.95" y="-2" size="0.4064" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1.5" y="-2" size="0.4064" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.75" x2="1.27" y2="0.75" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
</package>
<package name="RES2013X38N">
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="21"/>
<smd name="1" x="0" y="1" dx="0.95" dy="1.45" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-1" dx="0.95" dy="1.45" layer="1" roundness="25" rot="R270"/>
<text x="2" y="-2" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1" y="-2" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="51"/>
</package>
<package name="RESC5325X84N">
<wire x1="1.775" y1="3.52" x2="1.775" y2="-3.52" width="0.127" layer="21"/>
<wire x1="1.775" y1="-3.52" x2="-1.775" y2="-3.52" width="0.127" layer="21"/>
<wire x1="-1.775" y1="-3.52" x2="-1.775" y2="3.52" width="0.127" layer="21"/>
<wire x1="-1.775" y1="3.52" x2="1.775" y2="3.52" width="0.127" layer="21"/>
<smd name="1" x="0" y="2.55" dx="1.1" dy="2.65" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-2.55" dx="1.1" dy="2.65" layer="1" roundness="25" rot="R270"/>
<text x="2.45" y="-2" size="0.4064" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-2" y="-2" size="0.4064" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-3" y1="-1.27" x2="3" y2="1.27" layer="39" rot="R270"/>
</package>
<package name="RESC6332X65N">
<wire x1="2" y1="4" x2="2" y2="-4" width="0.127" layer="21"/>
<wire x1="2" y1="-4" x2="-2" y2="-4" width="0.127" layer="21"/>
<wire x1="-2" y1="-4" x2="-2" y2="4" width="0.127" layer="21"/>
<wire x1="-2" y1="4" x2="2" y2="4" width="0.127" layer="21"/>
<smd name="1" x="0" y="3" dx="1.25" dy="3.35" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-3" dx="1.25" dy="3.35" layer="1" roundness="25" rot="R270"/>
<text x="2" y="-2" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1" y="-2" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<wire x1="2" y1="4" x2="2" y2="-4" width="0.127" layer="51"/>
<wire x1="2" y1="-4" x2="-2" y2="-4" width="0.127" layer="51"/>
<wire x1="-2" y1="-4" x2="-2" y2="4" width="0.127" layer="51"/>
<wire x1="-2" y1="4" x2="2" y2="4" width="0.127" layer="51"/>
<rectangle x1="-1.5" y1="-3" x2="1.5" y2="3" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="PIN">
<pin name="1" x="-5.08" y="0" visible="off"/>
<wire x1="-3.302" y1="0" x2="-1.27" y2="0" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="94"/>
<text x="-3.302" y="0.508" size="1.27" layer="95" ratio="15">&gt;NAME</text>
</symbol>
<symbol name="RESISTER">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-1.27" y="1.4986" size="1.27" layer="95">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="96" distance="49">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="JST_PHD_14PIN_2MM" prefix="J" uservalue="yes">
<gates>
<gate name="-1" symbol="PIN" x="5.08" y="20.32"/>
<gate name="-2" symbol="PIN" x="5.08" y="17.78"/>
<gate name="-3" symbol="PIN" x="5.08" y="15.24"/>
<gate name="-4" symbol="PIN" x="5.08" y="12.7"/>
<gate name="-5" symbol="PIN" x="5.08" y="10.16"/>
<gate name="-6" symbol="PIN" x="5.08" y="7.62"/>
<gate name="-7" symbol="PIN" x="5.08" y="5.08"/>
<gate name="-8" symbol="PIN" x="5.08" y="2.54"/>
<gate name="-9" symbol="PIN" x="5.08" y="0"/>
<gate name="-10" symbol="PIN" x="5.08" y="-2.54"/>
<gate name="-11" symbol="PIN" x="5.08" y="-5.08"/>
<gate name="-12" symbol="PIN" x="5.08" y="-7.62"/>
<gate name="-13" symbol="PIN" x="5.08" y="-10.16"/>
<gate name="-14" symbol="PIN" x="5.08" y="-12.7"/>
</gates>
<devices>
<device name="14PIN_2MM" package="JST_PHD_14PIN_2MM">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-10" pin="1" pad="P$10"/>
<connect gate="-11" pin="1" pad="P$11"/>
<connect gate="-12" pin="1" pad="P$12"/>
<connect gate="-13" pin="1" pad="P$13"/>
<connect gate="-14" pin="1" pad="P$14"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
<connect gate="-5" pin="1" pad="P$5"/>
<connect gate="-6" pin="1" pad="P$6"/>
<connect gate="-7" pin="1" pad="P$7"/>
<connect gate="-8" pin="1" pad="P$8"/>
<connect gate="-9" pin="1" pad="P$9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TST-107-X-X-D" prefix="J" uservalue="yes">
<gates>
<gate name="-1" symbol="PIN" x="5.08" y="15.24"/>
<gate name="-2" symbol="PIN" x="5.08" y="12.7"/>
<gate name="-3" symbol="PIN" x="5.08" y="10.16"/>
<gate name="-4" symbol="PIN" x="5.08" y="7.62"/>
<gate name="-5" symbol="PIN" x="5.08" y="5.08"/>
<gate name="-6" symbol="PIN" x="5.08" y="2.54"/>
<gate name="-7" symbol="PIN" x="5.08" y="0"/>
<gate name="-8" symbol="PIN" x="5.08" y="-2.54"/>
<gate name="-9" symbol="PIN" x="5.08" y="-5.08"/>
<gate name="-10" symbol="PIN" x="5.08" y="-7.62"/>
<gate name="-11" symbol="PIN" x="5.08" y="-10.16"/>
<gate name="-12" symbol="PIN" x="5.08" y="-12.7"/>
<gate name="-13" symbol="PIN" x="5.08" y="-15.24"/>
<gate name="-14" symbol="PIN" x="5.08" y="-17.78"/>
</gates>
<devices>
<device name="" package="TST-107-X-X-D">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-10" pin="1" pad="10"/>
<connect gate="-11" pin="1" pad="11"/>
<connect gate="-12" pin="1" pad="12"/>
<connect gate="-13" pin="1" pad="13"/>
<connect gate="-14" pin="1" pad="14"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
<connect gate="-5" pin="1" pad="5"/>
<connect gate="-6" pin="1" pad="6"/>
<connect gate="-7" pin="1" pad="7"/>
<connect gate="-8" pin="1" pad="8"/>
<connect gate="-9" pin="1" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RES_SMD_" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="RESISTER" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="RESC1005X40N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="RESC1608X50N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="RESC2013X70N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="RESC3216X84N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805_SH" package="RES2013X38N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="RESC5325X84N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="RESC6332X65N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="J1" library="JMA_LBR" deviceset="JST_PHD_14PIN_2MM" device="14PIN_2MM"/>
<part name="J2" library="JMA_LBR" deviceset="TST-107-X-X-D" device=""/>
<part name="FRAME1" library="frames" deviceset="A4L-LOC" device=""/>
<part name="R1" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="R2" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="R3" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="-35.56" y="17.78" size="1.778" layer="97">JTAG</text>
<text x="17.78" y="17.78" size="1.778" layer="97">JST</text>
<text x="-38.1" y="15.24" size="1.778" layer="97" rot="R180">TDO/TDI</text>
<text x="-38.1" y="10.16" size="1.778" layer="97" rot="R180">TDI/VPP</text>
<text x="-38.1" y="5.08" size="1.778" layer="97" rot="R180">TMS</text>
<text x="-38.1" y="0" size="1.778" layer="97" rot="R180">TCK</text>
<text x="-38.1" y="-5.08" size="1.778" layer="97" rot="R180">GND</text>
<text x="-38.1" y="-10.16" size="1.778" layer="97" rot="R180">RST</text>
<text x="-38.1" y="-15.24" size="1.778" layer="97" rot="R180">NC</text>
<text x="-38.1" y="12.7" size="1.778" layer="97" rot="R180">VCC TOOL(&lt;--)</text>
<text x="-38.1" y="7.62" size="1.778" layer="97" rot="R180">VCC TARGET(--&gt;)</text>
<text x="-38.1" y="2.54" size="1.778" layer="97" rot="R180">NC</text>
<text x="-38.1" y="-7.62" size="1.778" layer="97" rot="R180">NC</text>
<text x="-38.1" y="-12.7" size="1.778" layer="97" rot="R180">NC</text>
<text x="-38.1" y="-17.78" size="1.778" layer="97" rot="R180">NC</text>
<text x="-38.1" y="-2.54" size="1.778" layer="97" rot="R180">TEST/VPP</text>
<text x="40.64" y="-17.78" size="1.778" layer="97">TDO</text>
<text x="40.64" y="-12.7" size="1.778" layer="97">TDI</text>
<text x="40.64" y="-7.62" size="1.778" layer="97">TMS</text>
<text x="40.64" y="-2.54" size="1.778" layer="97">TCK</text>
<text x="40.64" y="2.54" size="1.778" layer="97">GND</text>
<text x="40.64" y="7.62" size="1.778" layer="97">RST</text>
<text x="40.64" y="12.7" size="1.778" layer="97">NC</text>
<text x="40.64" y="-15.24" size="1.778" layer="97">VCC NC</text>
<text x="40.64" y="-10.16" size="1.778" layer="97">+3.3v</text>
<text x="40.64" y="-5.08" size="1.778" layer="97">NC</text>
<text x="40.64" y="0" size="1.778" layer="97">TEST</text>
<text x="40.64" y="5.08" size="1.778" layer="97">NC</text>
<text x="40.64" y="10.16" size="1.778" layer="97">NC</text>
<text x="40.64" y="15.24" size="1.778" layer="97">NC</text>
</plain>
<instances>
<instance part="J1" gate="-1" x="35.56" y="15.24" smashed="yes">
<attribute name="NAME" x="32.258" y="15.748" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-2" x="35.56" y="12.7" smashed="yes">
<attribute name="NAME" x="32.258" y="13.208" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-3" x="35.56" y="10.16" smashed="yes">
<attribute name="NAME" x="32.258" y="10.668" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-4" x="35.56" y="7.62" smashed="yes">
<attribute name="NAME" x="32.258" y="8.128" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-5" x="35.56" y="5.08" smashed="yes">
<attribute name="NAME" x="32.258" y="5.588" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-6" x="35.56" y="2.54" smashed="yes">
<attribute name="NAME" x="32.258" y="3.048" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-7" x="35.56" y="0" smashed="yes">
<attribute name="NAME" x="32.258" y="0.508" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-8" x="35.56" y="-2.54" smashed="yes">
<attribute name="NAME" x="32.258" y="-2.032" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-9" x="35.56" y="-5.08" smashed="yes">
<attribute name="NAME" x="32.258" y="-4.572" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-10" x="35.56" y="-7.62" smashed="yes">
<attribute name="NAME" x="32.258" y="-7.112" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-11" x="35.56" y="-10.16" smashed="yes">
<attribute name="NAME" x="32.258" y="-9.652" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-12" x="35.56" y="-12.7" smashed="yes">
<attribute name="NAME" x="32.258" y="-12.192" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-13" x="35.56" y="-15.24" smashed="yes">
<attribute name="NAME" x="32.258" y="-14.732" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-14" x="35.56" y="-17.78" smashed="yes">
<attribute name="NAME" x="32.258" y="-17.272" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J2" gate="-1" x="-33.02" y="15.24" smashed="yes" rot="R180">
<attribute name="NAME" x="-29.718" y="14.732" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J2" gate="-2" x="-33.02" y="12.7" smashed="yes" rot="R180">
<attribute name="NAME" x="-29.718" y="12.192" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J2" gate="-3" x="-33.02" y="10.16" smashed="yes" rot="R180">
<attribute name="NAME" x="-29.718" y="9.652" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J2" gate="-4" x="-33.02" y="7.62" smashed="yes" rot="R180">
<attribute name="NAME" x="-29.718" y="7.112" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J2" gate="-5" x="-33.02" y="5.08" smashed="yes" rot="R180">
<attribute name="NAME" x="-29.718" y="4.572" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J2" gate="-6" x="-33.02" y="2.54" smashed="yes" rot="R180">
<attribute name="NAME" x="-29.718" y="2.032" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J2" gate="-7" x="-33.02" y="0" smashed="yes" rot="R180">
<attribute name="NAME" x="-29.718" y="-0.508" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J2" gate="-8" x="-33.02" y="-2.54" smashed="yes" rot="R180">
<attribute name="NAME" x="-29.718" y="-3.048" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J2" gate="-9" x="-33.02" y="-5.08" smashed="yes" rot="R180">
<attribute name="NAME" x="-29.718" y="-5.588" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J2" gate="-10" x="-33.02" y="-7.62" smashed="yes" rot="R180">
<attribute name="NAME" x="-29.718" y="-8.128" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J2" gate="-11" x="-33.02" y="-10.16" smashed="yes" rot="R180">
<attribute name="NAME" x="-29.718" y="-10.668" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J2" gate="-12" x="-33.02" y="-12.7" smashed="yes" rot="R180">
<attribute name="NAME" x="-29.718" y="-13.208" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J2" gate="-13" x="-33.02" y="-15.24" smashed="yes" rot="R180">
<attribute name="NAME" x="-29.718" y="-15.748" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J2" gate="-14" x="-33.02" y="-17.78" smashed="yes" rot="R180">
<attribute name="NAME" x="-29.718" y="-18.288" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="FRAME1" gate="G$1" x="-132.08" y="-91.44"/>
<instance part="R1" gate="G$1" x="-10.16" y="12.7" smashed="yes">
<attribute name="NAME" x="-17.78" y="12.7" size="1.27" layer="95"/>
<attribute name="VALUE" x="-12.7" y="13.97" size="1.27" layer="96"/>
</instance>
<instance part="R2" gate="G$1" x="-10.16" y="7.62" smashed="yes">
<attribute name="NAME" x="-17.78" y="7.62" size="1.27" layer="95"/>
<attribute name="VALUE" x="-12.7" y="9.144" size="1.27" layer="96"/>
</instance>
<instance part="R3" gate="G$1" x="5.08" y="12.7" smashed="yes" rot="R180">
<attribute name="NAME" x="0" y="12.7" size="1.27" layer="95"/>
<attribute name="VALUE" x="2.54" y="14.224" size="1.27" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$5" class="0">
<segment>
<pinref part="J1" gate="-12" pin="1"/>
<wire x1="-22.86" y1="-12.7" x2="30.48" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="J2" gate="-3" pin="1"/>
<wire x1="-27.94" y1="10.16" x2="-22.86" y2="10.16" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="10.16" x2="-22.86" y2="-12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="J2" gate="-5" pin="1"/>
<wire x1="-27.94" y1="5.08" x2="-20.32" y2="5.08" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="5.08" x2="-20.32" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="J1" gate="-10" pin="1"/>
<wire x1="-20.32" y1="-7.62" x2="30.48" y2="-7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="J2" gate="-7" pin="1"/>
<wire x1="-27.94" y1="0" x2="22.86" y2="0" width="0.1524" layer="91"/>
<wire x1="22.86" y1="0" x2="22.86" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="J1" gate="-8" pin="1"/>
<wire x1="22.86" y1="-2.54" x2="30.48" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="J2" gate="-9" pin="1"/>
<wire x1="-27.94" y1="-5.08" x2="25.4" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="25.4" y1="-5.08" x2="25.4" y2="2.54" width="0.1524" layer="91"/>
<pinref part="J1" gate="-6" pin="1"/>
<wire x1="25.4" y1="2.54" x2="30.48" y2="2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="J2" gate="-11" pin="1"/>
<wire x1="-27.94" y1="-10.16" x2="-15.24" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="-10.16" x2="-15.24" y2="2.54" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="2.54" x2="22.86" y2="2.54" width="0.1524" layer="91"/>
<wire x1="22.86" y1="2.54" x2="22.86" y2="7.62" width="0.1524" layer="91"/>
<pinref part="J1" gate="-4" pin="1"/>
<wire x1="22.86" y1="7.62" x2="30.48" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="J2" gate="-8" pin="1"/>
<wire x1="-27.94" y1="-2.54" x2="-17.78" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="-2.54" x2="-17.78" y2="5.08" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="5.08" x2="27.94" y2="5.08" width="0.1524" layer="91"/>
<wire x1="27.94" y1="5.08" x2="27.94" y2="0" width="0.1524" layer="91"/>
<pinref part="J1" gate="-7" pin="1"/>
<wire x1="27.94" y1="0" x2="30.48" y2="0" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="J2" gate="-1" pin="1"/>
<wire x1="-27.94" y1="15.24" x2="-25.908" y2="15.24" width="0.1524" layer="91"/>
<pinref part="J1" gate="-14" pin="1"/>
<wire x1="-25.908" y1="-17.78" x2="30.48" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="-25.908" y1="15.24" x2="-25.908" y2="-17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="J1" gate="-11" pin="1"/>
<wire x1="-2.54" y1="-10.16" x2="30.48" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="-5.08" y1="12.7" x2="-2.54" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="12.7" x2="0" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="-10.16" x2="-2.54" y2="12.7" width="0.1524" layer="91"/>
<junction x="-2.54" y="12.7"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="J1" gate="-13" pin="1"/>
<wire x1="30.48" y1="-15.24" x2="10.16" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="-5.08" y1="7.62" x2="10.16" y2="7.62" width="0.1524" layer="91"/>
<wire x1="10.16" y1="7.62" x2="10.16" y2="12.7" width="0.1524" layer="91"/>
<wire x1="10.16" y1="-15.24" x2="10.16" y2="7.62" width="0.1524" layer="91"/>
<junction x="10.16" y="7.62"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="-27.94" y1="12.7" x2="-15.24" y2="12.7" width="0.1524" layer="91"/>
<pinref part="J2" gate="-2" pin="1"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="J2" gate="-4" pin="1"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="-27.94" y1="7.62" x2="-15.24" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
