#ifndef __SPI
#define __SPI

#include "processor_Def.h"

#define EXT_SPI_POUT           P9OUT
#define EXT_SPI_PIN            P9IN
// redefined 
#define EXT_SPI_BIT            BIT6
#define EXT_MISO_BIT           BIT2

typedef struct s_DAC_SPI_Msg
{
  unsigned char OVP_Reg;
  unsigned char OVP_DAC_data_H;
  unsigned char OVP_DAC_data_L; 
}DAC_SPI_Msg;

void SetupSPI_DAC();
void SendSPI_D2AConvertorMsg(unsigned char Reg_Type, unsigned char DAC_Channel, unsigned int Raw_OVP_Data);
char init_SpiAx(char spi_ucaX);

#endif