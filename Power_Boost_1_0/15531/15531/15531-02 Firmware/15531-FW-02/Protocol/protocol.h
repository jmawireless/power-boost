/***************************************************************************//**
 *  \file         protocol.h
 *  \brief        Program include file and function declaration for 
 *                communication protocol
 *  \author       RBG
 *  \date         December,2017
 *  \version      1.0.0
 ******************************************************************************/
#ifndef __PROTOCOL
#define __PROTOCOL
/***************************************************************************//**
 * FILE INCLUDES
 ******************************************************************************/
#include <string.h>
#include "processor_Def.h"
  
/***************************************************************************//**
 * CONSTANT DECLARATION
 ******************************************************************************/   
  #define MAX_OVP_PER_MSG         12    // 12 OVP voltages per channel
  #define MAX_DEVICE_ID           5     // 5 device id's that can be connected
  #define MAX_MODBUS_DATA_LENGTH  6     // Max Rx byte = 8 - 2 (slave and function) 

/***************************************************************************//**
 * STRUCTURE DECLARATION
 ******************************************************************************/
/*! \struct s_Data_Msg
    \brief Records OVP data with error flag
*/
typedef struct s_Data_Msg
{
  unsigned char Reg_data_h : 4;         // LSB nibble
  unsigned char Reg_Flag : 4;           // MSB nibble
  unsigned char Reg_data_l;
} Data_Msg;

typedef struct s_PBC_Mapping
{
  unsigned char Set_High_Flag;
  unsigned char Set_Low_Flag;
  unsigned char Set_Avg_Flag; 
  unsigned char Msg_OVP_Id_High[2];
  unsigned char Msg_OVP_Id_Low[2];
  unsigned char Msg_OVP_Id_Avg[2];
//  unsigned char Msg_OVP_Id2_High;
//  unsigned char Msg_OVP_Id2_Low;
//  unsigned char Msg_OVP_Id2_Avg;
  unsigned int PBC_Module_address[2];
} PBC_Mapping;

typedef struct s_PBC_Info
{
  unsigned char Update_Hybrid_Ch_Status;
  unsigned char Invalid_OVP_Flag;
  unsigned char Configured_OVP_Flag;
  unsigned char Adj_Ch_Paired_Flag;
  unsigned char Hybrid_Current_High;
  unsigned char Hybrid_Current_Low;
  unsigned char Hybrid_Ch_Status;
  unsigned char Linked_RRH_High;
  unsigned char Linked_RRH_Low;
  unsigned char Ovp_Msg_Location;
}PBC_Info;

/*! \struct s_PBC_Boost_Msg
    \brief Records PBC Tx message information
*/
typedef struct s_PBC_Boost_Tx_Msg
{
  unsigned char Slave_Addr;           // Slave Address Fixed to 01
  unsigned char Function;             // Modbus Function fixed to 06  
  unsigned char Reg_Addr_Hi;          // Slave Register Address High Byte
  unsigned char Reg_Addr_Lo;          // Slave Regiater Address Low Byte
  unsigned char Write_Data_Hi:4;      // Write date higher nibble  
  unsigned char Alarm:4;              // Alarm (refer table)
  unsigned char Write_Data_Lo;        // Write data lower byte
  unsigned char Error_Check_Lo;       // Error Check Low
  unsigned char Error_Check_Hi;       // Error Checl High
} PBC_Boost_Msg;

typedef struct s_PBC_Cmd_Addr_Info
{
  unsigned int Base_Address;
  unsigned char Device_id;
  unsigned char Ovp_id;
} PBC_Cmd_Addr_Info;

/*! \struct s_PBC_Boost_Msg
    \brief Records PBC Tx message information
*/
typedef struct s_PBC_Boost_Rx_LengthMsg
{
  unsigned char Slave_Addr;             // Slave Address Fixed to 01
  unsigned char Function;               // Modbus Function fixed to 06  
//  unsigned char Byte_Count;             // Slave Register Address High Byte
  unsigned char Data[MAX_MODBUS_DATA_LENGTH];   // Data
//  unsigned char Error_Check_Lo;         // Error Check Low
//  unsigned char Error_Check_Hi;         // Error Checl High
} PBC_Boost_Rx_LengthMsg;

///*! \struct s_PBC_Boost_Error_Msg
//    \brief Records PBC Rx message information
//*/
//typedef struct s_PBC_Boost_Error_Msg
//{
//  unsigned char Slave_Addr;           // Slave Address Fixed to 01
//  unsigned char Function;             // Modbus Function fixed to 06  
//  unsigned char Exception_Code;       // Exception Code
//  unsigned char Error_Check_Lo;       // Error Check Low
//  unsigned char Error_Check_Hi;       // Error Checl High
//} PBC_Boost_Error_Msg;

/*! \struct s_Power_Boost_Msg
    \brief  Records surge protection device Rx message format
*/
typedef struct s_Power_Boost_Msg 
{
  unsigned char start_byte_1;
  unsigned char start_byte_2; 
  unsigned char Device_Id : 3;          // LSB nibble        
  unsigned char System_Alarm : 5;       // MSB nibble
  unsigned char msg_length_l;
  struct s_Data_Msg Data_Msg[MAX_OVP_PER_MSG];
  unsigned char msg_checksum;
} Power_Boost_Msg;

/*! \struct s_Previous_Msg
    \brief Structure records OVP raw voltage information and processing information
*/
typedef struct s_Previous_Msg 
{
  struct s_Data_Msg Power_Boost_Processed_Msg[MAX_DEVICE_ID][MAX_OVP_PER_MSG];
  struct s_Data_Msg OVP_Raw_Tx_Data[MAX_DEVICE_ID][MAX_OVP_PER_MSG];
  struct s_Data_Msg OVP_Raw_Rx_Data[MAX_DEVICE_ID][MAX_OVP_PER_MSG];
  struct s_PBC_Mapping PBC_Register_Map[MAX_DEVICE_ID][MAX_OVP_PER_MSG];
  struct s_PBC_Info PBC_Controller_Info[MAX_DEVICE_ID][MAX_OVP_PER_MSG];
  unsigned char OVP_Processed_Flag[MAX_DEVICE_ID][MAX_OVP_PER_MSG];
} Previous_Msg;

/*! \struct s_Diagnostics
    \brief  Records diognostic data in case of invalid messages received
*/
typedef struct s_Diagnostics
{
  unsigned char Failed_Packet_Count[MAX_DEVICE_ID][MAX_OVP_PER_MSG];
  unsigned char Device_Id_Failure_Flag[MAX_DEVICE_ID]; // Bit 0 = No Commuication with RC, Bit1 = ID1, Bit2 = ID2, Bit3 = ID3, Bit4 = ID4, Bit5 = ID5,..
  unsigned int OVP_Id_Failure_Flag[MAX_DEVICE_ID][MAX_OVP_PER_MSG]; // Bit 0 = Future, Bit1 = OVP1, Bit2 = OVP2, Bit3 = OVP3 .... Bit12 = OVP12
  unsigned char OVP_Processed_Flag[MAX_DEVICE_ID][MAX_OVP_PER_MSG];
} Msg_Diagnostics;

/*! \struct s_Power_Boost_Flag
    \brief Structure is templete for PBC Rx message
*/
typedef struct s_Power_Boost_Flag
{
  unsigned char msg_start_byte1_rx;
  unsigned char msg_start_byte2_rx;
  unsigned char end_byte_rx;
  unsigned char Expected_Rx_Byte;
//  unsigned char PBC_Rx_Msg_Type; // 0 = Valid response !0: Error code 
}Power_Boost_Rx_Flag;

/*! \struct s_Config_Flag
    \brief Records and tracks the configuration status of OVP
*/
typedef struct s_Config_Flag 
{
  unsigned char Config_Busy_flag;
  unsigned char Config_Busy_id;
  unsigned char Config_Busy_ovp_id;
  unsigned char Config_Wait_Count;
}Config_Flag;

/*! \struct s_Beat_Msg
    \brief Records heartbeat count and flag to indicate when it is time to send 
           heartbeat
*/
typedef struct s_Beat_Msg
{
  unsigned int heartbeat_count;
  unsigned char send_heartbeat_flag; 
} HeartBeat_Msg;



/***************************************************************************//**
 * VARIABLE DECLARATION
 ******************************************************************************/
unsigned char RxBuffer[sizeof(Power_Boost_Msg)];
unsigned char PBC_RxBuffer[sizeof(PBC_Boost_Msg)];
unsigned char RxBuffer_Count;
unsigned char PBC_RxBuffer_Count;

/***************************************************************************//**
 * FUNCTION AND EXTERN FUNCTION DECLARATION
 ******************************************************************************/
void Setup_PBC();
void Process_OVP_Message();
void Send_UART_Debug_Info();
void Process_OVP_Rx_data(unsigned char rx_data);
void Record_PBC_Rx_Msg(unsigned char pbc_rx_data);
unsigned char Verify_OVP_Msg_End_Flag_Rx();
unsigned char Process_PBC_HeartBeat_Message();

extern void Clear_Timer_Count();
extern void Clear_Heartbeat_Count();
extern unsigned char Verify_OVP_Communication_Flag();
extern void Change_Led( unsigned char led_name, unsigned char state); 
extern void Set_Fault_Ctrl(unsigned char Led_No, unsigned char state);
extern void SendD2AConvertorMsg(unsigned char Reg_Type, unsigned char DAC_Channel, unsigned int Raw_OVP_Data);
extern void SendSPI_D2AConvertorMsg(unsigned char Reg_Type, unsigned char DAC_Channel, unsigned int Raw_OVP_Data);
extern void Set_Uart_Int_Flag(unsigned char uart_ucaX);
extern void Disable_Uart_Rx_Int(unsigned char uart_ucaX);
extern void Enable_Uart_Rx_Int(unsigned char uart_ucaX);

#endif