#include "processor_Def.h"

/***************************************************************************//**
 * @param Function      init_ADC()
 * @param Input         void
 * @param Return        void
 * @param Func_Type     Hardware - Processor
 * @param Description   Used to initialize ADC config
 ******************************************************************************/
void init_ADC();

/***************************************************************************//**
 * @param Function      Start_TemperatureConversion()
 * @param Input         void
 * @param Return        void
 * @param Func_Type     Hardware - Processor
 * @param Description   Used to measure device temperature
 ******************************************************************************/
unsigned int Start_AnalogConversion();
