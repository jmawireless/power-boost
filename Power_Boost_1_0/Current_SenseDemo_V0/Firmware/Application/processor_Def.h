#ifndef __PROCESSOR_DEF
#define __PROCESSOR_DEF

/***************************************************************************//**
 *                           FILE INCLUDES
 ******************************************************************************/
#include "msp430f5438a.h"                     // Device specific definitions

/***************************************************************************//**
 *                           CONSTANT DEFINE
 ******************************************************************************/
   

#define RX                      0
#define OFF                     0 
#define TX                      1
#define ON                      1   
#define TOGGLE                  2   
   
#define Red_Status_Led          0
#define Yellow_Status_Led       1       
#define Green_Status_Led        2
   
//#define NULL                                (void *)0

/* AUTO-GENERATED FILE. DO NOT MODIFY. */
/*  MACROS  */
#ifndef ROUND
#define ROUND(X) ((X) >= 0 ? (long)((X) + 0.5) : (long)((X) - 0.5))
#endif

/***************************************************************************//**
 *                         SYSTEM CONFIGURATION
 ******************************************************************************/

/* SYSTEM CONFIGURATION  */
/* Main system desired clock frequency */
#define SYS_CLOCK_FREQ_HZ                 8000000               // System clock frequency

/* Clock sources */
#define SYS_MCLK_SOURCE                 SELM__DCOCLKDIV       // Main Clock source select:
                                                              //    (SELM__XT1CLK) XT1CLK,
                                                              //    (SELM__VLOCLK) VLOCLK,
                                                              //    (SELM__REFOCLK) REFOCLK,
                                                              //    (SELM__DCOCLK) DCOCLK,
                                                              //    (SELM__DCOCLKDIV) DCOCLKDIV,
                                                              //    (SELM__XT2CLK) XT2CLK
#define SYS_SMCLK_SOURCE                SELS__DCOCLKDIV       // Sub Main Clock source select:
                                                              //    (SELS__XT1CLK) XT1CLK,
                                                              //    (SELS__VLOCLK) VLOCLK,
                                                              //    (SELS__REFOCLK) REFOCLK,
                                                              //    (SELS__DCOCLK) DCOCLK,
                                                              //    (SELS__DCOCLKDIV) DCOCLKDIV,
                                                              //    (SELS__XT2CLK) XT2CLK
#define SYS_ACLK_SOURCE                 SELA__XT1CLK          // Auxilary Clock source select:
                                                              //    (SELA__XT1CLK) XT1CLK,
                                                              //    (SELA__VLOCLK) VLOCLK,
                                                              //    (SELA__REFOCLK) REFOCLK,
                                                              //    (SELA__DCOCLK) DCOCLK,
                                                              //    (SELA__DCOCLKDIV) DCOCLKDIV,
                                                              //    (SELA__XT2CLK) XT2CLK

/* XT1 External Crystal */
#define SYS_XT1_PSEL                    P7SEL                    
#define SYS_XT1_XINSEL                  BIT0                  // XIN function: (0) off, (1) on
#define SYS_XT1_XOUTSEL                 BIT1                  // XOUT function (0) off, (1) on
#define SYS_XT1_BYPASS                  XT1BYPASS             // XT1 bypass enable
#define SYS_XT1_XCAP                    XCAP_2                // XIN/XOUT caps: (XCAP_0) 2pF,
                                                              // (XCAP_1) 6pF,
                                                              // (XCAP_2) 9pF,
                                                              // (XCAP_3) 12 pF
#define SYS_XT1_OFF                     XT1OFF                // High frequency oscillator disable
#define SYS_XT1_XTS                     XTS                   // Select high frequency oscillator


/* Frequency Locked Loop (FLL) */
#define SYS_FLL_REFERENCE               SELREF__XT1CLK        // FLL reference: (SELREF__REFOCLK) REFOCLK,
                                                              // (SELREF__XT1CLK) XT1CLK,
                                                              // (SELREF__XT2CLK) XT2CLK
#define SYS_FLL_MOD_COUNTER             0x00                  // FLL modulation counter (modified automatically during FLL operation)
#define SYS_FLL_LOOPDIV                 FLLD_1                // FLL loopback divider: (FLLD_0) 1,
                                                              // (FLLD_1) 2,
                                                              // (FLLD_2) 4,
                                                              // (FLLD_3) 8,
                                                              // (FLLD_4) 16,
                                                              // (FLLD_5, FLLD_6, FLLD_7) 32
#define SYS_FLL_PRESCALER               1                     // FLL prescaler (1, 2, 4, 8, 16, 32)
#define SYS_FLL_REFCLK_HZ               32768                 // FLL reference clock frequency (Hz)
#define SYS_FLL_REFDIV                  1                     // FLL reference divider (1, 2, 4, 8, 16, 32)

/* Digitally Controlled Oscillator (DCO) */
#define SYS_DCO_RANGE_SELECT            DCORSEL_4             // DCO range select: (DCORSEL_4) 1.3-28.2MHz
#define SYS_DCO_TAP                     0x00                  // DCO tap select (modified automatically during FLL operation)
#define SYS_DISMOD                      0x00
#define SYS_DCO_MULTIPLIER_BITS      ((SYS_CLOCK_FREQ_HZ / (SYS_FLL_PRESCALER * (SYS_FLL_REFCLK_HZ / SYS_FLL_REFDIV))) - 1)               // Calculated DCO multiplier bits

/* Calculate true system clock frequency in hertz */
#define SYSTEM_TRUE_CLOCK_HZ            (SYS_FLL_PRESCALER * (SYS_DCO_MULTIPLIER_BITS + 1) * (SYS_FLL_REFCLK_HZ / SYS_FLL_REFDIV))      // Calculated system frequency using desired clock frequency


/***************************************************************************//**
 *                         LPM CONFIGURATION
 ******************************************************************************/   
#define LPM0                            __bis_SR_register(LPM0_bits)
#define LPM1                            __bis_SR_register(LPM1_bits)
#define LPM2                            __bis_SR_register(LPM2_bits)
#define LPM3                            __bis_SR_register(LPM3_bits)

#endif