/***************************************************************************//**
 *                           FILE INCLUDES
 ******************************************************************************/
#include "sys_timer.h"

Failure_Timer Failure_Timer_1Sec = 
{
  Max_Timer_Count,
  0x0000,
  0x00,
};

/***************************************************************************//**
 *                           FUNCTION DEFINATION
 ******************************************************************************/

/***************************************************************************//**
 * Timer_A0_Configure() {
 ******************************************************************************/
void Timer_A0_Configure() {
  TA0CTL |= MC_0;                           // Ensure the timer is stopped
  TA0CCTL1 = CCIE;                          // CCR1 interrupt enabled
  TA0CTL |= TASSEL_1 | TACLR;               // Run the timer of the ACLK + UP mode + Start up clean
  TA0CTL |= MC_1;                           // Timer Set to Up Mode
}

/***************************************************************************//**
 * Timer_B0_Configure()
 ******************************************************************************/
void Timer_B0_Configure() {
  TB0CTL |= MC_0;                           // Ensure the timer is stopped
  TB0CCTL1 = CCIE;                          // CCR1 interrupt enabled
  TB0CTL |= TBSSEL_1 | TBCLR;               // Run the timer of the ACLK + Start up clean
  TB0CTL |= MC_1;                           // Timer Set to Up Mode
}

/***************************************************************************//**
 * Stop_Timer_A0()
 ******************************************************************************/
void Stop_Timer_A0(){
  TA0CTL = MC_0;
  // Alternate Method
  //TA0CTL = 0 ;
}

/***************************************************************************//**
 * Stop_Timer_B0()
 ******************************************************************************/
void Stop_Timer_B0(){
  TB0CTL = MC_0;
  // Alternate Method
  //TB0CTL = 0 ;
}

/***************************************************************************//**
 * Set_Timer_A0_CCR
 ******************************************************************************/
void Set_Timer_A0_CCR(unsigned char reg, unsigned int val) {
  /* Change corresponding register */
  switch (reg) {
  case 0:                                   // Capture/compare register 0
    TA0CCR0 = val;
    break;
  case 1:                                   // Capture/compare register 1
    TA0CCR1 = val;
    break;
  case 2:                                   // Capture/compare register 2
    TA0CCR2 = val;
    break;
  default: break;
  }
}

/***************************************************************************//**
 * Set_Timer_B0_CCR
******************************************************************************/
void Set_Timer_B0_CCR(unsigned char reg, unsigned int val) {
  /* Change corresponding register */
  switch (reg) {
  case 0:                                   // Capture/compare register 0
    TB0CCR0 = val;
    break;
  case 1:                                   // Capture/compare register 1
    TB0CCR1 = val;
    break;
  case 2:                                   // Capture/compare register 2
    TB0CCR2 = val;
    break;
  default: break;
  }
}

/***************************************************************************//**
 * Set_Timer_A0_Overflow_Counter(unsigned short int)
 ******************************************************************************/
void Set_Timer_A0_Overflow_Counter(unsigned short int val) {
  TA0R = val;
}

/***************************************************************************//**
 * Set_Timer_B0_Overflow_Counter(unsigned short int)
 ******************************************************************************/
void Set_Timer_B0_Overflow_Counter(unsigned short int val) {
  TB0R = val;
}

/***************************************************************************//**
 * Update_Timer_Count()
 ******************************************************************************/
void Update_Timer_Count()
{
  if (Failure_Timer_1Sec.TimerOF_Flag == 0)
  {
    Failure_Timer_1Sec.TimerCount++;

    if (Failure_Timer_1Sec.TimerCount > Max_Application_Count)
    {
      Failure_Timer_1Sec.TimerOF_Flag = 1;
      Change_Led(Green_Status_Led, TOGGLE);
    }
    else
    {
        Change_Led(Green_Status_Led, TOGGLE);
    }
  }
}

void Clear_Timer_Count()
{
  Failure_Timer_1Sec.TimerOF_Flag = 0;
  Failure_Timer_1Sec.TimerCount = 0x0000;
}

unsigned char Timer_Count_Expired()
{
  return Failure_Timer_1Sec.TimerOF_Flag;
}

/***************************************************************************//**
 * __interrupt void TIMER0_A1_ISR(void)
 ******************************************************************************/
// Timer_A1 Interrupt Vector (TAIV) handler
#pragma vector=TIMER0_INTA1
__interrupt void TIMER0_A1_ISR(void)
{
  LPM0_EXIT;
  switch(__even_in_range(TA0IV,14))
   {
    case 0: break;
    case 2:                 // CCR1 not used
      
    Update_Timer_Count();
    break;
    case 4:  break;         // CCR2 not used
    case 6:  break;         // CCR3 not used
    case 8:  break;         // CCR4 not used
    case 10: break;         // CCR5 not used
    case 12: break;         // Reserved not used
    case 14: break;
    default: break;
  }
   // Execute 5.5Sec failure mode
}

/***************************************************************************//**
 * __interrupt void TIMER0_B1_ISR(void)
 ******************************************************************************/
// Timer1_A3 Interrupt Vector (TAIV) handler
#pragma vector=TIMER0_INTB1
__interrupt void TIMER2_A1_ISR(void)
{
  LPM0_EXIT;
  switch(__even_in_range(TB0IV,14))
  {
    case  0: break;                           // No interrupt
    case  2:                                  // CCR1 not used
        
//        DM_SchedularMain_Func();
      break;

    case  4: break;                           // CCR2 not used
    case  6: break;                           // reserved
    case  8: break;                           // reserved
    case 10: break;                           // reserved
    case 12: break;                           // reserved
    case 14: break;                           // overflow
    default: break;
  }
}


void init_Timer()
{
  Timer_A0_Configure();
  Timer_B0_Configure();
  Set_Timer_A0_CCR(0,Max_Timer_Count);
  Set_Timer_A0_CCR(1,Max_Timer_Count);
  Clear_Timer_Count(); 
  Change_Led(Red_Status_Led, OFF);
  Change_Led(Yellow_Status_Led, OFF);
  Change_Led(Green_Status_Led, ON);
}

/***************************************************************************//**
 *                         End of Function
 ******************************************************************************/