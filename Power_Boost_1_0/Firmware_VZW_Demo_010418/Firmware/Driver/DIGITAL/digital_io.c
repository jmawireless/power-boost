/***************************************************************************//**
* FILE INCLUDES
*******************************************************************************/
#include "digital_io.h"

/***************************************************************************//**
 * FUNCTION DEFINATION
 ******************************************************************************/
/***************************************************************************//**
 * @param Function      
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  
 * @param Description   
 ******************************************************************************/
void init_Hardware()
{
  /********************** PORT 1 Defination**********************
   P1.0 - TP9, Default IN 
   P1.1 - TP10, Default IN
   P1.2 - NC
   P1.3 - NC
   P1.4 - NC
   P1.5 - NC
   P1.6 - NC
   P1.7 - NC
**************************************************************/    
  P1SEL &= ~(TP9_BIT + TP10_BIT + BIT2 + BIT3 + BIT4 + BIT5 + BIT6 + BIT7);
  P1DIR &= ~(TP9_BIT + TP10_BIT + BIT2 + BIT3 + BIT4 + BIT5 + BIT6 + BIT7);
   
 /********************** PORT 2 Defination**********************
   P2.0 - TP2, Default IN 
   P2.1 - TP1, Default IN 
   P2.2 - TP4, Default IN 
   P2.3 - TP3, Default IN 
   P2.4 - NC
   P2.5 - NC
   P2.6 - NC
   P2.7 - TP6, Default IN 
**************************************************************/      
  P2SEL &= ~(PBC_ERROR_BIT + TP1_BIT + TP4_BIT + TP3_BIT + BIT4 + BIT5 + BIT6 + BIT7);
  P2DIR &= ~(TP1_BIT + TP4_BIT + TP3_BIT + BIT4 + BIT5 + BIT6 + TP6_BIT);
  P2DIR |=  (PBC_ERROR_BIT);
 /********************** PORT 3 Defination**********************
   P3.0 - TP5, Default IN         
   P3.1 - TP8, Default IN                          
   P3.2 - TP7, Default IN                
   P3.3 - NC           
   P3.4 - PBC_TX - Delta              
   P3.5 - PBC_RX - Delta             
   P3.6 - NC    
   P3.7 - NC      
**************************************************************/     
  P3SEL &= ~(TP5_BIT + TP8_BIT + TP7_BIT + BIT3 + BIT6 + BIT7);
  P3SEL |=  (PBC_TX_BIT + PBC_RX_BIT);
  P3DIR &= ~(TP5_BIT + TP8_BIT + TP7_BIT + BIT3 + BIT6 + BIT7);
 
 /********************** PORT 4 Defination**********************
   P4.0 - NC
   P4.1 - NC
   P4.2 - NC
   P4.3 - NC
   P4.4 - NC
   P4.5 - NC  
   P4.6 - PBC_DE, Default IN 
   P4.7 - PBC_RE, Default IN 
**************************************************************/     
  P4SEL &= ~(BIT0 + BIT1 + BIT2 + BIT3 + BIT4 + BIT5 + PBC_DE + PBC_RE);
  P4DIR &= ~(BIT0 + BIT1 + BIT2 + BIT3 + BIT4 + BIT5 + PBC_DE);
  P4DIR |=  PBC_RE;
  P4OUT &= ~PBC_RE;
/********************** PORT 5 Defination**********************
   P5.0 - NC
   P5.1 - NC
   P5.2 - NC
   P5.3 - NC
   P5.4 - COM_RX_LED
   P5.5 - COM_TX_LED
   P5.6 - COM_DBG_UART_TX
   P5.7 - COM_DBG_UART_RX
**************************************************************/       
  P5SEL &=  ~(BIT0 + BIT1 + BIT2 + BIT3 + COM_RX_LED_BIT + COM_TX_LED_BIT);
  P5SEL |=   (COM_DBG_UART_TX_BIT + COM_DBG_UART_RX_BIT);
  P5DIR |=   (COM_RX_LED_BIT + COM_TX_LED_BIT);
//  P5DIR &=  ~(BIT0 + BIT1 + BIT2 + BIT3);
  P5OUT &=  ~(COM_RX_LED_BIT + COM_TX_LED_BIT);
  
  /********************** PORT 6 Defination**********************
   P6.0 - LED_1
   P6.1 - LED_2
   P6.2 - LED_3
   P6.3 - LED_4
   P6.4 - TP11
   P6.5 - TP12 
   P6.6 - TP13
   P6.7 - TP14
**************************************************************/   
  P6SEL &= ~(LED_1_BIT + LED_2_BIT + LED_3_BIT + LED_4_BIT + TP11_BIT + TP12_BIT + TP13_BIT + TP14_BIT);
  P6DIR =   (LED_1_BIT + LED_2_BIT + LED_3_BIT + LED_4_BIT);
  P6OUT =    0x00;
//  P6OUT &= ~(LED_1_BIT + LED_2_BIT + LED_3_BIT + LED_4_BIT);
  
 /********************** PORT 7 Defination**********************
   P7.0 - XIN - SP, IN
   P7.1 - XOUT - SP, OUT
   P7.2 - NC
   P7.3 - NC
   P7.4 - NC
   P7.5 - NC
   P7.6 - NC
   P7.7 - NC
**************************************************************/ 
P7SEL =  (XIN_BIT + XOUT_BIT);
//P7DIR =  (XOUT_BIT);
P7OUT &= ~(0xFC);
/********************** PORT 8 Defination**********************
   P8.0 - NC
   P8.1 - NC
   P8.2 - NC
   P8.3 - NC
   P8.4 - NC
   P8.5 - Mode_In_0
   P8.6 - Mode_In_1
   P8.7 - Mode_In_2
**************************************************************/  
  P8SEL &= ~(BIT0 + BIT1 + BIT2 + BIT3 + BIT4 + Mode_In_0_BIT + Mode_In_1_BIT + Mode_In_2_BIT);
  P8DIR &= ~(BIT0 + BIT1 + BIT2 + BIT3 + BIT4 + Mode_In_0_BIT + Mode_In_1_BIT + Mode_In_2_BIT);
  
/********************** PORT 9 Defination**********************
   P9.0 - NC
   P9.1 - NC
   P9.2 - NC
   P9.3 - NC
   P9.4 - COM_TXD
   P9.5 - COM_RXD
   P9.6 - RS_485_INT
   P9.7 - NC
**************************************************************/ 
  P9SEL &= ~(BIT0 + BIT1 + BIT2 + BIT3 + RS_485_INT_BIT + BIT7);
  P9SEL |= (COM_TXD_BIT + COM_RXD_BIT);
  P9DIR = (RS_485_INT_BIT);
 

/********************** PORT 10 Defination**********************
   P10.0 - ADD_1
   P10.1 - ADD_2
   P10.2 - ADD_4
   P10.3 - ADD_8
   P10.4 - PBC_DBG_TX
   P10.5 - PBC_DBG_RX
   P10.6 - PBC_DBG_LED_RX
   P10.7 - PBC_DBG_LED_TX

**************************************************************/  
  P10SEL &= ~(ADD_1_BIT + ADD_2_BIT + ADD_4_BIT + ADD_8_BIT + PBC_DBG_LED_RX_BIT + PBC_DBG_LED_TX_BIT);
  P10SEL |=  (PBC_DBG_TX_BIT + PBC_DBG_RX_BIT);
  P10DIR |=  (PBC_DBG_LED_RX_BIT + PBC_DBG_LED_TX_BIT);
  P10OUT &= ~(PBC_DBG_LED_RX_BIT + PBC_DBG_LED_TX_BIT);
  
/********************** PORT 11 Defination**********************
   P11.0 - Status_Led_3
   P11.1 - Status_Led_2
   P11.2 - Status_Led_1
**************************************************************/    
  P11SEL &= ~(Status_Led_3_BIT + Status_Led_2_BIT + Status_Led_1_BIT);
  P11DIR |= (Status_Led_3_BIT + Status_Led_2_BIT + Status_Led_1_BIT);
  P11OUT &= ~(Status_Led_3_BIT + Status_Led_2_BIT + Status_Led_1_BIT);
  
}

/***************************************************************************//**
 * @param Function      
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  
 * @param Description   
 ******************************************************************************/
void Change_Led( unsigned char led_name, unsigned char state)
{
  switch(led_name)
  {
  case RED_LED:
    if (state == ON)
      Status_Led_POUT |= Status_Led_3_BIT;
    else if (state == OFF)
      Status_Led_POUT &= ~Status_Led_3_BIT;
    else
      Status_Led_POUT ^= Status_Led_3_BIT;
    break;
  case YEL_LED: 
    if (state == ON)
      Status_Led_POUT |= Status_Led_2_BIT;
    else if (state == OFF)
      Status_Led_POUT &= ~Status_Led_2_BIT;
    else
      Status_Led_POUT ^= Status_Led_2_BIT;
    break;
  case GRN_LED:
    if (state == ON)
      Status_Led_POUT |= Status_Led_1_BIT;
    else if (state == OFF)
      Status_Led_POUT &= ~Status_Led_1_BIT;
    else
      Status_Led_POUT ^= Status_Led_1_BIT;
    break;
  case LED_1:
    if (state == ON)
      RJ45_LED_POUT |= LED_1_BIT;
    else if (state == OFF)
      RJ45_LED_POUT &= ~LED_1_BIT;
    else
      RJ45_LED_POUT ^= LED_1_BIT;
    break;
    case LED_2:
    if (state == ON)
      RJ45_LED_POUT |= LED_2_BIT;
    else if (state == OFF)
      RJ45_LED_POUT &= ~LED_2_BIT;
    else
      RJ45_LED_POUT ^= LED_2_BIT;
    break;
    case LED_3:
    if (state == ON)
      RJ45_LED_POUT |= LED_3_BIT;
    else if (state == OFF)
      RJ45_LED_POUT &= ~LED_3_BIT;
    else
      RJ45_LED_POUT ^= LED_3_BIT;
    break;
    case LED_4:
    if (state == ON)
      RJ45_LED_POUT |= LED_4_BIT;
    else if (state == OFF)
      RJ45_LED_POUT &= ~LED_4_BIT;
    else
      RJ45_LED_POUT ^= LED_4_BIT;
    break;
    case PBC_LED:
    if (state == ON)
      PBC_ERROR_POUT |= PBC_ERROR_BIT;
    else if (state == OFF)
      RJ45_LED_POUT &= ~PBC_ERROR_BIT;
    else
      RJ45_LED_POUT ^= PBC_ERROR_BIT;
    break;
  
  }
}
 
/***************************************************************************//**
 * @param Function      
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  
 * @param Description   
 ******************************************************************************/
void Set_UART0_LED(unsigned char mode, unsigned char state)
{
  switch(mode)
  {
  case TX:
    if (state == ON)
    {
      //P1OUT |= Fault_Led_14_BIT;
    }
    else
    {
      //P1OUT &= ~Fault_Led_14_BIT;
    }  
    break;
  default:
    if (state == ON)
    {
      //P2OUT |= Fault_Led_13_BIT;
    }
    else
    {
      //P2OUT &= ~Fault_Led_13_BIT;
    }  
    
    break;
  }
}
/***************************************************************************//**
 *                         End of Function
 ******************************************************************************/
