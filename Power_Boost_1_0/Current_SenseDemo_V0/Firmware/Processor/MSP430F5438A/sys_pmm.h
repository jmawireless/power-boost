/***************************************************************************//**
 *  \file         pmm.h
 *  \brief        Program include file and function declaration for Power Management
 *  \author       RBG
 *  \date         September,2011
 *  \version      1.0.0
 ******************************************************************************/
#ifndef __SYS_PMM
#define __SYS_PMM

/***************************************************************************//**
 *                           FILE INCLUDES
 ******************************************************************************/
#include "processor_Def.h"
   
//#define uint8                   unsigned char                                   // 8 bits
//#define uint16                  unsigned short int                              // 16 bits
//#define int8                    signed char                                     // 8 bits
//#define int16                   signed short int                                // 16 bits
//#define uchar                   unsigned char                                   // 8 bits
//#define ulong                   unsigned long

/***************************************************************************//**
 *                           CONSTANT DECLARATION
 ******************************************************************************/
#define PMM_STATUS_OK     0
#define PMM_STATUS_ERROR  1

/***************************************************************************//**
 *                           FUNCTION DELCLERATION
 ******************************************************************************/
unsigned short int SetVCore (unsigned char level);
unsigned short int SetVCoreUp (unsigned char level);
unsigned short int SetVCoreDown (unsigned char level);
void Stop_WatchDog_Timer();
void Software_Reset();

#endif /* End __SYS_PMM */
