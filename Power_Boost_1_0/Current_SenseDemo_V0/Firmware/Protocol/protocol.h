#ifndef __UART
#define __UART

#include <string.h>
#include "processor_Def.h"

#define Protocol_Start_Byte     0xA5

typedef struct s_Power_Boost_Msg 
{
  unsigned char start_byte;
  unsigned char msg_length_h;
  unsigned char msg_length_l;
  unsigned char OVP1_data_h;
  unsigned char OVP1_data_l;
  unsigned char OVP2_data_h;
  unsigned char OVP2_data_l;
  unsigned char OVP3_data_h;
  unsigned char OVP3_data_l;
  unsigned char OVP4_data_h;
  unsigned char OVP4_data_l;
  unsigned char OVP5_data_h;
  unsigned char OVP5_data_l;
  unsigned char OVP6_data_h;
  unsigned char OVP6_data_l;
  unsigned char OVP7_data_h;
  unsigned char OVP7_data_l;
  unsigned char OVP8_data_h;
  unsigned char OVP8_data_l;
  unsigned char OVP9_data_h;
  unsigned char OVP9_data_l;
  unsigned char OVP10_data_h;
  unsigned char OVP10_data_l;
  unsigned char OVP11_data_h;
  unsigned char OVP11_data_l;
  unsigned char OVP12_data_h;
  unsigned char OVP12_data_l;
  unsigned char msg_checksum;
} Power_Boost_Msg;

typedef struct s_Previous_Msg 
{
  unsigned int OVP1_data;
  unsigned int OVP2_data;
  unsigned int OVP3_data;
  unsigned int OVP4_data;
  unsigned int OVP5_data;
  unsigned int OVP6_data;
  unsigned int OVP7_data;
  unsigned int OVP8_data;
  unsigned int OVP9_data;
  unsigned int OVP10_data;
  unsigned int OVP11_data;
  unsigned int OVP12_data;
} Previous_Msg;

typedef struct s_Power_Boost_Flag
{
  unsigned char start_byte_rx;
  unsigned char end_byte_rx;
}Power_Boost_Flag;


typedef struct s_DAC_Msg
{
  unsigned short int OVP1_DAC_data;
  unsigned short int OVP2_DAC_data;
  unsigned short int OVP3_DAC_data;
  unsigned short int OVP4_DAC_data;
  unsigned short int OVP5_DAC_data;
  unsigned short int OVP6_DAC_data;
  unsigned short int OVP7_DAC_data;
  unsigned short int OVP8_DAC_data;
  unsigned short int OVP9_DAC_data;
  unsigned short int OVP10_DAC_data;
  unsigned short int OVP11_DAC_data;
  unsigned short int OVP12_DAC_data;
}DAC_Msg;

unsigned char RxBuffer[sizeof(Power_Boost_Msg)];
unsigned char RxBuffer_Count;
  
void clear_Power_Boost_Flags();
void clear_RxBufferAndCount();
void process_Rx_data(char rx_data);
unsigned char verify_Msg_End_Flag_Rx();
unsigned char Verify_Msg_CRC(Power_Boost_Msg* Msg);
void process_Message();
extern void Clear_Timer_Count();
extern void Change_Led( unsigned char led_color, unsigned char state); 
extern void Set_Fault_LED(unsigned char Led_No, unsigned char state);
#endif