﻿Part Number: 15359-01
Revision: 06

Layers: 4
Arrays: No
Dimensions: 3” x 4.5”
Route and Retain: No
Scoring: No
Finished 
Thickness: 0.062 inches +/-0.007 
Min 
Hole Size: 0.010 inches
Number Holes: 303 (293- PTH, 10 - NPTH) 
Number Drill Sizes: 8
Buried/Blind Vias: No
Material: FR4
Surface Finish: Imersion Gold (ENIG)
Gold Fingers: No
Min Trace Width: 0.008 inches
Min Space: 0.008 inches
Copper Weight: 0.5 Oz
Outer Layer Copper Finish: 0.5 Oz
Solder Mask: Both Sides, LPI, Blue, Semi-Gloss
Silk Screen: Both Sides, White
Internal Slots/Cutouts: 0
Edge Plating: No
Controlled Impedance: No
Controlled Dielectric: No

Counter Bore: No

Counter Sink: No

Fill via : No

Conformal coat: Yes, HumiSeal 1B73
Via type: Class 3


Layer Stackup:

---------- Layer 1 (0.5 oz Cu)  --------

           Prepreg FR4 

---------- Layer 2(0.5 oz Cu) ----------
           Core FR4 

---------- Layer 3 (0.5 oz Cu) ----------
           Prepreg FR4 
---------- Layer 4 (0.5 oz Cu) ----------

Gerber Files: 

15359-01_REV06 GERBER FILE FAB NOTES.gbr           	Board Outline, Drill Legend/Table, NPTH, Slots
15359-01_REV06 GERBER FILE TOP LAYER.gbr                Layer 1 (Top)
15359-01_REV06 GERBER FILE TOP SOLDERMASK.gbr           Layer 1 (Top) Solder Mask
15359-01_REV06 GERBER FILE TOP SILKSCREEN.gbr           Layer 1 (Top) Silk Screen
15359-01_REV06 GERBER FILE TOP PASTE.gbr            	Layer 1 (Top) Solder Paste
15359-01_REV06 GERBER FILE INTERNAL LAYER 2.gbr         Layer 2 
15359-01_REV06 GERBER FILE INTERNAL LAYER 3.gbr         Layer 3 
15359-01_REV06 GERBER FILE BOTTOM LAYER 4.gbr           Layer 4 (Bottom)
15359-01_REV06 GERBER FILE BOTTOM SOLDERMASK.gbr        Layer 4 (Bottom) Solder Mask
15359-01_REV06 GERBER FILE BOTTOM SILKSCREEN.gbr        Layer 4 (Bottom) Silk Screen
15359-01_REV06 GERBER FILE BOTTOM PASTE.gbr            	Layer 4 (Bottom) Solder Paste
15359-01_REV06 GERBER FILE PTH.gbr         		Plated Thru Holes
15359-01_REV06 GERBER FILE NPTH.gbr        		Non Plated Thru Holes
15359-01_REV06 GERBER FILE DRILL FILE.dxp        	Drill File binary data


