#ifndef __PROTOCOL
#define __PROTOCOL

#include <string.h>
#include "processor_Def.h"
//Protocol Level defination 
#define OVP_HIGH_BOUND          680     // 68.0V Max tower top voltage 
#define OVP_LOW_BOUND           380     // 48.0V Min tower top voltage
#define TOWERTOP_SET_VOLTAGE    560     // 54.0 Avg tower top ref voltage
#define MAX_CHANNEL_PER_BRICK   2       // 2 channel per PSU brick
#define PSU_MAX_VOLTAGE         720     // 62.0V Max PSU voltage
#define PSU_MIN_VOLTAGE         540     // 54.0V Min PSU voltage
#define PSU_REF_VOLTAGE         560     // 58.0V Avg PSU reference voltage
#define PSU_TOLERANCE_WINDOW    5      // +/-5V tolerance margin
#define PSU_SLAVE_ADDR          0x01    // Slave address =1
#define PSU_FUNCTION_TYPE       0x06    // Set voltgae function = 6
#define COMM_START_BYTE1        0xA5    // Protocol Comm start byte1
#define COMM_START_BYTE2        0x5A    // Protocol Comm start byte2
#define MAX_OVP_PER_MSG         12      // 12 OVP voltages per channel
#define MAX_OVP_IDENTIFIER      3       // 5 device id's that can be connected
#define NOS_PSU_VREF_CONTROL    1       // 1 = not redundant, 2 - redundant
//Error Check
#if NOS_PSU_VREF_CONTROL > 2
 printf("NOS_PSU_VREF_CONTROL invalid count")
#endif
#if PSU_MAX_VOLTAGE < PSU_MIN_VOLTAGE
  #error "Max voltage cant be less than Min Voltage";
#endif
#if MAX_CHANNEL_PER_BRICK > 2
  #error "Max channel per Brick cant be greater than 2";
#endif   
   
typedef struct s_Data_Msg
{
  unsigned char OVP_data_h : 4;         // LSB nibble
  unsigned char OVP_Flag : 4;           // MSB nibble
  unsigned char OVP_data_l;
} Data_Msg;

typedef struct s_PSU_Mapping
{
  unsigned char Configured_OVP_Flag: 1;
  unsigned char Set_Max_Flag: 1;
  unsigned char Set_Min_Flag: 1;
  unsigned char Set_Avg_Flag: 1;
  unsigned char Msg_Device_Id: 4;
  unsigned char Msg_OVP_Id1_Max;
  unsigned char Msg_OVP_Id1_Min;
  unsigned char Msg_OVP_Id1_Avg;
  #if defined NOS_PSU_VREF_CONTROL == 2
  unsigned char Msg_OVP_Id2_Max;
  unsigned char Msg_OVP_Id2_Min;
  unsigned char Msg_OVP_Id2_Avg;
  #endif
  unsigned char PSU_Module_address;
} PSU_Mapping;



typedef struct s_PSU_Boost_Msg
{
  unsigned char Slave_Addr;           // Slave Address Fixed to 01
  unsigned char Function;             // Modbus Function fixed to 06  
  unsigned char Reg_Addr_Hi;          // Slave Register Address High Byte
  unsigned char Reg_Addr_Lo;          // Slave Regiater Address Low Byte
  unsigned char Write_Data_Hi:4;      // Write date higher nibble  
  unsigned char Alarm:4;              // Alarm (refer table)
  unsigned char Write_Data_Lo;        // Write data lower byte
  unsigned char Error_Check_Lo;       // Error Check Low
  unsigned char Error_Check_Hi;       // Error Checl High
} PSU_Boost_Msg;

typedef struct s_PSU_Boost_Error_Msg
{
  unsigned char Slave_Addr;           // Slave Address Fixed to 01
  unsigned char Function;             // Modbus Function fixed to 06  
  unsigned char Exception_Code;       // Exception Code
  unsigned char Error_Check_Lo;       // Error Check Low
  unsigned char Error_Check_Hi;       // Error Checl High
} PSU_Boost_Error_Msg;

typedef struct s_Power_Boost_Msg 
{
  unsigned char start_byte_1;
  unsigned char start_byte_2; 
  unsigned char Device_Id : 3;          // LSB nibble        
  unsigned char System_Alarm : 5;       // MSB nibble
  unsigned char msg_length_l;
  struct s_Data_Msg Data_Msg[MAX_OVP_PER_MSG];
  unsigned char msg_checksum;
} Power_Boost_Msg;

typedef struct s_Previous_Msg 
{
  struct s_Data_Msg Power_Boost_Processed_Msg[MAX_OVP_IDENTIFIER][MAX_OVP_PER_MSG];
  struct s_Data_Msg OVP_Raw_Data[MAX_OVP_IDENTIFIER][MAX_OVP_PER_MSG];
  struct s_PSU_Mapping PSU_Register_Map[MAX_OVP_IDENTIFIER][MAX_OVP_PER_MSG];
//  unsigned char Configured_Device_Id[MAX_OVP_IDENTIFIER][OVP_Info_Per_Msg];
//  unsigned char Record_Device_Id[MAX_OVP_IDENTIFIER][OVP_Info_Per_Msg];
} Previous_Msg;

typedef struct s_Diagnostics
{
  unsigned char Failed_Packet_Count[MAX_OVP_IDENTIFIER][MAX_OVP_PER_MSG];
  unsigned char Device_Id_Failure_Flag[MAX_OVP_IDENTIFIER]; // Bit 0 = No Commuication with RC, Bit1 = ID1, Bit2 = ID2, Bit3 = ID3, Bit4 = ID4, Bit5 = ID5,..
  unsigned int OVP_Id_Failure_Flag[MAX_OVP_IDENTIFIER][MAX_OVP_PER_MSG]; // Bit 0 = Future, Bit1 = OVP1, Bit2 = OVP2, Bit3 = OVP3 .... Bit12 = OVP12
} Msg_Diagnostics;

typedef struct s_Power_Boost_Flag
{
  unsigned char start_byte1_rx;
  unsigned char start_byte2_rx;
  unsigned char end_byte_rx;
}Power_Boost_Rx_Flag;

typedef struct s_Config_Flag 
{
  unsigned char Config_Busy_flag;
  unsigned char Config_Busy_id;
  unsigned char Config_Busy_ovp_id;
  unsigned char Config_Wait_Count;
}Config_Flag;

typedef struct s_Beat_Msg
{
  unsigned int heartbeat_count;
  unsigned char send_heartbeat_flag; 
} HeartBeat_Msg;


unsigned char RxBuffer[sizeof(Power_Boost_Msg)];
unsigned char PSU_RxBuffer[sizeof(PSU_Boost_Msg)];
unsigned char RxBuffer_Count;
unsigned char PSU_RxBuffer_Count;
unsigned char PSU_Rx_Msg_Type; // 0 = Valid response 1: Error response 
 
//void clear_Power_Boost_Flags();
void clear_RxBufferAndCount();
void process_Rx_data(unsigned char rx_data);
void process_PSU_Rx_Msg(unsigned char psu_rx_data);
void Setup_PSU();
unsigned char verify_Msg_End_Flag_Rx();
unsigned char Verify_Msg_CRC(Power_Boost_Msg* Msg);
unsigned char process_HeartBeat_Message();
void process_Message();
extern void Clear_Timer_Count();
extern unsigned char verify_Communication_Flag();
extern void Change_Led( unsigned char led_name, unsigned char state); 
//extern void Set_Fault_LED(unsigned char Led_No, unsigned char state);
extern void Set_Fault_Ctrl(unsigned char Led_No, unsigned char state);
extern void SendD2AConvertorMsg(unsigned char Reg_Type, unsigned char DAC_Channel, unsigned int Raw_OVP_Data);
extern void SendSPI_D2AConvertorMsg(unsigned char Reg_Type, unsigned char DAC_Channel, unsigned int Raw_OVP_Data);
extern void Set_Uart_Int_Flag(unsigned char uart_ucaX);

extern void Clear_Heartbeat_Count();
#endif