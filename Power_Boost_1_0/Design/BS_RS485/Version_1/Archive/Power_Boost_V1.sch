<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.2.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="no"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="110" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="111" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="no"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="top_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="14" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="15" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
<layer number="255" name="Accent" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="JMA_LBR">
<packages>
<package name="QFP50P1600X1600X120-100N">
<smd name="1" x="-7.7" y="6" dx="1.5" dy="0.3" layer="1"/>
<smd name="2" x="-7.7" y="5.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="3" x="-7.7" y="5" dx="1.5" dy="0.3" layer="1"/>
<smd name="4" x="-7.7" y="4.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="5" x="-7.7" y="4" dx="1.5" dy="0.3" layer="1"/>
<smd name="6" x="-7.7" y="3.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="7" x="-7.7" y="3" dx="1.5" dy="0.3" layer="1"/>
<smd name="8" x="-7.7" y="2.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="9" x="-7.7" y="2" dx="1.5" dy="0.3" layer="1"/>
<smd name="10" x="-7.7" y="1.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="11" x="-7.7" y="1" dx="1.5" dy="0.3" layer="1"/>
<smd name="12" x="-7.7" y="0.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="13" x="-7.7" y="0" dx="1.5" dy="0.3" layer="1"/>
<smd name="14" x="-7.7" y="-0.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="15" x="-7.7" y="-1" dx="1.5" dy="0.3" layer="1"/>
<smd name="16" x="-7.7" y="-1.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="17" x="-7.7" y="-2" dx="1.5" dy="0.3" layer="1"/>
<smd name="18" x="-7.7" y="-2.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="19" x="-7.7" y="-3" dx="1.5" dy="0.3" layer="1"/>
<smd name="20" x="-7.7" y="-3.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="21" x="-7.7" y="-4" dx="1.5" dy="0.3" layer="1"/>
<smd name="22" x="-7.7" y="-4.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="23" x="-7.7" y="-5" dx="1.5" dy="0.3" layer="1"/>
<smd name="24" x="-7.7" y="-5.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="25" x="-7.7" y="-6" dx="1.5" dy="0.3" layer="1"/>
<smd name="26" x="-6" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="27" x="-5.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="28" x="-5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="29" x="-4.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="30" x="-4" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="31" x="-3.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="32" x="-3" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="33" x="-2.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="34" x="-2" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="35" x="-1.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="36" x="-1" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="37" x="-0.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="38" x="0" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="39" x="0.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="40" x="1" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="41" x="1.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="42" x="2" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="43" x="2.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="44" x="3" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="45" x="3.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="46" x="4" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="47" x="4.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="48" x="5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="49" x="5.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="50" x="6" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="51" x="7.7" y="-6" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="52" x="7.7" y="-5.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="53" x="7.7" y="-5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="54" x="7.7" y="-4.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="55" x="7.7" y="-4" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="56" x="7.7" y="-3.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="57" x="7.7" y="-3" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="58" x="7.7" y="-2.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="59" x="7.7" y="-2" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="60" x="7.7" y="-1.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="61" x="7.7" y="-1" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="62" x="7.7" y="-0.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="63" x="7.7" y="0" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="64" x="7.7" y="0.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="65" x="7.7" y="1" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="66" x="7.7" y="1.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="67" x="7.7" y="2" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="68" x="7.7" y="2.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="69" x="7.7" y="3" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="70" x="7.7" y="3.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="71" x="7.7" y="4" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="72" x="7.7" y="4.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="73" x="7.7" y="5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="74" x="7.7" y="5.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="75" x="7.7" y="6" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="76" x="6" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="77" x="5.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="78" x="5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="79" x="4.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="80" x="4" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="81" x="3.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="82" x="3" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="83" x="2.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="84" x="2" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="85" x="1.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="86" x="1" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="87" x="0.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="88" x="0" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="89" x="-0.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="90" x="-1" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="91" x="-1.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="92" x="-2" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="93" x="-2.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="94" x="-3" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="95" x="-3.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="96" x="-4" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="97" x="-4.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="98" x="-5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="99" x="-5.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="100" x="-6" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<wire x1="-6.5" y1="6.5" x2="6.5" y2="6.5" width="0.127" layer="21"/>
<wire x1="6.5" y1="6.5" x2="6.5" y2="-6.5" width="0.127" layer="21"/>
<wire x1="6.5" y1="-6.5" x2="-6.5" y2="-6.5" width="0.127" layer="21"/>
<wire x1="-6.5" y1="-6.5" x2="-6.5" y2="6.5" width="0.127" layer="21"/>
<wire x1="-9" y1="7" x2="-9" y2="-9" width="0.127" layer="21"/>
<wire x1="-9" y1="-9" x2="9" y2="-9" width="0.127" layer="21"/>
<wire x1="9" y1="-9" x2="9" y2="9" width="0.127" layer="21"/>
<wire x1="9" y1="9" x2="-7" y2="9" width="0.127" layer="21"/>
<wire x1="-7" y1="9" x2="-9" y2="7" width="0.127" layer="21"/>
<polygon width="0.127" layer="21">
<vertex x="-9" y="9"/>
<vertex x="-8.25" y="9"/>
<vertex x="-8.25" y="8.25"/>
<vertex x="-9" y="8.25"/>
</polygon>
<wire x1="-6.5" y1="6.5" x2="-6.5" y2="-6.5" width="0.127" layer="51"/>
<wire x1="-6.5" y1="-6.5" x2="6.5" y2="-6.5" width="0.127" layer="51"/>
<wire x1="6.5" y1="-6.5" x2="6.5" y2="6.5" width="0.127" layer="51"/>
<wire x1="6.5" y1="6.5" x2="-6.5" y2="6.5" width="0.127" layer="51"/>
<wire x1="-9" y1="7" x2="-9" y2="-9" width="0.127" layer="51"/>
<wire x1="-9" y1="-9" x2="9" y2="-9" width="0.127" layer="51"/>
<wire x1="9" y1="-9" x2="9" y2="9" width="0.127" layer="51"/>
<wire x1="9" y1="9" x2="-7" y2="9" width="0.127" layer="51"/>
<wire x1="-7" y1="9" x2="-9" y2="7" width="0.127" layer="51"/>
<wire x1="-9" y1="9" x2="-8.25" y2="9" width="0.127" layer="51"/>
<wire x1="-8.25" y1="9" x2="-8.25" y2="8.25" width="0.127" layer="51"/>
<wire x1="-8.25" y1="8.25" x2="-9" y2="8.25" width="0.127" layer="51"/>
<wire x1="-9" y1="8.25" x2="-9" y2="9" width="0.127" layer="51"/>
<text x="-7" y="9.5" size="0.6096" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-7" y="10.5" size="0.6096" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="RESC1005X40N">
<wire x1="-0.48" y1="-0.94" x2="-0.48" y2="0.94" width="0.127" layer="21"/>
<wire x1="-0.48" y1="0.94" x2="0.48" y2="0.94" width="0.127" layer="21"/>
<wire x1="0.48" y1="0.94" x2="0.48" y2="-0.94" width="0.127" layer="21"/>
<wire x1="0.48" y1="-0.94" x2="-0.48" y2="-0.94" width="0.127" layer="21" style="shortdash"/>
<smd name="1" x="0" y="0.48" dx="0.57" dy="0.62" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.48" dx="0.57" dy="0.62" layer="1" roundness="25" rot="R270"/>
<text x="0.635" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.524" y="1.397" size="0.635" layer="27" font="vector" ratio="10" rot="R270">&gt;VALUE</text>
<wire x1="-0.48" y1="-0.93" x2="-0.48" y2="0.93" width="0.127" layer="51"/>
<wire x1="-0.48" y1="0.93" x2="0.48" y2="0.93" width="0.127" layer="51"/>
<wire x1="0.48" y1="0.93" x2="0.48" y2="-0.93" width="0.127" layer="51"/>
<wire x1="0.48" y1="-0.93" x2="-0.48" y2="-0.93" width="0.127" layer="51" style="shortdash"/>
</package>
<package name="RESC1608X50N">
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.8" dx="0.95" dy="1" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.8" dx="0.95" dy="1" layer="1" roundness="25" rot="R270"/>
<text x="0.889" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.5875" y="1.27" size="0.635" layer="27" ratio="10" rot="R270">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
</package>
<package name="CAPC1005X55N">
<wire x1="-0.48" y1="-0.93" x2="-0.48" y2="0.93" width="0.127" layer="21"/>
<wire x1="-0.48" y1="0.93" x2="0.48" y2="0.93" width="0.127" layer="21"/>
<wire x1="0.48" y1="0.93" x2="0.48" y2="-0.93" width="0.127" layer="21"/>
<wire x1="0.48" y1="-0.93" x2="-0.48" y2="-0.93" width="0.127" layer="21" style="shortdash"/>
<smd name="1" x="0" y="0.45" dx="0.62" dy="0.62" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.45" dx="0.62" dy="0.62" layer="1" roundness="25" rot="R270"/>
<text x="0.635" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.524" y="1.397" size="0.635" layer="27" font="vector" ratio="10" rot="R270">&gt;VALUE</text>
<wire x1="-0.48" y1="-0.93" x2="-0.48" y2="0.93" width="0.127" layer="51"/>
<wire x1="-0.48" y1="0.93" x2="0.48" y2="0.93" width="0.127" layer="51"/>
<wire x1="0.48" y1="0.93" x2="0.48" y2="-0.93" width="0.127" layer="51"/>
<wire x1="0.48" y1="-0.93" x2="-0.48" y2="-0.93" width="0.127" layer="51" style="shortdash"/>
</package>
<package name="CAPC1608X55N">
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.762" dx="0.9" dy="0.95" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.762" dx="0.9" dy="0.95" layer="1" roundness="25" rot="R270"/>
<text x="0.889" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.5875" y="1.27" size="0.635" layer="27" ratio="10" rot="R270">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
</package>
<package name="CAPC2013X145N">
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.9" dx="1.15" dy="1.45" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.9" dx="1.15" dy="1.45" layer="1" roundness="25" rot="R270"/>
<text x="2.111" y="-2.02" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1.1625" y="-1.52" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="51"/>
</package>
<package name="RESC2013X70N">
<wire x1="1" y1="1.8" x2="1" y2="-1.8" width="0.127" layer="21"/>
<wire x1="1" y1="-1.8" x2="-1" y2="-1.8" width="0.127" layer="21"/>
<wire x1="-1" y1="-1.8" x2="-1" y2="1.8" width="0.127" layer="21"/>
<wire x1="-1" y1="1.8" x2="1" y2="1.8" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.95" dx="1.05" dy="1.4" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.95" dx="1.05" dy="1.4" layer="1" roundness="25" rot="R270"/>
<text x="1.511" y="-1.47" size="0.4064" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1.2125" y="-1.47" size="0.4064" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
</package>
<package name="RESC3216X84N">
<wire x1="1.275" y1="2.27" x2="1.275" y2="-2.27" width="0.127" layer="21"/>
<wire x1="1.275" y1="-2.27" x2="-1.275" y2="-2.27" width="0.127" layer="21"/>
<wire x1="-1.275" y1="-2.27" x2="-1.275" y2="2.27" width="0.127" layer="21"/>
<wire x1="-1.275" y1="2.27" x2="1.275" y2="2.27" width="0.127" layer="21"/>
<smd name="1" x="0" y="1.5" dx="1.05" dy="1.75" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-1.5" dx="1.05" dy="1.75" layer="1" roundness="25" rot="R270"/>
<text x="1.95" y="-2" size="0.4064" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1.5" y="-2" size="0.4064" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.75" x2="1.27" y2="0.75" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
</package>
<package name="RES2013X38N">
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="21"/>
<smd name="1" x="0" y="1" dx="0.95" dy="1.45" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-1" dx="0.95" dy="1.45" layer="1" roundness="25" rot="R270"/>
<text x="2" y="-2" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1" y="-2" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="51"/>
</package>
<package name="RESC5325X84N">
<wire x1="1.775" y1="3.52" x2="1.775" y2="-3.52" width="0.127" layer="21"/>
<wire x1="1.775" y1="-3.52" x2="-1.775" y2="-3.52" width="0.127" layer="21"/>
<wire x1="-1.775" y1="-3.52" x2="-1.775" y2="3.52" width="0.127" layer="21"/>
<wire x1="-1.775" y1="3.52" x2="1.775" y2="3.52" width="0.127" layer="21"/>
<smd name="1" x="0" y="2.55" dx="1.1" dy="2.65" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-2.55" dx="1.1" dy="2.65" layer="1" roundness="25" rot="R270"/>
<text x="2.45" y="-2" size="0.4064" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-2" y="-2" size="0.4064" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-3" y1="-1.27" x2="3" y2="1.27" layer="39" rot="R270"/>
</package>
<package name="CAPC3216X190N">
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.127" layer="21"/>
<smd name="1" x="0" y="1.5" dx="1.15" dy="1.8" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-1.5" dx="1.15" dy="1.8" layer="1" roundness="25" rot="R270"/>
<text x="2.361" y="-2.02" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1.4125" y="-1.52" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.75" x2="1.27" y2="0.75" layer="39" rot="R270"/>
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="51"/>
</package>
<package name="CAPC4532X279N">
<wire x1="2.175" y1="3.22" x2="2.175" y2="-3.22" width="0.127" layer="21"/>
<wire x1="2.175" y1="-3.22" x2="-2.175" y2="-3.22" width="0.127" layer="21"/>
<wire x1="-2.175" y1="-3.22" x2="-2.175" y2="3.22" width="0.127" layer="21"/>
<wire x1="-2.175" y1="3.22" x2="2.175" y2="3.22" width="0.127" layer="21"/>
<smd name="1" x="0" y="2.05" dx="1.4" dy="3.4" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-2.05" dx="1.4" dy="3.4" layer="1" roundness="25" rot="R270"/>
<text x="3.361" y="-2.02" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-2.6625" y="-1.52" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.264" y1="-1.494" x2="2.256" y2="1.514" layer="39" rot="R270"/>
<wire x1="1.56" y1="2.331" x2="1.56" y2="-2.355" width="0.127" layer="51"/>
<wire x1="1.56" y1="-2.355" x2="-1.56" y2="-2.355" width="0.127" layer="51"/>
<wire x1="-1.56" y1="-2.355" x2="-1.56" y2="2.331" width="0.127" layer="51"/>
<wire x1="-1.56" y1="2.331" x2="1.56" y2="2.331" width="0.127" layer="51"/>
</package>
<package name="ESWITCH_TL3304AF260QJ">
<smd name="1" x="-4.8006" y="-1.9939" dx="3.302" dy="0.9906" layer="1"/>
<smd name="2" x="4.8006" y="-1.9939" dx="3.302" dy="0.9906" layer="1"/>
<smd name="4" x="4.8006" y="1.9939" dx="3.302" dy="0.9906" layer="1"/>
<smd name="3" x="-4.8006" y="1.9939" dx="3.302" dy="0.9906" layer="1"/>
<wire x1="-5.08" y1="3.175" x2="-5.08" y2="5.08" width="0.127" layer="21"/>
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.127" layer="21"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="3.175" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-3.175" x2="-5.08" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="-5.08" width="0.127" layer="21"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="-3.175" width="0.127" layer="21"/>
<circle x="0" y="0" radius="2.54" width="0.127" layer="21"/>
<circle x="0" y="0" radius="2.54" width="0.127" layer="51"/>
<wire x1="-5.08" y1="3.175" x2="-5.08" y2="5.08" width="0.127" layer="51"/>
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.127" layer="51"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="3.175" width="0.127" layer="51"/>
<wire x1="-5.08" y1="-3.175" x2="-5.08" y2="-5.08" width="0.127" layer="51"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="-5.08" width="0.127" layer="51"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="-3.175" width="0.127" layer="51"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.127" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.127" layer="21"/>
<text x="7.62" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="7.62" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="TP_1.5MM">
<pad name="1" x="0" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-1.905" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<rectangle x1="-0.35" y1="-0.35" x2="0.35" y2="0.35" layer="51"/>
</package>
<package name="C120H40">
<pad name="1" x="0" y="0" drill="0.6" diameter="1.2" thermals="no"/>
<text x="0.9" y="-0.1" size="0.254" layer="25">&gt;NAME</text>
<text x="0.9" y="-0.5" size="0.254" layer="21">&gt;LABEL</text>
</package>
<package name="C131H51">
<pad name="1" x="0" y="0" drill="0.71" diameter="1.31" thermals="no"/>
<text x="1.2" y="0.1" size="0.254" layer="25">&gt;NAME</text>
<text x="1.2" y="-0.2" size="0.254" layer="21">&gt;LABEL</text>
</package>
<package name="C406H326">
<pad name="1" x="0" y="0" drill="3.26" diameter="4.06" thermals="no"/>
<text x="2.2" y="1.1" size="0.254" layer="25">&gt;NAME</text>
<text x="2.2" y="0.8" size="0.254" layer="21">&gt;LABEL</text>
</package>
<package name="C491H411">
<pad name="1" x="0" y="0" drill="4.31" diameter="4.91" thermals="no"/>
<text x="3.2" y="1.1" size="0.254" layer="25">&gt;NAME</text>
<text x="3.2" y="0.8" size="0.254" layer="21">&gt;LABEL</text>
</package>
<package name="LEDC1005X60N">
<wire x1="-0.52" y1="-0.93" x2="-0.52" y2="0.93" width="0.127" layer="21"/>
<wire x1="-0.52" y1="0.93" x2="0.52" y2="0.93" width="0.127" layer="21"/>
<wire x1="0.52" y1="0.93" x2="0.52" y2="-0.93" width="0.127" layer="21"/>
<wire x1="0.52" y1="-0.93" x2="-0.52" y2="-0.93" width="0.127" layer="21" style="shortdash"/>
<smd name="1" x="0" y="0.45" dx="0.5" dy="0.7" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.45" dx="0.5" dy="0.7" layer="1" roundness="25" rot="R270"/>
<text x="0.635" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.524" y="1.397" size="0.635" layer="27" font="vector" ratio="10" rot="R270">&gt;VALUE</text>
<wire x1="-0.52" y1="-0.93" x2="-0.52" y2="0.93" width="0.127" layer="51"/>
<wire x1="-0.52" y1="0.93" x2="0.52" y2="0.93" width="0.127" layer="51"/>
<wire x1="0.52" y1="0.93" x2="0.52" y2="-0.93" width="0.127" layer="51"/>
<wire x1="0.52" y1="-0.93" x2="-0.52" y2="-0.93" width="0.127" layer="51" style="shortdash"/>
<wire x1="-0.5" y1="-1.15" x2="0.5" y2="-1.15" width="0.127" layer="21"/>
<wire x1="-0.5" y1="-1.15" x2="0.5" y2="-1.15" width="0.127" layer="51"/>
</package>
<package name="LEDC1608X75N/80N">
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.825" dx="0.95" dy="1" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.825" dx="0.95" dy="1" layer="1" roundness="25" rot="R270"/>
<text x="0.889" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.5875" y="1.27" size="0.635" layer="27" ratio="10" rot="R270">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.65" x2="0.675" y2="-1.65" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.65" x2="0.675" y2="-1.65" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.65" x2="0.675" y2="-1.65" width="0.127" layer="21"/>
</package>
<package name="3PIN_JMP1608X50N">
<wire x1="-1.597" y1="0.675" x2="3.121" y2="0.675" width="0.127" layer="21"/>
<wire x1="3.121" y1="0.675" x2="3.121" y2="-0.675" width="0.127" layer="21"/>
<wire x1="3.121" y1="-0.675" x2="-1.597" y2="-0.675" width="0.127" layer="21"/>
<wire x1="-1.597" y1="-0.675" x2="-1.597" y2="0.675" width="0.127" layer="21"/>
<smd name="1" x="-0.8" y="0" dx="0.95" dy="1" layer="1" roundness="25"/>
<smd name="C" x="0.8" y="0" dx="0.95" dy="1" layer="1" roundness="25"/>
<text x="2.54" y="1.27" size="0.6096" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.3302" y1="-0.508" x2="2.8702" y2="0.508" layer="39"/>
<wire x1="-1.597" y1="0.675" x2="3.121" y2="0.675" width="0.127" layer="51"/>
<wire x1="3.121" y1="0.675" x2="3.121" y2="-0.675" width="0.127" layer="51"/>
<wire x1="3.121" y1="-0.675" x2="-1.597" y2="-0.675" width="0.127" layer="51"/>
<wire x1="-1.597" y1="-0.675" x2="-1.597" y2="0.675" width="0.127" layer="51"/>
<smd name="2" x="2.39511875" y="0" dx="0.95" dy="1" layer="1" roundness="25" cream="no"/>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39"/>
</package>
<package name="AB26TRQ">
<smd name="1" x="-0.45" y="2.45" dx="1.1" dy="0.6" layer="1" rot="R90"/>
<smd name="2" x="0.45" y="2.45" dx="1.1" dy="0.6" layer="1" rot="R90"/>
<smd name="3" x="0" y="-3.1" dx="2.4" dy="1.5" layer="1" rot="R90"/>
<wire x1="-1" y1="2" x2="-1" y2="-4.5" width="0.127" layer="21"/>
<wire x1="1" y1="2" x2="1" y2="-4.5" width="0.127" layer="21"/>
<wire x1="1" y1="-4.5" x2="-1" y2="-4.5" width="0.127" layer="21"/>
<wire x1="-1" y1="2" x2="-1" y2="-4.5" width="0.127" layer="51"/>
<wire x1="-1" y1="-4.5" x2="1" y2="-4.5" width="0.127" layer="51"/>
<wire x1="1" y1="-4.5" x2="1" y2="2" width="0.127" layer="51"/>
<wire x1="-1" y1="2.75" x2="-1" y2="3.25" width="0.127" layer="51"/>
<wire x1="-1" y1="3.25" x2="1" y2="3.25" width="0.127" layer="51"/>
<wire x1="1" y1="3.25" x2="1" y2="2.75" width="0.127" layer="51"/>
<wire x1="-1" y1="2.75" x2="-1" y2="3.25" width="0.127" layer="21"/>
<wire x1="-1" y1="3.25" x2="1" y2="3.25" width="0.127" layer="21"/>
<wire x1="1" y1="3.25" x2="1" y2="2.75" width="0.127" layer="21"/>
<text x="2" y="2" size="0.4064" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="2" y="1" size="0.4064" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="DO-214AC_475X290">
<wire x1="-2.02" y1="-3.43" x2="-2.02" y2="3.43" width="0.127" layer="21"/>
<wire x1="-2.02" y1="3.43" x2="2.02" y2="3.43" width="0.127" layer="21"/>
<wire x1="2.02" y1="3.43" x2="2.02" y2="-3.43" width="0.127" layer="21"/>
<wire x1="2.02" y1="-3.43" x2="-2.02" y2="-3.43" width="0.127" layer="21" style="shortdash"/>
<smd name="1" x="0" y="2.25" dx="1.7" dy="2.5" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-2.25" dx="1.7" dy="2.5" layer="1" roundness="25" rot="R270"/>
<text x="3" y="-2" size="0.635" layer="21" ratio="11" rot="R90">&gt;NAME</text>
<text x="-2.25" y="-2" size="0.635" layer="27" font="vector" ratio="10" rot="R90">&gt;VALUE</text>
<wire x1="-2.02" y1="-3.43" x2="-2.02" y2="3.43" width="0.127" layer="21"/>
<wire x1="-2.02" y1="3.43" x2="2.02" y2="3.43" width="0.127" layer="21"/>
<wire x1="2.02" y1="3.43" x2="2.02" y2="-3.43" width="0.127" layer="21"/>
<wire x1="2.02" y1="-3.43" x2="-2.02" y2="-3.43" width="0.127" layer="21" style="shortdash"/>
<wire x1="-2" y1="-3.65" x2="2" y2="-3.65" width="0.127" layer="21"/>
<wire x1="-2" y1="-3.65" x2="2" y2="-3.65" width="0.127" layer="21"/>
</package>
<package name="IND445X406X180">
<wire x1="2.425" y1="2.97" x2="2.425" y2="-3.22" width="0.127" layer="21"/>
<wire x1="2.425" y1="-3.22" x2="-2.425" y2="-3.22" width="0.127" layer="21"/>
<wire x1="-2.425" y1="-3.22" x2="-2.425" y2="2.97" width="0.127" layer="21"/>
<wire x1="-2.425" y1="2.97" x2="2.425" y2="2.97" width="0.127" layer="21"/>
<smd name="1" x="0" y="1.85" dx="1.5" dy="2.4" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-1.85" dx="1.5" dy="2.24" layer="1" roundness="25" rot="R270"/>
<text x="4" y="-2" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-3" y="-2" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<wire x1="2.425" y1="2.97" x2="2.425" y2="-3.22" width="0.127" layer="51"/>
<wire x1="2.425" y1="-3.22" x2="-2.425" y2="-3.22" width="0.127" layer="51"/>
<wire x1="-2.425" y1="-3.22" x2="-2.425" y2="2.97" width="0.127" layer="51"/>
<wire x1="-2.425" y1="2.97" x2="2.425" y2="2.97" width="0.127" layer="51"/>
</package>
<package name="ONSEMI_LM317">
<smd name="1" x="-2.54" y="-10.213" dx="1.016" dy="3.504" layer="1"/>
<smd name="2" x="0" y="0" dx="10.49" dy="8.38" layer="1"/>
<smd name="3" x="2.54" y="-10.213" dx="1.016" dy="3.504" layer="1"/>
<wire x1="-6" y1="5" x2="-6" y2="-6" width="0.127" layer="21"/>
<wire x1="-6" y1="-6" x2="-4" y2="-6" width="0.127" layer="21"/>
<wire x1="-4" y1="-6" x2="-4" y2="-13" width="0.127" layer="21"/>
<wire x1="-4" y1="-13" x2="-1" y2="-13" width="0.127" layer="21"/>
<wire x1="-1" y1="-13" x2="-1" y2="-6" width="0.127" layer="21"/>
<wire x1="-1" y1="-6" x2="1" y2="-6" width="0.127" layer="21"/>
<wire x1="1" y1="-6" x2="1" y2="-13" width="0.127" layer="21"/>
<wire x1="1" y1="-13" x2="4" y2="-13" width="0.127" layer="21"/>
<wire x1="4" y1="-13" x2="4" y2="-6" width="0.127" layer="21"/>
<wire x1="4" y1="-6" x2="6" y2="-6" width="0.127" layer="21"/>
<wire x1="6" y1="-6" x2="6" y2="5" width="0.127" layer="21"/>
<wire x1="6" y1="5" x2="-6" y2="5" width="0.127" layer="21"/>
<wire x1="-6" y1="5" x2="-6" y2="-6" width="0.127" layer="51"/>
<wire x1="-6" y1="-6" x2="-4" y2="-6" width="0.127" layer="51"/>
<wire x1="-4" y1="-6" x2="-4" y2="-13" width="0.127" layer="51"/>
<wire x1="-4" y1="-13" x2="-1" y2="-13" width="0.127" layer="51"/>
<wire x1="-1" y1="-13" x2="-1" y2="-6" width="0.127" layer="51"/>
<wire x1="-1" y1="-6" x2="1" y2="-6" width="0.127" layer="51"/>
<wire x1="1" y1="-6" x2="1" y2="-13" width="0.127" layer="51"/>
<wire x1="1" y1="-13" x2="4" y2="-13" width="0.127" layer="51"/>
<wire x1="4" y1="-13" x2="4" y2="-6" width="0.127" layer="51"/>
<wire x1="4" y1="-6" x2="6" y2="-6" width="0.127" layer="51"/>
<wire x1="6" y1="-6" x2="6" y2="5" width="0.127" layer="51"/>
<wire x1="6" y1="5" x2="-6" y2="5" width="0.127" layer="51"/>
<text x="-6" y="6" size="1.27" layer="25">&gt;NAME</text>
<text x="-6" y="8" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="DO-214AC_450X280">
<wire x1="-2.02" y1="-3.18" x2="-2.02" y2="3.18" width="0.127" layer="21"/>
<wire x1="-2.02" y1="3.18" x2="2.02" y2="3.18" width="0.127" layer="21"/>
<wire x1="2.02" y1="3.18" x2="2.02" y2="-3.18" width="0.127" layer="21"/>
<wire x1="2.02" y1="-3.18" x2="-2.02" y2="-3.18" width="0.127" layer="21" style="shortdash"/>
<smd name="1" x="0" y="2.25" dx="1.5" dy="3.6" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-2.25" dx="1.5" dy="3.6" layer="1" roundness="25" rot="R270"/>
<text x="3" y="-2" size="0.635" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-2.25" y="-2" size="0.635" layer="27" font="vector" ratio="10" rot="R90">&gt;VALUE</text>
<wire x1="-2.02" y1="-3.18" x2="-2.02" y2="3.18" width="0.127" layer="51"/>
<wire x1="-2.02" y1="3.18" x2="2.02" y2="3.18" width="0.127" layer="51"/>
<wire x1="2.02" y1="3.18" x2="2.02" y2="-3.18" width="0.127" layer="51"/>
<wire x1="2.02" y1="-3.18" x2="-2.02" y2="-3.18" width="0.127" layer="51" style="shortdash"/>
<wire x1="-2" y1="-3.4" x2="2" y2="-3.4" width="0.127" layer="21"/>
<wire x1="-2" y1="-3.4" x2="2" y2="-3.4" width="0.127" layer="51"/>
</package>
<package name="SOT-23">
<smd name="3" x="0" y="-1" dx="0.9" dy="0.8" layer="1" rot="R90"/>
<smd name="2" x="-0.95" y="1" dx="0.9" dy="0.8" layer="1" rot="R90"/>
<smd name="1" x="0.95" y="1" dx="0.9" dy="0.8" layer="1" rot="R90"/>
<wire x1="-2" y1="1" x2="-2" y2="-1" width="0.127" layer="21"/>
<wire x1="-2" y1="-1" x2="-1" y2="-1" width="0.127" layer="21"/>
<wire x1="1" y1="-1" x2="2" y2="-1" width="0.127" layer="21"/>
<wire x1="2" y1="-1" x2="2" y2="1" width="0.127" layer="21"/>
<wire x1="-0.25" y1="1" x2="0.25" y2="1" width="0.127" layer="21"/>
<wire x1="-2" y1="1" x2="-2" y2="-1" width="0.127" layer="51"/>
<wire x1="-2" y1="-1" x2="-1" y2="-1" width="0.127" layer="51"/>
<wire x1="2" y1="1" x2="2" y2="-1" width="0.127" layer="51"/>
<wire x1="2" y1="-1" x2="1" y2="-1" width="0.127" layer="51"/>
<wire x1="-0.25" y1="1" x2="0.25" y2="1" width="0.127" layer="51"/>
<text x="-2" y="2" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-2" y="4" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="TP_1.5MM_PAD">
<pad name="1" x="0" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-1.27" y="4.445" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="-0.35" y1="-0.35" x2="0.35" y2="0.35" layer="51"/>
<rectangle x1="-2" y1="-2.5" x2="8" y2="2.5" layer="1"/>
</package>
<package name="RJHSE_538X">
<hole x="-6.35" y="0" drill="3.2512"/>
<hole x="6.35" y="0" drill="3.2512"/>
<pad name="14" x="-8.128" y="3.429" drill="0.6"/>
<pad name="13" x="8.128" y="3.429" drill="0.6"/>
<pad name="8" x="-3.556" y="4.318" drill="0.889"/>
<pad name="6" x="-1.524" y="4.318" drill="0.889"/>
<pad name="4" x="0.508" y="4.318" drill="0.889"/>
<pad name="2" x="2.54" y="4.318" drill="0.889"/>
<pad name="7" x="-2.54" y="2.54" drill="0.889"/>
<pad name="5" x="-0.508" y="2.54" drill="0.889"/>
<pad name="3" x="1.524" y="2.54" drill="0.889"/>
<pad name="1" x="3.556" y="2.54" drill="0.889"/>
<pad name="12" x="-6.858" y="9.144" drill="0.889"/>
<pad name="11" x="-4.572" y="9.144" drill="0.889"/>
<pad name="9" x="6.858" y="9.144" drill="0.889"/>
<pad name="10" x="4.572" y="9.144" drill="0.889"/>
<wire x1="-8.89" y1="-5.715" x2="-8.89" y2="10.541" width="0.127" layer="21"/>
<wire x1="-8.89" y1="10.541" x2="8.89" y2="10.541" width="0.127" layer="21"/>
<wire x1="8.89" y1="10.541" x2="8.89" y2="-5.715" width="0.127" layer="21"/>
<wire x1="8.89" y1="-5.715" x2="-8.89" y2="-5.715" width="0.127" layer="21"/>
<text x="10.16" y="7.62" size="0.8128" layer="21" font="vector" ratio="15">&gt;NAME</text>
<text x="10.16" y="6.35" size="0.8128" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<text x="9.398" y="2.54" size="0.8128" layer="21" font="vector" ratio="15" rot="R180" align="bottom-right">1</text>
<wire x1="-8.128" y1="7.112" x2="-6.858" y2="7.112" width="0.127" layer="22"/>
<wire x1="-6.858" y1="7.112" x2="-6.858" y2="7.874" width="0.127" layer="22"/>
<wire x1="-6.858" y1="7.874" x2="-5.334" y2="7.112" width="0.127" layer="22"/>
<wire x1="-5.334" y1="7.112" x2="-6.858" y2="6.35" width="0.127" layer="22"/>
<wire x1="-6.858" y1="6.35" x2="-6.858" y2="7.112" width="0.127" layer="22"/>
<wire x1="-5.334" y1="7.112" x2="-5.334" y2="7.874" width="0.127" layer="22"/>
<wire x1="-5.334" y1="7.112" x2="-5.334" y2="6.35" width="0.127" layer="22"/>
<wire x1="-5.334" y1="7.112" x2="-4.064" y2="7.112" width="0.127" layer="22"/>
<wire x1="3.683" y1="6.985" x2="4.953" y2="6.985" width="0.127" layer="22"/>
<wire x1="4.953" y1="6.985" x2="4.953" y2="7.747" width="0.127" layer="22"/>
<wire x1="4.953" y1="7.747" x2="6.477" y2="6.985" width="0.127" layer="22"/>
<wire x1="6.477" y1="6.985" x2="4.953" y2="6.223" width="0.127" layer="22"/>
<wire x1="4.953" y1="6.223" x2="4.953" y2="6.985" width="0.127" layer="22"/>
<wire x1="6.477" y1="6.985" x2="6.477" y2="7.747" width="0.127" layer="22"/>
<wire x1="6.477" y1="6.985" x2="6.477" y2="6.223" width="0.127" layer="22"/>
<wire x1="6.477" y1="6.985" x2="7.747" y2="6.985" width="0.127" layer="22"/>
<text x="3.81" y="1.27" size="0.8128" layer="22" font="vector" ratio="15" rot="MR180" align="bottom-right">1</text>
<text x="-4.064" y="5.588" size="0.8128" layer="22" font="vector" ratio="15" rot="MR0" align="bottom-right">8</text>
<text x="-7.239" y="6.731" size="0.8128" layer="22" font="vector" ratio="15" rot="MR180" align="bottom-right">12</text>
<text x="7.62" y="6.604" size="0.8128" layer="22" font="vector" ratio="15" rot="MR180" align="bottom-right">9</text>
<wire x1="-8.89" y1="10.541" x2="-8.89" y2="-5.715" width="0.127" layer="22"/>
<wire x1="-8.89" y1="-5.715" x2="8.89" y2="-5.715" width="0.127" layer="22"/>
<wire x1="8.89" y1="-5.715" x2="8.89" y2="10.541" width="0.127" layer="22"/>
<wire x1="8.89" y1="10.541" x2="-8.89" y2="10.541" width="0.127" layer="22"/>
</package>
<package name="PHOENIX_CONTACT_MC-1,5/3-G-3,81">
<pad name="P$1" x="-3.81" y="0" drill="1.2"/>
<pad name="P$2" x="0" y="0" drill="1.2"/>
<pad name="P$3" x="3.81" y="0" drill="1.2"/>
<wire x1="-7" y1="2" x2="-7" y2="-8.5" width="0.127" layer="21"/>
<wire x1="-7" y1="-8.5" x2="7" y2="-8.5" width="0.127" layer="21"/>
<wire x1="7" y1="-8.5" x2="7" y2="2" width="0.127" layer="21"/>
<wire x1="7" y1="2" x2="-7" y2="2" width="0.127" layer="21"/>
<text x="8" y="0" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="8" y="-2" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="SOIC127P1032X265-16AN">
<smd name="1" x="-4.7" y="4.445" dx="1.9" dy="0.6" layer="1"/>
<smd name="2" x="-4.7" y="3.175" dx="1.9" dy="0.6" layer="1"/>
<smd name="3" x="-4.7" y="1.905" dx="1.9" dy="0.6" layer="1"/>
<smd name="4" x="-4.7" y="0.635" dx="1.9" dy="0.6" layer="1"/>
<smd name="5" x="-4.7" y="-0.635" dx="1.9" dy="0.6" layer="1"/>
<smd name="6" x="-4.7" y="-1.905" dx="1.9" dy="0.6" layer="1"/>
<smd name="7" x="-4.7" y="-3.175" dx="1.9" dy="0.6" layer="1"/>
<smd name="8" x="-4.7" y="-4.445" dx="1.9" dy="0.6" layer="1"/>
<smd name="9" x="4.7" y="-4.445" dx="1.9" dy="0.6" layer="1"/>
<smd name="10" x="4.7" y="-3.175" dx="1.9" dy="0.6" layer="1"/>
<smd name="11" x="4.7" y="-1.905" dx="1.9" dy="0.6" layer="1"/>
<smd name="12" x="4.7" y="-0.635" dx="1.9" dy="0.6" layer="1"/>
<smd name="13" x="4.7" y="0.635" dx="1.9" dy="0.6" layer="1"/>
<smd name="14" x="4.7" y="1.905" dx="1.9" dy="0.6" layer="1"/>
<smd name="15" x="4.7" y="3.175" dx="1.9" dy="0.6" layer="1"/>
<smd name="16" x="4.7" y="4.445" dx="1.9" dy="0.6" layer="1"/>
<wire x1="-6.35" y1="5.715" x2="-6.35" y2="-5.715" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-5.715" x2="6.35" y2="-5.715" width="0.127" layer="21"/>
<wire x1="6.35" y1="-5.715" x2="6.35" y2="5.715" width="0.127" layer="21"/>
<wire x1="6.35" y1="5.715" x2="-6.35" y2="5.715" width="0.127" layer="21"/>
<wire x1="-5.715" y1="6.985" x2="-5.715" y2="6.35" width="0.127" layer="21"/>
<wire x1="-5.715" y1="6.35" x2="-5.08" y2="6.35" width="0.127" layer="21"/>
<wire x1="-5.08" y1="6.35" x2="-5.08" y2="6.985" width="0.127" layer="21"/>
<wire x1="-5.08" y1="6.985" x2="-5.715" y2="6.985" width="0.127" layer="21"/>
<wire x1="-6.35" y1="5.715" x2="6.35" y2="5.715" width="0.127" layer="51"/>
<wire x1="6.35" y1="5.715" x2="6.35" y2="-5.715" width="0.127" layer="51"/>
<wire x1="6.35" y1="-5.715" x2="-6.35" y2="-5.715" width="0.127" layer="51"/>
<wire x1="-6.35" y1="-5.715" x2="-6.35" y2="5.715" width="0.127" layer="51"/>
<wire x1="-5.715" y1="6.985" x2="-5.08" y2="6.985" width="0.127" layer="51"/>
<wire x1="-5.08" y1="6.985" x2="-5.08" y2="6.35" width="0.127" layer="51"/>
<wire x1="-5.08" y1="6.35" x2="-5.715" y2="6.35" width="0.127" layer="51"/>
<wire x1="-5.715" y1="6.35" x2="-5.715" y2="6.985" width="0.127" layer="51"/>
<wire x1="-3.175" y1="5.08" x2="3.175" y2="5.08" width="0.127" layer="51"/>
<wire x1="3.175" y1="5.08" x2="3.175" y2="-5.08" width="0.127" layer="51"/>
<wire x1="3.175" y1="-5.08" x2="-3.175" y2="-5.08" width="0.127" layer="51"/>
<wire x1="-3.175" y1="-5.08" x2="-3.175" y2="5.08" width="0.127" layer="51"/>
<text x="-3.81" y="8.89" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-3.81" y="6.35" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<wire x1="-3.175" y1="5.08" x2="3.175" y2="5.08" width="0.127" layer="39"/>
<wire x1="3.175" y1="5.08" x2="3.175" y2="-5.08" width="0.127" layer="39"/>
<wire x1="3.175" y1="-5.08" x2="-3.175" y2="-5.08" width="0.127" layer="39"/>
<wire x1="-3.175" y1="-5.08" x2="-3.175" y2="5.08" width="0.127" layer="39"/>
<wire x1="-3.175" y1="5.08" x2="3.175" y2="5.08" width="0.127" layer="40"/>
<wire x1="3.175" y1="5.08" x2="3.175" y2="-5.08" width="0.127" layer="40"/>
<wire x1="3.175" y1="-5.08" x2="-3.175" y2="-5.08" width="0.127" layer="40"/>
<wire x1="-3.175" y1="-5.08" x2="-3.175" y2="5.08" width="0.127" layer="40"/>
<polygon width="0.127" layer="41">
<vertex x="-2.54" y="5.08"/>
<vertex x="3.175" y="5.08"/>
<vertex x="3.175" y="-5.08"/>
<vertex x="-3.175" y="-5.08"/>
<vertex x="-3.175" y="5.08"/>
</polygon>
<wire x1="-3.175" y1="5.08" x2="3.175" y2="5.08" width="0.127" layer="42"/>
<wire x1="3.175" y1="5.08" x2="3.175" y2="-5.08" width="0.127" layer="42"/>
<wire x1="3.175" y1="-5.08" x2="-3.175" y2="-5.08" width="0.127" layer="42"/>
<wire x1="-3.175" y1="-5.08" x2="-3.175" y2="5.08" width="0.127" layer="42"/>
</package>
<package name="JST_PHD_14PIN_2MM">
<pad name="P$1" x="-6" y="1" drill="0.7"/>
<pad name="P$2" x="-6" y="-1" drill="0.7"/>
<pad name="P$3" x="-4" y="1" drill="0.7"/>
<pad name="P$4" x="-4" y="-1" drill="0.7"/>
<pad name="P$5" x="-2" y="1" drill="0.7"/>
<pad name="P$6" x="-2" y="-1" drill="0.7"/>
<pad name="P$7" x="0" y="1" drill="0.7"/>
<pad name="P$8" x="0" y="-1" drill="0.7"/>
<pad name="P$9" x="2" y="1" drill="0.7"/>
<pad name="P$10" x="2" y="-1" drill="0.7"/>
<pad name="P$11" x="4" y="1" drill="0.7"/>
<pad name="P$12" x="4" y="-1" drill="0.7"/>
<pad name="P$13" x="6" y="1" drill="0.7"/>
<pad name="P$14" x="6" y="-1" drill="0.7"/>
<wire x1="-8" y1="-3" x2="-8" y2="8.5" width="0.127" layer="21"/>
<wire x1="-8" y1="8.5" x2="8" y2="8.5" width="0.127" layer="21"/>
<wire x1="8" y1="8.5" x2="8" y2="-3" width="0.127" layer="21"/>
<wire x1="8" y1="-3" x2="-8" y2="-3" width="0.127" layer="21"/>
<text x="9" y="7" size="1.27" layer="21" font="vector" ratio="15">&gt;NAME</text>
<text x="9" y="5" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<wire x1="-8" y1="0" x2="-8" y2="11" width="0.127" layer="21"/>
<wire x1="-8" y1="11" x2="8" y2="11" width="0.127" layer="21"/>
<wire x1="8" y1="11" x2="8" y2="8" width="0.127" layer="21"/>
<text x="-2" y="9" size="1.27" layer="21" font="vector" ratio="15">MATE </text>
<text x="-3" y="7" size="1.27" layer="21" font="vector" ratio="15">HEADER</text>
</package>
<package name="JST_B8B-PHDSS">
<pad name="1" x="3" y="-1" drill="0.7"/>
<pad name="2" x="3" y="1" drill="0.7"/>
<pad name="3" x="1" y="-1" drill="0.7"/>
<pad name="4" x="1" y="1" drill="0.7"/>
<pad name="5" x="-1" y="-1" drill="0.7"/>
<pad name="6" x="-1" y="1" drill="0.7"/>
<pad name="7" x="-3" y="-1" drill="0.7"/>
<pad name="8" x="-3" y="1" drill="0.7"/>
<wire x1="-5.5" y1="3.5" x2="5.5" y2="3.5" width="0.127" layer="21"/>
<wire x1="5.5" y1="3.5" x2="5.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="5.5" y1="-3.5" x2="-5.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-5.5" y1="-3.5" x2="-5.5" y2="3.5" width="0.127" layer="21"/>
<wire x1="-5.5" y1="3.5" x2="5.5" y2="3.5" width="0.127" layer="51"/>
<wire x1="5.5" y1="3.5" x2="5.5" y2="-3.5" width="0.127" layer="51"/>
<wire x1="5.5" y1="-3.5" x2="-5.5" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-5.5" y1="-3.5" x2="-5.5" y2="3.5" width="0.127" layer="51"/>
<wire x1="4" y1="-4" x2="6" y2="-4" width="0.127" layer="51"/>
<wire x1="6" y1="-4" x2="6" y2="-2" width="0.127" layer="51"/>
<wire x1="4" y1="-4" x2="6" y2="-4" width="0.127" layer="21"/>
<wire x1="6" y1="-4" x2="6" y2="-2" width="0.127" layer="21"/>
<text x="6.35" y="2.54" size="0.8128" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="6.35" y="1.27" size="0.8128" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="GRAYHILL_94HXB08RAX">
<pad name="1" x="-1.27" y="2.54" drill="0.6"/>
<pad name="C1" x="-1.27" y="0" drill="0.6"/>
<pad name="4" x="-1.27" y="-2.54" drill="0.6"/>
<pad name="2" x="1.27" y="-2.54" drill="0.6"/>
<pad name="C2" x="1.27" y="0" drill="0.6"/>
<pad name="8" x="1.27" y="2.54" drill="0.6"/>
<wire x1="-5.08" y1="5.715" x2="-3.81" y2="5.715" width="0.127" layer="21"/>
<wire x1="-3.81" y1="5.715" x2="2.54" y2="5.715" width="0.127" layer="21"/>
<wire x1="2.54" y1="5.715" x2="2.54" y2="-5.715" width="0.127" layer="21"/>
<wire x1="2.54" y1="-5.715" x2="-5.08" y2="-5.715" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-5.715" x2="-5.08" y2="4.445" width="0.127" layer="21"/>
<wire x1="-5.08" y1="4.445" x2="-5.08" y2="5.715" width="0.127" layer="21"/>
<wire x1="-5.08" y1="4.445" x2="-3.81" y2="5.715" width="0.127" layer="21"/>
<wire x1="-5.461" y1="4.445" x2="-5.461" y2="6.096" width="0.127" layer="21"/>
<wire x1="-5.461" y1="6.096" x2="-3.81" y2="6.096" width="0.127" layer="21"/>
<text x="3.81" y="3.81" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="3.81" y="1.27" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="RESC6332X65N">
<wire x1="2" y1="4" x2="2" y2="-4" width="0.127" layer="21"/>
<wire x1="2" y1="-4" x2="-2" y2="-4" width="0.127" layer="21"/>
<wire x1="-2" y1="-4" x2="-2" y2="4" width="0.127" layer="21"/>
<wire x1="-2" y1="4" x2="2" y2="4" width="0.127" layer="21"/>
<smd name="1" x="0" y="3" dx="1.25" dy="3.35" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-3" dx="1.25" dy="3.35" layer="1" roundness="25" rot="R270"/>
<text x="2" y="-2" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1" y="-2" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<wire x1="2" y1="4" x2="2" y2="-4" width="0.127" layer="51"/>
<wire x1="2" y1="-4" x2="-2" y2="-4" width="0.127" layer="51"/>
<wire x1="-2" y1="-4" x2="-2" y2="4" width="0.127" layer="51"/>
<wire x1="-2" y1="4" x2="2" y2="4" width="0.127" layer="51"/>
<rectangle x1="-1.5" y1="-3" x2="1.5" y2="3" layer="39"/>
</package>
<package name="0.5MM_FIDUCIAL">
<wire x1="0" y1="1" x2="0" y2="-1" width="0.0508" layer="51"/>
<wire x1="-1" y1="0" x2="1" y2="0" width="0.0508" layer="51"/>
<circle x="0" y="0" radius="0.5" width="0" layer="1"/>
<circle x="0" y="0" radius="0.889" width="0" layer="29"/>
<circle x="0" y="0" radius="1" width="0.0508" layer="51"/>
<circle x="0" y="0" radius="1.0307" width="0" layer="41"/>
<circle x="0" y="0" radius="0.5" width="0.0508" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="TI_MSP430F5438A">
<pin name="1" x="-5.08" y="-30.48" visible="pad" length="middle"/>
<pin name="2" x="-5.08" y="-33.02" visible="pad" length="middle"/>
<pin name="3" x="-5.08" y="-35.56" visible="pad" length="middle"/>
<pin name="4" x="-5.08" y="-38.1" visible="pad" length="middle"/>
<pin name="5" x="-5.08" y="-40.64" visible="pad" length="middle"/>
<pin name="6" x="-5.08" y="-43.18" visible="pad" length="middle"/>
<pin name="7" x="-5.08" y="-45.72" visible="pad" length="middle"/>
<pin name="8" x="-5.08" y="-48.26" visible="pad" length="middle"/>
<pin name="9" x="-5.08" y="-50.8" visible="pad" length="middle"/>
<pin name="10" x="-5.08" y="-53.34" visible="pad" length="middle"/>
<pin name="11" x="-5.08" y="-55.88" visible="pad" length="middle"/>
<pin name="12" x="-5.08" y="-58.42" visible="pad" length="middle"/>
<pin name="13" x="-5.08" y="-60.96" visible="pad" length="middle"/>
<pin name="14" x="-5.08" y="-63.5" visible="pad" length="middle"/>
<pin name="15" x="-5.08" y="-66.04" visible="pad" length="middle"/>
<pin name="16" x="-5.08" y="-68.58" visible="pad" length="middle"/>
<pin name="17" x="-5.08" y="-71.12" visible="pad" length="middle"/>
<pin name="18" x="-5.08" y="-73.66" visible="pad" length="middle"/>
<pin name="19" x="-5.08" y="-76.2" visible="pad" length="middle"/>
<pin name="20" x="-5.08" y="-78.74" visible="pad" length="middle"/>
<pin name="21" x="-5.08" y="-81.28" visible="pad" length="middle"/>
<pin name="22" x="-5.08" y="-83.82" visible="pad" length="middle"/>
<pin name="23" x="-5.08" y="-86.36" visible="pad" length="middle"/>
<pin name="24" x="-5.08" y="-88.9" visible="pad" length="middle"/>
<pin name="25" x="-5.08" y="-91.44" visible="pad" length="middle"/>
<pin name="26" x="30.48" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="27" x="33.02" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="28" x="35.56" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="29" x="38.1" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="30" x="40.64" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="31" x="43.18" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="32" x="45.72" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="33" x="48.26" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="34" x="50.8" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="35" x="53.34" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="36" x="55.88" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="37" x="58.42" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="38" x="60.96" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="39" x="63.5" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="40" x="66.04" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="41" x="68.58" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="42" x="71.12" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="43" x="73.66" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="44" x="76.2" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="45" x="78.74" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="46" x="81.28" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="47" x="83.82" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="48" x="86.36" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="49" x="88.9" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="50" x="91.44" y="-127" visible="pad" length="middle" rot="R90"/>
<pin name="51" x="127" y="-91.44" visible="pad" length="middle" rot="R180"/>
<pin name="52" x="127" y="-88.9" visible="pad" length="middle" rot="R180"/>
<pin name="53" x="127" y="-86.36" visible="pad" length="middle" rot="R180"/>
<pin name="54" x="127" y="-83.82" visible="pad" length="middle" rot="R180"/>
<pin name="55" x="127" y="-81.28" visible="pad" length="middle" rot="R180"/>
<pin name="56" x="127" y="-78.74" visible="pad" length="middle" rot="R180"/>
<pin name="57" x="127" y="-76.2" visible="pad" length="middle" rot="R180"/>
<pin name="58" x="127" y="-73.66" visible="pad" length="middle" rot="R180"/>
<pin name="59" x="127" y="-71.12" visible="pad" length="middle" rot="R180"/>
<pin name="60" x="127" y="-68.58" visible="pad" length="middle" rot="R180"/>
<pin name="61" x="127" y="-66.04" visible="pad" length="middle" rot="R180"/>
<pin name="62" x="127" y="-63.5" visible="pad" length="middle" rot="R180"/>
<pin name="63" x="127" y="-60.96" visible="pad" length="middle" rot="R180"/>
<pin name="64" x="127" y="-58.42" visible="pad" length="middle" rot="R180"/>
<pin name="65" x="127" y="-55.88" visible="pad" length="middle" rot="R180"/>
<pin name="66" x="127" y="-53.34" visible="pad" length="middle" rot="R180"/>
<pin name="67" x="127" y="-50.8" visible="pad" length="middle" rot="R180"/>
<pin name="68" x="127" y="-48.26" visible="pad" length="middle" rot="R180"/>
<pin name="69" x="127" y="-45.72" visible="pad" length="middle" rot="R180"/>
<pin name="70" x="127" y="-43.18" visible="pad" length="middle" rot="R180"/>
<pin name="71" x="127" y="-40.64" visible="pad" length="middle" rot="R180"/>
<pin name="72" x="127" y="-38.1" visible="pad" length="middle" rot="R180"/>
<pin name="73" x="127" y="-35.56" visible="pad" length="middle" rot="R180"/>
<pin name="74" x="127" y="-33.02" visible="pad" length="middle" rot="R180"/>
<pin name="75" x="127" y="-30.48" visible="pad" length="middle" rot="R180"/>
<pin name="76" x="91.44" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="77" x="88.9" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="78" x="86.36" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="79" x="83.82" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="80" x="81.28" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="81" x="78.74" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="82" x="76.2" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="83" x="73.66" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="84" x="71.12" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="85" x="68.58" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="86" x="66.04" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="87" x="63.5" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="88" x="60.96" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="89" x="58.42" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="90" x="55.88" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="91" x="53.34" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="92" x="50.8" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="93" x="48.26" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="94" x="45.72" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="95" x="43.18" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="96" x="40.64" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="97" x="38.1" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="98" x="35.56" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="99" x="33.02" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="100" x="30.48" y="5.08" visible="pad" length="middle" rot="R270"/>
<text x="2.54" y="-71.12" size="1.778" layer="94">P1.0/TA0CLK/ACLK</text>
<text x="2.54" y="-73.66" size="1.778" layer="94">P1.1/TA0.0</text>
<text x="2.54" y="-76.2" size="1.778" layer="94">P1.2/TA0.1</text>
<text x="2.54" y="-78.74" size="1.778" layer="94">P1.3/TA0.2</text>
<text x="2.54" y="-81.28" size="1.778" layer="94">P1.4/TA0.3</text>
<text x="2.54" y="-83.82" size="1.778" layer="94">P1.5/TA0.4</text>
<text x="2.54" y="-86.36" size="1.778" layer="94">P1.6/SMCLK</text>
<text x="2.54" y="-88.9" size="1.778" layer="94">P1.7</text>
<text x="2.54" y="-91.44" size="1.778" layer="94">P2.0/TA1CLK/MCLK</text>
<text x="30.48" y="-119.38" size="1.778" layer="94" rot="R90">P2.1/TA1.0</text>
<text x="33.02" y="-119.38" size="1.778" layer="94" rot="R90">P2.2/TA1.1</text>
<text x="35.56" y="-119.38" size="1.778" layer="94" rot="R90">P2.3/TA1.2</text>
<text x="38.1" y="-119.38" size="1.778" layer="94" rot="R90">P2.4/RTCCLK</text>
<text x="40.64" y="-119.38" size="1.778" layer="94" rot="R90">P2.5</text>
<text x="43.18" y="-119.38" size="1.778" layer="94" rot="R90">P2.6/ACLK</text>
<text x="45.72" y="-119.38" size="1.778" layer="94" rot="R90">P2.7/ADC12CLK/DMAE0</text>
<text x="2.54" y="-55.88" size="1.778" layer="94">AVCC</text>
<text x="2.54" y="-58.42" size="1.778" layer="94">AVSS</text>
<text x="58.42" y="-119.38" size="1.778" layer="94" rot="R90">DVSS3</text>
<text x="60.96" y="-119.38" size="1.778" layer="94" rot="R90">DVCC3</text>
<text x="119.38" y="-58.42" size="1.778" layer="94" align="bottom-right">DVCC2</text>
<text x="119.38" y="-60.96" size="1.778" layer="94" align="bottom-right">DVSS2</text>
<text x="60.96" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">DVSS4</text>
<text x="63.5" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">DVCC4</text>
<text x="2.54" y="-68.58" size="1.778" layer="94">DVCC1</text>
<text x="2.54" y="-66.04" size="1.778" layer="94">DVSS1</text>
<text x="48.26" y="-119.38" size="1.778" layer="94" rot="R90">P3.0/USBSTE/UCA0CLK</text>
<text x="50.8" y="-119.38" size="1.778" layer="94" rot="R90">P3.1/UCB0SIMO/UCB0SDA</text>
<text x="53.34" y="-119.38" size="1.778" layer="94" rot="R90">P3.2/UCB0SOMI/UCB0SCL</text>
<text x="55.88" y="-119.38" size="1.778" layer="94" rot="R90">P3.2/UCB0CLK/UCA0STE</text>
<text x="63.5" y="-119.38" size="1.778" layer="94" rot="R90">P3.4/UCA0TXD/UCA0SIMO</text>
<text x="66.04" y="-119.38" size="1.778" layer="94" rot="R90">P3.5/UCA0RXD/UCA0SOMI</text>
<text x="68.58" y="-119.38" size="1.778" layer="94" rot="R90">P3.6/UCB1STE/UCA0CLK</text>
<text x="71.12" y="-119.38" size="1.778" layer="94" rot="R90">P3.7/UCB1SIMO/UCB1SDA</text>
<text x="73.66" y="-119.38" size="1.778" layer="94" rot="R90">P4.0/TB0.0</text>
<text x="76.2" y="-119.38" size="1.778" layer="94" rot="R90">P4.1/TB0.1</text>
<text x="78.74" y="-119.38" size="1.778" layer="94" rot="R90">P4.2/TB0.2</text>
<text x="81.28" y="-119.38" size="1.778" layer="94" rot="R90">P4.3/TB0.3</text>
<text x="83.82" y="-119.38" size="1.778" layer="94" rot="R90">P4.4/TB0.4</text>
<text x="86.36" y="-119.38" size="1.778" layer="94" rot="R90">P4.5/TB0.5</text>
<text x="88.9" y="-119.38" size="1.778" layer="94" rot="R90">P4.6/TB0.6</text>
<text x="91.44" y="-119.38" size="1.778" layer="94" rot="R90">P4.7/TB0CLK/SMCLK</text>
<text x="2.54" y="-30.48" size="1.778" layer="94">P6.4/A4</text>
<text x="2.54" y="-33.02" size="1.778" layer="94">P6.5/A5</text>
<text x="2.54" y="-35.56" size="1.778" layer="94">P6.6/A6</text>
<text x="2.54" y="-38.1" size="1.778" layer="94">P6.7/A7</text>
<text x="30.48" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P6.3/A3</text>
<text x="33.02" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P6.2/A2</text>
<text x="35.56" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P6.1/A1</text>
<text x="38.1" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P6.0/A0</text>
<text x="2.54" y="-40.64" size="1.778" layer="94">P7.4/A12</text>
<text x="2.54" y="-43.18" size="1.778" layer="94">P7.5/A13</text>
<text x="2.54" y="-45.72" size="1.778" layer="94">P7.6/A14</text>
<text x="2.54" y="-48.26" size="1.778" layer="94">P7.7/A15</text>
<text x="2.54" y="-50.8" size="1.778" layer="94">P5.0/A8/VREF+/VeREF+</text>
<text x="2.54" y="-53.34" size="1.778" layer="94">P5.1/A9/VREF-/VeREF-</text>
<text x="2.54" y="-60.96" size="1.778" layer="94">P7.0/XIN</text>
<text x="2.54" y="-63.5" size="1.778" layer="94">P7.1/XOUT</text>
<text x="40.64" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">!RST!/NMI/SBTDIO</text>
<text x="43.18" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">PJ.3/TCK</text>
<text x="45.72" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">PJ.2/TMS</text>
<text x="48.26" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">PJ.1/TDI/TCLK</text>
<text x="50.8" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">PJ.0/TDO</text>
<text x="53.34" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">TEST/SBWTCK</text>
<text x="55.88" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P5.3/XT2OUT</text>
<text x="58.42" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P5.2/XT2IN</text>
<text x="66.04" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P11.2/SMCLK</text>
<text x="68.58" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P11.1/MCLK</text>
<text x="71.12" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P11.0/ACLK</text>
<text x="73.66" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P10.7</text>
<text x="76.2" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P10.6</text>
<text x="78.74" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P10.5/UCA3RXD/UCA3SOMI</text>
<text x="81.28" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P10.4/UCA3TXD/UCA3SIMO</text>
<text x="83.82" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P10.3/UCA3CLK/UCA3STE</text>
<text x="86.36" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P10.2/UCB3SOMI/UCB3SCL</text>
<text x="88.9" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P10.1/UCB3SIMO/UCB3SDA</text>
<text x="91.44" y="-2.54" size="1.778" layer="94" rot="R90" align="bottom-right">P10.0/UCB3STE/UCB3CLK</text>
<text x="119.38" y="-30.48" size="1.778" layer="94" align="bottom-right">P9.7</text>
<text x="119.38" y="-33.02" size="1.778" layer="94" align="bottom-right">P9.6</text>
<text x="119.38" y="-35.56" size="1.778" layer="94" align="bottom-right">P9.5/UCA2RXD/UCA2SOMI</text>
<text x="119.38" y="-38.1" size="1.778" layer="94" align="bottom-right">P9.4/UCA2TXD/UCA2SIMO</text>
<text x="119.38" y="-40.64" size="1.778" layer="94" align="bottom-right">P9.3/UCB2CLK/UCA2STE</text>
<text x="119.38" y="-43.18" size="1.778" layer="94" align="bottom-right">P9.2/UCB2SOMI/UCB2SCL</text>
<text x="119.38" y="-45.72" size="1.778" layer="94" align="bottom-right">P9.1/UCB2SIMO/UCB2SDA</text>
<text x="119.38" y="-48.26" size="1.778" layer="94" align="bottom-right">P9.0/UCB2STE/UCA2CLK</text>
<text x="119.38" y="-50.8" size="1.778" layer="94" align="bottom-right">P8.7</text>
<text x="119.38" y="-53.34" size="1.778" layer="94" align="bottom-right">P8.6/TA1.1</text>
<text x="119.38" y="-55.88" size="1.778" layer="94" align="bottom-right">P8.5/TA1.0</text>
<text x="119.38" y="-63.5" size="1.778" layer="94" align="bottom-right">VCORE</text>
<text x="119.38" y="-66.04" size="1.778" layer="94" align="bottom-right">P8.4/TA0.4</text>
<text x="119.38" y="-68.58" size="1.778" layer="94" align="bottom-right">P8.3/TA0.3</text>
<text x="119.38" y="-71.12" size="1.778" layer="94" align="bottom-right">P8.2/TA0.2</text>
<text x="119.38" y="-73.66" size="1.778" layer="94" align="bottom-right">P8.1/TA0.1</text>
<text x="119.38" y="-76.2" size="1.778" layer="94" align="bottom-right">P8.0/TA0.0</text>
<text x="119.38" y="-78.74" size="1.778" layer="94" align="bottom-right">P7.3/TA1.2</text>
<text x="119.38" y="-81.28" size="1.778" layer="94" align="bottom-right">P7.2/TB0OUTH/SVMOUT</text>
<text x="119.38" y="-83.82" size="1.778" layer="94" align="bottom-right">P5.7/UCA1RXD/UCA1SOMI</text>
<text x="119.38" y="-86.36" size="1.778" layer="94" align="bottom-right">P5.6/UCA1TXD/UCA1SIMO</text>
<text x="119.38" y="-88.9" size="1.778" layer="94" align="bottom-right">P5.5/UCB1CLK/UCA1STE</text>
<text x="119.38" y="-91.44" size="1.778" layer="94" align="bottom-right">P5.4/UCB1SOMI/UCB1SCL</text>
<wire x1="0" y1="-121.92" x2="0" y2="-15.24" width="0.254" layer="94"/>
<wire x1="0" y1="-15.24" x2="15.24" y2="0" width="0.254" layer="94"/>
<wire x1="15.24" y1="0" x2="121.92" y2="0" width="0.254" layer="94"/>
<wire x1="121.92" y1="0" x2="121.92" y2="-121.92" width="0.254" layer="94"/>
<wire x1="121.92" y1="-121.92" x2="0" y2="-121.92" width="0.254" layer="94"/>
<text x="50.8" y="-55.88" size="1.778" layer="94">MSP430F543XAIPZ</text>
<text x="50.8" y="-48.26" size="1.778" layer="95">&gt;NAME</text>
<text x="50.8" y="-50.8" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="PIN">
<pin name="1" x="-5.08" y="0" visible="off"/>
<wire x1="-3.302" y1="0" x2="-1.27" y2="0" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="94"/>
<text x="-3.302" y="0.508" size="1.27" layer="95" ratio="15">&gt;NAME</text>
</symbol>
<symbol name="RESISTER">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-1.27" y="1.4986" size="1.27" layer="95">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="96" distance="49">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="CAPACITOR">
<wire x1="-0.635" y1="-1.016" x2="-0.635" y2="0" width="0.254" layer="94"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="1.016" width="0.254" layer="94"/>
<wire x1="0.635" y1="1.016" x2="0.635" y2="0" width="0.254" layer="94"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="0.635" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-1.27" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="NC">
<wire x1="-0.635" y1="0.635" x2="0.635" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0.635" y1="0.635" x2="-0.635" y2="-0.635" width="0.254" layer="94"/>
</symbol>
<symbol name="SWITCH_4PIN_SPST">
<text x="8.89" y="4.0386" size="1.27" layer="95">&gt;NAME</text>
<text x="8.89" y="1.778" size="1.27" layer="96" distance="49">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="3" x="-5.08" y="5.08" visible="pad" length="short" direction="pas"/>
<pin name="4" x="5.08" y="5.08" visible="pad" length="short" direction="pas" rot="R180"/>
<wire x1="-2.54" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="1.27" width="0.254" layer="94"/>
<circle x="0" y="3.556" radius="0.254" width="0.254" layer="94"/>
<circle x="0" y="1.524" radius="0.254" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.81" x2="0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="0.762" y2="1.27" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="1.778" y2="2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="TP">
<wire x1="0.762" y1="1.778" x2="0" y2="0.254" width="0.254" layer="94"/>
<wire x1="0" y1="0.254" x2="-0.762" y2="1.778" width="0.254" layer="94"/>
<wire x1="-0.762" y1="1.778" x2="0.762" y2="1.778" width="0.254" layer="94" curve="-180"/>
<text x="-1.27" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<pin name="PP" x="0" y="0" visible="off" length="short" direction="in" rot="R90"/>
</symbol>
<symbol name="LED">
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.143" y2="2.413" width="0.254" layer="94"/>
<wire x1="-1.143" y1="2.413" x2="-0.508" y2="1.778" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.778" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.143" y1="2.413" x2="0.889" y2="4.445" width="0.254" layer="94"/>
<wire x1="0.889" y1="4.445" x2="0.127" y2="4.318" width="0.254" layer="94"/>
<wire x1="0.889" y1="4.445" x2="0.762" y2="3.683" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.778" x2="1.524" y2="3.81" width="0.254" layer="94"/>
<wire x1="1.524" y1="3.81" x2="0.762" y2="3.683" width="0.254" layer="94"/>
<wire x1="1.524" y1="3.81" x2="1.397" y2="3.048" width="0.254" layer="94"/>
<text x="-7.62" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="+" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="-" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.15875" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.15875" layer="94"/>
</symbol>
<symbol name="3PIN_JUMPER">
<pin name="1" x="-5.08" y="-2.54" visible="pad" length="short" rot="R90"/>
<pin name="C" x="0" y="-2.54" visible="pad" length="short" rot="R90"/>
<pin name="2" x="5.08" y="-2.54" visible="pad" length="short" rot="R90"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="0" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="-5.08" y2="0" width="0.254" layer="94"/>
<text x="8.89" y="2.54" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="8.89" y="0" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<wire x1="-5.08" y1="0" x2="-7.62" y2="0" width="0.254" layer="94"/>
<wire x1="-4.4958" y1="2.5654" x2="-4.1148" y2="3.5814" width="0.2032" layer="94"/>
<wire x1="-4.1148" y1="3.5814" x2="-3.4798" y2="1.5494" width="0.2032" layer="94"/>
<wire x1="-3.4798" y1="1.5494" x2="-2.8448" y2="3.5814" width="0.2032" layer="94"/>
<wire x1="-2.8448" y1="3.5814" x2="-2.2098" y2="1.5494" width="0.2032" layer="94"/>
<wire x1="-2.2098" y1="1.5494" x2="-1.5748" y2="3.5814" width="0.2032" layer="94"/>
<wire x1="-1.5748" y1="3.5814" x2="-0.9398" y2="1.5494" width="0.2032" layer="94"/>
<wire x1="-4.5466" y1="2.54" x2="-5.08" y2="2.54" width="0.2032" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="0" width="0.2032" layer="94"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="2.54" width="0.2032" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="-0.0254" width="0.2032" layer="94"/>
<wire x1="-0.9398" y1="1.6002" x2="-0.635" y2="2.54" width="0.2032" layer="94"/>
<text x="-0.889" y="3.048" size="1.27" layer="97">0</text>
</symbol>
<symbol name="CRYSTAL_3PIN">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="0" width="0.254" layer="94"/>
<wire x1="1.016" y1="0" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="0" width="0.254" layer="94"/>
<wire x1="-1.016" y1="0" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="2.54" y="2.54" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="2.54" y="4.064" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="3" x="0" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R90"/>
<wire x1="-1.778" y1="-1.524" x2="-1.778" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-2.54" x2="1.778" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.778" y1="-2.54" x2="1.778" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-1.778" y1="2.54" x2="1.778" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.778" y1="2.54" x2="1.778" y2="1.524" width="0.254" layer="94"/>
<wire x1="-1.778" y1="2.54" x2="-1.778" y2="1.524" width="0.254" layer="94"/>
</symbol>
<symbol name="SCHOTTKY_DIODE">
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="3.302" x2="1.27" y2="0" width="0.254" layer="94"/>
<text x="-7.62" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="2" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="1" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-3.302" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.15875" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.15875" layer="94"/>
<wire x1="1.27" y1="3.302" x2="0.508" y2="3.302" width="0.254" layer="94"/>
<wire x1="0.508" y1="3.302" x2="0.508" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="-3.302" x2="2.032" y2="-3.302" width="0.254" layer="94"/>
<wire x1="2.032" y1="-3.302" x2="2.032" y2="-2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="INDUCTOR">
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="0" x2="1.27" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.254" layer="94" curve="-180"/>
<text x="-2.54" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="ONSEMI_LM317">
<text x="6.604" y="0" size="1.778" layer="94" ratio="15" rot="R180" align="center-left">VOUT</text>
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<pin name="2" x="10.16" y="0" visible="pad" length="short" rot="R180"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<text x="-6.604" y="0" size="1.778" layer="94" ratio="15" align="center-left">VIN</text>
<text x="-2.032" y="-3.81" size="1.778" layer="94" ratio="15" align="center-left">ADJ</text>
<text x="-3.81" y="3.556" size="1.778" layer="94" ratio="15" align="center-left">LM317</text>
<pin name="1" x="0" y="-7.62" visible="pad" length="short" rot="R90"/>
<pin name="3" x="-10.16" y="0" visible="pad" length="short"/>
<text x="-7.62" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="10.16" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="DIODE">
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="0" width="0.254" layer="94"/>
<text x="-7.62" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="+" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="-" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.15875" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.15875" layer="94"/>
</symbol>
<symbol name="PNP_TRANSISTOR">
<wire x1="-2.086" y1="-1.678" x2="-1.578" y2="-2.594" width="0.1524" layer="94"/>
<wire x1="-1.578" y1="-2.594" x2="-0.516" y2="-1.478" width="0.1524" layer="94"/>
<wire x1="-0.516" y1="-1.478" x2="-2.086" y2="-1.678" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.808" y2="-2.124" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-0.508" y2="1.524" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.778" x2="-1.524" y2="-2.413" width="0.254" layer="94"/>
<wire x1="-1.524" y1="-2.413" x2="-0.762" y2="-1.651" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-1.651" x2="-1.778" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-1.778" x2="-1.524" y2="-2.159" width="0.254" layer="94"/>
<wire x1="-1.524" y1="-2.159" x2="-1.143" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.143" y1="-1.905" x2="-1.524" y2="-1.905" width="0.254" layer="94"/>
<text x="10.16" y="-7.62" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<text x="10.16" y="-5.08" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<rectangle x1="-0.508" y1="-2.54" x2="0.254" y2="2.54" layer="94" rot="R180"/>
<pin name="B" x="2.54" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="E" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="C" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="RJ45">
<pin name="1" x="7.62" y="12.7" visible="pad" length="middle" rot="R180"/>
<pin name="2" x="7.62" y="10.16" visible="pad" length="middle" rot="R180"/>
<pin name="3" x="7.62" y="7.62" visible="pad" length="middle" rot="R180"/>
<pin name="4" x="7.62" y="5.08" visible="pad" length="middle" rot="R180"/>
<pin name="5" x="7.62" y="-5.08" visible="pad" length="middle" rot="R180"/>
<pin name="6" x="7.62" y="-7.62" visible="pad" length="middle" rot="R180"/>
<pin name="7" x="7.62" y="-10.16" visible="pad" length="middle" rot="R180"/>
<pin name="8" x="7.62" y="-12.7" visible="pad" length="middle" rot="R180"/>
<wire x1="0" y1="2.54" x2="0" y2="1.778" width="0.254" layer="94"/>
<wire x1="0" y1="1.778" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="0.762" width="0.254" layer="94"/>
<wire x1="0" y1="0.762" x2="0" y2="0.254" width="0.254" layer="94"/>
<wire x1="0" y1="0.254" x2="0" y2="-0.254" width="0.254" layer="94"/>
<wire x1="0" y1="-0.254" x2="0" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0" y1="-0.762" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="-1.778" width="0.254" layer="94"/>
<wire x1="0" y1="-1.778" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-4.064" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-4.064" y1="-2.54" x2="-4.064" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-4.064" y1="-1.27" x2="-5.08" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-1.27" x2="-5.08" y2="1.27" width="0.254" layer="94"/>
<wire x1="-5.08" y1="1.27" x2="-4.064" y2="1.27" width="0.254" layer="94"/>
<wire x1="-4.064" y1="1.27" x2="-4.064" y2="2.54" width="0.254" layer="94"/>
<wire x1="-4.064" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="0.254" x2="-0.508" y2="0.254" width="0.254" layer="94"/>
<wire x1="0" y1="0.762" x2="-0.508" y2="0.762" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-0.508" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.778" x2="-0.508" y2="1.778" width="0.254" layer="94"/>
<wire x1="0" y1="-0.254" x2="-0.508" y2="-0.254" width="0.254" layer="94"/>
<wire x1="0" y1="-0.762" x2="-0.508" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-0.508" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.778" x2="-0.508" y2="-1.778" width="0.254" layer="94"/>
<wire x1="2.54" y1="12.7" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-12.7" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="1.016" y1="-0.254" x2="1.524" y2="-0.254" width="0.127" layer="94"/>
<wire x1="1.016" y1="-0.762" x2="1.524" y2="-0.762" width="0.127" layer="94"/>
<wire x1="1.016" y1="-1.27" x2="1.524" y2="-1.27" width="0.127" layer="94"/>
<wire x1="1.016" y1="-1.778" x2="1.524" y2="-1.778" width="0.127" layer="94"/>
<wire x1="1.016" y1="0.254" x2="1.524" y2="0.254" width="0.127" layer="94"/>
<wire x1="1.016" y1="0.762" x2="1.524" y2="0.762" width="0.127" layer="94"/>
<wire x1="1.016" y1="1.27" x2="1.524" y2="1.27" width="0.127" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.524" y2="1.778" width="0.127" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="1.905" width="0.127" layer="94"/>
<wire x1="1.016" y1="1.905" x2="0.508" y2="1.905" width="0.127" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="1.651" width="0.127" layer="94"/>
<wire x1="1.016" y1="1.651" x2="0.508" y2="1.651" width="0.127" layer="94"/>
<wire x1="1.016" y1="1.27" x2="1.016" y2="1.397" width="0.127" layer="94"/>
<wire x1="1.016" y1="1.397" x2="0.508" y2="1.397" width="0.127" layer="94"/>
<wire x1="1.016" y1="1.27" x2="1.016" y2="1.143" width="0.127" layer="94"/>
<wire x1="1.016" y1="1.143" x2="0.508" y2="1.143" width="0.127" layer="94"/>
<wire x1="1.016" y1="0.762" x2="1.016" y2="0.889" width="0.127" layer="94"/>
<wire x1="1.016" y1="0.889" x2="0.508" y2="0.889" width="0.127" layer="94"/>
<wire x1="1.016" y1="0.762" x2="1.016" y2="0.635" width="0.127" layer="94"/>
<wire x1="1.016" y1="0.635" x2="0.508" y2="0.635" width="0.127" layer="94"/>
<wire x1="1.016" y1="-0.762" x2="1.016" y2="-0.635" width="0.127" layer="94"/>
<wire x1="1.016" y1="-0.635" x2="0.508" y2="-0.635" width="0.127" layer="94"/>
<wire x1="1.016" y1="-0.762" x2="1.016" y2="-0.889" width="0.127" layer="94"/>
<wire x1="1.016" y1="-0.889" x2="0.508" y2="-0.889" width="0.127" layer="94"/>
<wire x1="1.016" y1="-0.254" x2="1.016" y2="-0.127" width="0.127" layer="94"/>
<wire x1="1.016" y1="-0.127" x2="0.508" y2="-0.127" width="0.127" layer="94"/>
<wire x1="1.016" y1="-0.254" x2="1.016" y2="-0.381" width="0.127" layer="94"/>
<wire x1="1.016" y1="-0.381" x2="0.508" y2="-0.381" width="0.127" layer="94"/>
<wire x1="1.016" y1="0.254" x2="1.016" y2="0.381" width="0.127" layer="94"/>
<wire x1="1.016" y1="0.381" x2="0.508" y2="0.381" width="0.127" layer="94"/>
<wire x1="1.016" y1="0.254" x2="1.016" y2="0.127" width="0.127" layer="94"/>
<wire x1="1.016" y1="0.127" x2="0.508" y2="0.127" width="0.127" layer="94"/>
<wire x1="1.016" y1="-1.27" x2="1.016" y2="-1.143" width="0.127" layer="94"/>
<wire x1="1.016" y1="-1.143" x2="0.508" y2="-1.143" width="0.127" layer="94"/>
<wire x1="1.016" y1="-1.27" x2="1.016" y2="-1.397" width="0.127" layer="94"/>
<wire x1="1.016" y1="-1.397" x2="0.508" y2="-1.397" width="0.127" layer="94"/>
<wire x1="1.016" y1="-1.778" x2="1.016" y2="-1.651" width="0.127" layer="94"/>
<wire x1="1.016" y1="-1.651" x2="0.508" y2="-1.651" width="0.127" layer="94"/>
<wire x1="1.016" y1="-1.778" x2="1.016" y2="-1.905" width="0.127" layer="94"/>
<wire x1="1.016" y1="-1.905" x2="0.508" y2="-1.905" width="0.127" layer="94"/>
<wire x1="1.524" y1="0.254" x2="1.524" y2="4.064" width="0.254" layer="94"/>
<wire x1="1.524" y1="4.064" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="1.524" y1="-0.254" x2="1.524" y2="-4.064" width="0.254" layer="94"/>
<wire x1="1.524" y1="-4.064" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<pin name="SHIELD" x="-2.54" y="-5.08" visible="pad" length="short" rot="R90"/>
</symbol>
<symbol name="ADM2X82E/87E">
<pin name="1" x="-12.7" y="7.62" visible="pad" length="middle"/>
<pin name="2" x="-12.7" y="5.08" visible="pad" length="middle"/>
<pin name="3" x="-12.7" y="2.54" visible="pad" length="middle"/>
<pin name="4" x="-12.7" y="0" visible="pad" length="middle"/>
<pin name="5" x="-12.7" y="-2.54" visible="pad" length="middle"/>
<pin name="6" x="-12.7" y="-5.08" visible="pad" length="middle"/>
<pin name="7" x="-12.7" y="-7.62" visible="pad" length="middle"/>
<pin name="8" x="-12.7" y="-10.16" visible="pad" length="middle"/>
<pin name="9" x="12.7" y="-10.16" visible="pad" length="middle" rot="R180"/>
<pin name="10" x="12.7" y="-7.62" visible="pad" length="middle" rot="R180"/>
<pin name="11" x="12.7" y="-5.08" visible="pad" length="middle" rot="R180"/>
<pin name="12" x="12.7" y="-2.54" visible="pad" length="middle" rot="R180"/>
<pin name="13" x="12.7" y="0" visible="pad" length="middle" rot="R180"/>
<pin name="14" x="12.7" y="2.54" visible="pad" length="middle" rot="R180"/>
<pin name="15" x="12.7" y="5.08" visible="pad" length="middle" rot="R180"/>
<pin name="16" x="12.7" y="7.62" visible="pad" length="middle" rot="R180"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-12.7" x2="7.62" y2="-12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="-12.7" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<text x="-7.112" y="7.62" size="1.27" layer="94" ratio="15">GND1</text>
<text x="-7.112" y="5.08" size="1.27" layer="94" ratio="15">VCC</text>
<text x="-7.112" y="2.54" size="1.27" layer="94" ratio="15">RXD</text>
<text x="-7.112" y="0" size="1.27" layer="94" ratio="15">!RE</text>
<text x="-7.112" y="-2.54" size="1.27" layer="94" ratio="15">DE</text>
<text x="-7.112" y="-5.08" size="1.27" layer="94" ratio="15">TXD</text>
<text x="-7.112" y="-7.62" size="1.27" layer="94" ratio="15">VCC</text>
<text x="-7.112" y="-10.16" size="1.27" layer="94" ratio="15">GND1</text>
<text x="7.112" y="-10.16" size="1.27" layer="94" ratio="15" align="bottom-right">GND2</text>
<text x="7.112" y="-7.62" size="1.27" layer="94" ratio="15" align="bottom-right">VISOOUT</text>
<text x="7.112" y="-5.08" size="1.27" layer="94" ratio="15" align="bottom-right">Y</text>
<text x="7.112" y="-2.54" size="1.27" layer="94" ratio="15" align="bottom-right">Z</text>
<text x="7.112" y="0" size="1.27" layer="94" ratio="15" align="bottom-right">B</text>
<text x="7.112" y="2.54" size="1.27" layer="94" ratio="15" align="bottom-right">A</text>
<text x="7.112" y="5.08" size="1.27" layer="94" ratio="15" align="bottom-right">VISOIN</text>
<text x="7.112" y="7.62" size="1.27" layer="94" ratio="15" align="bottom-right">GND2</text>
<text x="-7.62" y="11.43" size="1.27" layer="94" ratio="15">ADM2682/87E</text>
<text x="17.78" y="7.62" size="1.27" layer="95">&gt;NAME</text>
<text x="17.78" y="5.08" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="SWITCH_ROTARY_DIP">
<circle x="0" y="0" radius="2.5903" width="0.254" layer="94"/>
<text x="-2.286" y="2.286" size="1.016" layer="94" font="vector" ratio="15" align="bottom-right">1</text>
<text x="0" y="3.048" size="1.016" layer="94" font="vector" ratio="15" align="bottom-center">2</text>
<text x="2.286" y="2.286" size="1.016" layer="94" font="vector" ratio="15">3</text>
<text x="3.048" y="0" size="1.016" layer="94" font="vector" ratio="15">4</text>
<text x="2.286" y="-2.286" size="1.016" layer="94" font="vector" ratio="15" rot="R180" align="bottom-right">5</text>
<text x="0" y="-3.048" size="1.016" layer="94" font="vector" ratio="15" align="top-center">6</text>
<text x="-2.286" y="-2.286" size="1.016" layer="94" font="vector" ratio="15" rot="R180">7</text>
<text x="-3.048" y="0" size="1.016" layer="94" font="vector" ratio="15" align="bottom-right">0</text>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<pin name="1" x="-7.62" y="2.54" visible="pad" length="short"/>
<pin name="4" x="-7.62" y="-2.54" visible="pad" length="short"/>
<pin name="C" x="0" y="-7.62" visible="pad" length="short" rot="R90"/>
<pin name="2" x="7.62" y="-2.54" visible="pad" length="short" rot="R180"/>
<pin name="8" x="7.62" y="2.54" visible="pad" length="short" rot="R180"/>
<wire x1="-0.508" y1="2.54" x2="-0.508" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.508" y1="2.54" x2="0.508" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.27" x2="-1.016" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.016" y1="-1.27" x2="-2.286" y2="0" width="0.254" layer="94"/>
<wire x1="-2.286" y1="0" x2="-1.016" y2="1.27" width="0.254" layer="94"/>
</symbol>
<symbol name="FIDUCIAL">
<circle x="0" y="0" radius="3.81" width="0" layer="94"/>
<circle x="0" y="0" radius="6.35" width="0.254" layer="94"/>
<text x="-4.826" y="13.97" size="1.778" layer="125">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="TI_MSP430F5438AIPZR" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="TI_MSP430F5438A" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFP50P1600X1600X120-100N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="100" pad="100"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="21" pad="21"/>
<connect gate="G$1" pin="22" pad="22"/>
<connect gate="G$1" pin="23" pad="23"/>
<connect gate="G$1" pin="24" pad="24"/>
<connect gate="G$1" pin="25" pad="25"/>
<connect gate="G$1" pin="26" pad="26"/>
<connect gate="G$1" pin="27" pad="27"/>
<connect gate="G$1" pin="28" pad="28"/>
<connect gate="G$1" pin="29" pad="29"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="30" pad="30"/>
<connect gate="G$1" pin="31" pad="31"/>
<connect gate="G$1" pin="32" pad="32"/>
<connect gate="G$1" pin="33" pad="33"/>
<connect gate="G$1" pin="34" pad="34"/>
<connect gate="G$1" pin="35" pad="35"/>
<connect gate="G$1" pin="36" pad="36"/>
<connect gate="G$1" pin="37" pad="37"/>
<connect gate="G$1" pin="38" pad="38"/>
<connect gate="G$1" pin="39" pad="39"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="40" pad="40"/>
<connect gate="G$1" pin="41" pad="41"/>
<connect gate="G$1" pin="42" pad="42"/>
<connect gate="G$1" pin="43" pad="43"/>
<connect gate="G$1" pin="44" pad="44"/>
<connect gate="G$1" pin="45" pad="45"/>
<connect gate="G$1" pin="46" pad="46"/>
<connect gate="G$1" pin="47" pad="47"/>
<connect gate="G$1" pin="48" pad="48"/>
<connect gate="G$1" pin="49" pad="49"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="50" pad="50"/>
<connect gate="G$1" pin="51" pad="51"/>
<connect gate="G$1" pin="52" pad="52"/>
<connect gate="G$1" pin="53" pad="53"/>
<connect gate="G$1" pin="54" pad="54"/>
<connect gate="G$1" pin="55" pad="55"/>
<connect gate="G$1" pin="56" pad="56"/>
<connect gate="G$1" pin="57" pad="57"/>
<connect gate="G$1" pin="58" pad="58"/>
<connect gate="G$1" pin="59" pad="59"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="60" pad="60"/>
<connect gate="G$1" pin="61" pad="61"/>
<connect gate="G$1" pin="62" pad="62"/>
<connect gate="G$1" pin="63" pad="63"/>
<connect gate="G$1" pin="64" pad="64"/>
<connect gate="G$1" pin="65" pad="65"/>
<connect gate="G$1" pin="66" pad="66"/>
<connect gate="G$1" pin="67" pad="67"/>
<connect gate="G$1" pin="68" pad="68"/>
<connect gate="G$1" pin="69" pad="69"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="70" pad="70"/>
<connect gate="G$1" pin="71" pad="71"/>
<connect gate="G$1" pin="72" pad="72"/>
<connect gate="G$1" pin="73" pad="73"/>
<connect gate="G$1" pin="74" pad="74"/>
<connect gate="G$1" pin="75" pad="75"/>
<connect gate="G$1" pin="76" pad="76"/>
<connect gate="G$1" pin="77" pad="77"/>
<connect gate="G$1" pin="78" pad="78"/>
<connect gate="G$1" pin="79" pad="79"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="80" pad="80"/>
<connect gate="G$1" pin="81" pad="81"/>
<connect gate="G$1" pin="82" pad="82"/>
<connect gate="G$1" pin="83" pad="83"/>
<connect gate="G$1" pin="84" pad="84"/>
<connect gate="G$1" pin="85" pad="85"/>
<connect gate="G$1" pin="86" pad="86"/>
<connect gate="G$1" pin="87" pad="87"/>
<connect gate="G$1" pin="88" pad="88"/>
<connect gate="G$1" pin="89" pad="89"/>
<connect gate="G$1" pin="9" pad="9"/>
<connect gate="G$1" pin="90" pad="90"/>
<connect gate="G$1" pin="91" pad="91"/>
<connect gate="G$1" pin="92" pad="92"/>
<connect gate="G$1" pin="93" pad="93"/>
<connect gate="G$1" pin="94" pad="94"/>
<connect gate="G$1" pin="95" pad="95"/>
<connect gate="G$1" pin="96" pad="96"/>
<connect gate="G$1" pin="97" pad="97"/>
<connect gate="G$1" pin="98" pad="98"/>
<connect gate="G$1" pin="99" pad="99"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RES_SMD_" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="RESISTER" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="RESC1005X40N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="RESC1608X50N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="RESC2013X70N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="RESC3216X84N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805_SH" package="RES2013X38N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="RESC5325X84N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="RESC6332X65N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP_SMD_" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="CAPC1005X55N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="CAPC1608X55N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="CAPC2013X145N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="CAPC3216X190N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="CAPC4532X279N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NC" prefix="NC">
<gates>
<gate name="G$1" symbol="NC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ESWITCH_TL3304AF260QL" prefix="S" uservalue="yes">
<gates>
<gate name="G$1" symbol="SWITCH_4PIN_SPST" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ESWITCH_TL3304AF260QJ">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="4"/>
<connect gate="G$1" pin="4" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TP_CLASSB_" prefix="TP">
<gates>
<gate name="G$1" symbol="TP" x="0" y="0"/>
</gates>
<devices>
<device name="1.5MM" package="TP_1.5MM">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0.4MM" package="C120H40">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0.51MM" package="C131H51">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.26MM" package="C406H326">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4.11MM" package="C491H411">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1.5MM_PAD" package="TP_1.5MM_PAD">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED_SMD_" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="LEDC1005X60N">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="LEDC1608X75N/80N">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3PIN_JUMPER_" prefix="J" uservalue="yes">
<gates>
<gate name="G$1" symbol="3PIN_JUMPER" x="0" y="0"/>
</gates>
<devices>
<device name="0603" package="3PIN_JMP1608X50N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AB26TRQ-32.768KHZ" prefix="X" uservalue="yes">
<gates>
<gate name="G$1" symbol="CRYSTAL_3PIN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="AB26TRQ">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="COMCHIP_CDBA240LL-HF" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="SCHOTTKY_DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DO-214AC_475X290">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="2" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BOURNS_SRP4020TA-100M" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="IND445X406X180">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ONSEMI_LM317" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="ONSEMI_LM317" x="0" y="0"/>
</gates>
<devices>
<device name="D2PAK-3_D2T-SUFFIX_CASE936" package="ONSEMI_LM317">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="COMCHIP_CGRA400X-G" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DO-214AC_450X280">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SMMBT2907ALT1G" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="PNP_TRANSISTOR" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="SOT-23">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RJ45_1PORT_W/2LED">
<gates>
<gate name="G$1" symbol="RJ45" x="0" y="0"/>
<gate name="G$2" symbol="LED" x="-5.08" y="-12.7"/>
<gate name="G$3" symbol="LED" x="-5.08" y="10.16"/>
</gates>
<devices>
<device name="" package="RJHSE_538X">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="SHIELD" pad="13 14"/>
<connect gate="G$2" pin="+" pad="10"/>
<connect gate="G$2" pin="-" pad="9"/>
<connect gate="G$3" pin="+" pad="12"/>
<connect gate="G$3" pin="-" pad="11"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PHOENIX_CONTACT_MC-1,5/3-G-3,81" prefix="J" uservalue="yes">
<gates>
<gate name="-1" symbol="PIN" x="5.08" y="2.54"/>
<gate name="-2" symbol="PIN" x="5.08" y="0"/>
<gate name="-3" symbol="PIN" x="5.08" y="-2.54"/>
</gates>
<devices>
<device name="" package="PHOENIX_CONTACT_MC-1,5/3-G-3,81">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ADM2X82/87E" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="ADM2X82E/87E" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC127P1032X265-16AN">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JST_PHD_14PIN_2MM" prefix="J" uservalue="yes">
<gates>
<gate name="-1" symbol="PIN" x="5.08" y="20.32"/>
<gate name="-2" symbol="PIN" x="5.08" y="17.78"/>
<gate name="-3" symbol="PIN" x="5.08" y="15.24"/>
<gate name="-4" symbol="PIN" x="5.08" y="12.7"/>
<gate name="-5" symbol="PIN" x="5.08" y="10.16"/>
<gate name="-6" symbol="PIN" x="5.08" y="7.62"/>
<gate name="-7" symbol="PIN" x="5.08" y="5.08"/>
<gate name="-8" symbol="PIN" x="5.08" y="2.54"/>
<gate name="-9" symbol="PIN" x="5.08" y="0"/>
<gate name="-10" symbol="PIN" x="5.08" y="-2.54"/>
<gate name="-11" symbol="PIN" x="5.08" y="-5.08"/>
<gate name="-12" symbol="PIN" x="5.08" y="-7.62"/>
<gate name="-13" symbol="PIN" x="5.08" y="-10.16"/>
<gate name="-14" symbol="PIN" x="5.08" y="-12.7"/>
</gates>
<devices>
<device name="14PIN_2MM" package="JST_PHD_14PIN_2MM">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-10" pin="1" pad="P$10"/>
<connect gate="-11" pin="1" pad="P$11"/>
<connect gate="-12" pin="1" pad="P$12"/>
<connect gate="-13" pin="1" pad="P$13"/>
<connect gate="-14" pin="1" pad="P$14"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
<connect gate="-5" pin="1" pad="P$5"/>
<connect gate="-6" pin="1" pad="P$6"/>
<connect gate="-7" pin="1" pad="P$7"/>
<connect gate="-8" pin="1" pad="P$8"/>
<connect gate="-9" pin="1" pad="P$9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JST_B8B-PHDSS" prefix="J" uservalue="yes">
<gates>
<gate name="-1" symbol="PIN" x="5.08" y="7.62"/>
<gate name="-2" symbol="PIN" x="5.08" y="5.08"/>
<gate name="-3" symbol="PIN" x="5.08" y="2.54"/>
<gate name="-4" symbol="PIN" x="5.08" y="0"/>
<gate name="-5" symbol="PIN" x="5.08" y="-2.54"/>
<gate name="-6" symbol="PIN" x="5.08" y="-5.08"/>
<gate name="-7" symbol="PIN" x="5.08" y="-7.62"/>
<gate name="-8" symbol="PIN" x="5.08" y="-10.16"/>
</gates>
<devices>
<device name="" package="JST_B8B-PHDSS">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
<connect gate="-5" pin="1" pad="5"/>
<connect gate="-6" pin="1" pad="6"/>
<connect gate="-7" pin="1" pad="7"/>
<connect gate="-8" pin="1" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GRAYHILL_94HXB08RAX">
<gates>
<gate name="G$1" symbol="SWITCH_ROTARY_DIP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="GRAYHILL_94HXB08RAX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="C" pad="C1 C2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0.5MM_FIDUCIAL" prefix="F">
<gates>
<gate name="G$1" symbol="FIDUCIAL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0.5MM_FIDUCIAL">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+3V3">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" prefix="+3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A3L-LOC_JMA">
<wire x1="288.29" y1="3.81" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="3.81" x2="373.38" y2="3.81" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="383.54" y2="3.81" width="0.1016" layer="94"/>
<wire x1="383.54" y1="3.81" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="383.54" y1="8.89" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="383.54" y1="13.97" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="383.54" y1="19.05" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="3.81" x2="288.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="24.13" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="342.265" y1="24.13" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="373.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="342.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="342.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<text x="344.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="344.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="357.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="343.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="387.35" y2="260.35" columns="8" rows="5" layer="94"/>
<text x="292.1" y="15.24" size="2.54" layer="94" ratio="15">Proprietory Information
JMA WIRELESS </text>
<text x="344.424" y="20.32" size="2.54" layer="95" ratio="15">&gt;AUTHER</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="A3L-LOC_JMA">
<gates>
<gate name="G$1" symbol="A3L-LOC_JMA" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U5" library="JMA_LBR" deviceset="TI_MSP430F5438AIPZR" device="" value="MSP430F5438AIPZR"/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R20" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="47K"/>
<part name="C22" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C19" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="10uF"/>
<part name="C20" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="10uF"/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="NC1" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC2" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC3" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC4" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC5" library="JMA_LBR" deviceset="NC" device=""/>
<part name="+3V2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="SHEET1" library="frames" deviceset="A3L-LOC_JMA" device=""/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="C17" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="C27" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="C33" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="C29" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="C30" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.47uF"/>
<part name="C26" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.47uF"/>
<part name="C35" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.47uF"/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="S1" library="JMA_LBR" deviceset="ESWITCH_TL3304AF260QL" device="" value="TL3304AF260QL"/>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C16" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="4.7uF"/>
<part name="C18" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="GND12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C14" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="+3V4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="R31" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="330"/>
<part name="R32" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="330"/>
<part name="D7" library="JMA_LBR" deviceset="LED_SMD_" device="0402" value="APHHS1005CGCK"/>
<part name="D8" library="JMA_LBR" deviceset="LED_SMD_" device="0402" value="APHHS1005SURCK"/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="X1" library="JMA_LBR" deviceset="AB26TRQ-32.768KHZ" device="" value="AB26TRQ-32.768KHZ-9"/>
<part name="C13" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="18pF"/>
<part name="C12" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="18pF"/>
<part name="GND14" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C32" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="GND15" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C34" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.47uF"/>
<part name="GND16" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R6" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="R14" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="C4" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C8" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="15pF"/>
<part name="C5" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="15pF"/>
<part name="GND17" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="D3" library="JMA_LBR" deviceset="COMCHIP_CDBA240LL-HF" device="" value="CDBA240LL-HF"/>
<part name="L2" library="JMA_LBR" deviceset="BOURNS_SRP4020TA-100M" device="" value="10uF"/>
<part name="R11" library="JMA_LBR" deviceset="RES_SMD_" device="0805" value="DNP"/>
<part name="R7" library="JMA_LBR" deviceset="RES_SMD_" device="0805" value="DNP"/>
<part name="U2" library="JMA_LBR" deviceset="ONSEMI_LM317" device="D2PAK-3_D2T-SUFFIX_CASE936" value="LM317D2PAK-3"/>
<part name="C3" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="C2" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="R2" library="JMA_LBR" deviceset="RES_SMD_" device="0805" value="240,1%"/>
<part name="R3" library="JMA_LBR" deviceset="RES_SMD_" device="0805" value="392,1%"/>
<part name="R1" library="JMA_LBR" deviceset="RES_SMD_" device="0805" value="50k"/>
<part name="D1" library="JMA_LBR" deviceset="COMCHIP_CGRA400X-G" device="" value="CGRA4001-G"/>
<part name="U1" library="JMA_LBR" deviceset="SMMBT2907ALT1G" device="" value="SMMBT2907ALT1G"/>
<part name="C7" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="DNP"/>
<part name="C6" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="DNP"/>
<part name="C11" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="C9" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="4.7uF"/>
<part name="C1" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="GND30" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND32" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="D10" library="JMA_LBR" deviceset="LED_SMD_" device="0402" value="APHHS1005CGCK"/>
<part name="D9" library="JMA_LBR" deviceset="LED_SMD_" device="0402" value="APHHS1005SURCK"/>
<part name="GND31" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="TP_6" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="TP_3" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="TP_1" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="TP_4" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="TP_2" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="TP_5" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="TP7" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="TP8" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="TP5" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="TP6" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="TP3" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="TP4" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="TP1" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="TP2" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="U$3" library="frames" deviceset="A3L-LOC_JMA" device=""/>
<part name="C25" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="C24" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="+3V13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND36" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND37" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R9" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="R12" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="1K"/>
<part name="GND39" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R37" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="330"/>
<part name="R38" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="330"/>
<part name="D4" library="JMA_LBR" deviceset="LED_SMD_" device="0402" value="APHHS1005CGCK"/>
<part name="D5" library="JMA_LBR" deviceset="LED_SMD_" device="0402" value="APHHS1005SYCK"/>
<part name="D6" library="JMA_LBR" deviceset="LED_SMD_" device="0402" value="APHHS1005SURCK"/>
<part name="R28" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="330"/>
<part name="R29" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="330"/>
<part name="R30" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="330"/>
<part name="GND40" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="D2" library="JMA_LBR" deviceset="LED_SMD_" device="0402" value="APHHS1005CGCK"/>
<part name="R15" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="470"/>
<part name="+3V15" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="R40" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="R41" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="R43" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="R39" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="10K"/>
<part name="R42" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="10K"/>
<part name="R44" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="10K"/>
<part name="GND46" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V18" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="C15" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.47uF"/>
<part name="GND50" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND51" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND52" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R5" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP 0"/>
<part name="J2" library="JMA_LBR" deviceset="3PIN_JUMPER_" device="0603" value="0R"/>
<part name="J3" library="JMA_LBR" deviceset="3PIN_JUMPER_" device="0603" value="0R"/>
<part name="J4" library="JMA_LBR" deviceset="RJ45_1PORT_W/2LED" device=""/>
<part name="J1" library="JMA_LBR" deviceset="RJ45_1PORT_W/2LED" device=""/>
<part name="J5" library="JMA_LBR" deviceset="RJ45_1PORT_W/2LED" device=""/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND20" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND21" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U3" library="JMA_LBR" deviceset="ADM2X82/87E" device=""/>
<part name="R10" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="120"/>
<part name="R8" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="R13" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="R27" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="R22" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="C23" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C21" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="15pF"/>
<part name="C28" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="15pF"/>
<part name="GND18" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="R26" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="1K"/>
<part name="R24" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="1K"/>
<part name="GND19" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="J8" library="JMA_LBR" deviceset="PHOENIX_CONTACT_MC-1,5/3-G-3,81" device=""/>
<part name="U4" library="JMA_LBR" deviceset="ADM2X82/87E" device=""/>
<part name="R23" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="R25" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="R21" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="J7" library="JMA_LBR" deviceset="JST_PHD_14PIN_2MM" device="14PIN_2MM" value="S14B-PHDS"/>
<part name="J6" library="JMA_LBR" deviceset="JST_B8B-PHDSS" device=""/>
<part name="J9" library="JMA_LBR" deviceset="GRAYHILL_94HXB08RAX" device=""/>
<part name="R33" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="10K"/>
<part name="R35" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="10K"/>
<part name="R36" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="10K"/>
<part name="R34" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="10K"/>
<part name="+3V8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND22" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R16" library="JMA_LBR" deviceset="RES_SMD_" device="2512" value="1.1K, 1W"/>
<part name="L1" library="JMA_LBR" deviceset="BOURNS_SRP4020TA-100M" device="" value="10uF"/>
<part name="COM_RX" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="COM_TX" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="GND_6" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="GND_7" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="GND_3" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="GND_5" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="GND_2" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="GND_4" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="+3V3_TP" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="GND23" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U2_RXD" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="U2_TXD" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="GND_1" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="GND_8" library="JMA_LBR" deviceset="TP_CLASSB_" device="1.5MM"/>
<part name="C10" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C31" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="R4" library="JMA_LBR" deviceset="RES_SMD_" device="0805" value="330"/>
<part name="R19" library="JMA_LBR" deviceset="RES_SMD_" device="0805" value="330"/>
<part name="GND13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R18" library="JMA_LBR" deviceset="RES_SMD_" device="0805" value="330"/>
<part name="R17" library="JMA_LBR" deviceset="RES_SMD_" device="0805" value="330"/>
<part name="GND24" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="F1" library="JMA_LBR" deviceset="0.5MM_FIDUCIAL" device=""/>
<part name="F2" library="JMA_LBR" deviceset="0.5MM_FIDUCIAL" device=""/>
<part name="F3" library="JMA_LBR" deviceset="0.5MM_FIDUCIAL" device=""/>
<part name="R45" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP 0"/>
<part name="GND25" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="141.986" y="209.042" size="1.27" layer="97">Programming Pins</text>
<text x="320.04" y="40.64" size="1.27" layer="97">Vcore</text>
<text x="294.64" y="40.64" size="1.27" layer="97">DVCC(1-4)</text>
<text x="17.526" y="124.968" size="1.27" layer="97">External AVss</text>
<text x="178.816" y="216.662" size="1.27" layer="97">Debug UART_3</text>
<text x="177.038" y="222.25" size="1.27" layer="97" rot="R90">Green</text>
<text x="184.658" y="223.52" size="1.27" layer="97" rot="R90">Red</text>
<text x="167.894" y="34.29" size="1.27" layer="97" rot="R90">Delat_RS485_
To_UART_In</text>
<text x="245.618" y="67.31" size="1.27" layer="97" rot="R90">Green</text>
<text x="237.998" y="68.58" size="1.27" layer="97" rot="R90">Red</text>
<wire x1="287.02" y1="68.58" x2="340.36" y2="68.58" width="0.1524" layer="97" style="shortdash"/>
<wire x1="340.36" y1="68.58" x2="342.9" y2="66.04" width="0.1524" layer="97" style="shortdash"/>
<wire x1="342.9" y1="66.04" x2="342.9" y2="35.56" width="0.1524" layer="97" style="shortdash"/>
<wire x1="342.9" y1="35.56" x2="340.36" y2="33.02" width="0.1524" layer="97" style="shortdash"/>
<wire x1="340.36" y1="33.02" x2="287.02" y2="33.02" width="0.1524" layer="97" style="shortdash"/>
<wire x1="287.02" y1="33.02" x2="284.48" y2="35.56" width="0.1524" layer="97" style="shortdash"/>
<wire x1="284.48" y1="35.56" x2="284.48" y2="66.04" width="0.1524" layer="97" style="shortdash"/>
<wire x1="284.48" y1="66.04" x2="287.02" y2="68.58" width="0.1524" layer="97" style="shortdash"/>
<text x="287.02" y="71.12" size="1.778" layer="97">Decoupling Capacitor</text>
<wire x1="10.16" y1="251.46" x2="274.32" y2="251.46" width="0.1524" layer="97" style="shortdash"/>
<wire x1="274.32" y1="251.46" x2="276.86" y2="248.92" width="0.1524" layer="97" style="shortdash"/>
<wire x1="276.86" y1="248.92" x2="276.86" y2="10.16" width="0.1524" layer="97" style="shortdash"/>
<wire x1="276.86" y1="10.16" x2="274.32" y2="7.62" width="0.1524" layer="97" style="shortdash"/>
<wire x1="274.32" y1="7.62" x2="10.16" y2="7.62" width="0.1524" layer="97" style="shortdash"/>
<wire x1="10.16" y1="7.62" x2="7.62" y2="10.16" width="0.1524" layer="97" style="shortdash"/>
<wire x1="7.62" y1="10.16" x2="7.62" y2="248.92" width="0.1524" layer="97" style="shortdash"/>
<wire x1="7.62" y1="248.92" x2="10.16" y2="251.46" width="0.1524" layer="97" style="shortdash"/>
<text x="363.22" y="111.76" size="1.27" layer="97" ratio="15" rot="R270" align="bottom-right">Status LED</text>
<text x="363.22" y="91.44" size="1.27" layer="97" ratio="15" rot="R270" align="bottom-right">Power LED</text>
<text x="347.98" y="124.46" size="1.27" layer="97" ratio="15">STATUS: GOOD</text>
<text x="342.9" y="116.84" size="1.27" layer="97" ratio="15">STATUS: REGULATING</text>
<text x="347.98" y="109.22" size="1.27" layer="97" ratio="15">STATUS: FAULT</text>
<text x="350.52" y="96.52" size="1.27" layer="97" ratio="15">3.3V LED</text>
<wire x1="287.02" y1="251.46" x2="370.84" y2="251.46" width="0.1524" layer="97" style="shortdash"/>
<wire x1="370.84" y1="251.46" x2="373.38" y2="248.92" width="0.1524" layer="97" style="shortdash"/>
<wire x1="373.38" y1="248.92" x2="373.38" y2="78.74" width="0.1524" layer="97" style="shortdash"/>
<wire x1="373.38" y1="78.74" x2="370.84" y2="76.2" width="0.1524" layer="97" style="shortdash"/>
<wire x1="370.84" y1="76.2" x2="287.02" y2="76.2" width="0.1524" layer="97" style="shortdash"/>
<wire x1="287.02" y1="76.2" x2="284.48" y2="78.74" width="0.1524" layer="97" style="shortdash"/>
<wire x1="284.48" y1="78.74" x2="284.48" y2="248.92" width="0.1524" layer="97" style="shortdash"/>
<wire x1="284.48" y1="248.92" x2="287.02" y2="251.46" width="0.1524" layer="97" style="shortdash"/>
<text x="10.16" y="254" size="1.778" layer="97" ratio="15">MSP430 Micro-processor</text>
<text x="287.02" y="254" size="1.778" layer="97" ratio="15">FAULT, STATUS AND POWER LED</text>
<text x="101.6" y="187.96" size="1.778" layer="97">U1</text>
<text x="11.938" y="131.826" size="1.778" layer="97">J4</text>
</plain>
<instances>
<instance part="U5" gate="G$1" x="101.6" y="187.96" smashed="yes"/>
<instance part="SHEET1" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="344.17" y="15.24" size="1.27" layer="94"/>
<attribute name="LAST_DATE_TIME" x="344.17" y="10.16" size="1.27" layer="94"/>
<attribute name="SHEET" x="357.505" y="5.08" size="1.27" layer="94"/>
</instance>
<instance part="GND7" gate="1" x="162.56" y="200.66" smashed="yes" rot="R180">
<attribute name="VALUE" x="165.1" y="203.2" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="+3V3" gate="G$1" x="294.64" y="63.5" smashed="yes">
<attribute name="VALUE" x="292.1" y="58.42" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C17" gate="G$1" x="294.64" y="50.8" smashed="yes" rot="R270">
<attribute name="NAME" x="294.132" y="45.72" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="294.132" y="52.07" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C27" gate="G$1" x="299.72" y="50.8" smashed="yes" rot="R270">
<attribute name="NAME" x="299.212" y="45.72" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="299.212" y="52.07" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C33" gate="G$1" x="304.8" y="50.8" smashed="yes" rot="R270">
<attribute name="NAME" x="304.292" y="45.72" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="304.292" y="52.07" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C29" gate="G$1" x="309.88" y="50.8" smashed="yes" rot="R270">
<attribute name="NAME" x="309.372" y="45.72" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="309.372" y="52.07" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C30" gate="G$1" x="320.04" y="50.8" smashed="yes" rot="R270">
<attribute name="NAME" x="319.532" y="45.72" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="319.532" y="52.07" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C26" gate="G$1" x="325.12" y="50.8" smashed="yes" rot="R270">
<attribute name="NAME" x="324.612" y="45.72" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="324.612" y="52.07" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C35" gate="G$1" x="330.2" y="50.8" smashed="yes" rot="R270">
<attribute name="NAME" x="329.692" y="45.72" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="329.692" y="52.07" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND8" gate="1" x="330.2" y="38.1" smashed="yes">
<attribute name="VALUE" x="327.66" y="35.56" size="1.27" layer="96"/>
</instance>
<instance part="GND9" gate="1" x="236.22" y="127" smashed="yes" rot="R90">
<attribute name="VALUE" x="238.76" y="124.46" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="GND10" gate="1" x="160.02" y="53.34" smashed="yes">
<attribute name="VALUE" x="158.242" y="51.562" size="1.27" layer="96"/>
</instance>
<instance part="GND11" gate="1" x="88.9" y="121.92" smashed="yes" rot="R270">
<attribute name="VALUE" x="84.582" y="121.666" size="1.27" layer="96"/>
</instance>
<instance part="C16" gate="G$1" x="58.42" y="149.86" smashed="yes" rot="R270">
<attribute name="NAME" x="57.912" y="144.78" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="57.912" y="151.13" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C18" gate="G$1" x="53.34" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="52.832" y="144.78" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="52.832" y="151.13" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND12" gate="1" x="30.48" y="124.46" smashed="yes">
<attribute name="VALUE" x="33.02" y="124.46" size="1.27" layer="96"/>
</instance>
<instance part="C14" gate="G$1" x="63.5" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="62.992" y="144.78" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="62.992" y="151.13" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="+3V4" gate="G$1" x="53.34" y="165.1" smashed="yes">
<attribute name="VALUE" x="51.054" y="165.354" size="1.27" layer="96"/>
</instance>
<instance part="R31" gate="G$1" x="175.26" y="213.36" smashed="yes" rot="R90">
<attribute name="NAME" x="174.7774" y="207.772" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="174.752" y="216.408" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R32" gate="G$1" x="177.8" y="213.36" smashed="yes" rot="R90">
<attribute name="NAME" x="177.3174" y="207.772" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="177.292" y="216.408" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="D7" gate="G$1" x="175.26" y="228.6" smashed="yes" rot="R90">
<attribute name="NAME" x="174.752" y="223.52" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="174.752" y="230.378" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="D8" gate="G$1" x="182.88" y="228.6" smashed="yes" rot="R90">
<attribute name="NAME" x="182.372" y="223.52" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="182.372" y="230.378" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND5" gate="1" x="187.96" y="241.3" smashed="yes">
<attribute name="VALUE" x="185.42" y="238.76" size="1.27" layer="96"/>
</instance>
<instance part="X1" gate="G$1" x="48.26" y="116.84" smashed="yes">
<attribute name="NAME" x="46.482" y="119.888" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="41.402" y="124.968" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C13" gate="G$1" x="40.64" y="109.22" smashed="yes" rot="R90">
<attribute name="NAME" x="40.132" y="104.14" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="40.132" y="110.49" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C12" gate="G$1" x="55.88" y="109.22" smashed="yes" rot="R90">
<attribute name="NAME" x="55.372" y="104.14" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="55.372" y="110.49" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND14" gate="1" x="40.64" y="96.52" smashed="yes">
<attribute name="VALUE" x="38.1" y="93.98" size="1.27" layer="96"/>
</instance>
<instance part="C32" gate="G$1" x="261.62" y="129.54" smashed="yes" rot="R180">
<attribute name="NAME" x="256.54" y="130.048" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="262.89" y="130.048" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND15" gate="1" x="269.24" y="119.38" smashed="yes">
<attribute name="VALUE" x="266.7" y="116.84" size="1.27" layer="96"/>
</instance>
<instance part="C34" gate="G$1" x="261.62" y="124.46" smashed="yes">
<attribute name="NAME" x="256.54" y="124.968" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="262.89" y="124.968" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="+3V7" gate="G$1" x="254" y="134.62" smashed="yes">
<attribute name="VALUE" x="251.46" y="134.62" size="1.27" layer="96"/>
</instance>
<instance part="D10" gate="G$1" x="243.84" y="73.66" smashed="yes" rot="R270">
<attribute name="NAME" x="243.332" y="68.58" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="249.682" y="65.532" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="D9" gate="G$1" x="236.22" y="73.66" smashed="yes" rot="R270">
<attribute name="NAME" x="235.712" y="68.58" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="233.68" y="66.04" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND31" gate="1" x="233.68" y="58.42" smashed="yes">
<attribute name="VALUE" x="231.902" y="56.642" size="1.27" layer="96"/>
</instance>
<instance part="TP_6" gate="G$1" x="88.9" y="114.3" smashed="yes" rot="R90">
<attribute name="NAME" x="83.82" y="114.3" size="1.27" layer="95" rot="R180"/>
</instance>
<instance part="TP_3" gate="G$1" x="88.9" y="116.84" smashed="yes" rot="R90">
<attribute name="NAME" x="83.82" y="116.84" size="1.27" layer="95" rot="R180"/>
</instance>
<instance part="TP_1" gate="G$1" x="88.9" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="83.82" y="149.86" size="1.27" layer="95" align="bottom-right"/>
</instance>
<instance part="TP_4" gate="G$1" x="88.9" y="152.4" smashed="yes" rot="R90">
<attribute name="NAME" x="83.82" y="152.4" size="1.27" layer="95" align="bottom-right"/>
</instance>
<instance part="TP_2" gate="G$1" x="88.9" y="154.94" smashed="yes" rot="R90">
<attribute name="NAME" x="83.82" y="154.94" size="1.27" layer="95" align="bottom-right"/>
</instance>
<instance part="TP_5" gate="G$1" x="88.9" y="157.48" smashed="yes" rot="R90">
<attribute name="NAME" x="83.82" y="157.48" size="1.27" layer="95" align="bottom-right"/>
</instance>
<instance part="TP7" gate="G$1" x="154.94" y="55.88" smashed="yes" rot="R180">
<attribute name="NAME" x="154.94" y="50.8" size="1.27" layer="95" rot="R90" align="bottom-right"/>
</instance>
<instance part="TP8" gate="G$1" x="152.4" y="55.88" smashed="yes" rot="R180">
<attribute name="NAME" x="152.4" y="50.8" size="1.27" layer="95" rot="R90" align="bottom-right"/>
</instance>
<instance part="TP5" gate="G$1" x="149.86" y="55.88" smashed="yes" rot="R180">
<attribute name="NAME" x="149.86" y="50.8" size="1.27" layer="95" rot="R90" align="bottom-right"/>
</instance>
<instance part="TP6" gate="G$1" x="147.32" y="55.88" smashed="yes" rot="R180">
<attribute name="NAME" x="147.32" y="50.8" size="1.27" layer="95" rot="R90" align="bottom-right"/>
</instance>
<instance part="TP3" gate="G$1" x="137.16" y="55.88" smashed="yes" rot="R180">
<attribute name="NAME" x="137.16" y="50.8" size="1.27" layer="95" rot="R90" align="bottom-right"/>
</instance>
<instance part="TP4" gate="G$1" x="134.62" y="55.88" smashed="yes" rot="R180">
<attribute name="NAME" x="134.62" y="50.8" size="1.27" layer="95" rot="R90" align="bottom-right"/>
</instance>
<instance part="TP1" gate="G$1" x="132.08" y="55.88" smashed="yes" rot="R180">
<attribute name="NAME" x="132.08" y="50.8" size="1.27" layer="95" rot="R90" align="bottom-right"/>
</instance>
<instance part="TP2" gate="G$1" x="91.44" y="96.52" smashed="yes" rot="R90">
<attribute name="NAME" x="76.2" y="96.52" size="1.27" layer="95"/>
</instance>
<instance part="R37" gate="G$1" x="236.22" y="86.36" smashed="yes" rot="R270">
<attribute name="NAME" x="235.7374" y="80.264" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="235.712" y="89.408" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R38" gate="G$1" x="243.84" y="86.36" smashed="yes" rot="R270">
<attribute name="NAME" x="243.3574" y="80.518" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="243.332" y="89.408" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="D4" gate="G$1" x="337.82" y="127" smashed="yes">
<attribute name="NAME" x="340.868" y="129.286" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="340.868" y="127.508" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="D5" gate="G$1" x="337.82" y="119.38" smashed="yes">
<attribute name="NAME" x="340.868" y="121.666" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="340.868" y="119.888" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="D6" gate="G$1" x="337.82" y="111.76" smashed="yes">
<attribute name="NAME" x="340.614" y="114.046" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="340.614" y="112.268" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R28" gate="G$1" x="322.58" y="127" smashed="yes" rot="R180">
<attribute name="NAME" x="316.23" y="127.4826" size="1.27" layer="95"/>
<attribute name="VALUE" x="325.12" y="127.508" size="1.27" layer="96"/>
</instance>
<instance part="R29" gate="G$1" x="322.58" y="119.38" smashed="yes" rot="R180">
<attribute name="NAME" x="316.23" y="119.8626" size="1.27" layer="95"/>
<attribute name="VALUE" x="325.12" y="119.888" size="1.27" layer="96"/>
</instance>
<instance part="R30" gate="G$1" x="322.58" y="111.76" smashed="yes" rot="R180">
<attribute name="NAME" x="316.23" y="112.2426" size="1.27" layer="95"/>
<attribute name="VALUE" x="325.12" y="112.268" size="1.27" layer="96"/>
</instance>
<instance part="GND40" gate="1" x="360.68" y="86.36" smashed="yes">
<attribute name="VALUE" x="358.14" y="83.82" size="1.27" layer="96"/>
</instance>
<instance part="D2" gate="G$1" x="337.82" y="99.06" smashed="yes">
<attribute name="NAME" x="340.614" y="101.346" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="340.614" y="99.568" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R15" gate="G$1" x="322.58" y="99.06" smashed="yes" rot="R180">
<attribute name="NAME" x="316.23" y="99.5426" size="1.27" layer="95"/>
<attribute name="VALUE" x="325.12" y="99.568" size="1.27" layer="96"/>
</instance>
<instance part="+3V15" gate="G$1" x="302.26" y="106.68" smashed="yes">
<attribute name="VALUE" x="299.72" y="101.6" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R40" gate="G$1" x="238.76" y="231.14" smashed="yes" rot="R90">
<attribute name="NAME" x="238.2774" y="225.552" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="238.252" y="234.188" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R41" gate="G$1" x="241.3" y="231.14" smashed="yes" rot="R90">
<attribute name="NAME" x="240.8174" y="225.552" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="240.792" y="234.188" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R43" gate="G$1" x="243.84" y="231.14" smashed="yes" rot="R90">
<attribute name="NAME" x="243.3574" y="225.552" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="243.332" y="234.188" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R39" gate="G$1" x="238.76" y="210.82" smashed="yes" rot="R90">
<attribute name="NAME" x="238.2774" y="205.232" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="238.252" y="213.868" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R42" gate="G$1" x="241.3" y="210.82" smashed="yes" rot="R90">
<attribute name="NAME" x="240.8174" y="205.232" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="240.792" y="213.868" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R44" gate="G$1" x="243.84" y="210.82" smashed="yes" rot="R90">
<attribute name="NAME" x="243.3574" y="205.232" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="243.332" y="213.868" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="GND46" gate="1" x="238.76" y="198.12" smashed="yes">
<attribute name="VALUE" x="236.22" y="195.58" size="1.27" layer="96"/>
</instance>
<instance part="+3V18" gate="G$1" x="238.76" y="243.84" smashed="yes">
<attribute name="VALUE" x="236.22" y="238.76" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C15" gate="G$1" x="314.96" y="50.8" smashed="yes" rot="R270">
<attribute name="NAME" x="314.452" y="45.72" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="314.452" y="52.07" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="J9" gate="G$1" x="327.66" y="205.74"/>
<instance part="R33" gate="G$1" x="314.96" y="220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="314.4774" y="215.392" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="314.452" y="224.028" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R35" gate="G$1" x="309.88" y="220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="309.3974" y="215.392" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="309.372" y="224.028" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R36" gate="G$1" x="340.36" y="220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="339.8774" y="215.392" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="339.852" y="224.028" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R34" gate="G$1" x="345.44" y="220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="344.9574" y="215.392" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="344.932" y="224.028" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="+3V8" gate="G$1" x="327.66" y="236.22" smashed="yes">
<attribute name="VALUE" x="325.12" y="231.14" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="GND22" gate="1" x="327.66" y="190.5" smashed="yes">
<attribute name="VALUE" x="325.12" y="187.96" size="1.27" layer="96"/>
</instance>
<instance part="COM_RX" gate="G$1" x="167.64" y="48.26" smashed="yes" rot="R180"/>
<instance part="COM_TX" gate="G$1" x="165.1" y="48.26" smashed="yes" rot="R180"/>
<instance part="GND_6" gate="G$1" x="81.28" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="71.12" y="50.8" size="1.27" layer="95"/>
</instance>
<instance part="GND_7" gate="G$1" x="81.28" y="48.26" smashed="yes" rot="R90">
<attribute name="NAME" x="71.12" y="48.26" size="1.27" layer="95"/>
</instance>
<instance part="GND_3" gate="G$1" x="81.28" y="45.72" smashed="yes" rot="R90">
<attribute name="NAME" x="71.12" y="45.72" size="1.27" layer="95"/>
</instance>
<instance part="GND_5" gate="G$1" x="81.28" y="43.18" smashed="yes" rot="R90">
<attribute name="NAME" x="71.12" y="43.18" size="1.27" layer="95"/>
</instance>
<instance part="GND_2" gate="G$1" x="81.28" y="40.64" smashed="yes" rot="R90">
<attribute name="NAME" x="71.12" y="40.64" size="1.27" layer="95"/>
</instance>
<instance part="GND_4" gate="G$1" x="81.28" y="38.1" smashed="yes" rot="R90">
<attribute name="NAME" x="71.12" y="38.1" size="1.27" layer="95"/>
</instance>
<instance part="+3V3_TP" gate="G$1" x="297.18" y="99.06" smashed="yes" rot="R90">
<attribute name="NAME" x="289.56" y="96.52" size="1.27" layer="95"/>
</instance>
<instance part="GND23" gate="1" x="88.9" y="27.94" smashed="yes">
<attribute name="VALUE" x="86.36" y="25.4" size="1.27" layer="96"/>
</instance>
<instance part="U2_RXD" gate="G$1" x="256.54" y="162.56" smashed="yes" rot="R270">
<attribute name="NAME" x="261.62" y="162.56" size="1.27" layer="95" rot="R180" align="bottom-right"/>
</instance>
<instance part="U2_TXD" gate="G$1" x="256.54" y="160.02" smashed="yes" rot="R270">
<attribute name="NAME" x="261.62" y="160.02" size="1.27" layer="95" rot="R180" align="bottom-right"/>
</instance>
<instance part="GND_1" gate="G$1" x="81.28" y="35.56" smashed="yes" rot="R90">
<attribute name="NAME" x="71.12" y="35.56" size="1.27" layer="95"/>
</instance>
<instance part="GND_8" gate="G$1" x="81.28" y="33.02" smashed="yes" rot="R90">
<attribute name="NAME" x="71.12" y="33.02" size="1.27" layer="95"/>
</instance>
<instance part="F1" gate="G$1" x="208.28" y="25.4"/>
<instance part="F2" gate="G$1" x="226.06" y="25.4"/>
<instance part="F3" gate="G$1" x="243.84" y="25.4"/>
</instances>
<busses>
<bus name="TDO,TDI,TMS,TCK,!RST,TEST">
<segment>
<wire x1="142.24" y1="208.28" x2="154.94" y2="208.28" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="DBG_TX,DBG_RX">
<segment>
<wire x1="180.34" y1="215.9" x2="182.88" y2="215.9" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="TDO" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="92"/>
<wire x1="152.4" y1="193.04" x2="152.4" y2="208.28" width="0.1524" layer="91"/>
<label x="152.4" y="195.58" size="1.27" layer="95" rot="R90"/>
</segment>
</net>
<net name="TDI" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="93"/>
<wire x1="149.86" y1="193.04" x2="149.86" y2="208.28" width="0.1524" layer="91"/>
<label x="149.86" y="195.58" size="1.27" layer="95" rot="R90"/>
</segment>
</net>
<net name="TMS" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="94"/>
<wire x1="147.32" y1="193.04" x2="147.32" y2="208.28" width="0.1524" layer="91"/>
<label x="147.32" y="195.58" size="1.27" layer="95" rot="R90"/>
</segment>
</net>
<net name="TCK" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="95"/>
<wire x1="144.78" y1="193.04" x2="144.78" y2="208.28" width="0.1524" layer="91"/>
<label x="144.78" y="195.58" size="1.27" layer="95" rot="R90"/>
</segment>
</net>
<net name="!RST" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="96"/>
<wire x1="142.24" y1="193.04" x2="142.24" y2="208.28" width="0.1524" layer="91"/>
<label x="142.24" y="195.58" size="1.27" layer="95" rot="R90"/>
</segment>
</net>
<net name="TEST" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="91"/>
<wire x1="154.94" y1="193.04" x2="154.94" y2="208.28" width="0.1524" layer="91"/>
<label x="154.94" y="195.58" size="1.27" layer="95" rot="R90"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="88"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="162.56" y1="193.04" x2="162.56" y2="198.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="294.64" y1="45.72" x2="294.64" y2="43.18" width="0.1524" layer="91"/>
<wire x1="294.64" y1="43.18" x2="299.72" y2="43.18" width="0.1524" layer="91"/>
<wire x1="299.72" y1="43.18" x2="304.8" y2="43.18" width="0.1524" layer="91"/>
<wire x1="304.8" y1="43.18" x2="309.88" y2="43.18" width="0.1524" layer="91"/>
<wire x1="309.88" y1="43.18" x2="314.96" y2="43.18" width="0.1524" layer="91"/>
<wire x1="314.96" y1="43.18" x2="320.04" y2="43.18" width="0.1524" layer="91"/>
<wire x1="320.04" y1="43.18" x2="325.12" y2="43.18" width="0.1524" layer="91"/>
<wire x1="325.12" y1="43.18" x2="330.2" y2="43.18" width="0.1524" layer="91"/>
<wire x1="330.2" y1="43.18" x2="330.2" y2="40.64" width="0.1524" layer="91"/>
<pinref part="C35" gate="G$1" pin="2"/>
<wire x1="330.2" y1="45.72" x2="330.2" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="2"/>
<wire x1="299.72" y1="45.72" x2="299.72" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C33" gate="G$1" pin="2"/>
<wire x1="304.8" y1="45.72" x2="304.8" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="2"/>
<wire x1="309.88" y1="45.72" x2="309.88" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C30" gate="G$1" pin="2"/>
<wire x1="320.04" y1="45.72" x2="320.04" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C26" gate="G$1" pin="2"/>
<wire x1="325.12" y1="45.72" x2="325.12" y2="43.18" width="0.1524" layer="91"/>
<junction x="299.72" y="43.18"/>
<junction x="304.8" y="43.18"/>
<junction x="309.88" y="43.18"/>
<junction x="320.04" y="43.18"/>
<junction x="325.12" y="43.18"/>
<junction x="330.2" y="43.18"/>
<pinref part="GND8" gate="1" pin="GND"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="314.96" y1="45.72" x2="314.96" y2="43.18" width="0.1524" layer="91"/>
<junction x="314.96" y="43.18"/>
</segment>
<segment>
<pinref part="GND11" gate="1" pin="GND"/>
<pinref part="U5" gate="G$1" pin="15"/>
<wire x1="91.44" y1="121.92" x2="96.52" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="63"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="228.6" y1="127" x2="233.68" y2="127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="37"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="160.02" y1="60.96" x2="160.02" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D7" gate="G$1" pin="-"/>
<wire x1="175.26" y1="233.68" x2="175.26" y2="246.38" width="0.1524" layer="91"/>
<wire x1="175.26" y1="246.38" x2="182.88" y2="246.38" width="0.1524" layer="91"/>
<pinref part="D8" gate="G$1" pin="-"/>
<wire x1="182.88" y1="233.68" x2="182.88" y2="246.38" width="0.1524" layer="91"/>
<junction x="182.88" y="246.38"/>
<wire x1="182.88" y1="246.38" x2="187.96" y2="246.38" width="0.1524" layer="91"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="187.96" y1="246.38" x2="187.96" y2="243.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="40.64" y1="104.14" x2="40.64" y2="101.6" width="0.1524" layer="91"/>
<wire x1="40.64" y1="101.6" x2="48.26" y2="101.6" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="48.26" y1="101.6" x2="55.88" y2="101.6" width="0.1524" layer="91"/>
<wire x1="55.88" y1="101.6" x2="55.88" y2="104.14" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="3"/>
<wire x1="48.26" y1="111.76" x2="48.26" y2="101.6" width="0.1524" layer="91"/>
<wire x1="40.64" y1="101.6" x2="40.64" y2="99.06" width="0.1524" layer="91"/>
<pinref part="GND14" gate="1" pin="GND"/>
<junction x="40.64" y="101.6"/>
<junction x="48.26" y="101.6"/>
</segment>
<segment>
<pinref part="C32" gate="G$1" pin="1"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="266.7" y1="129.54" x2="269.24" y2="129.54" width="0.1524" layer="91"/>
<wire x1="269.24" y1="129.54" x2="269.24" y2="124.46" width="0.1524" layer="91"/>
<pinref part="C34" gate="G$1" pin="2"/>
<wire x1="269.24" y1="124.46" x2="269.24" y2="121.92" width="0.1524" layer="91"/>
<wire x1="266.7" y1="124.46" x2="269.24" y2="124.46" width="0.1524" layer="91"/>
<junction x="269.24" y="124.46"/>
</segment>
<segment>
<pinref part="D10" gate="G$1" pin="-"/>
<wire x1="243.84" y1="68.58" x2="243.84" y2="63.5" width="0.1524" layer="91"/>
<wire x1="243.84" y1="63.5" x2="236.22" y2="63.5" width="0.1524" layer="91"/>
<pinref part="D9" gate="G$1" pin="-"/>
<wire x1="236.22" y1="68.58" x2="236.22" y2="63.5" width="0.1524" layer="91"/>
<junction x="236.22" y="63.5"/>
<wire x1="236.22" y1="63.5" x2="233.68" y2="63.5" width="0.1524" layer="91"/>
<wire x1="233.68" y1="63.5" x2="233.68" y2="60.96" width="0.1524" layer="91"/>
<pinref part="GND31" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND40" gate="1" pin="GND"/>
<wire x1="360.68" y1="127" x2="360.68" y2="119.38" width="0.1524" layer="91"/>
<wire x1="360.68" y1="119.38" x2="360.68" y2="111.76" width="0.1524" layer="91"/>
<wire x1="360.68" y1="111.76" x2="360.68" y2="99.06" width="0.1524" layer="91"/>
<pinref part="D4" gate="G$1" pin="-"/>
<wire x1="360.68" y1="99.06" x2="360.68" y2="88.9" width="0.1524" layer="91"/>
<wire x1="342.9" y1="127" x2="360.68" y2="127" width="0.1524" layer="91"/>
<pinref part="D6" gate="G$1" pin="-"/>
<wire x1="342.9" y1="111.76" x2="360.68" y2="111.76" width="0.1524" layer="91"/>
<pinref part="D5" gate="G$1" pin="-"/>
<wire x1="342.9" y1="119.38" x2="360.68" y2="119.38" width="0.1524" layer="91"/>
<junction x="360.68" y="119.38"/>
<junction x="360.68" y="111.76"/>
<pinref part="D2" gate="G$1" pin="-"/>
<wire x1="342.9" y1="99.06" x2="360.68" y2="99.06" width="0.1524" layer="91"/>
<junction x="360.68" y="99.06"/>
</segment>
<segment>
<pinref part="R39" gate="G$1" pin="1"/>
<pinref part="GND46" gate="1" pin="GND"/>
<wire x1="238.76" y1="205.74" x2="238.76" y2="203.2" width="0.1524" layer="91"/>
<pinref part="R44" gate="G$1" pin="1"/>
<wire x1="238.76" y1="203.2" x2="238.76" y2="200.66" width="0.1524" layer="91"/>
<wire x1="243.84" y1="205.74" x2="243.84" y2="203.2" width="0.1524" layer="91"/>
<wire x1="243.84" y1="203.2" x2="241.3" y2="203.2" width="0.1524" layer="91"/>
<pinref part="R42" gate="G$1" pin="1"/>
<wire x1="241.3" y1="203.2" x2="238.76" y2="203.2" width="0.1524" layer="91"/>
<wire x1="241.3" y1="205.74" x2="241.3" y2="203.2" width="0.1524" layer="91"/>
<junction x="238.76" y="203.2"/>
<junction x="241.3" y="203.2"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="1"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="63.5" y1="144.78" x2="63.5" y2="142.24" width="0.1524" layer="91"/>
<wire x1="63.5" y1="142.24" x2="58.42" y2="142.24" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="58.42" y1="144.78" x2="58.42" y2="142.24" width="0.1524" layer="91"/>
<junction x="58.42" y="142.24"/>
<wire x1="58.42" y1="142.24" x2="53.34" y2="142.24" width="0.1524" layer="91"/>
<wire x1="53.34" y1="142.24" x2="53.34" y2="144.78" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="12"/>
<wire x1="63.5" y1="129.54" x2="96.52" y2="129.54" width="0.1524" layer="91"/>
<wire x1="63.5" y1="142.24" x2="63.5" y2="129.54" width="0.1524" layer="91"/>
<junction x="63.5" y="142.24"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="63.5" y1="129.54" x2="30.48" y2="129.54" width="0.1524" layer="91"/>
<wire x1="30.48" y1="129.54" x2="30.48" y2="127" width="0.1524" layer="91"/>
<junction x="63.5" y="129.54"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="C"/>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="327.66" y1="198.12" x2="327.66" y2="193.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND_6" gate="G$1" pin="PP"/>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="81.28" y1="50.8" x2="88.9" y2="50.8" width="0.1524" layer="91"/>
<wire x1="88.9" y1="50.8" x2="88.9" y2="48.26" width="0.1524" layer="91"/>
<pinref part="GND_4" gate="G$1" pin="PP"/>
<wire x1="88.9" y1="48.26" x2="88.9" y2="45.72" width="0.1524" layer="91"/>
<wire x1="88.9" y1="45.72" x2="88.9" y2="43.18" width="0.1524" layer="91"/>
<wire x1="88.9" y1="43.18" x2="88.9" y2="40.64" width="0.1524" layer="91"/>
<wire x1="88.9" y1="40.64" x2="88.9" y2="38.1" width="0.1524" layer="91"/>
<wire x1="88.9" y1="38.1" x2="88.9" y2="35.56" width="0.1524" layer="91"/>
<wire x1="88.9" y1="35.56" x2="88.9" y2="33.02" width="0.1524" layer="91"/>
<wire x1="88.9" y1="33.02" x2="88.9" y2="30.48" width="0.1524" layer="91"/>
<wire x1="81.28" y1="38.1" x2="88.9" y2="38.1" width="0.1524" layer="91"/>
<pinref part="GND_2" gate="G$1" pin="PP"/>
<wire x1="81.28" y1="40.64" x2="88.9" y2="40.64" width="0.1524" layer="91"/>
<pinref part="GND_5" gate="G$1" pin="PP"/>
<wire x1="81.28" y1="43.18" x2="88.9" y2="43.18" width="0.1524" layer="91"/>
<pinref part="GND_3" gate="G$1" pin="PP"/>
<wire x1="81.28" y1="45.72" x2="88.9" y2="45.72" width="0.1524" layer="91"/>
<pinref part="GND_7" gate="G$1" pin="PP"/>
<wire x1="81.28" y1="48.26" x2="88.9" y2="48.26" width="0.1524" layer="91"/>
<junction x="88.9" y="48.26"/>
<junction x="88.9" y="45.72"/>
<junction x="88.9" y="43.18"/>
<junction x="88.9" y="40.64"/>
<junction x="88.9" y="38.1"/>
<pinref part="GND_1" gate="G$1" pin="PP"/>
<wire x1="81.28" y1="35.56" x2="88.9" y2="35.56" width="0.1524" layer="91"/>
<junction x="88.9" y="35.56"/>
<pinref part="GND_8" gate="G$1" pin="PP"/>
<wire x1="81.28" y1="33.02" x2="88.9" y2="33.02" width="0.1524" layer="91"/>
<junction x="88.9" y="33.02"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="294.64" y1="60.96" x2="294.64" y2="58.42" width="0.1524" layer="91"/>
<wire x1="294.64" y1="58.42" x2="294.64" y2="55.88" width="0.1524" layer="91"/>
<wire x1="294.64" y1="58.42" x2="299.72" y2="58.42" width="0.1524" layer="91"/>
<pinref part="C35" gate="G$1" pin="1"/>
<wire x1="299.72" y1="58.42" x2="304.8" y2="58.42" width="0.1524" layer="91"/>
<wire x1="304.8" y1="58.42" x2="309.88" y2="58.42" width="0.1524" layer="91"/>
<wire x1="309.88" y1="58.42" x2="314.96" y2="58.42" width="0.1524" layer="91"/>
<wire x1="314.96" y1="58.42" x2="320.04" y2="58.42" width="0.1524" layer="91"/>
<wire x1="320.04" y1="58.42" x2="325.12" y2="58.42" width="0.1524" layer="91"/>
<wire x1="325.12" y1="58.42" x2="330.2" y2="58.42" width="0.1524" layer="91"/>
<wire x1="330.2" y1="58.42" x2="330.2" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="299.72" y1="55.88" x2="299.72" y2="58.42" width="0.1524" layer="91"/>
<pinref part="C33" gate="G$1" pin="1"/>
<wire x1="304.8" y1="55.88" x2="304.8" y2="58.42" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="1"/>
<wire x1="309.88" y1="55.88" x2="309.88" y2="58.42" width="0.1524" layer="91"/>
<pinref part="C30" gate="G$1" pin="1"/>
<wire x1="320.04" y1="55.88" x2="320.04" y2="58.42" width="0.1524" layer="91"/>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="325.12" y1="55.88" x2="325.12" y2="58.42" width="0.1524" layer="91"/>
<junction x="294.64" y="58.42"/>
<junction x="299.72" y="58.42"/>
<junction x="304.8" y="58.42"/>
<junction x="309.88" y="58.42"/>
<junction x="320.04" y="58.42"/>
<junction x="325.12" y="58.42"/>
<wire x1="330.2" y1="58.42" x2="332.74" y2="58.42" width="0.1524" layer="91"/>
<label x="332.74" y="58.42" size="1.27" layer="95" xref="yes"/>
<junction x="330.2" y="58.42"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="314.96" y1="55.88" x2="314.96" y2="58.42" width="0.1524" layer="91"/>
<junction x="314.96" y="58.42"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="38"/>
<wire x1="162.56" y1="60.96" x2="162.56" y2="45.72" width="0.1524" layer="91"/>
<label x="162.56" y="45.72" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="62"/>
<wire x1="228.6" y1="124.46" x2="254" y2="124.46" width="0.1524" layer="91"/>
<wire x1="254" y1="124.46" x2="254" y2="129.54" width="0.1524" layer="91"/>
<pinref part="C32" gate="G$1" pin="2"/>
<wire x1="254" y1="129.54" x2="254" y2="132.08" width="0.1524" layer="91"/>
<wire x1="256.54" y1="129.54" x2="254" y2="129.54" width="0.1524" layer="91"/>
<junction x="254" y="129.54"/>
<pinref part="C34" gate="G$1" pin="1"/>
<wire x1="256.54" y1="124.46" x2="254" y2="124.46" width="0.1524" layer="91"/>
<junction x="254" y="124.46"/>
<pinref part="+3V7" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="64"/>
<wire x1="228.6" y1="129.54" x2="241.3" y2="129.54" width="0.1524" layer="91"/>
<label x="241.3" y="129.54" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="16"/>
<wire x1="96.52" y1="119.38" x2="83.82" y2="119.38" width="0.1524" layer="91"/>
<label x="83.82" y="119.38" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="87"/>
<wire x1="165.1" y1="193.04" x2="165.1" y2="205.74" width="0.1524" layer="91"/>
<label x="165.1" y="205.74" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="53.34" y1="160.02" x2="63.5" y2="160.02" width="0.1524" layer="91"/>
<wire x1="63.5" y1="160.02" x2="63.5" y2="154.94" width="0.1524" layer="91"/>
<wire x1="53.34" y1="160.02" x2="53.34" y2="162.56" width="0.1524" layer="91"/>
<pinref part="+3V4" gate="G$1" pin="+3V3"/>
<junction x="53.34" y="160.02"/>
<wire x1="53.34" y1="157.48" x2="53.34" y2="160.02" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="53.34" y1="157.48" x2="58.42" y2="157.48" width="0.1524" layer="91"/>
<wire x1="58.42" y1="157.48" x2="58.42" y2="154.94" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="53.34" y1="154.94" x2="53.34" y2="157.48" width="0.1524" layer="91"/>
<wire x1="58.42" y1="157.48" x2="68.58" y2="157.48" width="0.1524" layer="91"/>
<wire x1="68.58" y1="157.48" x2="68.58" y2="132.08" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="11"/>
<wire x1="68.58" y1="132.08" x2="96.52" y2="132.08" width="0.1524" layer="91"/>
<junction x="58.42" y="157.48"/>
<junction x="53.34" y="157.48"/>
</segment>
<segment>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="317.5" y1="99.06" x2="302.26" y2="99.06" width="0.1524" layer="91"/>
<pinref part="+3V15" gate="G$1" pin="+3V3"/>
<wire x1="302.26" y1="99.06" x2="302.26" y2="104.14" width="0.1524" layer="91"/>
<pinref part="+3V3_TP" gate="G$1" pin="PP"/>
<wire x1="297.18" y1="99.06" x2="302.26" y2="99.06" width="0.1524" layer="91"/>
<junction x="302.26" y="99.06"/>
</segment>
<segment>
<pinref part="R40" gate="G$1" pin="2"/>
<wire x1="238.76" y1="236.22" x2="238.76" y2="238.76" width="0.1524" layer="91"/>
<pinref part="R43" gate="G$1" pin="2"/>
<wire x1="238.76" y1="238.76" x2="238.76" y2="241.3" width="0.1524" layer="91"/>
<wire x1="243.84" y1="236.22" x2="243.84" y2="238.76" width="0.1524" layer="91"/>
<wire x1="243.84" y1="238.76" x2="241.3" y2="238.76" width="0.1524" layer="91"/>
<pinref part="R41" gate="G$1" pin="2"/>
<wire x1="241.3" y1="238.76" x2="238.76" y2="238.76" width="0.1524" layer="91"/>
<wire x1="241.3" y1="236.22" x2="241.3" y2="238.76" width="0.1524" layer="91"/>
<junction x="238.76" y="238.76"/>
<junction x="241.3" y="238.76"/>
<pinref part="+3V18" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<wire x1="309.88" y1="231.14" x2="314.96" y2="231.14" width="0.1524" layer="91"/>
<pinref part="R33" gate="G$1" pin="2"/>
<wire x1="314.96" y1="231.14" x2="327.66" y2="231.14" width="0.1524" layer="91"/>
<wire x1="327.66" y1="231.14" x2="340.36" y2="231.14" width="0.1524" layer="91"/>
<wire x1="314.96" y1="226.06" x2="314.96" y2="231.14" width="0.1524" layer="91"/>
<junction x="314.96" y="231.14"/>
<pinref part="R36" gate="G$1" pin="2"/>
<wire x1="340.36" y1="226.06" x2="340.36" y2="231.14" width="0.1524" layer="91"/>
<junction x="340.36" y="231.14"/>
<pinref part="+3V8" gate="G$1" pin="+3V3"/>
<wire x1="327.66" y1="233.68" x2="327.66" y2="231.14" width="0.1524" layer="91"/>
<junction x="327.66" y="231.14"/>
<pinref part="R35" gate="G$1" pin="2"/>
<wire x1="309.88" y1="226.06" x2="309.88" y2="231.14" width="0.1524" layer="91"/>
<pinref part="R34" gate="G$1" pin="2"/>
<wire x1="345.44" y1="226.06" x2="345.44" y2="231.14" width="0.1524" layer="91"/>
<wire x1="345.44" y1="231.14" x2="340.36" y2="231.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DBG_TX" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="80"/>
<wire x1="182.88" y1="193.04" x2="182.88" y2="215.9" width="0.1524" layer="91"/>
<label x="182.88" y="195.58" size="1.27" layer="95" rot="R90"/>
</segment>
</net>
<net name="DBG_RX" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="81"/>
<wire x1="180.34" y1="193.04" x2="180.34" y2="215.9" width="0.1524" layer="91"/>
<label x="180.34" y="195.58" size="1.27" layer="95" rot="R90"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R31" gate="G$1" pin="2"/>
<wire x1="175.26" y1="218.44" x2="175.26" y2="223.52" width="0.1524" layer="91"/>
<pinref part="D7" gate="G$1" pin="+"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="R32" gate="G$1" pin="2"/>
<wire x1="177.8" y1="218.44" x2="177.8" y2="220.98" width="0.1524" layer="91"/>
<wire x1="177.8" y1="220.98" x2="182.88" y2="220.98" width="0.1524" layer="91"/>
<pinref part="D8" gate="G$1" pin="+"/>
<wire x1="182.88" y1="220.98" x2="182.88" y2="223.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DBG_TX_LED" class="0">
<segment>
<pinref part="R31" gate="G$1" pin="1"/>
<pinref part="U5" gate="G$1" pin="83"/>
<wire x1="175.26" y1="208.28" x2="175.26" y2="193.04" width="0.1524" layer="91"/>
<label x="175.26" y="195.58" size="1.27" layer="95" ratio="15" rot="R90"/>
</segment>
</net>
<net name="DBG_RX_LED" class="0">
<segment>
<pinref part="R32" gate="G$1" pin="1"/>
<pinref part="U5" gate="G$1" pin="82"/>
<wire x1="177.8" y1="208.28" x2="177.8" y2="193.04" width="0.1524" layer="91"/>
<label x="177.8" y="195.58" size="1.27" layer="95" ratio="15" rot="R90"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="40.64" y1="114.3" x2="40.64" y2="116.84" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="1"/>
<wire x1="40.64" y1="116.84" x2="43.18" y2="116.84" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="13"/>
<wire x1="96.52" y1="127" x2="40.64" y2="127" width="0.1524" layer="91"/>
<wire x1="40.64" y1="127" x2="40.64" y2="116.84" width="0.1524" layer="91"/>
<junction x="40.64" y="116.84"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="2"/>
<wire x1="53.34" y1="116.84" x2="55.88" y2="116.84" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="55.88" y1="116.84" x2="55.88" y2="114.3" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="14"/>
<wire x1="96.52" y1="124.46" x2="55.88" y2="124.46" width="0.1524" layer="91"/>
<wire x1="55.88" y1="124.46" x2="55.88" y2="116.84" width="0.1524" layer="91"/>
<junction x="55.88" y="116.84"/>
</segment>
</net>
<net name="RS485_INT" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="74"/>
<wire x1="228.6" y1="154.94" x2="246.38" y2="154.94" width="0.1524" layer="91"/>
<label x="246.38" y="154.94" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="EXT_TX_LED" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="52"/>
<wire x1="228.6" y1="99.06" x2="243.84" y2="99.06" width="0.1524" layer="91"/>
<pinref part="R38" gate="G$1" pin="1"/>
<wire x1="243.84" y1="99.06" x2="243.84" y2="91.44" width="0.1524" layer="91"/>
<label x="231.14" y="99.06" size="1.27" layer="95" ratio="15"/>
</segment>
</net>
<net name="EXT_RX_LED" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="51"/>
<wire x1="228.6" y1="96.52" x2="236.22" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R37" gate="G$1" pin="1"/>
<wire x1="236.22" y1="96.52" x2="236.22" y2="91.44" width="0.1524" layer="91"/>
<label x="231.14" y="96.52" size="1.27" layer="95" ratio="15"/>
</segment>
</net>
<net name="N$101" class="0">
<segment>
<pinref part="R37" gate="G$1" pin="2"/>
<pinref part="D9" gate="G$1" pin="+"/>
<wire x1="236.22" y1="81.28" x2="236.22" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$102" class="0">
<segment>
<pinref part="R38" gate="G$1" pin="2"/>
<pinref part="D10" gate="G$1" pin="+"/>
<wire x1="243.84" y1="81.28" x2="243.84" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="STATUS_LED1" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="86"/>
<wire x1="167.64" y1="193.04" x2="167.64" y2="218.44" width="0.1524" layer="91"/>
<wire x1="167.64" y1="218.44" x2="162.56" y2="218.44" width="0.1524" layer="91"/>
<wire x1="162.56" y1="218.44" x2="162.56" y2="231.14" width="0.1524" layer="91"/>
<label x="162.56" y="231.14" size="1.27" layer="95" ratio="15" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="R28" gate="G$1" pin="2"/>
<wire x1="317.5" y1="127" x2="302.26" y2="127" width="0.1524" layer="91"/>
<label x="302.26" y="127" size="1.27" layer="95" ratio="15" rot="R180" xref="yes"/>
</segment>
</net>
<net name="STATUS_LED2" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="85"/>
<wire x1="170.18" y1="193.04" x2="170.18" y2="220.98" width="0.1524" layer="91"/>
<wire x1="170.18" y1="220.98" x2="165.1" y2="220.98" width="0.1524" layer="91"/>
<wire x1="165.1" y1="220.98" x2="165.1" y2="231.14" width="0.1524" layer="91"/>
<label x="165.1" y="231.14" size="1.27" layer="95" ratio="15" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="R29" gate="G$1" pin="2"/>
<wire x1="317.5" y1="119.38" x2="302.26" y2="119.38" width="0.1524" layer="91"/>
<label x="302.26" y="119.38" size="1.27" layer="95" ratio="15" rot="R180" xref="yes"/>
</segment>
</net>
<net name="STATUS_LED3" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="84"/>
<wire x1="172.72" y1="193.04" x2="172.72" y2="223.52" width="0.1524" layer="91"/>
<wire x1="172.72" y1="223.52" x2="167.64" y2="223.52" width="0.1524" layer="91"/>
<wire x1="167.64" y1="223.52" x2="167.64" y2="231.14" width="0.1524" layer="91"/>
<label x="167.64" y="231.14" size="1.27" layer="95" ratio="15" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="R30" gate="G$1" pin="2"/>
<wire x1="317.5" y1="111.76" x2="302.26" y2="111.76" width="0.1524" layer="91"/>
<label x="302.26" y="111.76" size="1.27" layer="95" ratio="15" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="R28" gate="G$1" pin="1"/>
<pinref part="D4" gate="G$1" pin="+"/>
<wire x1="327.66" y1="127" x2="332.74" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="R29" gate="G$1" pin="1"/>
<pinref part="D5" gate="G$1" pin="+"/>
<wire x1="327.66" y1="119.38" x2="332.74" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<pinref part="R30" gate="G$1" pin="1"/>
<pinref part="D6" gate="G$1" pin="+"/>
<wire x1="327.66" y1="111.76" x2="332.74" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="1"/>
<pinref part="D2" gate="G$1" pin="+"/>
<wire x1="327.66" y1="99.06" x2="332.74" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MODE0" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="67"/>
<wire x1="228.6" y1="137.16" x2="236.22" y2="137.16" width="0.1524" layer="91"/>
<label x="236.22" y="137.16" size="1.27" layer="95" ratio="15" xref="yes"/>
</segment>
<segment>
<pinref part="R44" gate="G$1" pin="2"/>
<pinref part="R43" gate="G$1" pin="1"/>
<wire x1="243.84" y1="215.9" x2="243.84" y2="223.52" width="0.1524" layer="91"/>
<wire x1="243.84" y1="223.52" x2="243.84" y2="226.06" width="0.1524" layer="91"/>
<wire x1="243.84" y1="223.52" x2="251.46" y2="223.52" width="0.1524" layer="91"/>
<junction x="243.84" y="223.52"/>
<label x="251.46" y="223.52" size="1.27" layer="95" ratio="15" xref="yes"/>
</segment>
</net>
<net name="MODE1" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="66"/>
<wire x1="228.6" y1="134.62" x2="236.22" y2="134.62" width="0.1524" layer="91"/>
<label x="236.22" y="134.62" size="1.27" layer="95" ratio="15" xref="yes"/>
</segment>
<segment>
<pinref part="R42" gate="G$1" pin="2"/>
<pinref part="R41" gate="G$1" pin="1"/>
<wire x1="241.3" y1="215.9" x2="241.3" y2="220.98" width="0.1524" layer="91"/>
<wire x1="241.3" y1="220.98" x2="241.3" y2="226.06" width="0.1524" layer="91"/>
<wire x1="241.3" y1="220.98" x2="251.46" y2="220.98" width="0.1524" layer="91"/>
<junction x="241.3" y="220.98"/>
<label x="251.46" y="220.98" size="1.27" layer="95" ratio="15" xref="yes"/>
</segment>
</net>
<net name="MODE2" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="65"/>
<wire x1="228.6" y1="132.08" x2="236.22" y2="132.08" width="0.1524" layer="91"/>
<label x="236.22" y="132.08" size="1.27" layer="95" ratio="15" xref="yes"/>
</segment>
<segment>
<pinref part="R39" gate="G$1" pin="2"/>
<pinref part="R40" gate="G$1" pin="1"/>
<wire x1="238.76" y1="215.9" x2="238.76" y2="218.44" width="0.1524" layer="91"/>
<wire x1="238.76" y1="218.44" x2="238.76" y2="226.06" width="0.1524" layer="91"/>
<wire x1="238.76" y1="218.44" x2="251.46" y2="218.44" width="0.1524" layer="91"/>
<junction x="238.76" y="218.44"/>
<label x="251.46" y="218.44" size="1.27" layer="95" ratio="15" xref="yes"/>
</segment>
</net>
<net name="ADD_1" class="0">
<segment>
<pinref part="J9" gate="G$1" pin="1"/>
<pinref part="R33" gate="G$1" pin="1"/>
<wire x1="320.04" y1="208.28" x2="314.96" y2="208.28" width="0.1524" layer="91"/>
<wire x1="314.96" y1="208.28" x2="314.96" y2="215.9" width="0.1524" layer="91"/>
<wire x1="314.96" y1="208.28" x2="302.26" y2="208.28" width="0.1524" layer="91"/>
<junction x="314.96" y="208.28"/>
<label x="302.26" y="208.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="76"/>
<wire x1="193.04" y1="193.04" x2="193.04" y2="198.12" width="0.1524" layer="91"/>
<wire x1="193.04" y1="198.12" x2="205.74" y2="198.12" width="0.1524" layer="91"/>
<label x="205.74" y="198.12" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="ADD_4" class="0">
<segment>
<pinref part="J9" gate="G$1" pin="4"/>
<pinref part="R35" gate="G$1" pin="1"/>
<wire x1="320.04" y1="203.2" x2="309.88" y2="203.2" width="0.1524" layer="91"/>
<wire x1="309.88" y1="203.2" x2="309.88" y2="215.9" width="0.1524" layer="91"/>
<wire x1="309.88" y1="203.2" x2="302.26" y2="203.2" width="0.1524" layer="91"/>
<junction x="309.88" y="203.2"/>
<label x="302.26" y="203.2" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="78"/>
<wire x1="187.96" y1="193.04" x2="187.96" y2="203.2" width="0.1524" layer="91"/>
<wire x1="187.96" y1="203.2" x2="205.74" y2="203.2" width="0.1524" layer="91"/>
<label x="205.74" y="203.2" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="ADD_8" class="0">
<segment>
<pinref part="J9" gate="G$1" pin="8"/>
<pinref part="R36" gate="G$1" pin="1"/>
<wire x1="335.28" y1="208.28" x2="340.36" y2="208.28" width="0.1524" layer="91"/>
<wire x1="340.36" y1="208.28" x2="340.36" y2="215.9" width="0.1524" layer="91"/>
<wire x1="340.36" y1="208.28" x2="350.52" y2="208.28" width="0.1524" layer="91"/>
<junction x="340.36" y="208.28"/>
<label x="350.52" y="208.28" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="79"/>
<wire x1="185.42" y1="193.04" x2="185.42" y2="205.74" width="0.1524" layer="91"/>
<wire x1="185.42" y1="205.74" x2="205.74" y2="205.74" width="0.1524" layer="91"/>
<label x="205.74" y="205.74" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="ADD_2" class="0">
<segment>
<pinref part="J9" gate="G$1" pin="2"/>
<pinref part="R34" gate="G$1" pin="1"/>
<wire x1="335.28" y1="203.2" x2="345.44" y2="203.2" width="0.1524" layer="91"/>
<wire x1="345.44" y1="203.2" x2="345.44" y2="215.9" width="0.1524" layer="91"/>
<wire x1="345.44" y1="203.2" x2="350.52" y2="203.2" width="0.1524" layer="91"/>
<junction x="345.44" y="203.2"/>
<label x="350.52" y="203.2" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="77"/>
<wire x1="190.5" y1="193.04" x2="190.5" y2="200.66" width="0.1524" layer="91"/>
<wire x1="190.5" y1="200.66" x2="205.74" y2="200.66" width="0.1524" layer="91"/>
<label x="205.74" y="200.66" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="26"/>
<pinref part="TP1" gate="G$1" pin="PP"/>
<wire x1="132.08" y1="60.96" x2="132.08" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="27"/>
<pinref part="TP4" gate="G$1" pin="PP"/>
<wire x1="134.62" y1="60.96" x2="134.62" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="28"/>
<pinref part="TP3" gate="G$1" pin="PP"/>
<wire x1="137.16" y1="60.96" x2="137.16" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="TP2" gate="G$1" pin="PP"/>
<pinref part="U5" gate="G$1" pin="25"/>
<wire x1="91.44" y1="96.52" x2="96.52" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="TP_3" gate="G$1" pin="PP"/>
<pinref part="U5" gate="G$1" pin="17"/>
<wire x1="88.9" y1="116.84" x2="96.52" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="TP_6" gate="G$1" pin="PP"/>
<pinref part="U5" gate="G$1" pin="18"/>
<wire x1="88.9" y1="114.3" x2="96.52" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="TP_5" gate="G$1" pin="PP"/>
<pinref part="U5" gate="G$1" pin="1"/>
<wire x1="88.9" y1="157.48" x2="96.52" y2="157.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="TP_2" gate="G$1" pin="PP"/>
<pinref part="U5" gate="G$1" pin="2"/>
<wire x1="88.9" y1="154.94" x2="96.52" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="TP_4" gate="G$1" pin="PP"/>
<pinref part="U5" gate="G$1" pin="3"/>
<wire x1="88.9" y1="152.4" x2="96.52" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="TP_1" gate="G$1" pin="PP"/>
<pinref part="U5" gate="G$1" pin="4"/>
<wire x1="88.9" y1="149.86" x2="96.52" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="35"/>
<pinref part="TP7" gate="G$1" pin="PP"/>
<wire x1="154.94" y1="60.96" x2="154.94" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="34"/>
<pinref part="TP8" gate="G$1" pin="PP"/>
<wire x1="152.4" y1="60.96" x2="152.4" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TP6" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="33"/>
<pinref part="TP5" gate="G$1" pin="PP"/>
<wire x1="149.86" y1="60.96" x2="149.86" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="32"/>
<pinref part="TP6" gate="G$1" pin="PP"/>
<wire x1="147.32" y1="60.96" x2="147.32" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="COM_TXD" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="72"/>
<wire x1="246.38" y1="149.86" x2="241.3" y2="149.86" width="0.1524" layer="91"/>
<label x="246.38" y="149.86" size="1.27" layer="95" xref="yes"/>
<pinref part="U2_TXD" gate="G$1" pin="PP"/>
<wire x1="241.3" y1="149.86" x2="228.6" y2="149.86" width="0.1524" layer="91"/>
<wire x1="256.54" y1="160.02" x2="241.3" y2="160.02" width="0.1524" layer="91"/>
<wire x1="241.3" y1="160.02" x2="241.3" y2="149.86" width="0.1524" layer="91"/>
<junction x="241.3" y="149.86"/>
</segment>
</net>
<net name="COM_RXD" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="73"/>
<wire x1="246.38" y1="152.4" x2="238.76" y2="152.4" width="0.1524" layer="91"/>
<label x="246.38" y="152.4" size="1.27" layer="95" xref="yes"/>
<pinref part="U2_RXD" gate="G$1" pin="PP"/>
<wire x1="238.76" y1="152.4" x2="228.6" y2="152.4" width="0.1524" layer="91"/>
<wire x1="256.54" y1="162.56" x2="238.76" y2="162.56" width="0.1524" layer="91"/>
<wire x1="238.76" y1="162.56" x2="238.76" y2="152.4" width="0.1524" layer="91"/>
<junction x="238.76" y="152.4"/>
</segment>
</net>
<net name="EXT_TXD" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="39"/>
<pinref part="COM_TX" gate="G$1" pin="PP"/>
<wire x1="165.1" y1="60.96" x2="165.1" y2="48.26" width="0.1524" layer="91"/>
<label x="165.1" y="50.8" size="1.27" layer="95" rot="R90"/>
</segment>
</net>
<net name="EXT_RXD" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="40"/>
<pinref part="COM_RX" gate="G$1" pin="PP"/>
<wire x1="167.64" y1="60.96" x2="167.64" y2="48.26" width="0.1524" layer="91"/>
<label x="167.64" y="50.8" size="1.27" layer="95" rot="R90"/>
</segment>
</net>
<net name="U1_RXD" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="54"/>
<wire x1="228.6" y1="104.14" x2="243.84" y2="104.14" width="0.1524" layer="91"/>
<label x="243.84" y="104.14" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="U1_TXD" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="53"/>
<wire x1="228.6" y1="101.6" x2="243.84" y2="101.6" width="0.1524" layer="91"/>
<label x="243.84" y="101.6" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="LED_1" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="97"/>
<wire x1="139.7" y1="193.04" x2="139.7" y2="205.74" width="0.1524" layer="91"/>
<label x="139.7" y="205.74" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="LED_2" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="98"/>
<wire x1="137.16" y1="193.04" x2="137.16" y2="205.74" width="0.1524" layer="91"/>
<label x="137.16" y="205.74" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="LED_3" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="99"/>
<wire x1="134.62" y1="193.04" x2="134.62" y2="205.74" width="0.1524" layer="91"/>
<label x="134.62" y="205.74" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="LED_4" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="100"/>
<wire x1="132.08" y1="193.04" x2="132.08" y2="205.74" width="0.1524" layer="91"/>
<label x="132.08" y="205.74" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="EXT_!RE" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="50"/>
<wire x1="193.04" y1="60.96" x2="193.04" y2="50.8" width="0.1524" layer="91"/>
<label x="193.04" y="50.8" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="EXT_DE" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="49"/>
<wire x1="190.5" y1="60.96" x2="190.5" y2="50.8" width="0.1524" layer="91"/>
<label x="190.5" y="50.8" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="177.8" y="177.8" size="1.27" layer="97">Programming Pins</text>
<text x="172.212" y="234.188" size="1.27" layer="97">RST SW</text>
<text x="337.82" y="231.14" size="1.27" layer="97">TXD-&gt;RXD</text>
<text x="337.82" y="228.6" size="1.27" layer="97">RXD-&gt;TXD</text>
<text x="337.82" y="213.36" size="1.27" layer="97">TXD-&gt;RXD</text>
<text x="337.82" y="210.82" size="1.27" layer="97">RXD-&gt;TXD</text>
<text x="130.81" y="110.998" size="1.27" layer="97">TX_OUT-&gt;UART_RXD</text>
<text x="130.81" y="98.298" size="1.27" layer="97">RXD_IN &lt;-UART_TXD</text>
<text x="145.288" y="105.41" size="1.27" layer="97">INT0</text>
<wire x1="165.1" y1="248.92" x2="251.46" y2="248.92" width="0.1524" layer="97" style="shortdash"/>
<wire x1="251.46" y1="248.92" x2="254" y2="246.38" width="0.1524" layer="97" style="shortdash"/>
<wire x1="254" y1="246.38" x2="254" y2="170.18" width="0.1524" layer="97" style="shortdash"/>
<wire x1="254" y1="170.18" x2="251.46" y2="167.64" width="0.1524" layer="97" style="shortdash"/>
<wire x1="251.46" y1="167.64" x2="165.1" y2="167.64" width="0.1524" layer="97" style="shortdash"/>
<wire x1="165.1" y1="167.64" x2="162.56" y2="170.18" width="0.1524" layer="97" style="shortdash"/>
<wire x1="162.56" y1="170.18" x2="162.56" y2="246.38" width="0.1524" layer="97" style="shortdash"/>
<wire x1="162.56" y1="246.38" x2="165.1" y2="248.92" width="0.1524" layer="97" style="shortdash"/>
<text x="165.1" y="251.46" size="1.27" layer="97">PROGRAMMINNG HEADER</text>
<wire x1="10.16" y1="246.38" x2="12.7" y2="248.92" width="0.1524" layer="97" style="shortdash"/>
<wire x1="12.7" y1="248.92" x2="152.4" y2="248.92" width="0.1524" layer="97" style="shortdash"/>
<wire x1="152.4" y1="248.92" x2="154.94" y2="246.38" width="0.1524" layer="97" style="shortdash"/>
<wire x1="154.94" y1="246.38" x2="154.94" y2="198.12" width="0.1524" layer="97" style="shortdash"/>
<wire x1="154.94" y1="198.12" x2="152.4" y2="195.58" width="0.1524" layer="97" style="shortdash"/>
<wire x1="152.4" y1="195.58" x2="12.7" y2="195.58" width="0.1524" layer="97" style="shortdash"/>
<wire x1="12.7" y1="195.58" x2="10.16" y2="198.12" width="0.1524" layer="97" style="shortdash"/>
<wire x1="10.16" y1="198.12" x2="10.16" y2="246.38" width="0.1524" layer="97" style="shortdash"/>
<text x="12.7" y="251.46" size="1.27" layer="97" ratio="15">Voltage regulator (9/5V to 3.3V)</text>
<wire x1="12.7" y1="124.46" x2="12.7" y2="76.2" width="0.1524" layer="97" style="shortdash"/>
<wire x1="12.7" y1="76.2" x2="15.24" y2="73.66" width="0.1524" layer="97" style="shortdash"/>
<wire x1="15.24" y1="73.66" x2="152.4" y2="73.66" width="0.1524" layer="97" style="shortdash"/>
<wire x1="152.4" y1="73.66" x2="154.94" y2="76.2" width="0.1524" layer="97" style="shortdash"/>
<wire x1="154.94" y1="76.2" x2="154.94" y2="124.46" width="0.1524" layer="97" style="shortdash"/>
<wire x1="154.94" y1="124.46" x2="152.4" y2="127" width="0.1524" layer="97" style="shortdash"/>
<wire x1="152.4" y1="127" x2="15.24" y2="127" width="0.1524" layer="97" style="shortdash"/>
<wire x1="15.24" y1="127" x2="12.7" y2="124.46" width="0.1524" layer="97" style="shortdash"/>
<text x="15.24" y="129.54" size="1.27" layer="97" ratio="15">RS485 to UART Converter</text>
<text x="12.7" y="231.14" size="1.27" layer="97" ratio="15" rot="R180" align="bottom-right">PHEONIX_CONT_1935161</text>
<wire x1="264.16" y1="246.38" x2="266.7" y2="248.92" width="0.1524" layer="97" style="shortdash"/>
<wire x1="266.7" y1="248.92" x2="373.38" y2="248.92" width="0.1524" layer="97" style="shortdash"/>
<wire x1="373.38" y1="248.92" x2="375.92" y2="246.38" width="0.1524" layer="97" style="shortdash"/>
<wire x1="375.92" y1="246.38" x2="375.92" y2="35.56" width="0.1524" layer="97" style="shortdash"/>
<wire x1="375.92" y1="35.56" x2="373.38" y2="33.02" width="0.1524" layer="97" style="shortdash"/>
<wire x1="373.38" y1="33.02" x2="266.7" y2="33.02" width="0.1524" layer="97" style="shortdash"/>
<wire x1="266.7" y1="33.02" x2="264.16" y2="35.56" width="0.1524" layer="97" style="shortdash"/>
<wire x1="264.16" y1="35.56" x2="264.16" y2="246.38" width="0.1524" layer="97" style="shortdash"/>
<text x="266.7" y="251.46" size="1.27" layer="97" ratio="15">HEADER</text>
<text x="15.24" y="53.34" size="1.778" layer="97">Note: 
1803581 - Mating connector to Orian Controller
3pc - 26 Gaug wire (6-12 inches)
1803581 - Mating connector to JMA Controller</text>
<wire x1="12.7" y1="185.42" x2="12.7" y2="137.16" width="0.1524" layer="97" style="shortdash"/>
<wire x1="12.7" y1="137.16" x2="15.24" y2="134.62" width="0.1524" layer="97" style="shortdash"/>
<wire x1="15.24" y1="134.62" x2="152.4" y2="134.62" width="0.1524" layer="97" style="shortdash"/>
<wire x1="152.4" y1="134.62" x2="154.94" y2="137.16" width="0.1524" layer="97" style="shortdash"/>
<wire x1="154.94" y1="137.16" x2="154.94" y2="185.42" width="0.1524" layer="97" style="shortdash"/>
<wire x1="154.94" y1="185.42" x2="152.4" y2="187.96" width="0.1524" layer="97" style="shortdash"/>
<wire x1="152.4" y1="187.96" x2="15.24" y2="187.96" width="0.1524" layer="97" style="shortdash"/>
<wire x1="15.24" y1="187.96" x2="12.7" y2="185.42" width="0.1524" layer="97" style="shortdash"/>
<text x="15.24" y="190.5" size="1.27" layer="97" ratio="15">RS485 to UART Converter</text>
<text x="17.78" y="172.72" size="1.27" layer="97">DELTA_A</text>
<text x="17.78" y="160.02" size="1.27" layer="97">DELTA_B</text>
<text x="17.78" y="147.32" size="1.27" layer="97">DELTA_ISO_GND</text>
<text x="281.94" y="55.88" size="1.27" layer="97">Power In/Rev Power</text>
<text x="287.02" y="99.06" size="1.27" layer="97">Msg Rx</text>
</plain>
<instances>
<instance part="GND1" gate="1" x="198.12" y="195.58" smashed="yes">
<attribute name="VALUE" x="195.58" y="193.04" size="1.27" layer="96"/>
</instance>
<instance part="R20" gate="G$1" x="175.26" y="238.76" smashed="yes" rot="R180">
<attribute name="NAME" x="169.926" y="239.2426" size="1.27" layer="95"/>
<attribute name="VALUE" x="178.054" y="239.268" size="1.27" layer="96"/>
</instance>
<instance part="C22" gate="G$1" x="190.5" y="238.76" smashed="yes" rot="R180">
<attribute name="NAME" x="185.42" y="239.268" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="191.77" y="239.268" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C19" gate="G$1" x="233.68" y="193.04" smashed="yes" rot="R270">
<attribute name="NAME" x="233.172" y="187.96" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="233.172" y="194.31" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C20" gate="G$1" x="228.6" y="193.04" smashed="yes" rot="R270">
<attribute name="NAME" x="228.092" y="187.96" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="228.092" y="194.31" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="GND2" gate="1" x="228.6" y="182.88" smashed="yes">
<attribute name="VALUE" x="226.06" y="180.34" size="1.27" layer="96"/>
</instance>
<instance part="GND3" gate="1" x="233.68" y="182.88" smashed="yes">
<attribute name="VALUE" x="231.14" y="180.34" size="1.27" layer="96"/>
</instance>
<instance part="+3V1" gate="G$1" x="167.64" y="243.84" smashed="yes">
<attribute name="VALUE" x="165.354" y="244.094" size="1.27" layer="96"/>
</instance>
<instance part="GND4" gate="1" x="167.64" y="215.9" smashed="yes">
<attribute name="VALUE" x="165.1" y="213.36" size="1.27" layer="96"/>
</instance>
<instance part="NC1" gate="G$1" x="228.6" y="205.74" smashed="yes"/>
<instance part="NC2" gate="G$1" x="228.6" y="213.36" smashed="yes"/>
<instance part="NC3" gate="G$1" x="228.6" y="210.82" smashed="yes"/>
<instance part="NC4" gate="G$1" x="200.66" y="215.9" smashed="yes"/>
<instance part="NC5" gate="G$1" x="228.6" y="215.9" smashed="yes"/>
<instance part="+3V2" gate="G$1" x="241.3" y="220.98" smashed="yes">
<attribute name="VALUE" x="239.014" y="221.234" size="1.27" layer="96"/>
</instance>
<instance part="S1" gate="G$1" x="177.8" y="226.06" smashed="yes" rot="R90">
<attribute name="NAME" x="174.244" y="219.1766" size="1.27" layer="95"/>
<attribute name="VALUE" x="169.418" y="221.488" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="GND16" gate="1" x="325.12" y="200.66" smashed="yes">
<attribute name="VALUE" x="322.58" y="198.12" size="1.27" layer="96"/>
</instance>
<instance part="R6" gate="G$1" x="116.84" y="109.22" smashed="yes">
<attribute name="NAME" x="110.998" y="109.7026" size="1.27" layer="95"/>
<attribute name="VALUE" x="119.634" y="109.728" size="1.27" layer="96"/>
</instance>
<instance part="R14" gate="G$1" x="116.84" y="101.6" smashed="yes">
<attribute name="NAME" x="110.744" y="102.0826" size="1.27" layer="95"/>
<attribute name="VALUE" x="119.634" y="102.108" size="1.27" layer="96"/>
</instance>
<instance part="C4" gate="G$1" x="116.84" y="114.3" smashed="yes" rot="R180">
<attribute name="NAME" x="111.76" y="114.808" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="118.11" y="114.808" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C8" gate="G$1" x="58.42" y="88.9" smashed="yes" rot="R180">
<attribute name="NAME" x="53.34" y="89.408" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="59.69" y="89.408" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C5" gate="G$1" x="58.42" y="91.44" smashed="yes" rot="R180">
<attribute name="NAME" x="53.34" y="91.948" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="59.69" y="91.948" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND17" gate="1" x="104.14" y="91.44" smashed="yes">
<attribute name="VALUE" x="101.6" y="88.9" size="1.27" layer="96"/>
</instance>
<instance part="+3V6" gate="G$1" x="127" y="124.46" smashed="yes">
<attribute name="VALUE" x="124.968" y="125.222" size="1.27" layer="96"/>
</instance>
<instance part="D3" gate="G$1" x="33.02" y="236.22" smashed="yes">
<attribute name="NAME" x="28.194" y="236.728" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="27.432" y="240.284" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="L2" gate="G$1" x="48.26" y="236.22" smashed="yes">
<attribute name="NAME" x="42.418" y="236.728" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="51.308" y="236.728" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R11" gate="G$1" x="48.26" y="241.3" smashed="yes">
<attribute name="NAME" x="42.164" y="241.7826" size="1.27" layer="95"/>
<attribute name="VALUE" x="50.8" y="241.808" size="1.27" layer="96"/>
</instance>
<instance part="R7" gate="G$1" x="132.08" y="241.3" smashed="yes">
<attribute name="NAME" x="126.492" y="241.7826" size="1.27" layer="95"/>
<attribute name="VALUE" x="134.874" y="241.808" size="1.27" layer="96"/>
</instance>
<instance part="U2" gate="G$1" x="78.74" y="236.22" smashed="yes">
<attribute name="NAME" x="71.12" y="241.808" size="1.27" layer="95"/>
<attribute name="VALUE" x="71.12" y="243.84" size="1.27" layer="96"/>
</instance>
<instance part="C3" gate="G$1" x="58.42" y="228.6" smashed="yes" rot="R270">
<attribute name="NAME" x="57.912" y="223.52" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="57.912" y="229.87" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C2" gate="G$1" x="63.5" y="228.6" smashed="yes" rot="R270">
<attribute name="NAME" x="62.992" y="223.52" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="62.992" y="229.87" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R2" gate="G$1" x="96.52" y="228.6" smashed="yes" rot="R90">
<attribute name="NAME" x="96.0374" y="222.758" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="93.472" y="229.362" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R3" gate="G$1" x="71.12" y="218.44" smashed="yes" rot="R180">
<attribute name="NAME" x="72.39" y="216.9414" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="69.85" y="220.218" size="1.27" layer="96"/>
</instance>
<instance part="R1" gate="G$1" x="104.14" y="220.98" smashed="yes" rot="R180">
<attribute name="NAME" x="98.298" y="221.4626" size="1.27" layer="95"/>
<attribute name="VALUE" x="106.934" y="221.488" size="1.27" layer="96"/>
</instance>
<instance part="D1" gate="G$1" x="106.68" y="228.6" smashed="yes" rot="R180">
<attribute name="NAME" x="102.362" y="229.108" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="102.362" y="231.902" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="U1" gate="G$1" x="104.14" y="210.82" smashed="yes" rot="MR90">
<attribute name="NAME" x="99.06" y="208.788" size="1.27" layer="95" rot="MR0"/>
<attribute name="VALUE" x="96.52" y="207.772" size="1.27" layer="96" rot="MR180"/>
</instance>
<instance part="C7" gate="G$1" x="116.84" y="228.6" smashed="yes" rot="R270">
<attribute name="NAME" x="116.332" y="223.52" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="116.332" y="229.87" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C6" gate="G$1" x="121.92" y="228.6" smashed="yes" rot="R270">
<attribute name="NAME" x="121.412" y="223.52" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="121.412" y="229.87" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C11" gate="G$1" x="142.24" y="228.6" smashed="yes" rot="R270">
<attribute name="NAME" x="141.732" y="223.52" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="141.732" y="229.87" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C9" gate="G$1" x="147.32" y="228.6" smashed="yes" rot="R270">
<attribute name="NAME" x="146.812" y="223.52" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="146.812" y="229.87" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C1" gate="G$1" x="132.08" y="215.9" smashed="yes">
<attribute name="NAME" x="127" y="216.408" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="133.35" y="216.408" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND30" gate="1" x="147.32" y="203.2" smashed="yes">
<attribute name="VALUE" x="144.78" y="200.66" size="1.27" layer="96"/>
</instance>
<instance part="GND32" gate="1" x="58.42" y="213.36" smashed="yes">
<attribute name="VALUE" x="55.88" y="210.82" size="1.27" layer="96"/>
</instance>
<instance part="+3V12" gate="G$1" x="147.32" y="241.3" smashed="yes">
<attribute name="VALUE" x="145.034" y="242.062" size="1.27" layer="96"/>
</instance>
<instance part="U$3" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="344.17" y="15.24" size="1.27" layer="94"/>
<attribute name="LAST_DATE_TIME" x="344.17" y="10.16" size="1.27" layer="94"/>
<attribute name="SHEET" x="357.505" y="5.08" size="1.27" layer="94"/>
</instance>
<instance part="C25" gate="G$1" x="314.96" y="236.22" smashed="yes">
<attribute name="NAME" x="313.69" y="237.49" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="313.69" y="233.68" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C24" gate="G$1" x="314.96" y="220.98" smashed="yes">
<attribute name="NAME" x="313.69" y="222.25" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="313.69" y="218.44" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="+3V13" gate="G$1" x="322.58" y="241.3" smashed="yes">
<attribute name="VALUE" x="322.58" y="241.3" size="1.27" layer="96"/>
</instance>
<instance part="GND36" gate="1" x="304.8" y="220.98" smashed="yes" rot="R270">
<attribute name="VALUE" x="302.26" y="223.52" size="1.27" layer="96" rot="R270"/>
</instance>
<instance part="GND37" gate="1" x="304.8" y="236.22" smashed="yes" rot="R270">
<attribute name="VALUE" x="302.26" y="238.76" size="1.27" layer="96" rot="R270"/>
</instance>
<instance part="R9" gate="G$1" x="116.84" y="106.68" smashed="yes">
<attribute name="NAME" x="110.998" y="107.1626" size="1.27" layer="95"/>
<attribute name="VALUE" x="119.634" y="107.188" size="1.27" layer="96"/>
</instance>
<instance part="R12" gate="G$1" x="116.84" y="104.14" smashed="yes">
<attribute name="NAME" x="110.998" y="104.6226" size="1.27" layer="95"/>
<attribute name="VALUE" x="119.634" y="104.648" size="1.27" layer="96"/>
</instance>
<instance part="GND39" gate="1" x="127" y="91.44" smashed="yes">
<attribute name="VALUE" x="125.222" y="89.662" size="1.27" layer="96"/>
</instance>
<instance part="U3" gate="G$1" x="86.36" y="106.68" rot="MR0"/>
<instance part="R10" gate="G$1" x="43.18" y="106.68" smashed="yes">
<attribute name="VALUE" x="46.228" y="107.188" size="1.27" layer="96"/>
</instance>
<instance part="R8" gate="G$1" x="58.42" y="99.06" smashed="yes">
<attribute name="NAME" x="52.07" y="99.5426" size="1.27" layer="95"/>
<attribute name="VALUE" x="61.468" y="99.568" size="1.27" layer="96"/>
</instance>
<instance part="R13" gate="G$1" x="58.42" y="96.52" smashed="yes">
<attribute name="NAME" x="52.07" y="97.0026" size="1.27" layer="95"/>
<attribute name="VALUE" x="61.468" y="97.028" size="1.27" layer="96"/>
</instance>
<instance part="R27" gate="G$1" x="116.84" y="170.18" smashed="yes">
<attribute name="NAME" x="110.998" y="170.6626" size="1.27" layer="95"/>
<attribute name="VALUE" x="119.634" y="170.688" size="1.27" layer="96"/>
</instance>
<instance part="R22" gate="G$1" x="116.84" y="162.56" smashed="yes">
<attribute name="NAME" x="110.744" y="163.0426" size="1.27" layer="95"/>
<attribute name="VALUE" x="119.634" y="163.068" size="1.27" layer="96"/>
</instance>
<instance part="C23" gate="G$1" x="116.84" y="175.26" smashed="yes" rot="R180">
<attribute name="NAME" x="111.76" y="175.768" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="118.11" y="175.768" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C21" gate="G$1" x="58.42" y="149.86" smashed="yes" rot="R180">
<attribute name="NAME" x="53.34" y="150.368" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="59.69" y="150.368" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C28" gate="G$1" x="58.42" y="152.4" smashed="yes" rot="R180">
<attribute name="NAME" x="53.34" y="152.908" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="59.69" y="152.908" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND18" gate="1" x="104.14" y="152.4" smashed="yes">
<attribute name="VALUE" x="101.6" y="149.86" size="1.27" layer="96"/>
</instance>
<instance part="+3V5" gate="G$1" x="127" y="185.42" smashed="yes">
<attribute name="VALUE" x="124.968" y="186.182" size="1.27" layer="96"/>
</instance>
<instance part="R26" gate="G$1" x="116.84" y="167.64" smashed="yes">
<attribute name="NAME" x="110.998" y="168.1226" size="1.27" layer="95"/>
<attribute name="VALUE" x="119.634" y="168.148" size="1.27" layer="96"/>
</instance>
<instance part="R24" gate="G$1" x="116.84" y="165.1" smashed="yes">
<attribute name="NAME" x="110.998" y="165.5826" size="1.27" layer="95"/>
<attribute name="VALUE" x="119.634" y="165.608" size="1.27" layer="96"/>
</instance>
<instance part="GND19" gate="1" x="127" y="152.4" smashed="yes">
<attribute name="VALUE" x="125.222" y="150.622" size="1.27" layer="96"/>
</instance>
<instance part="J8" gate="-1" x="20.32" y="170.18" rot="R180"/>
<instance part="J8" gate="-2" x="20.32" y="165.1" rot="R180"/>
<instance part="J8" gate="-3" x="20.32" y="144.78" rot="R180"/>
<instance part="U4" gate="G$1" x="86.36" y="167.64" rot="MR0"/>
<instance part="R23" gate="G$1" x="43.18" y="167.64" smashed="yes">
<attribute name="VALUE" x="46.228" y="168.148" size="1.27" layer="96"/>
</instance>
<instance part="R25" gate="G$1" x="58.42" y="160.02" smashed="yes">
<attribute name="NAME" x="52.07" y="160.5026" size="1.27" layer="95"/>
<attribute name="VALUE" x="61.468" y="160.528" size="1.27" layer="96"/>
</instance>
<instance part="R21" gate="G$1" x="58.42" y="154.94" smashed="yes">
<attribute name="NAME" x="52.07" y="155.4226" size="1.27" layer="95"/>
<attribute name="VALUE" x="61.468" y="155.448" size="1.27" layer="96"/>
</instance>
<instance part="GND50" gate="1" x="309.88" y="99.06" smashed="yes">
<attribute name="VALUE" x="307.34" y="96.52" size="1.27" layer="96"/>
</instance>
<instance part="GND51" gate="1" x="309.88" y="134.62" smashed="yes">
<attribute name="VALUE" x="307.34" y="132.08" size="1.27" layer="96"/>
</instance>
<instance part="GND52" gate="1" x="353.06" y="93.98" smashed="yes">
<attribute name="VALUE" x="350.52" y="91.44" size="1.27" layer="96"/>
</instance>
<instance part="R5" gate="G$1" x="345.44" y="99.06" smashed="yes">
<attribute name="NAME" x="339.598" y="99.5426" size="1.27" layer="95"/>
<attribute name="VALUE" x="347.98" y="99.568" size="1.27" layer="96"/>
</instance>
<instance part="J2" gate="G$1" x="340.36" y="165.1" smashed="yes">
<attribute name="NAME" x="349.25" y="167.64" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="332.74" y="170.688" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="J3" gate="G$1" x="340.36" y="152.4" smashed="yes" rot="MR180">
<attribute name="NAME" x="349.25" y="149.86" size="1.27" layer="95" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="332.74" y="146.812" size="1.27" layer="96" ratio="10" rot="MR180"/>
</instance>
<instance part="J4" gate="G$1" x="312.42" y="66.04"/>
<instance part="J4" gate="G$2" x="345.44" y="58.42" rot="R270"/>
<instance part="J4" gate="G$3" x="289.56" y="66.04" rot="R180"/>
<instance part="J1" gate="G$1" x="312.42" y="147.32"/>
<instance part="J1" gate="G$2" x="355.6" y="187.96" smashed="yes">
<attribute name="NAME" x="347.98" y="189.23" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="347.98" y="193.04" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="J1" gate="G$3" x="355.6" y="180.34" smashed="yes">
<attribute name="NAME" x="347.98" y="181.61" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="345.44" y="175.26" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="J5" gate="G$1" x="312.42" y="111.76"/>
<instance part="J5" gate="G$2" x="297.18" y="180.34" smashed="yes">
<attribute name="NAME" x="299.72" y="181.61" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="287.02" y="175.26" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="J5" gate="G$3" x="297.18" y="187.96" smashed="yes">
<attribute name="NAME" x="299.72" y="189.23" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="287.02" y="193.04" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="GND6" gate="1" x="325.12" y="45.72" smashed="yes">
<attribute name="VALUE" x="322.58" y="43.18" size="1.27" layer="96"/>
</instance>
<instance part="GND20" gate="1" x="309.88" y="53.34" smashed="yes">
<attribute name="VALUE" x="307.34" y="50.8" size="1.27" layer="96"/>
</instance>
<instance part="GND21" gate="1" x="345.44" y="45.72" smashed="yes">
<attribute name="VALUE" x="342.9" y="43.18" size="1.27" layer="96"/>
</instance>
<instance part="J7" gate="-1" x="208.28" y="200.66"/>
<instance part="J7" gate="-2" x="220.98" y="200.66" rot="R180"/>
<instance part="J7" gate="-3" x="208.28" y="203.2"/>
<instance part="J7" gate="-4" x="220.98" y="203.2" rot="R180"/>
<instance part="J7" gate="-5" x="208.28" y="205.74"/>
<instance part="J7" gate="-6" x="220.98" y="205.74" rot="R180"/>
<instance part="J7" gate="-7" x="208.28" y="208.28"/>
<instance part="J7" gate="-8" x="220.98" y="208.28" rot="R180"/>
<instance part="J7" gate="-9" x="208.28" y="210.82"/>
<instance part="J7" gate="-10" x="220.98" y="210.82" rot="R180"/>
<instance part="J7" gate="-11" x="208.28" y="213.36"/>
<instance part="J7" gate="-12" x="220.98" y="213.36" rot="R180"/>
<instance part="J7" gate="-13" x="208.28" y="215.9"/>
<instance part="J7" gate="-14" x="220.98" y="215.9" rot="R180"/>
<instance part="J6" gate="-1" x="332.74" y="231.14"/>
<instance part="J6" gate="-2" x="332.74" y="228.6"/>
<instance part="J6" gate="-3" x="332.74" y="233.68"/>
<instance part="J6" gate="-4" x="332.74" y="226.06"/>
<instance part="J6" gate="-5" x="332.74" y="208.28"/>
<instance part="J6" gate="-6" x="332.74" y="215.9"/>
<instance part="J6" gate="-7" x="332.74" y="213.36"/>
<instance part="J6" gate="-8" x="332.74" y="210.82"/>
<instance part="R16" gate="G$1" x="345.44" y="71.12" rot="R90"/>
<instance part="L1" gate="G$1" x="132.08" y="236.22" smashed="yes">
<attribute name="NAME" x="126.238" y="236.728" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="135.128" y="236.728" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C10" gate="G$1" x="116.84" y="119.38" smashed="yes" rot="R180">
<attribute name="NAME" x="111.76" y="119.888" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="118.11" y="119.888" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C31" gate="G$1" x="116.84" y="180.34" smashed="yes" rot="R180">
<attribute name="NAME" x="111.76" y="180.848" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="118.11" y="180.848" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R4" gate="G$1" x="342.9" y="187.96" smashed="yes">
<attribute name="NAME" x="337.312" y="188.4426" size="1.27" layer="95"/>
<attribute name="VALUE" x="345.694" y="188.468" size="1.27" layer="96"/>
</instance>
<instance part="R19" gate="G$1" x="342.9" y="180.34" smashed="yes">
<attribute name="NAME" x="337.312" y="180.8226" size="1.27" layer="95"/>
<attribute name="VALUE" x="345.694" y="180.848" size="1.27" layer="96"/>
</instance>
<instance part="GND13" gate="1" x="365.76" y="175.26" smashed="yes">
<attribute name="VALUE" x="363.22" y="172.72" size="1.27" layer="96"/>
</instance>
<instance part="R18" gate="G$1" x="284.48" y="187.96" smashed="yes">
<attribute name="NAME" x="278.892" y="188.4426" size="1.27" layer="95"/>
<attribute name="VALUE" x="287.274" y="188.468" size="1.27" layer="96"/>
</instance>
<instance part="R17" gate="G$1" x="284.48" y="180.34" smashed="yes">
<attribute name="NAME" x="278.892" y="180.8226" size="1.27" layer="95"/>
<attribute name="VALUE" x="287.274" y="180.848" size="1.27" layer="96"/>
</instance>
<instance part="GND24" gate="1" x="307.34" y="175.26" smashed="yes">
<attribute name="VALUE" x="304.8" y="172.72" size="1.27" layer="96"/>
</instance>
<instance part="R45" gate="G$1" x="78.74" y="144.78" smashed="yes">
<attribute name="NAME" x="72.898" y="145.2626" size="1.27" layer="95"/>
<attribute name="VALUE" x="81.28" y="145.288" size="1.27" layer="96"/>
</instance>
<instance part="GND25" gate="1" x="86.36" y="139.7" smashed="yes">
<attribute name="VALUE" x="83.82" y="137.16" size="1.27" layer="96"/>
</instance>
</instances>
<busses>
<bus name="TDO,TDI,TMS,TCK,!RST,TEST">
<segment>
<wire x1="172.72" y1="210.82" x2="172.72" y2="177.8" width="0.762" layer="92"/>
<wire x1="172.72" y1="177.8" x2="175.26" y2="175.26" width="0.762" layer="92"/>
<wire x1="175.26" y1="175.26" x2="248.92" y2="175.26" width="0.762" layer="92"/>
<wire x1="248.92" y1="175.26" x2="251.46" y2="177.8" width="0.762" layer="92"/>
<wire x1="251.46" y1="177.8" x2="251.46" y2="205.74" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="DBG_TX,DBG_RX">
<segment>
<wire x1="304.8" y1="231.14" x2="304.8" y2="228.6" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="TDO" class="0">
<segment>
<wire x1="175.26" y1="200.66" x2="203.2" y2="200.66" width="0.1524" layer="91"/>
<label x="175.26" y="200.66" size="1.27" layer="95"/>
<wire x1="175.26" y1="200.66" x2="172.72" y2="198.12" width="0.1524" layer="91"/>
<pinref part="J7" gate="-1" pin="1"/>
</segment>
</net>
<net name="TDI" class="0">
<segment>
<wire x1="175.26" y1="203.2" x2="203.2" y2="203.2" width="0.1524" layer="91"/>
<label x="175.26" y="203.2" size="1.27" layer="95"/>
<wire x1="175.26" y1="203.2" x2="172.72" y2="200.66" width="0.1524" layer="91"/>
<pinref part="J7" gate="-3" pin="1"/>
</segment>
</net>
<net name="TMS" class="0">
<segment>
<wire x1="175.26" y1="205.74" x2="203.2" y2="205.74" width="0.1524" layer="91"/>
<label x="175.26" y="205.74" size="1.27" layer="95"/>
<wire x1="175.26" y1="205.74" x2="172.72" y2="203.2" width="0.1524" layer="91"/>
<pinref part="J7" gate="-5" pin="1"/>
</segment>
</net>
<net name="TCK" class="0">
<segment>
<wire x1="175.26" y1="208.28" x2="203.2" y2="208.28" width="0.1524" layer="91"/>
<label x="175.26" y="208.28" size="1.27" layer="95"/>
<wire x1="175.26" y1="208.28" x2="172.72" y2="205.74" width="0.1524" layer="91"/>
<pinref part="J7" gate="-7" pin="1"/>
</segment>
</net>
<net name="!RST" class="0">
<segment>
<wire x1="175.26" y1="213.36" x2="182.88" y2="213.36" width="0.1524" layer="91"/>
<label x="175.26" y="213.36" size="1.27" layer="95"/>
<pinref part="R20" gate="G$1" pin="1"/>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="182.88" y1="213.36" x2="203.2" y2="213.36" width="0.1524" layer="91"/>
<wire x1="180.34" y1="238.76" x2="182.88" y2="238.76" width="0.1524" layer="91"/>
<wire x1="182.88" y1="238.76" x2="185.42" y2="238.76" width="0.1524" layer="91"/>
<wire x1="182.88" y1="238.76" x2="182.88" y2="220.98" width="0.1524" layer="91"/>
<junction x="182.88" y="213.36"/>
<junction x="182.88" y="238.76"/>
<wire x1="182.88" y1="220.98" x2="182.88" y2="213.36" width="0.1524" layer="91"/>
<junction x="182.88" y="220.98"/>
<wire x1="175.26" y1="213.36" x2="172.72" y2="210.82" width="0.1524" layer="91"/>
<wire x1="182.88" y1="220.98" x2="180.34" y2="220.98" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="1"/>
<wire x1="180.34" y1="220.98" x2="177.8" y2="220.98" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="2"/>
<wire x1="177.8" y1="231.14" x2="180.34" y2="231.14" width="0.1524" layer="91"/>
<wire x1="180.34" y1="231.14" x2="180.34" y2="220.98" width="0.1524" layer="91"/>
<junction x="180.34" y="220.98"/>
<pinref part="J7" gate="-11" pin="1"/>
</segment>
</net>
<net name="TEST" class="0">
<segment>
<wire x1="226.06" y1="208.28" x2="248.92" y2="208.28" width="0.1524" layer="91"/>
<wire x1="248.92" y1="208.28" x2="251.46" y2="205.74" width="0.1524" layer="91"/>
<pinref part="J7" gate="-8" pin="1"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="203.2" y1="210.82" x2="198.12" y2="210.82" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="198.12" y1="210.82" x2="198.12" y2="198.12" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="195.58" y1="238.76" x2="198.12" y2="238.76" width="0.1524" layer="91"/>
<wire x1="198.12" y1="238.76" x2="198.12" y2="210.82" width="0.1524" layer="91"/>
<junction x="198.12" y="210.82"/>
<pinref part="J7" gate="-9" pin="1"/>
</segment>
<segment>
<pinref part="C20" gate="G$1" pin="2"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="228.6" y1="187.96" x2="228.6" y2="185.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C19" gate="G$1" pin="2"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="233.68" y1="187.96" x2="233.68" y2="185.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="167.64" y1="218.44" x2="167.64" y2="220.98" width="0.1524" layer="91"/>
<wire x1="167.64" y1="220.98" x2="170.18" y2="220.98" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="3"/>
<wire x1="170.18" y1="220.98" x2="172.72" y2="220.98" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="4"/>
<wire x1="172.72" y1="231.14" x2="170.18" y2="231.14" width="0.1524" layer="91"/>
<wire x1="170.18" y1="231.14" x2="170.18" y2="220.98" width="0.1524" layer="91"/>
<junction x="170.18" y="220.98"/>
</segment>
<segment>
<wire x1="327.66" y1="226.06" x2="325.12" y2="226.06" width="0.1524" layer="91"/>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="325.12" y1="226.06" x2="325.12" y2="208.28" width="0.1524" layer="91"/>
<wire x1="325.12" y1="208.28" x2="325.12" y2="203.2" width="0.1524" layer="91"/>
<pinref part="J6" gate="-4" pin="1"/>
<pinref part="J6" gate="-5" pin="1"/>
<wire x1="327.66" y1="208.28" x2="325.12" y2="208.28" width="0.1524" layer="91"/>
<junction x="325.12" y="208.28"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="111.76" y1="114.3" x2="109.22" y2="114.3" width="0.1524" layer="91"/>
<wire x1="109.22" y1="114.3" x2="104.14" y2="114.3" width="0.1524" layer="91"/>
<wire x1="104.14" y1="114.3" x2="104.14" y2="96.52" width="0.1524" layer="91"/>
<pinref part="GND17" gate="1" pin="GND"/>
<pinref part="U3" gate="G$1" pin="1"/>
<wire x1="104.14" y1="96.52" x2="104.14" y2="93.98" width="0.1524" layer="91"/>
<wire x1="99.06" y1="114.3" x2="104.14" y2="114.3" width="0.1524" layer="91"/>
<junction x="104.14" y="114.3"/>
<pinref part="U3" gate="G$1" pin="8"/>
<wire x1="99.06" y1="96.52" x2="104.14" y2="96.52" width="0.1524" layer="91"/>
<junction x="104.14" y="96.52"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="111.76" y1="119.38" x2="109.22" y2="119.38" width="0.1524" layer="91"/>
<wire x1="109.22" y1="119.38" x2="109.22" y2="114.3" width="0.1524" layer="91"/>
<junction x="109.22" y="114.3"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="116.84" y1="223.52" x2="116.84" y2="220.98" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="116.84" y1="220.98" x2="121.92" y2="220.98" width="0.1524" layer="91"/>
<wire x1="121.92" y1="220.98" x2="142.24" y2="220.98" width="0.1524" layer="91"/>
<wire x1="142.24" y1="220.98" x2="147.32" y2="220.98" width="0.1524" layer="91"/>
<wire x1="147.32" y1="220.98" x2="147.32" y2="223.52" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="142.24" y1="223.52" x2="142.24" y2="220.98" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="121.92" y1="223.52" x2="121.92" y2="220.98" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="137.16" y1="215.9" x2="147.32" y2="215.9" width="0.1524" layer="91"/>
<wire x1="147.32" y1="215.9" x2="147.32" y2="220.98" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="C"/>
<wire x1="109.22" y1="208.28" x2="147.32" y2="208.28" width="0.1524" layer="91"/>
<wire x1="147.32" y1="208.28" x2="147.32" y2="215.9" width="0.1524" layer="91"/>
<pinref part="GND30" gate="1" pin="GND"/>
<wire x1="147.32" y1="208.28" x2="147.32" y2="205.74" width="0.1524" layer="91"/>
<junction x="147.32" y="220.98"/>
<junction x="142.24" y="220.98"/>
<junction x="121.92" y="220.98"/>
<junction x="147.32" y="215.9"/>
<junction x="147.32" y="208.28"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="GND32" gate="1" pin="GND"/>
<wire x1="58.42" y1="223.52" x2="58.42" y2="218.44" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="58.42" y1="218.44" x2="58.42" y2="215.9" width="0.1524" layer="91"/>
<wire x1="66.04" y1="218.44" x2="63.5" y2="218.44" width="0.1524" layer="91"/>
<junction x="58.42" y="218.44"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="63.5" y1="218.44" x2="58.42" y2="218.44" width="0.1524" layer="91"/>
<wire x1="63.5" y1="218.44" x2="63.5" y2="223.52" width="0.1524" layer="91"/>
<junction x="63.5" y="218.44"/>
</segment>
<segment>
<pinref part="GND37" gate="1" pin="GND"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="307.34" y1="236.22" x2="309.88" y2="236.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND36" gate="1" pin="GND"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="307.34" y1="220.98" x2="309.88" y2="220.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="121.92" y1="104.14" x2="127" y2="104.14" width="0.1524" layer="91"/>
<wire x1="127" y1="104.14" x2="127" y2="93.98" width="0.1524" layer="91"/>
<pinref part="GND39" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="111.76" y1="175.26" x2="109.22" y2="175.26" width="0.1524" layer="91"/>
<wire x1="109.22" y1="175.26" x2="104.14" y2="175.26" width="0.1524" layer="91"/>
<wire x1="104.14" y1="175.26" x2="104.14" y2="157.48" width="0.1524" layer="91"/>
<pinref part="GND18" gate="1" pin="GND"/>
<pinref part="U4" gate="G$1" pin="1"/>
<wire x1="104.14" y1="157.48" x2="104.14" y2="154.94" width="0.1524" layer="91"/>
<wire x1="99.06" y1="175.26" x2="104.14" y2="175.26" width="0.1524" layer="91"/>
<junction x="104.14" y="175.26"/>
<pinref part="U4" gate="G$1" pin="8"/>
<wire x1="99.06" y1="157.48" x2="104.14" y2="157.48" width="0.1524" layer="91"/>
<junction x="104.14" y="157.48"/>
<pinref part="C31" gate="G$1" pin="2"/>
<wire x1="111.76" y1="180.34" x2="109.22" y2="180.34" width="0.1524" layer="91"/>
<wire x1="109.22" y1="180.34" x2="109.22" y2="175.26" width="0.1524" layer="91"/>
<junction x="109.22" y="175.26"/>
</segment>
<segment>
<pinref part="GND19" gate="1" pin="GND"/>
<wire x1="127" y1="167.64" x2="127" y2="154.94" width="0.1524" layer="91"/>
<pinref part="R26" gate="G$1" pin="2"/>
<wire x1="121.92" y1="167.64" x2="127" y2="167.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<pinref part="GND52" gate="1" pin="GND"/>
<wire x1="350.52" y1="99.06" x2="353.06" y2="99.06" width="0.1524" layer="91"/>
<wire x1="353.06" y1="99.06" x2="353.06" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="1"/>
<wire x1="320.04" y1="78.74" x2="325.12" y2="78.74" width="0.1524" layer="91"/>
<wire x1="325.12" y1="78.74" x2="325.12" y2="71.12" width="0.1524" layer="91"/>
<pinref part="J4" gate="G$1" pin="4"/>
<wire x1="325.12" y1="71.12" x2="325.12" y2="48.26" width="0.1524" layer="91"/>
<wire x1="320.04" y1="71.12" x2="325.12" y2="71.12" width="0.1524" layer="91"/>
<pinref part="GND6" gate="1" pin="GND"/>
<junction x="325.12" y="71.12"/>
</segment>
<segment>
<pinref part="J5" gate="G$1" pin="SHIELD"/>
<pinref part="GND50" gate="1" pin="GND"/>
<wire x1="309.88" y1="106.68" x2="309.88" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="SHIELD"/>
<pinref part="GND51" gate="1" pin="GND"/>
<wire x1="309.88" y1="142.24" x2="309.88" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="SHIELD"/>
<wire x1="309.88" y1="60.96" x2="309.88" y2="55.88" width="0.1524" layer="91"/>
<pinref part="GND20" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J4" gate="G$2" pin="-"/>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="345.44" y1="53.34" x2="345.44" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J1" gate="G$2" pin="-"/>
<wire x1="360.68" y1="187.96" x2="365.76" y2="187.96" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$3" pin="-"/>
<wire x1="360.68" y1="180.34" x2="365.76" y2="180.34" width="0.1524" layer="91"/>
<wire x1="365.76" y1="180.34" x2="365.76" y2="187.96" width="0.1524" layer="91"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="365.76" y1="180.34" x2="365.76" y2="177.8" width="0.1524" layer="91"/>
<junction x="365.76" y="180.34"/>
</segment>
<segment>
<pinref part="J5" gate="G$3" pin="-"/>
<wire x1="302.26" y1="187.96" x2="307.34" y2="187.96" width="0.1524" layer="91"/>
<wire x1="307.34" y1="187.96" x2="307.34" y2="180.34" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$2" pin="-"/>
<wire x1="307.34" y1="180.34" x2="307.34" y2="177.8" width="0.1524" layer="91"/>
<wire x1="302.26" y1="180.34" x2="307.34" y2="180.34" width="0.1524" layer="91"/>
<junction x="307.34" y="180.34"/>
<pinref part="GND24" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R45" gate="G$1" pin="2"/>
<pinref part="GND25" gate="1" pin="GND"/>
<wire x1="83.82" y1="144.78" x2="86.36" y2="144.78" width="0.1524" layer="91"/>
<wire x1="86.36" y1="144.78" x2="86.36" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC_PRG_OUT" class="0">
<segment>
<wire x1="226.06" y1="200.66" x2="233.68" y2="200.66" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="233.68" y1="198.12" x2="233.68" y2="200.66" width="0.1524" layer="91"/>
<pinref part="J7" gate="-2" pin="1"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<wire x1="167.64" y1="241.3" x2="167.64" y2="238.76" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="167.64" y1="238.76" x2="170.18" y2="238.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
<wire x1="226.06" y1="203.2" x2="228.6" y2="203.2" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="228.6" y1="203.2" x2="241.3" y2="203.2" width="0.1524" layer="91"/>
<wire x1="228.6" y1="198.12" x2="228.6" y2="203.2" width="0.1524" layer="91"/>
<junction x="228.6" y="203.2"/>
<pinref part="J7" gate="-4" pin="1"/>
<wire x1="241.3" y1="203.2" x2="241.3" y2="218.44" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="327.66" y1="233.68" x2="322.58" y2="233.68" width="0.1524" layer="91"/>
<wire x1="322.58" y1="233.68" x2="322.58" y2="236.22" width="0.1524" layer="91"/>
<wire x1="322.58" y1="236.22" x2="322.58" y2="238.76" width="0.1524" layer="91"/>
<wire x1="322.58" y1="220.98" x2="322.58" y2="233.68" width="0.1524" layer="91"/>
<junction x="322.58" y="233.68"/>
<pinref part="+3V13" gate="G$1" pin="+3V3"/>
<pinref part="C25" gate="G$1" pin="2"/>
<wire x1="322.58" y1="215.9" x2="322.58" y2="220.98" width="0.1524" layer="91"/>
<wire x1="320.04" y1="236.22" x2="322.58" y2="236.22" width="0.1524" layer="91"/>
<pinref part="C24" gate="G$1" pin="2"/>
<wire x1="320.04" y1="220.98" x2="322.58" y2="220.98" width="0.1524" layer="91"/>
<junction x="322.58" y="236.22"/>
<junction x="322.58" y="220.98"/>
<pinref part="J6" gate="-3" pin="1"/>
<pinref part="J6" gate="-6" pin="1"/>
<wire x1="327.66" y1="215.9" x2="322.58" y2="215.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="127" y1="114.3" x2="127" y2="119.38" width="0.1524" layer="91"/>
<wire x1="127" y1="119.38" x2="127" y2="121.92" width="0.1524" layer="91"/>
<wire x1="121.92" y1="114.3" x2="124.46" y2="114.3" width="0.1524" layer="91"/>
<pinref part="+3V6" gate="G$1" pin="+3V3"/>
<pinref part="U3" gate="G$1" pin="2"/>
<wire x1="124.46" y1="114.3" x2="127" y2="114.3" width="0.1524" layer="91"/>
<wire x1="99.06" y1="111.76" x2="124.46" y2="111.76" width="0.1524" layer="91"/>
<wire x1="124.46" y1="111.76" x2="124.46" y2="114.3" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="7"/>
<wire x1="99.06" y1="99.06" x2="124.46" y2="99.06" width="0.1524" layer="91"/>
<wire x1="124.46" y1="99.06" x2="124.46" y2="111.76" width="0.1524" layer="91"/>
<junction x="124.46" y="111.76"/>
<junction x="124.46" y="114.3"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="121.92" y1="119.38" x2="127" y2="119.38" width="0.1524" layer="91"/>
<junction x="127" y="119.38"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="137.16" y1="241.3" x2="139.7" y2="241.3" width="0.1524" layer="91"/>
<wire x1="139.7" y1="241.3" x2="139.7" y2="236.22" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="137.16" y1="236.22" x2="139.7" y2="236.22" width="0.1524" layer="91"/>
<wire x1="139.7" y1="236.22" x2="142.24" y2="236.22" width="0.1524" layer="91"/>
<wire x1="142.24" y1="236.22" x2="147.32" y2="236.22" width="0.1524" layer="91"/>
<wire x1="147.32" y1="236.22" x2="147.32" y2="233.68" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="142.24" y1="233.68" x2="142.24" y2="236.22" width="0.1524" layer="91"/>
<junction x="139.7" y="236.22"/>
<junction x="142.24" y="236.22"/>
<wire x1="147.32" y1="236.22" x2="147.32" y2="238.76" width="0.1524" layer="91"/>
<junction x="147.32" y="236.22"/>
<pinref part="+3V12" gate="G$1" pin="+3V3"/>
<pinref part="L1" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="127" y1="175.26" x2="127" y2="180.34" width="0.1524" layer="91"/>
<wire x1="127" y1="180.34" x2="127" y2="182.88" width="0.1524" layer="91"/>
<wire x1="121.92" y1="175.26" x2="124.46" y2="175.26" width="0.1524" layer="91"/>
<pinref part="+3V5" gate="G$1" pin="+3V3"/>
<pinref part="U4" gate="G$1" pin="2"/>
<wire x1="124.46" y1="175.26" x2="127" y2="175.26" width="0.1524" layer="91"/>
<wire x1="99.06" y1="172.72" x2="124.46" y2="172.72" width="0.1524" layer="91"/>
<wire x1="124.46" y1="172.72" x2="124.46" y2="175.26" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="7"/>
<wire x1="99.06" y1="160.02" x2="124.46" y2="160.02" width="0.1524" layer="91"/>
<wire x1="124.46" y1="160.02" x2="124.46" y2="165.1" width="0.1524" layer="91"/>
<junction x="124.46" y="172.72"/>
<junction x="124.46" y="175.26"/>
<pinref part="R24" gate="G$1" pin="2"/>
<wire x1="124.46" y1="165.1" x2="124.46" y2="172.72" width="0.1524" layer="91"/>
<wire x1="121.92" y1="165.1" x2="124.46" y2="165.1" width="0.1524" layer="91"/>
<junction x="124.46" y="165.1"/>
<pinref part="C31" gate="G$1" pin="1"/>
<wire x1="121.92" y1="180.34" x2="127" y2="180.34" width="0.1524" layer="91"/>
<junction x="127" y="180.34"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<wire x1="200.66" y1="215.9" x2="203.2" y2="215.9" width="0.1524" layer="91"/>
<pinref part="J7" gate="-13" pin="1"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<wire x1="226.06" y1="213.36" x2="228.6" y2="213.36" width="0.1524" layer="91"/>
<pinref part="J7" gate="-12" pin="1"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<wire x1="226.06" y1="210.82" x2="228.6" y2="210.82" width="0.1524" layer="91"/>
<pinref part="J7" gate="-10" pin="1"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<wire x1="226.06" y1="205.74" x2="228.6" y2="205.74" width="0.1524" layer="91"/>
<pinref part="J7" gate="-6" pin="1"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<wire x1="226.06" y1="215.9" x2="228.6" y2="215.9" width="0.1524" layer="91"/>
<pinref part="J7" gate="-14" pin="1"/>
</segment>
</net>
<net name="DBG_TX" class="0">
<segment>
<wire x1="327.66" y1="231.14" x2="304.8" y2="231.14" width="0.1524" layer="91"/>
<label x="307.34" y="231.14" size="1.27" layer="95"/>
<pinref part="J6" gate="-1" pin="1"/>
</segment>
</net>
<net name="DBG_RX" class="0">
<segment>
<wire x1="327.66" y1="228.6" x2="304.8" y2="228.6" width="0.1524" layer="91"/>
<label x="307.34" y="228.6" size="1.27" layer="95"/>
<pinref part="J6" gate="-2" pin="1"/>
</segment>
</net>
<net name="EXT_TXD" class="0">
<segment>
<pinref part="R22" gate="G$1" pin="2"/>
<wire x1="121.92" y1="162.56" x2="129.54" y2="162.56" width="0.1524" layer="91"/>
<label x="129.54" y="162.56" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="EXT_RXD" class="0">
<segment>
<pinref part="R27" gate="G$1" pin="2"/>
<wire x1="121.92" y1="170.18" x2="129.54" y2="170.18" width="0.1524" layer="91"/>
<label x="129.54" y="170.18" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="COM_TXD" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="121.92" y1="101.6" x2="129.54" y2="101.6" width="0.1524" layer="91"/>
<label x="129.54" y="101.6" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="COM_RXD" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="121.92" y1="109.22" x2="129.54" y2="109.22" width="0.1524" layer="91"/>
<label x="129.54" y="109.22" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="RS485_INT" class="0">
<segment>
<label x="129.54" y="106.68" size="1.27" layer="95" xref="yes"/>
<wire x1="129.54" y1="106.68" x2="121.92" y2="106.68" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PWR_IN" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="2"/>
<label x="25.4" y="236.22" size="1.27" layer="95" ratio="15" rot="R180" xref="yes"/>
<wire x1="27.94" y1="236.22" x2="25.4" y2="236.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="3"/>
<wire x1="320.04" y1="73.66" x2="332.74" y2="73.66" width="0.1524" layer="91"/>
<wire x1="332.74" y1="73.66" x2="332.74" y2="76.2" width="0.1524" layer="91"/>
<wire x1="332.74" y1="76.2" x2="332.74" y2="78.74" width="0.1524" layer="91"/>
<wire x1="332.74" y1="78.74" x2="345.44" y2="78.74" width="0.1524" layer="91"/>
<label x="353.06" y="78.74" size="1.27" layer="95" xref="yes"/>
<pinref part="J4" gate="G$1" pin="2"/>
<wire x1="345.44" y1="78.74" x2="353.06" y2="78.74" width="0.1524" layer="91"/>
<wire x1="320.04" y1="76.2" x2="332.74" y2="76.2" width="0.1524" layer="91"/>
<junction x="332.74" y="76.2"/>
<wire x1="345.44" y1="76.2" x2="345.44" y2="78.74" width="0.1524" layer="91"/>
<junction x="345.44" y="78.74"/>
<pinref part="R16" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="1"/>
<pinref part="L2" gate="G$1" pin="1"/>
<wire x1="38.1" y1="236.22" x2="40.64" y2="236.22" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="40.64" y1="236.22" x2="43.18" y2="236.22" width="0.1524" layer="91"/>
<wire x1="43.18" y1="241.3" x2="40.64" y2="241.3" width="0.1524" layer="91"/>
<wire x1="40.64" y1="241.3" x2="40.64" y2="236.22" width="0.1524" layer="91"/>
<junction x="40.64" y="236.22"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<pinref part="L2" gate="G$1" pin="2"/>
<pinref part="U2" gate="G$1" pin="3"/>
<wire x1="53.34" y1="236.22" x2="55.88" y2="236.22" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="55.88" y1="236.22" x2="58.42" y2="236.22" width="0.1524" layer="91"/>
<wire x1="58.42" y1="236.22" x2="63.5" y2="236.22" width="0.1524" layer="91"/>
<wire x1="63.5" y1="236.22" x2="68.58" y2="236.22" width="0.1524" layer="91"/>
<wire x1="53.34" y1="241.3" x2="55.88" y2="241.3" width="0.1524" layer="91"/>
<wire x1="55.88" y1="241.3" x2="55.88" y2="236.22" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="58.42" y1="233.68" x2="58.42" y2="236.22" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="63.5" y1="233.68" x2="63.5" y2="236.22" width="0.1524" layer="91"/>
<junction x="58.42" y="236.22"/>
<junction x="63.5" y="236.22"/>
<junction x="55.88" y="236.22"/>
</segment>
</net>
<net name="N$67" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="2"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="88.9" y1="236.22" x2="96.52" y2="236.22" width="0.1524" layer="91"/>
<wire x1="96.52" y1="236.22" x2="96.52" y2="233.68" width="0.1524" layer="91"/>
<wire x1="96.52" y1="236.22" x2="101.6" y2="236.22" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="101.6" y1="236.22" x2="116.84" y2="236.22" width="0.1524" layer="91"/>
<wire x1="116.84" y1="236.22" x2="121.92" y2="236.22" width="0.1524" layer="91"/>
<wire x1="121.92" y1="236.22" x2="124.46" y2="236.22" width="0.1524" layer="91"/>
<wire x1="124.46" y1="236.22" x2="127" y2="236.22" width="0.1524" layer="91"/>
<wire x1="121.92" y1="233.68" x2="121.92" y2="236.22" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="116.84" y1="233.68" x2="116.84" y2="236.22" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="127" y1="241.3" x2="124.46" y2="241.3" width="0.1524" layer="91"/>
<wire x1="124.46" y1="241.3" x2="124.46" y2="236.22" width="0.1524" layer="91"/>
<junction x="96.52" y="236.22"/>
<pinref part="D1" gate="G$1" pin="-"/>
<wire x1="101.6" y1="228.6" x2="101.6" y2="236.22" width="0.1524" layer="91"/>
<junction x="101.6" y="236.22"/>
<junction x="116.84" y="236.22"/>
<junction x="121.92" y="236.22"/>
<junction x="124.46" y="236.22"/>
<pinref part="L1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$69" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<pinref part="U2" gate="G$1" pin="1"/>
<wire x1="76.2" y1="218.44" x2="78.74" y2="218.44" width="0.1524" layer="91"/>
<wire x1="78.74" y1="218.44" x2="78.74" y2="228.6" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="96.52" y1="223.52" x2="96.52" y2="220.98" width="0.1524" layer="91"/>
<wire x1="96.52" y1="220.98" x2="99.06" y2="220.98" width="0.1524" layer="91"/>
<wire x1="78.74" y1="218.44" x2="96.52" y2="218.44" width="0.1524" layer="91"/>
<wire x1="96.52" y1="218.44" x2="96.52" y2="220.98" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="E"/>
<wire x1="99.06" y1="208.28" x2="96.52" y2="208.28" width="0.1524" layer="91"/>
<wire x1="96.52" y1="208.28" x2="96.52" y2="218.44" width="0.1524" layer="91"/>
<junction x="78.74" y="218.44"/>
<junction x="96.52" y="220.98"/>
<junction x="96.52" y="218.44"/>
</segment>
</net>
<net name="N$72" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="D1" gate="G$1" pin="+"/>
<wire x1="109.22" y1="220.98" x2="111.76" y2="220.98" width="0.1524" layer="91"/>
<wire x1="111.76" y1="220.98" x2="111.76" y2="228.6" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="B"/>
<wire x1="104.14" y1="213.36" x2="104.14" y2="215.9" width="0.1524" layer="91"/>
<wire x1="104.14" y1="215.9" x2="111.76" y2="215.9" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="111.76" y1="215.9" x2="127" y2="215.9" width="0.1524" layer="91"/>
<wire x1="111.76" y1="220.98" x2="111.76" y2="215.9" width="0.1524" layer="91"/>
<junction x="111.76" y="215.9"/>
<junction x="111.76" y="220.98"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="U3" gate="G$1" pin="10"/>
<wire x1="63.5" y1="99.06" x2="68.58" y2="99.06" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="15"/>
<wire x1="68.58" y1="99.06" x2="73.66" y2="99.06" width="0.1524" layer="91"/>
<wire x1="73.66" y1="111.76" x2="68.58" y2="111.76" width="0.1524" layer="91"/>
<wire x1="68.58" y1="111.76" x2="68.58" y2="99.06" width="0.1524" layer="91"/>
<junction x="68.58" y="99.06"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="3"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="99.06" y1="109.22" x2="111.76" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="6"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="99.06" y1="101.6" x2="111.76" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<pinref part="R9" gate="G$1" pin="1"/>
<pinref part="U3" gate="G$1" pin="4"/>
<pinref part="U3" gate="G$1" pin="5"/>
<wire x1="111.76" y1="106.68" x2="101.6" y2="106.68" width="0.1524" layer="91"/>
<wire x1="101.6" y1="106.68" x2="99.06" y2="106.68" width="0.1524" layer="91"/>
<wire x1="99.06" y1="104.14" x2="101.6" y2="104.14" width="0.1524" layer="91"/>
<wire x1="101.6" y1="104.14" x2="101.6" y2="106.68" width="0.1524" layer="91"/>
<junction x="101.6" y="106.68"/>
<wire x1="111.76" y1="104.14" x2="101.6" y2="104.14" width="0.1524" layer="91"/>
<junction x="101.6" y="104.14"/>
</segment>
</net>
<net name="IN_A" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="11"/>
<wire x1="73.66" y1="101.6" x2="66.04" y2="101.6" width="0.1524" layer="91"/>
<wire x1="66.04" y1="101.6" x2="66.04" y2="109.22" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="14"/>
<wire x1="66.04" y1="109.22" x2="73.66" y2="109.22" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="38.1" y1="106.68" x2="35.56" y2="106.68" width="0.1524" layer="91"/>
<wire x1="35.56" y1="106.68" x2="35.56" y2="109.22" width="0.1524" layer="91"/>
<wire x1="35.56" y1="109.22" x2="66.04" y2="109.22" width="0.1524" layer="91"/>
<junction x="66.04" y="109.22"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="53.34" y1="96.52" x2="35.56" y2="96.52" width="0.1524" layer="91"/>
<wire x1="35.56" y1="96.52" x2="35.56" y2="106.68" width="0.1524" layer="91"/>
<junction x="35.56" y="106.68"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="35.56" y1="96.52" x2="35.56" y2="88.9" width="0.1524" layer="91"/>
<wire x1="35.56" y1="88.9" x2="53.34" y2="88.9" width="0.1524" layer="91"/>
<junction x="35.56" y="96.52"/>
<wire x1="35.56" y1="109.22" x2="30.48" y2="109.22" width="0.1524" layer="91"/>
<junction x="35.56" y="109.22"/>
<label x="30.48" y="109.22" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<label x="350.52" y="157.48" size="1.27" layer="95" xref="yes"/>
<pinref part="J3" gate="G$1" pin="C"/>
<wire x1="350.52" y1="157.48" x2="345.44" y2="157.48" width="0.1524" layer="91"/>
<wire x1="345.44" y1="157.48" x2="340.36" y2="157.48" width="0.1524" layer="91"/>
<wire x1="340.36" y1="157.48" x2="340.36" y2="154.94" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="2"/>
<wire x1="345.44" y1="162.56" x2="345.44" y2="157.48" width="0.1524" layer="91"/>
<junction x="345.44" y="157.48"/>
</segment>
</net>
<net name="IN_B" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="12"/>
<wire x1="73.66" y1="104.14" x2="63.5" y2="104.14" width="0.1524" layer="91"/>
<wire x1="63.5" y1="104.14" x2="63.5" y2="106.68" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="13"/>
<wire x1="63.5" y1="106.68" x2="73.66" y2="106.68" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="63.5" y1="106.68" x2="53.34" y2="106.68" width="0.1524" layer="91"/>
<junction x="63.5" y="106.68"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="53.34" y1="106.68" x2="50.8" y2="106.68" width="0.1524" layer="91"/>
<wire x1="50.8" y1="106.68" x2="48.26" y2="106.68" width="0.1524" layer="91"/>
<wire x1="53.34" y1="99.06" x2="50.8" y2="99.06" width="0.1524" layer="91"/>
<wire x1="50.8" y1="99.06" x2="50.8" y2="106.68" width="0.1524" layer="91"/>
<junction x="50.8" y="106.68"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="53.34" y1="91.44" x2="50.8" y2="91.44" width="0.1524" layer="91"/>
<wire x1="50.8" y1="91.44" x2="50.8" y2="99.06" width="0.1524" layer="91"/>
<junction x="50.8" y="99.06"/>
<wire x1="30.48" y1="111.76" x2="53.34" y2="111.76" width="0.1524" layer="91"/>
<wire x1="53.34" y1="111.76" x2="53.34" y2="106.68" width="0.1524" layer="91"/>
<junction x="53.34" y="106.68"/>
<label x="30.48" y="111.76" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<label x="350.52" y="160.02" size="1.27" layer="95" xref="yes"/>
<pinref part="J2" gate="G$1" pin="C"/>
<wire x1="340.36" y1="162.56" x2="340.36" y2="160.02" width="0.1524" layer="91"/>
<wire x1="340.36" y1="160.02" x2="342.9" y2="160.02" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="2"/>
<wire x1="342.9" y1="160.02" x2="350.52" y2="160.02" width="0.1524" layer="91"/>
<wire x1="345.44" y1="154.94" x2="342.9" y2="154.94" width="0.1524" layer="91"/>
<wire x1="342.9" y1="154.94" x2="342.9" y2="160.02" width="0.1524" layer="91"/>
<junction x="342.9" y="160.02"/>
</segment>
</net>
<net name="V_REF_ISO" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="2"/>
<pinref part="U3" gate="G$1" pin="9"/>
<wire x1="63.5" y1="96.52" x2="68.58" y2="96.52" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="16"/>
<wire x1="68.58" y1="96.52" x2="71.12" y2="96.52" width="0.1524" layer="91"/>
<wire x1="71.12" y1="96.52" x2="73.66" y2="96.52" width="0.1524" layer="91"/>
<wire x1="73.66" y1="114.3" x2="71.12" y2="114.3" width="0.1524" layer="91"/>
<wire x1="71.12" y1="114.3" x2="71.12" y2="96.52" width="0.1524" layer="91"/>
<junction x="71.12" y="96.52"/>
<wire x1="68.58" y1="96.52" x2="68.58" y2="91.44" width="0.1524" layer="91"/>
<junction x="68.58" y="96.52"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="68.58" y1="91.44" x2="68.58" y2="88.9" width="0.1524" layer="91"/>
<wire x1="63.5" y1="91.44" x2="68.58" y2="91.44" width="0.1524" layer="91"/>
<junction x="68.58" y="91.44"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="63.5" y1="88.9" x2="68.58" y2="88.9" width="0.1524" layer="91"/>
<junction x="68.58" y="88.9"/>
<wire x1="68.58" y1="88.9" x2="68.58" y2="83.82" width="0.1524" layer="91"/>
<wire x1="68.58" y1="83.82" x2="60.96" y2="83.82" width="0.1524" layer="91"/>
<label x="60.96" y="83.82" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="320.04" y1="152.4" x2="330.2" y2="152.4" width="0.1524" layer="91"/>
<wire x1="330.2" y1="152.4" x2="330.2" y2="142.24" width="0.1524" layer="91"/>
<wire x1="330.2" y1="142.24" x2="330.2" y2="137.16" width="0.1524" layer="91"/>
<wire x1="330.2" y1="137.16" x2="330.2" y2="134.62" width="0.1524" layer="91"/>
<wire x1="330.2" y1="134.62" x2="330.2" y2="116.84" width="0.1524" layer="91"/>
<wire x1="330.2" y1="116.84" x2="330.2" y2="106.68" width="0.1524" layer="91"/>
<wire x1="330.2" y1="106.68" x2="330.2" y2="104.14" width="0.1524" layer="91"/>
<wire x1="330.2" y1="104.14" x2="330.2" y2="101.6" width="0.1524" layer="91"/>
<wire x1="330.2" y1="101.6" x2="330.2" y2="99.06" width="0.1524" layer="91"/>
<wire x1="330.2" y1="99.06" x2="320.04" y2="99.06" width="0.1524" layer="91"/>
<wire x1="320.04" y1="101.6" x2="330.2" y2="101.6" width="0.1524" layer="91"/>
<wire x1="320.04" y1="106.68" x2="330.2" y2="106.68" width="0.1524" layer="91"/>
<wire x1="320.04" y1="116.84" x2="330.2" y2="116.84" width="0.1524" layer="91"/>
<wire x1="320.04" y1="134.62" x2="330.2" y2="134.62" width="0.1524" layer="91"/>
<wire x1="320.04" y1="142.24" x2="330.2" y2="142.24" width="0.1524" layer="91"/>
<wire x1="320.04" y1="137.16" x2="330.2" y2="137.16" width="0.1524" layer="91"/>
<junction x="330.2" y="142.24"/>
<junction x="330.2" y="137.16"/>
<junction x="330.2" y="134.62"/>
<junction x="330.2" y="116.84"/>
<junction x="330.2" y="106.68"/>
<junction x="330.2" y="101.6"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="330.2" y1="99.06" x2="340.36" y2="99.06" width="0.1524" layer="91"/>
<junction x="330.2" y="99.06"/>
<pinref part="J1" gate="G$1" pin="4"/>
<pinref part="J1" gate="G$1" pin="5"/>
<pinref part="J1" gate="G$1" pin="7"/>
<pinref part="J1" gate="G$1" pin="8"/>
<pinref part="J5" gate="G$1" pin="4"/>
<pinref part="J5" gate="G$1" pin="5"/>
<pinref part="J5" gate="G$1" pin="7"/>
<pinref part="J5" gate="G$1" pin="8"/>
<wire x1="330.2" y1="104.14" x2="353.06" y2="104.14" width="0.1524" layer="91"/>
<junction x="330.2" y="104.14"/>
<label x="353.06" y="104.14" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="R25" gate="G$1" pin="2"/>
<pinref part="U4" gate="G$1" pin="10"/>
<wire x1="63.5" y1="160.02" x2="68.58" y2="160.02" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="15"/>
<wire x1="68.58" y1="160.02" x2="73.66" y2="160.02" width="0.1524" layer="91"/>
<wire x1="73.66" y1="172.72" x2="68.58" y2="172.72" width="0.1524" layer="91"/>
<wire x1="68.58" y1="172.72" x2="68.58" y2="160.02" width="0.1524" layer="91"/>
<junction x="68.58" y="160.02"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="3"/>
<pinref part="R27" gate="G$1" pin="1"/>
<wire x1="99.06" y1="170.18" x2="111.76" y2="170.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="6"/>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="99.06" y1="162.56" x2="111.76" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="EXT_!RE" class="0">
<segment>
<pinref part="R26" gate="G$1" pin="1"/>
<pinref part="U4" gate="G$1" pin="4"/>
<wire x1="111.76" y1="167.64" x2="109.22" y2="167.64" width="0.1524" layer="91"/>
<wire x1="109.22" y1="167.64" x2="99.06" y2="167.64" width="0.1524" layer="91"/>
<wire x1="109.22" y1="167.64" x2="109.22" y2="147.32" width="0.1524" layer="91"/>
<wire x1="109.22" y1="147.32" x2="127" y2="147.32" width="0.1524" layer="91"/>
<label x="127" y="147.32" size="1.27" layer="95" xref="yes"/>
<junction x="109.22" y="167.64"/>
</segment>
</net>
<net name="EXT_DE" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="5"/>
<pinref part="R24" gate="G$1" pin="1"/>
<wire x1="111.76" y1="165.1" x2="106.68" y2="165.1" width="0.1524" layer="91"/>
<wire x1="106.68" y1="165.1" x2="99.06" y2="165.1" width="0.1524" layer="91"/>
<wire x1="106.68" y1="165.1" x2="106.68" y2="144.78" width="0.1524" layer="91"/>
<wire x1="106.68" y1="144.78" x2="127" y2="144.78" width="0.1524" layer="91"/>
<label x="127" y="144.78" size="1.27" layer="95" xref="yes"/>
<junction x="106.68" y="165.1"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="16"/>
<wire x1="73.66" y1="175.26" x2="71.12" y2="175.26" width="0.1524" layer="91"/>
<wire x1="71.12" y1="175.26" x2="71.12" y2="157.48" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="9"/>
<wire x1="71.12" y1="157.48" x2="73.66" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="63.5" y1="152.4" x2="71.12" y2="152.4" width="0.1524" layer="91"/>
<wire x1="71.12" y1="152.4" x2="71.12" y2="154.94" width="0.1524" layer="91"/>
<junction x="71.12" y="157.48"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="71.12" y1="154.94" x2="71.12" y2="157.48" width="0.1524" layer="91"/>
<wire x1="63.5" y1="149.86" x2="71.12" y2="149.86" width="0.1524" layer="91"/>
<wire x1="71.12" y1="149.86" x2="71.12" y2="152.4" width="0.1524" layer="91"/>
<junction x="71.12" y="152.4"/>
<pinref part="J8" gate="-3" pin="1"/>
<wire x1="25.4" y1="144.78" x2="71.12" y2="144.78" width="0.1524" layer="91"/>
<wire x1="71.12" y1="144.78" x2="71.12" y2="149.86" width="0.1524" layer="91"/>
<junction x="71.12" y="149.86"/>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="63.5" y1="154.94" x2="71.12" y2="154.94" width="0.1524" layer="91"/>
<junction x="71.12" y="154.94"/>
<pinref part="R45" gate="G$1" pin="1"/>
<wire x1="71.12" y1="144.78" x2="73.66" y2="144.78" width="0.1524" layer="91"/>
<junction x="71.12" y="144.78"/>
</segment>
</net>
<net name="N$106" class="0">
<segment>
<wire x1="320.04" y1="124.46" x2="325.12" y2="124.46" width="0.1524" layer="91"/>
<wire x1="325.12" y1="124.46" x2="325.12" y2="119.38" width="0.1524" layer="91"/>
<wire x1="325.12" y1="119.38" x2="320.04" y2="119.38" width="0.1524" layer="91"/>
<wire x1="325.12" y1="124.46" x2="325.12" y2="154.94" width="0.1524" layer="91"/>
<wire x1="320.04" y1="160.02" x2="325.12" y2="160.02" width="0.1524" layer="91"/>
<wire x1="325.12" y1="160.02" x2="325.12" y2="154.94" width="0.1524" layer="91"/>
<wire x1="325.12" y1="154.94" x2="320.04" y2="154.94" width="0.1524" layer="91"/>
<junction x="325.12" y="154.94"/>
<junction x="325.12" y="124.46"/>
<wire x1="325.12" y1="160.02" x2="335.28" y2="160.02" width="0.1524" layer="91"/>
<junction x="325.12" y="160.02"/>
<pinref part="J2" gate="G$1" pin="1"/>
<wire x1="335.28" y1="160.02" x2="335.28" y2="162.56" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="1"/>
<pinref part="J1" gate="G$1" pin="3"/>
<pinref part="J5" gate="G$1" pin="1"/>
<pinref part="J5" gate="G$1" pin="3"/>
</segment>
</net>
<net name="N$109" class="0">
<segment>
<wire x1="320.04" y1="157.48" x2="327.66" y2="157.48" width="0.1524" layer="91"/>
<wire x1="320.04" y1="139.7" x2="327.66" y2="139.7" width="0.1524" layer="91"/>
<wire x1="327.66" y1="139.7" x2="327.66" y2="157.48" width="0.1524" layer="91"/>
<wire x1="320.04" y1="104.14" x2="327.66" y2="104.14" width="0.1524" layer="91"/>
<wire x1="327.66" y1="104.14" x2="327.66" y2="121.92" width="0.1524" layer="91"/>
<wire x1="327.66" y1="121.92" x2="327.66" y2="139.7" width="0.1524" layer="91"/>
<wire x1="320.04" y1="121.92" x2="327.66" y2="121.92" width="0.1524" layer="91"/>
<junction x="327.66" y="139.7"/>
<junction x="327.66" y="121.92"/>
<junction x="327.66" y="157.48"/>
<wire x1="327.66" y1="157.48" x2="335.28" y2="157.48" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="1"/>
<wire x1="335.28" y1="157.48" x2="335.28" y2="154.94" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="2"/>
<pinref part="J1" gate="G$1" pin="6"/>
<pinref part="J5" gate="G$1" pin="2"/>
<pinref part="J5" gate="G$1" pin="6"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="J4" gate="G$2" pin="+"/>
<wire x1="345.44" y1="66.04" x2="345.44" y2="63.5" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DELTA_A" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="53.34" y1="154.94" x2="35.56" y2="154.94" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="35.56" y1="154.94" x2="35.56" y2="149.86" width="0.1524" layer="91"/>
<wire x1="35.56" y1="149.86" x2="53.34" y2="149.86" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="14"/>
<wire x1="66.04" y1="170.18" x2="73.66" y2="170.18" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="11"/>
<wire x1="73.66" y1="162.56" x2="66.04" y2="162.56" width="0.1524" layer="91"/>
<wire x1="66.04" y1="162.56" x2="66.04" y2="170.18" width="0.1524" layer="91"/>
<pinref part="R23" gate="G$1" pin="1"/>
<wire x1="38.1" y1="167.64" x2="35.56" y2="167.64" width="0.1524" layer="91"/>
<wire x1="35.56" y1="167.64" x2="35.56" y2="170.18" width="0.1524" layer="91"/>
<wire x1="35.56" y1="170.18" x2="66.04" y2="170.18" width="0.1524" layer="91"/>
<junction x="66.04" y="170.18"/>
<wire x1="35.56" y1="167.64" x2="35.56" y2="154.94" width="0.1524" layer="91"/>
<junction x="35.56" y="167.64"/>
<junction x="35.56" y="154.94"/>
<pinref part="J8" gate="-1" pin="1"/>
<wire x1="25.4" y1="170.18" x2="35.56" y2="170.18" width="0.1524" layer="91"/>
<junction x="35.56" y="170.18"/>
</segment>
</net>
<net name="DELTA_B" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="12"/>
<wire x1="73.66" y1="165.1" x2="63.5" y2="165.1" width="0.1524" layer="91"/>
<wire x1="63.5" y1="165.1" x2="63.5" y2="167.64" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="13"/>
<wire x1="63.5" y1="167.64" x2="73.66" y2="167.64" width="0.1524" layer="91"/>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="63.5" y1="167.64" x2="50.8" y2="167.64" width="0.1524" layer="91"/>
<junction x="63.5" y="167.64"/>
<pinref part="R25" gate="G$1" pin="1"/>
<wire x1="50.8" y1="167.64" x2="48.26" y2="167.64" width="0.1524" layer="91"/>
<wire x1="53.34" y1="160.02" x2="50.8" y2="160.02" width="0.1524" layer="91"/>
<pinref part="C28" gate="G$1" pin="2"/>
<wire x1="53.34" y1="152.4" x2="50.8" y2="152.4" width="0.1524" layer="91"/>
<wire x1="50.8" y1="152.4" x2="50.8" y2="160.02" width="0.1524" layer="91"/>
<wire x1="50.8" y1="160.02" x2="50.8" y2="165.1" width="0.1524" layer="91"/>
<junction x="50.8" y="160.02"/>
<junction x="50.8" y="167.64"/>
<pinref part="J8" gate="-2" pin="1"/>
<wire x1="50.8" y1="165.1" x2="50.8" y2="167.64" width="0.1524" layer="91"/>
<wire x1="25.4" y1="165.1" x2="50.8" y2="165.1" width="0.1524" layer="91"/>
<junction x="50.8" y="165.1"/>
</segment>
</net>
<net name="U1_TXD" class="0">
<segment>
<pinref part="J6" gate="-7" pin="1"/>
<wire x1="327.66" y1="213.36" x2="317.5" y2="213.36" width="0.1524" layer="91"/>
<label x="317.5" y="213.36" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="U1_RXD" class="0">
<segment>
<pinref part="J6" gate="-8" pin="1"/>
<wire x1="327.66" y1="210.82" x2="317.5" y2="210.82" width="0.1524" layer="91"/>
<label x="317.5" y="210.82" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LED_1" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="337.82" y1="187.96" x2="335.28" y2="187.96" width="0.1524" layer="91"/>
<label x="335.28" y="187.96" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="J1" gate="G$2" pin="+"/>
<wire x1="347.98" y1="187.96" x2="350.52" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED_2" class="0">
<segment>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="337.82" y1="180.34" x2="335.28" y2="180.34" width="0.1524" layer="91"/>
<label x="335.28" y="180.34" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="R19" gate="G$1" pin="2"/>
<pinref part="J1" gate="G$3" pin="+"/>
<wire x1="347.98" y1="180.34" x2="350.52" y2="180.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="R18" gate="G$1" pin="2"/>
<pinref part="J5" gate="G$3" pin="+"/>
<wire x1="289.56" y1="187.96" x2="292.1" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="2"/>
<pinref part="J5" gate="G$2" pin="+"/>
<wire x1="289.56" y1="180.34" x2="292.1" y2="180.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED_3" class="0">
<segment>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="279.4" y1="187.96" x2="276.86" y2="187.96" width="0.1524" layer="91"/>
<label x="276.86" y="187.96" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LED_4" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="279.4" y1="180.34" x2="276.86" y2="180.34" width="0.1524" layer="91"/>
<label x="276.86" y="180.34" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="106,2,200.66,71.12,N$1,,,,,"/>
<approved hash="106,2,223.52,68.58,N$4,,,,,"/>
<approved hash="106,2,223.52,66.04,N$5,,,,,"/>
<approved hash="106,2,223.52,60.96,N$6,,,,,"/>
<approved hash="106,2,223.52,71.12,N$7,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, Eagle supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
</compatibility>
</eagle>
