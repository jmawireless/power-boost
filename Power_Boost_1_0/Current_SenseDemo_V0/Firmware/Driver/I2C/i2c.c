#include "i2c.h"

char init_I2cAx(char uart_ucaX)
{
  switch(uart_ucaX)
  {
  default: 
    //Add code if necessary
    __no_operation();
    break;
  } 
  return 0;
}

char init_I2cBx(char uart_ucbX)
{
  switch(uart_ucbX)
  {
  case 0:
    
    UCB0CTL1 |= UCXXRST;                                        // Enable SW reset
    UCB0CTL0 = UCB0_MST_MODE + UCB0_MODEx + UCB0_SYNCMODE;      // I2C Master, synchronous mode
    UCB0CTL1 = UCB0_I2C_CLK_SOURCE + UCXXRST;                   // Use SMCLK, keep SW reset
    UCB0BR0 = UCB0BR0_L;                                        // fSCL = ~ 400kHz
    UCB0BR1 = UCB0BR0_H;
    UCB0I2COA = UCB0_I2C_OWN_ADD;                               // Own Address 
    UCB0I2CSA = UCB0_I2C_SLADDR;                                // Slave Address 
    UCB0CTL1 &= ~UCXXRST;                                       // Clear SW reset, resume operation
    UCB0IE |= UCTXIE;                                           // Enable TX interrupt   
    break;
    
  case 2: 
    UCB2CTL1 |= UCXXRST;                                        // Enable SW reset
    UCB2CTL0 = UCB2_MST_MODE + UCB2_MODEx + UCB2_SYNCMODE;      // I2C Master, synchronous mode
    UCB2CTL1 = UCB2_I2C_CLK_SOURCE + UCXXRST;                   // Use SMCLK, keep SW reset
    UCB2BR0 = UCB2BR0_L;                                        // fSCL = ~100kHz
    UCB2BR1 = UCB2BR0_H;
    UCB2I2COA = UCB2_I2C_OWN_ADD;                               // Own Address
    UCB0I2CSA = UCB2_I2C_SLADDR;                                // Slave Address
    UCB2CTL1 &= ~UCXXRST;                                       // Clear SW reset, resume operation
    UCB2IE |= UCTXIE;                                           // Enable TX interrupt  
    break;
    
  case 3:
    UCB3CTL1 |= UCXXRST;                                        // Enable SW reset
    UCB3CTL0 = UCB3_MST_MODE + UCB3_MODEx + UCB3_SYNCMODE;      // I2C Master, synchronous mode
    UCB3CTL1 = UCB3_I2C_CLK_SOURCE + UCXXRST;                   // Use SMCLK, keep SW reset
    UCB3BR0 = UCB3BR0_L;                                        // fSCL = ~100kHz
    UCB3BR1 = UCB3BR0_H;
    UCB3I2COA = UCB3_I2C_OWN_ADD;                               // Own Address
    UCB0I2CSA = UCB3_I2C_SLADDR;                                // Slave Address
    UCB3CTL1 &= ~UCXXRST;                                       // Clear SW reset, resume operation
    UCB3IE |= UCTXIE;                                           // Enable TX interrupt  
    break;
    
  default: 
    //Add code if necessary
    __no_operation();
    break;
  }
  return 0;
}

//void SendD2AConvertorMsg()
//{
//    __delay_cycles(50);                     // Delay required between transaction
//    PTxData = (unsigned char *)TxData;      // TX array start address
//                                            // Place breakpoint here to see each
//                                            // transmit operation.
//    TXByteCtr = sizeof TxData;              // Load TX byte counter
//
//    UCB0CTL1 |= UCTR + UCTXSTT;             // I2C TX, start condition
//
//    __bis_SR_register(LPM0_bits + GIE);     // Enter LPM0, enable interrupts
//    __no_operation();                       // Remain in LPM0 until all data
//                                            // is TX'd
//    while (UCB0CTL1 & UCTXSTP);             // Ensure stop condition got sent
//
//}

#pragma vector = USCI_B0_VECTOR
__interrupt void USCI_B0_ISR(void)
{
  switch(__even_in_range(UCB0IV,12))
  {
  case  0: break;                           // Vector  0: No interrupts
  case  2: break;                           // Vector  2: ALIFG
  case  4: break;                           // Vector  4: NACKIFG
  case  6:                                  // Vector  6: STTIFG
 //    UCB0IFG &= ~UCSTTIFG;                  // Clear start condition int flag
     break;
  case  8:                                  // Vector  8: STPIFG
//    TXData++;                               // Increment TXData
//    UCB0IFG &= ~UCSTPIFG;                   // Clear stop condition int flag
    break;
  case 10: break;                           // Vector 10: RXIFG  
  case 12:                                  // Vector 12: TXIFG
//    if (TXByteCtr > 0)                      // Check TX byte counter
//    {
//      UCB0TXBUF = *PTxData++;               // Load TX buffer
//      TXByteCtr--;                          // Decrement TX byte counter
//    }
//    else
//    {
//      UCB0CTL1 |= UCTXSTP;                  // I2C stop condition
//      UCB0IFG &= ~UCTXIFG;                  // Clear USCI_B0 TX int flag
//      __bic_SR_register_on_exit(LPM0_bits); // Exit LPM0
//    }
    break;
  default: break; 
  }
}

#pragma vector = USCI_B2_VECTOR
__interrupt void USCI_B2_ISR(void)
{
  switch(__even_in_range(UCB2IV,12))
  {
  case  0: break;                           // Vector  0: No interrupts (ti: 38.4.11 Section)
  case  2: break;                           // Vector  2: ALIFG
  case  4: break;                           // Vector  4: NACKIFG
  case  6: break;                           // Vector  6: STTIFG   
  case  8: break;                           // Vector  8: STPIFG 
  case 10: break;                           // Vector 10: RXIFG  
  case 12: break;                           // Vector 12: TXIFG  
  default: break; 
  }
}

#pragma vector = USCI_B3_VECTOR
__interrupt void USCI_B3_ISR(void)
{
  switch(__even_in_range(UCB3IV,12))
  {
  case  0: break;                           // Vector  0: No interrupts (ti: 38.4.11 Section)
  case  2: break;                           // Vector  2: ALIFG
  case  4: break;                           // Vector  4: NACKIFG
  case  6: break;                           // Vector  6: STTIFG   
  case  8: break;                           // Vector  8: STPIFG 
  case 10: break;                           // Vector 10: RXIFG  
  case 12: break;                           // Vector 12: TXIFG  
  default: break; 
  }
}

