#ifndef __MAIN
#define __MAIN

#include "sys_clk.h"
#include "sys_pmm.h"
#include "digital_io.h"
#include "uart.h"


extern unsigned char process_HeartBeat_Message();
extern void process_Message();
extern unsigned char setup_OVP_Register();
extern void Setup_PSU();
extern unsigned char verify_Msg_End_Flag_Rx();
extern void init_Timer();
extern char init_SpiAx(char spi_ucaX);
extern unsigned char verify_Heartbeat_Flag();
#endif
