#ifndef __I2C
#define __I2C

#include "processor_Def.h"

#define UCXXRST                  UCSWRST 
//#define UCA10_OWN_ADDR           UCA10          // Own addressing mode 0 for 7 bit; 1 for 10 bit
//#define UCA10_SLA_ADDR           UCSLA10        // Own addressing mode 0 for 7 bit; 1 for 10 bit
//#define UCMM_1_MST_ENV           UCMM           // Own addressing mode 0 for one-master; 1 for multi-master
#define UCB0_MST_MODE           UCMST           // Master Mode select 0b: Slave and 1b: Master
#define UCB0_SYNCMODE           UCSYNC          // 0: Async and 1: Sync
#define UCB0_I2C_CLK_SOURCE     UCSSEL_2        // SMCLK source clock
  
#define UCB0_I2C_FREQ_HZ        400000          // 400Khz i2c operatig freq
#define UCB0_I2C_OWN_ADD        0x77            //
#define UCB0_I2C_SLADDR         0x48;           // Slave Address

#define UCB0BRx_VAL             ROUND(SYS_CLOCK_FREQ_HZ/UCB0_I2C_FREQ_HZ)
#define UCB0BR0_L               (UCB0BRx_VAL & 0xFF)               
#define UCB0BR0_H               (UCB0BRx_VAL >> 8) 
#define UCB0_MODEx              UCMODE_3 // 00b: 3-pin SPI; 01b: 4-pin SPI (STE=1),10b 4-pin SPI (STE= 0); 11b = i2c mode

// UCB2

#define UCB2_MST_MODE           UCMST          // Master Mode select 0b: Slave and 1b: Master
#define UCB2_SYNCMODE           UCSYNC         // 0: Async and 1: Sync
#define UCB2_I2C_CLK_SOURCE     UCSSEL_2       // SMCLK source clock

#define UCB2_I2C_FREQ_HZ        100000         // 100Khz i2c operatig freq
#define UCB2_I2C_OWN_ADD        0x78
#define UCB2_I2C_SLADDR         0x50;           // Slave Address is 048h
#define UCB2BRx_VAL             ROUND(SYS_CLOCK_FREQ_HZ/UCB2_I2C_FREQ_HZ)
#define UCB2BR0_L               (UCB2BRx_VAL & 0xFF)               
#define UCB2BR0_H               (UCB2BRx_VAL >> 8) 
#define UCB2_MODEx              UCMODE_3 // 00b: 3-pin SPI; 01b: 4-pin SPI (STE=1),10b 4-pin SPI (STE= 0); 11b = i2c mode

// UCB3

#define UCB3_MST_MODE           UCMST          // Master Mode select 0b: Slave and 1b: Master
#define UCB3_SYNCMODE           UCSYNC         // 0: Async and 1: Sync
#define UCB3_I2C_CLK_SOURCE     UCSSEL_2       // SMCLK source clock

#define UCB3_I2C_FREQ_HZ        100000         // 400Khz i2c operatig freq
#define UCB3_I2C_OWN_ADD        0x79
#define UCB3_I2C_SLADDR         0x51;           // Slave Address is 048h
#define UCB3BRx_VAL             ROUND(SYS_CLOCK_FREQ_HZ/UCB3_I2C_FREQ_HZ)
#define UCB3BR0_L               (UCB3BRx_VAL & 0xFF)               
#define UCB3BR0_H               (UCB3BRx_VAL >> 8) 
#define UCB3_MODEx              UCMODE_3 // 00b: 3-pin SPI; 01b: 4-pin SPI (STE=1),10b 4-pin SPI (STE= 0); 11b = i2c mode

/***************************************************************************//**
 *                      INTERRUPT VECTOR DEFINATION
 ******************************************************************************/
/* Interrupt Vectors */
#define I2C_0    USCI_B0_VECTOR          // UART interrupt vector
#define I2C_2    USCI_B2_VECTOR          // UART interrupt vector
#define I2C_3    USCI_B3_VECTOR          // UART interrupt vector  

char init_I2cAx(char uart_ucaX);
char init_I2cBx(char uart_ucbX);


#endif