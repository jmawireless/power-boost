/***************************************************************************//**
 *  \file         main.h
 *  \brief        Program include file and function declaration for main
 *  \author       RBG
 *  \date         December,2017
 *  \version      1.0.0
 ******************************************************************************/
#ifndef __MAIN
#define __MAIN

/***************************************************************************//**
* FILE INCLUDES
*******************************************************************************/
#include "sys_clk.h"
#include "sys_pmm.h"
#include "digital_io.h"
#include "uart.h"
#include "sys_timer.h"

/***************************************************************************//**
* FUNCTION AND EXTERN FUNCTION DELCLERATION
*******************************************************************************/
extern unsigned char Process_PBC_HeartBeat_Message();
extern void Process_OVP_Message();
extern void Setup_OVP_Register();
extern void Setup_PBC();
extern unsigned char Verify_OVP_Msg_End_Flag_Rx();
extern unsigned char Verify_Heartbeat_Flag();
extern void Send_UART_Debug_Info();
#endif
