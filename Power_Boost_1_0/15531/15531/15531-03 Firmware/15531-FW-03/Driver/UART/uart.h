/***************************************************************************//**
 *  \file         uart.h
 *  \brief        Program include file and function declaration for UART 
 *                initialize and routine
 *  \author       RBG
 *  \date         December,2017
 *  \version      1.0.0
 ******************************************************************************/
#ifndef __UART
#define __UART
/***************************************************************************//**
* FILE INCLUDES
*******************************************************************************/
#include "processor_Def.h"
#define Debug_RS485 TRUE

typedef struct s_CyclicBuffer
{
  unsigned char * pBufferStart;            
  unsigned char * pBufferEnd;               
  unsigned char * pWrite;                   
  unsigned char * pRead;                    
  unsigned int length;                       
} CyclicBuffer;

/***************************************************************************//**
 * UART CONFIGURATION
 ******************************************************************************/    
  
//#define RS485_UART                     1   
#define UART_UCA0_BAUDRATE             38400      // Operational at 19200 baud rate 
#define UART_UCA0_DATA_BITS            8         // 7, 8 data bits
#define UART_UCA0_PARITY_BITS          0         // none (0), odd (1), even (2) parity bits
#define UART_UCA0_STOP_BITS            1         // 1, 2 stop bits
#define UART_UCA0_LSB_MSB_FIRST        0         // lsb (0), msb (1) first
#define UART_UCA0_BYTES_BEFORE_TX      1         // bytes before transmission (Tx)

//#define SECONDARY_UART                 2  
#define UART_UCA1_BAUDRATE             115200    // default baud rate functional baud 115200
#define UART_UCA1_DATA_BITS            8         // 7, 8 data bits
#define UART_UCA1_PARITY_BITS          0         // none (0), odd (1), even (2) parity bits
#define UART_UCA1_STOP_BITS            1         // 1, 2 stop bits
#define UART_UCA1_LSB_MSB_FIRST        0         // lsb (0), msb (1) first
#define UART_UCA1_BYTES_BEFORE_TX      1         // bytes before transmission (Tx)

//#define TERSHARY UART                 3  
#define UART_UCA2_BAUDRATE             19200     // default baud rate 19200 Functional Baud 38400
#define UART_UCA2_DATA_BITS            8         // 7, 8 data bits
#define UART_UCA2_PARITY_BITS          0         // none (0), odd (1), even (2) parity bits
#define UART_UCA2_STOP_BITS            1         // 1, 2 stop bits
#define UART_UCA2_LSB_MSB_FIRST        0         // lsb (0), msb (1) first
#define UART_UCA2_BYTES_BEFORE_TX      1         // bytes before transmission (Tx)

//#define DEBUG_UART                     3  
#define UART_UCA3_BAUDRATE             115200    // default baud rate
#define UART_UCA3_DATA_BITS            8         // 7, 8 data bits
#define UART_UCA3_PARITY_BITS          0         // none (0), odd (1), even (2) parity bits
#define UART_UCA3_STOP_BITS            1         // 1, 2 stop bits
#define UART_UCA3_LSB_MSB_FIRST        0         // lsb (0), msb (1) first
#define UART_UCA3_BYTES_BEFORE_TX      1         // bytes before transmission (Tx)



/***************************************************************************//**
 *                           UART UCA0 CONFIGURATION
 ******************************************************************************/
#ifndef UCXXRST                  
#define UCXXRST                               UCSWRST   
#endif

#define UART_UCA0_MODULATION                 1                     // enable (1), disable (0)
#define UART_UCA0_CLOCK_SOURCE               UCSSEL__SMCLK         // UCSSEL__SMCLK, UCSSEL__ACLK
#define UART_UCA0_CLOCK_FREQUENCY            SYSTEM_TRUE_CLOCK_HZ  // Calculated clock freq

/* Baudrate */
#define UART_UCA0_BAUDRATE_DIV_FACTOR        (UART_UCA0_CLOCK_FREQUENCY / UART_UCA0_BAUDRATE)  //
#define UART_UCA0_BAUDRATE_CTL_REG1          (UART_UCA0_BAUDRATE_DIV_FACTOR >> 8)         //
#define UART_UCA0_BAUDRATE_CTL_REG0          (UART_UCA0_BAUDRATE_DIV_FACTOR & 0xFF)       //

/* Modulation */
//#define UART_UCA0_MODULATION_UCBRSX          6
#define UART_UCA0_MODULATION_UCBRSX          ROUND(((((UART_UCA0_CLOCK_FREQUENCY * 1.0) / (UART_UCA0_BAUDRATE * 1.0)) - (UART_UCA0_BAUDRATE_DIV_FACTOR * 1.0)) * 8.0))  // Calculate low frequency modulation bit select
#define UART_UCA0_MODULATION_UCBRFX          0                     //
#define UART_UCA0_MODULATION_UCOS16          UCOS16                //

/* Data bits */
#if (UART_UCA0_DATA_BITS == 7)                                     // 7 data bits
#define UART_UCA0_DATA_BIT_SELECT            UC7BIT                //
#else                                                              // 8 data bits
#define UART_UCA0_DATA_BIT_SELECT            0                     //
#endif

/* Parity bits */
#if (UART_UCA0_PARITY_BITS == 0)                                  
#define UART_UCA0_PARITY_ENABLE              0                     // Parity Disable
#define UART_UCA0_PARITY_SELECT              0                     // No Parity 
#elif (UART_UCA0_PARITY_BITS == 1)                                  
#define UART_UCA0_PARITY_ENABLE              UCPEN                 // Parity Enable
#define UART_UCA0_PARITY_SELECT              0                     // Odd parity bit
#elif (UART_UCA0_PARITY_BITS == 2)                                  
#define UART_UCA0_PARITY_ENABLE              UCPEN                 // Parity Enable
#define UART_UCA0_PARITY_SELECT              UCPAR                 // Even parity bit
#endif

/* Stop bits */
#if (UART_UCA0_STOP_BITS == 1)                                     // 1 stop bit
#define UART_UCA0_STOP_BIT_SELECT            0                     //
#else                                                         // 2 stop bits
#define UART_UCA0_STOP_BIT_SELECT            UCSPB                 //
#endif

/* Flow control */
// TODO: Add defines for flow control

/* LSB, MSB first */
#if (UART_UCA0_LSB_MSB_FIRST == 1)                                 // MSB first
#define UART_UCA0_ENDIAN_BITS                UCMSB                 //
#else                                                           // LSB first
#define UART_UCA0_ENDIAN_BITS                0                     //
#endif  
   
#define UART_UCA0_MODE                       UCMODE_0            // UCMODE_0 - UART Mode
                                                                 // UCMODE_1 - idle line multipoint mode
                                                                 // UCMODE_2 - address bit multipoint mode
                                                                 // UCMODE_3 - UART mode with automatic baud-rate detect
  
#define Uart_UCA0_Tx_Buffer                        UCA0TXBUF
#define Uart_UCA0_Rx_Buffer                        UCA0RXBUF
   

/***************************************************************************//**
 * UART UCA1 CONFIGURATION
 ******************************************************************************/
/* UART : UCA0 CONFIGURATION */
#define UART_UCA1_MODULATION                 1                     // enable (1), disable (0)
#define UART_UCA1_CLOCK_SOURCE               UCSSEL__SMCLK         // UCSSEL__SMCLK, UCSSEL__ACLK
#define UART_UCA1_CLOCK_FREQUENCY            SYSTEM_TRUE_CLOCK_HZ  // Calculated clock freq

/* Baudrate */
#define UART_UCA1_BAUDRATE_DIV_FACTOR        (UART_UCA1_CLOCK_FREQUENCY / UART_UCA1_BAUDRATE)  //
#define UART_UCA1_BAUDRATE_CTL_REG1          (UART_UCA1_BAUDRATE_DIV_FACTOR >> 8)         //
#define UART_UCA1_BAUDRATE_CTL_REG0          (UART_UCA1_BAUDRATE_DIV_FACTOR & 0xFF)       //

/* Modulation */
//#define UART_UCA1_MODULATION_UCBRSX          6
#define UART_UCA1_MODULATION_UCBRSX          ROUND(((((UART_UCA1_CLOCK_FREQUENCY * 1.0) / (UART_UCA1_BAUDRATE * 1.0)) - (UART_UCA1_BAUDRATE_DIV_FACTOR * 1.0)) * 8.0)) + 1  // Calculate low frequency modulation bit select
#define UART_UCA1_MODULATION_UCBRFX          0                     //
#define UART_UCA1_MODULATION_UCOS16          UCOS16                //

/* Data bits */
#if (UART_UCA1_DATA_BITS == 7)                                     // 7 data bits
#define UART_UCA1_DATA_BIT_SELECT            UC7BIT                //
#else                                                              // 8 data bits
#define UART_UCA1_DATA_BIT_SELECT            0                     //
#endif

/* Parity bits */
#if (UART_UCA1_PARITY_BITS == 0)                                   
#define UART_UCA1_PARITY_ENABLE              0                     // Parity Disable
#define UART_UCA1_PARITY_SELECT              0                     // No Parity
#elif (UART_UCA1_PARITY_BITS == 1)                                  
#define UART_UCA1_PARITY_ENABLE              UCPEN                 // Parity Enable
#define UART_UCA1_PARITY_SELECT              0                     // Odd parity bit
#elif (UART_UCA1_PARITY_BITS == 2)                                  
#define UART_UCA1_PARITY_ENABLE              UCPEN                 // Parity Enable
#define UART_UCA1_PARITY_SELECT              UCPAR                 // Even parity bit
#endif

/* Stop bits */
#if (UART_UCA1_STOP_BITS == 1)                                     // 1 stop bit
#define UART_UCA1_STOP_BIT_SELECT            0                     //
#else                                                         // 2 stop bits
#define UART_UCA1_STOP_BIT_SELECT            UCSPB                 //
#endif

/* Flow control */
// TODO: Add defines for flow control

/* LSB, MSB first */
#if (UART_UCA1_LSB_MSB_FIRST == 1)                                 // MSB first
#define UART_UCA1_ENDIAN_BITS                UCMSB                 //
#else                                                         // LSB first
#define UART_UCA1_ENDIAN_BITS                0                     //
#endif   

#define UART_UCA1_MODE                       UCMODE_0            // UCMODE_0 - UART Mode
                                                                 // UCMODE_1 - idle line multipoint mode
                                                                 // UCMODE_2 - address bit multipoint mode
                                                                 // UCMODE_3 - UART mode with automatic baud-rate detect
#define Uart_UCA1_Tx_Buffer                        UCA1TXBUF
#define Uart_UCA1_Rx_Buffer                        UCA1RXBUF
   
/***************************************************************************//**
 * UART UCA2 CONFIGURATION
 ******************************************************************************/
#define UART_UCA2_MODULATION                 1                     // enable (1), disable (0)
#define UART_UCA2_CLOCK_SOURCE               UCSSEL__SMCLK         // UCSSEL__SMCLK, UCSSEL__ACLK
#define UART_UCA2_CLOCK_FREQUENCY            SYSTEM_TRUE_CLOCK_HZ  // Calculated clock freq

/* Baudrate */
#define UART_UCA2_BAUDRATE_DIV_FACTOR        (UART_UCA2_CLOCK_FREQUENCY / UART_UCA2_BAUDRATE)  //
#define UART_UCA2_BAUDRATE_CTL_REG1          (UART_UCA2_BAUDRATE_DIV_FACTOR >> 8)         //
#define UART_UCA2_BAUDRATE_CTL_REG0          (UART_UCA2_BAUDRATE_DIV_FACTOR & 0xFF)       //

/* Modulation */
#define UART_UCA2_MODULATION_UCBRSX          ROUND(((((UART_UCA2_CLOCK_FREQUENCY * 1.0) / (UART_UCA2_BAUDRATE * 1.0)) - (UART_UCA2_BAUDRATE_DIV_FACTOR * 1.0)) * 8.0))  // Calculate low frequency modulation bit select
#define UART_UCA2_MODULATION_UCBRFX          0                  //
#define UART_UCA2_MODULATION_UCOS16          UCOS16             //

/* Data bits */
#if (UART_UCA2_DATA_BITS == 7)                                  // 7 data bits
#define UART_UCA2_DATA_BIT_SELECT            UC7BIT             //
#else                                                           // 8 data bits
#define UART_UCA2_DATA_BIT_SELECT            0                  //
#endif

/* Parity bits */
#if (UART_UCA2_PARITY_BITS == 0)                                   
#define UART_UCA2_PARITY_ENABLE              0                  // Parity Disable
#define UART_UCA2_PARITY_SELECT              0                  // No parity bit
#elif (UART_UCA2_PARITY_BITS == 1)                                  
#define UART_UCA2_PARITY_ENABLE              UCPEN              // Parity Enable
#define UART_UCA2_PARITY_SELECT              0                  // Odd parity bit
#elif (UART_UCA2_PARITY_BITS == 2)                                 
#define UART_UCA2_PARITY_ENABLE              UCPEN              // Parity Enable
#define UART_UCA2_PARITY_SELECT              UCPAR              // Even parity bit
#endif

/* Stop bits */
#if (UART_UCA2_STOP_BITS == 1)                                  // 1 stop bit
#define UART_UCA2_STOP_BIT_SELECT            0                  //
#else                                                           // 2 stop bits
#define UART_UCA2_STOP_BIT_SELECT            UCSPB              //
#endif

/* Flow control */
// TODO: Add defines for flow control

/* LSB, MSB first */
#if (UART_UCA2_LSB_MSB_FIRST == 1)                              // MSB first
#define UART_UCA2_ENDIAN_BITS                UCMSB              //
#else                                                           // LSB first
#define UART_UCA2_ENDIAN_BITS                0                  //
#endif 
   
#define UART_UCA2_MODE                       UCMODE_0            // UCMODE_0 - UART Mode
                                                                 // UCMODE_1 - idle line multipoint mode
                                                                 // UCMODE_2 - address bit multipoint mode
                                                                 // UCMODE_3 - UART mode with automatic baud-rate detect
  
#define Uart_UCA2_Tx_Buffer                  UCA2TXBUF
#define Uart_UCA2_Rx_Buffer                  UCA2RXBUF

/***************************************************************************//**
 * UART UCA3 CONFIGURATION
 ******************************************************************************/
#define UART_UCA3_MODULATION                 1                     // enable (1), disable (0)
#define UART_UCA3_CLOCK_SOURCE               UCSSEL__SMCLK         // UCSSEL__SMCLK, UCSSEL__ACLK
#define UART_UCA3_CLOCK_FREQUENCY            SYSTEM_TRUE_CLOCK_HZ  // Calculated clock freq

/* Baudrate */
#define UART_UCA3_BAUDRATE_DIV_FACTOR        (UART_UCA3_CLOCK_FREQUENCY / UART_UCA3_BAUDRATE)  //
#define UART_UCA3_BAUDRATE_CTL_REG1          (UART_UCA3_BAUDRATE_DIV_FACTOR >> 8)         //
#define UART_UCA3_BAUDRATE_CTL_REG0          (UART_UCA3_BAUDRATE_DIV_FACTOR & 0xFF)       //

/* Modulation */
#define UART_UCA3_MODULATION_UCBRSX          ROUND(((((UART_UCA3_CLOCK_FREQUENCY * 1.0) / (UART_UCA3_BAUDRATE * 1.0)) - (UART_UCA3_BAUDRATE_DIV_FACTOR * 1.0)) * 8.0))  // Calculate low frequency modulation bit select
#define UART_UCA3_MODULATION_UCBRFX          0                  //
#define UART_UCA3_MODULATION_UCOS16          UCOS16             //

/* Data bits */
#if (UART_UCA3_DATA_BITS == 7)                                  // 7 data bits
#define UART_UCA3_DATA_BIT_SELECT            UC7BIT             //
#else                                                           // 8 data bits
#define UART_UCA3_DATA_BIT_SELECT            0                  //
#endif

/* Parity bits */
#if (UART_UCA3_PARITY_BITS == 0)                                   
#define UART_UCA3_PARITY_ENABLE              0                  // Parity Disable
#define UART_UCA3_PARITY_SELECT              0                  // No parity bit
#elif (UART_UCA3_PARITY_BITS == 1)                                  
#define UART_UCA3_PARITY_ENABLE              UCPEN              // Parity Enable
#define UART_UCA3_PARITY_SELECT              0                  // Odd parity bit
#elif (UART_UCA3_PARITY_BITS == 2)                                 
#define UART_UCA3_PARITY_ENABLE              UCPEN              // Parity Enable
#define UART_UCA3_PARITY_SELECT              UCPAR              // Even parity bit
#endif

/* Stop bits */
#if (UART_UCA3_STOP_BITS == 1)                                  // 1 stop bit
#define UART_UCA3_STOP_BIT_SELECT            0                  //
#else                                                           // 2 stop bits
#define UART_UCA3_STOP_BIT_SELECT            UCSPB              //
#endif

/* Flow control */
// TODO: Add defines for flow control

/* LSB, MSB first */
#if (UART_UCA3_LSB_MSB_FIRST == 1)                              // MSB first
#define UART_UCA3_ENDIAN_BITS                UCMSB              //
#else                                                           // LSB first
#define UART_UCA3_ENDIAN_BITS                0                  //
#endif 
   
#define UART_UCA3_MODE                       UCMODE_0            // UCMODE_0 - UART Mode
                                                                 // UCMODE_1 - idle line multipoint mode
                                                                 // UCMODE_2 - address bit multipoint mode
                                                                 // UCMODE_3 - UART mode with automatic baud-rate detect
  
#define Uart_UCA3_Tx_Buffer                  UCA3TXBUF
#define Uart_UCA3_Rx_Buffer                  UCA3RXBUF
     
 /***************************************************************************//**
 * INTERRUPT VECTOR DEFINATION
 ******************************************************************************/
/* Interrupt Vectors */
#define UART_0    USCI_A0_VECTOR          // UART interrupt vector
#define UART_1    USCI_A1_VECTOR          // UART interrupt vector
#define UART_2    USCI_A2_VECTOR          // UART interrupt vector
#define UART_3    USCI_A3_VECTOR          // UART interrupt vector  
   
/***************************************************************************//**
* BUFFER DEFINATION
*******************************************************************************/
extern CyclicBuffer PBC_TX_Buffer;      // Cyclic Buffer for Tx 
extern CyclicBuffer Debug_Buffer;       // Cyclic Buffer for Rx

/***************************************************************************//**
* FUNCTION AND EXTERN FUNCTION DEFINATION
*******************************************************************************/
unsigned char Init_UartAx(unsigned char uart_ucaX);
unsigned char AppWriteBufferBlock(CyclicBuffer *pAppBuffer, unsigned char *pWriteblock, unsigned char blocklength);
extern void Set_UART0_LED(unsigned char mode, unsigned char state);
extern void Process_OVP_Rx_data(unsigned char rx_data);
extern void Record_PBC_Rx_Msg(unsigned char pbc_rx_data);
extern void Change_Led( unsigned char led_name, unsigned char state);
#endif