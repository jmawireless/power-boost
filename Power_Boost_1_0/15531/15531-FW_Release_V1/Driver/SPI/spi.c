
#include "spi.h"

#define DAC_OFFSET 0x3333
#define DAC_FACTOR ROUND(3276/(OVP_High_Bound - OVP_Low_Bound))
//#define DAC_FACTOR 0x0D

unsigned char *pSpiTxData;                     // Pointer to TX data
unsigned char SpiTXByteCtr;

DAC_SPI_Msg DAC_SPI_X =
{
//  UCB3_I2C_SLADDR,
  0x00,         // A/~B, R/~W, 0, 0, A3: A1
  0xC0,         // REG1 = REG0 = 1, D13->D8 = 0x
  0x00,         // D7->D0 = 0x00
};

DAC_SPI_Msg DAC_SPI_C =
{
//  UCB3_I2C_SLADDR,
  0x00,         // A/~B, R/~W, 0, 0, A3: A1
  0xA0,         // REG1= 1, REG0 = 0, D13->D8 = 0xA0
  0x00,         // D7->D0 = 0x00
};

DAC_SPI_Msg DAC_SPI_M =
{
//  UCB3_I2C_SLADDR,
  0x01,         //A3->A0 = 0001
  0x7F,         // REG1=0, REG0= 1, D13->D8 = 0x7F
  0xFF,         // D7->D0 = 0xFF 
};

DAC_SPI_Msg DAC_SPI_SFR =
{
//  UCB3_I2C_SLADDR,
  0x01,         // A/~B, R/~W, 0, 0, A3: A1
  0x3E,         // REG1 = REG0 = 0, D13->D8 = 0x3E
  0x00,         // D7->D0 = 0x00  
};


DAC_SPI_Msg *pDAC_SPI_X = &DAC_SPI_X;
DAC_SPI_Msg *pDAC_SPI_M = &DAC_SPI_M;
DAC_SPI_Msg *pDAC_SPI_C = &DAC_SPI_C;
DAC_SPI_Msg *pDAC_SPI_SFR = &DAC_SPI_SFR;


char init_SpiAx(char spi_ucaX)
{
  switch(spi_ucaX)
  {
  case 2: 
        // Setup the USCIA2 peripheral for SPI operation.
      UCA2CTL1 |= UCSWRST;
      UCA2CTL0 |= (UCMODE_0 | UCCKPH | UCMSB | UCMST | UCSYNC);
      UCA2CTL1 |= UCSSEL_2;
      UCA2BR1 = 0;
      UCA2BR0 = 1;                                                                                                                                                             \
      UCA2CTL1 &= ~UCSWRST;                                         // Enable TX interrupt  
      SetupSPI_DAC();
    break;
  default: 
    //Add code if necessary
    __no_operation();
    break;
  }
  return 0;
}

void SetupSPI_DAC()
{
    unsigned char i;
    for (i = 0; i< 1;i++)
    {   
      SendSPI_D2AConvertorMsg(m,i, 0x3333);
      SendSPI_D2AConvertorMsg(c,i, 0x2000);
      SendSPI_D2AConvertorMsg(s,i, 0x1C00);   
      SendSPI_D2AConvertorMsg(x,i, OVP_Low_Bound);
    }
}

void SendSPI_D2AConvertorMsg(unsigned char Reg_Type, unsigned char DAC_Channel, unsigned int Raw_OVP_Data)
{
  
    unsigned short int Data = 0x0000;
    unsigned short int Voltage_Fact = 0x0000;
//    unsigned short int Previous_DAC_Val = 0x0000;
//    unsigned short int New_Raw_value = 0x0000;
    switch (Reg_Type)
    {
    case x: 
      //Method 1: 
      if (Raw_OVP_Data > TP_SET_Voltage)
      {
        Voltage_Fact = DAC_OFFSET;
      }
      else
      {
        Voltage_Fact = (((TP_SET_Voltage - Raw_OVP_Data) * DAC_FACTOR) + DAC_OFFSET);  
      }
//      //Method 2:
//      Previous_DAC_Val = (((DAC_SPI_X.OVP_DAC_data_H & 0x3F) * 256) + DAC_SPI_X.OVP_DAC_data_L);
//      if (Raw_OVP_Data > TP_SET_Voltage)
//      {
//        Voltage_Fact = (Previous_DAC_Val - (((Raw_OVP_Data - TP_SET_Voltage) * DAC_FACTOR) + DAC_OFFSET));
//      }
//      else
//      {
//        Voltage_Fact = (Previous_DAC_Val + (((TP_SET_Voltage - Raw_OVP_Data) * DAC_FACTOR) + DAC_OFFSET));
//      }
//      if (Voltage_Fact > 0x3FFF)
//      {
//        Voltage_Fact = 0x3FFF;
//      }
//      else if (Voltage_Fact < DAC_OFFSET)
//      {
//        Voltage_Fact = DAC_OFFSET;
//      }
      Data = ((Voltage_Fact & 0x3FFF ) | 0xC000);
      DAC_SPI_X.OVP_DAC_data_L = (unsigned char) (Data & 0xFF);
      DAC_SPI_X.OVP_DAC_data_H = (unsigned char) ((Data >> 8) & 0xFF);
      // This function will default reg A and write
      DAC_SPI_X.OVP_Reg = DAC_Channel;
      pSpiTxData = (unsigned char *)pDAC_SPI_X;  // TX array start address
                                              // Place breakpoint here to see each
                                              // transmit operation.
      SpiTXByteCtr = sizeof DAC_SPI_X;              // Load TX byte counter   
      break;
    
    
    case m:
      Data = ((Raw_OVP_Data & 0x3FFF ) | 0x4000);
      DAC_SPI_M.OVP_DAC_data_L = (unsigned char) (Data & 0xFF);
      DAC_SPI_M.OVP_DAC_data_H = (unsigned char) ((Data >> 8) & 0xFF);
      // This function will default reg A and write     
      DAC_SPI_M.OVP_Reg = DAC_Channel;
      pSpiTxData = (unsigned char *)pDAC_SPI_M;  // TX array start address
                                              // Place breakpoint here to see each
                                              // transmit operation.
      SpiTXByteCtr = sizeof DAC_SPI_M;              // Load TX byte counter   
      break;
    

  case c:
      Data = ((Raw_OVP_Data & 0x3FFF ) | 0x8000);
      DAC_SPI_C.OVP_DAC_data_L = (unsigned char) (Data & 0xFF);
      DAC_SPI_C.OVP_DAC_data_H = (unsigned char) ((Data >> 8) & 0xFF);
      // This function will default reg A and write         
      DAC_SPI_C.OVP_Reg = DAC_Channel;
      pSpiTxData = (unsigned char *)pDAC_SPI_C;  // TX array start address
                                              // Place breakpoint here to see each
                                              // transmit operation.
      SpiTXByteCtr = sizeof DAC_SPI_C;              // Load TX byte counter   
      break;
   
  case s:
      Data = (Raw_OVP_Data & 0x3FFF );
      DAC_SPI_SFR.OVP_DAC_data_L = (Data & 0xFF);
      DAC_SPI_SFR.OVP_DAC_data_H = ((Data >> 8) & 0xFF);
      // This function will default reg A and write  
      DAC_SPI_SFR.OVP_Reg = DAC_Channel;
      pSpiTxData = (unsigned char *)pDAC_SPI_SFR;  // TX array start address
                                              // Place breakpoint here to see each
                                              // transmit operation.
      SpiTXByteCtr = sizeof DAC_SPI_SFR;              // Load TX byte counter   
      break;
    }
    
  unsigned char i;

  EXT_SPI_POUT &= ~EXT_SPI_BIT;
  // Look for CHIP_RDYn from radio.
  //while (EXT_SPI_PIN & EXT_MISO_BIT);
  for (i = 0; i < SpiTXByteCtr; i++)
  {  
    while (!(UCA2IFG & UCTXIFG));
    UCA2TXBUF = *(pSpiTxData+i);
  }
  EXT_SPI_POUT |= EXT_SPI_BIT;
}

