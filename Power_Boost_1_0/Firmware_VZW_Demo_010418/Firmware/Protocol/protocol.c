#include "protocol.h"
#include "uart.h"

#define PSU_REGISTER_START_ADDR 40000
#define PSU_HEARTBEAT_START_ADDR 30000
#define PSU_APPLICATION_BUFFER_SIZE 500
#define PSU_CONFIG_WAIT_COUNT 10
#define MAX_RETRY 1
#define MAX_ERROR_COUNT 5


#define INVALID_DATA_TYPE 0x02
//#define DEBUG_PROTOCOL 
#if defined (DEBUG_PROTOCOL)
  #include "stdio.h"
#endif

unsigned char PSU_DataLog[PSU_APPLICATION_BUFFER_SIZE] = {0x00};

CyclicBuffer PSU_TX_Buffer =
{
  &PSU_DataLog[0],       // pBufferStart;            
  &PSU_DataLog[0] + (PSU_APPLICATION_BUFFER_SIZE -1),       // pBufferEnd;               
  &PSU_DataLog[0],       // pWrite;                   
  &PSU_DataLog[0],       // pRead;                    
  PSU_APPLICATION_BUFFER_SIZE,       // length;                       
}; 


PSU_Boost_Msg PSU_Tx_Msg = 
{
  0x00,         // Slave Address Fixed to 01
  0x00,         // Modbus Function fixed to 06  
  0x00,         // Slave Register Address High Byte
  0x00,         // Slave Regiater Address Low Byte
  0x00,         // Write date higher nibble  
  0x00,         // Alarm (refer table)
  0x00,         // Write data lower byte
  0x00,         // Error Check Low
  0x00,         // Error Checl High
};

PSU_Boost_Msg PSU_Rx_Msg = 
{
  0x00,         // Slave Address Fixed to 01
  0x00,         // Modbus Function fixed to 06  
  0x00,         // Slave Register Address High Byte
  0x00,         // Slave Regiater Address Low Byte
  0x00,         // Write date higher nibble 
  0x00,         // Alarm (refer table)  
  0x00,         // Write data lower byte
  0x00,         // Error Check Low
  0x00,         // Error Checl High
};

PSU_Boost_Error_Msg PSU_Rx_Error_Msg = 
{
  0x00,         // Slave Address Fixed to 01
  0x00,         // Modbus Function fixed to 06  
  0x00,         // Exception Code
  0x00,         // Error Check Low
  0x00,         // Error Checl High
};

Power_Boost_Msg Rx_Msg = 
{
  0x00,                 // start_byte 1
  0x00,                 // start_byte 2
  0x00,                 // System Alarm
  0x00,                 // Device Id
  0x00,                 // msg length
  {
    {
      0x00,             // OVP1_data_h
      0x00,             // OVP1_alarm
      0x00,             // OVP1_data_l
    },
    {
      0x00,             // OVP2_data_h  
      0x00,             // OVP2_alarm
      0x00,             // OVP2_data_l
    },
    {
      0x00,             // OVP3_data_h
      0x00,             // OVP3_alarm
      0x00,             // OVP3_data_l
    },
    {
      0x00,             // OVP4_data_h
      0x00,             // OVP4_alarm
      0x00,             // OVP4_data_l
    },
    {
      0x00,             // OVP5_data_h
      0x00,             // OVP5_alarm
      0x00,             // OVP5_data_l
    },
    {
      0x00,             // OVP6_data_h
      0x00,             // OVP6_alarm
      0x00,             // OVP6_data_l
    },
    {
      0x00,             // OVP7_data_h
      0x00,             // OVP7_alarm
      0x00,             // OVP7_data_l
    },
    {
      0x00,             // OVP8_data_h
      0x00,             // OVP8_alarm
      0x00,             // OVP8_data_l
    },
    {
      0x00,             // OVP9_data_h
      0x00,             // OVP9_alarm
      0x00,             // OVP9_data_l
    },
    {
      0x00,             // OVP10_data_h
      0x00,             // OVP10_alarm
      0x00,             // OVP10_data_l
    },
    {
      0x00,             // OVP11_data_h
      0x00,             // OVP11_alarm
      0x00,             // OVP11_data_l
    },
    {
      0x00,             // OVP12_data_h
      0x00,             // OVP12_alarm
      0x00,             // OVP12_data_l
    },
  },
  0x00                  // msg_checksum
};

Power_Boost_Msg *pRx_Msg = &Rx_Msg;

Previous_Msg Previous_Rx_Msg =
{0};

//Previous_Msg Previous_Rx_Msg = // Address 0
//{
//  { //s_Data_Msg Power_Boost_Processed_Msg[MAX_OVP_IDENTIFIER][OVP_Info_Per_Msg]
//    {
//      // Power_Boost_Processed_Msg[1-12][Address 0]
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP1            
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP2 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP3 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP4 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP5 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP6 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP7 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP8 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP9 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP10 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP11 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP12 
//    },
//    { // Power_Boost_Processed_Msg[1-12][Address 1]
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP1            
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP2 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP3 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP4 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP5 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP6 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP7 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP8 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP9 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP10 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP11 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP12 
//    },
//    { // Power_Boost_Processed_Msg[1-12][Address 2]
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP1            
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP2 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP3 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP4 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP5 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP6 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP7 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP8 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP9 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP10 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP11 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP12 
//    },
////    { // Power_Boost_Processed_Msg[1-12][Address 3]
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP1            
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP2 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP3 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP4 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP5 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP6 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP7 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP8 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP9 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP10 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP11 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP12 
////    },
////    { // Power_Boost_Processed_Msg[1-12][Address 4]
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP1            
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP2 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP3 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP4 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP5 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP6 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP7 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP8 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP9 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP10 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP11 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP12 
////    },
//  },
//  { //s_Data_Msg OVP_Raw_Data[MAX_OVP_IDENTIFIER][OVP_Info_Per_Msg];
//    { // OVP_Raw_Data[1-12][Address 0]
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP1            
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP2 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP3 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP4 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP5 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP6 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP7 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP8 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP9 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP10 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP11 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP12 
//    },
//    { // OVP_Raw_Data[1-12][Address 1]
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP1            
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP2 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP3 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP4 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP5 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP6 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP7 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP8 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP9 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP10 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP11 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP12 
//    },
//    { // OVP_Raw_Data[1-12][Address 2]
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP1            
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP2 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP3 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP4 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP5 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP6 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP7 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP8 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP9 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP10 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP11 
//      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP12 
//    },
////    { // OVP_Raw_Data[1-12][Address 3]
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP1            
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP2 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP3 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP4 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP5 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP6 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP7 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP8 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP9 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP10 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP11 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP12 
////    },
////    { // OVP_Raw_Data[1-12][Address 4]
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP1            
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP2 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP3 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP4 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP5 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP6 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP7 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP8 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP9 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP10 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP11 
////      {0x00,  0x00, 0x00},          // OVP_data_h, OVP_alarm, OVP_data_l : OVP12 
////    },
//  },
//#if defined NOS_PSU_VREF_CONTROL == 1
//  { // s_PSU_Mapping PSU_Register_Map[MAX_OVP_IDENTIFIER][OVP_Info_Per_Msg];
//    {
//      // Power_Boost_Processed_Msg[1-12][Address 0]
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP1            
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP2 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP3 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP4 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP5 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP6 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP7 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP8 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP9 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP10 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP11 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP12 
//    },
//    { // Power_Boost_Processed_Msg[1-12][Address 1]
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP1            
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP2 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP3 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP4 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP5 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP6 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP7 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP8 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP9 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP10 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP11 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP12  
//    },
//    { // Power_Boost_Processed_Msg[1-12][Address 2]
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP1            
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP2 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP3 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP4 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP5 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP6 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP7 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP8 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP9 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP10 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP11 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP12 
//    },
////    { // Power_Boost_Processed_Msg[1-12][Address 3]
////      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP1            
////      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP2 
////      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP3 
////      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP4 
////      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP5 
////      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP6 
////      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP7 
////      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP8 
////      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP9 
////      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP10 
////      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP11 
////      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP12 
////    },
////    { // Power_Boost_Processed_Msg[1-12][Address 4]
////      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP1            
////      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP2 
////      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP3 
////      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP4 
////      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP5 
////      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP6 
////      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP7 
////      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP8 
////      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP9 
////      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP10 
////      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP11 
////      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP12 
////    },
//  },
//  #elif defined NOS_PSU_VREF_CONTROL == 2
//  { // s_PSU_Mapping PSU_Register_Map[MAX_OVP_IDENTIFIER][OVP_Info_Per_Msg];
//    {
//      // Power_Boost_Processed_Msg[1-12][Address 0]
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP1            
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP2 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP3 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP4 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP5 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP6 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP7 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP8 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP9 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP10 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP11 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP12 
//    },
//    { // Power_Boost_Processed_Msg[1-12][Address 1]
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP1            
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP2 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP3 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP4 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP5 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP6 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP7 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP8 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP9 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP10 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP11 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP12  
//    },
//    { // Power_Boost_Processed_Msg[1-12][Address 2]
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP1            
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP2 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP3 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP4 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP5 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP6 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP7 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP8 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP9 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP10 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP11 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP12 
//    },
//    { // Power_Boost_Processed_Msg[1-12][Address 3]
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP1            
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP2 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP3 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP4 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP5 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP6 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP7 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP8 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP9 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP10 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP11 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP12 
//    },
//    { // Power_Boost_Processed_Msg[1-12][Address 4]
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP1            
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP2 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP3 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP4 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP5 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP6 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP7 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP8 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP9 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP10 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP11 
//      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Process_OVP_Flag, Configured_OVP_Flag, PSU_Module_address, OVP_MSG_Id, PSU_Address : OVP12 
//    },
//  }
//  #endif
////  // Record_Device_Id[MAX_OVP_IDENTIFIER][OVP_Info_Per_Msg]
////  {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
////  {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
////  {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
////  {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
////  {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
//};

Msg_Diagnostics RC_Msg_Error = 
{
  {0x00,},
  0x00,
  {0x0000,},  
};

 Power_Boost_Rx_Flag Rx_Msg_Flags =
{
  0x00, 
  0x00,
  0x00,
};

 Config_Flag PSU_Config_Flag =
{
  0x00,         // Config_Busy_flag
  0x00,         // Config_Busy_id    
  0x00,         // Config_Busy_ovp_id    
  0x00,         // Config_Wait_Count
}; 

Power_Boost_Rx_Flag PSU_Rx_Msg_Flag = 
{
  0x00, 
  0x00,
  0x00,
};


//Previous_Msg *pPrevious_Rx_Msg = &Previous_Rx_Msg;

HeartBeat_Msg PCU_Beat_Msg =
{
  0x0000,
  0x00, 
};

/******************************************************************************

******************************************************************************/

unsigned int CRC16 (nData, wLength)
unsigned char *nData ;                      
unsigned char wLength ;  
{
static const unsigned int wCRCTable[] = {
   0X0000, 0XC0C1, 0XC181, 0X0140, 0XC301, 0X03C0, 0X0280, 0XC241,
   0XC601, 0X06C0, 0X0780, 0XC741, 0X0500, 0XC5C1, 0XC481, 0X0440,
   0XCC01, 0X0CC0, 0X0D80, 0XCD41, 0X0F00, 0XCFC1, 0XCE81, 0X0E40,
   0X0A00, 0XCAC1, 0XCB81, 0X0B40, 0XC901, 0X09C0, 0X0880, 0XC841,
   0XD801, 0X18C0, 0X1980, 0XD941, 0X1B00, 0XDBC1, 0XDA81, 0X1A40,
   0X1E00, 0XDEC1, 0XDF81, 0X1F40, 0XDD01, 0X1DC0, 0X1C80, 0XDC41,
   0X1400, 0XD4C1, 0XD581, 0X1540, 0XD701, 0X17C0, 0X1680, 0XD641,
   0XD201, 0X12C0, 0X1380, 0XD341, 0X1100, 0XD1C1, 0XD081, 0X1040,
   0XF001, 0X30C0, 0X3180, 0XF141, 0X3300, 0XF3C1, 0XF281, 0X3240,
   0X3600, 0XF6C1, 0XF781, 0X3740, 0XF501, 0X35C0, 0X3480, 0XF441,
   0X3C00, 0XFCC1, 0XFD81, 0X3D40, 0XFF01, 0X3FC0, 0X3E80, 0XFE41,
   0XFA01, 0X3AC0, 0X3B80, 0XFB41, 0X3900, 0XF9C1, 0XF881, 0X3840,
   0X2800, 0XE8C1, 0XE981, 0X2940, 0XEB01, 0X2BC0, 0X2A80, 0XEA41,
   0XEE01, 0X2EC0, 0X2F80, 0XEF41, 0X2D00, 0XEDC1, 0XEC81, 0X2C40,
   0XE401, 0X24C0, 0X2580, 0XE541, 0X2700, 0XE7C1, 0XE681, 0X2640,
   0X2200, 0XE2C1, 0XE381, 0X2340, 0XE101, 0X21C0, 0X2080, 0XE041,
   0XA001, 0X60C0, 0X6180, 0XA141, 0X6300, 0XA3C1, 0XA281, 0X6240,
   0X6600, 0XA6C1, 0XA781, 0X6740, 0XA501, 0X65C0, 0X6480, 0XA441,
   0X6C00, 0XACC1, 0XAD81, 0X6D40, 0XAF01, 0X6FC0, 0X6E80, 0XAE41,
   0XAA01, 0X6AC0, 0X6B80, 0XAB41, 0X6900, 0XA9C1, 0XA881, 0X6840,
   0X7800, 0XB8C1, 0XB981, 0X7940, 0XBB01, 0X7BC0, 0X7A80, 0XBA41,
   0XBE01, 0X7EC0, 0X7F80, 0XBF41, 0X7D00, 0XBDC1, 0XBC81, 0X7C40,
   0XB401, 0X74C0, 0X7580, 0XB541, 0X7700, 0XB7C1, 0XB681, 0X7640,
   0X7200, 0XB2C1, 0XB381, 0X7340, 0XB101, 0X71C0, 0X7080, 0XB041,
   0X5000, 0X90C1, 0X9181, 0X5140, 0X9301, 0X53C0, 0X5280, 0X9241,
   0X9601, 0X56C0, 0X5780, 0X9741, 0X5500, 0X95C1, 0X9481, 0X5440,
   0X9C01, 0X5CC0, 0X5D80, 0X9D41, 0X5F00, 0X9FC1, 0X9E81, 0X5E40,
   0X5A00, 0X9AC1, 0X9B81, 0X5B40, 0X9901, 0X59C0, 0X5880, 0X9841,
   0X8801, 0X48C0, 0X4980, 0X8941, 0X4B00, 0X8BC1, 0X8A81, 0X4A40,
   0X4E00, 0X8EC1, 0X8F81, 0X4F40, 0X8D01, 0X4DC0, 0X4C80, 0X8C41,
   0X4400, 0X84C1, 0X8581, 0X4540, 0X8701, 0X47C0, 0X4680, 0X8641,
   0X8201, 0X42C0, 0X4380, 0X8341, 0X4100, 0X81C1, 0X8081, 0X4040 };

unsigned char nTemp = 0x00;
unsigned int wCRCWord = 0xFFFF;

   while (wLength--)
   {
      nTemp = *nData++ ^ wCRCWord;
      wCRCWord >>= 8;
      wCRCWord  ^= wCRCTable[nTemp];
   }
   return wCRCWord;

} // End: CRC16



void System_Alarm()
{
  __no_operation();
}

void OVP_Specific_Alarm()
{
  __no_operation();
}

void Critical_System_Alarm()
{
  __no_operation();
}

 void clear_RxBufferAndCount()
 {
    // Reset all timers, flags and rx buffers
    RxBuffer_Count = 0x00;
    unsigned char i;
    for (i= 0; i < sizeof(Power_Boost_Msg); i++)
    {
     RxBuffer[i] = 0x00;
    } 
 }
 
 void clear_PSU_RxBufferAndCount()
 {
    // Reset all timers, flags and rx buffers
    PSU_RxBuffer_Count = 0x00;
    unsigned char i;
    for (i= 0; i < sizeof(Power_Boost_Msg); i++)
    {
     PSU_RxBuffer[i] = 0x00;
    } 
 }


// void clear_Power_Boost_Flags()
// {
//    // Reset all timers, flags and rx buffers
//    RxBuffer_Count = 0x00;
//    unsigned char i;
//    for (i= 0; i < sizeof(Power_Boost_Msg); i++)
//    {
//      memcpy(&Rx_Msg, NULL, sizeof(Rx_Msg));
//    } 
// }
  
 unsigned char Verify_Msg_CRC(Power_Boost_Msg* Msg)
 {
   unsigned char i;
   unsigned char Cal_Crc = 0x00;
   
   for (i=0; i < (sizeof(Power_Boost_Msg)-1); i++)
   {
     Cal_Crc = (unsigned char)((Cal_Crc + *(&Msg->start_byte_1 + i)) & 0x00FF);
     //todo: Uncomment below line to skip CRC check
     //Cal_Crc = Msg->msg_checksum;
   }
   if (Cal_Crc != Msg->msg_checksum)
   {
     return FALSE;
   }
   else
   {
     return TRUE;
   }
 }

 unsigned char Verify_PSU_CRC(PSU_Boost_Msg* PSU_Msg)
 {
//   unsigned char Cal_Crc = 0x00;
//   
//   Cal_Crc = CRC16(PSU_Msg,6);
   
   if (memcmp(PSU_Msg,&PSU_Tx_Msg,sizeof(PSU_Msg)))
   {
     return FALSE;
   }
   else
   {
     return TRUE;
   }
 }
 
  

unsigned char Validate_Rx_Error_Msg(PSU_Boost_Error_Msg* Rx_Error_Msg)
{   
  if ((Rx_Error_Msg->Slave_Addr == PSU_SLAVE_ADDR) & (Rx_Error_Msg->Slave_Addr == PSU_FUNCTION_TYPE & 0x80))
  {
    unsigned int Error_Check = CRC16(&Rx_Error_Msg,3);
    if (Error_Check == (Rx_Error_Msg->Error_Check_Hi * 256 + Rx_Error_Msg->Error_Check_Lo))
    {
      return TRUE;
    }
  }
  return FALSE;
}
/***************************************************************************//**
 * @param Function      
 * @param Input         
 * @param Return        
 * @param Calling_Func  
 * @param Description   
 ******************************************************************************/
void process_Rx_data(unsigned char rx_data)
 {
  // Check if start of data received 
  if ((rx_data == COMM_START_BYTE1) & (Rx_Msg_Flags.start_byte1_rx != 0x01))
  {
   Change_Led( GRN_LED, OFF); 
   // Start of message received
   clear_RxBufferAndCount();
   Rx_Msg_Flags.end_byte_rx = 0x00;
   Rx_Msg_Flags.start_byte2_rx = 0x00;
   Rx_Msg_Flags.start_byte1_rx = 0x01;
   // Store data 
   RxBuffer[RxBuffer_Count] = rx_data;
  }
  // Message a part of original message begin received
  // Process message only if valid start byte 1 received.   
  else
  {
    // Verify if valid start byte 2 received
    if ((rx_data == COMM_START_BYTE2) && (Rx_Msg_Flags.start_byte1_rx == 0x01) && (Rx_Msg_Flags.start_byte2_rx != 0x01))
    {
      RxBuffer_Count++;
      Rx_Msg_Flags.end_byte_rx = 0x00;
      Rx_Msg_Flags.start_byte2_rx = 0x01;
      // Store data 
      RxBuffer[RxBuffer_Count] = rx_data;
    }
    // Process message if valid start sequency received. 
    else
    { 
      if ((Rx_Msg_Flags.start_byte2_rx == 0x01) && (Rx_Msg_Flags.start_byte1_rx == 0x01))
      {
        // Store data in RxBuffer
        RxBuffer_Count++;
        RxBuffer[RxBuffer_Count] = rx_data; 
        // check if complete message recived
        if ((RxBuffer_Count >= sizeof(Rx_Msg) -1) || (RxBuffer[3]  == (RxBuffer_Count + 1)))
        {
          // Read message id and then process - TBD
          memcpy(&Rx_Msg, RxBuffer, RxBuffer_Count + 1);
          //Rx_Msg.msg_checksum = RxBuffer[RxBuffer_Count];
          if (Verify_Msg_CRC(&Rx_Msg) !=0) 
          {
            // Valid CRC
            Rx_Msg_Flags.end_byte_rx = 0x01;
            Rx_Msg_Flags.start_byte1_rx = 0x00;
            Rx_Msg_Flags.start_byte2_rx = 0x00;
            Clear_Timer_Count();
          }
          else 
          {
            #if defined (DEBUG_PROTOCOL)
              printf("CRC ERROR\n");
            #endif
            // Invalid CRC
            Rx_Msg_Flags.end_byte_rx = 0x00;
            Rx_Msg_Flags.start_byte1_rx = 0x00;
            Rx_Msg_Flags.start_byte2_rx = 0x00;
          } 
          clear_RxBufferAndCount();
          
        }
      }
      // Out of line start message byte received 
      // Flush data
      else    
      {
        // Fault message  
        clear_RxBufferAndCount();
        #if defined (DEBUG_PROTOCOL)
          printf("INVALID START\n");
        #endif
      }
    }
  }
}
 
/***************************************************************************//**
 * @param Function      
 * @param Input         
 * @param Return        
 * @param Calling_Func  
 * @param Description   
 ******************************************************************************/
void process_PSU_Rx_Msg(unsigned char psu_rx_data)
{
  unsigned char Expected_Rx_Byte = (sizeof(PSU_Boost_Msg) - 1);
  PSU_Rx_Msg_Type = 0;
  // Check if start of data received 
  if ((psu_rx_data == PSU_SLAVE_ADDR) & (PSU_Rx_Msg_Flag.start_byte1_rx != 0x01))
  {
    // Change_Led( Green_Status_Led, OFF); 
    // Start of message received
    clear_PSU_RxBufferAndCount();
    // Adjust PSU Rx message start and end flags
    PSU_Rx_Msg_Flag.end_byte_rx = 0x00;
    PSU_Rx_Msg_Flag.start_byte2_rx = 0x00;
    PSU_Rx_Msg_Flag.start_byte1_rx = 0x01;
    Change_Led(PBC_LED,ON);
    // Store data               
    PSU_RxBuffer[PSU_RxBuffer_Count] = psu_rx_data; // Store rx data to buffer
  }
  // Message a part of original message begin received
  // Process message only if valid start byte 1 received.   
  else
  {
    // Verify if valid start byte 2 received is actual function (0x06) or error fucntion (0x86)
    if ((psu_rx_data == PSU_FUNCTION_TYPE & 0x7F) && (PSU_Rx_Msg_Flag.start_byte1_rx == 0x01) && (PSU_Rx_Msg_Flag.start_byte2_rx != 0x01))
    {
      // updated the Rx message lenght to data length
      if (psu_rx_data == PSU_FUNCTION_TYPE & 0x80 > 0)
      {
        Expected_Rx_Byte = sizeof(PSU_Boost_Error_Msg);
      }    
      PSU_RxBuffer_Count++;
      PSU_Rx_Msg_Flag.end_byte_rx = 0x00;
      PSU_Rx_Msg_Flag.start_byte2_rx = 0x01;
      // Store data 
      PSU_RxBuffer[PSU_RxBuffer_Count] = psu_rx_data;
    }
    // Process message if valid start sequency received. 
    else
    { 
      if ((PSU_Rx_Msg_Flag.start_byte2_rx == 0x01) && (PSU_Rx_Msg_Flag.start_byte1_rx == 0x01))
      {
        // Store data in RxBuffer
        PSU_RxBuffer_Count++;
        PSU_RxBuffer[PSU_RxBuffer_Count] = psu_rx_data; 
        unsigned char Rx_Msg_Validate_Flag = 0;
        // check if complete message recived
        if (PSU_RxBuffer_Count >= Expected_Rx_Byte)
        {
          if (psu_rx_data == PSU_FUNCTION_TYPE & 0x80 > 0)
          {
            memcpy(&PSU_Rx_Error_Msg, PSU_RxBuffer, Expected_Rx_Byte + 1);
            Rx_Msg_Validate_Flag = Validate_Rx_Error_Msg(&PSU_Rx_Error_Msg);
            PSU_Rx_Msg_Type = PSU_Rx_Error_Msg.Exception_Code;
          }  
          else
          {
            // Read message id and then process - TBD
            memcpy(&PSU_Rx_Msg, PSU_RxBuffer, Expected_Rx_Byte + 1);
            Rx_Msg_Validate_Flag = Verify_PSU_CRC(&PSU_Rx_Msg);
          }
          if (Rx_Msg_Validate_Flag == TRUE) 
          {
            // Valid CRC
            PSU_Rx_Msg_Flag.end_byte_rx = 0x01;    
            //todo: Start heartbeat timer 
            // Clear_Timer_Count();
          }
          else 
          {
            #if defined (DEBUG_PROTOCOL)
              printf("PSU ERROR\n");
            #endif
            // Invalid CRC
            PSU_Rx_Msg_Flag.end_byte_rx = 0x00;   
          }
 
          PSU_Rx_Msg_Flag.start_byte1_rx = 0x00;
          PSU_Rx_Msg_Flag.start_byte2_rx = 0x00;
          clear_PSU_RxBufferAndCount();
          
        }
      }
      // Out of line start message byte received 
      // Flush data
      else    
      {
        // Fault message  
        clear_PSU_RxBufferAndCount();
        #if defined (DEBUG_PROTOCOL)
          printf("INVALID START\n");
        #endif
      }
    }
  } 
}

 unsigned char verify_Msg_End_Flag_Rx()
 {
   return Rx_Msg_Flags.end_byte_rx;
 }

 
 
unsigned char Check_System_Alarm()
{
  if (Rx_Msg.System_Alarm > 0)
  {
     #if defined (DEBUG_PROTOCOL)
        printf("MAJOR ALARM\n");
      #endif
      // process major alarm
      System_Alarm();  
      return TRUE;
  }
  else
  {
      return FALSE;
  }
}
 


unsigned char  PSU_TX_RX_Data()
{
  // define local vaiables
  unsigned char count;
  unsigned char return_val = FALSE;
  // Clear hearbeat timer count
  Clear_Heartbeat_Count();
  
  for (count = 0; count < MAX_RETRY; count++)
  {
    // Send message to PSU
    AppWriteBufferBlock(&PSU_TX_Buffer, (unsigned char*) &PSU_Tx_Msg, (sizeof(PSU_Tx_Msg)));
    // Set interrupt flag to send out data.
    Set_Uart_Int_Flag(0);
    unsigned int delay_count;
    for (delay_count = 0; ((delay_count < 200) & (PSU_Rx_Msg_Flag.end_byte_rx == 0)); delay_count ++)
    {
      __delay_cycles(25000);
    }
    // check if data received
    if(PSU_Rx_Msg_Flag.end_byte_rx == 1)
    {
      PSU_Rx_Msg_Flag.end_byte_rx =0;
      // compare received data 
      if (PSU_Rx_Msg_Type == INVALID_DATA_TYPE)
      {
        //todo: Add logic here if INVALID_DATA_TYPE needs to be processed
        __no_operation();
      }
        count = MAX_RETRY;
        Change_Led(PBC_LED,OFF);
        unsigned char count;
        for(count = 0x00; count < sizeof(PSU_Boost_Msg);count ++)
        {
          PSU_RxBuffer[count] = 0x00;
        }
        return_val = TRUE;
        break;    

    }
    else
    {
      Change_Led(PBC_LED,ON);
    }
  }
  return return_val;
}


// Initialize Previous Rx Msg by setting all the internal buffer values to 0x00
unsigned char setup_OVP_Register()
{
  unsigned char addr_count, OVP_count;
  // set all the default structure to 0
  for (addr_count = 0; addr_count < MAX_OVP_IDENTIFIER; addr_count++)
  {
    for(OVP_count = 0; OVP_count < MAX_OVP_PER_MSG; OVP_count++)
    {
      Previous_Rx_Msg.Power_Boost_Processed_Msg[addr_count][OVP_count].OVP_data_h = 0x00;
      Previous_Rx_Msg.Power_Boost_Processed_Msg[addr_count][OVP_count].OVP_Flag = 0x00;
      Previous_Rx_Msg.Power_Boost_Processed_Msg[addr_count][OVP_count].OVP_data_l = 0x00;

      Previous_Rx_Msg.OVP_Raw_Data[addr_count][OVP_count].OVP_data_h = 0x00;
      Previous_Rx_Msg.OVP_Raw_Data[addr_count][OVP_count].OVP_Flag = 0x00;
      Previous_Rx_Msg.OVP_Raw_Data[addr_count][OVP_count].OVP_data_l = 0x00;
            
      Previous_Rx_Msg.PSU_Register_Map[addr_count][OVP_count].Configured_OVP_Flag = 0x00;
      Previous_Rx_Msg.PSU_Register_Map[addr_count][OVP_count].Set_Max_Flag = 0x00;
      Previous_Rx_Msg.PSU_Register_Map[addr_count][OVP_count].Set_Min_Flag = 0x00;
      Previous_Rx_Msg.PSU_Register_Map[addr_count][OVP_count].Set_Avg_Flag = 0x00;
      Previous_Rx_Msg.PSU_Register_Map[addr_count][OVP_count].Msg_Device_Id = 0x00; 
      Previous_Rx_Msg.PSU_Register_Map[addr_count][OVP_count].PSU_Module_address = 0x00;   
    }
  }
  return 0;
}


void Setup_PSU()
{
  unsigned status = 0;
  unsigned int PSU_Register_Address;
  // Sent message on all the address to known setpoint
  PSU_Tx_Msg.Slave_Addr = PSU_SLAVE_ADDR;
  PSU_Tx_Msg.Function = PSU_FUNCTION_TYPE;
  
  unsigned char device_addr, psu_addr;
  for (device_addr = 0; device_addr < MAX_OVP_IDENTIFIER; device_addr++)
  {
    // We dont need to individualy configure each PSU as one brick can output multiple channel
    for (psu_addr = 0; psu_addr < MAX_OVP_PER_MSG; psu_addr++) 
    {  
      // Record Previous RX Msg Raw OVP data value to Set voltage
      Previous_Rx_Msg.OVP_Raw_Data[device_addr][psu_addr].OVP_data_h = (PSU_REF_VOLTAGE >> 8);
      Previous_Rx_Msg.OVP_Raw_Data[device_addr][psu_addr].OVP_data_l = (PSU_REF_VOLTAGE & 0xFF);
      Previous_Rx_Msg.OVP_Raw_Data[device_addr][psu_addr].OVP_Flag = 0x00; 
    
      PSU_Tx_Msg.Write_Data_Hi = Previous_Rx_Msg.OVP_Raw_Data[device_addr][psu_addr].OVP_data_h;
      PSU_Tx_Msg.Write_Data_Lo = Previous_Rx_Msg.OVP_Raw_Data[device_addr][psu_addr].OVP_data_l;
      PSU_Tx_Msg.Alarm = Previous_Rx_Msg.OVP_Raw_Data[device_addr][psu_addr].OVP_Flag;
      
      PSU_Register_Address =  (PSU_REGISTER_START_ADDR + ((device_addr * MAX_OVP_PER_MSG) + psu_addr));
      PSU_Tx_Msg.Reg_Addr_Lo = (unsigned char)(PSU_Register_Address & 0x00FF);
      PSU_Tx_Msg.Reg_Addr_Hi = (unsigned char)(PSU_Register_Address >> 8);
      
      unsigned int Error_Check = CRC16(&PSU_Tx_Msg, 6);
      PSU_Tx_Msg.Error_Check_Lo = (unsigned char)(Error_Check &0x00FF);
      PSU_Tx_Msg.Error_Check_Hi = (unsigned char)(Error_Check >> 8);      
      // Transmit msg to PSU
      status |= PSU_TX_RX_Data();  
    }
  }
  PSU_Config_Flag.Config_Wait_Count = 0;
//  // Set interrupt flag to send out data.
//  Set_Uart_Int_Flag(0);
}



void Process_OVP(unsigned char id, unsigned char ovp_id)
{
  unsigned char Process_OVP_CommData = FALSE;
  unsigned char Send_PSU_Data = TRUE;
  unsigned int OVP_Failure_Flag = 0x0001;
  unsigned int OVP_readout = ((Rx_Msg.Data_Msg[ovp_id].OVP_data_h *256) + Rx_Msg.Data_Msg[ovp_id].OVP_data_l);
  unsigned int OVP_Previous_Data = ((Previous_Rx_Msg.OVP_Raw_Data[id][ovp_id].OVP_data_h *256) + Previous_Rx_Msg.OVP_Raw_Data[id][ovp_id].OVP_data_l);
  unsigned int Average_Deviation = 0x0000;
//  unsigned int Data_Average = 0x0000;
  unsigned int Updated_Set_Point = 0x0000;
  unsigned int PSU_Register_Address = 0x0000;
  unsigned int Error_Check  =0x0000;
  unsigned status = FALSE;
  if ((OVP_readout > OVP_LOW_BOUND) & (OVP_readout < OVP_HIGH_BOUND))
  { 
    Process_OVP_CommData = TRUE;
  }
  else // enter after device configured.
  { 
    #if defined (DEBUG_PROTOCOL)
      printf("NOTCFG/INVDATA\n");
    #endif
    Process_OVP_CommData = FALSE;
  }
  if (Process_OVP_CommData)
  {
    // Perform bound check
    if ((OVP_readout > OVP_LOW_BOUND) & (OVP_readout < OVP_HIGH_BOUND))
    {
      #if defined (DEBUG_PROTOCOL)
        printf("MSG OK\n");
      #endif       
      // Reset failure count 
      RC_Msg_Error.Failed_Packet_Count[id][ovp_id] = 0x00;
      RC_Msg_Error.OVP_Id_Failure_Flag[id][ovp_id] &= ~(OVP_Failure_Flag << ovp_id);
      
      // Run filter algorithm
//      Data_Average = ((OVP_Previous_Data + OVP_readout) >> 1);
      if (OVP_readout <= (TOWERTOP_SET_VOLTAGE - PSU_TOLERANCE_WINDOW))
      {
        Average_Deviation = TOWERTOP_SET_VOLTAGE - OVP_readout;
        // Updated control setpoint
        Updated_Set_Point = (OVP_Previous_Data + Average_Deviation);
        // Send message to Delta controller
        Send_PSU_Data = TRUE;
      }
      else if (OVP_readout >= (TOWERTOP_SET_VOLTAGE + PSU_TOLERANCE_WINDOW))
      {
        Average_Deviation = OVP_readout - TOWERTOP_SET_VOLTAGE;
        // Updated control setpoint
        Updated_Set_Point = (OVP_Previous_Data - Average_Deviation);   

        
        // Send message to Delta controller
        Send_PSU_Data = TRUE; 
      }
      if(Send_PSU_Data)
      {
        // Send message to PSU of setpoint needs to be updated      
        PSU_Tx_Msg.Slave_Addr = PSU_SLAVE_ADDR;
        PSU_Tx_Msg.Function = PSU_FUNCTION_TYPE;
        
        // Store Raw OVP data 
        unsigned char channel; 
        if (MAX_CHANNEL_PER_BRICK == 2)
        {
          if (ovp_id & 0x01) 
          {
            ovp_id = ovp_id>>1;
          }
        }
        if (Updated_Set_Point < PSU_MIN_VOLTAGE)
        {
          Updated_Set_Point = PSU_MIN_VOLTAGE;
        }
        else if (Updated_Set_Point > PSU_MAX_VOLTAGE)
        {
          Updated_Set_Point = PSU_MAX_VOLTAGE;
        }
//        for (channel = 0; channel < MAX_CHANNEL_PER_BRICK; channel ++)
//        {
//          
//          Previous_Rx_Msg.OVP_Raw_Data[id][ovp_id].OVP_data_h = (unsigned char)(Updated_Set_Point >> 8);
//          Previous_Rx_Msg.OVP_Raw_Data[id][ovp_id].OVP_data_l = (unsigned char)(Updated_Set_Point & 0x00FF);
//          Previous_Rx_Msg.OVP_Raw_Data[id][ovp_id].OVP_Flag = Rx_Msg.Data_Msg[ovp_id].OVP_Flag; 
//          // Prepare the message 
//          PSU_Tx_Msg.Write_Data_Hi = Previous_Rx_Msg.OVP_Raw_Data[id][ovp_id].OVP_data_h;
//          PSU_Tx_Msg.Write_Data_Lo = Previous_Rx_Msg.OVP_Raw_Data[id][ovp_id].OVP_data_l;
//          PSU_Tx_Msg.Alarm = Previous_Rx_Msg.OVP_Raw_Data[id][ovp_id].OVP_Flag;
//          
//          PSU_Register_Address =  (PSU_REGISTER_START_ADDR + ((id * 16)+ ovp_id)+ channel);
//          PSU_Tx_Msg.Reg_Addr_Lo = (unsigned char)(PSU_Register_Address & 0x00FF);
//          PSU_Tx_Msg.Reg_Addr_Hi = (unsigned char)(PSU_Register_Address >> 8);
//           
//          Error_Check = CRC16(&PSU_Tx_Msg, 6);
//          PSU_Tx_Msg.Error_Check_Lo = (unsigned char)(Error_Check &0x00FF);
//          PSU_Tx_Msg.Error_Check_Hi = (unsigned char)(Error_Check >> 8);
//          status |= PSU_TX_RX_Data();
//        }
        if(Rx_Msg.Data_Msg[ovp_id].OVP_Flag > 0)
        {
          for (channel = 0; channel < MAX_CHANNEL_PER_BRICK; channel ++)
          {
            Previous_Rx_Msg.OVP_Raw_Data[id][ovp_id].OVP_data_h = (unsigned char)(TOWERTOP_SET_VOLTAGE >> 8);
            Previous_Rx_Msg.OVP_Raw_Data[id][ovp_id].OVP_data_l = (unsigned char)(TOWERTOP_SET_VOLTAGE & 0x00FF);
            Previous_Rx_Msg.OVP_Raw_Data[id][ovp_id].OVP_Flag = Rx_Msg.Data_Msg[ovp_id].OVP_Flag; 
            // Prepare the message 
            PSU_Tx_Msg.Write_Data_Hi = Previous_Rx_Msg.OVP_Raw_Data[id][ovp_id].OVP_data_h;
            PSU_Tx_Msg.Write_Data_Lo = Previous_Rx_Msg.OVP_Raw_Data[id][ovp_id].OVP_data_l;
            PSU_Tx_Msg.Alarm = Previous_Rx_Msg.OVP_Raw_Data[id][ovp_id].OVP_Flag;
            
            PSU_Register_Address =  (PSU_REGISTER_START_ADDR + ((id * 16)+ ovp_id)+ channel);
            PSU_Tx_Msg.Reg_Addr_Lo = (unsigned char)(PSU_Register_Address & 0x00FF);
            PSU_Tx_Msg.Reg_Addr_Hi = (unsigned char)(PSU_Register_Address >> 8);
             
            Error_Check = CRC16(&PSU_Tx_Msg, 6);
            PSU_Tx_Msg.Error_Check_Lo = (unsigned char)(Error_Check &0x00FF);
            PSU_Tx_Msg.Error_Check_Hi = (unsigned char)(Error_Check >> 8);
            status |= PSU_TX_RX_Data();
          }
        }
      }
    }
    else
    {
      #if defined (DEBUG_PROTOCOL)
        printf("OUT OF BND\n");
      #endif
      // Set failure count 
      RC_Msg_Error.Failed_Packet_Count[id][ovp_id]++;
      // check fault flag if more than MAX ERROR Count number of cycles then flag
      if (RC_Msg_Error.Failed_Packet_Count[id][ovp_id] > MAX_ERROR_COUNT)
      {
        RC_Msg_Error.OVP_Id_Failure_Flag[id][ovp_id] |= (OVP_Failure_Flag << ovp_id);
        if (MAX_CHANNEL_PER_BRICK == 2)
        {
          if (ovp_id & 0x01) 
          {
            ovp_id = ovp_id>>1;
          }
        }
        // Store Raw OVP data
        unsigned char channel; 
        for (channel = 0; channel < MAX_CHANNEL_PER_BRICK; channel ++)
        {
          Previous_Rx_Msg.OVP_Raw_Data[id][ovp_id+channel].OVP_data_h = (unsigned char)(PSU_REF_VOLTAGE >> 8);
          Previous_Rx_Msg.OVP_Raw_Data[id][ovp_id+channel].OVP_data_l = (unsigned char)(PSU_REF_VOLTAGE & 0x00FF);
          Previous_Rx_Msg.OVP_Raw_Data[id][ovp_id+channel].OVP_Flag = Rx_Msg.Data_Msg[ovp_id].OVP_Flag | 0x80; 
        
          PSU_Tx_Msg.Write_Data_Hi = Previous_Rx_Msg.OVP_Raw_Data[ovp_id+channel]->OVP_data_h;
          PSU_Tx_Msg.Write_Data_Lo = Previous_Rx_Msg.OVP_Raw_Data[ovp_id+channel]->OVP_data_l;
          PSU_Tx_Msg.Alarm = (Previous_Rx_Msg.OVP_Raw_Data[ovp_id+channel]->OVP_Flag | 0x10);
        
          PSU_Register_Address = (PSU_REGISTER_START_ADDR + ((Previous_Rx_Msg.PSU_Register_Map[id][ovp_id+channel].Msg_Device_Id * MAX_OVP_PER_MSG)+ Previous_Rx_Msg.PSU_Register_Map[id][ovp_id+channel].Msg_OVP_Id1_Avg));
          PSU_Tx_Msg.Reg_Addr_Lo = (unsigned char)(PSU_Register_Address & 0x00FF);
          PSU_Tx_Msg.Reg_Addr_Hi = (unsigned char)(PSU_Register_Address >> 8);
                
          Error_Check = CRC16(&PSU_Tx_Msg, 6);
          PSU_Tx_Msg.Error_Check_Lo = (unsigned char)(Error_Check &0x00FF);
          PSU_Tx_Msg.Error_Check_Hi = (unsigned char)(Error_Check >> 8);
          status |= PSU_TX_RX_Data();
        }
      }
    }
  }
}  

unsigned char Configure_OVP(unsigned char id, unsigned char ovp_id)
{
  unsigned char status = 0;
  if ((PSU_Config_Flag.Config_Busy_flag == TRUE) && (PSU_Config_Flag.Config_Wait_Count <  PSU_CONFIG_WAIT_COUNT))
  {
    PSU_Config_Flag.Config_Wait_Count++;
    return TRUE;  
  }
  else
  {
    PSU_Config_Flag.Config_Wait_Count = 0;
    if (((PSU_Config_Flag.Config_Busy_flag == TRUE) && (PSU_Config_Flag.Config_Busy_id == id) && (PSU_Config_Flag.Config_Busy_ovp_id == ovp_id)) || (PSU_Config_Flag.Config_Busy_flag == FALSE))
    {
      unsigned int PSU_Register_Address = 0x0000;
      unsigned char count;
      unsigned int Error_Check;
      if (Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Set_Max_Flag)
      {
        Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Set_Max_Flag = FALSE;
        Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Set_Min_Flag = TRUE;
        //Check if the message is greater than set point
            // check if the message is lower than setpoint
        unsigned int Ovp_value = 0x0000;
        unsigned int Ovp_value_1 = 0x0000;
        #if defined NOS_PSU_VREF_CONTROL == 2
          unsigned int Ovp_value_2 = 0x0000;
        #endif
        for (count = 0; count < MAX_OVP_PER_MSG ; count += MAX_CHANNEL_PER_BRICK)
        {
            /* If current element is smaller than first 
               then update both first and second */
            Ovp_value = ((Rx_Msg.Data_Msg[count].OVP_data_h * 256) + Rx_Msg.Data_Msg[count].OVP_data_l);
            Ovp_value_1 = ((Rx_Msg.Data_Msg[Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id1_Max].OVP_data_h * 256) + Rx_Msg.Data_Msg[Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id1_Max].OVP_data_l);
            #if defined NOS_PSU_VREF_CONTROL == 2
            Ovp_value_2 = ((Rx_Msg.Data_Msg[Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id2_Max].OVP_data_h * 256) + Rx_Msg.Data_Msg[Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id2_Max].OVP_data_l); 
            #endif
            if ((Ovp_value > TOWERTOP_SET_VOLTAGE) || (Ovp_value > Ovp_value_1))
            {
              #if defined NOS_PSU_VREF_CONTROL == 2
                Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id2_Max = Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id1_Max;
                Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id1_Max = count;
              #elseif 
                Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id1_Max = count;
              #endif 
            }
            #if defined NOS_PSU_VREF_CONTROL == 2
            /* If value is in between first and second 
               then update second  */
            else if ((Ovp_value > Ovp_value_2) && ( Ovp_value != Ovp_value_1))
            {
                Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id2_Max = count;
            }
            #endif
        }
        // Set PSU to Min voltage
        PSU_Tx_Msg.Write_Data_Hi = (unsigned char)(PSU_MIN_VOLTAGE >> 8);
        PSU_Tx_Msg.Write_Data_Lo = (unsigned char)(PSU_MIN_VOLTAGE & 0x00FF);
        //PSU_Tx_Msg.Alarm = PSU_Rx_Msg.Alarm;
        PSU_Tx_Msg.Alarm = 0; // no alram during config
        Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Set_Min_Flag = TRUE;
      }
      else if (Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Set_Min_Flag)
      {
        Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Set_Min_Flag = FALSE;
        Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Set_Avg_Flag = TRUE;
        unsigned int Ovp_value = 0x0000;
        unsigned int Ovp_value_1 = 0x0000;
        #if defined NOS_PSU_VREF_CONTROL == 2
          unsigned int Ovp_value_2 = 0x0000;
        #endif
        // check if the message is lower than setpoint
        Ovp_value = ((Rx_Msg.Data_Msg[count].OVP_data_h * 256) + Rx_Msg.Data_Msg[count].OVP_data_l);
        Ovp_value_1 = ((Rx_Msg.Data_Msg[Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id1_Min].OVP_data_h * 256) + Rx_Msg.Data_Msg[Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id1_Min].OVP_data_l);
        #if defined NOS_PSU_VREF_CONTROL == 2
        Ovp_value_2 = ((Rx_Msg.Data_Msg[Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id2_Min].OVP_data_h * 256) + Rx_Msg.Data_Msg[Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id2_Min].OVP_data_l); 
        #endif
        for (count = 0; count < MAX_OVP_PER_MSG ; count ++)
        {
            /* If current element is smaller than first 
               then update both first and second */
            Ovp_value = ((Rx_Msg.Data_Msg[count].OVP_data_h * 256) + Rx_Msg.Data_Msg[count].OVP_data_l);
            if (Ovp_value < Ovp_value_1)
            {
              #if defined NOS_PSU_VREF_CONTROL == 2
                Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id2_Min = Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id1;
              #endif  
                Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id1_Min = count;
            }
            #if defined NOS_PSU_VREF_CONTROL == 2
            /* If value is in between first and second 
               then update second  */
            else if ((Ovp_value <  Ovp_value_2) && (Ovp_value != Ovp_value_1))
                Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id2_Min = count;
            #endif
        }
        // Set PSU to Avg voltage
        PSU_Tx_Msg.Write_Data_Hi = (unsigned char)(PSU_REF_VOLTAGE >> 8);
        PSU_Tx_Msg.Write_Data_Lo = (unsigned char)(PSU_REF_VOLTAGE & 0x00FF);
        //PSU_Tx_Msg.Alarm = PSU_Rx_Msg.Alarm;
        PSU_Tx_Msg.Alarm = 0; // no alram during config
        Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Set_Avg_Flag = TRUE;
      }
      else if (Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Set_Avg_Flag)
      {
        Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Set_Avg_Flag = FALSE;
        if (((Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id1_Min) == (Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id1_Max)) 
            #if defined NOS_PSU_VREF_CONTROL == 2
            & ((Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id2_Min) == (Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id2_Max))
            #endif
              )
        {
          // check if the message is close to setpoint and all the command responds to same OVP address
          unsigned int Ovp_value_1 = 0x0000;
              Ovp_value_1 = ((Rx_Msg.Data_Msg[Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id1_Min].OVP_data_h * 256) + Rx_Msg.Data_Msg[Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id1_Min].OVP_data_l);
          #if defined NOS_PSU_VREF_CONTROL == 2
          unsigned int Ovp_value_2 = 0x0000;
          Ovp_value_2 = ((Rx_Msg.Data_Msg[Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id2_Min].OVP_data_h * 256) + Rx_Msg.Data_Msg[Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id2_Min].OVP_data_l); 
          #endif
          // Register OVP Flag
          if (((TOWERTOP_SET_VOLTAGE - PSU_TOLERANCE_WINDOW) < Ovp_value_1 < (TOWERTOP_SET_VOLTAGE + PSU_TOLERANCE_WINDOW)) 
              #if defined NOS_PSU_VREF_CONTROL == 2
              & ((TP_SET_Voltage - PSU_TOLERANCE_WINDOW) < Ovp_value_2 < (TP_SET_Voltage + PSU_TOLERANCE_WINDOW))
              #endif  
             )
          {
            #if defined NOS_PSU_VREF_CONTROL == 1
            Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id1_Avg = Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id1_Min;
            //Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Configured_OVP_Flag = TRUE;
            Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_Device_Id = id;
            #elif defined NOS_PSU_VREF_CONTROL == 2
            Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id2_Avg = Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id2_Min;
            Previous_Rx_Msg.PSU_Register_Map[id][Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id2_Min].Msg_OVP_Id1_Avg = Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id1_Min;
            Previous_Rx_Msg.PSU_Register_Map[id][Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id2_Min].Msg_OVP_Id2_Avg = Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id2_Min;
            Previous_Rx_Msg.PSU_Register_Map[id][Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id2_Avg].Configured_OVP_Flag = TRUE;        
            Previous_Rx_Msg.PSU_Register_Map[id][Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id2_Avg].Msg_Device_Id = id;
            #endif      
          }
        }
        else
        {
         // reset all flags 
          
        }     
        // Set the PSU to Optimum power level one last time. 
        PSU_Tx_Msg.Write_Data_Hi = (unsigned char)(PSU_REF_VOLTAGE >> 8);
        PSU_Tx_Msg.Write_Data_Lo = (unsigned char)(PSU_REF_VOLTAGE & 0x00FF);
        //PSU_Tx_Msg.Alarm = PSU_Rx_Msg.Alarm;
        PSU_Tx_Msg.Alarm = 0; // no alram during config
        //Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Configured_OVP_Flag = TRUE;
        PSU_Config_Flag.Config_Busy_flag = FALSE;
        PSU_Config_Flag.Config_Busy_id = 0x00;
        PSU_Config_Flag.Config_Busy_ovp_id = 0x00;
      }
      else
      {
        // if the device configured first time
        Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id1_Max = 0;
        Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id1_Min = 0;
        Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id1_Avg = 0;
        #if defined NOS_PSU_VREF_CONTROL == 2
        Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id2_Max = 0;
        Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id2_Min = 0;
        Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Msg_OVP_Id2_Avg = 0;
        #endif
        Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Set_Max_Flag = TRUE;
        // Set PSU to Max voltage
        PSU_Tx_Msg.Write_Data_Hi = (unsigned char)(PSU_MAX_VOLTAGE >> 8);
        PSU_Tx_Msg.Write_Data_Lo = (unsigned char)(PSU_MAX_VOLTAGE & 0x00FF);
        //PSU_Tx_Msg.Alarm = PSU_Rx_Msg.Alarm;
        PSU_Tx_Msg.Alarm = 0; // no alram during config
        Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Set_Max_Flag = TRUE;
        PSU_Config_Flag.Config_Busy_flag = TRUE;
        PSU_Config_Flag.Config_Busy_id = id;
        PSU_Config_Flag.Config_Busy_ovp_id = ovp_id;
      }
      unsigned char channel; 
      if (MAX_CHANNEL_PER_BRICK == 2)
      {
        if (ovp_id & 0x01) 
        {
          ovp_id = ovp_id>>1;
        }
      }
      for (channel = 0; channel < MAX_CHANNEL_PER_BRICK; channel ++)
      {
        // check if the OVP message non zero else remove from registry
        //  OVP_readout = ((pRx_Msg->Data_Msg[ovp_id].OVP_data_h *256) + pRx_Msg->Data_Msg[ovp_id].OVP_data_l);
        PSU_Register_Address =  (PSU_REGISTER_START_ADDR + ((id * MAX_OVP_PER_MSG) + ovp_id)+channel);
        PSU_Tx_Msg.Reg_Addr_Lo = (unsigned char)(PSU_Register_Address & 0x00FF);
        PSU_Tx_Msg.Reg_Addr_Hi = (unsigned char)(PSU_Register_Address >> 8);  
      
        Error_Check = CRC16(&PSU_Tx_Msg, 6);
        PSU_Tx_Msg.Error_Check_Lo = (unsigned char)(Error_Check &0x00FF);
        PSU_Tx_Msg.Error_Check_Hi = (unsigned char)(Error_Check >> 8);
        status |= PSU_TX_RX_Data();
      }
    }    
    else
    {
      status = 0;
    }
  }
  return status;
}

unsigned char process_HeartBeat_Message()
{
  unsigned int Error_Check;
  // Prepare the message 
  // Send message to PSU of setpoint needs to be updated      
  PSU_Tx_Msg.Slave_Addr = PSU_SLAVE_ADDR;
  PSU_Tx_Msg.Function = PSU_FUNCTION_TYPE;
  // Prepare 2 byte data
  PSU_Tx_Msg.Write_Data_Hi = 0x00;
  PSU_Tx_Msg.Write_Data_Lo = 0x00;
  if(verify_Communication_Flag())
  {
    //PSU_Tx_Msg.Alarm = 0x01;
    PSU_Tx_Msg.Alarm = 0x00;
  }
  else
  {
    PSU_Tx_Msg.Alarm = 0x00;  
  }
  
  PSU_Tx_Msg.Reg_Addr_Lo = (unsigned char)(PSU_HEARTBEAT_START_ADDR & 0x00FF);
  PSU_Tx_Msg.Reg_Addr_Hi = (unsigned char)(PSU_HEARTBEAT_START_ADDR >> 8);
  Error_Check = CRC16(&PSU_Tx_Msg, 6);
  PSU_Tx_Msg.Error_Check_Lo = (unsigned char)(Error_Check &0x00FF);
  PSU_Tx_Msg.Error_Check_Hi = (unsigned char)(Error_Check >> 8); 
  return PSU_TX_RX_Data();
}

// Check if message received from new device or routine message received. 
void process_Message()
{
  unsigned char id = pRx_Msg->Device_Id;
  unsigned char ovp_id;
  // clear device id failure flag
  for (ovp_id = 0; ovp_id < MAX_OVP_PER_MSG ; ovp_id += MAX_CHANNEL_PER_BRICK)
  {
//    // if device_id and OVP configured
//    if (Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Configured_OVP_Flag == FALSE)
//    {
//////      if(Configure_OVP(id, ovp_id))
//////      {
//////        break;
//////      }
//      // Uncomment lines below to bypass configuration
//      unsigned char channel;
//      for (channel = 0; channel < MAX_CHANNEL_PER_BRICK; channel ++)
//      {
//        Previous_Rx_Msg.OVP_Raw_Data[id][ovp_id].OVP_data_h = (PSU_REF_VOLTAGE >> 8);
//        Previous_Rx_Msg.OVP_Raw_Data[id][ovp_id].OVP_data_l = (PSU_REF_VOLTAGE & 0xFF);
//        Previous_Rx_Msg.OVP_Raw_Data[id][ovp_id].OVP_Flag = 0x00; 
//        Previous_Rx_Msg.PSU_Register_Map[id][ovp_id].Configured_OVP_Flag == TRUE;
//      }
//      
//    }
//    else
//    {
       Process_OVP(id, ovp_id);
//    }
  }
    Rx_Msg_Flags.end_byte_rx = 0x00;
}