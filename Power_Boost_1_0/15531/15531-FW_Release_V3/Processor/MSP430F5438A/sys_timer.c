/***************************************************************************//**
 * FILE INCLUDES
 ******************************************************************************/
#include "sys_timer.h"

Timer_Flag Failure_Timer_5Sec = 
{
  MAX_TIMER_COUNT,
  0x0000,
  0x00,
};

Timer_Flag Heartbeat_Timer_Sec = 
{
  MAX_TIMER_COUNT,
  0x0000,
  0x00,
};

/***************************************************************************//**
 * FUNCTION DEFINATION
 ******************************************************************************/

/***************************************************************************//**
 * @param Function      Timer_A0_Configure
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  
 * @param Description   Configure timer A0 
 ******************************************************************************/
void Timer_A0_Configure() 
{
  TA0CTL |= MC_0;                           // Ensure the timer is stopped
  TA0CCTL1 = CCIE;                          // CCR1 interrupt enabled
  TA0CTL |= TASSEL_1 | TACLR;               // Run the timer of the ACLK + UP mode + Start up clean
  TA0CTL |= MC_1;                           // Timer Set to Up Mode
}

/***************************************************************************//**
 * @param Function      Timer_B0_Configure
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  
 * @param Description   Configure timer B0
 ******************************************************************************/
void Timer_B0_Configure() 
{
  TB0CTL |= MC_0;                           // Ensure the timer is stopped
  TB0CCTL1 = CCIE;                          // CCR1 interrupt enabled
  TB0CTL |= TBSSEL_1 | TBCLR;               // Run the timer of the ACLK + Start up clean
  TB0CTL |= MC_1;                           // Timer Set to Up Mode
}

/***************************************************************************//**
 * @param Function      Stop_Timer_A0
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  
 * @param Description   Stop timer A0
 ******************************************************************************/
void Stop_Timer_A0()
{
  TA0CTL = MC_0;
  // Alternate Method
  //TA0CTL = 0 ;
}

/***************************************************************************//**
 * @param Function      Stop_Timer_B0
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  
 * @param Description   Stop timer B0
 ******************************************************************************/
void Stop_Timer_B0()
{
  TB0CTL = MC_0;
  // Alternate Method
  //TB0CTL = 0 ;
}

/***************************************************************************//**
 * @param Function      Set_Timer_A0_CCR
 * @param Input         unsigned char, unsigned int
 * @param Return        void
 * @param Calling_Func  
 * @param Description   Load Timer CCR A0 value
 ******************************************************************************/
void Set_Timer_A0_CCR(unsigned char reg, unsigned int val) 
{
  /* Change corresponding register */
  switch (reg) {
  case 0:                                   // Capture/compare register 0
    TA0CCR0 = val;
    break;
  case 1:                                   // Capture/compare register 1
    TA0CCR1 = val;
    break;
  case 2:                                   // Capture/compare register 2
    TA0CCR2 = val;
    break;
  default: break;
  }
}

/***************************************************************************//**
 * @param Function      Set_Timer_B0_CCR
 * @param Input         unsigned char, unsigned int
 * @param Return        void
 * @param Calling_Func  
 * @param Description   Load Timer CCR B0 value
 ******************************************************************************/
void Set_Timer_B0_CCR(unsigned char reg, unsigned int val) 
{
  /* Change corresponding register */
  switch (reg) {
  case 0:                                   // Capture/compare register 0
    TB0CCR0 = val;
    break;
  case 1:                                   // Capture/compare register 1
    TB0CCR1 = val;
    break;
  case 2:                                   // Capture/compare register 2
    TB0CCR2 = val;
    break;
  default: break;
  }
}

/***************************************************************************//**
 * @param Function      Set_Timer_A0_Overflow_Counter
 * @param Input         unsigned short int
 * @param Return        void
 * @param Calling_Func  
 * @param Description   Load Timer A0 overflow value
 ******************************************************************************/
void Set_Timer_A0_Overflow_Counter(unsigned short int val) 
{
  TA0R = val;
}

/***************************************************************************//**
 * @param Function      Set_Timer_B0_Overflow_Counter
 * @param Input         unsigned short int
 * @param Return        void
 * @param Calling_Func  
 * @param Description   Load Timer B0 overflow value
 ******************************************************************************/
void Set_Timer_B0_Overflow_Counter(unsigned short int val) 
{
  TB0R = val;
}

/***************************************************************************//**
 * @param Function      Clear_Timer_Count 
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  
 * @param Description   Clear 5 Sec timer count
 ******************************************************************************/
void Clear_Timer_Count()
{
  Failure_Timer_5Sec.TimerCount = 0x0000;
  Failure_Timer_5Sec.Set_flag = 0x00;
}

/***************************************************************************//**
 * @param Function      Clear_Heartbeat_Count
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  
 * @param Description   Clear heartbeat monitor count
 ******************************************************************************/
void Clear_Heartbeat_Count()
{
  Heartbeat_Timer_Sec.TimerCount = 0x0000;
  Heartbeat_Timer_Sec.Set_flag = 0x00;
}

/***************************************************************************//**
 * @param Function      Verify_Heartbeat_Flag
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  
 * @param Description   Check if heartbeat timer flag set
 ******************************************************************************/
 unsigned char Verify_Heartbeat_Flag()
 {
   return Heartbeat_Timer_Sec.Set_flag; 
 }

 /***************************************************************************//**
 * @param Function      Verify_OVP_Communication_Flag
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  
 * @param Description   Check of OVP communication not received for 5sec
 ******************************************************************************/
 unsigned char Verify_OVP_Communication_Flag()
 {
   return Failure_Timer_5Sec.Set_flag; 
 }
 
/***************************************************************************//**
 * @param Function      Update_Timer_Count
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  
 * @param Description   Update timer count value and flags
 ******************************************************************************/
void Update_Timer_Count()
{
  // RC Failure Count
  Failure_Timer_5Sec.TimerCount++;
  if (Failure_Timer_5Sec.TimerCount > Max_Application_Count)
  {
      //Clear_Timer_Count();
      Failure_Timer_5Sec.Set_flag = 0x01;
      Change_Led(RED_LED, ON);
      Change_Led(YEL_LED, OFF);
      Change_Led(GRN_LED, OFF);
  }
  else
  {
      Change_Led(YEL_LED, TOGGLE);
      Change_Led(RED_LED, OFF);
  }
  // Heartbeat message
  Heartbeat_Timer_Sec.TimerCount++;
  if (Heartbeat_Timer_Sec.TimerCount > (2* Max_Application_Count)) // approx 10 sec
  {
    //Set heartbeat message flag
    Heartbeat_Timer_Sec.Set_flag = 0x01;
  }
  // else do nothing
}



/***************************************************************************//**
 * __interrupt void vTIMER0_A1_ISR(void)
 ******************************************************************************/
/***************************************************************************//**
 * @param Function      vTIMER0_A1_ISR 
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  
 * @param Description   Timer0 Interrupt service routine 
 ******************************************************************************/
// Timer_A1 Interrupt Vector (TAIV) handler
#pragma vector=TIMER0_INTA1
__interrupt void vTIMER0_A1_ISR(void)
{
  LPM0_EXIT;
  switch(__even_in_range(TA0IV,14))
   {
    case 0: break;
    case 2:                 // CCR1 not used
      
    Update_Timer_Count();
    break;
    case 4:  break;         // CCR2 not used
    case 6:  break;         // CCR3 not used
    case 8:  break;         // CCR4 not used
    case 10: break;         // CCR5 not used
    case 12: break;         // Reserved not used
    case 14: break;
    default: break;
  }
   // Execute 5.5Sec failure mode
}

/***************************************************************************//**
 * __interrupt void vTIMER0_B1_ISR(void)
 ******************************************************************************/
/***************************************************************************//**
 * @param Function      vTIMER2_A1_ISR
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  
 * @param Description   Timer2 Interrupt service routine 
 ******************************************************************************/
// Timer0_B1 Interrupt Vector (TAIV) handler
#pragma vector=TIMER0_INTB1
__interrupt void vTIMER2_A1_ISR(void)
{
  LPM0_EXIT;
  switch(__even_in_range(TB0IV,14))
  {
    case  0: break;                           // No interrupt
    case  2: break;                           // CCR1 not used 
    case  4: break;                           // CCR2 not used
    case  6: break;                           // reserved
    case  8: break;                           // reserved
    case 10: break;                           // reserved
    case 12: break;                           // reserved
    case 14: break;                           // overflow
    default: break;
  }
}

/***************************************************************************//**
 * @param Function      Init_Timer 
 * @param Input         void
 * @param Return        void
 * @param Calling_Func          
 * @param Description   Initialize Timer and  timer variables 
 ******************************************************************************/
void Init_Timer()
{
  Timer_A0_Configure();
  Timer_B0_Configure();
  Set_Timer_A0_CCR(0,MAX_TIMER_COUNT);
  Set_Timer_A0_CCR(1,MAX_TIMER_COUNT);
  Clear_Timer_Count(); 
  Change_Led(YEL_LED, ON);
  //Change_Led(RED_LED, OFF);
  //Change_Led(GRN_LED, OFF);
}

/***************************************************************************//**
 *                         End of Function
 ******************************************************************************/