#ifndef __MAIN
#define __MAIN

#include "sys_clk.h"
#include "sys_pmm.h"
#include "digital_io.h"
#include "uart.h"
#include "analog.h"


extern void init_Timer();
extern unsigned char Timer_Count_Expired();
extern void Clear_Timer_Count();
#endif
