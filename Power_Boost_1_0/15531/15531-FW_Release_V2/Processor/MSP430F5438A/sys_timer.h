/***************************************************************************//**
 *  \file         sys_timer.h
 *  \brief        Program include file and function declaration for system timer 
 *                setup and timer function
 *  \author       RBG
 *  \date         December,2017
 *  \version      1.0.0
 ******************************************************************************/
#ifndef __SYS_TIMER
#define __SYS_TIMER

/***************************************************************************//**
 * FILE INCLUDES
 ******************************************************************************/
#include "processor_Def.h"
   
#define MAX_TIMER_COUNT         40000                   // 10ms at 8M clock
#define DESIRED_DELAY_mSEC      1000
#define Max_Application_Count   ROUND(DESIRED_DELAY_mSEC/(SYS_CLOCK_FREQ_HZ / MAX_TIMER_COUNT))
   
/***************************************************************************//**
 * STRUCTURE DECLARATION
 ******************************************************************************/
/*! \struct s_Timer_Flag
    \brief Records timer count value and flags
*/
typedef struct s_Timer_Flag 
{
  unsigned short int Max_Counter_Val;
  unsigned short int TimerCount;
  unsigned char Set_flag;
}Timer_Flag;

 /***************************************************************************//**
 * INTERRUPT VECTOR DEFINATION
 ******************************************************************************/
#define TIMER0_INTA0   TIMER0_A0_VECTOR         
#define TIMER0_INTA1   TIMER0_A1_VECTOR  
   
#define TIMER0_INTB0   TIMER1_B0_VECTOR   
#define TIMER0_INTB1   TIMER0_B1_VECTOR 
   
#define TIMER1_INTA0   TIMER1_A0_VECTOR         
#define TIMER1_INTA1   TIMER1_A1_VECTOR
              
/***************************************************************************//**
 * EXTERNAL DELCLERATION
 ******************************************************************************/

/***************************************************************************//**
 * FUNCTION AND EXTERN FUNCTION DELCLERATION
 ******************************************************************************/
void Init_Timer();
void Stop_Timer_A0();
void Stop_Timer_B0();
void Clear_Timer_Count();
void Timer_A0_Configure(void);
void Timer_B0_Configure(void);
void Set_Timer_A0_CCR(unsigned char reg, unsigned int val);
void Set_Timer_A0_Overflow_Counter(unsigned short int val);
void Set_Timer_B0_CCR(unsigned char reg, unsigned int val);
void Set_Timer_B0_Overflow_Counter(unsigned short int val);
unsigned char Verify_Heartbeat_Flag();
unsigned char Verify_OVP_Communication_Flag();  

extern void Change_Led( unsigned char led_color, unsigned char state);
#endif 
