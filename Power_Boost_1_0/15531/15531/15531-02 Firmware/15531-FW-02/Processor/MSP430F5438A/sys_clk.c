/***************************************************************************//**
 * FILE INCLUDES
 ******************************************************************************/
#include "processor_Def.h"

/***************************************************************************//**
 * FUNCTION DEFINATION
 ******************************************************************************/
   /***************************************************************************//**
 * @param Function      
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  
 * @param Description   
 ******************************************************************************/
void Init_Sys_Clock()
{
  /* Select XIN,XOUT operation on P7.0,P7.1 */
//  SYS_XT1_PSEL |= SYS_XT1_XINSEL + SYS_XT1_XOUTSEL;         // Set XIN operation on P7.0
//  SYS_XT1_PSEL |= SYS_XT1_XOUTSEL;                      // Set XIN operation on P7.1,X-DNC is XIN PxSEL =1
   /* Set up Unifed Clock System (UCS) */
    UCSCTL0 = (SYS_DCO_TAP                                // Set DCO tap selection (modified during FLL operation) = 0
             | SYS_FLL_MOD_COUNTER);                    // Set modulation bit counter (modified during FLL operation) = 0

    UCSCTL1 = SYS_DCO_RANGE_SELECT;                       // Set DCO frequency range 
//  UCSCTL1 = (SYS_DCO_RANGE_SELECT                     // Set DCO frequency range 
//             | SYS_DISMOD) ;                          // Set enable/disable modulation
  
//  // Loop until XT1,XT2 & DCO stabilizes - In this case only DCO has to stabilize
//  do
//    {
//      UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + DCOFFG);
//      // Clear XT2,XT1,DCO fault flags
//      SFRIFG1 &= ~OFIFG;                      // Clear fault flags
//    }while (SFRIFG1&OFIFG);                   // Test oscillator fault flag
  
  // Loop until XT1 fault flag is cleared
  do
  {
    UCSCTL7 &= ~XT1LFOFFG;                  // Clear XT1 fault flags
  }while (UCSCTL7&XT1LFOFFG);               // Test XT1 fault flag

  __bis_SR_register(SCG0);                  // Disable the FLL control loop
  UCSCTL2 = (SYS_FLL_LOOPDIV                            // Set FLL loop divider
             | SYS_DCO_MULTIPLIER_BITS);                // Set DCO multiplier
  __bic_SR_register(SCG0);                  // Enable the FLL control loop
  
//  UCSCTL3 = (SYS_FLL_REFERENCE                          // Select reference for FLL
//            | SYS_FLL_REFDIV);                          // Select FLL Reference Divider
  UCSCTL3 = SYS_FLL_REFERENCE;                          // Select reference for FLL
  /* Select references for clock sources */
  UCSCTL4 = (SYS_ACLK_SOURCE                            // Set reference for ACLK
             | SYS_SMCLK_SOURCE                         // Set reference for SMCLK
             | SYS_MCLK_SOURCE);                        // Set reference for MCLK
  /* Set up UCS to use XIN/XOUT and external crystal */
  /* XT1 oscillator on board (32768Hz) */
  UCSCTL6 &= ~SYS_XT1_BYPASS;                           // Set XT1 bypass mode
  UCSCTL6 |= XT1DRIVE_2;                 // Xtal is now stable, reduce drive strength
  // TODO: Remove physical capacitor from board
  UCSCTL6 |= SYS_XT1_XCAP;                              // Set XIN/XOUT capacitor
  UCSCTL6 &= ~SYS_XT1_OFF;                              // Set high frequency oscillator 1 disable
  UCSCTL6 &= ~SYS_XT1_XTS;                              // Set select high frequency oscillator
  SFRIE1 |= OFIE;                                       // Set osc fault interrupt enable
  // Worst-case settling time for the DCO when the DCO range bits have been
  // changed is n x 32 x 32 x f_MCLK / f_FLL_reference. See UCS chapter in 5xx
  // UG for optimization.
  // 32 x 32 x 8 MHz / 32,768 Hz = 250000 = MCLK cycles for DCO to settle
  __delay_cycles(250000);
}

/***************************************************************************//**
 * @param Function      
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  
 * @param Description   
 ******************************************************************************/             
#pragma vector=UNMI_VECTOR
__interrupt void USER_NMI_ISR(void)
{
  if(UCSCTL7){                              // Any osc fault detected
    UCSCTL7 = 0;                            // Clear osc fault flag
    SFRIFG1 &= ~OFIFG;                      // Clear osc fault interrupt flag
  }
}

/***************************************************************************//**
 *                         End of Function
 ******************************************************************************/