#ifndef __DIGITAL_IO
#define __DIGITAL_IO

#include "processor_Def.h"



/***************************************************************************//**
 *                           GPIO CONFIGURATION
 ******************************************************************************/
/* SEL, DIR, OUT, IN, REN, IE, IES not defined for IO pins only defined for 
   SP function registers */
   
 /********************** PORT 1 Defination**********************
   P1.0 - TP8
   P1.1 - Buzzer PWM
   P1.2 - NC
   P1.3 - NC
   P1.4 - NC
   P1.5 - Fault Control 14
   P1.6 - Fault Led 14
   P1.7 - Fault Control 13
**************************************************************/     
#define TP8_BIT                 BIT0
   
#define Buzzer_SEL              P1SEL
#define Buzzer_DIR              P1DIR  

#define Buzzer_BIT              BIT1 
#define Fault_Ctrl_14_BIT       BIT5 
#define Fault_Led_14_BIT        BIT6
#define Fault_Ctrl_13_BIT       BIT7
   
/********************** PORT 2 Defination**********************
   P2.0 - Fault_Led_13_BIT
   P2.1 - Fault_Ctrl_12_BIT
   P2.2 - Fault_Led_12_BIT
   P2.3 - Fault_Ctrl_11_BIT
   P2.4 - Fault_Led_11_BIT
   P2.5 - Fault_Ctrl_10_BIT
   P2.6 - Fault_Led_10_BIT  
   P2.7 - Fault_Ctrl_9_BIT
**************************************************************/     
   
#define Fault_Led_13_BIT        BIT0
#define Fault_Ctrl_12_BIT       BIT1
#define Fault_Led_12_BIT        BIT2
#define Fault_Ctrl_11_BIT       BIT3
#define Fault_Led_11_BIT        BIT4
#define Fault_Ctrl_10_BIT       BIT5
#define Fault_Led_10_BIT        BIT6
#define Fault_Ctrl_9_BIT        BIT7
   
/********************** PORT 3 Defination**********************
   P3.0 - Fault_Led_9           
   P3.1 - SDA                           
   P3.2 - SCL                   
   P3.3 - RS485_INT             
   P3.4 - COM_TX                
   P3.5 - COM_RX                
   P3.6 - Fault_Ctrl_8_BIT       
   P3.7 - Fault_Led_8_BIT      
**************************************************************/     
#define Fault_Led_9_BIT         BIT0

#define PS_I2C_SEL              P3SEL
#define PS_I2C_DIR              P3DIR   

#define PS_SDA_BIT              BIT1        
#define PS_SCL_BIT              BIT2
#define RS485_INT_BIT           BIT3

#define COM_UART_SEL            P3SEL
#define COM_UART_DIR            P3DIR

#define COM_TX_BIT              BIT4
#define COM_RX_BIT              BIT5
   
#define Fault_Ctrl_8_BIT       BIT6  
#define Fault_Led_8_BIT        BIT7
   
/********************** PORT 4 Defination**********************
   P4.0 - Fault_Ctrl_7_BIT
   P4.1 - Fault_Led_7_BIT
   P4.2 - Fault_Ctrl_6_BIT
   P4.3 - Fault_Led_6_BIT
   P4.4 - Fault_Ctrl_5_BIT
   P4.5 - Fault_Led_5_BIT  
   P4.6 - Fault_Ctrl_4_BIT
   P4.7 - Fault_Led_4_BIT
**************************************************************/     
   
#define Fault_Ctrl_7_BIT        BIT0
#define Fault_Led_7_BIT         BIT1
#define Fault_Ctrl_6_BIT        BIT2
#define Fault_Led_6_BIT         BIT3
#define Fault_Ctrl_5_BIT        BIT4
#define Fault_Led_5_BIT         BIT5
#define Fault_Ctrl_4_BIT        BIT6
#define Fault_Led_4_BIT         BIT7
   
/********************** PORT 5 Defination**********************
   P5.0 - AIN8
   P5.1 - AIN9
   P5.2 - NC
   P5.3 - NC
   P5.4 - EXT_RX_LED
   P5.5 - EXT_TX_LED
   P5.6 - EXT_UART_TX
   P5.7 - EXT_UART_RX
**************************************************************/       
#define AIN8_BIT                BIT0
#define AIN9_BIT                BIT1

#define EXT_RX_LED_BIT          BIT4
#define EXT_TX_LED_BIT          BIT5

#define EXT_UART_SEL            P5SEL
#define EXT_UART_DIR            P5DIR

#define EXT_UART_TX_BIT         BIT6    
#define EXT_UART_RX_BIT         BIT7

   
/********************** PORT 6 Defination**********************
   P6.0 - AIN0
   P6.1 - AIN1
   P6.2 - AIN2
   P6.3 - AIN3
   P6.4 - AIN4
   P6.5 - AIN5
   P6.6 - AIN6
   P6.7 - AIN7
**************************************************************/     
#define AIN0_BIT                BIT0
#define AIN1_BIT                BIT1
#define AIN2_BIT                BIT2
#define AIN3_BIT                BIT3
#define AIN4_BIT                BIT4
#define AIN5_BIT                BIT5
#define AIN6_BIT                BIT6
#define AIN7_BIT                BIT7
   
/********************** PORT 7 Defination**********************
   P7.0 - XIN
   P7.1 - XOUT
   P7.2 - Fault_Ctrl_3
   P7.3 - Fault_Led_3
   P7.4 - AIN10
   P7.5 - AIN11
   P7.6 - AIN12 
   P7.7 - AIN13
**************************************************************/     
#define XIN_BIT                 BIT0
#define XOUT_BIT                BIT1
#define Fault_Ctrl_3_BIT        BIT2
#define Fault_Led_3_BIT         BIT3
#define AIN10_BIT               BIT4
#define AIN11_BIT               BIT5
#define AIN12_BIT               BIT6 
#define AIN13_BIT               BIT7
   
/********************** PORT 8 Defination**********************
   P8.0 - NC
   P8.1 - Fault_Ctrl_2
   P8.2 - Fault_Led_2
   P8.1 - Fault_Ctrl_1
   P8.2 - Fault_Led_1
   P8.5 - Mode_In_0
   P8.6 - Mode_In_1
   P8.7 - Mode_In_2
**************************************************************/      
#define Fault_Ctrl_2_BIT        BIT1
#define Fault_Led_2_BIT         BIT2
#define Fault_Ctrl_1_BIT        BIT3
#define Fault_Led_1_BIT         BIT4
#define Mode_In_0_BIT           BIT5
#define Mode_In_1_BIT           BIT6
#define Mode_In_2_BIT           BIT7
   
 /********************** PORT 9 Defination**********************
   P9.0 - NC
   P9.1 - AUX_SDA
   P9.2 - AUX_SCL
   P9.3 - EXT_SCLK
   P9.4 - EXT_MOSI
   P9.5 - EXT_MISO
   P9.6 - EXT_CS
   P9.7 - NC
**************************************************************/  
#define AUX_I2C_SEL             P9SEL
#define AUX_I2C_DIR             P9DIR

#define AUX_SDA_BIT             BIT1
#define AUX_SCL_BIT             BIT2

#define EXT_SPI_SEL            P9SEL
#define EXT_SPI_DIR            P9DIR

#define EXT_SCLK_BIT            BIT3
#define EXT_MOSI_BIT            BIT4
#define EXT_MISO_BIT            BIT5
#define EXT_CS_BIT              BIT6
     
/********************** PORT 10 Defination**********************
   P10.0 - NC
   P10.1 - DAC_SDA
   P10.2 - DAC_SCL
   P10.3 - NC
   P10.4 - DBG_TX
   P10.5 - DBG_RX
   P10.6 - DBG_LED_RX
   P10.7 - DBG_LED_TX
**************************************************************/     
//BIT0 - NC
#define DAC_I2C_SEL             P10SEL
#define DBG_I2C_DIR             P10DIR   

#define DAC_SDA_BIT             BIT1
#define DAC_SCL_BIT             BIT2
//BIT3 - NC

#define DBG_UART_SEL            P10SEL
#define DBG_UART_DIR            P10DIR

#define DBG_TX_BIT              BIT4
#define DBG_RX_BIT              BIT5
#define DBG_LED_RX_BIT          BIT6    
#define DBG_LED_TX_BIT          BIT7
   
/********************** PORT 11 Defination**********************
   P11.0 - Status_Led_3
   P11.1 - Status_Led_2
   P11.2 - Status_Led_1
**************************************************************/
#define Status_Led_POUT         P11OUT
#define Status_Led_3_BIT        BIT0
#define Status_Led_2_BIT        BIT1
#define Status_Led_1_BIT        BIT2
   
 /********************** PORT J Defination**********************
   PJ.0 - TDO
   PJ.1 - TDI
   PJ.2 - TMS
   PJ.3 - TCK

**************************************************************/     
// J port is defaulted tp programming and debugging so no need to configure   
   
   


void init_Hardware();
void Set_UART0_LED(unsigned char mode, unsigned char state);

void Change_Led( unsigned char led_color, unsigned char state); 
#endif