#ifndef __UART
#define __UART

#include "processor_Def.h"


#define Debug_RS485 0x01


/***************************************************************************//**
 *                           UART CONFIGURATION
 ******************************************************************************/
/***************************************************************************//**
 *                           UART CONFIGURATION
 ******************************************************************************/    
  
   
#define UART_UCA0_BAUDRATE             4800      // Operational at 2400 baud rate todo: Check why there is offset of 2
#define UART_UCA0_DATA_BITS            8         // 7, 8 data bits
#define UART_UCA0_PARITY_BITS          0         // none (0), odd (1), even (2) parity bits
#define UART_UCA0_STOP_BITS            1         // 1, 2 stop bits
#define UART_UCA0_LSB_MSB_FIRST        0         // lsb (0), msb (1) first
#define UART_UCA0_BYTES_BEFORE_TX      1         // bytes before transmission (Tx)

#define UART_UCA1_BAUDRATE             115200      // default baud rate
#define UART_UCA1_DATA_BITS            8         // 7, 8 data bits
#define UART_UCA1_PARITY_BITS          0         // none (0), odd (1), even (2) parity bits
#define UART_UCA1_STOP_BITS            1         // 1, 2 stop bits
#define UART_UCA1_LSB_MSB_FIRST        0         // lsb (0), msb (1) first
#define UART_UCA1_BYTES_BEFORE_TX      1         // bytes before transmission (Tx)

#define UART_UCA3_BAUDRATE             115200      // default baud rate
#define UART_UCA3_DATA_BITS            8         // 7, 8 data bits
#define UART_UCA3_PARITY_BITS          0         // none (0), odd (1), even (2) parity bits
#define UART_UCA3_STOP_BITS            1         // 1, 2 stop bits
#define UART_UCA3_LSB_MSB_FIRST        0         // lsb (0), msb (1) first
#define UART_UCA3_BYTES_BEFORE_TX      1         // bytes before transmission (Tx)



/***************************************************************************//**
 *                           UART UCA0 CONFIGURATION
 ******************************************************************************/
#ifndef UCXXRST                  
#define UCXXRST                               UCSWRST   
#endif

#define UART_UCA0_MODULATION                 1                     // enable (1), disable (0)
#define UART_UCA0_CLOCK_SOURCE               UCSSEL__SMCLK         // UCSSEL__SMCLK, UCSSEL__ACLK
#define UART_UCA0_CLOCK_FREQUENCY            SYSTEM_TRUE_CLOCK_HZ  // Calculated clock freq

/* Baudrate */
#define UART_UCA0_BAUDRATE_DIV_FACTOR        (UART_UCA0_CLOCK_FREQUENCY / UART_UCA0_BAUDRATE)  //
#define UART_UCA0_BAUDRATE_CTL_REG1          (UART_UCA0_BAUDRATE_DIV_FACTOR >> 8)         //
#define UART_UCA0_BAUDRATE_CTL_REG0          (UART_UCA0_BAUDRATE_DIV_FACTOR & 0xFF)       //

/* Modulation */
#define UART_UCA0_MODULATION_UCBRSX          ROUND(((((UART_UCA0_CLOCK_FREQUENCY * 1.0) / (UART_UCA0_BAUDRATE * 1.0)) - (UART_UCA0_BAUDRATE_DIV_FACTOR * 1.0)) * 8.0))  // Calculate low frequency modulation bit select
#define UART_UCA0_MODULATION_UCBRFX          0                     //
#define UART_UCA0_MODULATION_UCOS16          UCOS16                //

/* Data bits */
#if (UART_UCA0_DATA_BITS == 7)                                     // 7 data bits
#define UART_UCA0_DATA_BIT_SELECT            UC7BIT                //
#else                                                              // 8 data bits
#define UART_UCA0_DATA_BIT_SELECT            0                     //
#endif

/* Parity bits */
#if (UART_UCA0_PARITY_BITS == 0)                                  
#define UART_UCA0_PARITY_ENABLE              0                     // Parity Disable
#define UART_UCA0_PARITY_SELECT              0                     // No Parity 
#elif (UART_UCA0_PARITY_BITS == 1)                                  
#define UART_UCA0_PARITY_ENABLE              UCPEN                 // Parity Enable
#define UART_UCA0_PARITY_SELECT              0                     // Odd parity bit
#elif (UART_UCA0_PARITY_BITS == 2)                                  
#define UART_UCA0_PARITY_ENABLE              UCPEN                 // Parity Enable
#define UART_UCA0_PARITY_SELECT              UCPAR                 // Even parity bit
#endif

/* Stop bits */
#if (UART_UCA0_STOP_BITS == 1)                                     // 1 stop bit
#define UART_UCA0_STOP_BIT_SELECT            0                     //
#else                                                         // 2 stop bits
#define UART_UCA0_STOP_BIT_SELECT            UCSPB                 //
#endif

/* Flow control */
// TODO: Add defines for flow control

/* LSB, MSB first */
#if (UART_UCA0_LSB_MSB_FIRST == 1)                                 // MSB first
#define UART_UCA0_ENDIAN_BITS                UCMSB                 //
#else                                                           // LSB first
#define UART_UCA0_ENDIAN_BITS                0                     //
#endif  
   
#define UART_UCA0_MODE                       UCMODE_0            // UCMODE_0 - UART Mode
                                                                 // UCMODE_1 - idle line multipoint mode
                                                                 // UCMODE_2 - address bit multipoint mode
                                                                 // UCMODE_3 - UART mode with automatic baud-rate detect
  
#define Uart_UCA0_Tx_Buffer                        UCA0TXBUF
#define Uart_UCA0_Rx_Buffer                        UCA0RXBUF
   

/***************************************************************************//**
 *                           UART UCA1 CONFIGURATION
 ******************************************************************************/
/* UART : UCA0 CONFIGURATION */
#define UART_UCA1_MODULATION                 1                     // enable (1), disable (0)
#define UART_UCA1_CLOCK_SOURCE               UCSSEL__SMCLK         // UCSSEL__SMCLK, UCSSEL__ACLK
#define UART_UCA1_CLOCK_FREQUENCY            SYSTEM_TRUE_CLOCK_HZ  // Calculated clock freq

/* Baudrate */
#define UART_UCA1_BAUDRATE_DIV_FACTOR        (UART_UCA1_CLOCK_FREQUENCY / UART_UCA1_BAUDRATE)  //
#define UART_UCA1_BAUDRATE_CTL_REG1          (UART_UCA1_BAUDRATE_DIV_FACTOR >> 8)         //
#define UART_UCA1_BAUDRATE_CTL_REG0          (UART_UCA1_BAUDRATE_DIV_FACTOR & 0xFF)       //

/* Modulation */
#define UART_UCA1_MODULATION_UCBRSX          ROUND(((((UART_UCA1_CLOCK_FREQUENCY * 1.0) / (UART_UCA1_BAUDRATE * 1.0)) - (UART_UCA1_BAUDRATE_DIV_FACTOR * 1.0)) * 8.0))  // Calculate low frequency modulation bit select
#define UART_UCA1_MODULATION_UCBRFX          0                     //
#define UART_UCA1_MODULATION_UCOS16          UCOS16                //

/* Data bits */
#if (UART_UCA1_DATA_BITS == 7)                                     // 7 data bits
#define UART_UCA1_DATA_BIT_SELECT            UC7BIT                //
#else                                                              // 8 data bits
#define UART_UCA1_DATA_BIT_SELECT            0                     //
#endif

/* Parity bits */
#if (UART_UCA1_PARITY_BITS == 0)                                   
#define UART_UCA1_PARITY_ENABLE              0                     // Parity Disable
#define UART_UCA1_PARITY_SELECT              0                     // No Parity
#elif (UART_UCA1_PARITY_BITS == 1)                                  
#define UART_UCA1_PARITY_ENABLE              UCPEN                 // Parity Enable
#define UART_UCA1_PARITY_SELECT              0                     // Odd parity bit
#elif (UART_UCA1_PARITY_BITS == 2)                                  
#define UART_UCA1_PARITY_ENABLE              UCPEN                 // Parity Enable
#define UART_UCA1_PARITY_SELECT              UCPAR                 // Even parity bit
#endif

/* Stop bits */
#if (UART_UCA1_STOP_BITS == 1)                                     // 1 stop bit
#define UART_UCA1_STOP_BIT_SELECT            0                     //
#else                                                         // 2 stop bits
#define UART_UCA1_STOP_BIT_SELECT            UCSPB                 //
#endif

/* Flow control */
// TODO: Add defines for flow control

/* LSB, MSB first */
#if (UART_UCA1_LSB_MSB_FIRST == 1)                                 // MSB first
#define UART_UCA1_ENDIAN_BITS                UCMSB                 //
#else                                                         // LSB first
#define UART_UCA1_ENDIAN_BITS                0                     //
#endif   

#define UART_UCA1_MODE                       UCMODE_0            // UCMODE_0 - UART Mode
                                                                 // UCMODE_1 - idle line multipoint mode
                                                                 // UCMODE_2 - address bit multipoint mode
                                                                 // UCMODE_3 - UART mode with automatic baud-rate detect
#define Uart_UCA1_Tx_Buffer                        UCA1TXBUF
#define Uart_UCA1_Rx_Buffer                        UCA1RXBUF
   
/***************************************************************************//**
 *                           UART UCA3 CONFIGURATION
 ******************************************************************************/
#define UART_UCA3_MODULATION                 1                     // enable (1), disable (0)
#define UART_UCA3_CLOCK_SOURCE               UCSSEL__SMCLK         // UCSSEL__SMCLK, UCSSEL__ACLK
#define UART_UCA3_CLOCK_FREQUENCY            SYSTEM_TRUE_CLOCK_HZ  // Calculated clock freq

/* Baudrate */
#define UART_UCA3_BAUDRATE_DIV_FACTOR        (UART_UCA3_CLOCK_FREQUENCY / UART_UCA3_BAUDRATE)  //
#define UART_UCA3_BAUDRATE_CTL_REG1          (UART_UCA3_BAUDRATE_DIV_FACTOR >> 8)         //
#define UART_UCA3_BAUDRATE_CTL_REG0          (UART_UCA3_BAUDRATE_DIV_FACTOR & 0xFF)       //

/* Modulation */
#define UART_UCA3_MODULATION_UCBRSX          ROUND(((((UART_UCA3_CLOCK_FREQUENCY * 1.0) / (UART_UCA3_BAUDRATE * 1.0)) - (UART_UCA3_BAUDRATE_DIV_FACTOR * 1.0)) * 8.0))  // Calculate low frequency modulation bit select
#define UART_UCA3_MODULATION_UCBRFX          0                  //
#define UART_UCA3_MODULATION_UCOS16          UCOS16             //

/* Data bits */
#if (UART_UCA3_DATA_BITS == 7)                                  // 7 data bits
#define UART_UCA3_DATA_BIT_SELECT            UC7BIT             //
#else                                                           // 8 data bits
#define UART_UCA3_DATA_BIT_SELECT            0                  //
#endif

/* Parity bits */
#if (UART_UCA3_PARITY_BITS == 0)                                   
#define UART_UCA3_PARITY_ENABLE              0                  // Parity Disable
#define UART_UCA3_PARITY_SELECT              0                  // No parity bit
#elif (UART_UCA3_PARITY_BITS == 1)                                  
#define UART_UCA3_PARITY_ENABLE              UCPEN              // Parity Enable
#define UART_UCA3_PARITY_SELECT              0                  // Odd parity bit
#elif (UART_UCA3_PARITY_BITS == 2)                                 
#define UART_UCA3_PARITY_ENABLE              UCPEN              // Parity Enable
#define UART_UCA3_PARITY_SELECT              UCPAR              // Even parity bit
#endif

/* Stop bits */
#if (UART_UCA3_STOP_BITS == 1)                                  // 1 stop bit
#define UART_UCA3_STOP_BIT_SELECT            0                  //
#else                                                           // 2 stop bits
#define UART_UCA3_STOP_BIT_SELECT            UCSPB              //
#endif

/* Flow control */
// TODO: Add defines for flow control

/* LSB, MSB first */
#if (UART_UCA3_LSB_MSB_FIRST == 1)                              // MSB first
#define UART_UCA3_ENDIAN_BITS                UCMSB              //
#else                                                           // LSB first
#define UART_UCA3_ENDIAN_BITS                0                  //
#endif 
   
#define UART_UCA3_MODE                       UCMODE_0            // UCMODE_0 - UART Mode
                                                                 // UCMODE_1 - idle line multipoint mode
                                                                 // UCMODE_2 - address bit multipoint mode
                                                                 // UCMODE_3 - UART mode with automatic baud-rate detect
  
#define Uart_UCA3_Tx_Buffer                  UCA3TXBUF
#define Uart_UCA3_Rx_Buffer                  UCA3RXBUF
     
 /***************************************************************************//**
 *                      INTERRUPT VECTOR DEFINATION
 ******************************************************************************/
/* Interrupt Vectors */
#define UART_0    USCI_A0_VECTOR          // UART interrupt vector
#define UART_1    USCI_A1_VECTOR          // UART interrupt vector
#define UART_3    USCI_A3_VECTOR          // UART interrupt vector  
   

unsigned char init_UartAx(unsigned char uart_ucaX);
unsigned char check_Uart_Int_Flag(unsigned char uart_ucaX, unsigned char flag);
void clear_Uart_Int_Flag(unsigned char uart_ucaX, unsigned char flag);
char get_Uart_Rx_Data(unsigned char uart_ucaX); 
void set_Uart_Tx_Data(unsigned char uart_ucaX, char data); 
unsigned char Uart_Tx_Busy(unsigned char uart_ucaX);
extern void Set_UART0_LED(unsigned char mode, unsigned char state);
//extern void process_Rx_data(char rx_data);

#endif