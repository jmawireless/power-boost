/***************************************************************************//**
 *  \file         pmm.c (Power Management Module)
 *  \brief        Hardware dependant function for Power Management settings
 *  \author       RBG
 *  \date         September,2011
 *  \version      1.0.0
 ******************************************************************************/
/***************************************************************************//**
 *                           FILE INCLUDES
 ******************************************************************************/
#include "sys_pmm.h"

/***************************************************************************//**
 *                           VARIABLE DECLARATION
 ******************************************************************************/
#define _HAL_PMM_DISABLE_SVML_
#define _HAL_PMM_DISABLE_SVSL_
#define _HAL_PMM_DISABLE_FULL_PERFORMANCE_

//****************************************************************************//
#ifdef _HAL_PMM_DISABLE_SVML_
#define _HAL_PMM_SVMLE SVMLE
#else
#define _HAL_PMM_SVMLE 0
#endif
#ifdef _HAL_PMM_DISABLE_SVSL_
#define _HAL_PMM_SVSLE SVSLE
#else
#define _HAL_PMM_SVSLE 0
#endif
#ifdef _HAL_PMM_DISABLE_FULL_PERFORMANCE_
#define _HAL_PMM_SVSFP SVSLFP
#else
#define _HAL_PMM_SVSFP 0
#endif

/***************************************************************************//**
 *                           FUNCTION DEFINATION
 ******************************************************************************/
/***************************************************************************//**
 * @param Function      unsigned short int SetVCore (unsigned char)
 * @param Input         unsigned char     level     Set new power level
 * @param Return        unsigned short int    status    Return current status value
 * @param Calling_Func  pmm.c (only)
 * @param Description   This hardware dependant function used to set VCore power level
 ******************************************************************************/
unsigned short int SetVCore (unsigned char level)
{
  unsigned short int actlevel;
  unsigned short int status = 0;
  level &= PMMCOREV_3;                       // Set Mask for Max. level
  actlevel = (PMMCTL0 & PMMCOREV_3);         // Get actual VCore

  while (((level != actlevel) && (status == 0)) || (level < actlevel))		// step by step increase or decrease
  {
    if (level > actlevel)
      status = SetVCoreUp(++actlevel);
    else
      status = SetVCoreDown(--actlevel);
  }
  return status;
}

/***************************************************************************//**
 * @param Function      unsigned short int SetVCoreUp (unsigned char)
 * @param Input         unsigned char     level           Set new power level
 * @param Return        unsigned short int    status          Returns Success or Failure
 * @param Calling_Func  pmm.c (only)
 * @param Description   This hardware dependant function used to initialize and
 *                configure VCORE power to increase level
 ******************************************************************************/
unsigned short int SetVCoreUp (unsigned char level)
{
  unsigned short int PMMRIE_backup,SVSMHCTL_backup;

  // Open PMM registers for write access
  PMMCTL0_H = 0xA5;

  // Disable dedicated Interrupts to prevent that needed flags will be cleared
  PMMRIE_backup = PMMRIE;
  PMMRIE &= ~(SVSMHDLYIE | SVSMLDLYIE | SVMLVLRIE | SVMHVLRIE | SVMHVLRPE);
  // Set SVM highside to new level and check if a VCore increase is possible
  SVSMHCTL_backup = SVSMHCTL;
  PMMIFG &= ~(SVMHIFG | SVSMHDLYIFG);
  SVSMHCTL = SVMHE | SVMHFP | (SVSMHRRL0 * level);
  // Wait until SVM highside is settled
  while ((PMMIFG & SVSMHDLYIFG) == 0);
  // Disable full-performance mode to save energy
  SVSMHCTL &= ~_HAL_PMM_SVSFP ;
  // Check if a VCore increase is possible
  if ((PMMIFG & SVMHIFG) == SVMHIFG){			//-> Vcc is to low for a Vcore increase
  	// recover the previous settings
  	PMMIFG &= ~SVSMHDLYIFG;
  	SVSMHCTL = SVSMHCTL_backup;
  	// Wait until SVM highside is settled
  	while ((PMMIFG & SVSMHDLYIFG) == 0);
  	// Clear all Flags
  	PMMIFG &= ~(SVMHVLRIFG | SVMHIFG | SVSMHDLYIFG | SVMLVLRIFG | SVMLIFG | SVSMLDLYIFG);
  	// backup PMM-Interrupt-Register
  	PMMRIE = PMMRIE_backup;
  	
  	// Lock PMM registers for write access
  	PMMCTL0_H = 0x00;
  	return PMM_STATUS_ERROR;                       // return: voltage not set
  }
  // Set also SVS highside to new level			//-> Vcc is high enough for a Vcore increase
  SVSMHCTL |= SVSHE | (SVSHRVL0 * level);
  // Set SVM low side to new level
  SVSMLCTL = SVMLE | SVMLFP | (SVSMLRRL0 * level);
  // Wait until SVM low side is settled
  while ((PMMIFG & SVSMLDLYIFG) == 0);
  // Clear already set flags
  PMMIFG &= ~(SVMLVLRIFG | SVMLIFG);
  // Set VCore to new level
  PMMCTL0_L = PMMCOREV0 * level;
  // Wait until new level reached
  if (PMMIFG & SVMLIFG)
  while ((PMMIFG & SVMLVLRIFG) == 0);
  // Set also SVS/SVM low side to new level
  PMMIFG &= ~SVSMLDLYIFG;
  SVSMLCTL |= SVSLE | (SVSLRVL0 * level);
  // wait for lowside delay flags
  while ((PMMIFG & SVSMLDLYIFG) == 0);

// Disable SVS/SVM Low
// Disable full-performance mode to save energy
  SVSMLCTL &= ~(_HAL_PMM_DISABLE_SVSL_+_HAL_PMM_DISABLE_SVML_+_HAL_PMM_SVSFP );

  // Clear all Flags
  PMMIFG &= ~(SVMHVLRIFG | SVMHIFG | SVSMHDLYIFG | SVMLVLRIFG | SVMLIFG | SVSMLDLYIFG);
  // backup PMM-Interrupt-Register
  PMMRIE = PMMRIE_backup;

  // Lock PMM registers for write access
  PMMCTL0_H = 0x00;
  return PMM_STATUS_OK;                               // return: OK
}

/***************************************************************************//**
 * @param Function      unsigned short int SetVCoreDown (unsigned char level)
 * @param Input         unsigned char     level           Set new power level
 * @param Return        unsigned short int    status          Returns Success or Failure
 * @param Calling_Func  pmm.c (only)
 * @param Description   This hardware dependant function used to initialize and
 *                configure VCORE power to decrease level
 ******************************************************************************/
unsigned short int SetVCoreDown (unsigned char level)
{
  unsigned short int PMMRIE_backup;

  // Open PMM registers for write access
  PMMCTL0_H = 0xA5;

  // Disable dedicated Interrupts to prevent that needed flags will be cleared
  PMMRIE_backup = PMMRIE;
  PMMRIE &= ~(SVSMHDLYIE | SVSMLDLYIE | SVMLVLRIE | SVMHVLRIE | SVMHVLRPE);

  // Set SVM high side and SVM low side to new level
  PMMIFG &= ~(SVMHIFG | SVSMHDLYIFG | SVMLIFG | SVSMLDLYIFG);
  SVSMHCTL = SVMHE | SVMHFP | (SVSMHRRL0 * level);
  SVSMLCTL = SVMLE | SVMLFP | (SVSMLRRL0 * level);
  // Wait until SVM high side and SVM low side is settled
  while ((PMMIFG & SVSMHDLYIFG) == 0 || (PMMIFG & SVSMLDLYIFG) == 0);

  // Set VCore to new level
  PMMCTL0_L = PMMCOREV0 * level;

  // Set also SVS highside and SVS low side to new level
  PMMIFG &= ~(SVSHIFG | SVSMHDLYIFG | SVSLIFG | SVSMLDLYIFG);
  SVSMHCTL |= SVSHE | SVSHFP | (SVSHRVL0 * level);
  SVSMLCTL |= SVSLE | SVSLFP | (SVSLRVL0 * level);
  // Wait until SVS high side and SVS low side is settled
  while ((PMMIFG & SVSMHDLYIFG) == 0 || (PMMIFG & SVSMLDLYIFG) == 0);
  // Disable full-performance mode to save energy
  SVSMHCTL &= ~_HAL_PMM_SVSFP;
// Disable SVS/SVM Low
// Disable full-performance mode to save energy
  SVSMLCTL &= ~(_HAL_PMM_DISABLE_SVSL_+_HAL_PMM_DISABLE_SVML_+_HAL_PMM_SVSFP );
	
  // Clear all Flags
  PMMIFG &= ~(SVMHVLRIFG | SVMHIFG | SVSMHDLYIFG | SVMLVLRIFG | SVMLIFG | SVSMLDLYIFG);
  // backup PMM-Interrupt-Register
  PMMRIE = PMMRIE_backup;
  // Lock PMM registers for write access
  PMMCTL0_H = 0x00;

  if ((PMMIFG & SVMHIFG) == SVMHIFG)
    return PMM_STATUS_ERROR;					 	// Highside is still to low for the adjusted VCore Level
  else return PMM_STATUS_OK;						// Return: OK
}


void Stop_WatchDog_Timer(){
  WDTCTL = WDTPW + WDTHOLD;
}

void Software_Reset(){
 WDTCTL = WDT_MRST_0_064;    // Perform software reset in 0.064ms
}


//#pragma vector=RESET_VECTOR
//__interrupt void RESET_ISR(void)
//{
//
//__no_operation();
//}

#pragma vector=SYSNMI_VECTOR
__interrupt void SYSNMI_ISR(void)
{
__no_operation();
// write current state to FLASH


}
/***************************************************************************//**
 *                         End of Function
 ******************************************************************************/
