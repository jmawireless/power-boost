#include "protocol.h"

#define OVP_High_Bound 680
#define OVP_Low_Bound 440


Power_Boost_Msg Rx_Msg = 
{
  0x00,         // start_byte
  0x00,         // msg_length
  0x00,         // OVP1_data_h
  0x00,         // OVP1_data_l
  0x00,         // OVP2_data_h
  0x00,         // OVP2_data_l
  0x00,         // OVP3_data_h
  0x00,         // OVP3_data_l
  0x00,         // OVP4_data_h
  0x00,         // OVP4_data_l
  0x00,         // OVP5_data_h
  0x00,         // OVP5_data_l
  0x00,         // OVP6_data_h
  0x00,         // OVP6_data_l
  0x00,         // OVP7_data_h
  0x00,         // OVP7_data_l
  0x00,         // OVP8_data_h
  0x00,         // OVP8_data_l
  0x00,         // OVP9_data_h
  0x00,         // OVP9_data_l
  0x00,         // OVP10_data_h
  0x00,         // OVP10_data_l
  0x00,         // OVP11_data_h
  0x00,         // OVP11_data_l
  0x00,         // OVP12_data_h
  0x00,         // OVP12_data_l
  0x00          // msg_checksum
};

Previous_Msg Previous_Rx_Msg = 
{
  OVP_Low_Bound,         // OVP1_data
  OVP_Low_Bound,         // OVP2_data
  OVP_Low_Bound,         // OVP3_data
  OVP_Low_Bound,         // OVP4_data
  OVP_Low_Bound,         // OVP5_data
  OVP_Low_Bound,         // OVP6_data
  OVP_Low_Bound,         // OVP7_data
  OVP_Low_Bound,         // OVP8_data
  OVP_Low_Bound,         // OVP9_data
  OVP_Low_Bound,         // OVP10_data
  OVP_Low_Bound,         // OVP11_data
  OVP_Low_Bound,         // OVP12_data
};

 Power_Boost_Flag Rx_Msg_Flags =
{
  0x00, 
  0x00,
};

 void clear_RxBufferAndCount()
 {
    // Reset all timers, flags and rx buffers
    RxBuffer_Count = 0x00;
    unsigned char i;
    for (i= 0; i < sizeof(Power_Boost_Msg); i++)
    {
     RxBuffer[i] = 0x00;
    } 
 }
 
 void clear_Power_Boost_Flags()
 {
    // Reset all timers, flags and rx buffers
    RxBuffer_Count = 0x00;
    unsigned char i;
    for (i= 0; i < sizeof(Power_Boost_Msg); i++)
    {
      memcpy(&Rx_Msg, NULL, sizeof(Rx_Msg));
    } 
 }
  
  unsigned char Verify_Msg_CRC(Power_Boost_Msg* Msg)
 {
   unsigned char i;
   unsigned char Cal_Crc = 0x00;
   
   for (i=0; i < (sizeof(Power_Boost_Msg)-1); i++)
   {
     //Cal_Crc = (unsigned char)(Cal_Crc + Msg[i].start_byte);
     //todo: Update the CRC logic 
     Cal_Crc = Msg->msg_checksum;
   }
   if (Cal_Crc != Msg->msg_checksum)
   {
     return 0x00;
   }
   else
   {
     return 0x01;
   }
 }
 
/***************************************************************************//**
 * @param Function      
 * @param Input         
 * @param Return        
 * @param Calling_Func  
 * @param Description   
 ******************************************************************************/
void process_Rx_data(char rx_data)
 {
   // Check if start of data received 
   if ((rx_data == Protocol_Start_Byte) & (Rx_Msg_Flags.start_byte_rx != 0x01))
   {
     // Start of message received
     clear_RxBufferAndCount();
     Rx_Msg_Flags.end_byte_rx = 0x00;
     Rx_Msg_Flags.start_byte_rx = 0x01;
     // Store data 
     RxBuffer[RxBuffer_Count] = rx_data;
     Clear_Timer_Count();
   }
   // Message a part of original message begin received
   else
   {
     if (Rx_Msg_Flags.start_byte_rx == 0x01)
     {
       RxBuffer_Count++;
       RxBuffer[RxBuffer_Count] = rx_data; 
       if ((RxBuffer_Count >= sizeof(Rx_Msg) -1) || (((RxBuffer[1]*256) + RxBuffer[2])  == (RxBuffer_Count+1)))
       {
         memcpy(&Rx_Msg, RxBuffer, RxBuffer_Count);
         Rx_Msg.msg_checksum = RxBuffer[RxBuffer_Count];
         clear_RxBufferAndCount();
         if (Verify_Msg_CRC(&Rx_Msg) !=0) 
         {
           // Valid CRC
            Rx_Msg_Flags.end_byte_rx = 0x01;
            Rx_Msg_Flags.start_byte_rx = 0x00;
         }
         else 
         {
            // Invalid CRC
            Rx_Msg_Flags.end_byte_rx = 0x00;
            Rx_Msg_Flags.start_byte_rx = 0x00;
         }
       }
     }
     else // Out of line message byte received    
     {
        // Fault message  
        clear_RxBufferAndCount();
     }
   }
 }
 

 

 unsigned char verify_Msg_End_Flag_Rx()
 {
   return Rx_Msg_Flags.end_byte_rx;
 }

 
 unsigned char Control_OVP1()
 {
   unsigned int OVP1_data = ((Rx_Msg.OVP1_data_h *256) + (Rx_Msg.OVP1_data_l));
   if (OVP1_data == 0)
   {
     return 0;
   }
   if ((OVP1_data <= OVP_High_Bound) && (OVP1_data >= OVP_Low_Bound))
   {
    // Measure OVP1
    if (OVP1_data == Previous_Rx_Msg.OVP1_data)
    {
        __no_operation();
    }
    else 
    {
      if (OVP1_data > Previous_Rx_Msg.OVP1_data) 
      {
        //// Send message to DAC to decrease voltage
        // unsigned short int Diff_Voltage2Decrease;
        // Diff_Voltage2Decrease = OVP1_data -Previous_Rx_Msg.OVP1_data);
        // Decrease_OVP_Vge(1,Diff_Voltage2Decrease);
        __no_operation();
      }
      else
      {
        //// Send message to DAC to increase voltage
        // unsigned short int Diff_Voltage2Decrease;
        // Diff_Voltage2Increase = Previous_Rx_Msg.OVP1_data - OVP1_data);
        // Increase_OVP_Vge(1,Diff_Voltage2Increase);
        __no_operation();
      }
    } 
    Previous_Rx_Msg.OVP1_data = OVP1_data;
    Set_Fault_LED(1, OFF);
    return 0;
   }
   else // Out of bound
   {
     if (OVP1_data > OVP_High_Bound)
     {
       Previous_Rx_Msg.OVP1_data =  OVP_High_Bound;  // for storage and comparision
     }
     else if (OVP1_data < OVP_Low_Bound)
     {
      Previous_Rx_Msg.OVP1_data = OVP_Low_Bound;    // for storage and comparision
     }
     Set_Fault_LED(1, ON);
     return 1;
   }
 }
 
 
  unsigned char Control_OVP2()
 {
   unsigned int OVP2_data = ((Rx_Msg.OVP2_data_h *256) + (Rx_Msg.OVP2_data_l));
   if (OVP2_data == 0)
   {
     return 0;
   }
   if ((OVP2_data <= OVP_High_Bound) && (OVP2_data >= OVP_Low_Bound))
   {
    // Measure OVP2
    if (OVP2_data == Previous_Rx_Msg.OVP2_data)
    {
        __no_operation();
    }
    else 
    {
      if (OVP2_data > Previous_Rx_Msg.OVP2_data) 
      {
        //// Send message to DAC to decrease voltage
        // unsigned short int Diff_Voltage2Decrease;
        // Diff_Voltage2Decrease = OVP2_data -Previous_Rx_Msg.OVP2_data);
        // Decrease_OVP_Vge(1,Diff_Voltage2Decrease);
        __no_operation();
      }
      else
      {
        //// Send message to DAC to increase voltage
        // unsigned short int Diff_Voltage2Decrease;
        // Diff_Voltage2Increase = Previous_Rx_Msg.OVP2_data - OVP2_data);
        // Increase_OVP_Vge(1,Diff_Voltage2Increase);
        __no_operation();
      }
    } 
    Previous_Rx_Msg.OVP2_data = OVP2_data;
    Set_Fault_LED(2, OFF);
    return 0;
   }
   else // Out of bound
   {
     if (OVP2_data > OVP_High_Bound)
     {
       Previous_Rx_Msg.OVP2_data =  OVP_High_Bound;  // for storage and comparision
     }
     else if (OVP2_data < OVP_Low_Bound)
     {
      Previous_Rx_Msg.OVP2_data = OVP_Low_Bound;    // for storage and comparision
     }
     Set_Fault_LED(2, ON);
     return 1;
   }
 }
 
  unsigned char Control_OVP3()
 {
   unsigned int OVP3_data = ((Rx_Msg.OVP3_data_h *256) + (Rx_Msg.OVP3_data_l));
   if (OVP3_data == 0)
   {
     return 0;
   }
   if ((OVP3_data <= OVP_High_Bound) && (OVP3_data >= OVP_Low_Bound))
   {
    // Measure OVP3
    if (OVP3_data == Previous_Rx_Msg.OVP3_data)
    {
        __no_operation();
    }
    else 
    {
      if (OVP3_data > Previous_Rx_Msg.OVP3_data) 
      {
        //// Send message to DAC to decrease voltage
        // unsigned short int Diff_Voltage2Decrease;
        // Diff_Voltage2Decrease = OVP3_data -Previous_Rx_Msg.OVP3_data);
        // Decrease_OVP_Vge(1,Diff_Voltage2Decrease);
        __no_operation();
      }
      else
      {
        //// Send message to DAC to increase voltage
        // unsigned short int Diff_Voltage2Decrease;
        // Diff_Voltage2Increase = Previous_Rx_Msg.OVP3_data - OVP3_data);
        // Increase_OVP_Vge(1,Diff_Voltage2Increase);
        __no_operation();
      }
    } 
    Previous_Rx_Msg.OVP3_data = OVP3_data;
    Set_Fault_LED(3, OFF);
    return 0;
   }
   else // Out of bound
   {
     if (OVP3_data > OVP_High_Bound)
     {
       Previous_Rx_Msg.OVP3_data =  OVP_High_Bound;  // for storage and comparision
     }
     else if (OVP3_data < OVP_Low_Bound)
     {
      Previous_Rx_Msg.OVP3_data = OVP_Low_Bound;    // for storage and comparision
     }
     Set_Fault_LED(3, ON);
     return 1;
   }
 }
 
  unsigned char Control_OVP4()
 {
   unsigned int OVP4_data = ((Rx_Msg.OVP4_data_h *256) + (Rx_Msg.OVP4_data_l));
   if (OVP4_data == 0)
   {
     return 0;
   }
   if ((OVP4_data <= OVP_High_Bound) && (OVP4_data >= OVP_Low_Bound))
   {
    // Measure OVP4
    if (OVP4_data == Previous_Rx_Msg.OVP4_data)
    {
        __no_operation();
    }
    else 
    {
      if (OVP4_data > Previous_Rx_Msg.OVP4_data) 
      {
        //// Send message to DAC to decrease voltage
        // unsigned short int Diff_Voltage2Decrease;
        // Diff_Voltage2Decrease = OVP4_data -Previous_Rx_Msg.OVP4_data);
        // Decrease_OVP_Vge(1,Diff_Voltage2Decrease);
        __no_operation();
      }
      else
      {
        //// Send message to DAC to increase voltage
        // unsigned short int Diff_Voltage2Decrease;
        // Diff_Voltage2Increase = Previous_Rx_Msg.OVP4_data - OVP4_data);
        // Increase_OVP_Vge(1,Diff_Voltage2Increase);
        __no_operation();
      }
    } 
    Previous_Rx_Msg.OVP4_data = OVP4_data;
    Set_Fault_LED(4, OFF);
    return 0;
   }
   else // Out of bound
   {
     if (OVP4_data > OVP_High_Bound)
     {
       Previous_Rx_Msg.OVP4_data =  OVP_High_Bound;  // for storage and comparision
     }
     else if (OVP4_data < OVP_Low_Bound)
     {
      Previous_Rx_Msg.OVP4_data = OVP_Low_Bound;    // for storage and comparision
     }
     Set_Fault_LED(4, ON);
     return 1;
   }
 }
 
  unsigned char Control_OVP5()
 {
   unsigned int OVP5_data = ((Rx_Msg.OVP5_data_h *256) + (Rx_Msg.OVP5_data_l));
   if (OVP5_data == 0)
   {
     return 0;
   }
   if ((OVP5_data <= OVP_High_Bound) && (OVP5_data >= OVP_Low_Bound))
   {
    // Measure OVP5
    if (OVP5_data == Previous_Rx_Msg.OVP5_data)
    {
        __no_operation();
    }
    else 
    {
      if (OVP5_data > Previous_Rx_Msg.OVP5_data) 
      {
        //// Send message to DAC to decrease voltage
        // unsigned short int Diff_Voltage2Decrease;
        // Diff_Voltage2Decrease = OVP5_data -Previous_Rx_Msg.OVP5_data);
        // Decrease_OVP_Vge(1,Diff_Voltage2Decrease);
        __no_operation();
      }
      else
      {
        //// Send message to DAC to increase voltage
        // unsigned short int Diff_Voltage2Decrease;
        // Diff_Voltage2Increase = Previous_Rx_Msg.OVP5_data - OVP5_data);
        // Increase_OVP_Vge(1,Diff_Voltage2Increase);
        __no_operation();
      }
    } 
    Previous_Rx_Msg.OVP5_data = OVP5_data;
    Set_Fault_LED(5, OFF);
    return 0;
   }
   else // Out of bound
   {
     if (OVP5_data > OVP_High_Bound)
     {
       Previous_Rx_Msg.OVP5_data =  OVP_High_Bound;  // for storage and comparision
     }
     else if (OVP5_data < OVP_Low_Bound)
     {
      Previous_Rx_Msg.OVP5_data = OVP_Low_Bound;    // for storage and comparision
     }
     Set_Fault_LED(5, ON);
     return 1;
   }
 }
 
  unsigned char Control_OVP6()
 {
   unsigned int OVP6_data = ((Rx_Msg.OVP6_data_h *256) + (Rx_Msg.OVP6_data_l));
   if (OVP6_data == 0)
   {
     return 0;
   }
   if ((OVP6_data <= OVP_High_Bound) && (OVP6_data >= OVP_Low_Bound))
   {
    // Measure OVP6
    if (OVP6_data == Previous_Rx_Msg.OVP6_data)
    {
        __no_operation();
    }
    else 
    {
      if (OVP6_data > Previous_Rx_Msg.OVP6_data) 
      {
        //// Send message to DAC to decrease voltage
        // unsigned short int Diff_Voltage2Decrease;
        // Diff_Voltage2Decrease = OVP6_data -Previous_Rx_Msg.OVP6_data);
        // Decrease_OVP_Vge(1,Diff_Voltage2Decrease);
        __no_operation();
      }
      else
      {
        //// Send message to DAC to increase voltage
        // unsigned short int Diff_Voltage2Decrease;
        // Diff_Voltage2Increase = Previous_Rx_Msg.OVP6_data - OVP6_data);
        // Increase_OVP_Vge(1,Diff_Voltage2Increase);
        __no_operation();
      }
    } 
    Previous_Rx_Msg.OVP6_data = OVP6_data;
    Set_Fault_LED(6, OFF);
    return 0;
   }
   else // Out of bound
   {
     if (OVP6_data > OVP_High_Bound)
     {
       Previous_Rx_Msg.OVP6_data =  OVP_High_Bound;  // for storage and comparision
     }
     else if (OVP6_data < OVP_Low_Bound)
     {
      Previous_Rx_Msg.OVP6_data = OVP_Low_Bound;    // for storage and comparision
     }
     Set_Fault_LED(6, ON);
     return 1;
   }
 }
 
  unsigned char Control_OVP7()
 {
   unsigned int OVP7_data = ((Rx_Msg.OVP7_data_h *256) + (Rx_Msg.OVP7_data_l));
   if (OVP7_data == 0)
   {
     return 0;
   }
   if ((OVP7_data <= OVP_High_Bound) && (OVP7_data >= OVP_Low_Bound))
   {
    // Measure OVP7
    if (OVP7_data == Previous_Rx_Msg.OVP7_data)
    {
        __no_operation();
    }
    else 
    {
      if (OVP7_data > Previous_Rx_Msg.OVP7_data) 
      {
        //// Send message to DAC to decrease voltage
        // unsigned short int Diff_Voltage2Decrease;
        // Diff_Voltage2Decrease = OVP7_data -Previous_Rx_Msg.OVP7_data);
        // Decrease_OVP_Vge(1,Diff_Voltage2Decrease);
        __no_operation();
      }
      else
      {
        //// Send message to DAC to increase voltage
        // unsigned short int Diff_Voltage2Decrease;
        // Diff_Voltage2Increase = Previous_Rx_Msg.OVP7_data - OVP7_data);
        // Increase_OVP_Vge(1,Diff_Voltage2Increase);
        __no_operation();
      }
    } 
    Previous_Rx_Msg.OVP7_data = OVP7_data;
    Set_Fault_LED(7, OFF);
    return 0;
   }
   else // Out of bound
   {
     if (OVP7_data > OVP_High_Bound)
     {
       Previous_Rx_Msg.OVP7_data =  OVP_High_Bound;  // for storage and comparision
     }
     else if (OVP7_data < OVP_Low_Bound)
     {
      Previous_Rx_Msg.OVP7_data = OVP_Low_Bound;    // for storage and comparision
     }
     Set_Fault_LED(7, ON);
     return 1;
   }
 }
 
  unsigned char Control_OVP8()
 {
   unsigned int OVP8_data = ((Rx_Msg.OVP8_data_h *256) + (Rx_Msg.OVP8_data_l));
   if (OVP8_data == 0)
   {
     return 0;
   }
   if ((OVP8_data <= OVP_High_Bound) && (OVP8_data >= OVP_Low_Bound))
   {
    // Measure OVP8
    if (OVP8_data == Previous_Rx_Msg.OVP8_data)
    {
        __no_operation();
    }
    else 
    {
      if (OVP8_data > Previous_Rx_Msg.OVP8_data) 
      {
        //// Send message to DAC to decrease voltage
        // unsigned short int Diff_Voltage2Decrease;
        // Diff_Voltage2Decrease = OVP8_data -Previous_Rx_Msg.OVP8_data);
        // Decrease_OVP_Vge(1,Diff_Voltage2Decrease);
        __no_operation();
      }
      else
      {
        //// Send message to DAC to increase voltage
        // unsigned short int Diff_Voltage2Decrease;
        // Diff_Voltage2Increase = Previous_Rx_Msg.OVP8_data - OVP8_data);
        // Increase_OVP_Vge(1,Diff_Voltage2Increase);
        __no_operation();
      }
    } 
    Previous_Rx_Msg.OVP8_data = OVP8_data;
    Set_Fault_LED(8, OFF);
    return 0;
   }
   else // Out of bound
   {
     if (OVP8_data > OVP_High_Bound)
     {
       Previous_Rx_Msg.OVP8_data =  OVP_High_Bound;  // for storage and comparision
     }
     else if (OVP8_data < OVP_Low_Bound)
     {
      Previous_Rx_Msg.OVP8_data = OVP_Low_Bound;    // for storage and comparision
     }
     Set_Fault_LED(8, ON);
     return 1;
   }
 }
 
  unsigned char Control_OVP9()
 {
   unsigned int OVP9_data = ((Rx_Msg.OVP9_data_h *256) + (Rx_Msg.OVP9_data_l));
   if (OVP9_data == 0)
   {
     return 0;
   }
   if ((OVP9_data <= OVP_High_Bound) && (OVP9_data >= OVP_Low_Bound))
   {
    // Measure OVP9
    if (OVP9_data == Previous_Rx_Msg.OVP9_data)
    {
        __no_operation();
    }
    else 
    {
      if (OVP9_data > Previous_Rx_Msg.OVP9_data) 
      {
        //// Send message to DAC to decrease voltage
        // unsigned short int Diff_Voltage2Decrease;
        // Diff_Voltage2Decrease = OVP9_data -Previous_Rx_Msg.OVP9_data);
        // Decrease_OVP_Vge(1,Diff_Voltage2Decrease);
        __no_operation();
      }
      else
      {
        //// Send message to DAC to increase voltage
        // unsigned short int Diff_Voltage2Decrease;
        // Diff_Voltage2Increase = Previous_Rx_Msg.OVP9_data - OVP9_data);
        // Increase_OVP_Vge(1,Diff_Voltage2Increase);
        __no_operation();
      }
    } 
    Previous_Rx_Msg.OVP9_data = OVP9_data;
    Set_Fault_LED(9, OFF);
    return 0;
   }
   else // Out of bound
   {
     if (OVP9_data > OVP_High_Bound)
     {
       Previous_Rx_Msg.OVP9_data =  OVP_High_Bound;  // for storage and comparision
     }
     else if (OVP9_data < OVP_Low_Bound)
     {
      Previous_Rx_Msg.OVP9_data = OVP_Low_Bound;    // for storage and comparision
     }
     Set_Fault_LED(9, ON);
     return 1;
   }
 }
 
  unsigned char Control_OVP10()
 {
   unsigned int OVP10_data = ((Rx_Msg.OVP10_data_h *256) + (Rx_Msg.OVP10_data_l));
   if (OVP10_data == 0)
   {
     return 0;
   }
   if ((OVP10_data <= OVP_High_Bound) && (OVP10_data >= OVP_Low_Bound))
   {
    // Measure OVP10
    if (OVP10_data == Previous_Rx_Msg.OVP10_data)
    {
        __no_operation();
    }
    else 
    {
      if (OVP10_data > Previous_Rx_Msg.OVP10_data) 
      {
        //// Send message to DAC to decrease voltage
        // unsigned short int Diff_Voltage2Decrease;
        // Diff_Voltage2Decrease = OVP10_data -Previous_Rx_Msg.OVP10_data);
        // Decrease_OVP_Vge(1,Diff_Voltage2Decrease);
        __no_operation();
      }
      else
      {
        //// Send message to DAC to increase voltage
        // unsigned short int Diff_Voltage2Decrease;
        // Diff_Voltage2Increase = Previous_Rx_Msg.OVP10_data - OVP10_data);
        // Increase_OVP_Vge(1,Diff_Voltage2Increase);
        __no_operation();
      }
    } 
    Previous_Rx_Msg.OVP10_data = OVP10_data;
    Set_Fault_LED(10, OFF);
    return 0;
   }
   else // Out of bound
   {
     if (OVP10_data > OVP_High_Bound)
     {
       Previous_Rx_Msg.OVP10_data =  OVP_High_Bound;  // for storage and comparision
     }
     else if (OVP10_data < OVP_Low_Bound)
     {
      Previous_Rx_Msg.OVP10_data = OVP_Low_Bound;    // for storage and comparision
     }
     Set_Fault_LED(10, ON);
     return 1;
   }
 }
 
  unsigned char Control_OVP11()
 {
   unsigned int OVP11_data = ((Rx_Msg.OVP11_data_h *256) + (Rx_Msg.OVP11_data_l));
   if (OVP11_data == 0)
   {
     return 0;
   }
   if ((OVP11_data <= OVP_High_Bound) && (OVP11_data >= OVP_Low_Bound))
   {
    // Measure OVP11
    if (OVP11_data == Previous_Rx_Msg.OVP11_data)
    {
        __no_operation();
    }
    else 
    {
      if (OVP11_data > Previous_Rx_Msg.OVP11_data) 
      {
        //// Send message to DAC to decrease voltage
        // unsigned short int Diff_Voltage2Decrease;
        // Diff_Voltage2Decrease = OVP11_data -Previous_Rx_Msg.OVP11_data);
        // Decrease_OVP_Vge(1,Diff_Voltage2Decrease);
        __no_operation();
      }
      else
      {
        //// Send message to DAC to increase voltage
        // unsigned short int Diff_Voltage2Decrease;
        // Diff_Voltage2Increase = Previous_Rx_Msg.OVP11_data - OVP11_data);
        // Increase_OVP_Vge(1,Diff_Voltage2Increase);
        __no_operation();
      }
    } 
    Previous_Rx_Msg.OVP11_data = OVP11_data;
    Set_Fault_LED(11, OFF);
    return 0;
   }
   else // Out of bound
   {
     if (OVP11_data > OVP_High_Bound)
     {
       Previous_Rx_Msg.OVP11_data =  OVP_High_Bound;  // for storage and comparision
     }
     else if (OVP11_data < OVP_Low_Bound)
     {
      Previous_Rx_Msg.OVP11_data = OVP_Low_Bound;    // for storage and comparision
     }
     Set_Fault_LED(11, ON);
     return 1;
   }
 }
 
  unsigned char Control_OVP12()
 {
   unsigned int OVP12_data = ((Rx_Msg.OVP12_data_h *256) + (Rx_Msg.OVP12_data_l));
   if (OVP12_data == 0)
   {
     return 0;
   }
   if ((OVP12_data <= OVP_High_Bound) && (OVP12_data >= OVP_Low_Bound))
   {
    // Measure OVP12
    if (OVP12_data == Previous_Rx_Msg.OVP12_data)
    {
        __no_operation();
    }
    else 
    {
      if (OVP12_data > Previous_Rx_Msg.OVP12_data) 
      {
        //// Send message to DAC to decrease voltage
        // unsigned short int Diff_Voltage2Decrease;
        // Diff_Voltage2Decrease = OVP12_data -Previous_Rx_Msg.OVP12_data);
        // Decrease_OVP_Vge(1,Diff_Voltage2Decrease);
        __no_operation();
      }
      else
      {
        //// Send message to DAC to increase voltage
        // unsigned short int Diff_Voltage2Decrease;
        // Diff_Voltage2Increase = Previous_Rx_Msg.OVP12_data - OVP12_data);
        // Increase_OVP_Vge(1,Diff_Voltage2Increase);
        __no_operation();
      }
    } 
    Previous_Rx_Msg.OVP12_data = OVP12_data;
    Set_Fault_LED(12, OFF);
    return 0;
   }
   else // Out of bound
   {
     if (OVP12_data > OVP_High_Bound)
     {
       Previous_Rx_Msg.OVP12_data =  OVP_High_Bound;  // for storage and comparision
     }
     else if (OVP12_data < OVP_Low_Bound)
     {
      Previous_Rx_Msg.OVP12_data = OVP_Low_Bound;    // for storage and comparision
     }
     Set_Fault_LED(12, ON);
     return 1;
   }
 }
 
void process_Message()
{
  unsigned char OVP_Status = 0;
  Change_Led(Yellow_Status_Led, ON); 
  OVP_Status |= Control_OVP1();
  OVP_Status |= Control_OVP2();
  OVP_Status |= Control_OVP3();
  OVP_Status |= Control_OVP4();
  OVP_Status |= Control_OVP5();
  OVP_Status |= Control_OVP6();
  OVP_Status |= Control_OVP7();
  OVP_Status |= Control_OVP8();
  OVP_Status |= Control_OVP9();
  OVP_Status |= Control_OVP10();
  OVP_Status |= Control_OVP11();
  OVP_Status |= Control_OVP12();
  
  // Clear received message flag
  Rx_Msg_Flags.end_byte_rx = 0x00;
  Rx_Msg_Flags.start_byte_rx = 0x00;
  
  if (OVP_Status > 0)
  {
    Change_Led( Red_Status_Led, ON); 
  }
  else
  {
    Change_Led( Green_Status_Led, ON); 
  }
  Change_Led( Yellow_Status_Led, OFF); 
}