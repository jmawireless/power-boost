/***************************************************************************//**
* FILE INCLUDES
*******************************************************************************/
#include "main.h" 
//#define DEBUG_MAIN
#if defined (DEBUG_MAIN)
  #include "stdio.h"
#endif

/***************************************************************************//**
 * FUNCTION DEFINATION
 ******************************************************************************/
/***************************************************************************//**
 * @param Function      Init_PBC_Comm_Peripheral
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  main.c
 * @param Description   This hardware dependant function used to transmit and receive
                        date from UART
 ******************************************************************************/
void Init_PBC_Comm_Peripheral()
 {
   // Initialize UART<->RS485 Delta 
   Init_UartAx(0);                      
 }

/***************************************************************************//**
 * @param Function      Init_Comm_Peripheral
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  main.c
 * @param Description   This hardware dependant function used to initialize UART
                        3rd party surge protection device
 ******************************************************************************/
 void Init_Comm_Peripheral()
 {
   // Initialize UART<-RS485 from RC
   Init_UartAx(2);               
 }

/***************************************************************************//**
 * @param Function      Init_Debug_peripheral
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  Application.c
 * @param Description   This hardware dependant function used to initialize UART
                        for 3rd party surge protection devices and PBC controller
 ******************************************************************************/
void Init_Debug_peripheral()
{
  // Initialize UART Delta debug 
  Init_UartAx(3);              
  // Initialize UART RC Debug    
  Init_UartAx(1);             
}

/***************************************************************************//**
 * @param Function      main
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  main.c
 * @param Description   This is primary function of the protocol. This function 
                        will initialize hardware, software parameters and run in
                        loop scanning for message from 3rd party surge protection 
                        device and process commands to PBC controller
 ******************************************************************************/
void main(void)
{
  // Stop watchdog timer to prevent time out reset
  Stop_WatchDog_Timer();        
  #if defined (DEBUG_MAIN)      
    printf("WDT DISABLED\n");
  #endif
  // Disable Global Interrupts
  __disable_interrupt();        
    #if defined (DEBUG_MAIN)
    printf("INT DISABLED\n");
  #endif
  // Set Core voltage   
  SetVCore(2);                  
  #if defined (DEBUG_MAIN)
  printf("PWR UP\n");
  #endif
  // Initialize hardware IO to default/known state
  Init_Hardware();              
  #if defined (DEBUG_MAIN)
    printf("HW IO INIT\n");
  #endif  
  // Initialize system clocks
  Init_Sys_Clock();             
    #if defined (DEBUG_MAIN)
    printf("CLOCK SETUP\n");
  #endif
  // Setup debug port
  Init_Debug_peripheral();      
  // Send hardware informaiton
  Send_UART_Debug_Info();        
  // Set up peripherials UART 
  Init_PBC_Comm_Peripheral();   
  #if defined (DEBUG_MAIN)
    printf("PHER COMM INIT\n");
  #endif
  // Enable Global interrupts
  __enable_interrupt();         
  #if defined (DEBUG_MAIN)
    printf("INT ENABLED\n");
  #endif
  /* Ping the PBC controller/sec for 10min until the PBC controller is up 
    Initialized and ready to process commands */
  unsigned int startup_delay;
  for(startup_delay = 0; startup_delay < 600; startup_delay++)
  {
    // Check if heartbeat message response received
    if(Process_PBC_HeartBeat_Message() == FALSE)
    {
      __delay_cycles(8000000);
    }
    else
    {
      break;
    }
  }
  // Reset all previous OVP data register 
  Setup_OVP_Register();   
  // PBC Setup 
  Setup_PBC();
 
  //Set up peripherials UART
  Init_Comm_Peripheral();   
  // Initialize timer 
  Init_Timer();                 
  #if defined (DEBUG_MAIN)
    printf("TIMER SETUP\n");
  #endif
  // enter infinite loop, code will process any incoming commands from Surge protection device/
  while(1)
  {
    // Verify if end of message flag set
    unsigned char Rx_Message_Flag = Verify_OVP_Msg_End_Flag_Rx(); 
    unsigned char Heartbeat_Flag = Verify_Heartbeat_Flag();
    // Check if the Rx Message is Valid
    if (Rx_Message_Flag == 1) 
    {
      Process_OVP_Message();
    }
    else if (Heartbeat_Flag == 1)
    {
      Process_PBC_HeartBeat_Message();
    }
    // else if invalid or incomplete message received 
    else 
    {
      // LPM0;
      // No operation
      __no_operation();
    }
  }
}
/***************************************************************************//**
 *                         End of Function
 ******************************************************************************/