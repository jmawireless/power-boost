#include "debug_uart.h"
#define UART_DEBUG

#if defined (UART_DEBUG)
#include "stdio.h"
#endif

#define DEBUG_DELAY_CYCLE_COUNT 100

#if (DEBUG_FLAG == ON)

void init_DebugUart()
{
  init_UartAx(3);      // Debug Uart 
}

void Send_Msg(unsigned char *data, unsigned char length){
  unsigned char execution_count;
  unsigned char execution_flag = FALSE;
  for(execution_count = 0; execution_count < 10; execution_count ++){
    if(!(UCA1IFG & UCTXIFG)){
      execution_flag = TRUE;
      break;
    }
    else{
      __delay_cycles(DEBUG_DELAY_CYCLE_COUNT);
    }
  }
  
  if(execution_flag == TRUE)
  {
    unsigned char count;
    for(count = 0; count < length; count ++)
    {
      set_Uart_Tx_Data(DEBUG_UART,*data);
      *data++; 
    }
  }
  else
  {
    #if defined (UART_DEBUG)
      printf("Debug UART UCTXIFG Flag issue");
    #endif 
  }
}

void Receive_Msg(){
  
}
 
#endif
