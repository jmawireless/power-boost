#include "uart.h"
#include "protocol.h"

unsigned char init_UartAx(unsigned char uart_ucaX)
{
  switch(uart_ucaX)
  {
  case 0: 
    /* Set up USCI UART A0 mode */
    UCA0CTL1 |= UCXXRST;                                // Hold USCI in reset state
    UCA0CTL1 |= UART_UCA0_CLOCK_SOURCE;                 // Select clock source [7]

    UCA0CTL0 |= (UART_UCA0_PARITY_ENABLE                // Set parity enable bit [7]
                 | UART_UCA0_PARITY_SELECT);            // Set parity select bit [6]
    UCA0CTL0 |= UART_UCA0_ENDIAN_BITS;                  // Set MSB first select [5]
    UCA0CTL0 |= UART_UCA0_DATA_BIT_SELECT;              // Set character length [4]
    UCA0CTL0 |= UART_UCA0_STOP_BIT_SELECT;              // Set stop bit select [3]
    UCA0CTL0 |= UART_UCA0_MODE;                         // Set UART mode [2:1]

    UCA0BR0 = UART_UCA0_BAUDRATE_CTL_REG0;              // Set baudrate low byte [7:0]
    UCA0BR1 = UART_UCA0_BAUDRATE_CTL_REG1;              // Set baudrate high byte [7:0]

    UCA0MCTL |= UART_UCA0_MODULATION_UCBRFX;            // Set first modulation stage select [7:4]
    UCA0MCTL |= UART_UCA0_MODULATION_UCBRSX;            // Set second modulation stage select [3:1]
 
    UCA0MCTL &= ~UART_UCA0_MODULATION_UCOS16;           // Set oversampling mode enable [0]

    UCA0CTL1 &= ~UCXXRST;                               // Release USCI reset

    UCA0IFG = 0x00;                                     // Clear Tx [1] and Rx [0] flags
    UCA0IE |= (UCRXIE | UCTXIE);                        // Enable Tx [1] and Rx [0] interrupts
    break;
  case 1: 
    /* Set up USCI UART A0 mode */
    UCA1CTL1 |= UCXXRST;                                // Hold USCI in reset state
    UCA1CTL1 |= UART_UCA1_CLOCK_SOURCE;                 // Select clock source [7]

    UCA1CTL0 |= (UART_UCA1_PARITY_ENABLE                // Set parity enable bit [7]
                 | UART_UCA1_PARITY_SELECT);            // Set parity select bit [6]
    UCA1CTL0 |= UART_UCA1_ENDIAN_BITS;                  // Set MSB first select [5]
    UCA1CTL0 |= UART_UCA1_DATA_BIT_SELECT;              // Set character length [4]
    UCA1CTL0 |= UART_UCA1_STOP_BIT_SELECT;              // Set stop bit select [3]
    UCA1CTL0 |= UART_UCA1_MODE;                         // Set UART mode [2:1]

    UCA1BR0 = UART_UCA1_BAUDRATE_CTL_REG0;              // Set baudrate low byte [7:0]
    UCA1BR1 = UART_UCA1_BAUDRATE_CTL_REG1;              // Set baudrate high byte [7:0]

    UCA1MCTL |= UART_UCA1_MODULATION_UCBRFX;            // Set first modulation stage select [7:4]
    UCA1MCTL |= UART_UCA1_MODULATION_UCBRSX;            // Set second modulation stage select [3:1]
 
    UCA1MCTL &= ~UART_UCA1_MODULATION_UCOS16;           // Set oversampling mode enable [0]

    UCA1CTL1 &= ~UCXXRST;                               // Release USCI reset

    UCA1IFG = 0x00;                                     // Clear Tx [1] and Rx [0] flags
    UCA1IE |= (UCRXIE | UCTXIE);                        // Enable Tx [1] and Rx [0] interrupts
    break;
  case 2: 
    //Add code if necessary
    __no_operation();
    break;
    
  case 3: 
    /* Set up USCI UART A0 mode */
    UCA3CTL1 |= UCXXRST;                                // Hold USCI in reset state
    UCA3CTL1 |= UART_UCA3_CLOCK_SOURCE;                 // Select clock source [7]

    UCA3CTL0 |= (UART_UCA3_PARITY_ENABLE                // Set parity enable bit [7]
                 | UART_UCA3_PARITY_SELECT);            // Set parity select bit [6]
    UCA3CTL0 |= UART_UCA3_ENDIAN_BITS;                  // Set MSB first select [5]
    UCA3CTL0 |= UART_UCA3_DATA_BIT_SELECT;              // Set character length [4]
    UCA3CTL0 |= UART_UCA3_STOP_BIT_SELECT;              // Set stop bit select [3]
    UCA3CTL0 |= UART_UCA3_MODE;                         // Set UART mode [2:1]

    UCA3BR0 = UART_UCA3_BAUDRATE_CTL_REG0;              // Set baudrate low byte [7:0]
    UCA3BR1 = UART_UCA3_BAUDRATE_CTL_REG1;              // Set baudrate high byte [7:0]

    UCA3MCTL |= UART_UCA3_MODULATION_UCBRFX;            // Set first modulation stage select [7:4]
    UCA3MCTL |= UART_UCA3_MODULATION_UCBRSX;            // Set second modulation stage select [3:1]
 
    UCA3MCTL &= ~UART_UCA3_MODULATION_UCOS16;           // Set oversampling mode enable [0]

    UCA3CTL1 &= ~UCXXRST;                               // Release USCI reset

    UCA3IFG = 0x00;                                     // Clear Tx [1] and Rx [0] flags
    UCA3IE |= (UCRXIE | UCTXIE);                        // Enable Tx [1] and Rx [0] interrupts
    break;
  default: 
    //Add code if necessary
    __no_operation();
    break;
  } 
  return 0;
}

/***************************************************************************//**
 
 ******************************************************************************/
unsigned char check_Uart_Int_Flag(unsigned char uart_ucaX, unsigned char flag)
{
  unsigned short int bval = 0;
  switch (uart_ucaX)
  {
  case 0: 
    switch (flag) 
    {
    case 0:                                     // Receive (Rx) flag
      if (UCA0IFG & UCRXIFG) { bval = 1; }      // Check interrupt flag
      break;
    case 1:                                     // Transmit (Tx) flag
      if (UCA0IFG & UCTXIFG) { bval = 1; }      // Check interrupt flag
      break;
    }  
    break;
  case 1: 
    switch (flag) 
    {
    case 0:                                     // Receive (Rx) flag
      if (UCA1IFG & UCRXIFG) { bval = 1; }      // Check interrupt flag
      break;
    case 1:                                     // Transmit (Tx) flag
      if (UCA1IFG & UCTXIFG) { bval = 1; }      // Check interrupt flag
      break;
    }
    break;
  case 3: 
    switch (flag) 
    {
    case 0:                                     // Receive (Rx) flag
      if (UCA3IFG & UCRXIFG) { bval = 1; }      // Check interrupt flag
      break;
    case 1:                                     // Transmit (Tx) flag
      if (UCA3IFG & UCTXIFG) { bval = 1; }      // Check interrupt flag
      break;
    }
    break;
  default:
    __no_operation();                           // todo: add exception if any
    break;  
  }
 return bval;
}

/***************************************************************************//**
 
 ******************************************************************************/
void clear_Uart_Int_Flag(unsigned char uart_ucaX, unsigned char flag)
{
  switch (uart_ucaX)
  {
    
  case 0:
    switch (flag) 
    {
    case 0:                                 // Receive (Rx) flag
      UCA0IFG &= ~UCRXIFG;                  // Clear interrupt flag
      break;
    case 1:                                 // Transmit (Tx) flag
      UCA0IFG &= ~UCTXIFG;                  // Clear interrupt flag
      break;
    } 
    break;
    
  case 1: 
    switch (flag) 
    {
    case 0:                                 // Receive (Rx) flag
      UCA1IFG &= ~UCRXIFG;                  // Clear interrupt flag
      break;
    case 1:                                 // Transmit (Tx) flag
      UCA1IFG &= ~UCTXIFG;                  // Clear interrupt flag
      break;
    } 
    break;
    
  case 3: 
    switch (flag) 
    {
    case 0:                                 // Receive (Rx) flag
      UCA3IFG &= ~UCRXIFG;                  // Clear interrupt flag
      break;
    case 1:                                 // Transmit (Tx) flag
      UCA3IFG &= ~UCTXIFG;                  // Clear interrupt flag
      break;
    } 
    break;
    
  default:
    __no_operation();                           // todo: add exception if any
    break;  
  }
}

char get_Uart_Rx_Data(unsigned char uart_ucaX) 
{ 
  switch(uart_ucaX)
  {
  case 0: 
    return UCA0RXBUF;  
    break;
    
  case 1: 
    return UCA1RXBUF;  
    break;
    
  case 3: 
    return UCA3RXBUF;  
    break;
    
  default:
    return 0x00;
  break;
  } 
}


void set_Uart_Tx_Data(unsigned char uart_ucaX, char data) 
{ 
  switch(uart_ucaX)
  {
  case 0: 
    UCA0TXBUF = data;  
    break;   
  case 1: 
    UCA1TXBUF = data;  
    break;    
  case 3: 
    UCA3TXBUF = data; 
    break;
  }   
}

unsigned char Uart_Tx_Busy(unsigned char uart_ucaX)
{
  unsigned char status;
  switch(uart_ucaX)
  {
  case 0: 
    status =  UCA0IFG&UCTXIFG;
    break;   
  case 1: 
    status = UCA1IFG&UCTXIFG;
    break;    
  case 3: 
    status = UCA3IFG&UCTXIFG;
    break;
  } 
  return status;
}


/***************************************************************************//**
 *                          ISR for UART
*******************************************************************************/

/***************************************************************************//**
 * @param Function      __interrupt void vUART0_WIRED_ISR( void )
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  Application.c
 * @param Description   This hardware dependant function used to transmit and receive
                        date from UART
 ******************************************************************************/
// RS485 Input
#pragma vector=UART_0
__interrupt void vUART0_WIRED_ISR( void )
{ 
     LPM0_EXIT;
  // check if data received or data transmitted. 
  if(check_Uart_Int_Flag(0,0)){                           // Ckeck if Received data flag set
    // set Rx LED on
    Set_UART0_LED(RX,ON);
    clear_Uart_Int_Flag(0,0);
    //process_Rx_data(Uart_UCA0_Rx_Buffer);
    //Uart_UCA0_Tx_Buffer = Uart_UCA0_Rx_Buffer;
    #if defined (Debug_RS485)
    Uart_UCA3_Tx_Buffer = Uart_UCA0_Rx_Buffer;
    #endif
    Set_UART0_LED(RX,OFF);
  }
  if(check_Uart_Int_Flag(0,1)){                           // Check Transmit data flag set
    // set Tx LED on
    Set_UART0_LED(TX,ON);
    clear_Uart_Int_Flag(0,1);
    // This application just accepts data from RS485 and will not respond back
    //process_Tx_data();
    Set_UART0_LED(TX,OFF);
  }
}


/***************************************************************************//**
 * @param Function      __interrupt void vUART1_WIRED_ISR( void )
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  Application.c
 * @param Description   This hardware dependant function used to transmit and receive
                        date from UART
 ******************************************************************************/
//External UART
#pragma vector=UART_1
__interrupt void vUART1_WIRED_ISR( void )
{ 
  LPM0_EXIT;
  // check if data received or data transmitted. 
  if(check_Uart_Int_Flag(1,0)){                           // Ckeck if Received data flag set
    // set Rx LED on
    clear_Uart_Int_Flag(1,0);
    // No message will be received as this is used for debugging RS485 msg
  }
  if(check_Uart_Int_Flag(1,1)){                           // Check Transmit data flag set
    // set Tx LED on
    clear_Uart_Int_Flag(1,1);
  } 
}

/***************************************************************************//**
 * @param Function      __interrupt void vUART3_WIRED_ISR( void )
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  Application.c
 * @param Description   This hardware dependant function used to transmit and receive
                        date from UART
 ******************************************************************************/
// Debug UART
#pragma vector=UART_3
__interrupt void vUART3_WIRED_ISR( void )
{ 
  // check if data received or data transmitted. 
  if(check_Uart_Int_Flag(3,0)){                           // Ckeck if Received data flag set
    // set Rx LED on
    clear_Uart_Int_Flag(3,0);
    //process_Rx_data();
  }
  if(check_Uart_Int_Flag(3,1)){                           // Check Transmit data flag set
    // set Tx LED on
    clear_Uart_Int_Flag(3,1);
    //process_Tx_data()
  }
}


