/***************************************************************************//**
 * FILE INCLUDES
 ******************************************************************************/
#include "protocol.h"
#include "uart.h"

//#define DEBUG_PROTOCOL 
#if defined (DEBUG_PROTOCOL)
  #include "stdio.h"
#endif

/***************************************************************************//**
 * CONSTANT DECLARATION
 ******************************************************************************/
//#define RESPONSE        1       // 0 = Dynamic response, 1 is step up and down response.
#define MAPPING         OFF     // ON will automatically map network else will go into manual configuration
//Protocol Level defination 
#define OVP_HIGH_BOUND          585     // 58.0V Max tower top voltage 
#define OVP_LOW_BOUND           380     // 38.0V Min tower top voltage
#define TOWERTOP_TARGET_VOLTAGE 560     // 56.0 Avg tower top ref voltage
#define OVP_TOLERANCE_WINDOW    05      // -/+0.5V difference
#define VOLTAGE_STEP            05      // Voltage increment step

#define PBC_MAX_VOLTAGE         700     // 70.0V Max PBC voltage
#define PBC_MIN_VOLTAGE         540     // 54.5V Min PBC voltage
#define PBC_REF_VOLTAGE         TOWERTOP_TARGET_VOLTAGE     // 60.0V Avg PBC reference voltage
#define PBC_CONFIG_DELTA        20      // 2.0V difference

#define QUALITY_OF_REGISTER     0x0001  // Quality register value = 1
#define PBC_SLAVE_ADDR          0x01    // Slave address =1
#define COMM_START_BYTE1        0xA5    // Protocol Comm start byte1
#define COMM_START_BYTE2        0x5A    // Protocol Comm start byte2
#define MIN_PBC_MSG_COUNT       0x05    // Min 5 bytes will be received (Byte 0-Byte4)      
#define CH_PER_BRICK            2

// ModBus function
//#define PBC_FUNCTION_TYPE       0x06    // Set voltgae function = 6
#define READ_COIL_STATUS                01
#define READ_INPUT_STATUS               02
#define READ_HOLDING_REGISTERS          03
#define READ_INPUT_REGISTERS            04
#define WRITE_SINGLE_COIL               05
#define WRITE_SINGLE_REGISTER           06
// Not supported
//#define WRITE_MULTIPLE_COILS            15
//#define WRITE_MULTIPLE_REGISTERS        16

#define PBC_HEARTBEAT_START_ADDR        30000
#define PBC_REGISTER_START_ADDR         40000
#define PBC_CHANNEL_CURRENT_START_ADDR  41000
#define PBC_PBC_RRH_CHANNEL_SET_ADDR    42000
#define PBC_CHANNEL_STATUS_ADDR         43000
#define APPLICATION_BUFFER_SIZE         250
#define PBC_CONFIG_WAIT_COUNT           10
#define MAX_RETRY                       1
#define MAX_ERROR_COUNT                 5

#define VALID_ADDRESS                   0x00
#define INVALID_DATA_TYPE               0x02
#define OVP_WAIT_COUNT                  5


/***************************************************************************//**
 * ERROR CHECK DECLARATION
 ******************************************************************************/
//Error Check
#if (PBC_REF_VOLTAGE - PBC_CONFIG_DELTA < PBC_MIN_VOLTAGE)
printf("PBC_REF_VOLTAGE invalid count")
#endif
#if (PBC_MAX_VOLTAGE < PBC_MIN_VOLTAGE)
  #error "Max voltage cant be less than Min Voltage";
#endif
#if MAX_CHANNEL_PER_BRICK > 2
  #error "Max channel per Brick cant be greater than 2";
#endif   
#if (PBC_REF_VOLTAGE + PBC_CONFIG_DELTA > OVP_HIGH_BOUND)
  #error "PBC REF voltage and PBC config delta will violate OVP high bound"
#endif

#if (PBC_REF_VOLTAGE - PBC_CONFIG_DELTA < PBC_MIN_VOLTAGE)
  #error "PBC REF voltage and PBC config delta will violate PBC low range"
#endif

#if (VOLTAGE_STEP > 9)
  #error "VOLTAGE_STEP invalid system will go unstable"
#endif


unsigned char PBC_DataLog[APPLICATION_BUFFER_SIZE] = {0x00};
unsigned char Debug_DataLog[APPLICATION_BUFFER_SIZE] = {0x00};

/*
 * CyclicBuffer PBC_TX_Buffer init
 */
CyclicBuffer PBC_TX_Buffer =
{
  &PBC_DataLog[0],       // pBufferStart;            
  &PBC_DataLog[0] + (APPLICATION_BUFFER_SIZE -1),       // pBufferEnd;               
  &PBC_DataLog[0],       // pWrite;                   
  &PBC_DataLog[0],       // pRead;                    
  APPLICATION_BUFFER_SIZE,       // length;                       
}; 

/*
 * CyclicBuffer Debug_Buffer init
 */
CyclicBuffer Debug_Buffer =
{
  &Debug_DataLog[0],       // pBufferStart;            
  &Debug_DataLog[0] + (APPLICATION_BUFFER_SIZE -1),       // pBufferEnd;               
  &Debug_DataLog[0],       // pWrite;                   
  &Debug_DataLog[0],       // pRead;                    
  APPLICATION_BUFFER_SIZE,       // length;                       
}; 

/*
 * PBC_Boost_Msg PBC_Tx_Msg init
 */
PBC_Boost_Msg PBC_Tx_Msg = 
{
  0x00,         // Slave Address Fixed to 01
  0x00,         // Modbus Function fixed to 06  
  0x00,         // Slave Register Address High Byte
  0x00,         // Slave Regiater Address Low Byte
  0x00,         // Write date higher nibble  
  0x00,         // Alarm (refer table)
  0x00,         // Write data lower byte
  0x00,         // Error Check Low
  0x00,         // Error Checl High
};

PBC_Cmd_Addr_Info PBC_Tx_Cmd_Addr_Info =
{
  00,           // base address
  0x00,         // device id
  0x00,         // ovp id
};

///*
// * PBC_Boost_Msg PBC_Rx_Msg init
// */
//PBC_Boost_Msg PBC_Rx_Msg = // Response for WRITE_SINGLE_REGISTER/HEARTBEAT
//{
//  0x00,         // Slave Address Fixed to 01
//  0x00,         // Modbus Function fixed to 06  
//  0x00,         // Slave Register Address High Byte
//  0x00,         // Slave Regiater Address Low Byte
//  0x00,         // Write date higher nibble 
//  0x00,         // Alarm (refer table)  
//  0x00,         // Write data lower byte
//  0x00,         // Error Check Low
//  0x00,         // Error Checl High
//};

/*
 * PBC_Boost_Msg PBC_Rx_Msg init
 */
PBC_Boost_Rx_LengthMsg PBC_Rx_LengthMsg = 
{
  0x00,                 // Slave Address Fixed to 01
  0x00,                 // Modbus Function 
//  0x00,                 // Slave Register Address High Byte
  {0x00,},              // Data
//  0x00,                 // Error Check Low
//  0x00,                 // Error Checl High
};

///*
// * PBC_Boost_Error_Msg PBC_Rx_Error_Msg init
// */
//PBC_Boost_Error_Msg PBC_Rx_Error_Msg = 
//{
//  0x00,         // Slave Address Fixed to 01
//  0x00,         // Modbus Function fixed to 06  
//  0x00,         // Exception Code
//  0x00,         // Error Check Low
//  0x00,         // Error Checl High
//};

/*
 * Power_Boost_Msg Rx_Msg init
 */
Power_Boost_Msg Rx_Msg = 
{
  0x00,                 // start_byte 1
  0x00,                 // start_byte 2
  0x00,                 // System Alarm
  0x00,                 // Device Id
  0x00,                 // msg length
  {
    {
      0x00,             // OVP1_data_h
      0x00,             // OVP1_alarm
      0x00,             // OVP1_data_l
    },
    {
      0x00,             // OVP2_data_h  
      0x00,             // OVP2_alarm
      0x00,             // OVP2_data_l
    },
    {
      0x00,             // OVP3_data_h
      0x00,             // OVP3_alarm
      0x00,             // OVP3_data_l
    },
    {
      0x00,             // OVP4_data_h
      0x00,             // OVP4_alarm
      0x00,             // OVP4_data_l
    },
    {
      0x00,             // OVP5_data_h
      0x00,             // OVP5_alarm
      0x00,             // OVP5_data_l
    },
    {
      0x00,             // OVP6_data_h
      0x00,             // OVP6_alarm
      0x00,             // OVP6_data_l
    },
    {
      0x00,             // OVP7_data_h
      0x00,             // OVP7_alarm
      0x00,             // OVP7_data_l
    },
    {
      0x00,             // OVP8_data_h
      0x00,             // OVP8_alarm
      0x00,             // OVP8_data_l
    },
    {
      0x00,             // OVP9_data_h
      0x00,             // OVP9_alarm
      0x00,             // OVP9_data_l
    },
    {
      0x00,             // OVP10_data_h
      0x00,             // OVP10_alarm
      0x00,             // OVP10_data_l
    },
    {
      0x00,             // OVP11_data_h
      0x00,             // OVP11_alarm
      0x00,             // OVP11_data_l
    },
    {
      0x00,             // OVP12_data_h
      0x00,             // OVP12_alarm
      0x00,             // OVP12_data_l
    },
  },
  0x00                  // msg_checksum
};

Power_Boost_Msg *pRx_Msg = &Rx_Msg;

/*
 * Previous_Msg Previous_Rx_Msg init
 */
Previous_Msg Previous_Rx_Msg =
{0};

/*
 * Msg_Diagnostics RC_Msg_Error init
 */
Msg_Diagnostics RC_Msg_Error = 
{
  {0x00,},
  0x00,
  {0x0000,},  
};

/*
 * Power_Boost_Rx_Flag Rx_Msg_Flags init
 */
Power_Boost_Rx_Flag Rx_Msg_Flags =
{
  0x00, 
  0x00,
  0x00,
};

/*
 * Config_Flag PBC_Config_Flag init
 */
Config_Flag PBC_Config_Flag =
{
  0x00,         // Config_Busy_flag
  0x00,         // Config_Busy_id    
  0x00,         // Config_Busy_ovp_id    
  0x00,         // Config_Wait_Count
}; 

/*
 * Power_Boost_Rx_Flag PBC_Rx_Msg_Flag init
 */
Power_Boost_Rx_Flag PBC_Rx_Msg_Flag = 
{
  0x00,         // start byte 1
  0x00,         // start byte 2
  0x00,         // message rx complete flag 
  0x00,         // expected message length
};


/*
 * HeartBeat_Msg PCU_Beat_Msg init
 */
HeartBeat_Msg PCU_Beat_Msg =
{
  0x0000,
  0x00, 
};

/***************************************************************************//**
 * FUNCTION DEFINATION
 ******************************************************************************/
/***************************************************************************//**
 * @param Function      CRC16
 * @param Input         nData, wLength
 * @param Return        int
 * @param Calling_Func  PBC TX, RX and validation
 * @param Description   This function is used to calculate CRC for MODBUS protocol
 ******************************************************************************/
unsigned int CRC16 (nData, wLength)
unsigned char *nData ;                      
unsigned char wLength ;  
{
static const unsigned int wCRCTable[] = {
   0X0000, 0XC0C1, 0XC181, 0X0140, 0XC301, 0X03C0, 0X0280, 0XC241,
   0XC601, 0X06C0, 0X0780, 0XC741, 0X0500, 0XC5C1, 0XC481, 0X0440,
   0XCC01, 0X0CC0, 0X0D80, 0XCD41, 0X0F00, 0XCFC1, 0XCE81, 0X0E40,
   0X0A00, 0XCAC1, 0XCB81, 0X0B40, 0XC901, 0X09C0, 0X0880, 0XC841,
   0XD801, 0X18C0, 0X1980, 0XD941, 0X1B00, 0XDBC1, 0XDA81, 0X1A40,
   0X1E00, 0XDEC1, 0XDF81, 0X1F40, 0XDD01, 0X1DC0, 0X1C80, 0XDC41,
   0X1400, 0XD4C1, 0XD581, 0X1540, 0XD701, 0X17C0, 0X1680, 0XD641,
   0XD201, 0X12C0, 0X1380, 0XD341, 0X1100, 0XD1C1, 0XD081, 0X1040,
   0XF001, 0X30C0, 0X3180, 0XF141, 0X3300, 0XF3C1, 0XF281, 0X3240,
   0X3600, 0XF6C1, 0XF781, 0X3740, 0XF501, 0X35C0, 0X3480, 0XF441,
   0X3C00, 0XFCC1, 0XFD81, 0X3D40, 0XFF01, 0X3FC0, 0X3E80, 0XFE41,
   0XFA01, 0X3AC0, 0X3B80, 0XFB41, 0X3900, 0XF9C1, 0XF881, 0X3840,
   0X2800, 0XE8C1, 0XE981, 0X2940, 0XEB01, 0X2BC0, 0X2A80, 0XEA41,
   0XEE01, 0X2EC0, 0X2F80, 0XEF41, 0X2D00, 0XEDC1, 0XEC81, 0X2C40,
   0XE401, 0X24C0, 0X2580, 0XE541, 0X2700, 0XE7C1, 0XE681, 0X2640,
   0X2200, 0XE2C1, 0XE381, 0X2340, 0XE101, 0X21C0, 0X2080, 0XE041,
   0XA001, 0X60C0, 0X6180, 0XA141, 0X6300, 0XA3C1, 0XA281, 0X6240,
   0X6600, 0XA6C1, 0XA781, 0X6740, 0XA501, 0X65C0, 0X6480, 0XA441,
   0X6C00, 0XACC1, 0XAD81, 0X6D40, 0XAF01, 0X6FC0, 0X6E80, 0XAE41,
   0XAA01, 0X6AC0, 0X6B80, 0XAB41, 0X6900, 0XA9C1, 0XA881, 0X6840,
   0X7800, 0XB8C1, 0XB981, 0X7940, 0XBB01, 0X7BC0, 0X7A80, 0XBA41,
   0XBE01, 0X7EC0, 0X7F80, 0XBF41, 0X7D00, 0XBDC1, 0XBC81, 0X7C40,
   0XB401, 0X74C0, 0X7580, 0XB541, 0X7700, 0XB7C1, 0XB681, 0X7640,
   0X7200, 0XB2C1, 0XB381, 0X7340, 0XB101, 0X71C0, 0X7080, 0XB041,
   0X5000, 0X90C1, 0X9181, 0X5140, 0X9301, 0X53C0, 0X5280, 0X9241,
   0X9601, 0X56C0, 0X5780, 0X9741, 0X5500, 0X95C1, 0X9481, 0X5440,
   0X9C01, 0X5CC0, 0X5D80, 0X9D41, 0X5F00, 0X9FC1, 0X9E81, 0X5E40,
   0X5A00, 0X9AC1, 0X9B81, 0X5B40, 0X9901, 0X59C0, 0X5880, 0X9841,
   0X8801, 0X48C0, 0X4980, 0X8941, 0X4B00, 0X8BC1, 0X8A81, 0X4A40,
   0X4E00, 0X8EC1, 0X8F81, 0X4F40, 0X8D01, 0X4DC0, 0X4C80, 0X8C41,
   0X4400, 0X84C1, 0X8581, 0X4540, 0X8701, 0X47C0, 0X4680, 0X8641,
   0X8201, 0X42C0, 0X4380, 0X8341, 0X4100, 0X81C1, 0X8081, 0X4040 };

unsigned char nTemp = 0x00;
unsigned int wCRCWord = 0xFFFF;

   while (wLength--)
   {
      nTemp = *nData++ ^ wCRCWord;
      wCRCWord >>= 8;
      wCRCWord  ^= wCRCTable[nTemp];
   }
   return wCRCWord;

} // End: CRC16

/***************************************************************************//**
 * @param Function      Send_UART_Debug_Info
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  main
 * @param Description   Send software information during startup
 ******************************************************************************/
void Send_UART_Debug_Info()
{
  //Send IP information on debug port
  AppWriteBufferBlock(&Debug_Buffer, (unsigned char*) &APPLICATION_COMPANY, (sizeof(APPLICATION_COMPANY) -1));
  // Send hardware version on debug port
  AppWriteBufferBlock(&Debug_Buffer, (unsigned char*) &APPLICATION_HW_VERSION, (sizeof(APPLICATION_HW_VERSION) -1));
  // Send firmware version on debug port
  AppWriteBufferBlock(&Debug_Buffer, (unsigned char*) &APPLICATION_SW_VERSION, (sizeof(APPLICATION_SW_VERSION) -1));
   // Set interrupt flag to send out data.
  Set_Uart_Int_Flag(1);
}

/***************************************************************************//**
 * @param Function      
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  
 * @param Description   
 ******************************************************************************/
void System_Alarm()
{
  __no_operation();
}

/////***************************************************************************//**
//// * @param Function      
//// * @param Input         void
//// * @param Return        void
//// * @param Calling_Func  
//// * @param Description   
//// ******************************************************************************/
////void OVP_Specific_Alarm()
////{
////  __no_operation();
////}

/***************************************************************************//**
 * @param Function      Set_Tx_Address_Info 
 * @param Input         base_addr, id, ovp_id
 * @param Return        void
 * @param Calling_Func  
 * @param Description   Stores address info to structure for cmd rx processing
 ******************************************************************************/
void Set_Tx_Address_Info(unsigned int base_addr, unsigned char id, unsigned char ovp_id)
{
  PBC_Tx_Cmd_Addr_Info.Base_Address = base_addr;
  PBC_Tx_Cmd_Addr_Info.Device_id = id;
  PBC_Tx_Cmd_Addr_Info.Ovp_id = ovp_id;
}

/***************************************************************************//**
 * @param Function      Clear_OVP_RxBufferAndCount
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  Protocol Rx
 * @param Description   This function will clear Rx buffer and count value for 
 *                      commands received from surge protection device
 ******************************************************************************/
 void Clear_OVP_RxBufferAndCount()
 {
    // Reset all timers, flags and rx buffers
    RxBuffer_Count = 0x00;
    unsigned char i;
    for (i= 0; i < sizeof(Power_Boost_Msg); i++)
    {
     RxBuffer[i] = 0x00;
    } 
 }
 
/***************************************************************************//**
 * @param Function      Clear_PBC_RxBufferAndCount
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  Protocol Rx
 * @param Description   This function will clear Rx buffer and count value for 
 *                      commands received from surge protection device
 ******************************************************************************/
 void Clear_PBC_RxBufferAndCount()
 {
    // Reset all timers, flags and rx buffers
    PBC_RxBuffer_Count = 0x00;
    unsigned char i;
    for (i= 0; i < sizeof(Power_Boost_Msg); i++)
    {
     PBC_RxBuffer[i] = 0x00;
    } 
 }


// void clear_Power_Boost_Flags()
// {
//    // Reset all timers, flags and rx buffers
//    RxBuffer_Count = 0x00;
//    unsigned char i;
//    for (i= 0; i < sizeof(Power_Boost_Msg); i++)
//    {
//      memcpy(&Rx_Msg, NULL, sizeof(Rx_Msg));
//    } 
// }
  
/***************************************************************************//**
 * @param Function      Verify_OVP_Msg_CRC
 * @param Input         Power_Boost_Msg
 * @param Return        char
 * @param Calling_Func  protocol RC Rx
 * @param Description   Verify CRC from incomming 3rd party surge protection device
 ******************************************************************************/
 unsigned char Verify_OVP_Msg_CRC(Power_Boost_Msg* Msg)
 {
   unsigned char i;
   unsigned char Cal_Crc = 0x00;
   
   for (i=0; i < (sizeof(Power_Boost_Msg)-1); i++)
   {
     Cal_Crc = (unsigned char)((Cal_Crc + *(&Msg->start_byte_1 + i)) & 0x00FF);
     //todo: Uncomment below line to skip CRC check
     //Cal_Crc = Msg->msg_checksum;
   }
   if (Cal_Crc != Msg->msg_checksum)
   {
     return FALSE;
   }
   else
   {
     return TRUE;
   }
 }

/***************************************************************************//**
 * @param Function      Verify_PBC_CRC
 * @param Input         PBC_Boost_Rx_Msg
 * @param Return        char
 * @param Calling_Func  protocol PBC Rx
 * @param Description   Verify CRC from incomming PBC controller
 ******************************************************************************/
unsigned char Verify_PBC_CRC(PBC_Boost_Msg* PBC_Msg)
 {
//   unsigned char Cal_Crc = 0x00;   
//   Cal_Crc = CRC16(PBC_Msg,6);
   
   if (memcmp(PBC_Msg,&PBC_Tx_Msg,sizeof(PBC_Msg)))
   {
     return FALSE;
   }
   else
   {
     return TRUE;
   }
 }
 
// /***************************************************************************//**
// * @param Function      Validate_Rx_Error_Msg 
// * @param Input         PBC_Boost_Error_Msg
// * @param Return        char
// * @param Calling_Func  Protocol Rx msg
// * @param Description   Function validates if PBC incomming message has error 
// ******************************************************************************/ 
//unsigned char Validate_Rx_Error_Msg(PBC_Boost_Error_Msg* Rx_Error_Msg)
//{   
//  if ((Rx_Error_Msg->Slave_Addr == PBC_Tx_Msg.Slave_Addr) & (Rx_Error_Msg->Function == PBC_Tx_Msg.Function))
//  {
//    unsigned int Error_Check = CRC16(&Rx_Error_Msg,3);
//    if (Error_Check == (Rx_Error_Msg->Error_Check_Hi * 256 + Rx_Error_Msg->Error_Check_Lo))
//    {
//      return TRUE;
//    }
//  }
//  return FALSE;
//}

 /***************************************************************************//**
 * @param Function      Verify_Msg_CRC 
 * @param Input         PBC_Boost_Error_Msg
 * @param Return        char
 * @param Calling_Func  Protocol Rx msg
 * @param Description   Function validates if PBC incomming message has error 
 ******************************************************************************/ 
unsigned char Verify_Msg_CRC(unsigned char *Msg_ptr, unsigned char size)
{   
  unsigned int Error_Check = CRC16(Msg_ptr,(size-1));
  if (Error_Check == (Msg_ptr[size-1] + Msg_ptr[size]*256))
  {
    return TRUE;
  }
  return FALSE;
}
/***************************************************************************//**
 * @param Function      Process_OVP_Rx_data
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  UART Rx interrupt
 * @param Description   Process surge protector device messages
 ******************************************************************************/
void Process_OVP_Rx_data(unsigned char rx_data)
 {
  // Check if start of data received 
  if ((rx_data == COMM_START_BYTE1) & (Rx_Msg_Flags.msg_start_byte1_rx != 0x01))
  {
   Change_Led( GRN_LED, OFF); 
   // Start of message received
   Clear_OVP_RxBufferAndCount();
   Rx_Msg_Flags.end_byte_rx = 0x00;
   Rx_Msg_Flags.msg_start_byte2_rx = 0x00;
   Rx_Msg_Flags.msg_start_byte1_rx = 0x01;
   // Store data 
   RxBuffer[RxBuffer_Count] = rx_data;
  }
  // Message a part of original message begin received
  // Process message only if valid start byte 1 received.   
  else
  {
    // Verify if valid start byte 2 received
    if ((rx_data == COMM_START_BYTE2) && (Rx_Msg_Flags.msg_start_byte1_rx == 0x01) && (Rx_Msg_Flags.msg_start_byte2_rx != 0x01))
    {
      RxBuffer_Count++;
      Rx_Msg_Flags.end_byte_rx = 0x00;
      Rx_Msg_Flags.msg_start_byte2_rx = 0x01;
      // Store data 
      RxBuffer[RxBuffer_Count] = rx_data;
    }
    // Process message if valid start sequency received. 
    else
    { 
      if ((Rx_Msg_Flags.msg_start_byte2_rx == 0x01) && (Rx_Msg_Flags.msg_start_byte1_rx == 0x01))
      {
        // Store data in RxBuffer
        RxBuffer_Count++;
        RxBuffer[RxBuffer_Count] = rx_data; 
        // check if complete message recived
        if ((RxBuffer_Count >= sizeof(Rx_Msg) -1) || (RxBuffer[3]  == (RxBuffer_Count + 1)))
        {
          // Read message id and then process - TBD
          memcpy(&Rx_Msg, RxBuffer, RxBuffer_Count + 1);
          //Rx_Msg.msg_checksum = RxBuffer[RxBuffer_Count];
          if (Verify_OVP_Msg_CRC(&Rx_Msg) !=0) 
          {
            // Valid CRC
            Rx_Msg_Flags.end_byte_rx = 0x01;
            Rx_Msg_Flags.msg_start_byte1_rx = 0x00;
            Rx_Msg_Flags.msg_start_byte2_rx = 0x00;
            Clear_Timer_Count();
          }
          else 
          {
            #if defined (DEBUG_PROTOCOL)
              printf("CRC ERROR\n");
            #endif
            // Invalid CRC
            Rx_Msg_Flags.end_byte_rx = 0x00;
            Rx_Msg_Flags.msg_start_byte1_rx = 0x00;
            Rx_Msg_Flags.msg_start_byte2_rx = 0x00;
          } 
          Clear_OVP_RxBufferAndCount();
          
        }
      }
      // Out of line start message byte received 
      // Flush data
      else    
      {
        // Fault message  
        Clear_OVP_RxBufferAndCount();
        #if defined (DEBUG_PROTOCOL)
          printf("INVALID START\n");
        #endif
      }
    }
  }
}
 
/***************************************************************************//**
 * @param Function     Expected_PBC_MsgLength 
 * @param Input        unsigned char
 * @param Return       unsigned char
 * @param Calling_Func Protocol PBC Rx Msg 
 * @param Description  Function will process messages received from PBC controller 
 ******************************************************************************/
unsigned char Expected_PBC_MsgLength(unsigned char function,unsigned char length)
{
  if ((function & 0x80) > 1)
  {
    return MIN_PBC_MSG_COUNT -1; 
  }
  else
  {
    // Check type of message received
    switch (function)
    {
//  case READ_COIL_STATUS:        // READ_COIL_STATUS = 01
//     return expected_length =length + MIN_PBC_MSG_COUNT; 
//    break;
//  case READ_INPUT_STATUS:       // READ_INPUT_STATUS = 02
//    expected_length =length + MIN_PBC_MSG_COUNT; 
//    break;
//   case READ_HOLDING_REGISTERS: // READ_HOLDING_REGISTERS = 03
//    break;
//  case READ_INPUT_REGISTERS:    // READ_INPUT_REGISTERS = 04
//    expected_length =length + MIN_PBC_MSG_COUNT; 
//    break;
//  case WRITE_SINGLE_COIL:       // WRITE_SINGLE_COIL = 05
//    expected_length =length + MIN_PBC_MSG_COUNT; 
//    break;
    case WRITE_SINGLE_REGISTER:   // WRITE_SINGLE_REGISTER = 06
      return sizeof(PBC_Boost_Msg) -1;
      break;
    default:
      return ((length + MIN_PBC_MSG_COUNT)-1); 
      break;
    // Not supported currently
//  case WRITE_MULTIPLE_COILS:    // WRITE_MULTIPLE_COILS = 15
//    expected_length =length + MIN_PBC_MSG_COUNT; 
//    break;
//  case WRITE_MULTIPLE_REGISTERS:// WRITE_MULTIPLE_REGISTERS = 16        
//    expected_length =length + MIN_PBC_MSG_COUNT; 
//    break;
    }   
  }
}

/***************************************************************************//**
 * @param Function     Record_PBC_Rx_Msg 
 * @param Input        char 
 * @param Return       void       
 * @param Calling_Func uart Rx 
 * @param Description  Function will process messages received from PBC controller 
 ******************************************************************************/
void Record_PBC_Rx_Msg(unsigned char pbc_rx_data)
{
//  PBC_Rx_Msg_Flag.PBC_Rx_Msg_Type = 0;
  // Check if start of data and valid slave address is received 
  if ((pbc_rx_data == PBC_Tx_Msg.Slave_Addr) && 
      (PBC_Rx_Msg_Flag.msg_start_byte1_rx != 0x01))
  {
    // Clear message received buffer
    Clear_PBC_RxBufferAndCount();
    // Adjust PBC Rx message start and end flags
    PBC_Rx_Msg_Flag.end_byte_rx = 0x00;
    PBC_Rx_Msg_Flag.msg_start_byte2_rx = 0x00;
    PBC_Rx_Msg_Flag.msg_start_byte1_rx = 0x01;
    // Store data to Rxbuffer              
    PBC_RxBuffer[PBC_RxBuffer_Count] = pbc_rx_data; // Store rx data to buffer
    Change_Led(PBC_LED,ON);
    PBC_Rx_Msg_Flag.Expected_Rx_Byte = 0;
  }
  else
  {
    // Verify if valid start function response received or error fucntion (0x1xxx xxxx)
    if ((PBC_Tx_Msg.Function == (pbc_rx_data & 0x7F)) && 
        (PBC_Rx_Msg_Flag.msg_start_byte1_rx == 0x01)  && 
        (PBC_Rx_Msg_Flag.msg_start_byte2_rx != 0x01))
    { 
//      // Clear end byte flag
//      PBC_Rx_Msg_Flag.end_byte_rx = 0x00;
      // Set the flag to indicate valid function response received  
      PBC_Rx_Msg_Flag.msg_start_byte2_rx = 0x01;
      // increment Rx buffer count before storing data 
      PBC_RxBuffer_Count++;
      // Store incoming uart data to RxBuffer 
      PBC_RxBuffer[PBC_RxBuffer_Count] = pbc_rx_data;
    }
    // Process message if valid start sequency received. 
    else
    { 
      //check if valid slave address and function response from PBC controller received
      if ((PBC_Rx_Msg_Flag.msg_start_byte2_rx == 0x01) && 
          (PBC_Rx_Msg_Flag.msg_start_byte1_rx == 0x01))
      {
        // Store data in RxBuffer
        PBC_RxBuffer_Count++;
        // Store incoming uart data to RxBuffer 
        PBC_RxBuffer[PBC_RxBuffer_Count] = pbc_rx_data; 
        // Check and calculate expected length field  
        // ModBus Byte0->Slave Address, Byte2->Functio, Byte2->Byte length or data (based on function)
        if (PBC_RxBuffer_Count == 0x02) 
        {
          // updated the Rx message lenght to data length
          PBC_Rx_Msg_Flag.Expected_Rx_Byte = Expected_PBC_MsgLength(PBC_RxBuffer[1],PBC_RxBuffer[2]); 
        }
        // check if complete message recived
        if (PBC_RxBuffer_Count >= PBC_Rx_Msg_Flag.Expected_Rx_Byte)
        {
          if (Verify_Msg_CRC(PBC_RxBuffer, PBC_RxBuffer_Count))
          {
            // Do not copy the CRC as the msg is verified
            memcpy(&PBC_Rx_LengthMsg, &PBC_RxBuffer, PBC_Rx_Msg_Flag.Expected_Rx_Byte - 1);
            // Valid CRC
            PBC_Rx_Msg_Flag.end_byte_rx = 0x01;    
          }
          else 
          {
            #if defined (DEBUG_PROTOCOL)
              printf("PBC ERROR\n");
            #endif
            // Invalid CRC
            PBC_Rx_Msg_Flag.end_byte_rx = 0x00;   
          }
 
          PBC_Rx_Msg_Flag.msg_start_byte1_rx = 0x00;
          PBC_Rx_Msg_Flag.msg_start_byte2_rx = 0x00;
          Clear_PBC_RxBufferAndCount();
          PBC_Rx_Msg_Flag.Expected_Rx_Byte = 0x00;
        }
      }
      // Out of line start message byte received 
      // Flush data
      else    
      {
        // Fault message  
        Clear_PBC_RxBufferAndCount();
        #if defined (DEBUG_PROTOCOL)
          printf("INVALID START\n");
        #endif
      }
    }
  } 
}

/***************************************************************************//**
 * @param Function      Verify_OVP_Msg_End_Flag_Rx
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  main
 * @param Description   Function return a flag to indicate if the message is 
 *                      pending
 ******************************************************************************/
 unsigned char Verify_OVP_Msg_End_Flag_Rx()
 {
   return Rx_Msg_Flags.end_byte_rx;
 }
 
/***************************************************************************//**
 * @param Function      Check_System_Alarm
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  unused
 * @param Description   returns alarm flag status
 ******************************************************************************/ 
unsigned char Check_System_Alarm()
{
  if (Rx_Msg.System_Alarm > 0)
  {
     #if defined (DEBUG_PROTOCOL)
        printf("ALARM\n");
      #endif
      // process major alarm
      System_Alarm();  
      return TRUE;
  }
  else
  {
      return FALSE;
  }
}
 
/***************************************************************************//**
 * @param Function      Process_PBC_Rx_Msg
 * @param Input         PBC_Boost_Msg
 * @param Return        unsigned char
 * @param Calling_Func  protocol PBC Tx, Rx function
 * @param Description   Function is used process PBC controller Rx message. 
 ******************************************************************************/
unsigned char Process_PBC_Rx_Msg(PBC_Boost_Msg Tx_Msg) 
{
  unsigned char return_val = FALSE;
  unsigned int base_addr = PBC_Tx_Cmd_Addr_Info.Base_Address;
  unsigned char id = PBC_Tx_Cmd_Addr_Info.Device_id;
  unsigned char ovp_id = PBC_Tx_Cmd_Addr_Info.Ovp_id; 
  // check if response message is for recent Tx command
  if ((PBC_Rx_LengthMsg.Function & 0x7F) == Tx_Msg.Function)
  {
    // check if the response is error message
    if ((PBC_Rx_LengthMsg.Function & 0x80) > 0)
    {
      // error message received
      switch (PBC_Rx_LengthMsg.Function & 0x7F)
      {
        // error response for write single register - Set test point
        case WRITE_SINGLE_REGISTER: // 0x06 
          {
            if (PBC_Rx_LengthMsg.Data[0] == INVALID_DATA_TYPE)
            {
              // Flag the PBC channel to be not configured
              Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Invalid_OVP_Flag |= 1; 
              return_val = TRUE;
            }
            // no action taken for undocumented error occurs
            else
            {
              // other message type not supported
              __no_operation();
            }
          }
          break;
        default: 
          // other function error code not supported
          __no_operation();
          break;
      }
    }
    // Response is valid response
    else
    {
      // valid message received
      switch (PBC_Rx_LengthMsg.Function)
      {
        case READ_INPUT_STATUS: // 0x02
        {
          // Store Hybrid channel status response
          if (base_addr == PBC_CHANNEL_STATUS_ADDR)
          { 
            // check if received message has valid length
            if (PBC_Rx_LengthMsg.Data[0] == 0x01)
            {
              Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Hybrid_Ch_Status = PBC_Rx_LengthMsg.Data[1];
              if (Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Hybrid_Ch_Status == 1)
              {
                Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Configured_OVP_Flag |= 0;
                Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Invalid_OVP_Flag |= 1; 
              }
              return_val = TRUE;
            }   
          }
          else
          {
            // Device not ready
//            Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Update_Hybrid_Ch_Status = 0;
            Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Invalid_OVP_Flag |= 1;
            __no_operation();
          }
        }
        break;
        case READ_INPUT_REGISTERS: // 0x04
          // Store hybrid channel current 
          if (base_addr == PBC_CHANNEL_CURRENT_START_ADDR)
          { 
            // check if valid lenght received
            if (PBC_Rx_LengthMsg.Data[0] == 0x02)
            {
              // store the current value
              Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Hybrid_Current_High = PBC_Rx_LengthMsg.Data[1];
              Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Hybrid_Current_Low = PBC_Rx_LengthMsg.Data[2];
              return_val = TRUE;
            }   
          }
          // Store hybrid channel RRH link 
          else if (base_addr == PBC_PBC_RRH_CHANNEL_SET_ADDR)
          {
            // Check if the received message has valid length
            if (PBC_Rx_LengthMsg.Data[0] == 0x02)
            {
              // Store the RRH link#
              if ((Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Linked_RRH_High != PBC_Rx_LengthMsg.Data[1]) | 
                  (Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Linked_RRH_Low != PBC_Rx_LengthMsg.Data[2])) 
              {
                // New channel found
//                Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Update_Hybrid_Ch_Status = 1;
                Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Configured_OVP_Flag = 0;
                Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Linked_RRH_High = PBC_Rx_LengthMsg.Data[1];
                Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Linked_RRH_Low = PBC_Rx_LengthMsg.Data[2];
              }
              
              if(((Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Linked_RRH_High == 0xFF) &&
                  (Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Linked_RRH_Low == 0xFF)) ||
                 ((Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Linked_RRH_High == 0x00) &&
                  (Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Linked_RRH_Low == 0x00)))
              {
                Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Adj_Ch_Paired_Flag = FALSE;
              }
              else
              {
                Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Adj_Ch_Paired_Flag = TRUE;
              }
              return_val = TRUE;
            } 
          }
          else
          {
            // other commands not supported 
            __no_operation();
          }

          break;
        case WRITE_SINGLE_REGISTER: // 0x06 
          {
            unsigned int tx_addr = (base_addr + (id * MAX_OVP_PER_MSG) + ovp_id);
            unsigned int rx_addr = (PBC_Rx_LengthMsg.Data[0] * 256 + PBC_Rx_LengthMsg.Data[1]);
            // Store hybrid channel set point
            if (base_addr == PBC_REGISTER_START_ADDR)
            { 
              // validate if the Tx and Rx register address are same to valide response message
              if (tx_addr == rx_addr)
              {
                // Store hybrid channel set point
                Previous_Rx_Msg.Power_Boost_Processed_Msg[id][ovp_id].Reg_data_h = PBC_Rx_LengthMsg.Data[2] & 0x0F;
                Previous_Rx_Msg.Power_Boost_Processed_Msg[id][ovp_id].Reg_data_l = PBC_Rx_LengthMsg.Data[3];
                Previous_Rx_Msg.Power_Boost_Processed_Msg[id][ovp_id].Reg_Flag = (PBC_Rx_LengthMsg.Data[2] >> 4);
                return_val = TRUE;
              }   
            }
            else if (base_addr == PBC_HEARTBEAT_START_ADDR)
            {
              if (tx_addr == rx_addr)
              {
                // Clear PBC communication error flag if any.
                __no_operation();
                return_val = TRUE;
              }
            }
            else
            {
              // other commands not supported 
              __no_operation();
            }
          }
          break;
        default:
          // Other commands not supported
          __no_operation();
          break;
      }
    }
  }
  else
  {
    // dont process command
    // error
    __no_operation();
  }
  return return_val;
}

/***************************************************************************//**
 * @param Function      Send_Receive_PBC_Data
 * @param Input         PBC_Boost_Tx_Msg
 * @param Return        char
 * @param Calling_Func  protocol PBC Tx, Rx function
 * @param Description   Function is used to transmit and validate response 
 *                      message from PBC controller. 
 ******************************************************************************/
unsigned char Send_Receive_PBC_Data(PBC_Boost_Msg Tx_Msg)
{
  // define local vaiables
  unsigned char count;
  unsigned char return_val = FALSE;
  // Clear hearbeat timer count
  Clear_Heartbeat_Count();

  for (count = 0; count < MAX_RETRY; count++)
  {
    // Send message to PBC
    AppWriteBufferBlock(&PBC_TX_Buffer, (unsigned char*) &Tx_Msg, (sizeof(PBC_Tx_Msg)));
    // Set interrupt flag to send out data.
    Set_Uart_Int_Flag(0);
    unsigned int delay_count;
    for (delay_count = 0; ((delay_count < 250) & (PBC_Rx_Msg_Flag.end_byte_rx == 0)); delay_count ++)
    {
      __delay_cycles(20000);
    }
    // check if data received
    if(PBC_Rx_Msg_Flag.end_byte_rx == 1)
    {    
      PBC_Rx_Msg_Flag.end_byte_rx =0;
      return_val = Process_PBC_Rx_Msg(Tx_Msg);  
      Change_Led(PBC_LED,OFF);
    }
    else
    {
      Change_Led(PBC_LED,ON);
    }
  }
  Set_Tx_Address_Info(0,0,0);
  return return_val;
}

/***************************************************************************//**
 * @param Function      Setup_OVP_Register
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  main
 * @param Description   Function clears all the OVP register and set them to 
 *                      default. Function is executed after power up or reset. 
 ******************************************************************************/
// Initialize Previous Rx Msg by setting all the internal buffer values to 0x00
void Setup_OVP_Register()
{
  unsigned char addr_count, OVP_count;
  // set all the default structure to 0
  for (addr_count = 0; addr_count < MAX_DEVICE_ID; addr_count++)
  {
    for(OVP_count = 0; OVP_count < MAX_OVP_PER_MSG; OVP_count++)
    {
      Previous_Rx_Msg.Power_Boost_Processed_Msg[addr_count][OVP_count].Reg_data_h = 0x00;
      Previous_Rx_Msg.Power_Boost_Processed_Msg[addr_count][OVP_count].Reg_Flag = 0x00;
      Previous_Rx_Msg.Power_Boost_Processed_Msg[addr_count][OVP_count].Reg_data_l = 0x00;
      // Clear transmit buffers
      Previous_Rx_Msg.OVP_Raw_Tx_Data[addr_count][OVP_count].Reg_data_h = 0x00;
      Previous_Rx_Msg.OVP_Raw_Tx_Data[addr_count][OVP_count].Reg_Flag = 0x00;
      Previous_Rx_Msg.OVP_Raw_Tx_Data[addr_count][OVP_count].Reg_data_l = 0x00;
      // Clear receive buffers
      Previous_Rx_Msg.OVP_Raw_Rx_Data[addr_count][OVP_count].Reg_data_h = 0x00;
      Previous_Rx_Msg.OVP_Raw_Rx_Data[addr_count][OVP_count].Reg_Flag = 0x00;
      Previous_Rx_Msg.OVP_Raw_Rx_Data[addr_count][OVP_count].Reg_data_l = 0x00;
      
      Previous_Rx_Msg.PBC_Controller_Info[addr_count][OVP_count].Invalid_OVP_Flag = 0x00;  
      Previous_Rx_Msg.PBC_Controller_Info[addr_count][OVP_count].Hybrid_Current_High = 0x00;
      Previous_Rx_Msg.PBC_Controller_Info[addr_count][OVP_count].Hybrid_Current_Low = 0x00;
      Previous_Rx_Msg.PBC_Controller_Info[addr_count][OVP_count].Hybrid_Ch_Status = 0x00;
      Previous_Rx_Msg.PBC_Controller_Info[addr_count][OVP_count].Linked_RRH_High = 0x00;
      Previous_Rx_Msg.PBC_Controller_Info[addr_count][OVP_count].Linked_RRH_Low = 0x00; 
      
      Previous_Rx_Msg.PBC_Register_Map[addr_count][OVP_count].Msg_OVP_Id_High[0] = 0;
      Previous_Rx_Msg.PBC_Register_Map[addr_count][OVP_count].Msg_OVP_Id_Low[0] = 0;
      Previous_Rx_Msg.PBC_Register_Map[addr_count][OVP_count].Msg_OVP_Id_Avg[0] = 0;
      Previous_Rx_Msg.PBC_Register_Map[addr_count][OVP_count].Msg_OVP_Id_High[1] = 0;
      Previous_Rx_Msg.PBC_Register_Map[addr_count][OVP_count].Msg_OVP_Id_Low[1] = 0;
      Previous_Rx_Msg.PBC_Register_Map[addr_count][OVP_count].Msg_OVP_Id_Avg[1] = 0;
     
      Previous_Rx_Msg.PBC_Register_Map[addr_count][OVP_count].Set_High_Flag = 0;
      Previous_Rx_Msg.PBC_Register_Map[addr_count][OVP_count].Set_Low_Flag = 0;
      Previous_Rx_Msg.PBC_Register_Map[addr_count][OVP_count].Set_Avg_Flag = 0;

      Previous_Rx_Msg.OVP_Processed_Flag[addr_count][OVP_count] = 0;
    }
  }
}


/***************************************************************************//**
 * @param Function      Set_PBC_Vref_Setpoint 
 * @param Input         device_addr, pbc_addr
 * @param Return        void
 * @param Calling_Func  main
 * @param Description   Function configures each PBC to known setup  
 ******************************************************************************/
void Set_PBC_Vref_Setpoint(unsigned char device_addr, unsigned char pbc_addr)
{
  unsigned status = 0;
  unsigned int PBC_Register_Address;
  // Sent message on all the address to known setpoint
  PBC_Tx_Msg.Slave_Addr = PBC_SLAVE_ADDR;
  PBC_Tx_Msg.Function = WRITE_SINGLE_REGISTER;
  
//  unsigned char device_addr, pbc_addr;
//  for (device_addr = 0; device_addr < MAX_DEVICE_ID; device_addr++)
//  {
//    // We dont need to individualy configure each PBC as one brick can output multiple channel
//    for (pbc_addr = 0; pbc_addr < MAX_OVP_PER_MSG; pbc_addr++) 
//    {  
      // Record Previous RX Msg Raw OVP data value to Set voltage
      Previous_Rx_Msg.OVP_Raw_Tx_Data[device_addr][pbc_addr].Reg_data_h = (PBC_REF_VOLTAGE >> 8);
      Previous_Rx_Msg.OVP_Raw_Tx_Data[device_addr][pbc_addr].Reg_data_l = (PBC_REF_VOLTAGE & 0xFF);
      Previous_Rx_Msg.OVP_Raw_Tx_Data[device_addr][pbc_addr].Reg_Flag = 0x00; 
    
      PBC_Tx_Msg.Write_Data_Hi = Previous_Rx_Msg.OVP_Raw_Tx_Data[device_addr][pbc_addr].Reg_data_h;
      PBC_Tx_Msg.Write_Data_Lo = Previous_Rx_Msg.OVP_Raw_Tx_Data[device_addr][pbc_addr].Reg_data_l;
      PBC_Tx_Msg.Alarm = Previous_Rx_Msg.OVP_Raw_Tx_Data[device_addr][pbc_addr].Reg_Flag;
      
      PBC_Register_Address =  (PBC_REGISTER_START_ADDR + ((device_addr * MAX_OVP_PER_MSG) + pbc_addr));
      PBC_Tx_Msg.Reg_Addr_Lo = (unsigned char)(PBC_Register_Address & 0x00FF);
      PBC_Tx_Msg.Reg_Addr_Hi = (unsigned char)(PBC_Register_Address >> 8);
      Set_Tx_Address_Info(PBC_REGISTER_START_ADDR,device_addr,pbc_addr);
      
      unsigned int Error_Check = CRC16(&PBC_Tx_Msg, 6);
      PBC_Tx_Msg.Error_Check_Lo = (unsigned char)(Error_Check &0x00FF);
      PBC_Tx_Msg.Error_Check_Hi = (unsigned char)(Error_Check >> 8);      
      // Transmit msg to PBC
      status |= Send_Receive_PBC_Data(PBC_Tx_Msg);  
//    }
//  }
  PBC_Config_Flag.Config_Wait_Count = 0;
//  // Set interrupt flag to send out data.
//  Set_Uart_Int_Flag(0);
//  clear device id failure flag
}

/***************************************************************************//**
 * @param Function      Get_PBC_Channel_Current 
 * @param Input         device_addr, pbc_addr
 * @param Return        void
 * @param Calling_Func  main
 * @param Description   Function send commands to PBC to get Hybrid channel current  
 ******************************************************************************/
unsigned char Get_PBC_Channel_Current(unsigned char device_addr, unsigned char pbc_addr)
{
  unsigned status = 0;
  unsigned int PBC_Channel_Address;
  // Sent message on all the address to known setpoint
  PBC_Tx_Msg.Slave_Addr = PBC_SLAVE_ADDR;
  PBC_Tx_Msg.Function = READ_INPUT_REGISTERS;
   
  // Record Previous RX Msg Raw OVP data value to Set voltage
  PBC_Tx_Msg.Write_Data_Hi  = (QUALITY_OF_REGISTER >> 8);
  PBC_Tx_Msg.Write_Data_Lo = (QUALITY_OF_REGISTER & 0xFF);
  PBC_Tx_Msg.Alarm = 0x00; 
    
  PBC_Channel_Address =  (PBC_CHANNEL_CURRENT_START_ADDR + ((device_addr * MAX_OVP_PER_MSG) + pbc_addr));
  PBC_Tx_Msg.Reg_Addr_Lo = (unsigned char)(PBC_Channel_Address & 0x00FF);
  PBC_Tx_Msg.Reg_Addr_Hi = (unsigned char)(PBC_Channel_Address >> 8);
  Set_Tx_Address_Info(PBC_CHANNEL_CURRENT_START_ADDR,device_addr,pbc_addr);
  
  unsigned int Error_Check = CRC16(&PBC_Tx_Msg, 6);
  PBC_Tx_Msg.Error_Check_Lo = (unsigned char)(Error_Check &0x00FF);
  PBC_Tx_Msg.Error_Check_Hi = (unsigned char)(Error_Check >> 8);      
  // Transmit msg to PBC
  status |= Send_Receive_PBC_Data(PBC_Tx_Msg);  
  return status;
}

/***************************************************************************//**
 * @param Function      Get_PBC_Channel_Status 
 * @param Input         device_addr, pbc_addr
 * @param Return        unsigned char
 * @param Calling_Func  main
 * @param Description   Function send commands to PBC to get Hybrid channel current  
 ******************************************************************************/
unsigned char Get_PBC_Channel_Status(unsigned char device_addr, unsigned char pbc_addr)
{
  unsigned status = 0;
  unsigned int PBC_Channel_Status_Address;
  // Sent message on all the address to known setpoint
  PBC_Tx_Msg.Slave_Addr = PBC_SLAVE_ADDR;
  PBC_Tx_Msg.Function = READ_INPUT_STATUS;
  
  // Record Previous RX Msg Raw OVP data value to Set voltage
  PBC_Tx_Msg.Write_Data_Hi  = (QUALITY_OF_REGISTER >> 8);
  PBC_Tx_Msg.Write_Data_Lo = (QUALITY_OF_REGISTER & 0xFF);
  PBC_Tx_Msg.Alarm = 0x00; 
    
  PBC_Channel_Status_Address =  (PBC_CHANNEL_STATUS_ADDR + ((device_addr * MAX_OVP_PER_MSG) + pbc_addr));
  PBC_Tx_Msg.Reg_Addr_Lo = (unsigned char)(PBC_Channel_Status_Address & 0x00FF);
  PBC_Tx_Msg.Reg_Addr_Hi = (unsigned char)(PBC_Channel_Status_Address >> 8);
  Set_Tx_Address_Info(PBC_CHANNEL_STATUS_ADDR,device_addr,pbc_addr);
  
  unsigned int Error_Check = CRC16(&PBC_Tx_Msg, 6);
  PBC_Tx_Msg.Error_Check_Lo = (unsigned char)(Error_Check &0x00FF);
  PBC_Tx_Msg.Error_Check_Hi = (unsigned char)(Error_Check >> 8);      
  // Transmit msg to PBC
  status = Send_Receive_PBC_Data(PBC_Tx_Msg); 
  return status;
}

/***************************************************************************//**
 * @param Function      Get_PBC_RRH_Channel_Map 
 * @param Input         device_addr, pbc_addr
 * @param Return        unsigned char
 * @param Calling_Func  main
 * @param Description   Function send commands to PBC controller ro get RRH 
 *                      mapping information
 ******************************************************************************/
unsigned char Get_PBC_RRH_Channel_Map(unsigned char device_addr, unsigned char pbc_addr)
{
  unsigned status = 0;
  unsigned int PBC_RRH_Channel_Address;
  // Sent message on all the address to known setpoint
  PBC_Tx_Msg.Slave_Addr = PBC_SLAVE_ADDR;
  PBC_Tx_Msg.Function = READ_INPUT_REGISTERS;

  // Record Previous RX Msg Raw OVP data value to Set voltage
  PBC_Tx_Msg.Write_Data_Hi  = (QUALITY_OF_REGISTER >> 8);
  PBC_Tx_Msg.Write_Data_Lo = (QUALITY_OF_REGISTER & 0xFF);
  PBC_Tx_Msg.Alarm = 0x00; 
    
  PBC_RRH_Channel_Address =  (PBC_PBC_RRH_CHANNEL_SET_ADDR + ((device_addr * MAX_OVP_PER_MSG) + pbc_addr));
  PBC_Tx_Msg.Reg_Addr_Lo = (unsigned char)(PBC_RRH_Channel_Address & 0x00FF);
  PBC_Tx_Msg.Reg_Addr_Hi = (unsigned char)(PBC_RRH_Channel_Address >> 8);
  Set_Tx_Address_Info(PBC_PBC_RRH_CHANNEL_SET_ADDR,device_addr,pbc_addr);
  
  unsigned int Error_Check = CRC16(&PBC_Tx_Msg, 6);
  PBC_Tx_Msg.Error_Check_Lo = (unsigned char)(Error_Check &0x00FF);
  PBC_Tx_Msg.Error_Check_Hi = (unsigned char)(Error_Check >> 8);      
  // Transmit msg to PBC
  status = Send_Receive_PBC_Data(PBC_Tx_Msg); 
  return status;
}

/***************************************************************************//**
 * @param Function      Get_PBC_Channel_Info 
 * @param Input         device_addr, pbc_addr
 * @param Return        void
 * @param Calling_Func  main
 * @param Description   Function send commands to PBC to get Hybrid channel current  
 ******************************************************************************/

void Get_PBC_Channel_Info(unsigned char device_addr, unsigned char pbc_addr)
{
  Get_PBC_Channel_Status(device_addr,pbc_addr);       
  Set_PBC_Vref_Setpoint(device_addr,pbc_addr);
  Get_PBC_RRH_Channel_Map(device_addr,pbc_addr);
  Get_PBC_Channel_Current(device_addr,pbc_addr);  
}

/***************************************************************************//**
 * @param Function      Get_PBC_Channel_Info 
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  main
 * @param Description   Function send commands to PBC to get Hybrid channel current  
 ******************************************************************************/

void Register_PBC_Channel(unsigned char device_addr, unsigned char pbc_addr)
{ 
  unsigned char count; 
  unsigned char cal_ovp_id = (pbc_addr & 0xFE);
  for (count = 0; count < 2; count ++)
  {
    Get_PBC_Channel_Info(device_addr,cal_ovp_id+count);
  }
  
  // if any of 2 combined channel is valid
  if ((Previous_Rx_Msg.PBC_Controller_Info[device_addr][cal_ovp_id].Configured_OVP_Flag == 0) | 
      (Previous_Rx_Msg.PBC_Controller_Info[device_addr][cal_ovp_id + 1].Configured_OVP_Flag == 0))
  {       
    // Bypass automapping, if Mapping == OFF
    #if (MAPPING == OFF)
    {
      // check if Hybrid channels redundant 
      unsigned int Linked_Addr;
      
      Linked_Addr = ((Previous_Rx_Msg.PBC_Controller_Info[device_addr][pbc_addr].Linked_RRH_High * 256) + Previous_Rx_Msg.PBC_Controller_Info[device_addr][pbc_addr].Linked_RRH_Low);
      if (Linked_Addr == 0xFFFF)
      {
        // Channel A & B connected; Channel A, B on brick 1 connected to Channel B, A on brick 2
        // load mapping structure with respective
        Previous_Rx_Msg.PBC_Controller_Info[device_addr][pbc_addr].Adj_Ch_Paired_Flag = FALSE;
        Previous_Rx_Msg.PBC_Controller_Info[device_addr][pbc_addr].Configured_OVP_Flag = 0;

        Previous_Rx_Msg.PBC_Register_Map[device_addr][pbc_addr].Msg_OVP_Id_Avg[0] = 0x00;
        Previous_Rx_Msg.PBC_Register_Map[device_addr][pbc_addr].Msg_OVP_Id_Avg[1] = 0x00;
        
        Previous_Rx_Msg.PBC_Register_Map[device_addr][pbc_addr].PBC_Module_address[0] = 0xFF;
        Previous_Rx_Msg.PBC_Register_Map[device_addr][pbc_addr].PBC_Module_address[1] =  0xFF;   
      }
      else if(Linked_Addr == 0x00) 
      {
        // Channel A & B connected; Channel A, B on brick 1 connected to Channel B, A on brick 2
        // load mapping structure with respective
        Previous_Rx_Msg.PBC_Controller_Info[device_addr][pbc_addr].Adj_Ch_Paired_Flag = FALSE;
        
        Previous_Rx_Msg.PBC_Register_Map[device_addr][pbc_addr].Msg_OVP_Id_Avg[0] = cal_ovp_id;
        Previous_Rx_Msg.PBC_Register_Map[device_addr][pbc_addr].Msg_OVP_Id_Avg[1] = cal_ovp_id;
        
        Previous_Rx_Msg.PBC_Register_Map[device_addr][pbc_addr].PBC_Module_address[0] = (PBC_REGISTER_START_ADDR + ((device_addr * MAX_OVP_PER_MSG) + pbc_addr));
        Previous_Rx_Msg.PBC_Register_Map[device_addr][pbc_addr].PBC_Module_address[1] =  Previous_Rx_Msg.PBC_Register_Map[device_addr][pbc_addr].PBC_Module_address[0];
      } 
      else
      {
        // the devices not redundant
        // load mapping structure with respective values
        Previous_Rx_Msg.PBC_Controller_Info[device_addr][pbc_addr].Adj_Ch_Paired_Flag = TRUE;
        Previous_Rx_Msg.PBC_Controller_Info[device_addr][cal_ovp_id+1].Adj_Ch_Paired_Flag = TRUE;
        
        Previous_Rx_Msg.PBC_Register_Map[device_addr][pbc_addr].Msg_OVP_Id_Avg[0] = cal_ovp_id;
        Previous_Rx_Msg.PBC_Register_Map[device_addr][pbc_addr].Msg_OVP_Id_Avg[1] = cal_ovp_id + 1;
        
        Previous_Rx_Msg.PBC_Register_Map[device_addr][cal_ovp_id].PBC_Module_address[0] = (PBC_REGISTER_START_ADDR + ((device_addr * MAX_OVP_PER_MSG) + pbc_addr));
        Previous_Rx_Msg.PBC_Register_Map[device_addr][cal_ovp_id].PBC_Module_address[1] = Previous_Rx_Msg.PBC_Register_Map[device_addr][cal_ovp_id].PBC_Module_address[0] + 1;
      }
      Previous_Rx_Msg.PBC_Register_Map[device_addr][cal_ovp_id + 1].PBC_Module_address[0] = Previous_Rx_Msg.PBC_Register_Map[device_addr][cal_ovp_id].PBC_Module_address[0];
      Previous_Rx_Msg.PBC_Register_Map[device_addr][cal_ovp_id + 1].PBC_Module_address[1] =  Previous_Rx_Msg.PBC_Register_Map[device_addr][cal_ovp_id].PBC_Module_address[1];
      Previous_Rx_Msg.PBC_Controller_Info[device_addr][Previous_Rx_Msg.PBC_Register_Map[device_addr][pbc_addr].Msg_OVP_Id_Avg[0]].Configured_OVP_Flag = TRUE; 
      Previous_Rx_Msg.PBC_Controller_Info[device_addr][Previous_Rx_Msg.PBC_Register_Map[device_addr][pbc_addr].Msg_OVP_Id_Avg[1]].Configured_OVP_Flag = TRUE; 
    }
    #endif
  }
}

/***************************************************************************//**
 * @param Function      Setup_PBC 
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  main
 * @param Description   Function send commands to PBC to get Hybrid channel current  
 ******************************************************************************/
void Setup_PBC()
{
  // Get PBC RRH channel mapping, Setup PBC, 
  unsigned char device_addr, pbc_addr;
  for (device_addr = 0; device_addr < MAX_DEVICE_ID; device_addr++)
  {
    // We dont need to individualy configure each PBC, as one brick can output multiple channel
    for (pbc_addr = 0; pbc_addr < MAX_OVP_PER_MSG; pbc_addr++) 
    {  
      Register_PBC_Channel(device_addr,pbc_addr);
    }
  }
}

/***************************************************************************//**
 * @param Function      Process_OVP
 * @param Input         char, char
 * @param Return        void
 * @param Calling_Func  
 * @param Description   Function programs spcific PBC and its reference channel
 ******************************************************************************/
void Process_OVP(unsigned char id, unsigned char ovp_id)
{
  // Local variable define
  unsigned char status,Send_PBC_Data = 0;
  unsigned int OVP_Failure_Flag = 0x0001;
  unsigned int PBC_Register_Address, Error_Check;
  unsigned char ovp_paired_id;
  // Calculate ovp eqivalent voltage
  unsigned int Current_OVP_Value,Paired_OVP_Value;
  unsigned int Setpoint_Deviation, New_Setpoint;
  unsigned int Previous_PBC_Setpoint;
  unsigned int Previous_Paired_PBC_Setpoint;
//  unsigned int TowerTop_Max_Bound = (TOWERTOP_TARGET_VOLTAGE + OVP_TOLERANCE_WINDOW);
//  unsigned int TowerTop_Min_Bound = (TOWERTOP_TARGET_VOLTAGE - OVP_TOLERANCE_WINDOW);  
  Current_OVP_Value = ((Rx_Msg.Data_Msg[ovp_id].Reg_data_h *256) + Rx_Msg.Data_Msg[ovp_id].Reg_data_l);
  Previous_PBC_Setpoint = ((Previous_Rx_Msg.OVP_Raw_Tx_Data[id][ovp_id].Reg_data_h * 256) + Previous_Rx_Msg.OVP_Raw_Tx_Data[id][ovp_id].Reg_data_l);
  
     
  // Calculate paired OVP_id
  if (Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Adj_Ch_Paired_Flag == TRUE)
  {
    // Update OVP to new ovp_id
    if (ovp_id == Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Avg[0])
    {
      ovp_paired_id = Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Avg[1];
    }
    else
    {
      ovp_paired_id = Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Avg[0];  
    }
    Paired_OVP_Value = ((Rx_Msg.Data_Msg[ovp_paired_id].Reg_data_h *256) + Rx_Msg.Data_Msg[ovp_paired_id].Reg_data_l);
    Previous_Paired_PBC_Setpoint = ((Previous_Rx_Msg.OVP_Raw_Tx_Data[id][ovp_paired_id].Reg_data_h * 256) + Previous_Rx_Msg.OVP_Raw_Tx_Data[id][ovp_paired_id].Reg_data_l);  
  }
  else
  {
    ovp_paired_id = ovp_id;
    Paired_OVP_Value = Current_OVP_Value;
  }
  // Check if OVP output stable and ready to process another data.
  if (Previous_Rx_Msg.OVP_Processed_Flag[id][ovp_id] == 0)
  {
    Previous_Rx_Msg.OVP_Processed_Flag[id][ovp_id] = Previous_Rx_Msg.OVP_Processed_Flag[id][ovp_id]+ OVP_WAIT_COUNT;
    Previous_Rx_Msg.OVP_Processed_Flag[id][ovp_paired_id] =   Previous_Rx_Msg.OVP_Processed_Flag[id][ovp_id]; 
   
    // Write error or register mismatch
    if ((Previous_PBC_Setpoint != Previous_Paired_PBC_Setpoint) & (Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Adj_Ch_Paired_Flag == TRUE))
    {
      New_Setpoint = TOWERTOP_TARGET_VOLTAGE;
      Send_PBC_Data =1;
    }
    // check if paired data and current data are within bound, process data 
    if (((Current_OVP_Value >= OVP_LOW_BOUND) & (Current_OVP_Value <= OVP_HIGH_BOUND)) &
        ((Paired_OVP_Value >= OVP_LOW_BOUND) & (Paired_OVP_Value <= OVP_HIGH_BOUND)))
    {
      // RRH Valid voltage range and regulated - No change required
      if (((Current_OVP_Value >= (TOWERTOP_TARGET_VOLTAGE - OVP_TOLERANCE_WINDOW)) & (Current_OVP_Value <= (TOWERTOP_TARGET_VOLTAGE + OVP_TOLERANCE_WINDOW))) &
          ((Paired_OVP_Value >= (TOWERTOP_TARGET_VOLTAGE - OVP_TOLERANCE_WINDOW)) & (Paired_OVP_Value <= (TOWERTOP_TARGET_VOLTAGE + OVP_TOLERANCE_WINDOW))))
      {
        RC_Msg_Error.Failed_Packet_Count[id][ovp_id] = 0x00;
        RC_Msg_Error.OVP_Id_Failure_Flag[id][ovp_id] &= ~(OVP_Failure_Flag << ovp_id);
        RC_Msg_Error.Failed_Packet_Count[id][ovp_paired_id] = 0x00;
        RC_Msg_Error.OVP_Id_Failure_Flag[id][ovp_paired_id] &= ~(OVP_Failure_Flag << ovp_id);
        Send_PBC_Data =0;
      }  
      // check if current OVP is within regualtion window
      else if ((Current_OVP_Value > (TOWERTOP_TARGET_VOLTAGE - OVP_TOLERANCE_WINDOW)) & (Paired_OVP_Value > (TOWERTOP_TARGET_VOLTAGE - OVP_TOLERANCE_WINDOW)))
      {
        // Increase voltage as it is not connected to anything
        Setpoint_Deviation = VOLTAGE_STEP ;
        New_Setpoint = Previous_PBC_Setpoint - Setpoint_Deviation;
        Send_PBC_Data =1;
      }
      // check if both current and paired OVP are lower than reference 
      else if ((Current_OVP_Value < (TOWERTOP_TARGET_VOLTAGE + OVP_TOLERANCE_WINDOW)) & (Paired_OVP_Value < (TOWERTOP_TARGET_VOLTAGE + OVP_TOLERANCE_WINDOW)))
      {
        // Increase voltage as it is not connected to anything
        Setpoint_Deviation = VOLTAGE_STEP ;
        New_Setpoint = Previous_PBC_Setpoint + Setpoint_Deviation;
        Send_PBC_Data =1;
      }
      // one of the voltage unregulated
      else
      {
        if((Current_OVP_Value  > OVP_HIGH_BOUND) | (Paired_OVP_Value  > OVP_HIGH_BOUND))
        {
          // Increase voltage the two channel have different loading
          Setpoint_Deviation = (VOLTAGE_STEP << 1) ;
          New_Setpoint = Previous_PBC_Setpoint - Setpoint_Deviation;
        }
        else 
        {
          // Increase voltage the two channel have different loading
          Setpoint_Deviation = VOLTAGE_STEP ;
          New_Setpoint = Previous_PBC_Setpoint + Setpoint_Deviation;
        }
        Send_PBC_Data =1;
      }
      
    }    
    else if((Current_OVP_Value == 0x0000) & (Current_OVP_Value == 0xFFFF)) 
    {
      #if defined (DEBUG_PROTOCOL)
      printf("OUT OF BND\n");
      #endif    
      Send_PBC_Data = 0;
      Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Configured_OVP_Flag = 0;
      Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Invalid_OVP_Flag = 1;
      
      if (Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Adj_Ch_Paired_Flag == TRUE)
      {
        Previous_Rx_Msg.PBC_Controller_Info[id][ovp_paired_id].Configured_OVP_Flag = 0;
        Previous_Rx_Msg.PBC_Controller_Info[id][ovp_paired_id].Invalid_OVP_Flag = 1;
      }
    }
    else if (((Current_OVP_Value < OVP_LOW_BOUND) | (Current_OVP_Value > OVP_HIGH_BOUND)) |
             ((Paired_OVP_Value < OVP_LOW_BOUND) | (Paired_OVP_Value > OVP_HIGH_BOUND)))
    {
      if (((Current_OVP_Value < OVP_LOW_BOUND) & (Paired_OVP_Value < OVP_LOW_BOUND)) | 
          ((Current_OVP_Value > OVP_HIGH_BOUND) & (Paired_OVP_Value > OVP_HIGH_BOUND)))
      {
        #if defined (DEBUG_PROTOCOL)
        printf("UKWN BND\n");
        #endif 
        
        New_Setpoint = PBC_REF_VOLTAGE; 
        //New_Setpoint = PBC_REF_VOLTAGE;
        Send_PBC_Data = 1;
        // Set failure count 
        RC_Msg_Error.Failed_Packet_Count[id][ovp_id]++;
        if ((Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Adj_Ch_Paired_Flag == TRUE))
        {
          RC_Msg_Error.Failed_Packet_Count[id][ovp_paired_id]++;
        }
        // check fault flag if more than MAX ERROR Count number of cycles then flag
        if ((RC_Msg_Error.Failed_Packet_Count[id][ovp_id] > MAX_ERROR_COUNT) |
            (RC_Msg_Error.Failed_Packet_Count[id][ovp_paired_id] > MAX_ERROR_COUNT))
        {
          RC_Msg_Error.OVP_Id_Failure_Flag[id][ovp_id] |= (OVP_Failure_Flag << ovp_id);
          Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Configured_OVP_Flag = 0;
          Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Invalid_OVP_Flag = 1;
          if (Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Adj_Ch_Paired_Flag == TRUE)
          {
            Previous_Rx_Msg.PBC_Controller_Info[id][ovp_paired_id].Configured_OVP_Flag = 0;
            Previous_Rx_Msg.PBC_Controller_Info[id][ovp_paired_id].Invalid_OVP_Flag = 1;
          }
        }
      }
      // Paired channel out of bound, but current voltage valid, neglect paired channel
      else if ((OVP_HIGH_BOUND < Paired_OVP_Value) | (OVP_HIGH_BOUND < Current_OVP_Value) )
      {
        Setpoint_Deviation = (VOLTAGE_STEP  << 1);
        New_Setpoint = Previous_PBC_Setpoint - Setpoint_Deviation;
        Send_PBC_Data = 1;
      }
      // Current channel out of bound but Alternate/Paired voltage valid, neglect current channel   
      else if ((Current_OVP_Value < OVP_LOW_BOUND) | (Paired_OVP_Value > OVP_LOW_BOUND))
      {
        Setpoint_Deviation = VOLTAGE_STEP;
        New_Setpoint = Previous_PBC_Setpoint + Setpoint_Deviation;
        Send_PBC_Data = 1;
      }
      // if only paired value out of bound
      else if ((OVP_HIGH_BOUND > Current_OVP_Value) & (OVP_LOW_BOUND < Current_OVP_Value))
      {
        if (Current_OVP_Value > (PBC_REF_VOLTAGE + OVP_TOLERANCE_WINDOW))
        {
          Setpoint_Deviation = VOLTAGE_STEP;
          New_Setpoint = Previous_PBC_Setpoint - Setpoint_Deviation;
          Send_PBC_Data = 1;
        }
        else if (Current_OVP_Value < (PBC_REF_VOLTAGE - OVP_TOLERANCE_WINDOW))
        {
          Setpoint_Deviation = VOLTAGE_STEP;
          New_Setpoint = Previous_PBC_Setpoint + Setpoint_Deviation;
          Send_PBC_Data = 1;
        }
        else
        {
          Send_PBC_Data = 0;
        }
      }
      // if only current value out of bound
      else if ((OVP_HIGH_BOUND > Paired_OVP_Value) & (OVP_LOW_BOUND < Paired_OVP_Value) )
      {
        if (Paired_OVP_Value > PBC_REF_VOLTAGE + OVP_TOLERANCE_WINDOW)
        {
          Setpoint_Deviation = VOLTAGE_STEP;
          New_Setpoint = Previous_PBC_Setpoint - Setpoint_Deviation;
          Send_PBC_Data = 1;
        }
        else if (Paired_OVP_Value < PBC_REF_VOLTAGE - OVP_TOLERANCE_WINDOW)
        {
          Setpoint_Deviation = VOLTAGE_STEP;
          New_Setpoint = Previous_PBC_Setpoint + Setpoint_Deviation;
          Send_PBC_Data = 1;
        }
        else
        {
          Send_PBC_Data = 0;
        }
      }
      // if any failure exception missing
      else
      {
        New_Setpoint = PBC_REF_VOLTAGE; 
        //New_Setpoint = PBC_REF_VOLTAGE;
        Send_PBC_Data = 1;
      }   
    } 
    // if any overall condition missed
    else
    {
      New_Setpoint = PBC_REF_VOLTAGE; 
      //New_Setpoint = PBC_REF_VOLTAGE;
      Send_PBC_Data = 1;
    }  
    
    if(Send_PBC_Data)
    {
      // Send message to PBC of setpoint needs to be updated      
      PBC_Tx_Msg.Slave_Addr = PBC_SLAVE_ADDR;
      PBC_Tx_Msg.Function = WRITE_SINGLE_REGISTER;
          
      if (New_Setpoint < PBC_MIN_VOLTAGE)
      {
        New_Setpoint = PBC_MIN_VOLTAGE;
      }
      else if (New_Setpoint > PBC_MAX_VOLTAGE)
      {
        New_Setpoint = PBC_MAX_VOLTAGE;
      }

      // Store Raw OVP data 
      unsigned char channel; 
      for (channel = 0; channel < CH_PER_BRICK; channel ++)
      {
        Previous_Rx_Msg.OVP_Raw_Tx_Data[id][ovp_id].Reg_data_h = (New_Setpoint >> 8);
        Previous_Rx_Msg.OVP_Raw_Tx_Data[id][ovp_id].Reg_data_l = (New_Setpoint & 0x00FF);
        Previous_Rx_Msg.OVP_Raw_Tx_Data[id][ovp_id].Reg_Flag = Previous_Rx_Msg.OVP_Raw_Rx_Data[id][ovp_id].Reg_Flag;
          
        // Prepare the message 
        PBC_Tx_Msg.Write_Data_Hi = Previous_Rx_Msg.OVP_Raw_Tx_Data[id][ovp_id].Reg_data_h;
        PBC_Tx_Msg.Write_Data_Lo = Previous_Rx_Msg.OVP_Raw_Tx_Data[id][ovp_id].Reg_data_l;
        PBC_Tx_Msg.Alarm = Previous_Rx_Msg.OVP_Raw_Tx_Data[id][ovp_id].Reg_Flag;
        
  //      PBC_Register_Address =  (PBC_REGISTER_START_ADDR + ((id * MAX_OVP_PER_MSG)+ ovp_id));
        PBC_Register_Address = Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].PBC_Module_address[channel];
        PBC_Tx_Msg.Reg_Addr_Lo = (unsigned char)(PBC_Register_Address & 0x00FF);
        PBC_Tx_Msg.Reg_Addr_Hi = (unsigned char)(PBC_Register_Address >> 8);
        Set_Tx_Address_Info(PBC_REGISTER_START_ADDR,id,ovp_id);
        
        Error_Check = CRC16(&PBC_Tx_Msg, 6);
        PBC_Tx_Msg.Error_Check_Lo = (unsigned char)(Error_Check &0x00FF);
        PBC_Tx_Msg.Error_Check_Hi = (unsigned char)(Error_Check >> 8);
        status |= Send_Receive_PBC_Data(PBC_Tx_Msg);
        // clear processed ovp_id
        Rx_Msg.Data_Msg[ovp_id].Reg_data_h = 0x00;
        Rx_Msg.Data_Msg[ovp_id].Reg_data_l = 0x00;
        Rx_Msg.Data_Msg[ovp_id].Reg_Flag = 0x00;
        
        if (CH_PER_BRICK == 1)
        {
          if (Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Adj_Ch_Paired_Flag == TRUE)
          {
            ovp_id = ovp_paired_id;
          }
          else
          {
            channel++;
          }
        }
      }
    }  
  }
  else
  {
    Previous_Rx_Msg.OVP_Processed_Flag[id][ovp_id]--;
    Previous_Rx_Msg.OVP_Processed_Flag[id][ovp_paired_id] = Previous_Rx_Msg.OVP_Processed_Flag[id][ovp_id];
  } 
}  


/***************************************************************************//**
 * @param Function      Configure_OVP
 * @param Input         char, char
 * @param Return        char
 * @param Calling_Func  
 * @param Description   Function maps/configures PBC on power up/reset for 
 *                      mapping OVP with surge protection address
 ******************************************************************************/
//#if defined MAPPING
unsigned char Configure_OVP(unsigned char id, unsigned char ovp_id)
{
//  // Disable OVP message uart
//  Disable_Uart_Rx_Int(2);

//  // if mapping value changes or updates configure network   
//  // Disable OVP message uart
//  Enable_Uart_Rx_Int(2);
  unsigned status = FALSE;
  
  // Check if device  OVP_id config busy
  if (PBC_Config_Flag.Config_Busy_flag == FALSE)
  {
    // Check the Mapping log from PBC controller
    unsigned char ovp_count;
    //Check of any other port mapping changed
    for (ovp_count = 0; ovp_count < MAX_OVP_PER_MSG; ovp_count++)
    {
      Get_PBC_Channel_Info(id,ovp_count);
    }
    PBC_Config_Flag.Config_Busy_flag = TRUE;
    // Set the device on that OVP to go high the device configured first time
    Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Set_High_Flag = TRUE; 
    // Clear the PBC [id] & [ovp_id] corresponding to PBC address  
    Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_High[0] = 0;
    Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Low[0] = 0;
    Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Avg[0] = 0;
    // clear the buff
    Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_High[1] = 0;
    Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Low[1] = 0;
    Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Avg[1] = 0;
   
    // Set PBC to Max voltage
    PBC_Tx_Msg.Write_Data_Hi = (unsigned char)((PBC_REF_VOLTAGE + PBC_CONFIG_DELTA) >> 8);
    PBC_Tx_Msg.Write_Data_Lo = (unsigned char)((PBC_REF_VOLTAGE + PBC_CONFIG_DELTA) & 0x00FF);
    //PBC_Tx_Msg.Alarm = PBC_Rx_Msg.Alarm;
    PBC_Tx_Msg.Alarm = 0; // no alram during config
    Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Set_High_Flag = TRUE;
    
    PBC_Config_Flag.Config_Busy_id = id;
    PBC_Config_Flag.Config_Busy_ovp_id = ovp_id;     
  }
  // Check if the device was set high 
  else if (Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Set_High_Flag)
  {
    Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Set_High_Flag = FALSE;
    // Monitor and record the OVP with voltage change high
    
    // If current element is smaller than first then update both first and second
    unsigned int Ovp_value,Ovp_value_1, Ovp_value_2 = 0x0000;
    // Ovp_value_1 = PBC_REF_VOLTAGE + PBC_CONFIG_DELTA - OVP_TOLERANCE_WINDOW;
    Ovp_value_1 = OVP_LOW_BOUND - OVP_TOLERANCE_WINDOW; // lowest voltage with full load
    if(Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Adj_Ch_Paired_Flag == TRUE)
    {
      // Ovp_value_2 = PBC_REF_VOLTAGE + PBC_CONFIG_DELTA - OVP_TOLERANCE_WINDOW;
      Ovp_value_2 = OVP_LOW_BOUND - OVP_TOLERANCE_WINDOW; // lowest voltage at full load
    }
    unsigned char count;
    for (count = 0; count < MAX_OVP_PER_MSG; count ++)
    {
      if(Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Configured_OVP_Flag != 1)
      {
        Ovp_value = ((Rx_Msg.Data_Msg[count].Reg_data_h * 256) + Rx_Msg.Data_Msg[count].Reg_data_l);
        if ((Ovp_value > (OVP_LOW_BOUND - OVP_TOLERANCE_WINDOW)) & (Ovp_value < (OVP_HIGH_BOUND + OVP_TOLERANCE_WINDOW)))
        {
          if (Ovp_value >= Ovp_value_1)
          {
            if(Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Adj_Ch_Paired_Flag == TRUE)
            {
              Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_High[1] = Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_High[0];
              Ovp_value_2 = Ovp_value_1;
              Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_High[0] = count;
              Ovp_value_1 = Ovp_value;
            }
            else
            {
              Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_High[0] = count;
              Ovp_value_1 = Ovp_value;
            } 
          }
          else if (Ovp_value > Ovp_value_2) 
          {
            /* If value is in between first and second then update second  */
            Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_High[1] = count;
            Ovp_value_2 = Ovp_value;
          }
        }
      }
    }
    // set the device on that OVP to go low
    // Set PBC to Min voltage
    PBC_Tx_Msg.Write_Data_Hi = (unsigned char)((PBC_REF_VOLTAGE - PBC_CONFIG_DELTA)  >> 8);
    PBC_Tx_Msg.Write_Data_Lo = (unsigned char)((PBC_REF_VOLTAGE - PBC_CONFIG_DELTA) & 0x00FF);
    //PBC_Tx_Msg.Alarm = PBC_Rx_Msg.Alarm;
    PBC_Tx_Msg.Alarm = 0; // no alram during config
    Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Set_Low_Flag = TRUE;
  }
  else if (Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Set_Low_Flag)
  {
    Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Set_Low_Flag = FALSE;
    
    // If current element is smaller than first then update both first and second
    // If current element is smaller than first then update both first and second
    unsigned int Ovp_value,Ovp_value_1, Ovp_value_2 = 0xFFFF;
    // Ovp_value_1 = PBC_REF_VOLTAGE + PBC_CONFIG_DELTA - OVP_TOLERANCE_WINDOW;
    Ovp_value_1 = PBC_MAX_VOLTAGE; // max voltage with no load
    if(Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Adj_Ch_Paired_Flag == TRUE)
    {
      // Ovp_value_2 = PBC_REF_VOLTAGE + PBC_CONFIG_DELTA - OVP_TOLERANCE_WINDOW;
      Ovp_value_2 = PBC_MAX_VOLTAGE; // max voltage at no load
    }
    unsigned char count;
    for (count = 0; count < MAX_OVP_PER_MSG; count ++)
    {
      if(Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Configured_OVP_Flag != 1)
      {
        Ovp_value = ((Rx_Msg.Data_Msg[count].Reg_data_h * 256) + Rx_Msg.Data_Msg[count].Reg_data_l);
        if ((Ovp_value > (OVP_LOW_BOUND - OVP_TOLERANCE_WINDOW)) & (Ovp_value < (OVP_HIGH_BOUND + OVP_TOLERANCE_WINDOW)))
        {
          if (Ovp_value <= Ovp_value_1) 
          {
            if(Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Adj_Ch_Paired_Flag == TRUE)
            {
              Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Low[1] = Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Low[0];
              Ovp_value_2 = Ovp_value_1;
              Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Low[0] = count;
              Ovp_value_1 = Ovp_value;
            }
            else
            {
              Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Low[0] = count;
              Ovp_value_1 = Ovp_value;
            } 
          }
          else if (Ovp_value < Ovp_value_2) 
          {
            /* If value is in between first and second  then update second  */
            Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Low[1] = count;
            Ovp_value_2 = Ovp_value;
          }
        }
      }
    }
    // set the device on that OVP to go low
    // Set PBC to Min voltage
    PBC_Tx_Msg.Write_Data_Hi = (unsigned char)(PBC_REF_VOLTAGE  >> 8);
    PBC_Tx_Msg.Write_Data_Lo = (unsigned char)(PBC_REF_VOLTAGE & 0x00FF);
    //PBC_Tx_Msg.Alarm = PBC_Rx_Msg.Alarm;
    PBC_Tx_Msg.Alarm = 0; // no alram during config
    Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Set_Avg_Flag = TRUE; 
    
  }
  else if (Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Set_Avg_Flag)
  {
    Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Set_Avg_Flag = FALSE;
    // Compare if the same ovp responded to commands
    if(((Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Adj_Ch_Paired_Flag == TRUE)
    &   (((((Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Low[0]) == (Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_High[0])) 
    &   ((Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Low[1]) == (Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_High[1]))))
    |   (((Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Low[1]) == (Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_High[0])) 
    &   ((Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Low[0]) == (Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_High[1])))))       
    | ((Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Adj_Ch_Paired_Flag == FALSE) 
    & ((Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Low[0]) == (Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_High[0]))))
    {
      Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Avg[0] = Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Low[0];
      Previous_Rx_Msg.PBC_Controller_Info[id][Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Avg[0]].Configured_OVP_Flag = TRUE; 
      Previous_Rx_Msg.PBC_Register_Map[id][Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Avg[0]].PBC_Module_address[0] = ((PBC_Tx_Msg.Reg_Addr_Hi *256) + (PBC_Tx_Msg.Reg_Addr_Lo & 0xFE));
      Previous_Rx_Msg.PBC_Register_Map[id][Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Avg[0]].PBC_Module_address[1] = ((PBC_Tx_Msg.Reg_Addr_Hi *256) + (PBC_Tx_Msg.Reg_Addr_Lo | 0x01));
      if (Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Adj_Ch_Paired_Flag == TRUE)
      {
        Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Avg[1] = Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Low[1];
        Previous_Rx_Msg.PBC_Controller_Info[id][Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Avg[1]].Configured_OVP_Flag = TRUE; 
        Previous_Rx_Msg.PBC_Register_Map[id][Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Avg[1]].PBC_Module_address[0] = Previous_Rx_Msg.PBC_Register_Map[id][Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Avg[0]].PBC_Module_address[0];
        Previous_Rx_Msg.PBC_Register_Map[id][Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Avg[1]].PBC_Module_address[1] = Previous_Rx_Msg.PBC_Register_Map[id][Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Avg[0]].PBC_Module_address[0] + 1;
      }
      if (ovp_id == Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Low[0])
      {
        Previous_Rx_Msg.PBC_Register_Map[id][Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Low[1]].Msg_OVP_Id_Avg[0] = Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Avg[0];
        Previous_Rx_Msg.PBC_Register_Map[id][Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Low[1]].Msg_OVP_Id_Avg[1] = Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Avg[1];
      }
      else // ovp_id == Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Low[1];
      {
        Previous_Rx_Msg.PBC_Register_Map[id][Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Low[0]].Msg_OVP_Id_Avg[0] = Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Avg[0];
        Previous_Rx_Msg.PBC_Register_Map[id][Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Low[0]].Msg_OVP_Id_Avg[1] = Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Avg[1];
      }
      // clear high low register
      Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Low[0] = 0;
      Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_High[0] = 0;
      Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Low[1] = 0;
      Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_High[1] = 0;
    }
    else 
    {
      // Clear the PBC [id] & [ovp_id] corresponding to PBC address  
      Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_High[0] = 0;
      Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Low[0] = 0;
      Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Avg[0] = 0;     
      Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_High[1] = 0;
      Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Low[1] = 0;
      Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Avg[1] = 0;
    }
    
    // Set the PBC to Optimum power level one last time. 
    PBC_Tx_Msg.Write_Data_Hi = (unsigned char)(PBC_REF_VOLTAGE >> 8);
    PBC_Tx_Msg.Write_Data_Lo = (unsigned char)(PBC_REF_VOLTAGE & 0x00FF);
    //PBC_Tx_Msg.Alarm = PBC_Rx_Msg.Alarm;
    PBC_Tx_Msg.Alarm = 0; // no alram during config
    //Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Configured_OVP_Flag = TRUE;
    PBC_Config_Flag.Config_Busy_flag = FALSE;
    PBC_Config_Flag.Config_Busy_id = 0x00;
    PBC_Config_Flag.Config_Busy_ovp_id = 0x00;    
  }
    
  //Prepare to send data out to PBC controller  
  // Adjust the start address of PBC controller 
  unsigned char channel; 
  if (Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Adj_Ch_Paired_Flag == TRUE)
  {
    ovp_id = ovp_id & 0xFE;
  }
  unsigned int PBC_Register_Address, Error_Check;
  //Send data out to PBC controller
  for (channel = 0; channel < CH_PER_BRICK; channel ++)
  {
    Previous_Rx_Msg.OVP_Raw_Tx_Data[id][Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Avg[channel]].Reg_data_h = PBC_Tx_Msg.Write_Data_Hi;
    Previous_Rx_Msg.OVP_Raw_Tx_Data[id][Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Avg[channel]].Reg_data_l = PBC_Tx_Msg.Write_Data_Lo;
    Previous_Rx_Msg.OVP_Raw_Tx_Data[id][Previous_Rx_Msg.PBC_Register_Map[id][ovp_id].Msg_OVP_Id_Avg[channel]].Reg_Flag = 0;
    
    
    PBC_Tx_Msg.Function = WRITE_SINGLE_REGISTER;
    // check if the OVP message non zero else remove from registry
    PBC_Register_Address =  (PBC_REGISTER_START_ADDR + ((id * MAX_OVP_PER_MSG) + ovp_id)+channel);
    PBC_Tx_Msg.Reg_Addr_Lo = (unsigned char)(PBC_Register_Address & 0x00FF);
    PBC_Tx_Msg.Reg_Addr_Hi = (unsigned char)(PBC_Register_Address >> 8); 
    Set_Tx_Address_Info(PBC_REGISTER_START_ADDR,id,channel);
  
    Error_Check = CRC16(&PBC_Tx_Msg, 6);
    PBC_Tx_Msg.Error_Check_Lo = (unsigned char)(Error_Check &0x00FF);
    PBC_Tx_Msg.Error_Check_Hi = (unsigned char)(Error_Check >> 8);
    status |= Send_Receive_PBC_Data(PBC_Tx_Msg);
    // Check if PBC ref voltage linked
    if (Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Adj_Ch_Paired_Flag == FALSE)
    {
      channel++;
    }
    
//    if (status == FALSE)
//    {
//      break;
//    }
  } 
  // Return return status   
  return status;
}
//#endif

/***************************************************************************//**
 * @param Function      OVP_Mapping
 * @param Input         char, char
 * @param Return        char
 * @param Calling_Func  
 * @param Description   
 ******************************************************************************/
//#if defined MAPPING
unsigned char OVP_Mapping(unsigned char device_id_count, unsigned char ovp_count)
{ 
  unsigned char status = 0;
  unsigned int Ovp_value = ((Rx_Msg.Data_Msg[ovp_count].Reg_data_h * 256) + Rx_Msg.Data_Msg[ovp_count].Reg_data_l);
  if ((Ovp_value > (OVP_LOW_BOUND - OVP_TOLERANCE_WINDOW)) && (Ovp_value < (PBC_MAX_VOLTAGE + OVP_TOLERANCE_WINDOW)))
  {
    if ((PBC_Config_Flag.Config_Busy_flag == TRUE) && (PBC_Config_Flag.Config_Wait_Count <  PBC_CONFIG_WAIT_COUNT))
    {
      PBC_Config_Flag.Config_Wait_Count++;
      status = TRUE;  
    }
    else
    {
      if (((PBC_Config_Flag.Config_Busy_flag == TRUE) && (PBC_Config_Flag.Config_Busy_id == device_id_count) && (PBC_Config_Flag.Config_Busy_ovp_id == ovp_count)) || (PBC_Config_Flag.Config_Busy_flag == FALSE))
      {
        PBC_Config_Flag.Config_Wait_Count = 0;
        if (Configure_OVP(device_id_count,ovp_count))
        {
          status = TRUE;
        }
      }
    }
  }
  return status;
}
//#endif

/***************************************************************************//**
 * @param Function      Process_PBC_HeartBeat_Message
 * @param Input         void
 * @param Return        char
 * @param Calling_Func  
 * @param Description   Function processes and send heartbeat messages
 ******************************************************************************/
unsigned char Process_PBC_HeartBeat_Message()
{
  unsigned int Error_Check;
  // Prepare the message 
  // Send message to PBC of setpoint needs to be updated      
  PBC_Tx_Msg.Slave_Addr = PBC_SLAVE_ADDR;
  PBC_Tx_Msg.Function = WRITE_SINGLE_REGISTER;
  // Prepare 2 byte data
  PBC_Tx_Msg.Write_Data_Hi = 0x00;
  PBC_Tx_Msg.Write_Data_Lo = 0x00;
  if(Verify_OVP_Communication_Flag())
  {
    PBC_Tx_Msg.Alarm = 0x08;
    //PBC_Tx_Msg.Alarm = 0x00;
  }
  else
  {
    PBC_Tx_Msg.Alarm = 0x00;  
  }
  
  PBC_Tx_Msg.Reg_Addr_Lo = (unsigned char)(PBC_HEARTBEAT_START_ADDR & 0x00FF);
  PBC_Tx_Msg.Reg_Addr_Hi = (unsigned char)(PBC_HEARTBEAT_START_ADDR >> 8);
  Set_Tx_Address_Info(PBC_HEARTBEAT_START_ADDR,0,0);
  
  Error_Check = CRC16(&PBC_Tx_Msg, 6);
  PBC_Tx_Msg.Error_Check_Lo = (unsigned char)(Error_Check &0x00FF);
  PBC_Tx_Msg.Error_Check_Hi = (unsigned char)(Error_Check >> 8);
  // Independent of device id and ovp id
  return Send_Receive_PBC_Data(PBC_Tx_Msg);
}

/***************************************************************************//**
 * @param Function      Process_OVP_Message
 * @param Input         void
 * @param Return        void
 * @param Calling_Func  
 * @param Description   Function proceess coming message from surge protection 
 *                      device
 ******************************************************************************/
// Check if message received from new device or routine message received. 
void Process_OVP_Message()
{
  unsigned char id = pRx_Msg->Device_Id;
  unsigned char ovp_id;
  // cycle through each OVP
  for (ovp_id = 0; ovp_id < MAX_OVP_PER_MSG; ovp_id++)
  {
    // if device_id and OVP configured
    if (Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Configured_OVP_Flag == 0)
    {
      #if(MAPPING == OFF)
      unsigned int Ovp_Data = ((pRx_Msg->Data_Msg[ovp_id].Reg_data_h * 256) + (pRx_Msg->Data_Msg[ovp_id].Reg_data_l));
      if ((Ovp_Data > OVP_LOW_BOUND) & (Ovp_Data < PBC_MAX_VOLTAGE))
      {
        Register_PBC_Channel(id,ovp_id); 
      }
      #else
        OVP_Mapping(id, ovp_id);    
      #endif
    }
    else
    {
      if ((PBC_Config_Flag.Config_Busy_flag == FALSE) & (Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Configured_OVP_Flag == 1))
      {
        Process_OVP(id, ovp_id);
      }
    }
    if (Previous_Rx_Msg.PBC_Controller_Info[id][ovp_id].Adj_Ch_Paired_Flag == TRUE)
    {
      ovp_id = ovp_id+1;
    }
  }
//  // Clear processed flag for current device id
//  for (ovp_id = 0; ovp_id < MAX_OVP_PER_MSG; ovp_id++)
//  {
//    Previous_Rx_Msg.OVP_Processed_Flag[id][ovp_id]= FALSE; 
//  }
  Rx_Msg_Flags.end_byte_rx = 0x00;
}

/***************************************************************************//**
 *                         End of Function
 ******************************************************************************/